<?php

namespace App\Utility;

use App\Model\Entity\Attachment;
use Cake\Http\Client;
use Cake\Utility\Text;

class SxVimeoUtility
{
    public static $api = 'http://vimeo.com/api/v2/video/%s.json';

    public static function api($id)
    {
        return sprintf(self::$api, $id);
    }

    public static function getUrl($id)
    {
        if ( is_a($id, Attachment::class) ) {
            $id = $id->get('identifier');
        }

        return "https://player.vimeo.com/video/{$id}";
    }

    public static function extractID($url)
    {
        $match = preg_match('/(?<=\/)([0-9]+)(?=&|$)/', $url, $result);

        if ( $match == true ) {
            return $result[0];
        }

        throw new \Exception(__d('system', 'Vimeo ID konnte nicht ausgelesen werden.'));
    }

    public static function getResponse($id)
    {
        if ( preg_match('/^https?:\/\//', $id) ) {
            $id = self::extractID($id);
        }

        $http = new Client();

        $response = $http->get(self::api($id));

        if ( $response->getStatusCode() !== 200 ) {
            throw new \Exception(__d('system', 'Vimeo API nicht erreichbar oder Video nicht existent.'));
        }

        return array_get($response->getJson(), '0', null);
    }


    public static function getAttachmentData($url, $entity)
    {
        $data = self::getResponse($url);

        if ( $data === null ) {
            throw new \Exception(__d('system', 'Vimeo Video nicht gefunden.'));
        }

        // Set youtube provider
        $entity->set('provider', 'vimeo');

        $identifier = array_get($data, 'id');
        $entity->set('identifier', $identifier);

        // Get file name
        $name = array_get($data, 'title');
        $entity->set('name', $name);

        $file = array_get($data, 'thumbnail_large');

        $thumbnail = $entity->getTable()->newEntity([
            'name' => $name, 'file' => $file, 'parent_id' => $entity->parent_id
        ]);

        $thumbnail = $entity->getTable()->save($thumbnail);

        $entity->set('thumb_id', $thumbnail->id);

        return $entity;
    }


}
