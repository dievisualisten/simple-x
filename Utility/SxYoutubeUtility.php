<?php

namespace App\Utility;

use App\Model\Entity\Attachment;
use Cake\Http\Client;
use Cake\Utility\Text;

class SxYoutubeUtility
{
    public static $api = 'https://www.googleapis.com/youtube/v3/videos?id=%s&key=%s&part=snippet';

    public static function api($id, $key = null)
    {
        if ( $key === null ) {
            $key = config('Youtube.key');
        }

        return sprintf(self::$api, $id, $key);
    }

    public static function getUrl($id)
    {
        if ( is_a($id, Attachment::class) ) {
            $id = $id->get('identifier');
        }

        return "https://www.youtube.com/embed/{$id}";
    }

    public static function extractID($url)
    {
        $url = trim($url);

        $match = preg_match('/(?<=(\?|&)v=)(.*?)(?=&|$)/', $url, $result);

        if ( $match == true ) {
            return $result[0];
        }

        $match = preg_match('/(?<=\/embed\/|\.be\/)(.*?)(?=\?|$)/', $url, $result);

        if ( $match == true ) {
            return $result[0];
        }

        throw new \Exception(__d('system', 'Youtube ID konnte nicht ausgelesen werden.'));
    }

    public static function getResponse($id)
    {
        if ( preg_match('/^https?:\/\//', $id) ) {
            $id = self::extractID($id);
        }

        $http = new Client();

        $response = $http->get(self::api($id));

        if ( $response->getStatusCode() !== 200 ) {
            throw new \Exception(__d('system', 'Youtube API nicht erreichbar oder Video nicht existent.'));
        }

        return array_get($response->getJson(), 'items.0', null);
    }


    public static function getAttachmentData($url, $entity)
    {
        $data = self::getResponse($url);

        if ( $data === null ) {
            throw new \Exception(__d('system', 'Youtube Video nicht gefunden.'));
        }

        // Set youtube provider
        $entity->set('provider', 'youtube');

        $identifier = array_get($data, 'id');
        $entity->set('identifier', $identifier);

        // Get file name
        $name = array_get($data, 'snippet.title');
        $entity->set('name', $name);

        $file = null;

        $file = array_get($data,'snippet.thumbnails.default.url', $file);
        $file = array_get($data,'snippet.thumbnails.medium.url', $file);
        $file = array_get($data,'snippet.thumbnails.high.url', $file);
        $file = array_get($data,'snippet.thumbnails.maxres.url', $file);

        $thumbnail = $entity->getTable()->newEntity([
            'name' => $name, 'file' => $file, 'parent_id' => $entity->parent_id
        ]);

        $thumbnail = $entity->getTable()->save($thumbnail);

        $entity->set('thumb_id', $thumbnail->id);

        return $entity;
    }


}
