<?php

namespace App\Library\Utility;

class SxGeoUtility
{
    protected function getLatLon($address, $updateLatLon = true, $updateAddressData = true, $setAddressData = false)
    {

        $search = "";

        if (is_array($address)) {
            $fields = ['street', 'number', 'zip', 'city', 'country', 'address'];
            $a = "";
            foreach ($fields as $field) {
                if (!empty($address[$field])) {
                    $a = $a . $address[$field] . ' ';
                };
            }

            $search = $a;
            $result = $address;
        } else {
            $search = $address;
            $result = [];
        }

        $update = (empty($result['lat']) || empty($result['lon']));

        if ($updateLatLon || $update) {

            $url = 'http://maps.google.com/maps/api/geocode/json?address=' . urlencode($search) . '&sensor=false&language=' . Configure::read('Config.language');

            $response = @file_get_contents($url);

            if ($response === false) {
                return $result;
            }

            $response = json_decode($response);

            if ($response->status != 'OK') {
                return $result;
            }


            $result['lat'] = $response->results['0']->geometry->location->lat;
            $result['lon'] = $response->results['0']->geometry->location->lng;
            $result['googleaddress'] = $response->results['0']->formatted_address;

            if ($updateAddressData) {
                foreach ($response->results['0']->address_components as $data) {

                    if ($data->types['0'] == 'country' && ($setAddressData || isset($result['country']))) {
                        $result['country'] = strtolower($data->short_name);
                    }

                    if ($data->types['0'] == 'route' && ($setAddressData || isset($result['street']))) {
                        $result['street'] = $data->long_name;
                    }

                    if ($data->types['0'] == 'street_number' && ($setAddressData || isset($result['number']))) {
                        $result['number'] = $data->long_name;
                    }

                    if ($data->types['0'] == 'locality' && ($setAddressData || isset($result['city']))) {
                        $result['city'] = $data->long_name;
                    }

                    if ($data->types['0'] == 'postal_code' && ($setAddressData || isset($result['zip']))) {
                        $result['zip'] = $data->long_name;
                    }

                    if ($data->types['0'] == 'administrative_area_level_1' && ($setAddressData || isset($result['state']))) {
                        $result['state'] = $data->long_name;
                    }

                }
            }
        }

        return $result;
    }

}
