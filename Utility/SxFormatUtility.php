<?php

namespace App\Utility;

use Cake\I18n\Number;

class SxFormatUtility
{
    public static function currency($number, $format = [])
    {
        $format = array_merge([
            'places'    => 2,
            'escape'    => false,
            'before'    => ' € ',
            'decimals'  => ',',
            'thousands' => '.',
        ], $format);

        $number = Number::format(floatval($number), $format);

        $split = explode(',', $number);

        if ( isset($split[1]) ) {
            $split[1] = '<span class="price-decimals">' . $split[1] . '</span>';
        }

        $number = implode(',', $split);

        return $number;
    }

}
