<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class EventsFixture extends TestFixture
{
    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model'      => 'Events',
        'connection' => 'default',
    ];

    public function init()
    {
        $this->records = [

        ];

        parent::init();
    }

}
