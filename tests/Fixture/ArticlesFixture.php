<?php

namespace App\Test\Fixture;

use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class ArticlesFixture extends TestFixture
{
    use ModelAwareTrait;

    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model'      => 'Articles',
        'connection' => 'default',
    ];

    public function insert(ConnectionInterface $db)
    {
        $this->loadModel('Articles');

        $entities = $this->Articles->newEntities([
            [
                'id'          => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
                'type'        => 'page',
                'layout'      => 'default',
                'teasertext'  => '<a href="' . domain() . '/article-1">' . domain() . '/article-1</a>',
                'teasertext2' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'title'       => 'Article 1 DE',
                'slug'        => 'article-1',
                'content'     => 'Lorem ipsum <a href="' . domain() . '/article-1">' . domain() . '/article-1</a> dolore',
                'content2'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
                'canonical'   => domain() . '/article-1',
                '_locale'     => 'de',
            ],
            [
                'id'         => 'cb73bb24-4eca-4601-b075-62a2481c0a41',
                'type'       => 'page',
                'layout'     => 'default',
                'teasertext' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'title'      => 'Article 2 DE',
                'slug'       => 'article-2',
                'content'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
                'canonical'  => null,
                '_locale'    => 'de',
            ],
            [
                'id'         => '3192148c-9b77-44db-9242-955a9194b40c',
                'type'       => 'page',
                'layout'     => 'default',
                'teasertext' => '<a href="' . domain() . '/article-3">' . domain() . '/article-3</a>',
                'title'      => 'Article 3 DE',
                'slug'       => 'article-3',
                'content'    => 'Lorem ipsum <a href="' . domain() . '/article-3">' . domain() . '/article-3</a> dolore',
                'canonical'  => null,
                '_locale'    => 'de',
            ],
            [
                'id'         => 'c316026d-7fe6-45ac-bc22-e33c621df4f7',
                'type'       => 'page',
                'layout'     => 'default',
                'teasertext' => '<a href="' . domain() . '/article-4">' . domain() . '/article-4</a>',
                'title'      => 'Article 4 DE',
                'slug'       => 'article-4',
                'content'    => 'Lorem ipsum <a href="' . domain() . '/article-4">' . domain() . '/article-4</a> dolore',
                'canonical'  => null,
                '_locale'    => 'de',
            ],
        ]);

        $this->Articles->saveMany($entities);

        parent::insert($db);
    }

}
