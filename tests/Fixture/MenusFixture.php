<?php

namespace App\Test\Fixture;

use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class MenusFixture extends TestFixture
{
    use ModelAwareTrait;

    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model' => 'Menus', 'connection' => 'default',
    ];

    public function insert(ConnectionInterface $db)
    {
        $this->loadModel('Menus');

        $entities = $this->Menus->newEntities([
            [
                'id'          => 'f7b22b29-daa5-42d3-8b15-6104dea7b97d',
                'parent_id'   => null,
                'foreign_key' => null,
                'type'        => 'domain',
                'slug'        => config('liveDomain'),
                'title'       => config('liveDomain'),
                'path'        => config('liveDomain'),
                'model'       => 'menu',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => '2a30457d-652d-4c0c-a887-bcfc802cff5d',
                'parent_id'   => 'f7b22b29-daa5-42d3-8b15-6104dea7b97d',
                'foreign_key' => null,
                'type'        => 'menu',
                'slug'        => null,
                'title'       => 'Mainmenu',
                'teaser'      => 1,
            ],
            [
                'id'          => 'bfb121a5-e64c-45f0-9157-0107248ead01',
                'parent_id'   => '2a30457d-652d-4c0c-a887-bcfc802cff5d',
                'foreign_key' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
                'type'        => 'page',
                'slug'        => 'article-1',
                'title'       => 'Article 1 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => '3139568b-75a2-45ed-888c-08659c17e3d3',
                'parent_id'   => 'bfb121a5-e64c-45f0-9157-0107248ead01',
                'foreign_key' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
                'type'        => 'page',
                'slug'        => 'article-1',
                'title'       => 'Article 1 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => 'd4262797-6541-4278-8d4d-d5bb4fe85523',
                'parent_id'   => '2a30457d-652d-4c0c-a887-bcfc802cff5d',
                'foreign_key' => 'cb73bb24-4eca-4601-b075-62a2481c0a41',
                'type'        => 'page',
                'slug'        => 'article-2',
                'title'       => 'Article 2 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => 'ab9c9844-b7a0-4f2f-8cc1-0a329b03545f',
                'parent_id'   => 'd4262797-6541-4278-8d4d-d5bb4fe85523',
                'foreign_key' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
                'type'        => 'page',
                'slug'        => 'article-1',
                'title'       => 'Article 1 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => 'e136c60f-a409-453b-bc72-16c980b3d1fa',
                'parent_id'   => '2a30457d-652d-4c0c-a887-bcfc802cff5d',
                'foreign_key' => '3192148c-9b77-44db-9242-955a9194b40c',
                'type'        => 'page',
                'slug'        => 'article-3',
                'title'       => 'Article 3 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => '213fdc3f-d120-4123-81d9-55de6eff17df',
                'parent_id'   => 'e136c60f-a409-453b-bc72-16c980b3d1fa',
                'foreign_key' => '3192148c-9b77-44db-9242-955a9194b40c',
                'type'        => 'page',
                'slug'        => 'article-3',
                'title'       => 'Article 3 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
            [
                'id'          => '1696108f-7d95-4eab-8e69-d37fc36386df',
                'parent_id'   => '2a30457d-652d-4c0c-a887-bcfc802cff5d',
                'foreign_key' => 'c316026d-7fe6-45ac-bc22-e33c621df4f7',
                'type'        => 'page',
                'slug'        => 'article-4',
                'title'       => 'Article 4 DE',
                'model'       => 'Article',
                'controller'  => 'articles',
                'action'      => 'view_page',
                '_locale'     => 'de',
                'teaser'      => 1,
            ],
        ]);

        $this->Menus->saveMany($entities);

        parent::insert($db);
    }

}
