<?php

namespace App\Test\Fixture;

use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class ArticlesTranslationsFixture extends TestFixture
{
    use ModelAwareTrait;

    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model'      => 'ArticlesTranslations',
        'connection' => 'default',
    ];

    public function insert(ConnectionInterface $db)
    {
        $this->loadModel('Articles');
        $this->loadModel('Menus');

        $menus = $this->Menus->find()
            ->enableHydration(
                false
            )
            ->select([
                'id' => 'parent_id', 'teaser' => 'teaser',
            ])
            ->order([
                'lft ASC',
            ]);

        $entities = $this->Articles->newEntities([
            [
                'id'         => 'cb73bb24-4eca-4601-b075-62a2481c0a41',
                'type'       => 'page',
                'layout'     => 'default',
                'teasertext' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'title'      => 'Article 2 EN',
                'slug'       => 'article-2',
                'content'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
                'canonical'  => null,
                '_locale'    => 'en',
            ],
            [
                'id'          => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
                'type'        => 'page',
                'layout'      => 'default',
                'teasertext'  => '<a href="' . domain() . '/article-1">' . domain() . '/article-1</a>',
                'teasertext2' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'title'       => 'Article 1 EN',
                'slug'        => 'article-1',
                'content'     => 'Lorem ipsum <a href="' . domain() . '/article-1">' . domain() . '/article-1</a> dolore',
                'content2'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
                'canonical'   => domain() . '/article-1',
                '_locale'     => 'en',
            ],
        ]);

        foreach ( $entities as $entity ) {
            $this->Articles->save($entity);
        }

        parent::insert($db);
    }

}
