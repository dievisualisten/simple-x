<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class CrontasksFixture extends TestFixture
{
    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model' => 'Crontasks', 'connection' => 'default',
    ];

    public function init()
    {
        $this->records = [
            [
                'id'      => '2acdba62-cb89-4a1b-b0f0-0c48297c9e81',
                'name'    => 'checkLongRunningTasks',
                'method'  => 'checkLongRunningTasks',
                'execute' => 'always',
                'active'  => 1,
            ],
            [
                'id'      => '710893fc-651a-4c1a-992b-8886c477d5fc',
                'name'    => 'make301Redirects',
                'method'  => 'make301Redirects',
                'execute' => 'always',
                'active'  => 1,
            ],
            [
                'id'      => '63b9b040-f2cb-4d3c-b839-7cad8992e291',
                'name'    => 'replaceMovedArticleUrls',
                'method'  => 'replaceMovedArticleUrls',
                'execute' => 'always',
                'active'  => 1,
            ],
            [
                'id'      => 'ad1899ed-932b-410a-bc19-287f7431e188',
                'name'    => 'replaceMovedCanonicalUrls',
                'method'  => 'replaceMovedCanonicalUrls',
                'execute' => 'always',
                'active'  => 1,
            ],
            [
                'id'      => '9a75f1b5-4282-4642-953d-d967867c18e1',
                'name'    => 'newsletterSendToSend',
                'method'  => 'newsletterSendToSend',
                'execute' => 'always',
                'active'  => 1,
            ],
        ];

        parent::init();
    }

}
