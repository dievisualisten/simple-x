<?php

namespace App\Test\Fixture;

use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class MenusFixture
 *
 * @package App\Test\Fixture
 */
class MenusTranslationsFixture extends TestFixture
{
    use ModelAwareTrait;

    /**
     * @var string
     */
    public $connection = 'test';

    /**
     * @var array
     */
    public $import = [
        'model'      => 'MenusTranslations',
        'connection' => 'default',
    ];

    public function insert(ConnectionInterface $db)
    {
        $this->loadModel('Menus');

        $entities = $this->Menus->newEntities([
            [
                'id'          => 'f7b22b29-daa5-42d3-8b15-6104dea7b97d',
                'type'        => 'domain',
                'slug'        => config('liveDomain') . '/en',
                'title'       => config('liveDomain'),
                'path'        => config('liveDomain') . '/en',
                '_locale'      => 'en',
            ]
        ]);

        $this->Menus->saveMany($entities);

        parent::insert($db);
    }

}
