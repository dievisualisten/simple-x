<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContentelementstructuresFixture
 *
 */
class ContentelementstructuresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'article_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'contentelement_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'parent_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'sequence' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'article_id' => ['type' => 'index', 'columns' => ['article_id'], 'length' => []],
            'contentelement_id' => ['type' => 'index', 'columns' => ['contentelement_id'], 'length' => []],
            'parent_id' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '15fa2ea4-61e9-4d24-a179-e5b95404b982',
                'article_id' => '9ae4059f-4f5a-44f0-a0df-8c936ce4029d',
                'contentelement_id' => '41034fba-1b59-4379-aaa4-f4391cfc60a0',
                'parent_id' => '5ecd7b2e-0ab8-4ee7-9093-63758eb6b694',
                'sequence' => 1,
                'created' => '2019-03-31 12:25:20',
                'modified' => '2019-03-31 12:25:20'
            ],
        ];
        parent::init();
    }
}
