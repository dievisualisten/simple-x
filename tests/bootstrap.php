<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

define('PHPUNIT_MODE', true);

/**
 * Configure paths required to find CakePHP + general filepath constants
 */
require dirname(__DIR__) . '/src-sx/bootstrap/paths.php';

/*
 * Load application functions
 */
require dirname(__DIR__) . '/src-sx/bootstrap/functions.php';

/**
 * Load composer autoload
 */
require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Load base application bootstrap
 */
require dirname(__DIR__) . '/src/bootstrap/bootstrap.php';


/**
 * Set php self to backslash - dunno why
 */
$_SERVER['PHP_SELF'] = '/';
