<?php

namespace App\Test\TestCase\Command;

use Cake\Console\ConsoleIo;
use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

class Make301RedirectsTaskTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use ModelAwareTrait;

    protected $Articles;

    protected $Menus;

    protected $Redirects;

    public $fixtures = [
        'app.Crontasks',
        'app.Menus',
        'app.MenusTranslations',
        'app.MenusTemporary',
        'app.Articles',
        'app.ArticlesTranslations',
        'app.Redirects',
        'app.Events',
    ];

    public function setUp()
    {
        parent::setUp();
        $this->useCommandRunner();
        $this->loadModel('Articles');
        $this->loadModel('Menus');
        $this->loadModel('MenusTranslations');
        $this->loadModel('Redirects');
    }

    public function testCrontaskFunctionality()
    {
        $this->exec('crontasks Make301Redirects');
        $this->assertOutputContains('Make301Redirects is done!');
    }

    public function testRenameSiteOneChild()
    {
        $this->exec('crontasks Make301Redirects');

        $article = $this->Articles->get('47dc2e39-57d4-4004-b49f-c74cff2c8172');

        $this->Articles->patchEntity($article, [
            'slug'       => 'article-1-renamed',
            '_locale'    => 'de',
        ]);

        $this->Articles->save($article);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-1-renamed/article-1-renamed',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/article-2/article-1',
                'target_url' => domain() . '/article-2/article-1-renamed',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/article-1',
                'target_url' => domain() . '/article-1-renamed',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testMoveSiteOneChild()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('3139568b-75a2-45ed-888c-08659c17e3d3');

        $this->Menus->patchEntity($menu, [
            'parent_id' => 'd4262797-6541-4278-8d4d-d5bb4fe85523',
        ]);

        $this->Menus->save($menu);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-2/article-1--1',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-1/article-1',
                'target_url' => domain() . '/en/article-2/article-1--1',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testMoveAndRevertSiteOneChild()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('3139568b-75a2-45ed-888c-08659c17e3d3');

        $this->Menus->patchEntity($menu, [
            'parent_id' => 'd4262797-6541-4278-8d4d-d5bb4fe85523',
        ]);

        $this->Menus->save($menu);

        $this->exec('crontasks Make301Redirects');

        $this->Menus->patchEntity($menu, [
            'parent_id' => 'bfb121a5-e64c-45f0-9157-0107248ead01',
        ]);

        $this->Menus->save($menu);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-2/article-1--1',
                'status'     => config('Redirect.states.deactivated'),
                'active'     => false,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-1/article-1',
                'target_url' => domain() . '/en/article-2/article-1--1',
                'status'     => config('Redirect.states.deactivated'),
                'active'     => false,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/article-2/article-1--1',
                'target_url' => domain() . '/article-1/article-1',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-2/article-1--1',
                'target_url' => domain() . '/en/article-1/article-1',
                'status'     => config('Redirect.states.moved'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testMoveAndRevertLoopSiteOneChild()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('3139568b-75a2-45ed-888c-08659c17e3d3');

        foreach ( [ 0, 1, 2 ] as $i ) {

            $this->Menus->patchEntity($menu, [
                'parent_id' => 'd4262797-6541-4278-8d4d-d5bb4fe85523',
            ]);

            $this->Menus->save($menu);

            $this->exec('crontasks Make301Redirects');

            $this->Menus->patchEntity($menu, [
                'parent_id' => 'bfb121a5-e64c-45f0-9157-0107248ead01',
            ]);

            $this->Menus->save($menu);

            $this->exec('crontasks Make301Redirects');
        }

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-2/article-1--1',
                'status'     => config('Redirect.states.deactivated'),
                'active'     => false,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-1/article-1',
                'target_url' => domain() . '/en/article-2/article-1--1',
                'status'     => config('Redirect.states.deactivated'),
                'active'     => false,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/article-2/article-1--1',
                'target_url' => domain() . '/article-1/article-1',
                'status'     => config('Redirect.states.activated'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-2/article-1--1',
                'target_url' => domain() . '/en/article-1/article-1',
                'status'     => config('Redirect.states.activated'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testRemoveSiteOneChild()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('3139568b-75a2-45ed-888c-08659c17e3d3');

        $this->Menus->delete($menu);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-2/article-1',
                'status'     => config('Redirect.states.dublicated'),
                'active'     => false,
                'notify'     => true,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/article-1/article-1',
                'target_url' => domain() . '/article-1',
                'status'     => config('Redirect.states.dublicated'),
                'active'     => true,
                'notify'     => true,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-1/article-1',
                'target_url' => domain() . '/en/article-2/article-1',
                'status'     => config('Redirect.states.dublicated'),
                'active'     => false,
                'notify'     => true,
                'locked'     => false,
            ],
            [
                'from_url'   => domain() . '/en/article-1/article-1',
                'target_url' => domain() . '/en/article-1',
                'status'     => config('Redirect.states.dublicated'),
                'active'     => true,
                'notify'     => true,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testRemoveSiteThreeChild()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('213fdc3f-d120-4123-81d9-55de6eff17df');

        $this->Menus->delete($menu);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-3/article-3',
                'target_url' => domain() . '/article-3',
                'status'     => config('Redirect.states.changed'),
                'active'     => true,
                'notify'     => false,
                'locked'     => false,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testRemoveSiteFour()
    {
        $this->exec('crontasks Make301Redirects');

        $menu = $this->Menus->get('1696108f-7d95-4eab-8e69-d37fc36386df');

        $this->Menus->delete($menu);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-4',
                'target_url' => '',
                'status'     => config('Redirect.states.removed'),
                'active'     => false,
                'notify'     => true,
                'locked'     => true,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testSelfRefrencedSiteTest()
    {
        $this->exec('crontasks Make301Redirects');

        $redirect = $this->Redirects->newEntity([
            'from_url'   => domain() . '/article-redirects-to-itself',
            'target_url' => domain() . '/article-redirects-to-itself',
        ]);

        $this->Redirects->save($redirect);

        $this->exec('crontasks Make301Redirects');

        $result = $this->Redirects->find()
            ->select([
                'from_url', 'target_url', 'status', 'active', 'notify', 'locked',
            ])
            ->order([
                'id ASC',
            ]);

        $assert = [
            [
                'from_url'   => domain() . '/article-redirects-to-itself',
                'target_url' => domain() . '/article-redirects-to-itself',
                'status'     => config('Redirect.states.refrenced'),
                'active'     => false,
                'notify'     => true,
                'locked'     => true,
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }


}
