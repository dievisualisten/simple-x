<?php

namespace App\Test\TestCase\Command;

use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

class ReplaceMovedArticleUrlsTaskTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use ModelAwareTrait;

    protected $Articles;

    protected $Redirects;

    public $fixtures = [
        'app.Crontasks',
        'app.Menus',
        'app.MenusTranslations',
        'app.Articles',
        'app.ArticlesTranslations',
        'app.Redirects',
    ];

    public function setUp()
    {
        parent::setUp();
        $this->useCommandRunner();
        $this->loadModel('Articles');
        $this->loadModel('ArticlesTranslations');
        $this->loadModel('Redirects');
    }

    public function testCrontaskFunctionality()
    {
        $this->exec('crontasks ReplaceMovedArticleUrls');
        $this->assertOutputContains('ReplaceMovedArticleUrls is done!');
    }

    public function testReplaceArticleUrlTest()
    {
        $redirect = $this->Redirects->newEntity([
            'from_url'   => domain() . '/article-1',
            'target_url' => domain() . '/article-3',
            'active'     => true,
        ]);

        $this->Redirects->save($redirect);

        $this->exec('crontasks ReplaceMovedArticleUrls');

        $result = $this->Articles->find()
            ->where([
                'id' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
            ])
            ->select([
                'teasertext', 'teasertext2', 'content', 'content2',
            ]);

        $assert = [
            [
                'teasertext'  => '<a href="' . domain() . '/article-3">' . domain() . '/article-3</a>',
                'teasertext2' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'content'     => 'Lorem ipsum <a href="' . domain() . '/article-3">' . domain() . '/article-3</a> dolore',
                'content2'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

    public function testReplaceArticleTranslationUrlTest()
    {
        $redirect = $this->Redirects->newEntity([
            'from_url'   => domain() . '/article-1',
            'target_url' => domain() . '/article-3',
            'active'     => true,
        ]);

        $this->Redirects->save($redirect);

        $this->exec('crontasks ReplaceMovedArticleUrls');

        $result = $this->ArticlesTranslations->find()
            ->where([
                'id' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
            ])
            ->select([
                'teasertext', 'teasertext2', 'content', 'content2',
            ]);

        $assert = [
            [
                'teasertext'  => '<a href="' . domain() . '/article-3">' . domain() . '/article-3</a>',
                'teasertext2' => '<a href="' . domain() . '/article-2">' . domain() . '/article-2</a>',
                'content'     => 'Lorem ipsum <a href="' . domain() . '/article-3">' . domain() . '/article-3</a> dolore',
                'content2'    => 'Lorem ipsum <a href="' . domain() . '/article-2">' . domain() . '/article-2</a> dolore',
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

}
