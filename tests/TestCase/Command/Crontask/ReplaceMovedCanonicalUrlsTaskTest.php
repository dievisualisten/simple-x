<?php

namespace App\Test\TestCase\Command;

use Cake\Datasource\ModelAwareTrait;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

class ReplaceMovedCanonicalUrlsTaskTest extends TestCase
{
    use ConsoleIntegrationTestTrait;
    use ModelAwareTrait;

    protected $Articles;

    protected $Redirects;

    public $fixtures = [
        'app.Crontasks',
        'app.Menus',
        'app.MenusTranslations',
        'app.Articles',
        'app.ArticlesTranslations',
        'app.Redirects',
    ];

    public function setUp()
    {
        parent::setUp();
        $this->useCommandRunner();
        $this->loadModel('Articles');
        $this->loadModel('ArticlesTranslations');
        $this->loadModel('Redirects');
    }

    public function testCrontaskFunctionality()
    {
        $this->exec('crontasks ReplaceMovedCanonicalUrls');
        $this->assertOutputContains('ReplaceMovedCanonicalUrls is done!');
    }

    public function testReplaceArticleUrlTest()
    {
        $redirect = $this->Redirects->newEntity([
            'from_url'   => domain() . '/article-1',
            'target_url' => domain() . '/article-3',
            'active'     => true,
        ]);

        $this->Redirects->save($redirect);

        $this->exec('crontasks ReplaceMovedCanonicalUrls');

        $result = $this->Articles->find()
            ->where([
                'id' => '47dc2e39-57d4-4004-b49f-c74cff2c8172',
            ])
            ->select([
                'canonical',
            ]);

        $assert = [
            [
                'canonical'  => domain() . '/article-3'
            ],
        ];

        $this->assertEquals($result->enableHydration(false)->toArray(), $assert);
    }

}
