<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentelementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentelementsTable Test Case
 */
class ContentelementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentelementsTable
     */
    public $Contentelements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Contentelements',
        'app.Contentelementstructures',
        'app.Menus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Contentelements') ? [] : ['className' => ContentelementsTable::class];
        $this->Contentelements = TableRegistry::getTableLocator()->get('Contentelements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contentelements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
