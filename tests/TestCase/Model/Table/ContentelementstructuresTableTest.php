<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentelementstructuresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentelementstructuresTable Test Case
 */
class ContentelementstructuresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentelementstructuresTable
     */
    public $Contentelementstructures;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Contentelementstructures',
        'app.Articles',
        'app.Attachments',
        'app.Menus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Contentelementstructures') ? [] : ['className' => ContentelementstructuresTable::class];
        $this->Contentelementstructures = TableRegistry::getTableLocator()->get('Contentelementstructures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contentelementstructures);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
