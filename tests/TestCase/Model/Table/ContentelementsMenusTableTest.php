<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentelementsMenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentelementsMenusTable Test Case
 */
class ContentelementsMenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentelementsMenusTable
     */
    public $ContentelementsMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ContentelementsMenus',
        'app.Menus',
        'app.Contentelements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContentelementsMenus') ? [] : ['className' => ContentelementsMenusTable::class];
        $this->ContentelementsMenus = TableRegistry::getTableLocator()->get('ContentelementsMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentelementsMenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
