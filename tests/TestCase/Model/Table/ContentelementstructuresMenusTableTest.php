<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentelementstructuresMenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentelementstructuresMenusTable Test Case
 */
class ContentelementstructuresMenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentelementstructuresMenusTable
     */
    public $ContentelementstructuresMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ContentelementstructuresMenus',
        'app.Menus',
        'app.Contentelementstructurs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContentelementstructuresMenus') ? [] : ['className' => ContentelementstructuresMenusTable::class];
        $this->ContentelementstructuresMenus = TableRegistry::getTableLocator()->get('ContentelementstructuresMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentelementstructuresMenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
