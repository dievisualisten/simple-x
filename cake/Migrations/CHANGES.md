##Changes
#####Migrations\\ConfigurationTrait.php:66

```php
$seedsPath = $this->getOperationsPath($this->input, 'Seeds'); // Original
$seedsPath = $this->getOperationsPath($this->input, 'src/Seeds'); // New
```

#####Migrations\\Util\\UtilTrait.php:63

```php
protected function getOperationsPath(InputInterface $input, $default = 'Migrations') // Original
protected function getOperationsPath(InputInterface $input, $default = 'src/Migrations') // New
```

#####Migrations\\Util\\UtilTrait.php:67

```php
$dir = ROOT . DS . 'config' . DS . $folder; // Original
$dir = ROOT . DS . $folder; // New
```

#####Migrations\\Shell\\Task\\SeedTask.php:36

```php
public $pathFragment = 'config/Seeds/'; // Original
public $pathFragment = 'src/Seeds/'; // New
```

#####Migrations\\Shell\\Task\\SimpleMigrationTask.php:32

```php
public $pathFragment = 'config/Migrations/'; // Original
public $pathFragment = 'src/Migrations/'; // New
```
