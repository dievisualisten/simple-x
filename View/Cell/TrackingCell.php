<?php

namespace App\View\Cell;

use Cake\View\Cell;


class TrackingCell extends Cell
{
    public function formConversionTracking()
    {
        $formularconfig_id = $this->request->getQuery('formularconfig_id');
        if ( $formularconfig_id ) {
            $this->loadModel('Formularconfigs');
            $formularconfig = $this->Formularconfigs->get($formularconfig_id);
            if ( $formularconfig ) {
                if($formularconfig->get('use_conversion_tracking')){
                    $this->set('tracking', $formularconfig);
                }

            }
        }
    }
}
