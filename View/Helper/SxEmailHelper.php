<?php

namespace App\View\Helper;

use Cake\Event\Event;


class SxEmailHelper extends \Gourmet\Email\View\Helper\EmailHelper
{
    public function beforeRenderFile(Event $event, $viewFile)
    {
        if (strpos($viewFile, 'Email' . DS . 'text') !== -1) {
            $this->_emailType = 'text';
        } else {
            if (strpos($viewFile, 'Email' . DS . 'html') !== -1) {
                $this->_emailType = 'html';
            }
        }


        $this->_eol = 'text' == $this->_emailType ? PHP_EOL : '<br>';
    }
}

?>
