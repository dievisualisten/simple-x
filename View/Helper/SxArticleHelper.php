<?php

namespace App\View\Helper;

use App\Library\DomainManager;
use App\Library\Facades\Menu;
use App\Library\Facades\Request;
use App\Model\Entity\Article;
use App\Utility\StringUtility;
use Cake\I18n\Time;
use Cake\Utility\Hash;
use Cake\View\Helper;
use Cake\View\View;

/**
 * @version 4.0.0
 */
class SxArticleHelper extends Helper
{

    /**
     * @var \App\Model\Entity\Article|mixed
     */
    protected $rootArticle;

    /**
     * @var
     */
    protected $currentArticle;

    public function __construct(View $view, array $config = [])
    {
        $article = $view->get('data');

        if ( is_a($article, Article::class) ) {
            $this->rootArticle = $article;
        }

        parent::__construct($view, $config);
    }

    public function root(&$article = null)
    {
        if ( ! empty($article) ) {
            $this->rootArticle = $article;
        }

        return $this->rootArticle;
    }

    /**
     * @param null $article
     * @return mixed
     */
    public function current(&$article = null)
    {
        if ( ! isset($this->rootArticle) ) {
            $this->root($article);
        }

        if ( ! empty($article) ) {
            $this->currentArticle = $article;
        }

        return $this->currentArticle;
    }

    /**
     * Results where visibility is present in key
     *
     * @param int $key
     * @param array|null $data
     * @return array
     */
    public function matrix(int $key, array $data = null): array
    {
        if ( is_null($data) ) {
            $data = $this->rootArticle->get('children', []);
        }

        $items = array_filter($data, function ($item) use ($key) {
            return in_array($key, Menu::getMenuVisibility($item->menu->teaser));
        });

        return collect($items)->values()->toArray();
    }

    /**
     * @param null $data
     * @param string $key
     * @return array|mixed|null
     */
    public function getChildrenDataList($data = null, string $key = 'children')
    {

        if ( is_null($data) ) {
            $data = $this->rootArticle->get($key, []);
        }

        if ( is_a($data, 'Cake\Datasource\ResultSetDecorator') ) {
            $data = $data->toArray();
        }

        return $data;
    }

    /**
     * Results where visibility is present in key
     *
     * @param string $key
     * @param array|null $data
     * @return \App\Utility\CollectionUtility
     */
    public function type(string $key, array $data = null): \App\Utility\CollectionUtility
    {
        $data = $this->getChildrenDataList($data);

        $items = array_filter($data, function ($item) use ($key) {
            return $item->type === $key;
        });

        return collect($items);
    }

    /**
     * Results where geolocation is present
     *
     * @param array $data
     * @return \App\Utility\CollectionUtility
     */
    public function markers($data = null)
    {
        $data = $this->getChildrenDataList($data);

        $items = array_filter($data, function ($item) {
            return ! $item->isEmpty('lat') && ! $item->isEmpty('lon');
        });

        return collect($items);
    }

    public function groupByDomain($key, $group, $data = null): array
    {
        $domains = DomainManager::getDomain($group);

        $data = $this->getChildrenDataList($data);
        $groups = [];

        foreach ( $domains as $domain => $title ) {

            $group = [
                'domain' => $domain, 'title' => $title, 'items' => [],
            ];

            foreach ( $data as $item ) {
                if ( in_array($domain, (array) $item->get($key, [])) ) {
                    $group['items'][] = $item;
                }
            }

            $groups[] = array_merge($group, [
                'ids' => Hash::extract($group['items'], '{n}.id'),
            ]);
        }

        return $groups;
    }

    public function paginate($data = null)
    {
        if ( $data === null ) {
            $data = $this->getChildrenDataList($data);
        }

        //ToDo extended_data_cfg.article.paging_count
        return collect($data)->forPage(Request::param('page', 1),
            Request::param('limit', config('Sx.app.articles.limit', 10)))->toArray();
    }

    /**
     * @param null $data
     * @return array
     */
    public function pages($data = null): array
    {
        $data = $this->getChildrenDataList($data);

        //ToDo extended_data_cfg.article.paging_count
        $pages = collect($data)->forPages(Request::param('page', 1),
            Request::param('limit', config('Sx.app.articles.limit', 10)))->toArray();

        foreach ( $pages as $page => $data ) {

            if ( $page === 'first' ) {
                $pages[$page]['title'] = __('Erste');
            }

            if ( $page === 'prev' ) {
                $pages[$page]['title'] = __('Zurück');
            }

            if ( $page === 'next' ) {
                $pages[$page]['title'] = __('Weiter');
            }

            if ( $page === 'last' ) {
                $pages[$page]['title'] = __('Letzte');
            }

            $pages[$page]['url'] = $data['disabled'] ? 'javascript:void(0)' :
                Request::getCurrent(['page' => $data['page']]);
        }

        return $pages;
    }

    /**
     * @param null $data
     * @param string $finderKey
     * @return array
     */
    public function monthRange($data = null, $finderKey = 'created')
    {
        $data = $this->getChildrenDataList($data, 'events');


        $monthRange = [];

        foreach ( $data as $item ) {

            /** @var \App\Model\Entity\Article $item */

            if ( ! $date = $item->get($finderKey) ) {
                continue;
            }

            $dateMonth = $date->format('Y-m');

            if ( ! isset($monthRange[$dateMonth]) ) {
                $monthRange[$dateMonth] = collect(new \stdClass);
            }

            $monthRange[$dateMonth]->push($item);
        }

        ksort($monthRange);

        return $monthRange;
    }

    /**
     * @return string
     */
    public function getMonthRangeStart()
    {
        $startDateParam = Request::param('from');

        if ( $startDateParam ) {
            return Time::parse($startDateParam)->format('Y-m-d');
        }

        return Time::parse('now')->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getMonthRangeEnd()
    {
        $endDateParam = Request::param('to');

        if ( $endDateParam ) {
            return Time::parse($endDateParam)->format('Y-m-d');
        }

        $defaultFuture = config('Sx.app.calendar.range.default', 90);

        return Time::parse($this->getMonthRangeStart())
            ->modify("+{$defaultFuture}days")->format('Y-m-d');
    }

    /**
     * @param null $data
     * @param string $finderKey
     * @return array
     */
    public function paginateMonthRange($data = null, $finderKey = 'created')
    {
        $data = $this->getChildrenDataList($data, 'events');

        // Get start date
        $startDate = Time::parse($this->getMonthRangeStart())
            ->format('Ymd');

        // Get end date
        $endDate = Time::parse($this->getMonthRangeEnd())
            ->format('Ymd');

        $data = array_filter($data, function ($item) use ($startDate, $endDate, $finderKey) {

            /** @var \App\Model\Entity\Article $item */
            $eventDate = Time::parse($item->get($finderKey))->format('Ymd');

            return $eventDate >= $startDate && $eventDate <= $endDate;
        });

        return $this->monthRange($data, $finderKey);
    }

    /**
     * @param null $data
     * @param array $options
     * @return string|null
     */
    public function url($data = null, array $options = []): ?string
    {
        if ( empty($data) ) {
            $data = $this->rootArticle;
        }

        if ( is_object($data) && $data->has('menu') ) {
            $data = $data->get('menu');
        }

        if ( empty($data) ) {
            return null;
        }

        /** @var \App\Model\Entity\Menu $data */

        if ( empty($options['url']) ) {
            $options['url'] = $data->get('url');
        }
	
	if ( !empty($options['url']) ) {
            return $options['url'];
        }

        if ( empty($options['url']) ) {
            $options['url'] = $data->get('path');
        }

        if ( empty($options['url']) ) {
            return null;
        }

        // Remove https from url
        $url = preg_replace('/^https?:\/\//', '', $options['url']);

        return Request::getContextDomain(Request::scheme() . $url);
    }

    /**
     * @param $url
     * @param ...$args
     * @return string
     */
    public function link($url, ...$args): string
    {
        $url = preg_replace('/^https?:\/\/[^\/]+\//', '', $url);

        if ( count($args) ) {
            $url = sprintf($url, ...$args);
        }

        $url = str_join('/', Request::fullDomain(), $url);

        return Request::getContextDomain($url);
    }

    /**
     * @param null $data
     * @param array $options
     * @return string
     */
    public function openLink($data = null, array $options = []): string
    {
        if ( empty($data) ) {
            $data = $this->rootArticle;
        }

        $url = $this->url($data);

        if ( $data->get('menu.blank', false) ) {
            $options['target'] = '_blank';
        }

        if ( $data->get('extended_data_cfg.article.nofollow', false) ) {
            $options['rel'] = 'nofollow';
        }

        if ( ! isset($options['title']) ) {
            $options['title'] = $data->get('title', null);
        }

        if ( $data->get('menu.teaser', 0) > TEASER_WITHOUT_URL[0] ) {
            $url = str_lreplace('/', '#', $url);
        }

        $attributes = StringUtility::domAttr($options);

        return "<a href=\"{$url}\" {$attributes}>";
    }

    /**
     * @return string
     */
    public function closeLink(): string
    {
        return '</a>';
    }

    /**
     * Detect depth in items list
     *
     * @param $items
     * @param int $depth
     * @return int
     */
    function depth($items, $depth = 0): int
    {
        $depths = [$depth];

        if ( count($items) ) {
            $depths[] = $depth + 1;
        }

        foreach ( $items as $item ) {

            if ( ! count($item->children) ) {
                continue;
            }

            if ( count($item->children) ) {
                $depths[] = $this->depth($item->children, $depth + 1);
            }

        }

        asort($depths);

        return end($depths);
    }


}
