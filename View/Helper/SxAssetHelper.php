<?php

namespace App\View\Helper;

use App\Library\Facades\Request;
use Cake\View\Helper;
use Cake\View\View;
use MatthiasMullie\Minify;

class SxAssetHelper extends Helper
{
    /**
     * @var string $cacheFile
     */
    protected $cacheFile = 'asset_cache.php';

    /**
     * @var array $cacheData
     */
    protected $cacheData;

    /**
     * @var bool $applyRebuild
     */
    protected $applyRebuild = false;

    /**
     * @var array $styleStore
     */
    protected $styleStore = [];

    /**
     * @var array $styleStore
     */
    protected $styleLinkStore = [];

    /**
     * @var array $scriptStore
     */
    protected $scriptStore = [];

    /**
     * @var array $scriptLinkStore
     */
    protected $scriptLinkStore = [];

    /**
     * @var array $defaultCacheData
     */
    protected $defaultCacheData = [
      'timestamp' => null, 'debug' => null, 'style' => [], 'script' => [],
    ];

    /**
     * @inheritdoc
     */
    public function __construct(View $View, array $config = [])
    {
        $this->cacheData = $this->defaultCacheData;

        if ( $this->isMinifyOff() ) {

            // Clear cache
            $this->clearCache();
        }

        // Load cache file
        $this->loadCache();

        if ( $this->isMinifyOn() && $this->hasDebugDiff() ) {

            // Push new data to cache if debug mode changes
            $this->cacheData = array_merge($this->cacheData, [
              'timestamp' => time(), 'debug' => config('debug'),
            ]);

            // Apply rebuild in render
            $this->applyRebuild = true;
        }

        parent::__construct($View, $config);
    }

    public function isMinifyOn()
    {
        return ! config('debug') || config('minify');
    }

    public function isMinifyOff()
    {
        return config('debug') && ! config('minify');
    }

    public function hasDebugDiff()
    {
        return config('debug') !== $this->cacheData['debug'];
    }

    /**
     * Is a link y/n?
     *
     * @param $link
     * @return bool
     */
    public function isLink($link)
    {
        return preg_match('/^(https?\:\/\/|\/\/)/i', $link);
    }

    /**
     * Is a path y/n?
     *
     * @param $path
     * @return bool
     */
    public function isPath($path)
    {
        return preg_match('/^.*\.[a-z0-9]+$/i', $path);
    }

    /**
     * Remove https? from string
     *
     * @param $link
     * @return string
     * @throws \Exception
     */
    public function extractLink($link)
    {
        return Request::protocol(false) . ':' . preg_replace('/^https?:/', '', $link);
    }

    /**
     * Get absolute file path
     *
     * @param $path
     * @return string
     */
    public function extractLocalPath($path)
    {
        return APP . 'webroot' . DS . ltrim($path, '/');
    }

    /**
     * Get absolute file url
     *
     * @param $path
     * @return string
     * @throws \Exception
     */
    public function extractLocalDomain($path)
    {
        return Request::fullDomain(false) . DS . ltrim($path, '/');
    }

    /**
     * Remove tags from string
     *
     * @param $content
     * @return string
     */
    public function extractTag($content)
    {
        return preg_replace('/(<style[^>]*>|<\/style>|<script[^>]*>|<\/script>)/', '', $content);
    }

    /**
     * Load cache from file
     *
     * @return void
     */
    public function loadCache()
    {
        $cachePath = CACHE . ltrim($this->cacheFile, '/');

        if ( ! file_exists($cachePath) ) {

            $this->applyRebuild = true;

            return;
        }

        $cachedData = require $cachePath;
        $this->cacheData = array_merge($this->defaultCacheData, $cachedData);
    }

    /**
     * Clear cache
     *
     * @return void
     */
    public function clearCache()
    {
        $cachePath = CACHE . ltrim($this->cacheFile, '/');

        if ( ! file_exists($cachePath) ) {
            return;
        }

        unlink($cachePath);
    }

    /**
     * Write cache from file
     *
     * @return void
     */
    public function writeCache()
    {
        $cacheString = "<?php\nreturn " . var_export($this->cacheData, true) . ";";

        file_put_contents(CACHE . ltrim($this->cacheFile, '/'), $cacheString);
    }

    /**
     * Append styles to storage
     *
     * @param $styles
     * @param bool $forceCache
     * @param array $attrs
     */
    public function style($styles, $forceCache = false, $attrs = [])
    {
        foreach ( is_array($styles) ? $styles : [$styles] as $style ) {
            if ( ! $this->isLink($style) || $forceCache ) {
                $this->styleStore[] = [$style, (array) $attrs];
            } else {
                $this->styleLinkStore[] = [$style, (array) $attrs];
            }
        }
    }

    /**
     * Append scripts to storage
     *
     * @param $scripts
     * @param bool $forceCache
     * @param array $attrs
     */
    public function script($scripts, $forceCache = false, $attrs = [])
    {
        foreach ( is_array($scripts) ? $scripts : [$scripts] as $script ) {
            if ( ! $this->isLink($script) || $forceCache ) {
                $this->scriptStore[] = [$script, (array) $attrs];
            } else {
                $this->scriptLinkStore[] = [$script, (array) $attrs];
            }
        }
    }

    /**
     * Render styles and return all styles or minified style
     *
     * @param string $dest
     * @param array $attrs
     * @return string
     * @throws \Exception
     */
    public function renderStyles($attrs = [], $dest = 'dist/cache/style.css')
    {
        $render = ['<style>%s</style>', '<link rel="stylesheet" href="%s"[attrs]>'];

        $output = $this->renderRaw($this->styleLinkStore, ...$render);

        if ( $this->isMinifyOff() ) {
            return $this->renderRaw($this->styleStore, ...$render) . $output;
        }

        $forceRebuild = ! file_exists($this->extractLocalPath($dest)) ||
          $this->hasDebugDiff();

        if ( $this->applyRebuild || $forceRebuild || config('debug') ) {
            $this->renderCachedStyles($this->styleStore, $dest, $forceRebuild);
        }

        $hash = md5(implode('', $this->cacheData['style']));

        $cachePath = str_replace('.css', "-{$hash}.css",
          $this->extractLocalDomain($dest));


        $link = str_replace('[attrs]', $this->getAttrs($attrs), $render[1]);

        return sprintf($link, $cachePath) . $output;
    }

    /**
     * Render scripts and return all scripts or minified style
     *
     * @param string $dest
     * @param array $attrs
     * @return string
     * @throws \Exception
     */
    public function renderScripts($attrs = [], $dest = 'dist/cache/script.js'): string
    {
        $render = ['<script>%s</script>', '<script src="%s"[attrs]></script>'];

        $output = $this->renderRaw($this->scriptLinkStore, ...$render);

        if ( $this->isMinifyOff() ) {
            return $this->renderRaw($this->scriptStore, ...$render) . $output;
        }

        $forceRebuild = ! file_exists($this->extractLocalPath($dest)) ||
          $this->hasDebugDiff();

        if ( $this->applyRebuild || $forceRebuild || config('debug') ) {
            $this->renderCachedScripts($this->scriptStore, $dest, $forceRebuild);
        }

        $hash = md5(implode('', $this->cacheData['script']));

        $cachePath = str_replace('.js', "-{$hash}.js",
          $this->extractLocalDomain($dest));

        $link = str_replace('[attrs]', $this->getAttrs($attrs), $render[1]);

        return sprintf($link, $cachePath) . $output;
    }

    /**
     * Render all data in given format
     *
     * @param $items
     * @param $plain
     * @param $link
     * @return string
     * @throws \Exception
     */
    public function renderRaw($items, $plain, $link): string
    {
        $keys = [];

        $items = array_map(function ($item) use ($plain, $link, &$keys) {

            if ( in_array($item[0], $keys) ) {
                return null;
            }

            $finalLink = str_replace('[attrs]', $this->getAttrs($item[1]), $link);

            array_push($keys, $item[0]);

            if ( $this->isLink($item[0]) ) {
                return sprintf($finalLink, $this->extractLink($item[0]));
            }

            if ( $this->isPath($item[0]) ) {
                return sprintf($finalLink, $this->extractLocalDomain($item[0]));
            }

            return sprintf($plain, $this->extractTag($item[0]));

        }, $items);

        return implode("\n", array_filter($items));
    }

    /**
     * Convert attrs array to string
     *
     * @param array $attrs
     * @return string
     */
    public function getAttrs(array $attrs): string
    {
        if ( empty($attrs) ) {
            return '';
        }

        $tags = '';

        $isAssoc = is_string(array_keys($attrs)[0]);

        foreach ( $attrs as $key => $value ) {
            if ( ! $isAssoc ) {
                $tags .= " {$value}";
            } else {
                $tags .= " {$key}=\"{$value}\"";
            }
        }

        return $tags;
    }

    /**
     * Extract hashes from filelist
     *
     * @param $items
     * @return array
     */
    public function getHashes($items): array
    {
        return array_map(function ($item) {

            if ( $this->isLink($item[0]) ) {
                return md5(strlen($item[0]));
            }

            if ( ! file_exists($item[0]) ) {
                return md5(strlen($item[0]));
            }

            return md5(filesize($item[0]));

        }, $items);
    }

    /**
     * Save styles in cached file
     *
     * @param $items
     * @param $dest
     * @param bool $forceRebuild
     * @throws \Exception
     */
    public function renderCachedStyles($items, $dest, $forceRebuild = false)
    {
        // Get hashes for all items
        $hashes = $this->getHashes($items);

        if ( ! array_diff($hashes, $this->cacheData['style']) && ! $forceRebuild ) {
            return; // Abort if no changes are made
        }

        // Update cache md5 hashes
        $this->cacheData['style'] = $hashes;
        $this->writeCache();

        $items = array_map(function ($item) {

            if ( $this->isLink($item[0]) ) {
                return file_get_contents($item[0]);
            }

            if ( $this->isPath($item[0]) ) {
                return $this->extractLocalPath($item[0]);
            }

            return $this->extractTag($item[0]);

        }, $items);

        $minifier = new Minify\CSS($items);

        if ( ! file_exists($dest) ) {
            $this->extractLocalPath($dest);
            mkdir(dirname($this->extractLocalPath($dest)), 666, true);
        }

        $minifier->minify($this->extractLocalPath($dest));
    }

    /**
     * Save scripts in cached file
     *
     * @param $items
     * @param $dest
     * @param bool $forceRebuild
     */
    public function renderCachedScripts($items, $dest, $forceRebuild = false)
    {
        // Get hashes for all items
        $hashes = $this->getHashes($items);

        if ( ! array_diff($hashes, $this->cacheData['script']) && ! $forceRebuild ) {
            return; // Abort if no changes are made
        }

        // Update cache md5 hashes
        $this->cacheData['script'] = $hashes;
        $this->writeCache();

        $items = array_map(function ($item) {

            if ( $this->isLink($item[0]) ) {
                return file_get_contents($item[0]);
            }

            if ( $this->isPath($item[0]) ) {
                return $this->extractLocalPath($item[0]);
            }

            return $this->extractTag($item[0]);

        }, $items);

        $minifier = new Minify\JS($items);

        $minifier->minify($this->extractLocalPath($dest));
    }

}
