<?php

namespace App\View\Helper;

use App\Library\RouterService;
use App\Model\Entity\Article;
use App\Model\Entity\Attachment;
use App\Model\Entity\Element;
use App\Model\Entity\Menu;
use Cake\View\Helper;

class SxFeHelper extends Helper
{
    public $helpers = [
        'Html',
        'Number',
        'Media',
    ];

    function rewriteUrl2($url, $newslettercampaign, $base_href)
    {
        $url = $base_href . '/newslettercampaigns/rd/' . $newslettercampaign->id . '?url=' . urlencode($url);

        return $url;
    }

    function rewriteUrl($content, $newslettercampaign, $base_href)
    {

        if ( empty($newslettercampaign) ) {
            return $content;
        }

        return preg_replace(',<a([^>]+)href="([^>"\s]+)",ie', '"<a\1href=\"" . $this->rewriteUrl2("\2",$newslettercampaign,$base_href) . "\""', $content);
    }

    //DONE SX3
    function link($item, $options = [])
    {

        $url = $this->getUrl($item, $options);
        $title = $this->getTitle($item, $options);
        $menu = $this->getMenuEntity($item);
        $article = $this->getArticleEntity($item);

        if ( empty($options['escape']) ) {
            $options['escape'] = false;
        }

        if ( ! empty($menu->blank) ) {
            $options['target'] = '_blank';
        }

        if ( $article->get('extended_data_cfg.article.nofollow', false) ) {
            $options['rel'] = 'nofollow';
        }

        $options['title'] = empty($options['title']) ? (empty($article->headline) ? $article->description ?? null : $item->headline) : h($options['title']);

        return $this->Html->link($title, $url, $options);
    }

    function getUrl($item, $options = [])
    {
        $item = $this->getMenuEntity($item);


        if ( empty($item) ) {
            return null;
        }

        $url = $options['url'] ?? $item->url;


        if ( ! empty($url) ) {
            $parsed = parse_url($url);
            if ( empty($parsed['scheme']) ) {
                $url = request()->getContextDomain('http://' . ltrim($url, '/'));
            }

            return $url;
        }

        if ( ! empty($item->path) ) {
            $parsed = parse_url($item->path);
            if ( empty($parsed['scheme']) ) {
                $item->path = request()->getContextDomain(request()->scheme() . $item->path);
            }

            return $item->path;
        }

        return null;
    }


    //DONE SX3

    function getMenuEntity($item)
    {
        if ( $item instanceof Menu ) {
            return $item;
        }

        if ( $item->menu instanceof Menu ) {
            return $item->menu;
        }
        if ( $item instanceof Article ) {
            if ( ! empty($item->menus) ) {
                foreach ( $item->menus as $menu ) {
                    if ( $menu->lft >= menu()->getDomainRecord('lft') && $menu->rght <= menu()->getDomainRecord('rght') ) {
                        return $menu;
                    }
                }
            }
        }

        return null;
    }

    //DONE SX3

    function getTitle($item, $options = [])
    {
        return empty($options['linktext']) ? (empty($item->title) ? $this->getMenuEntity($item)->title ?? null : $item->title) : $options['linktext'];
    }

    //DONE SX3

    function getArticleEntity($item)
    {
        if ( $item instanceof Article || $item instanceof Element ) {
            return $item;
        }
        if ( $item instanceof Menu ) {
            if ( ! empty($item->article) ) {
                return $item->article;
            }
        }

        return null;
    }

    //DONE SX3
    function getOpenTagA($item, $options = [])
    {
        $url = $this->getUrl($item, $options);
        $menu = $this->getMenuEntity($item);
        $article = $this->getArticleEntity($item);

        if ( empty($options['escape']) ) {
            $options['escape'] = false;
        }

        if ( ! empty($menu->blank) ) {
            $options['target'] = '_blank';
        }

        if ( $article->get('extended_data_cfg.article.nofollow', false) ) {
            $options['rel'] = 'nofollow';
        }

        if ( ! isset($options['title']) ) {
            $options['title'] = $article->get('title', null);
        }

        $attributesStr = '';
        foreach ( $options as $k => $attribute ) {
            if ( ! empty($attribute) ) {
                $attributesStr .= ' ' . $k . '="' . $attribute . '"';
            }
        }

        return '<a href="' . $url . '"' . $attributesStr . '>';
    }

    //DONE SX3
    function getImg($data = null, $options = [])
    {
        $imgs = $this->getAllAttachments($data, $options);

        return $imgs[0] ?? null;
    }


    //DONE SX3
    function getAllAttachments($data = null, $options = [])
    {
        $pick = 'all';
        $type = null;

        extract($options);

        if ( empty($data) ) {
            return [];
        }

        $pick = (array) $pick;

        foreach ( $pick as $k => $v ) {
            $pick[$k] = mb_strtolower($v);
        }

        $attachments = [];

        if ( $data instanceof Attachment ) {
            $attachments[] = $data;
        } else {
            if ( ! empty($data->attachments) ) {
                $attachments = $data->attachments;
            } else {
                if ( ! empty($data->article) && ! empty($data->article->attachments) ) {
                    $attachments = $data->article->attachments;
                }
            }
        }

        if ( empty($attachments) ) {
            return [];
        }

        $ret = $sortedRet = [];

        foreach ( $attachments as $attachment ) {
            if ( ! empty($attachment->_joinData) ) {
                if ( in_array(strtolower($attachment->_joinData->type), $pick) || in_array('all', $pick) ) {
                    if ( $type ) {
                        if ( $attachment->type == $type ) {
                            $ret[] = $attachment;
                        }
                    } else {
                        $ret[] = $attachment;
                    }
                }
            }
        }

        //sortieren nach der Reihenfolge von $pick und ggf all hinten dran
        foreach ( $pick as $s ) {
            foreach ( $ret as $key => $r ) {
                if ( strtolower($r->_joinData->type) == strtolower($s) ) {
                    $sortedRet[] = $r;
                    unset($ret[$key]);
                }
            }
        }

        return array_merge($sortedRet, $ret);
    }

    //DONE SX3
    function getImgTitle($img)
    {
        return ! empty($img->title) ? strip_tags($img->title) : null;
    }

    //DONE SX3
    function getImgAlt($img)
    {
        return ! empty($img->alternative) ? strip_tags($img->alternative) : null;
    }

    //DONE SX3
    function getImgName($img)
    {
        return ! empty($img->name) ? strip_tags($img->name) : null;
    }

    //TODO SX4 height
    function getMediaUrl($img, $version, $width = null, $height = null)
    {
        if ( ! empty($img) ) {

            $pathPiece = '/media/';

            if ( ! empty($width) ) {
                $pathPiece .= 'w-' . $width . '/';
            }

            if ( ! empty($height) ) {
                //$pathPiece .= 'h-' . $height . '';
            }

            return request()->scheme() . request()->domain(true) . $pathPiece . $img->dirname . '/' . $version . '/' . $img->basename;
        }

        return null;
    }

    function getMediaUrlFromPath($img, $version, $width = null, $height = null)
    {
        if ( ! empty($img) ) {

            $pathPiece = '/media/';

            if ( ! empty($width) ) {
                $pathPiece .= 'w-' . $width . '/';
            }

            if ( ! empty($height) ) {
                //$pathPiece .= 'h-' . $height . '';
            }

            // return request()->scheme() . request()->domain(true) . $pathPiece . $img->dirname . '/' . $version . '/' . $img->basename;
            return $pathPiece . $img->dirname . '/' . $version . '/' . $img->basename;
        }

        return null;
    }

    //DONE SX3
    function getList($data = null, $key = 4)
    {
        if ( $data === null ) {
            return [];
        }

        $ret = [];

        foreach ( $data as $item ) {
            if ( in_array($key, menu()->getMenuVisibilitys($item->teaser)) ) {
                $ret[] = $item;
            }
        }

        return $ret;
    }

    //DONE SX3
    function getRequest($key = null)
    {
        return $this->request->getData($key);
    }

    //DONE SX3
    function getCloseTagA()
    {
        return '</a>';
    }

    function getBemClass($bemBlock, $bemBlockMod = '')
    {
        if ( ! empty($bemBlockMod) ) {
            $bemBlock = $bemBlock . ' ' . $bemBlock . '--' . $bemBlockMod;
        }

        return $bemBlock;
    }

    function getHtmlListFromContent($content = '', $options = [])
    {
        if ( empty($content) ) {
            return '';
        }

        return $this->arrayToHtmlList(explode("\n", $content), $options);
    }

    function arrayToHtmlList($list = [], $options = [])
    {
        if ( empty($list) ) {
            return [];
        }

        $lis = array_map(function ($value) {
            return "<li>$value</li>";
        }, $list);

        return $this->Html->tag('ul', implode('', $lis), $options);
    }


}
