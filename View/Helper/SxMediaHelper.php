<?php

namespace App\View\Helper;

use App\Library\AuthManager;
use App\Model\Entity\Attached;
use App\Model\Entity\Attachment;
use Cake\Datasource\ModelAwareTrait;
use Cake\ORM\Entity;
use Cake\View\Helper;

/**
 * @version 4.0.0
 */
 
class SxMediaHelper extends Helper
{
    use ModelAwareTrait;

    protected $_forceAttachment;

    protected $Attachments;

    public function initialize(array $config): void
    {
        if ( ! config('Media.debug.image') ) {
            return;
        }

        $this->loadModel('Attachments');

        AuthManager::pause();

        $attachment = $this->Attachments->find()->where([
            'id' => config('Media.debug.image')
        ])->first();


        AuthManager::resume();

        $this->_forceAttachment = $attachment;
    }

    public function _getDebugAttachment($attached)
    {
        if ( $this->_forceAttachment ) {
            $attached->set('attachment', $this->_forceAttachment);
        }

        if ( ! $attached->isEmpty('title') ) {
            $attached->attachment->title = $attached->get('title');
        }

        if ( ! $attached->isEmpty('description') ) {
            $attached->attachment->description = $attached->get('description');
        }

        if ( ! $attached->isEmpty('alternative') ) {
            $attached->attachment->alternative = $attached->get('alternative');
        }

        return $attached;
    }

    public function getAttachments($data = null, $options = [])
    {
        if ( empty($data) ) {
            return [];
        }

        $options = array_merge([
            'pick' => ['all'], 'type' => null
        ], $options);

        $formats = [];

        foreach ( (array) $options['pick'] as $value ) {
            $formats[] = strtolower($value);
        }

        if ( ! is_a($data, Entity::class) ) {
            throw new \Exception('Media.getAttchments data is invalid');
        }

        /** @var $data \Cake\ORM\Entity */

        $attachments = [];

        if ( $data->has('article.attached') ) {
            $attachments = $data->get('article.attached');
        }

        if ( $data->has('attached') ) {
            $attachments = $data->get('attached');
        }

        if ( is_a($data, Attached::class) ) {
            $attachments = [$data];
        }

        if ( empty($attachments) ) {
            return [];
        }

        $result = [];

        foreach ( $attachments as $attached ) {

            $included = in_array(strtolower($attached->type),
                $formats);

            if ( ! $included && ! in_array('all', $formats) ) {
                continue;
            }

            if ( $options['type'] && $attached->type !== $options['type'] ) {
                $attached = null;
            }

            $result[$attached->id] = $attached;
        }

        $sortedResult = [];

        foreach ( $formats as $format ) {
            foreach ( $result as $attached ) {

                $excluded = strtolower($attached->type) !==
                    $format && $format !== 'all';

                if ( $excluded ) {
                    continue;
                }

                $sortedResult[$attached->id] = $attached;
            }
        }

        if ( $this->_forceAttachment && empty($sortedResult) ) {
            return array_map([$this, '_getDebugAttachment'], $result);
        }

        return array_map([$this, '_getDebugAttachment'], $sortedResult);
    }

    public function getAttachment($data, $options = [])
    {
        $attachments = $this->getAttachments($data, $options);

        return array_first($attachments);
    }

    public function getRealAttachment($data, $options = [])
    {
        if ( is_a($data, Attachment::class) ) {
            return $data;
        }

        return data_get($this->getAttachment($data, $options), 'attachment');
    }

    public function getUrl($url)
    {
        $host = request()->fullDomain();

        if ( config('Media.debug.url') ) {
            $host = config('Media.debug.url');

            if ( config('Media.debug.is_sx2') ) {
                $url = explode('/', $url);
                $url = $url[0] . '/filter/' . $url[5] . '/account/' . $url[3] . '/' . $url[4] . '/' . $url[6];
            }
        }

        $url = preg_replace('/^https?:\/\/[^\/]+\//', '', $url);

        return $host . '/' . ltrim($url, '/');
    }

    public function getVersionUrl($attachment, $version, $width = null, $height = null)
    {
        if ( is_a($attachment, Attached::class) ) {
            $attachment = $attachment->get('attachment');
        }

        if ( empty($attachment) ) {
            return null;
        }

        if ( empty($attachment->preview) ) {
            return $this->getUrl($attachment->getUrl());
        }

        $base = ['media'];

        if ( ! empty($width) ) {
            $base[] = "w-{$width}";
        }

        if ( ! empty($height) ) {
            // $base[] = "h-{$height}";
        }

        $base = array_merge($base, [
            $attachment->dirname, $version, $attachment->basename
        ]);

        return $this->getUrl(str_join('/', ...$base));
    }

    public function getMediaQuery($format = [])
    {
        if ( ! is_array($format) ) {
            $format = explode(' ', (string) $format);
        }

        $mediaQuery = [
          'src'    => null,
          'src-sm' => null,
          'src-md' => null,
          'src-lg' => null,
          'src-xl' => null
        ];

        foreach ( $format as $media ) {

            $modes = explode('@', $media);

            if ( ! isset($modes[1]) ) {
                $modes[1] = 'src';
            } else {
                $modes[1] = 'src-' . $modes[1];
            }

            $mediaQuery[$modes[1]] = $modes[0];
        }

        foreach ( $mediaQuery as $key => $media ) {

            /* @var string $previousMedia */

            if ( empty($media) ) {
                $mediaQuery[$key] = $previousMedia;
            }

            $previousMedia = $mediaQuery[$key];
        }

        return $mediaQuery;
    }

}
