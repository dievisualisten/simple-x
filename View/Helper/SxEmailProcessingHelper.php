<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Pelago;

class SxEmailProcessingHelper extends Helper

{
    /**
     * Process Email HTML content after rendering of the email
     *
     * @param string $layoutFile The layout file that was rendered.
     * @return void
     */
    public function afterLayout($layoutFile)
    {
        $content = $this->_View->Blocks->get('content');
        $emogrifier = new Pelago\Emogrifier();
        $emogrifier->setHtml($content);
        $content = $emogrifier->emogrify();
        $this->_View->Blocks->set('content', $content);
    }
}
