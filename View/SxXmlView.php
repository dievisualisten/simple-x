<?php

namespace App\View;

use Cake\View\XmlView;

/**
 * A view class that is used for XML responses.
 */
class SxXmlView extends XmlView
{
    public function render($view = null, $layout = null): string
    {
        $this->getResponse()->withType('application/xml');

        return parent::render($view, $layout);
    }
}
