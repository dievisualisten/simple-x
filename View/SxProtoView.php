<?php

namespace App\View;

/**
 * Class SxProtoView
 *
 * @package Proto\View
 * @method element(string $view, array $options = [])
 */
class SxProtoView {

    /**
     * @var \App\View\Helper\FeHelper $Fe !! YOU WILL DIE
     */
    public $Fe;

    /**
     * @var \App\View\Helper\AssetHelper $Asset
     */
    public $Asset;

    /**
     * @var \App\View\Helper\MediaHelper $Media
     */
    public $Media;

    /**
     * @var \App\View\Helper\ArticleHelper $Article
     */
    public $Article;

    /**
     * @var \App\View\Helper\EmailHelper $Email
     */
    public $Email;

    /**
     * @var \App\View\Helper\TreeHelper $Tree
     */
    public $Tree;

}