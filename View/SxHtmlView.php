<?php

namespace App\View;

use Cake\View\View as HtmlView;

/**
 * A view class that is used for HTML responses.
 */
class SxHtmlView extends HtmlView
{
    public function render($view = null, $layout = null): string
    {
        $this->getResponse()->withType('text/html');

        return parent::render($view, $layout);
    }
}
