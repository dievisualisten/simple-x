<?php

namespace App\View;

use Cake\View\JsonView;

/**
 * A view class that is used for JSON responses.
 */
class SxJsonView extends JsonView
{
    public function render($view = null, $layout = null): string
    {
        $this->getResponse()->withType('application/json');

        return parent::render($view, $layout);
    }
}
