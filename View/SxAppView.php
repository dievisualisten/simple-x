<?php

namespace App\View;

use Cake\View\View;

/**
 * Class SxAppView
 *
 * @package App\View
 */
class SxAppView extends View
{
    public function initialize(): void
    {
        $this->loadHelper('WyriHaximus/MinifyHtml.MinifyHtml');
    }

    public function render($view = null, $layout = null): string
    {
        $this->getResponse()->withType('text/html');

        return parent::render($view, $layout);
    }

}
