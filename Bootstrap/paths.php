<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */

/**
 * Use the DS to separate the directories in other defines
 */
if ( ! defined('DS') ) {
    define('DS', DIRECTORY_SEPARATOR);
}

/**
 * These defines should only be edited if you have cake installed in
 * a directory layout other than the way it is distributed.
 * When using custom settings be sure to use the DS and do not add a trailing DS.
 */

/**
 * The full path to the directory which holds "src", WITHOUT a trailing DS.
 */
if ( ! defined('ROOT') ) {
    define('ROOT', rtrim(dirname(dirname(__DIR__)), DIRECTORY_SEPARATOR));
}

/**
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
if ( ! defined('APP_DIR') ) {
    define('APP_DIR', 'src');
}

/**
 * Path to the application's directory.
 */
if ( ! defined('APP') ) {
    define('APP', ROOT . DS . APP_DIR . DS);
}


/**
 * The actual directory name for the sx application directory. Normally
 * named 'src'.
 */
if ( ! defined('SX_DIR') ) {
    define('SX_DIR', 'src-sx');
}

/**
 * Path to the sx application's directory.
 */
if ( ! defined('SX') ) {
    define('SX', ROOT . DS . SX_DIR . DS);
}


/**
 * Path to the config directory.
 */
if ( ! defined('CONFIG') ) {
    define('CONFIG', ROOT . DS . 'src' . DS . 'Config' . DS);
}

/**
 * File path to the webroot directory.
 */
if ( ! defined('WWW_ROOT') ) {
    define('WWW_ROOT', ROOT . DS . 'src' . DS . 'webroot' . DS);
}

/**
 * Path to the tests directory.
 */
if ( ! defined('TESTS') ) {
    define('TESTS', SX . DS . 'tests' . DS);
}

/**
 * Path to the temporary files directory.
 */
if ( ! defined('TMP') ) {
    define('TMP', APP . DS . 'tmp' . DS);
}

/**
 * Path to the logs directory.
 */
if ( ! defined('LOGS') ) {
    define('LOGS', APP . DS . 'logs' . DS);
}

/**
 * Path to the cache files directory. It can be shared between hosts in a multi-server setup.
 */
if ( ! defined('CACHE') ) {
    define('CACHE', TMP . 'cache' . DS);
}

/**
 * The absolute path to the "cake" directory, WITHOUT a trailing DS.
 *
 * CakePHP should always be installed with composer, so look there.
 */
if ( ! defined('CAKE_CORE_INCLUDE_PATH') ) {
    define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');
}

/**
 * Path to the cake directory.
 */
if ( ! defined('CORE_PATH') ) {
    define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
}

if ( ! defined('CAKE') ) {
    define('CAKE', CORE_PATH . 'src' . DS);
}

if ( ! defined('MEDIA') ) {
    define('MEDIA', WWW_ROOT . 'media' . DS);
}

if ( ! defined('MEDIA_URL') ) {
    define('MEDIA_URL',  '/media/' );
}




