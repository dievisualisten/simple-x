<?php

use App\Library\Facades\Language;
use App\Library\Facades\Logger;
use App\Library\Facades\Menu;
use App\Library\Facades\Mailer;
use App\Library\Facades\Request;
use App\Library\Facades\Search;
use Cake\Core\Configure;
use App\Library\ServiceManager;
use App\Utility\ArrayUtility;
use App\Utility\CollectionUtility;

if ( ! function_exists('dc') ) {
    /**
     * Die and dump with all methods.
     */
    function dc()
    {
        $arguments = [];

        foreach ( func_get_args() as $argument ) {
            array_push($arguments, $argument, get_class_methods($argument));
        }

        dump(...$arguments);
    }
}

if ( ! function_exists('ddc') ) {
    /**
     * Die and dump with all methods.
     */
    function ddc()
    {
        $arguments = [];

        foreach ( func_get_args() as $argument ) {
            array_push($arguments, $argument, get_class_methods($argument));
        }

        dump(...$arguments);
        die();
    }
}

if ( ! function_exists('realempty') ) {
    /**
     * Die and dump with all methods.
     *
     * @param mixed $value
     * @return mixed
     */
    function realempty($value)
    {
        if ( is_string($value) ) {
            $value = trim($value);
        }


        if ( $value === '0' || $value === 0 ) {
            return false;
        }


        if ( is_array($value) ) {
            $value = array_filter($value);
        }

        return empty($value);
    }
}


if ( ! function_exists('config') ) {
    /**
     * Get value from config.
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function config($key = null, $default = null)
    {
        return Configure::read($key, $default);
    }
}

if ( ! function_exists('service') ) {
    /**
     * Get service instance.
     *
     * @param string $key
     * @return mixed
     */
    function service($key)
    {
        return call_user_func([ServiceManager::class, $key]);
    }
}

if ( ! function_exists('request') ) {
    /**
     * Get request instance.
     *
     * @return \App\Library\Services\RequestService
     * @throws \Exception
     */
    function request()
    {
        return Request::getInstance();
    }
}

if ( ! function_exists('language') ) {
    /**
     * Get language instance.
     *
     * @return \App\Library\Services\LanguageService
     * @throws \Exception
     */
    function language()
    {
        return Language::getInstance();
    }
}

if ( ! function_exists('menu') ) {
    /**
     * Get menu instance.
     *
     * @return \App\Library\Services\MenuService
     * @throws \Exception
     */
    function menu()
    {
        return Menu::getInstance();
    }
}

if ( ! function_exists('logger') ) {
    /**
     * Get logger instance.
     *
     * @return \App\Library\Services\LoggerService
     * @throws \Exception
     */
    function logger()
    {
        return Logger::getInstance();
    }
}

if ( ! function_exists('mailer') ) {
    /**
     * Get mailer instance.
     *
     * @return \App\Library\Services\MailerService
     * @throws \Exception
     */
    function mailer()
    {
        return Mailer::getInstance();
    }
}


if ( ! function_exists('search') ) {
    /**
     * Get logger instance.
     *
     * @return \App\Library\Services\SearchService
     * @throws \Exception
     */
    function search()
    {
        return Search::getInstance();
    }
}

if ( ! function_exists('collect') ) {
    /**
     * Create collection from data
     *
     * @param array $data
     * @return \App\Utility\CollectionUtility
     */
    function collect($data = [])
    {
        return new CollectionUtility($data);
    }
}

if ( ! function_exists('domain') ) {
    /**
     * @param null $domain
     * @return string
     */
    function domain($domain = null)
    {
        if ( $domain == null ) {
            $domain = (@PHPUNIT_MODE ? config('liveDomain') : config('localDomain'));
        }

        return config('Sx.app.baseprotocol') . $domain;
    }
}

if ( ! function_exists('str_join') ) {

    /**
     * @param string $glue
     * @return string
     */
    function str_join($glue)
    {
        $args = func_get_args();
        $args = array_splice($args, 1);

        foreach ( $args as $key => $arg ) {
            $args[$key] = trim($arg, $glue);
        }

        return implode($glue, array_filter($args));
    }

}

if ( ! function_exists('array_map_recursive') ) {
    /**
     * @param $array
     * @param $key
     * @param $callback
     * @param $pre
     * @return array|mixed
     */
    function array_map_recursive($array, $key, $callback)
    {
        if ( isset($array[$key]) ) {
            $array = call_user_func($callback, $array);
        }

        if ( is_array($array) && isset($array[$key]) === false ) {
            $array = array_map(function ($value) use ($key, $callback) {
                return array_map_recursive($value, $key, $callback);
            }, $array);
        }

        if ( is_array($array) && isset($array[$key]) === true ) {
            $array[$key] = array_map(function ($value) use ($key, $callback) {
                return array_map_recursive($value, $key, $callback);
            }, $array[$key]);
        }

        if ( is_object($array) && isset($array->$key) === true ) {
            $array->$key = array_map(function ($value) use ($key, $callback) {
                return array_map_recursive($value, $key, $callback);
            }, $array->$key);
        }

        return $array;
    }
}

if ( ! function_exists('array_map_recursive_keys') ) {
    /**
     * @param $array
     * @param $key
     * @param $callback
     * @param $index
     * @param $path
     * @return array|mixed
     */
    function array_map_recursive_keys($array, $key, $callback, $index = null, $path = [])
    {
        if ( isset($array[$key]) ) {
            $array = call_user_func($callback, $array, $index, $path);
        }

        if ( is_array($array) && isset($array[$key]) === false ) {
            $array = array_map(function ($value, $index) use ($key, $callback, $path) {
                return array_map_recursive_keys($value, $key, $callback, $index, array_merge($path, [$index]));
            }, $array, array_keys($array));
        }

        if ( is_array($array) && isset($array[$key]) === true ) {
            $array[$key] = array_map(function ($value, $index) use ($key, $callback, $path) {
                return array_map_recursive_keys($value, $key, $callback, $index, array_merge($path, [$index]));
            }, $array[$key], array_keys($array[$key]));
        }

        if ( is_object($array) && isset($array->$key) === true ) {
            $array->$key = array_map(function ($value, $index) use ($key, $callback, $path) {
                return array_map_recursive_keys($value, $key, $callback, $index, array_merge($path, [$index]));
            }, $array->$key, array_keys($array->$key));
        }

        return $array;
    }
}

if ( ! function_exists('data_get') ) {
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $default
     * @return mixed
     */
    function data_get($target, $key, $default = null)
    {
        if ( is_null($key) ) {
            return $target;
        }

        $key = is_array($key) ? $key : explode('.', $key);

        foreach ( $key as $segment ) {

            if ( is_array($target) ) {

                if ( ! array_key_exists($segment, $target) ) {
                    return value($default);
                }

                $target = $target[$segment];

            } elseif ( $target instanceof ArrayAccess ) {

                if ( ! isset($target[$segment]) ) {
                    return value($default);
                }

                $target = $target[$segment];

            } elseif ( is_object($target) ) {

                if ( ! isset($target->{$segment}) ) {
                    return value($default);
                }

                $target = $target->{$segment};

            } else {
                return value($default);
            }

        }

        return $target;
    }
}

if ( ! function_exists('data_fill') ) {
    /**
     * Fill in data where it's missing.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     * @return mixed
     */
    function data_fill(&$target, $key, $value)
    {
        return data_set($target, $key, $value, false);
    }
}
if ( ! function_exists('data_get') ) {
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param mixed $target
     * @param string|array|int $key
     * @param mixed $default
     * @return mixed
     */
    function data_get($target, $key, $default = null)
    {
        if ( is_null($key) ) {
            return $target;
        }

        $key = is_array($key) ? $key : explode('.', $key);

        while ( ! is_null($segment = array_shift($key)) ) {

            if ( $segment === '*' ) {

                if ( $target instanceof CollectionUtility ) {
                    $target = $target->all();
                } elseif ( ! is_array($target) ) {
                    return value($default);
                }

                $result = [];

                foreach ( $target as $item ) {
                    $result[] = data_get($item, $key);
                }

                return in_array('*', $key) ? ArrayUtility::collapse($result) : $result;
            }

            if ( ArrayUtility::accessible($target) && ArrayUtility::exists($target, $segment) ) {
                $target = $target[$segment];
            } elseif ( is_object($target) && isset($target->{$segment}) ) {
                $target = $target->{$segment};
            } else {
                return value($default);
            }

        }

        return $target;
    }
}
if ( ! function_exists('data_set') ) {
    /**
     * Set an item on an array or object using dot notation.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     * @param bool $overwrite
     * @return mixed
     */
    function data_set(&$target, $key, $value, $overwrite = true)
    {
        $segments = is_array($key) ? $key : explode('.', $key);

        if ( ($segment = array_shift($segments)) === '*' ) {

            if ( ! ArrayUtility::accessible($target) ) {
                $target = [];
            }

            if ( $segments ) {

                foreach ( $target as &$inner ) {
                    data_set($inner, $segments, $value, $overwrite);
                }

            } elseif ( $overwrite ) {

                foreach ( $target as &$inner ) {
                    $inner = $value;
                }

            }

        } elseif ( ArrayUtility::accessible($target) ) {

            if ( $segments ) {

                if ( ! ArrayUtility::exists($target, $segment) ) {
                    $target[$segment] = [];
                }

                data_set($target[$segment], $segments, $value, $overwrite);

            } elseif ( $overwrite || ! ArrayUtility::exists($target, $segment) ) {
                $target[$segment] = $value;
            }

        } elseif ( is_object($target) ) {

            if ( $segments ) {

                if ( ! isset($target->{$segment}) ) {
                    $target->{$segment} = [];
                }

                data_set($target->{$segment}, $segments, $value, $overwrite);

            } elseif ( $overwrite || ! isset($target->{$segment}) ) {
                $target->{$segment} = $value;
            }

        } else {

            $target = [];

            if ( $segments ) {
                data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ( $overwrite ) {
                $target[$segment] = $value;
            }

        }

        return $target;
    }
}


if ( ! function_exists('value') ) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if ( ! function_exists('array_add') ) {
    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     *
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return array
     */
    function array_add($array, $key, $value)
    {
        return ArrayUtility::add($array, $key, $value);
    }
}

if ( ! function_exists('array_collapse') ) {
    /**
     * Collapse an array of arrays into a single array.
     *
     * @param array $array
     * @return array
     */
    function array_collapse($array)
    {
        return ArrayUtility::collapse($array);
    }
}

if ( ! function_exists('array_divide') ) {
    /**
     * Divide an array into two arrays. One with keys and the other with values.
     *
     * @param array $array
     * @return array
     */
    function array_divide($array)
    {
        return ArrayUtility::divide($array);
    }
}

if ( ! function_exists('array_dot') ) {
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param array $array
     * @param string $prepend
     * @return array
     */
    function array_dot($array, $prepend = '')
    {
        return ArrayUtility::dot($array, $prepend);
    }
}

if ( ! function_exists('array_except') ) {
    /**
     * Get all of the given array except for a specified array of keys.
     *
     * @param array $array
     * @param array|string $keys
     * @return array
     */
    function array_except($array, $keys)
    {
        return ArrayUtility::except($array, $keys);
    }
}

if ( ! function_exists('array_first') ) {
    /**
     * Return the first element in an array passing a given truth test.
     *
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     * @return mixed
     */
    function array_first($array, callable $callback = null, $default = null)
    {
        return ArrayUtility::first($array, $callback, $default);
    }
}

if ( ! function_exists('array_flatten') ) {
    /**
     * Flatten a multi-dimensional array into a single level.
     *
     * @param array $array
     * @param int $depth
     * @return array
     */
    function array_flatten($array, $depth = INF)
    {
        return ArrayUtility::flatten($array, $depth);
    }
}

if ( ! function_exists('array_forget') ) {
    /**
     * Remove one or many array items from a given array using "dot" notation.
     *
     * @param array $array
     * @param array|string $keys
     * @return void
     */
    function array_forget(&$array, $keys)
    {
        return ArrayUtility::forget($array, $keys);
    }
}

if ( ! function_exists('array_get') ) {
    /**
     * Get an item from an array using "dot" notation.
     *
     * @param \ArrayAccess|array $array
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function array_get($array, $key, $default = null)
    {
        return ArrayUtility::get($array, $key, $default);
    }
}

if ( ! function_exists('array_set') ) {
    /**
     * Set an item from an array using "dot" notation.
     *
     * @param \ArrayAccess|array $array
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    function array_set($array, $key, $value)
    {
        return ArrayUtility::set($array, $key, $value);
    }
}


if ( ! function_exists('array_has') ) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param \ArrayAccess|array $array
     * @param string|array $keys
     * @return bool
     */
    function array_has($array, $keys)
    {
        return ArrayUtility::has($array, $keys);
    }
}

if ( ! function_exists('array_last') ) {
    /**
     * Return the last element in an array passing a given truth test.
     *
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     * @return mixed
     */
    function array_last($array, callable $callback = null, $default = null)
    {
        return ArrayUtility::last($array, $callback, $default);
    }
}

if ( ! function_exists('array_only') ) {
    /**
     * Get a subset of the items from the given array.
     *
     * @param array $array
     * @param array|string $keys
     * @return array
     */
    function array_only($array, $keys)
    {
        return ArrayUtility::only($array, $keys);
    }
}

if ( ! function_exists('array_pluck') ) {
    /**
     * Pluck an array of values from an array.
     *
     * @param array $array
     * @param string|array $value
     * @param string|array|null $key
     * @return array
     */
    function array_pluck($array, $value, $key = null)
    {
        return ArrayUtility::pluck($array, $value, $key);
    }
}

if ( ! function_exists('array_undot') ) {
    /**
     * Unflatten a multi-dimensional associative array with dots.
     *
     * @param array $dotNotationArray
     * @return array
     */
    function array_undot($dotNotationArray)
    {
        $array = [];

        foreach ( $dotNotationArray as $key => $value ) {
            $array = array_set($array, $key, $value);
        }

        return $array;
    }
}

if ( ! function_exists('format_type') ) {
    /**
     * Formats var in given format
     *
     * @param string $type
     * @param mixed $value
     * @return mixed
     */
    function format_type($type, $value)
    {
        if ( $type == 'float' ) {
            return filter_var($value, FILTER_VALIDATE_FLOAT) ?: 0;
        }

        if ( $type == 'integer' ) {
            return filter_var($value, FILTER_VALIDATE_INT) ?: 0;
        }

        if ( ! is_bool($value) && $type == 'boolean' ) {
            return filter_var($value, FILTER_VALIDATE_BOOLEAN) ?: false;
        }

        if ( is_string($value) && $type == 'datetime' ) {
            return \Cake\I18n\Time::parse($value);
        }

        if ( is_string($value) && $type == 'date' ) {
            return \Cake\I18n\Date::parse($value);
        }

        if ( is_string($value) && $type == 'object' ) {
            return json_decode($value === '[]' ? '{}' :
                ($value ?: '{}'), false);
        }

        if ( is_array($value) && $type == 'object' ) {
            return json_decode(json_encode($value), false);
        }

        if ( is_null($value) && $type == 'object' ) {
            return json_decode('{}', false);
        }

        if ( is_string($value) && $type == 'array' ) {
            return json_decode($value === '{}' ? '[]' :
                ($value ?: '[]'), true);
        }

        if ( is_object($value) && $type == 'array' ) {
            return json_decode(json_encode($value), true);
        }

        if ( is_null($value) && $type == 'array' ) {
            return json_decode('{}', true);
        }

        if ( is_object($value) && $type == 'string' ) {
            return json_encode($value);
        }

        if ( is_array($value) && $type == 'string' ) {
            return json_encode($value);
        }

        if ( is_null($value) && $type === 'string' ) {
            return '';
        }

        if ( is_object($value) && $type == 'text' ) {
            return json_encode($value);
        }

        if ( is_array($value) && $type == 'text' ) {
            return json_encode($value);
        }

        if ( is_null($value) && $type === 'text' ) {
            return '';
        }

//        if ( is_null($value) && $type == 'uuid' ) {
//            return null;
//        }

        return $value;
    }
}

if ( ! function_exists('convertToUTF8') ) {
    /**
     * Converts any string to UTF8
     *
     * @param string $content
     * @return string
     */
    function convertToUTF8($content)
    {
        $formats = [
            "ISO-8859-1", "ISO-8859-15" . "Windows-1251", "Windows-1252", "UTF-8",
        ];

        return mb_convert_encoding($content, "UTF-8", $formats);
    }
}

if ( ! function_exists('str_lreplace') ) {
    /**
     * Replace last occurrence of a string in a string
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     */
    function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if ( $pos !== false ) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }
}


if ( ! function_exists('include_view') ) {
    /**
     * include $file from src or src-sx folder
     *
     * @param string $file
     * @return string
     */
    function include_view($file)
    {
        $srcFolder = APP . 'Templates' . DS . $file;
        $srcSxFolder = SX . 'Templates' . DS . $file;

        if ( file_exists($srcFolder) ) {
            return $srcFolder;
        }

        return $srcSxFolder;
    }
}


if ( ! function_exists('telStrToHrefTel') ) {
    /**
     * convert Str Tel to Href Tel 
     *
     * @param string $number
     * @return string
     */
    function telStrToHrefTel($number="")
    {
    	$valid = "<^((\\+|00)[1-9]\\d{0,3}|0 ?[1-9]|\\(00? ?[1-9][\\d ]*\\))[\\d\\-/ ]*$>";
       if (preg_match($valid, $number)) {
            $number = preg_replace("<^\\+>", "00", $number);
            $number = preg_replace("<\\D+>", "", $number);
        }
        return $number;
    }
}
