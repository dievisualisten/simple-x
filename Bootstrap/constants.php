<?php
define('STATUS_RUNNING', 'Running ...');

if ( ! defined('EMPTY_VAR') ) {
    define('EMPTY_VAR', -128);
}

define('FONT_PATH', APP . 'fonts' . DS);

define('VOUCHER_PATH_PDFS', APP . 'files' . DS . 'vouchers' . DS . 'pdfs' . DS);
define('VOUCHER_PATH_LAYOUTS', APP . 'files' . DS . 'vouchers' . DS . 'layouts' . DS);
define('VOUCHER_PATH_PDF_PREVIEWS', APP . 'files' . DS . 'vouchers' . DS . 'pdfs' . DS . 'previews' . DS);

define('INVOICE_PATH_PDFS', APP . 'files' . DS . 'orders' . DS . 'pdfs' . DS);
define('INVOICE_PATH_LAYOUTS', APP . 'files' . DS . 'orders' . DS . 'layouts' . DS);


if ( ! defined('SHARED_FOLDER_ACO') ) {
    define('SHARED_FOLDER_ACO', '3735626e-5ae4-11e6-8b77-86f30ca893d3');
}

if ( ! defined('SUPER_ADMIN_ROLE') ) {
    define('SUPER_ADMIN_ROLE_ID', '22222222-2222-2222-2222-222222222222');
}

if ( ! defined('SUPER_ADMIN_PW') ) {
    define('SUPER_ADMIN_PW', '578a6d9aefbc353fd9e6c6aeb2ca8f04865437b1');
}

if ( ! defined('GMAP_CLIENT_API_KEY') ) {
    define('GMAP_CLIENT_API_KEY', 'AIzaSyDvJ7NNFrrSL13b3xEB07Itw0luJSHDTmE');
}

if ( ! defined('TEASER_WITHOUT_URL') ) {
    define('TEASER_WITHOUT_URL', [1024, 2048, 4096, 8192]);
}

if ( ! defined('FROALA_KEY') ) {
    define('FROALA_KEY', 'Kb3A3pD3D2G2B4C3D3oCd2ZSb1XHi1Cb2a1KIWCWMJHXCLSwG1G1B2B1B1C7A5E1D4B3==');
}



