<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Cache\Cache;
use Cake\Error\ConsoleErrorHandler;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Security;
use Cake\I18n\Time;

/*
 * Define app constants first
 */
include_once APP . '/Bootstrap/constants.php';

/*
 * Load sx constants
 */
include_once SX . '/Bootstrap/constants.php';


/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

/**
 * Uncomment block of code below if you want to use `.env` file during development.
 * You should copy `config/.env.default to `config/.env` and set/modify the
 * variables as required.
 */
if ( ! env('APP_NAME') && ! env('APP_DEBUG') && file_exists(ROOT . DS . '.env') ) {

    $dotenv = new \josegonzalez\Dotenv\Loader([
        ROOT . DS . '.env',
    ]);

    $dotenv->parse()->putenv()->toEnv()->toServer();
}

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {

    /**
     * Register default config path
     */
    Configure::config('default', new PhpConfig(SX . 'Config' . DS));

    /**
     * Filter bootstrap and routes file from folder
     */
    $files = array_filter(scandir(SX . 'Config' . DS), function ($file) {
        return ! in_array($file, ['bootstrap.php', 'bootstrap_cli.php', 'routes.php']);
    });

    /**
     * Load all php files from list
     */
    foreach ( $files as $file ) {

        $config = preg_replace('/\.php$/', '', $file);

        if ( $file == $config ) {
            continue;
        }

        Configure::load($config, 'default', true);
    }

    /**
     * Register default config path
     */
    Configure::config('default', new PhpConfig(APP . 'Config' . DS));

    /**
     * Filter bootstrap and routes file from folder
     */
    $files = array_filter(scandir(APP . 'Config' . DS), function ($file) {
        return ! in_array($file, ['bootstrap.php', 'bootstrap_cli.php', 'routes.php']);
    });

    /**
     * Load all php files from list
     */
    foreach ( $files as $file ) {

        $config = preg_replace('/\.php$/', '', $file);

        if ( $file == $config ) {
            continue;
        }

        Configure::load($config, 'default', true);
    }

    foreach ( Configure::read('Aliases', []) as $alias => $orginal ) {
        class_alias($orginal, $alias);
    }

} catch ( \Exception $e ) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file.
 * You can use a file like app_local.php to provide local overrides to your
 * shared configuration.
 */
//Configure::load('app_local', 'default');

/*
 * Override debug if ip is in scope and debug is in url.
 */

$hasDebugAccess = in_array(@$_SERVER['REMOTE_ADDR'] ?: '::1',
    (array) Configure::read('ips'));

$hasDebugParam = preg_match('/(^|&)debug(=true)?(&|$)/',
    @$_SERVER['QUERY_STRING'] ?: '');


if ( $hasDebugAccess && $hasDebugParam ) {
    Configure::write('debug', true);
}

/**
 * Override preview if preview is in url
 */
$hasPreviewParam = preg_match('/(^|&)preview(=true)?(&|$)/',
    @$_SERVER['QUERY_STRING'] ?: '');

if ( $hasPreviewParam ) {
    Configure::write('preview', true);
}

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */

if ( Configure::read('debug') ) {
    Configure::write('Cache._cake_model_.duration', '+1 minute');
    Configure::write('Cache._cake_core_.duration', '+1 minute');
}

/*
 * Set the default server timezone. Using UTC makes time calculations / conversions easier.
 * Check http://php.net/manual/en/timezones.php for list of valid timezone strings.
 */
date_default_timezone_set(Configure::read('App.defaultTimezone'));

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */


ini_set('intl.default_locale', Configure::read('Sx.app.language.defaultlanguage'));

/*
 * Register application error and exception handlers.
 */
$isCli = PHP_SAPI === 'cli';

if ( $isCli ) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new App\Error\ErrorHandler(Configure::read('Error')))->register();
}

/*
 * Include the CLI bootstrap overrides.
 */
if ( $isCli ) {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if ( ! Configure::read('App.fullBaseUrl') ) {

    $httpHost = env('HTTP_HOST');

    if ( isset($httpHost) ) {
        Configure::write('App.fullBaseUrl', (env('HTTPS') ? 'https' : 'http') . '://' . $httpHost);
    }

    unset($httpHost);
}

Cache::setConfig(Configure::consume('Cache'));
ConnectionManager::setConfig(Configure::consume('Datasources'));
TransportFactory::setConfig(Configure::consume('EmailTransport'));
Mailer::setConfig(Configure::consume('Email'));
Log::setConfig(Configure::consume('Log'));
Security::setSalt(Configure::consume('Security.salt'));

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
ServerRequest::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isMobile();
});
ServerRequest::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isTablet();
});

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link https://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
Type::build('time')
    ->useImmutable();
Type::build('date')
    ->useImmutable();
Type::build('datetime')
    ->useImmutable();
Type::build('timestamp')
    ->useImmutable();

/*
 * Custom Inflector rules, can be set to correctly pluralize or singularize
 * table, model, controller names or whatever other string is passed to the
 * inflection functions.
 */
//Inflector::rules('plural', ['/^(inflect)or$/i' => '\1ables']);
//Inflector::rules('irregular', ['red' => 'redlings']);
//Inflector::rules('uninflected', ['dontinflectme']);
//Inflector::rules('transliteration', ['/å/' => 'aa']);

/**
 * Enable default locale format parsing.
 * This is needed for matching the auto-localized string output of Time() class when parsing dates.
 *
 * Also enable immutable time objects in the ORM.
 */
//Type::build('time')->useImmutable()->useLocaleParser();
//Type::build('date')->useImmutable()->useLocaleParser();
//Type::build('datetime')->useImmutable()->useLocaleParser()->setLocaleFormat('yyyy-MM-dd HH:mm:ss');

Time::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');
FrozenDate::setToStringFormat('yyyy-MM-dd');

use mm\Mime\Type as MMType;

MMType::config('glob', [
    'adapter' => 'Freedesktop',
    'file' => ROOT . DS . 'vendor' . DS . 'davidpersson' . DS . 'mm' . DS . 'data' . DS . 'glob.db',
]);

MMType::config('magic', [
    'adapter' => 'Fileinfo',
]);

MMType::$name = [
    'application/ogg'       => 'audio',
    'application/pdf'       => 'document',
    'application/msword'    => 'document',
    'officedocument'        => 'document',
    'image/icon'            => 'icon',
    'text/css'              => 'css',
    'text/javascript'       => 'javascript',
    'text/code'             => 'generic',
    'text/rtf'              => 'document',
    'text/plain'            => 'text',
    'image/svg'             => 'svg',
    'image/gif'             => 'gif',
    'image/'                => 'image',
    'audio/'                => 'audio',
    'video/'                => 'video',
    '/'                     => 'generic'
];
