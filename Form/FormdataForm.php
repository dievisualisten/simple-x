<?php

namespace App\Form;


use App\Library\Formular\FormularField;
use Cake\Event\EventManager;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class FormdataForm extends Form
{
    /**
     * Form fields
     *
     * @var array
     */
    public $fields = [
        //
    ];

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        'text' => 'string',
        'textarea' => 'text',
        'checkbox' => 'bool',
        'radio' => 'string',
        'select' => 'string',
        'multiselect' => 'array',
        'number' => 'integer',
        'date' => 'date',
        'datetime' => 'datetime',
        'time' => 'time',
        'hidden' => 'string',
        'file' => '',
        'element' => 'string',
        'password' => 'string',
    ];

    /**
     * Default empty messages
     *
     * @var array
     */
    protected $_typeEmptyErrorMessages = [
        //
    ];


    /**
     * @inheritdoc
     */
    public function __construct(?EventManager $eventManager = null)
    {
        // Set tranlatebale empty messages
        $this->_typeEmptyErrorMessages = [
            'text' => __('Bitte füllen Sie das Feld aus.'),
            'textarea' => __('Bitte füllen Sie das Feld aus.'),
            'password' => __('Bitte füllen Sie das Feld aus.'),
            'checkbox' => __('Bitte markieren Sie die Checkbox.'),
            'radio' => __('Bitte treffen Sie eine Auswahl.'),
            'select' => __('Bitte treffen Sie eine Auswahl.'),
            'multiselect' => __('Bitte treffen Sie eine Auswahl.'),
            'zahl' => __('Bitte füllen Sie das Feld aus.'),
            'date' => __('Bitte füllen Sie das Feld aus.'),
            'datetime' => __('Bitte füllen Sie das Feld aus.'),
            'time' => __('Bitte füllen Sie das Feld aus.'),
            'file' => __('Bitte laden Sie eine Datei hoch.'),
            'fileuploader' => __('Bitte laden Sie eine Datei hoch.'),
            'element' => __('??'),
        ];


        return parent::__construct($eventManager);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data): bool
    {
        $data = Hash::flatten($data);

        $validator = $this->getValidator();
        $this->_errors = Hash::expand($validator->validate($data));

        return count($this->_errors) === 0;
    }

    /**
     * Set form fields
     *
     * @param array $fields
     * @return $this
     */
    public function setFields($fields = [])
    {
        // Set fields.
        foreach ( $fields as $config ) {
            $this->fields[] = new FormularField($config);
        }

        return $this;
    }

    /**
     * Get form fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Get field by name
     *
     * @param $name
     * @return FormularField|null
     */
    public function getField($name): FormularField
    {
        $fields = array_filter($this->fields, function ($field) use ($name) {
            return $field->getName() === $name;
        });

        return ! empty($fields) ? reset($fields) : null;
    }

    /**
     * @inheritdoc
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        foreach ( $this->fields as $field ) {

            /** @var \App\Library\Formular\FormularField $field */

            if ( ! $field->hasTypeIn($this->_casts) ) {
                continue;
            }

            $schema->addField($field->getName(),
                $field->solveTypeIn($this->_casts));
        }

        return $schema;
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        // Use simplex validator.
        $validator->setProvider('simplex', "App\Model\Validation\Validation");

        $reflectionClass = new \ReflectionClass("App\Model\Validation\Validation");

        foreach ( $this->fields as $field ) {

            /** @var \App\Library\Formular\FormularField $field */

            if ( ! array_key_exists($field->type, $this->_casts) ) {
                continue;
            }

            $notBlankMessage = $field->notBlankMessage ?:
                $field->solveTypeIn($this->_typeEmptyErrorMessages);


            if ( $field->notBlank ) {

                $validator->add($field->getName(), "_notBlank", [
                    'provider' => 'simplex', 'notBlank', 'message' => $notBlankMessage,
                ]);

            }

            foreach ( $field->getValidationRules() as $rule ) {

                $rules = [
                    $rule['validationRule'],
                ];

                if ( ! realempty($rule['validationParam1']) ) {
                    $rules[] = $rule['validationParam1'];
                }

                if ( ! realempty($rule['validationParam2']) ) {
                    $rules[] = $rule['validationParam2'];
                }

                $provider = 'default';

                if ( $reflectionClass->hasMethod($rule['validationRule']) ) {
                    $provider = 'simplex';
                }

                $validator->add($field->getName(), "_{$rule['validationRule']}", [
                    'provider' => $provider,
                    'rule' => $rules,
                    'message' => $rule['validationMessage'],
                ]);
            }
        }

        return $validator;
    }

    /**
     * @param array $errors
     */
    public function addErrors(array $errors = [])
    {
        $this->_errors = array_merge_recursive($this->_errors, $errors);

    }


}
