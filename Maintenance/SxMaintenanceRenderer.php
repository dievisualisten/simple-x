<?php

namespace App\Maintenance;

use Exception;
use Cake\Core\App;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Core\Container;
use Cake\Routing\Router;
use Cake\Http\ServerRequest;
use Cake\Controller\Controller;
use Cake\Http\ServerRequestFactory;
use Cake\Routing\DispatcherFactory;
use Cake\Controller\ControllerFactory;

/**
 * @version 4.0.0
 */

class SxMaintenanceRenderer
{
    /**
     * Get controller which should be used.
     *
     * @var array
     */
    protected $className = [
        'Maintenance', 'Controller', 'Controller'
    ];

    /**
     * Controller instance.
     *
     * @var \Cake\Controller\Controller
     */
    protected $controller;

    /**
     * Template to render.
     *
     * @var string
     */
    protected $template = 'index';

    /**
     * Status code for response.
     *
     * @var int
     */
    protected $statusCode = 503;

    /**
     * If set, this will be request used to create the controller that will render
     * the maintenance.
     *
     * @var \Cake\Http\ServerRequest|null
     */
    protected $request = null;

    /**
     * MaintenanceRenderer constructor.
     *
     * @param \Cake\Http\ServerRequest $request
     */
    public function __construct(ServerRequest $request = null)
    {
        $this->request = $request;
        $this->controller = $this->_getController();
    }

    protected function _getController(): Controller
    {
        $request = $this->request;
        $routerRequest = Router::getRequest();

        // Fallback to the request in the router or make a new one from
        // $_SERVER
        if ($request === null) {
            $request = $routerRequest ?: ServerRequestFactory::fromGlobals();
        }

        // If the current request doesn't have routing data, but we
        // found a request in the router context copy the params over
        if ($request->getParam('controller') === null && $routerRequest !== null) {
            $request = $request->withAttribute('params', $routerRequest->getAttribute('params'));
        }

        $errorOccured = false;
        try {
            $params = $request->getAttribute('params');
            $params['controller'] = 'Maintenance';

            $factory = new ControllerFactory(new Container());
            $class = $factory->getControllerClass($request->withAttribute('params', $params));

            if (!$class) {
                /** @var string $class */
                $class = App::className(...$this->className);
            }

            /** @var \Cake\Controller\Controller $controller */
            $controller = new $class($request);

            $controller->startupProcess();
        } catch (\Throwable $e) {
            $errorOccured = true;
        }

        if (!isset($controller)) {
            return new Controller($request);
        }

        // Retry RequestHandler, as another aspect of startupProcess()
        // could have failed. Ignore any exceptions out of startup, as
        // there could be userland input data parsers.
        if ($errorOccured && isset($controller->RequestHandler)) {
            try {
                $event = new Event('Controller.startup', $controller);
                $controller->RequestHandler->startup($event);
            } catch (\Throwable $e) {
                //
            }
        }

        return $controller;
    }

    /**
     * Renders the response for the exception.
     *
     * @return \Cake\Http\Response The response to be sent.
     */
    public function render()
    {
        $url = $this->controller->getRequest()->getRequestTarget();
        $response = $this->controller->getResponse();

        $response = $response->withStatus($this->statusCode);

        $viewVars = [
            'message' => __('Maintenance'),
            'url' => h($url),
            'code' => $this->statusCode,
            '_serialize' => ['message', 'url', 'code'],
        ];

        $this->controller->set($viewVars);

        $this->controller->response = $response;

        $this->controller->render($this->template);

        return $this->_shutdown();
    }

    /**
     * Run the shutdown events.
     *
     * Triggers the afterFilter and afterDispatch events.
     *
     * @return \Cake\Http\Response The response to serve.
     */
    protected function _shutdown(): Response
    {
        $this->controller->dispatchEvent('Controller.shutdown');

        return $this->controller->getResponse();
    }

}
