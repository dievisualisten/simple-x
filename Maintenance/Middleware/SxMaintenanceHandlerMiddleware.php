<?php

namespace App\Maintenance\Middleware;

use Cake\Core\App;
use App\Library\AuthManager;
use App\Maintenance\MaintenanceRenderer;

/**
 * @version 4.0.0
 */

class SxMaintenanceHandlerMiddleware
{
    /**
     * @var array $allowed
     */
    public $allowed = [
        '/^(\/[a-z]+)?(\/users)?\/login(\.json|\.html)?(\?.*?)?$/i',
        '/^\/system(\/.*?)?$/i'
    ];

    /**
     * @inheritdoc
     */
    public function __invoke($request, $response, $next)
    {
        $allowed = collect($this->allowed)->map(function ($pattern) use ($request) {
            return preg_match($pattern, $request->getRequestTarget()) ? true : false;
        });

        $isAuthorized = AuthManager::isAuthorized('System', 'index') ||
            AuthManager::isAuthorized('System', '*');

        $isModeOn = format_type('integer', config('Sx.app.maintenance'));

        if ( ! $isModeOn || $allowed->contains(true) || $isAuthorized ) {
            return $next($request, $response);
        }

        $class = App::className(MaintenanceRenderer::class, 'Maintenance');

        /* @var \App\Maintenance\MaintenanceRenderer $renderer */
        $renderer = new $class($request);

        return $renderer->render();
    }

}
