<?php

namespace App\Library\Translation;

use App\Library\Facades\Language;
use Cake\ORM\Behavior\Translate\ShadowTableStrategy;

class SxShadowTableStrategy extends ShadowTableStrategy
{
    /**
     * Merge config from behaviour
     *
     * @param $values
     * @return $this
     */
    public function mergeConfig($values)
    {
        $this->_config = array_merge($this->_config, $values);

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale ?: Language::getLanguage();
    }

    /**
     * Modifies the results from a table find in order to merge the translated fields
     * into each entity for a given locale.
     *
     * @param \Cake\Datasource\ResultSetInterface $results Results to map.
     * @param string $locale Locale string
     * @return \Cake\Collection\CollectionInterface
     */
    protected function rowMapper($results, $locale)
    {
        $allowEmpty = $this->_config['allowEmptyTranslations'];

        if ( ! isset ($this->_config['clearNotTranslatedFields']) ) {
            $this->_config['clearNotTranslatedFields'] = false;
        }

        $clearFields = $this->_config['clearNotTranslatedFields'];

        if ( ! isset ($this->_config['forceRealLocale']) ) {
            $this->_config['forceRealLocale'] = false;
        }

        $forceLocale = $this->_config['forceRealLocale'];

        return $results->map(function ($row) use ($allowEmpty, $clearFields, $forceLocale, $locale) {

            /** @var \Cake\ORM\Entity $row */

            if ( $row === null ) {
                return $row;
            }

            $hydrated = ! is_array($row);

            if ( empty($row['translation']) ) {

                $row['_locale'] = Language::getDefaultLanguage();

                if ( $clearFields && $locale !== $row['_locale'] ) {

                    foreach ( $this->translatedFields() as $field ) {
                        $row[$field] = '';
                    }

                }

                if ( $forceLocale ) {
                    $row['_locale'] = $locale;
                }

                unset($row['translation']);

                if ($hydrated) {
                    $row->clean();
                }

                return $row;
            }

            $translation = $row['translation'];

            $keys = $hydrated ? $translation->getVisible() : array_keys($translation);

            /** @var $row \App\Model\Entity\AppEntity */

            foreach ( $keys as $field ) {

                if ( $translation[$field] !== null ) {
                    if ( $allowEmpty ) {
                        $row->set($field, $translation[$field]);
                    }
                }

            }

            if ( isset($translation['locale']) ) {
                $row->set('_locale', $translation['locale']);
            }

            unset($row['translation'], $row['locale']);

            if ( $hydrated ) {
                $row->clean();
            }

            return $row;
        });
    }

}
