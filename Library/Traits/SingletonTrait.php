<?php

namespace App\Library\Traits;

use Closure;

trait SingletonTrait
{
    private static $instance;

    private final function __construct()
    {
        static::$instance = $this;
    }

    public final static function getInstance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public final static function runInstance($callback)
    {
        return Closure::bind($callback, self::getInstance(), self::class)();
    }

    public final function __call($name, $arguments)
    {
        return self::getInstance()->{$name}(...$arguments);
    }

    public final static function __callStatic($name, $arguments)
    {
        return self::getInstance()->{$name}(...$arguments);
    }
}
