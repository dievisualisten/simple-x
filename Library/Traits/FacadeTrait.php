<?php

namespace App\Library\Traits;

trait FacadeTrait
{
    protected static $serviceInstance;

    public static function setupFacade()
    {
        if ( ! self::$serviceInstance ) {
            self::$serviceInstance = new self::$serviceClass(...func_get_args());
        }

        if ( method_exists(self::$serviceInstance, 'setupService') ) {
            self::$serviceInstance->setupService();
        }

        return self::$serviceInstance;
    }

    public static function getInstance()
    {
        if ( ! isset(self::$serviceInstance) ) {
            throw new \Exception('"' . self::$serviceClass . '" not initialized');
        }

        return self::$serviceInstance;
    }

    public function __call($name, $arguments)
    {
        return self::getInstance()->{$name}(...$arguments);
    }

    public final static function __callStatic($name, $arguments)
    {
        return self::getInstance()->{$name}(...$arguments);
    }
}
