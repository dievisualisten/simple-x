<?php

namespace App\Library\Middleware;

use App\Library\Facades\Article;
use App\Library\Facades\Language;
use App\Library\Facades\Logger;
use App\Library\Facades\Mailer;
use App\Library\Facades\Menu;
use App\Library\Facades\Request;
use App\Library\Facades\Search;
use Cake\Http\ServerRequest;
use Psr\Http\Message\ResponseInterface;

/**
 * @version 4.0.0
 */
class SxSetupMiddleware
{
    /**
     * @inheritdoc
     */
    public function __invoke(ServerRequest $request, ResponseInterface $response, $next)
    {
        Request::setupFacade($request);
        Logger::setupFacade($request);
        Language::setupFacade($request);
        Menu::setupFacade($request);
        Mailer::setupFacade($request);
        Article::setupFacade($request);
        Search::setupFacade($request);

        return $next($request, $response);
    }

}
