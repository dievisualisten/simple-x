<?php

namespace App\Library\Article;

use App\Model\Entity\Article;

class SxCalendarParser implements ParserInterface
{
    public function matchArticle(Article $article): bool
    {
        return $article->get('type', 'default') === 'calendar';
    }

    public function parseArticle(Article &$article): void
    {
        //
    }

    public function parseChildren(Article &$article, &$children): void
    {
        $baseEvents = array_filter($children, function ($child) {
            return $child->get('type', 'page') === 'event';
        });

        $children = array_filter($children, function ($child) {
            return $child->get('type', 'page') !== 'event';
        });

        $finalEvents = [];

        foreach ( $baseEvents as $baseEvent ) {

            foreach ( $baseEvent->get('menu.events', []) as $finalEvent ) {

                $cloneEvent = $baseEvent->clone(['menu']);

                // Format date
                $formatedDate = $finalEvent->date->format('Y-m-d');

                $cloneEvent->set('menu.path',
                    "{$baseEvent->menu->path}/event:{$formatedDate}");

                // Set start date
                $cloneEvent->set('extended_data_cfg.event.start_date',
                    $finalEvent->date);

                $finalEvents[] = $cloneEvent;
            }

        }

        $finalEvents = collect($finalEvents)
            ->sortBy('extended_data_cfg.event.start_date')->toArray();

        $article->set('events', $finalEvents);
    }
}
