<?php

namespace App\Library\Article;

use App\Model\Entity\Article;

interface ParserInterface
{
    public function matchArticle(Article $article): bool;

    public function parseArticle(Article &$article): void;

    public function parseChildren(Article &$article, &$children): void;
}
