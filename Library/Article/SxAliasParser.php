<?php

namespace App\Library\Article;

use App\Library\Facades\Article as ArticleService;
use App\Model\Entity\Article;

class SxAliasParser implements ParserInterface
{
    public function matchArticle(Article $article): bool
    {
        return true;
    }

    public function parseArticle(Article &$article): void
    {
        //
    }

    public function parseChildren(Article &$article, &$children): void
    {
        $children = array_map(function ($child) {
            return ArticleService::getArticleByArticleId($child->foreign_key);
        }, $children);
    }
}
