<?php

namespace App\Library\Services;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\Exception\NotImplementedException;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;


/**
 * @version 4.0.0 Fixme ZS
 */
class SxSearchService
{
    use LocatorAwareTrait;

    /**
     * @var | insta
     */
    protected $client = null;

    /**
     * @var |null
     */
    protected $documentType = '_doc';


    public function getDocumentType()
    {
        return $this->documentType;
    }

    public function setDocumentType($type)
    {
        $this->documentType = $type;
    }

    public function getClient(): Client
    {

        if ( $this->client === null ) {
            $this->client = ClientBuilder::create()
                ->setHosts([
                    config('Elasticsearch.config'),
                ])
                ->build();
        }


        return $this->client;
    }

    public function reIndexAll($tableName, $locale)
    {

        $aliasName = $this->getPrefixedIndexName($tableName, $locale);
        $currentIndex = null;
        $indexA = $this->getPrefixedIndexName($tableName . '_a', $locale);
        $indexB = $this->getPrefixedIndexName($tableName . '_b', $locale);
        $tableName = strtolower($tableName);

        $indexExists = $this->getClient()->indices()->exists([
            'index'  => $indexA,
            'client' => [
                'verbose' => true,
                'verify'  => false //because SSL ERROR local on windows
            ],
        ]);

        if ( $indexExists ) {
            $currentIndex = 'a';
        }

        $indexExists = $this->getClient()->indices()->exists([
            'index'  => $indexB,
            'client' => [
                'verbose' => true,
                'verify'  => false //because SSL ERROR local on windows
            ],
        ]);

        if ( $indexExists ) {
            $currentIndex = 'b';
        }

        if ( $currentIndex == 'a' ) {
            $index = $indexB;
            $deleteIndex = $indexA;
        } else {
            $index = $indexA;
            $deleteIndex = $indexB;
        }

        $this->createFulltextIndex($index, $locale);

        $table = $this->getTableLocator()->get(ucfirst($tableName));

        $query = $table->getElasticSearchIndexQuery();

        $params = [
            'body'   => [],
            'client' => [
                'verbose' => true,
                'verify'  => false //because SSL ERROR local on windows
            ],
        ];
        foreach ( $query as $k => $entity ) {
            $params['body'][] = [
                'index' => [
                    '_index' => $index,
                    '_id'    => $entity->get('id'),
                    '_type'  => $this->getDocumentType(),
                ],
            ];

            $params['body'][] = $table->convertToElasticSearchData($entity);

            // Every 1000 documents stop and send the bulk request
            if ( $k % 1000 == 0 ) {
                $responses = $this->getClient()->bulk($params);
                // erase the old bulk request
                $params = [
                    'body'   => [],
                    'client' => [
                        'verbose' => true,
                        'verify'  => false //because SSL ERROR local on windows
                    ],
                ];
                // unset the bulk response when you are done to save memory
                unset($responses);
            }
        }

        // Send the last batch if it exists
        if ( ! empty($params['body']) ) {
            $this->getClient()->bulk($params);
        }

        $params['body'] = [

            'actions' => [
                [
                    'add' => [
                        'index' => $index,
                        'alias' => $aliasName,
                    ],
                ],
            ],
        ];


        $this->getClient()->indices()->updateAliases($params);

        $this->getClient()->indices()->delete([
            'index'  => $deleteIndex,
            'client' => [
                'verify' => false //because SSL ERROR local on windows
            ],
        ]);

    }


    public function getPrefixedIndexName($indexName, $locale)
    {
        if ( empty($locale) ) {
            $locale = language()->getDefaultLanguage();;
        }

        $prefix = Configure::read('Elasticsearch.indexprefix');

        if ( $prefix == 'myprefix' ) {
            throw new NotImplementedException('Change the Elasticsearch.indexprefix config to an unique prefix.');
        }

        return strtolower(Configure::read('Elasticsearch.indexprefix') . '_' . $indexName . '_' . $locale);
    }


    public function createFulltextIndex($index, $locale = 'en')
    {
        $langConfigs = [
            'en' => [
                'filter'   => [
                    'english_stop'               => [
                        'type'      => 'stop',
                        'stopwords' => '_english_',
                    ],
                    'english_stemmer'            => [
                        'type'     => 'stemmer',
                        'language' => 'english',
                    ],
                    'english_possessive_stemmer' => [
                        'type'     => 'stemmer',
                        'language' => 'possessive_english',
                    ],
                ],
                'analyzer' => [
                    'default' => [
                        'tokenizer' => 'standard',
                        'filter'    => [
                            'english_possessive_stemmer',
                            'lowercase',
                            'english_stop',
                            'english_stemmer',
                        ],
                    ],
                ],
            ],
            'de' => [
                'filter'   => [
                    'german_stop'            => [
                        'type'      => 'stop',
                        'stopwords' => '_german_',
                    ],
                    'german_stemmer'         => [
                        'type'     => 'stemmer',
                        'language' => 'light_german',
                    ],
                    'snowball_german_umlaut' => [
                        'type' => 'snowball',
                        'name' => 'German2',
                    ],
                ],
                'analyzer' => [
                    'default' => [
                        'tokenizer' => 'standard',
                        'filter'    => [
                            'snowball_german_umlaut',
                            'lowercase',
                            'german_stop',
                            'german_normalization',
                            'german_stemmer',
                            'asciifolding',
                        ],
                    ],
                ],
            ],
        ];


        $langConfg = [];

        if ( ! empty($langConfigs[$locale]) ) {
            $langConfg = $langConfigs[$locale];
        }

        $settings = [
            'number_of_shards'                   => 1,
            'number_of_replicas'                 => 0,
            'index.write.wait_for_active_shards' => 1,
            'analysis'                           => $langConfg,
        ];

        $indexExists = $this->getClient()->indices()->exists([
            'index'  => $index,
            'client' => [
                'verbose' => true,
                'verify'  => false //because SSL ERROR local on windows
            ],
        ]);


        if ( $indexExists ) {
            return;
        }


        $this->getClient()->indices()->create([
            'index'  => $index,
            'body'   => [
                'settings' => $settings,
            ],
            'client' => [
                'verbose' => true,
                'verify'  => false //because SSL ERROR local on windows
            ],
        ]);
    }

    public function getSearchBody($queryString)
    {
        if ( substr($queryString, 0, 1) == '~' ) {
            $queryString = substr($queryString, 1);

            if ( empty($queryString) ) {
                return [];
            }

            return [
                'query' => [
                    'multi_match' => [
                        'query'            => strtolower($queryString),
                        'operator'         => 'and',
                        'fuzziness'        => 1.2,
                        'cutoff_frequency' => 0.001,
                    ],
                ],
            ];
        }

        if ( substr($queryString, 0, 3) == 'OR~' ) {
            $queryString = substr($queryString, 3);

            return [
                'query' => [
                    'multi_match' => [
                        'query'            => strtolower($queryString),
                        'operator'         => 'or',
                        'fuzziness'        => 1.2,
                        'cutoff_frequency' => 0.001,
                    ],
                ],
            ];
        }

        return [];
    }


    public function perfomSearch(Controller $controller, Entity $articleEntity)
    {
        $isPerformed = (bool) $controller->getRequest()->getData('search.performed', false);
        $result = null;

        if ( $isPerformed === true ) {

            $methode = $controller->getRequest()->getData('search.methode', 'fulltext');
            $model = $controller->getRequest()->getData('search.model', 'Articles');
            $fields = $controller->getRequest()->getData('search.fields', []);


            $params = null;

            if ( $methode === 'fulltext' ) {
                $params = [
                    "size"  => 100, //TODO SX4
                    'index' => search()->getPrefixedIndexName($model, language()->getLanguage()),
                    'body'  => search()->getSearchBody('~' . trim($fields['query'])),
                ];
            }

            if ( $methode === 'filltext_or' ) {
                $params = [
                    "size"  => 100, //TODO SX4
                    'index' => search()->getPrefixedIndexName($model, language()->getLanguage()),
                    'body'  => search()->getSearchBody('OR~' . trim($fields['query'])),
                ];
            }

            //Fixme ZS
            /*
                        if ($qType == 'location') {
                            if (!empty($search['location'])) {
                                $country = (isset($search['country']) ? $search['country'] : "de");
                                $distance = (!empty($search['distance']) ? $search['distance'] : 20) . "km";
                                $point = $this->Geo->findGeoPoint($search['location'], $country);
                                if (!empty($point)) {
                                    $pagination['searchsettings']['filter'][]['geo_distance'] = [
                                        'distance' => $distance,
                                        'location' => $point['lat'] . ',' . $point['lon'],
                                    ];
                                }
                            }
                        } else {
                            if (!in_array($qType, ['country', 'distance'])) {
                                if ($qType == 'range') {
                                    $myfilter = [];
                                    foreach ($filter as $instruction => $oneFilter) {
                                        $f = Hash::flatten($oneFilter);
                                        $t = array_keys($f);
                                        $field = $t[0];
                                        $t = array_values($f);
                                        $value = $t[0];
                                        if (!empty($value)) {
                                            $myfilter[$instruction] = $value;
                                        }
                                    }
                                } else {
                                    $myfilter = "";

                                    if (is_array($filter)) {
                                        $f = Hash::flatten($filter);
                                        $t = array_keys($f);
                                        $field = $t[0];
                                        $t = array_values($f);
                                        $myfilter = $t[0];
                                    }
                                }
                                if (!empty($myfilter)) {
                                    $pagination['searchsettings']['filter'][][$qType][$field] = $myfilter;
                                }
                            }
                        }*/


            if ( $params ) {

                $searchResults = search()->getClient()->search($params);
                $ids = Hash::extract($searchResults, 'hits.hits.{n}._id');

                if ( ! empty($ids) ) {
                    $table = $this->getTableLocator()->get('Menus');

                    $query = $table->find('published')->where([
                        'Menus.type NOT IN'    => ['frontpage', 'menu', 'domain', 'alias'],
                        'Menus.teaser NOT IN'  => TEASER_WITHOUT_URL,
                        'Menus.foreign_key IN' => $ids,
                    ])
                        ->contain(['Articles.Attachments'])
                        ->order('FIELD(Menus.foreign_key, "' . implode('","', $ids) . '")');


                    if ( $articleEntity->get('extended_data_cfg.search.single_domain', true) ) {
                        $query->where([
                            'Menus.lft >'  => menu()->getDomainRecord('lft'),
                            'Menus.rght <' => menu()->getDomainRecord('rght'),
                        ]);
                    }

                    $result = $query->all();
                }
            } else {
                throwException(NotImplementedException::class);
            }
        }

        $controller->set('searchresult', $result);
    }
}
