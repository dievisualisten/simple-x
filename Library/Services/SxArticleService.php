<?php

namespace App\Library\Services;

use App\Library\AuthManager;
use App\Library\Facades\Language;
use App\Library\Facades\Request;
use App\Model\Entity\Article;
use Cake\Controller\Controller;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\ServerRequest;
use Cake\Datasource\ModelAwareTrait;
use Cake\I18n\Time;
use Cake\ORM\Entity;

/**
 * @property \App\Model\Table\ArticlesTable $Articles
 * @property \App\Model\Table\MenusTable $Menus
 * @property \App\Model\Table\ActionviewsTable $Actionviews
 */
class SxArticleService
{
    use ModelAwareTrait;

    /**
     * @var \Cake\Http\ServerRequest $request
     */
    protected $_request;

    protected $_parserList = [
        //
    ];

    public $contains = [
        'Attached',
        'Attached.Attachments',
        'Attached.Attachments.Thumbnail',
        'Acos',
        'Formularconfigs.Formulars',
        'Formularconfigs.Emails',
    ];

    public $menuContains = [
        'Events',
        'ChildMenus',
        'ChildMenus.Events',
    ];

    public function __construct(ServerRequest $request)
    {
        $this->_request = $request;

        foreach ( config('Article.parser') as $parser ) {
            $this->_parserList[] = new $parser;
        }

        $this->loadModel('Articles');
        $this->loadModel('Menus');
        $this->loadModel('Actionviews');
    }

    public function runArticleParsers(&$article)
    {
        /** @var \App\Library\Article\ParserInterface $parser */

        foreach ( $this->_parserList as $parser ) {
            if ( $parser->matchArticle($article) ) {
                $parser->parseArticle($article);
            }
        }

        return $article;
    }

    public function runChildrenParsers(&$article, &$children)
    {
        /** @var \App\Library\Article\ParserInterface $parser */

        foreach ( $this->_parserList as $parser ) {
            if ( $parser->matchArticle($article) ) {
                $parser->parseChildren($article, $children);
            }
        }

        return $children;
    }

    public function getContains()
    {
        $defaultContains = [
            'Menus' => [
                'Articles', 'Events',
            ],
        ];

        $defaultContains['Articles'] = $this->Articles
            ->getDefaultContains();

        $defaultContains['Articles'] = array_merge($defaultContains['Articles'],
            $this->contains);

        $queries = [
            'Events' => function ($query) {
                return $this->getEventQuery($query);
            },
            'Default' => function ($query) {
                return $query;
            }
        ];

        foreach ( $defaultContains['Articles'] as $contain ) {

            if ( ! isset($defaultContains[$contain]) ) {
                continue;
            }

            foreach ( $defaultContains[$contain] as $child ) {
                $defaultContains['Articles'][] = "{$contain}.{$child}";
            }

        }

        $contains = [];

        foreach ( $defaultContains['Articles'] as $contain ) {

            $realContain = preg_replace('/^(.*?)([^\.]+)$/', '$2', $contain);

            if ( ! isset($query[$realContain]) ) {
                $realContain = 'Default';
            }

            $contains[$contain] = $queries[$realContain];

        }

        return $contains;
    }

    public function getMenuContains()
    {
        $defaultContains = [
            'Menus' => [
                'Articles', 'Events', 'ChildMenus'
            ],
            'ChildMenus' => [
                'Articles', 'Events', //'ChildMenus'
            ]
        ];

        $defaultContains['Articles'] = $this->Articles
            ->getDefaultContains();

        $defaultContains['Articles'] = array_merge($defaultContains['Articles'],
            $this->contains);

        $queries = [
            'ChildMenus' => function ($query) {
                return $this->Menus->findPublished($query);
            },
            'Events' => function ($query) {
                return $this->getEventQuery($query);
            },
            'Default' => function ($query) {
                return $query;
            }
        ];

        foreach ( $defaultContains['ChildMenus'] as $contain ) {

            if ( ! isset($defaultContains[$contain]) ) {
                continue;
            }

            foreach ( $defaultContains[$contain] as $child ) {
                $defaultContains['ChildMenus'][] = "{$contain}.{$child}";
            }

        }

        foreach ( $defaultContains['Menus'] as $contain ) {

            if ( ! isset($defaultContains[$contain]) ) {
                continue;
            }

            foreach ( $defaultContains[$contain] as $child ) {
                $defaultContains['Menus'][] = "{$contain}.{$child}";
            }

        }

        $contains = [];

        foreach ( $defaultContains['Menus'] as $contain ) {

            $realContain = preg_replace('/^(.*?)([^\.]+)$/', '$2', $contain);

            if ( ! isset($queries[$realContain]) ) {
                $realContain = 'Default';
            }

            $contains[$contain] = $queries[$realContain];

        }

        return $contains;
    }

    public function getArticleQuery()
    {
        $contains = $this->getContains();

        return $this->Articles->find()
            ->contain($contains, true)->leftJoinWith('Menus');
    }

    public function getArticleByArticleId($id)
    {
        $conditions = [
            'Articles.id' => $id,
        ];

        $article = AuthManager::unprotected(function () use ($conditions) {
            return $this->getArticleQuery()->where($conditions)->first();
        });

        if ( ! empty($article) ) {
            $this->runArticleParsers($article);
        }

        return $article;
    }

    public function getArticleByMenu($menu, $fallback = null)
    {
        $article = $menu->article;

        if ( empty($article) ) {
            return $fallback;
        }

        $article->set('menu', $menu);

        if ( ! empty($article) ) {
            $this->runArticleParsers($article);
        }

        return $article;
    }

    public function getArticleByMenuId($id, $props = [])
    {
        $conditions = [
            'Menus.id' => $id,
        ];

        $article = AuthManager::unprotected(function () use ($conditions) {
            return $this->getArticleQuery()->where($conditions)->first();
        });

        foreach ( $props as $key => $value ) {
            $article->set($key, $value);
        }

        if ( ! empty($article) ) {
            $this->runArticleParsers($article);
        }

        return $article;
    }

    public function getMonthRangeStart()
    {
        $startDateParam = Request::param('from');

        if ( $startDateParam ) {
            return Time::parse($startDateParam)->format('Y-m-d');
        }

        return Time::parse('now')->format('Y-m-d');
    }

    public function getMonthRangeEnd()
    {
        $endDateParam = Request::param('to');

        if ( $endDateParam ) {
            return Time::parse($endDateParam)->format('Y-m-d');
        }

        $defaultFuture = config('Sx.app.calendar.range.default', 90);

        return Time::parse($this->getMonthRangeStart())
            ->modify("+{$defaultFuture}days")->format('Y-m-d');
    }

    public function getEventQuery($query)
    {
        // TODO: Filter events to a specific date

        $query->order([
            'Events.date' => 'ASC',
        ]);

        return $query;
    }

    public function getMonthQuery($query)
    {
        $query
            ->where([
                'Events.date >=' => $this->getMonthRangeStart(),
                'Events.date <=' => $this->getMonthRangeEnd(),
            ]);

        return $query;
    }

    public function getMenuQuery()
    {
        $contains = $this->getMenuContains();

        return $this->Menus->find('published')
            ->contain($contains, true);
    }

    public function getMenuById($id)
    {
        $conditions = [
            'Menus.id' => $id,
        ];

        return AuthManager::unprotected(function () use ($conditions) {
            return $this->getMenuQuery()->where($conditions)->first();
        });
    }

    public function getMenuByArticleId($id)
    {
        $conditions = [
            'Articles.id' => $id,
        ];

        return AuthManager::unprotected(function () use ($conditions) {
            return $this->getMenuQuery()->where($conditions)->first();
        });
    }

    public function getMenuChildren($source, $fallback = [])
    {
        if ( is_object($source) && $source->has('menu') ) {
            $source = $source->menu;
        }

        if ( empty($source) ) {
            return $fallback;
        }

        $conditions = [
            'Menus.parent_id IS' => $source->id,
        ];

        $children = $source->child_menus;

        if ( $children === null ) {
            $children = AuthManager::unprotected(function () use ($conditions) {
                return $this->getMenuQuery()->where($conditions)->all()->toArray();
            });
        }

        $children = array_map(function ($child) {

            if ( $child->get('_locale') !== Language::getLanguage() ) {
                return null;
            }

            $article = $child->article;

            if ( empty($article) && $child->type === 'alias' ) {
                $article = $this->getArticleByMenuId($child->alias_id);
            }

            if ( empty($article) ) {
                $article = $this->getArticleByMenuId($child->id);
            }

            return $article->clone()->set('menu', $child);


        }, $children);

        return collect($children)->filter()->sortBy('menu.lft')
            ->values()->toArray();
    }

    public function getArticleNextDepth(&$article)
    {
        //Fixme, was ist mit 1024 + 1?
        if ( empty($article->menu) || $article->menu->teaser < 1024 ) {
            return;
        }

        $children = [];

        if ( empty($article->menu->child_menus) ) {
            $children = $this->getMenuChildren($article->menu);
        }

        if ( ! empty($children) ) {
            $this->runChildrenParsers($article, $children);
        }

        $article->set('children', $children);

        return $article;
    }

    public function getArticle($menu, $fallback = null)
    {
        if ( is_string($menu) ) {
            $menu = $this->getMenuById($menu);
        }

        if ( empty($menu) ) {
            return $fallback;
        }

        /** @var \App\Model\Entity\Article $article */
        $article = $this->getArticleByMenuId($menu->id, [
            'menu' => $menu,
        ]);


        if ( empty($article) ) {
            return $fallback;
        }

        $children = $this->getMenuChildren($menu);

        if ( ! empty($children) ) {
            $this->runChildrenParsers($article, $children);
        }

        array_map([$this, 'getArticleNextDepth'], $children);

        $article->set('children', $children);

        return $article;
    }

    public function getArticleView(Controller $controller, $menu): Entity
    {
        $article = $this->getArticle($menu, []);

        if ( empty($article) ) {
            throw new NotFoundException('Not found', 404);
        }

        $controller->viewBuilder()->setTemplate(
            $article->get('type', 'page') . '_' . $article->get('layout', 'default')
        );

        $controller->set('data', $article);

        return $article;
    }


}
