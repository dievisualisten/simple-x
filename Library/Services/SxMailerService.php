<?php
namespace App\Library\Services;

use Cake\Log\Log;
use Cake\Utility\Hash;
use Cake\Mailer\Mailer;
use Tools\Utility\Text;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use App\Library\Facades\Request;

class SxMailerService
{
    protected $layout;
    protected $emailFormat;
    protected $smtpUsername;
    protected $smtpPassword;
    protected $smtpHost;
    protected $smtpPort;
    protected $smtpTls;
    protected $from;
    protected $fromName;
    protected $to;
    protected $bcc;
    protected $attachments;
    protected $readReceipt;
    protected $replyTo;
    protected $returnPath;
    protected $template;
    protected $base_href;
    protected $method;
    protected $subject;
    protected $emailId;
    protected $emailConfig;
    protected $formularConfig;
    protected $formdata;
    protected $mailcontent;
    protected $log;
    protected $viewVars;
    protected $transport;
    protected $theme;
    protected $helpers;

    protected $defaults = [

        'smtpUsername' => 'systemadmin@simple-x.de',
        'smtpPassword' => '6Tdt9E47tP',
        'smtpHost' => 'smtp.simple-x.de',
        'smtpPort' => 25,
        'smtpTls' => false,

        'from' => 'systemadmin@simple-x.de',
        'fromName' => 'simple X CMS Systemadmin',
        'to' => 'systemadmin@simple-x.de',
        'bcc' => '',

        'replyTo' => null,
        'returnPath' => null,
        'readReceipt' => null,

        'log' => true,

        'method' => 'smtp',
        'transport' => 'default',
        'emailFormat' => 'html',

        'layout' => 'default', // mit Header oder blank
        'template' => 'default',
        'theme' => '',
        'helpers' => ['Html', 'Text', 'Email', 'EmailProcessing'],

        'subject' => '',
        'attachments' => [],

        'viewVars' => [],
        'mailcontent' => null,
        'base_href' => '',

        'emailId' => null,
        'emailConfig' => null,
        'formularConfig' => [],
        'formdata' => [],
    ];

    public function sendMail($config = [])
    {


        $email = $this->_getMailer($config);

        if (!$email) {
            Log::write('SxMailer', "Failed to send email, config failed");

            return false;
        }

        try {
            $email->send();

            return true;
        } catch (\Exception $e) {
            Log::write('SxMailer', "Failed to send email: " . $e->getMessage());

            return false;
        }

    }

    public function _getMailer($config = [])
    {

        //Reset, alles auf die Standardwerte
        foreach ($this->defaults as $key => $value) {
            $this->{$key} = $value;
        }

        //welche Konfig soll genommen werden

        //übergebene Config
        if (!empty($config['emailConfig'])) {
            $this->_setVarsFromEmailConfig($config['emailConfig']);
            //übergebene Email id
        } else {
            if (!empty($config['emailId'])) {
                $emailConfig = TableRegistry::getTableLocator()->get('Emails')->find()->where(['id' => $config['emailId']])->first();
                $this->_setVarsFromEmailConfig($emailConfig);
                //default
            } else {
                $emailConfig = TableRegistry::getTableLocator()->get('Emails')->find()->where(['is_default' => true])->first();
                $this->_setVarsFromEmailConfig($emailConfig);
            }
        }

        $this->_setVarsFromFormdata($config);

        $this->_setVarsFromformularConfig($config);

        //Apply Config
        foreach ($this->defaults as $key => $value) {
            if (!empty($config[$key])) {
                if ($key == 'bcc') {
                    $this->_addToBcc($config[$key]);
                } else {
                    $this->{$key} = $config[$key];
                }
            }
        }
        $this->_addToBcc(Configure::read('Sx.email.bcc'));
        if (!empty($config['bcc'])) {
            $this->_addToBcc($config['bcc']);
        }

        $this->_cleanEmails();
        $this->_removeDuplicates();

        // E-Mail Adressen validieren
        if (!$this->_validateAddresses()) {
            return false;
        }

        //http weils sonst die Bilder nicht angezeigt werden
        if (empty($this->base_href)) {
            $this->base_href = Configure::read('Sx.app.baseprotocol') . Request::domain(true);
        }

        $this->mailcontent = $this->mailcontent . ' ' . __('email.grusszeile');

        $this->mailcontent = Text::insert($this->mailcontent, [
            'base_href' => $this->base_href,
            'from_name' => $this->fromName,
            'domain' => Request::domain(true),
        ]);

        $this->subject = Text::insert($this->subject, [
            'from_name' => $this->fromName,
            'domain' => Request::domain(true),
        ]);

        if (strtolower($this->method) == 'mail') {
            Mailer::drop('default');
            Mailer::setConfig('default', [
                'className' => 'Mail',
            ]);
        }

        if (strtolower($this->method) == 'debug') {
            Mailer::drop('default');
            Mailer::setConfig('default', [
                'className' => 'Debug',
            ]);
        }

        if (strtolower($this->method) == 'smtp') {
        
            Mailer::drop('default');
            Mailer::setConfig('default', [
                'host' => $this->smtpHost,
                'port' => $this->smtpPort,
                'username' => $this->smtpUsername,
                'password' => $this->smtpPassword,
                'className' => 'Smtp',
                'tls' => $this->smtpTls,
            ]);
        }

        $this->viewVars = Hash::merge(['to' => $this->to, 'formdata' => $this->formdata, 'mailcontent' => $this->mailcontent, 'base_href' => $this->base_href],
            $this->viewVars);

        $email = new Mailer([

            'from' => [$this->from => $this->fromName],
            'to' => $this->to,
            'bcc' => $this->bcc,
            'replyTo' => $this->replyTo,
            'readReceipt' => $this->readReceipt,
            'returnPath' => $this->returnPath,
            'messageId' => true,
            'subject' => $this->subject,
            //'message': Content of message. Do not set this field if you are using rendered content.
            //'headers': Headers to be included. See Email::setHeaders().
            'template' => $this->template,
            'theme' => $this->theme,
            'layout' => $this->layout,
            'viewVars' => $this->viewVars,
            'attachments' => $this->attachments,
            'emailFormat' => $this->emailFormat,
            'transport' => $this->transport,
            'log' => $this->log,
            'helpers' => $this->helpers,
        ]);

        /*
         * When sending email on behalf of other people, it’s often a good idea to define the original sender using the Sender header
         */
        if (!empty($config['formularConfig']->email->from_email)) {
            $email->sender($config['formularConfig']->email->from_email, $config['formularConfig']->email->from_name);
        }

        return $email;
    }

    public function _setVarsFromEmailConfig($config)
    {
        if (!is_array($config)) {
            return false;
        }

        $this->smtpUsername = $config['smtp_username'];
        $this->smtpPassword = $config['smtp_password'];
        $this->smtpHost = $config['smtp_host'];
        $this->smtpPort = $config['smtp_port'];
        $this->from = $config['from_email'];
        $this->fromName = $config['from_name'];
        $this->to = $config['email_to'];
        $this->replyTo = $config['reply_to'];
        $this->returnPath = $config['return_path'];

        $this->_addToBcc($config['email_bcc']);

        return null;
    }

    public function _addToBcc($add)
    {
        if (!empty($add)) {
            if (is_string($add)) {
                $add = explode(',', (str_replace(' ', '', $add)));
            }
            $this->bcc = (!empty($this->bcc)) ? Hash::merge($this->bcc, $add) : $add;
        }
    }

    /*
     * getMailer()
     * simpleX Special E-Mail Config Fallback System ;-)
     */

    public function _setVarsFromFormdata($config)
    {
        if (empty($config['formdata'])) {
            return;
        }

        if (!empty($config['formdata']['email'])) {
            $this->replyTo = $config['formdata']['email'];
        }

        $fistname = '';

        if (!empty($config['formdata']['firstname'])) {
            $fistname = $config['formdata']['firstname'] . ' ';
        }

        if (!empty($config['formdata']['lastname'])) {
            $this->fromName = $fistname . $config['formdata']['lastname'];
        }

    }

    /*
     * added $add zu $this->>bcc
     */

    public function _setVarsFromformularConfig($config)
    {
        if (isset($config['formularConfig'])) {
            $config = $config['formularConfig'];
        }

        if (!empty($config['email_to'])) {
            $this->to = $config['email_to'];
        }

        if (!empty($config['email_bcc'])) {
            $this->_addToBcc($config['email_bcc']);
        }

        if (!empty($config['subject'])) {
            $this->subject = $config['subject'];
        }
    }

    public function _cleanEmails()
    {
        $this->from = trim($this->from);

        if (is_string($this->to)) {
            $this->to = explode(',', $this->to);
        }

        foreach ($this->to as $k => $address) {
            $this->to[$k] = trim($address);
        }

        if (is_string($this->bcc)) {
            $this->bcc = [$this->bcc];
        }

        foreach ($this->bcc as $k => $address) {
            $this->bcc[$k] = trim($address);
        }

    }

    /*
     * validate E-Mail Adresses
     * $deep Perform a deeper validation (if true), by also checking availability of host
     */

    public function _removeDuplicates()
    {
        // to
        if (is_array($this->to)) {
            $this->to = array_values(array_unique($this->to));
        }

        // bcc
        if (!empty($this->bcc)) {
            if (is_string($this->bcc)) {
                if (is_string($this->to) && $this->bcc == $this->to) {
                    $this->bcc = null;
                } else {
                    if (is_array($this->to) && in_array($this->bcc, $this->to)) {
                        $this->bcc = null;
                    }
                }
            } else {
                if (is_array($this->bcc)) {
                    $this->bcc = array_unique($this->bcc);
                    foreach ($this->bcc as $k => $bcc) {
                        if (is_string($this->to) && $bcc == $this->to) {
                            unset ($this->bcc[$k]);
                        } else {
                            if (is_array($this->to) && in_array($bcc, $this->to)) {
                                unset ($this->bcc[$k]);
                            }
                        }
                    }
                    $this->bcc = array_values($this->bcc);
                }
            }
        }
    }

    /*
     * remove Duplicates
     * within $this->>to and $this->>bcc
     */

    public function _validateAddresses()
    {

        // from
        if (!Validation::email($this->from)) {
            Log::write('SxMailer', "From not set");

            return false;
        }

        //TO
        foreach ($this->to as $k => $address) {
            if (!Validation::email($address)) {
                unset ($this->to[$k]);
            }
        }

        if (empty($this->to)) {
            Log::write('SxMailer', "TO not set");

            return false;
        }

        $this->to = array_values($this->to);

        //BCC
        foreach ($this->bcc as $k => $address) {
            if (!Validation::email($address)) {
                unset ($this->bcc[$k]);
            }
        }

        $this->bcc = array_values($this->bcc);

        return true;
    }
}
