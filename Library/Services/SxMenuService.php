<?php

namespace App\Library\Services;

use App\Library\Facades\Menu;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\Datasource\ModelAwareTrait;
use App\Utility\StringUtility;
use App\Library\Facades\Language;
use App\Library\Facades\Request;

/**
 * @version 4.0.0
 */
class SxMenuService
{
    use ModelAwareTrait;

    /**
     * @var \Cake\Http\ServerRequest
     */
    protected $request;

    /**
     * @var \App\Model\Table\MenusTable
     */
    protected $Menus;

    /**
     * @var \App\Model\Table\MenusTranslationsTable
     */
    protected $MenusTranslations;

    /**
     * @var \App\Model\Entity\Menu
     */
    public $domainRecord = null;

    /**
     * @var \App\Model\Entity\Menu
     */
    public $frontpageRecord = null;

    /**
     * @var \App\Model\Entity\Menu
     */
    public $menuRecord = null;

    /**
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * SxMenuService constructor
     *
     * @param \Cake\Http\ServerRequest $request
     */
    public function __construct(ServerRequest $request)
    {
        $this->loadModel('Menus');
        $this->loadModel('MenusTranslations');

        $this->request = $request;

        $this->initialize();
    }


    public function initialize()
    {
        if ( php_sapi_name() === 'cli' ) {
            return;
        }

        // Set domain record
        $this->_setDomainRecord();

        // Set frontpage record
        $this->_setFrontpageRecord();

    }

    protected function _setDomainRecord()
    {
        // Set language to default to get all routes
        $this->Menus->setLocale(
            Language::getDefaultLanguage()
        );

        $query = $this->Menus->find()
            ->where([
                'Menus.type' => 'domain',
                'Menus.path' => Request::domain(true),
            ])
            ->contain([
                'Acos', 'Emails',
            ]);

        // Set language back to origin
        $this->Menus->setLocale(
            Language::getLanguage()
        );

        $domainRecord = $query->first();

        if ( ! Configure::check('domainRecord') ) {
            Configure::write('domainRecord', $domainRecord);
        }

        return $this->domainRecord = $domainRecord;
    }

    protected function _setMenuRecord()
    {
        // Get domain and path
        $currentRoute = Request::domain(true) .
            Request::path();

        $query = $this->Menus->find('published')
            ->where([
                'Menus.model' => 'Article',

                // Hide unnecessary items
                'Menus.teaser NOT IN' => [
                    1024, 2048, 4096, 8192,
                ],

                // Hide unnecessary items
                'Menus.type NOT IN' => [
                    'domain', 'menu', 'alias', // 'teaserpage',
                ],

            ])
            ->contain([
                'Articles.Formularconfigs.Formulars',
                'Articles.Formularconfigs.Emails',
            ]);

        $data = null;

        if ( Language::isDefaultLanguage() ) {

            $menuRecord = $query->where([
                'Menus.path' => $currentRoute,
            ])->first();

            if ( empty($menuRecord) ) {
                return false;
            }

            foreach ( Language::getAllowedLanguages() as $locale ) {
                Language::inLanguage($locale, function () use ($menuRecord, $locale) {

                    $this->Menus->setLocale($locale);

                    $conditions = [
                        'Menus.id' => $menuRecord->id,
                    ];

                    $translation = $this->Menus->find()
                        ->where($conditions)->first();

                    if ( $translation->get('_locale') !== $locale ) {
                        $translation->set('path', $this->getHomeUrl(true, $locale));
                    }

                    $menuRecord->set("locales.{$locale}", $translation);
                });
            }

            $this->Menus->setLocale(null);

            if ( ! Configure::check('menuRecord') ) {
                Configure::write('menuRecord', $menuRecord);
            }

            return $this->menuRecord = $menuRecord;
        }

        $translations = $this->MenusTranslations->find()
            ->select([
                'id',
            ])
            ->where([
                'MenusTranslations.path' => $currentRoute,
                'MenusTranslations.locale' => Language::getLanguage(),
            ]);

        if ( $translations->count() !== 0 ) {

            $menuRecord = $query->andWhere([
                'Menus.id IN' => $translations->extract('id')->toArray(),
            ])->first();

            if ( empty($menuRecord) ) {
                return false;
            }

            foreach ( Language::getAllowedLanguages() as $locale ) {
                Language::inLanguage($locale, function () use ($menuRecord, $locale) {

                    $this->Menus->setLocale($locale);

                    $conditions = [
                        'Menus.id' => $menuRecord->id,
                    ];

                    $translation = $this->Menus->find()
                        ->where($conditions)->first();

                    if ( $translation->get('_locale') !== $locale ) {
                        $translation->set('path', $this->getHomeUrl(true, $locale));
                    }

                    $menuRecord->set("locales.{$locale}", $translation);
                });
            }

            $this->Menus->setLocale(null);

            if ( ! Configure::check('menuRecord') ) {
                Configure::write('menuRecord', $menuRecord);
            }


            return $this->menuRecord = $menuRecord;
        }


        return false;
    }

    protected function _setFrontpageRecord()
    {
        if ( empty($this->domainRecord) ) {
            return false;
        }

        $query = $this->Menus->find();

        $conditions = [
            'Menus.parent_id' => $this->domainRecord->id,
            'Menus.type' => 'frontpage',
        ];

        $frontpageRecord = $query->where($conditions)->first();

        if ( empty($frontpageRecord) ) {
            return false;
        }

        foreach ( Language::getAllowedLanguages() as $locale ) {
            Language::inLanguage($locale, function () use ($frontpageRecord, $locale) {

                $this->Menus->setLocale($locale);

                $conditions = [
                    'Menus.id' => $frontpageRecord->id,
                ];

                $translation = $this->Menus->find()
                    ->where($conditions)->first();

                if ( $translation->get('_locale') !== $locale ) {
                    $translation->set('path', $this->getHomeUrl(true, $locale));
                }

                $frontpageRecord->set("locales.{$locale}", $translation);
            });
        }

        $this->Menus->setLocale(null);

        if ( ! Configure::check('frontpageRecord') ) {
            Configure::write('frontpageRecord', $frontpageRecord);
        }

        $this->frontpageRecord = $frontpageRecord;
    }

    protected function _setBreadcrumbs()
    {
        $breadcrumbs = $this->Menus
            ->find('path', [
                'for' => $this->menuRecord->get('id'),
            ])
            ->contain([
                'Articles.Formularconfigs.Formulars',
            ]);

        // Unterhalb einer Loginseite? -> Mitteilung an den app_controller
        foreach ( $breadcrumbs->toArray() as $key => $menu ) {

            /** @var \App\Model\Entity\Menu $menu */

            if ( in_array($menu->get('type'), ['domain', 'menu', 'alias']) ) {
                continue;
            }

            if ( in_array($menu->get('teaser'), TEASER_WITHOUT_URL) ) {
                continue;
            }

            //fixMe brauchen wir nicht mehr, ist in der formualrconfig schon raus
            $articleIsLogin = ($key < $breadcrumbs->count() - 1) &&
                $menu->get('article.formularconfig.formular.use_login', false) == '1';

            if ( $articleIsLogin ) {
                Configure::write('Menu.loginPage', $menu->get('Menu.path'));
            }

            $this->breadcrumbs[] = $menu;
        }

        return $this->breadcrumbs;
    }

    public function getRouteParams()
    {
        // Return if is no html request
        if ( ! Request::is('html') ) {
            return null;
        }

        // Is current lang same as default
        $languageIsDefault = language()
            ->isDefaultLanguage();

        // Is current lang same as route
        $languageIsRoute = language()
            ->hasRouteLanguage();

        // Redirect if language is same as default language
        if ( $languageIsDefault && $languageIsRoute && ! $this->request->is('post') ) {

            header('HTTP/1.1 301 Moved Permanently');

            $redirectUrl = Language::normalizeUrl(
                Request::fullUrl(false)
            );

            header("Location:" . $redirectUrl);
            exit();
        }

        /** @var \App\Model\Entity\Menu $menuRecord */
        $menuRecord = $this->_setMenuRecord();

        if ( $menuRecord === false ) {
            return null;
        }

        // Build breadcrumbs for menu record
        $this->_setBreadcrumbs();

        $params = [
            'controller' => StringUtility::grizzly(
                $menuRecord->get('controller')
            ),
            'action' => StringUtility::lower(
                $menuRecord->get('dispatcher', 'dispatchview')
            ),
            'pass' => [
                $menuRecord->get('foreign_key'),
                $menuRecord->get('id'),
                $menuRecord->get('action'),
                $menuRecord->get('params'),
            ],
            'ext' => Request::ext(),
        ];

        return $params;
    }

    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    public function getDomainRecord($key = null, $fallback = null)
    {
        if ( empty($this->domainRecord) ) {
            return $fallback;
        }

        if ( $key ) {
            return $this->domainRecord->get($key, $fallback);
        }

        return $this->domainRecord;
    }

    public function getFrontpageRecord($key = null, $fallback = null)
    {
        if ( empty($this->frontpageRecord) ) {
            return $fallback;
        }

        if ( $key ) {
            return $this->frontpageRecord->get($key, $fallback);
        }

        return $this->frontpageRecord;
    }

    public function getMenuRecord($key = null, $fallback = null)
    {
        if ( empty($this->menuRecord) ) {
            return $fallback;
        }

        if ( $key ) {
            return $this->menuRecord->get($key, $fallback);
        }

        return $this->menuRecord;
    }

    public function isHome()
    {
        if ( empty($this->menuRecord) ) {
            return false;
        }

        return $this->menuRecord->get('type') === 'frontpage';
    }

    public function isHomeUrl($url = '', $adjustLocal = false)
    {
        return preg_replace('/^https?:\/\//', '', $this->getHomeUrl($adjustLocal)) ==
            preg_replace('/^https?:\/\//', '', $url);
    }

    public function getHomeUrl($useDev = false, $locale = null)
    {
        if ( Language::isDefaultLanguage($locale) ) {
            return Request::fullDomain(false, $useDev);
        }

        return Request::fullDomain(false, $useDev) . '/' . ($locale ?: Language::getLanguage());
    }

    public function getMenuVisibility($number)
    {
        $rechte = [];

        for ( $i = 20; $i >= 0; $i-- ) {
            $value = pow(2, $i);
            if ( $number >= $value ) {
                $rechte[] = $value;
                $number -= $value;
            }
        }

        return $rechte;
    }

}
