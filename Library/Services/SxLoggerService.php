<?php

namespace App\Library\Services;

use Cake\Log\Log;

/**
 * @version 4.0.0
 */
class SxLoggerService
{
    /**
     * @var array
     */
    protected $logs = [];

    /**
     * Start logging.
     */
    public function start()
    {
        $this->logs = [];
    }

    /**
     * Output logging.
     *
     * @return string
     */
    public function stop()
    {
        return implode("\n", $this->logs);
    }

    /**
     * Logging fast.
     *
     * @param string $message
     * @param string $file
     */
    public function log($message, $file = 'log')
    {
        $this->makeConfig($file, [$file]);

        $this->write(LOG_INFO, print_r($message, true), $file);
    }

    /**
     * If config does exists.
     *
     * @param string $file
     * @return bool
     */
    public function hasConfig($file)
    {
        return Log::getConfig($file) !== null;
    }

    /**
     * Create new config if not exists.
     *
     * @param $file
     * @param array $scopes
     */
    public function makeConfig($file, $scopes = ['*'])
    {
        if ( $this->hasConfig($file) === true ) {
            return;
        }

        Log::setConfig($file, [
            'className' => 'File',
            'path' => LOGS,
            'file' => $file,
            'scopes' => $scopes,
        ]);
    }

    /**
     * Drop config if exists.
     *
     * @param $file
     */
    public function dropConfig($file)
    {
        if ( $this->hasConfig($file) === false ) {
            return;
        }

        Log::drop($file);
    }

    /**
     * Write to log.
     *
     * @param string $level
     * @param string $message
     * @param array $scopes
     * @param array $context
     */
    public function write($level, $message, $scopes = [], $context = [])
    {
        if ( empty($scopes) ) {
            $scopes = ['*'];
        }

        // Set scope if not isset.
        $scopes = array_merge([
            php_sapi_name() === 'cli' ? 'cli' : 'web',
        ], (array) $scopes);

        // Merge context with scope.
        $context = array_merge([
            'scope' => (array) $scopes,
        ], $context);

        // Write to internal log.
        $this->logs[] = date('Y-m-d H:i:s') . ' ' . ucfirst($level) . ': ' . print_r($message, true);

        // Write log.
        Log::write($level, print_r($message, true), $context);
    }

    /**
     * Write log to file.
     *
     * @param $level
     * @param $message
     * @param $file
     */
    public function writeFile($level, $message, $file)
    {
        $this->makeConfig($file, [$file]);

        $this->write($level, print_r($message, true), $file);
    }

    /**
     * Logging for debug.
     *
     * @param string $message
     * @param array $scopes
     */
    public function debug($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for debug.
     *
     * @param string $message
     * @param string $file
     */
    public function debugFile($message, $file = '')
    {
        return $this->writeFile('debug', print_r($message, true), $file);
    }

    /**
     * Logging for info.
     *
     * @param string $message
     * @param array $scopes
     */
    public function info($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for info.
     *
     * @param string $message
     * @param string $file
     */
    public function infoFile($message, $file = '')
    {
        return $this->writeFile('info', print_r($message, true), $file);
    }

    /**
     * Logging for notice.
     *
     * @param string $message
     * @param array $scopes
     */
    public function notice($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for notice.
     *
     * @param string $message
     * @param string $file
     */
    public function noticeFile($message, $file = '')
    {
        return $this->writeFile('notice', print_r($message, true), $file);
    }

    /**
     * Logging for alert.
     *
     * @param string $message
     * @param array $scopes
     */
    public function alert($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for alert.
     *
     * @param string $message
     * @param string $file
     */
    public function alertFile($message, $file = '')
    {
        return $this->writeFile('alert', print_r($message, true), $file);
    }

    /**
     * Logging for warning.
     *
     * @param string $message
     * @param array $scopes
     */
    public function warning($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for warning.
     *
     * @param string $message
     * @param string $file
     */
    public function warningFile($message, $file = '')
    {
        return $this->writeFile('warning', print_r($message, true), $file);
    }

    /**
     * Logging for error.
     *
     * @param string $message
     * @param array $scopes
     */
    public function error($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for error.
     *
     * @param string $message
     * @param string $file
     */
    public function errorFile($message, $file = '')
    {
        return $this->writeFile('error', print_r($message, true), $file);
    }

    /**
     * Logging for critical.
     *
     * @param string $message
     * @param array $scopes
     */
    public function critical($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for critical.
     *
     * @param string $message
     * @param string $file
     */
    public function criticalFile($message, $file = '')
    {
        return $this->writeFile('critical', print_r($message, true), $file);
    }

    /**
     * Logging for emergency.
     *
     * @param string $message
     * @param array $scopes
     */
    public function emergency($message, $scopes = [])
    {
        return $this->write(__FUNCTION__, print_r($message, true), $scopes);
    }

    /**
     * Logging for emergency.
     *
     * @param string $message
     * @param string $file
     */
    public function emergencyFile($message, $file = '')
    {
        return $this->writeFile('emergency', print_r($message, true), $file);
    }

}
