<?php

namespace App\Library\Services;

use Cake\Http\ServerRequest;

/**
 * @version 4.0.0
 */

class SxRequestService
{
    /**
     * @var \Cake\Http\ServerRequest $request
     */
    protected $request;

    /**
     * Store request in instance.
     *
     * @param \Cake\Http\ServerRequest $request
     */
    public function __construct(ServerRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Return request.
     *
     * @return \Cake\Http\ServerRequest
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * Return session.
     *
     * @return \Cake\Http\Session
     */
    public function session()
    {
        return $this->request->getSession();
    }

    /**
     * Get current protocol.
     *
     * @param bool $forceSecure
     * @return string
     */
    public function protocol($forceSecure = false)
    {
        $protocol = config('Sx.app.baseprotocol', 'http');

        if ( $forceSecure === true ) {
            $protocol = 'https';
        }

        return preg_replace('/:\/\/$/', '', $protocol ?:
            $this->request->scheme());
    }

    /**
     * Get current schema.
     *
     * @param bool $forceSecure
     * @return string
     */
    public function scheme($forceSecure = false)
    {
        return $this->protocol($forceSecure) . '://';
    }

    /**
     * Get current domain.
     *
     * @param bool $useDev
     * @return mixed
     */
    public function domain($useDev = false)
    {
        $domain = ! empty($_SERVER['SERVER_NAME']) ?
            $_SERVER['SERVER_NAME'] : config('App.fullBaseUrl');

        if ( config('env') === 'local' && $useDev === false ) {
            $domain = config('localDomain');
        }

        if ( config('env') === 'local' && $useDev === true ) {
            $domain = config('liveDomain');
        }

        return rtrim($domain, '/');
    }

    /**
     * Get current path.
     *
     * @param bool $withExtension
     * @param bool $withParams
     * @return mixed
     */
    public function path($withExtension = true, $withParams = false)
    {
        $path = $this->request->getPath();

        if ( ! $withExtension ) {
            $path = preg_replace('/(\.[a-z0-9]+)?$/i', '', $path);
        }

        if ( ! $withParams ) {
            $path = preg_replace('/\/([^\/]+:[^\/]+)(?=\/|$)/', '', $path);
        }

        return $path === '/' ? '' : $path;
    }

    /**
     * Get url without query and scheme.
     *
     * @param bool $useDev
     * @param bool $withExt
     * @return string
     */
    public function url($useDev = false, $withExt = true)
    {
        return $this->domain($useDev) . $this->path($withExt);
    }

    /**
     * Get query from request.
     *
     * @param array $override
     * @param bool $source
     * @return array
     */
    public function query($override = [], $source = true)
    {
        return array_merge($source ? $this->request->getQuery() :
            [], $override);
    }

    /**
     * Get querystring from request.
     *
     * @param array $override
     * @param bool $source
     * @return string
     */
    public function queryString($override = [], $source = true)
    {
        return http_build_query($this->query($override, $source));
    }


    /**
     * Get whole domain including protocol.
     *
     * @param bool $forceSecure
     * @param bool $useDev
     * @return string
     */
    public function fullDomain($forceSecure = false, $useDev = false)
    {
        return $this->scheme($forceSecure) . $this->domain($useDev);
    }

    /**
     * Return current full url.
     *
     * @param bool $forceSecure
     * @param bool $useDev
     * @param bool $withExtension
     * @return string
     */
    public function fullPath($forceSecure = false, $useDev = false, $withExtension = true)
    {
        return $this->fullDomain($forceSecure, $useDev) . $this->path($withExtension);
    }

    /**
     * Return current full url with query.
     *
     * @param bool $forceSecure
     * @param bool $useDev
     * @param bool $withExtension
     * @return string
     */
    public function fullUrl($forceSecure = false, $useDev = false, $withExtension = true)
    {
        $queryString = $this->queryString();

        return $this->fullPath($forceSecure, $useDev, $withExtension) .
            ($queryString ? '?' . $queryString : '');
    }

    /**
     * Returns route extenstion
     *
     * @param mixed $fallback
     * @return string
     */
    public function ext($fallback = 'html')
    {
        preg_match('/^.*?\.([a-z0-9]+)$/i', $this->path(), $matches);

        return count($matches) === 2 ? $matches[1] : $fallback;
    }

    /**
     * Get boolean if route matches type (incl. file extension).
     *
     * @param string|array $types
     * @return bool
     */
    public function is($types)
    {
        $types = ! is_array($types) ? preg_split('/\s*,\s*/', $types) : $types;

        foreach ( $types as $type ) {
            if ( $this->request->is($type) || $this->ext() === $type ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Compares request method
     *
     * @param string $type
     * @return bool
     */
    public function isMethod($type = 'get')
    {
        return strtolower($type) === strtolower($this->request->getMethod());
    }

    /**
     * Get all segments from url
     *
     * @param bool $withLocale
     * @param bool $withExt
     * @return array
     * @throws \Exception
     */
    public function segments($withLocale = false, $withExt = false)
    {
        preg_match_all('/(?<=\/|^)[^\/]+(?=\/|$)+/', $this->path($withExt), $matches);

        $segments = @$matches[0] ?: [];

        if ( ! $withLocale && isset($segments[0]) && preg_match('/^[a-z]{2}$/', $segments[0]) ) {
            array_shift($segments);
        }

        return $segments;
    }

    /**
     * Get specific segment from url
     *
     * @param int $index
     * @param mixed $fallback
     * @param bool $withLocale
     * @return mixed
     * @throws \Exception
     */
    public function segment($index = 0, $fallback = null, $withLocale = false)
    {
        return data_get($this->segments($withLocale), $index, $fallback);
    }

    /**
     * Get all params from url
     *
     * @return array
     * @throws \Exception
     */
    public function params()
    {
        preg_match_all('/(?<=^|\/)[^\/]+:[^\/]+(?=\/|$)/', $this->path(false, true), $matches);

        $params = @$matches[0] ?: [];

        return array_reduce($params, function($merge, $param) {
            return array_merge($merge, [explode(':', $param)[0] => explode(':', $param)[1]]);
        }, []);
    }

    /**
     * Get specific param from url
     *
     * @param int $index
     * @param mixed $fallback
     * @return mixed
     * @throws \Exception
     */
    public function param($index, $fallback = null)
    {
        return data_get($this->params(), $index, $fallback);
    }

    /**
     * Return current url with params
     *
     * @param array $params
     * @return string
     */
    public function getCurrent($params = [])
    {
        $url = rtrim($this->fullUrl(), '/');

        foreach ( $params as $key => $param ) {
            $params[$key] = $key . ':' . implode(',', (array) $param);
        }

        if ( count($params) ) {
            $url .= '/' . implode('/', $params);
        }

        return $url;
    }

    /**
     * Get contextual domain
     *
     * @param string $domain
     * @return string
     */
    public function getContextDomain($domain)
    {
        if ( config('env') === 'local' ) {
            $domain = str_replace(config('liveDomain'), config('localDomain'), $domain);
        }

        return $domain;
    }

    /**
     * Extract domain from path with context
     *
     * @param mixed $path
     * @param bool $forceSecure
     * @return string
     */
    public function getDomainFromPath($path, $forceSecure = false)
    {
        if ( ! is_string($path) ) {
            $path = is_object($path) ? $path->path : $path['path'];
        }

        preg_match('/^(https?:\/\/)?[^\/]+/', $path, $matches);

        return $this->protocol($forceSecure) . $this->getContextDomain($matches[0]);
    }

}
