<?php

namespace App\Library\Services;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\Http\ServerRequest;
use Cake\I18n\I18n;

/**
 * @version 4.0.0
 */

class SxLanguageService
{
    use EventDispatcherTrait;

    /**
     * @var \Cake\Http\ServerRequest $request
     */
    protected $request;

    /**
     * @var string $language
     */
    public $language = 'en';

    /**
     * @var string $defaultLanguage
     */
    public $defaultLanguage = 'en';

    /**
     * @var array $allowedLanguages
     */
    public $allowedLanguages = [];

    /**
     * @var string $userDefaultLanguage
     */
    public $userLanguage = '';

    /**
     * @var string $routeLanguage
     */
    public $routeLanguage = '';

    /**
     * SxLanguageService constructor
     *
     * @param \Cake\Http\ServerRequest $request
     */
    public function __construct(ServerRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Setup service
     */
    public function setupService()
    {
        // Get defaults from config
        $this->defaultLanguage = (string) config('Sx.app.language.defaultlanguage');
        $this->allowedLanguages = (array) config('Sx.app.language.accept');

        // Get HTTP request language
        $this->_setUserLanguage();

        // Get route language
        $this->_setRouteLanguage();

        // Bind event to trigger language change
        $this->getEventManager()->on('App.Language.updateLanguage', [
            $this, '_updateLanguage'
        ]);

        // Define current language
        $language = $this->defaultLanguage;

        if ( $this->routeLanguage && $language !== $this->routeLanguage ) {
            $language = $this->routeLanguage;
        }

        $this->setLanguage($language);
    }

    /**
     * Set route language
     */
    protected function _setRouteLanguage()
    {
        $segments = request()->segments(true);

        // Find matching locales in segments
        $languages = array_intersect($this->allowedLanguages, $segments);

        // Set route language
        $this->routeLanguage = reset($languages) ?: '';
    }

    /**
     * Set HTTP request language
     */
    protected function _setUserLanguage()
    {
        $http = @$_SERVER['HTTP_ACCEPT_LANGUAGE'] ?: '';
        preg_match('/(?<=^|,)[a-z]{2}(?=;|,|$)/', $http, $accepted);

        // Find matching locales in segments
        $languages = array_intersect($this->allowedLanguages, $accepted);

        // Set user language
        $this->userLanguage = reset($languages) ?: '';
    }

    /**
     * Define language
     *
     * @param $language
     */
    public function setLanguage($language = null)
    {
        $this->dispatchEvent('App.Language.updateLanguage', [
            $this->language = $language ?: $this->defaultLanguage
        ]);
    }

    /**
     * Update language Event
     *
     * @param \Cake\Event\Event $event
     * @param $language
     */
    public function _updateLanguage(Event $event, $language)
    {
        // Define locale in translations
        I18n::setLocale($language);

        // Update cake language
        Configure::write('Config.language', $language);

        // Update cms language
        Configure::write('Sx.Config.language', $language);
    }

    /**
     * Return current language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Returns default language
     *
     * @return string
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * Returns true if default language is same as current
     *
     * @var string $language
     * @return bool
     */
    public function isDefaultLanguage($language = null)
    {
        return $this->defaultLanguage === ($language ?: $this->language);
    }

    /**
     * Returns allowed languages
     *
     * @return array
     */
    public function getAllowedLanguages()
    {
        return $this->allowedLanguages;
    }

    /**
     * Test if language is allowed
     *
     * @param $language
     * @return bool
     */
    public function isAllowedLanguage($language = null)
    {
        return in_array($language ?: $this->language, $this->allowedLanguages);
    }

    /**
     * Returns HTTP request language
     *
     * @return string
     */
    public function getUserLanguage()
    {
        return $this->userLanguage;
    }

    /**
     * Returns true if HTTP request language isset
     *
     * @return bool
     */
    public function hasUserLanguage()
    {
        return $this->userLanguage !== '';
    }

    /**
     * Returns route language
     *
     * @return string
     */
    public function getRouteLanguage()
    {
        return $this->routeLanguage;
    }

    /**
     * Returns true if route language isset
     *
     * @return bool
     */
    public function hasRouteLanguage()
    {
        return $this->routeLanguage !== '';
    }

    /**
     * Run scripts in language
     *
     * @param string $language
     * @param \Closure $callback
     * @return mixed
     */
    public function inLanguage($language, $callback)
    {
        if ( ! $language ) {
            return call_user_func($callback);
        }

        $currentLanguage = $this->getLanguage();
        $this->setLanguage($language);

        $result = call_user_func($callback);

        $this->setLanguage($currentLanguage);

        return $result;
    }

    /**
     * Run scripts in language
     *
     * @param \Closure $callback
     * @return mixed
     */
    public function inDefaultLanguage($callback)
    {
        return $this->inLanguage($this->defaultLanguage, $callback);
    }

    /**
     * Remove default locale from url
     *
     * @param string $url
     * @return string
     */
    public function normalizeUrl($url)
    {
        if (endsWith($url,'/' . $this->defaultLanguage)){
            $url = str_replace('/' . $this->defaultLanguage,'',$url);
        }

        $pattern = '#(/' . preg_quote($this->defaultLanguage, '#') . '/)#';

        return preg_replace($pattern, '/', $url);
    }

}
