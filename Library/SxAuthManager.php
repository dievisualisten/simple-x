<?php

namespace App\Library;

use Authentication\Authenticator\UnauthenticatedException;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Controller\Component;
use Cake\Utility\Hash;

/**
 * @version 4.0.0
 */
class SxAuthManager
{
    /**
     * @var \App\Controller\Component\Authentication\AuthenticationComponent
     */
    protected static $_Component = null;

    /**
     * @var \Cake\Http\ServerRequest
     */
    protected static $_Request = null;

    /**
     * @var int Paused counter
     */
    protected static $_paused = 0;

    /**
     * @var bool Active state
     */
    protected static $_active = false;

    /**
     * Initialize library
     *
     * @param $request
     * @param bool $active
     * @return true;
     */
    public static function initialize($request, $active = true)
    {
        self::$_Request = $request;

        if ( empty($request) ) {
            return false;
        }

        self::$_active = $active;

        return true;
    }

    /**
     * Return request
     *
     * @return \Cake\Http\ServerRequest
     */
    public static function getRequest()
    {
        return self::$_Request;
    }

    /**
     * Set auth component
     *
     * @param \Cake\Controller\Component $component
     */
    public static function setComponent(Component $component)
    {
        self::$_Component = $component;
    }

    /**
     * @return \App\Controller\Component\Authentication\AuthenticationComponent
     */
    public static function getComponent()
    {
        if ( self::$_Component === null ) {
            throw new Exception('Authentication.AuthenticationComponent not boooted');
        }

        return self::$_Component;
    }

    /**
     * Get controller from component
     *
     * @return \Cake\Controller\Controller
     */
    public static function getController()
    {
        return self::getComponent()->getController();
    }

    /**
     * Get session from request
     *
     * @return \Cake\Http\Session
     */
    public static function getSession()
    {
        if ( ! self::getRequest() ) {
            return null;
        }

        return self::getRequest()->getSession();
    }

    /**
     * Returns active state
     *
     * @return bool
     */
    public static function isActive()
    {
        return self::$_active === true && self::$_paused === 0;
    }

    /**
     * Clears paused count and force activates authentication
     */
    public static function start()
    {
        self::$_paused = 0;
        self::$_active = true;
    }

    /**
     * Clears paused count and force deactivates authentication
     */
    public static function stop()
    {
        self::$_paused = 0;
        self::$_active = false;
    }

    /**
     * Increase pause count
     */
    public static function pause()
    {
        self::$_paused++;
    }

    /**
     * Reduce pause count
     */
    public static function resume()
    {
        self::$_paused = self::$_paused === 0 ?
            self::$_paused : self::$_paused - 1;
    }

    /**
     * Call function in protected mode
     *
     * @param $callback
     * @return mixed
     */
    public static function protected($callback)
    {
        $paused = self::$_paused;
        $active = self::$_active;

        self::$_paused = 0;
        self::$_active = true;

        $result = call_user_func($callback);

        self::$_paused = $paused;
        self::$_active = $active;

        return $result;
    }

    /**
     * Call function in protected mode
     *
     * @param $callback
     * @return mixed
     */
    public static function unprotected($callback)
    {
        $paused = self::$_paused;
        $active = self::$_active;

        self::$_paused = 0;
        self::$_active = false;

        $result = call_user_func($callback);

        self::$_paused = $paused;
        self::$_active = $active;

        return $result;
    }

    /**
     * Caches user into session
     *
     * @param null $currentAcoId
     * @param null $authConfigKey
     */
    public static function cacheUser($currentAcoId = null, $authConfigKey = null)
    {
        $authConfig = null;

        if ( empty($authConfigKey) ) {
            $authConfig = self::getAuthConfig();
        }
        self::unprotected(function () use ($currentAcoId, $authConfigKey, $authConfig) {

            /* @var $users \App\Model\Table\UsersTable */
            $users = TableRegistry::getTableLocator()->get('Users');

            $id = self::getController()->Authentication
                ->getIdentityData('id');

            self::getSession()->write('AuthManager',
                $users->getUser($id, $currentAcoId));

            if ( $authConfigKey ) {
                self::setAuthConfigByKey($authConfigKey);
            }

            if ( $authConfig ) {
                self::setAuthConfig($authConfig);
            }
        });
    }


    /**
     * Return user object or property in user
     *
     * @param null $key
     * @param null $fallback
     * @return mixed
     */
    public static function getUser($key = null, $fallback = null)
    {
        if ( self::isActive() === false ) {
            return $fallback;
        }

        $data = self::getSession()->read('AuthManager.user');

        if ( $key === null ) {
            return $data;
        }

        return array_get($data, $key, $fallback);
    }


    /**
     * @param null $authConfigKey
     * @throws \Exception
     */
    public static function setAuthConfigByKey($authConfigKey = null)
    {
        self::setAuthConfig(config('Sx.auth.' . str_replace('auth.', '', $authConfigKey)));
    }


    /**
     * @param array $authConfig
     * @throws \Exception
     */
    public static function setAuthConfig($authConfig = [])
    {
        $defaultAuthConfig = [
            'credentials' => [
                'username' => [
                    'login',
                ],
                //'active' => true,
            ],

            'logout_url' => '/logout',
            'login_url' => '/login',

            'send_new_password_url' => '/users/sendnewpassword',
            'must_change_new_pw' => false,

            'after_login_redirect_url' => '/' . language()->getLanguage(),
            'after_logout_redirect_url' => '/' . language()->getLanguage(),

            'error_msg' => __('Es konnte kein Benutzer zu den angegebenen Daten gefunden werden.'),
            'success_msg' => __('Willkommen zurück'),
        ];

        $authConfig = Hash::merge($defaultAuthConfig, $authConfig);

        self::getSession()->write('AuthManager.authconfig', $authConfig);
    }

    /**
     * Returns the current authconfig
     *
     * @return array|null
     */
    public static function getAuthConfig($key = null, $fallback = null)
    {

        $data = self::getSession()->read('AuthManager.authconfig');

        if ( $key === null ) {
            return $data;
        }

        return array_get($data, $key, $fallback);
    }

    /**
     * Return perms object or property in perms
     *
     * @param null $key
     * @param null $fallback
     * @return mixed
     */
    public static function getPerms($key = null, $fallback = null)
    {
        if ( self::isActive() === false ) {
            return $fallback;
        }

        $data = self::getSession()->read('AuthManager.perms');

        if ( $key === null ) {
            return $data;
        }

        return array_get($data, $key, $fallback);
    }

    /**
     * Return auth object or property in auth
     *
     * @param null $key
     * @param null $fallback
     * @return mixed
     */
    public static function get($key = null, $fallback = null)
    {
        if ( self::isActive() === false ) {
            return $fallback;
        }

        $data = self::getSession()->read('AuthManager');

        if ( $key === null ) {
            return $data;
        }

        return array_get($data, $key, $fallback);
    }

    /**
     * Return aco ids for current user
     *
     * @param $model
     * @param array $fallback
     * @return array
     */
    public static function getAcoIds($model, $fallback = ['notAllowed'])
    {
        if ( self::isActive() === false ) {
            return $fallback;
        }

        $data = self::getPerms("models.{$model}.acoIds");

        return empty($data) ? $fallback : (array) $data;
    }

    /**
     * Get base access level for current user
     *
     * @param null $fallback
     * @return mixed
     */
    public static function getBaseAccessLevel($fallback = null)
    {
        return self::getUser('Baseaco.level', $fallback);
    }

    /**
     * Get access level of current user for model
     *
     * @param $model
     * @return int
     */
    public static function getAccessLevel($model)
    {
        $data = self::getPerms("models.{$model}.accessLevel");

        return $data === null ? 100000000 : $data;
    }

    /**
     * Get authorized actions for a class
     *
     * @param string $controller
     * @return array
     * @throws \Exception
     */
    public static function getAuthorized($controller)
    {
        $result = [];

        // Normalize controller
        $controller = strtolower($controller);

        if ( ! self::isUrlAllowed() ) {
            throw new UnauthenticatedException();
        }

        // Get allowed controllers from session
        $controllers = self::getPerms('controllers');

        // Get allowed actions from session
        $actions = self::getPerms('actions');

        if ( ! data_get($controllers, $controller, false) ) {
            return $result;
        }

        $allowed = array_keys(data_get($actions, $controller));

        if ( in_array('*', $allowed) ) {
            $result[] = self::getRequest()->getParam('action');
        }

        return array_merge($allowed, $result);
    }

    /**
     * @param \Cake\Http\ServerRequest $request
     * @return array
     * @throws \Exception
     */
    public static function getAuthorizedByRequest($request)
    {
        return self::getAuthorized($request->getParam('controller'));
    }

    /**
     * Test if controller and action is allowed
     *
     * @param $controller
     * @param $action
     * @return bool
     * @throws \Exception
     */
    public static function isAuthorized($controller, $action)
    {
        // Normalize action
        $action = strtolower($action);

        return in_array($action, self::getAuthorized($controller));
    }

    /**
     * Test if url is allowed
     *
     * @return bool
     * @throws \Exception
     */
    public static function isUrlAllowed($compareUrl = null)
    {
        if ( ! $compareUrl ) {
            $compareUrl = request()->url(false);
        }

        $deniedUrls = self::getPerms('urls.deniedUrls');


        if ( ! self::getUser() ) {
            $deniedUrls = config('SxResources');
        }

        $matchedUrls = array_filter($deniedUrls, function ($url) use ($compareUrl) {

            if ( preg_match('/^\//', $url) ) {
                $url = str_join('/', request()->domain(false), $url);
            }

            // Prepare Url for comparison
            $url = preg_quote(trim($url, '/'), '/');

            return preg_match('/^' . str_replace('\\*', '(.*?)', $url) .
                '\/?(.*?)$/', $compareUrl);
        });

        return ! count($matchedUrls);
    }

    /**
     * Extend query to only find allowed entries
     *
     * @param \App\Model\Table\AppTable $table
     * @param \Cake\ORM\Query|boolean $query
     * @return \Cake\ORM\Query|boolean
     */
    public static function getConditions($table, $query)
    {
        if ( config('debug') && isset($_GET['SXTEST']) ) {
            return $query;
        }

        if ( $table->getTable() === 'roles' ) {
            return self::getRoleConditions($table, $query);
        }

        if ( self::isActive() === false || $table->hasField('aco_id') === false ) {
            return $query;
        }

        $tableName = strtolower(
            Inflector::singularize($table->getAlias())
        );

        $permissions = self::getPerms("models.$tableName");

        if ( empty($permissions) ) {
            // Keine Ergebnisse, quasi Abruch der Query
            return $query->where([$table->getAlias() . '.aco_id IN' => ['notAllowed']]);
        }

        if ( $permissions['accessLevel'] > 0 ) {
            // 0 = Systemadmin (dann kein Check), sonst werden Datensätze, die keinem Aco mehr zugeordnet sind, nicht mehr angezeigt
            $query->where([$table->getAlias() . '.aco_id IN' => (array) $permissions['acoIds']]);
        }

        return $query;
    }

    public static function getRoleConditions($table, $query)
    {
        $tableName = strtolower(
            Inflector::singularize($table->getAlias())
        );

        $permissions = self::getAccessLevel('role');

        $query->where([
            $table->getAlias() . '.aco_accesslevel >=' => $permissions
        ]);
    }

    /**
     * Call login function in component
     *
     * @return bool
     */
    public static function login()
    {
        return self::getComponent()->login();
    }

    /**
     *
     * @return bool
     */
    public static function clearSession()
    {
        return self::getSession()->delete('AuthManager');
    }

}
