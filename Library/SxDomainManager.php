<?php

namespace App\Library;

use App\Utility\StringUtility;
use App\Library\Facades\Language;

/**
 * @version 4.0.0
 */

class SxDomainManager
{
    /**
     * Get from item or domain from texts
     *
     * @param $type
     * @param null|string $key
     * @param null|string $language
     * @return mixed
     */
    public static function get($type, $key = null, $language = null)
    {
        // Normalize type
        $type = strtolower($type);

        return Language::inLanguage($language, function() use ($type, $key) {

            $domain = config("SxDomain.{$type}" . ($key ? '.' . strtolower($key) : ''), null);

            return $domain ? __($domain) : $domain;
        });
    }

    /**
     * Get domain from texts
     *
     * @param $type
     * @param null|string $language
     * @param bool $sorter
     * @return mixed
     */
    public static function getDomain($type, $language = null, $sorter = false)
    {
        // Normalize type
        $type = strtolower($type);

        return Language::inLanguage($language, function() use ($type, $sorter) {

            $domains = [];

            foreach ( config("SxDomain.{$type}", []) as $key => $value ) {
                $domains[$key] = __($value);
            }

            if ( ! $sorter ) {
                asort($domains);
            }

            if ( ! is_array($domains) ) dd($domains);

            return $domains;
        });
    }

    /**
     * Convert data array to a formated list
     *
     * @param array $data
     * @param string $valueProp
     * @param string $keyProp
     * @return array
     */
    public static function format($data, $valueProp = 'value', $keyProp = 'key')
    {
        $domains = [];

        if ( empty($data) ) {
            return $domains;
        }

        $isAssoc = array_keys($data) !== range(0, count($data) - 1);

        foreach ( $data as $key => $value ) {

            if ( ! is_object($value) ) {
                $domains[] = [
                    'key' => $isAssoc ? $key : StringUtility::kebab($value), 'value' => $value
                ];
            }

            if ( is_object($value) ) {
                $domains[] = [
                    'key' => data_get($value, $keyProp), 'value' => data_get($value, $valueProp)
                ];
            }

        }

        return $domains;
    }

    /**
     * Get allowed languages with text vars
     *
     * @return array
     */
    public static function getAllowedLanguages()
    {
        $languages = Language::getAllowedLanguages();

        $domains = [];

        foreach ($languages as $language) {
            $domains[$language] = self::get('language', $language, $language);
        }

        return self::format($domains);
    }

}
