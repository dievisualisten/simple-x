<?php

namespace App\Library;

use App\Library\Elasticsearch\SxElasticsearchManager;
use App\Library\Traits\SingletonTrait;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/*
 * FixMe ZS, wird in den Article Service & Search Service aufgesplittet, wird allerdings im Moment noch verwendet ...
 * */

class SxPageBuilder
{
    use SingletonTrait;


    protected function articleView(
      $id,
      $menuId = null,
      $type = null,
      $contain = [],
      $additionalChildrenConditions = [],
      $childrenContain = [],
      $childrenOrder = ['Menus.lft' => 'ASC'],
      $pagingFinder = null
    ) {
        $articlesTable = TableRegistry::getTableLocator()->get('Articles');

        if ( empty($contain) ) {
            $contain = [
              'Menus',
                'Attached',
              'Attachments',
              'Acos',
              'Formularconfigs.Formulars',
              'Formularconfigs.Emails',
            ];
        }

        $contain = array_filter(Hash::merge($contain, (array) Configure::read('Sx.app.articles.article.extracontain')));

        AuthManager::pause();

        $articleEntity = $articlesTable->find()->where([
          'Articles.id' => $id,
        ])->contain($contain)->first();


        $result = $this->getChildrenLists(
          $type,
          $articleEntity->get('layout'),
          $menuId,
          $articleEntity,
          $additionalChildrenConditions,
          $childrenContain,
          $childrenOrder, $pagingFinder);

        AuthManager::resume();

        return $result;
    }

    protected function getChildrenLists(
      $viewType,
      $layout,
      $menuId,
      $articleEntity,
      $additionalConditions = [],
      $additionalContains = [],
      $order = ['Menus.lft' => 'ASC'],
      $pagingFinder = null
    ) {

        if ( empty($pagingFinder) ) {
            $pagingFinder = function (Query $query, $limit) {
                return $query->limit($limit)->all();
            };
        }

        $menusTable = TableRegistry::getTableLocator()->get('Menus');

        $return = [
          'list'     => [],
          'children' => [],
        ];

        if ( empty($menuId) ) {
            return $return;
        }

        $menu = $menusTable->find()->where(['Menus.id' => $menuId])->first();

        if ( empty($menu) ) {
            return $return;
        }

        $children = ['children'];

        $contain = array_filter(array_unique(Hash::merge([
          'Articles',
          'Articles.Attached',
          'Articles.Attachments',
          'Articles.Acos',
        ], $additionalContains, (array) Configure::read('Sx.app.articles.children.extracontain'))));


        //wenn die Seite Listeneinträge hat
        if ( Configure::read('Sx.resource.modules.article.pagetypes.' . $viewType . '.' . $layout . '.panels.menu.teaser32.show') ) {
            $children[] = 'list';
        }


        $searchIsEnabled = $articleEntity->get('extended_data_cfg.search.enable', 'false');

        $limit = $articleEntity->get('extended_data_cfg.article.paging_count', null) ?? Configure::read('Sx.app.articles.limit');


        foreach ( $children as $child ) {


            //Suche erlaubt?
            if ( $searchIsEnabled && $child == 'list' ) {

                $hasSearch = request()->session()->read('Visitor.frontendsearch.' . menu()->getMenuRecord('id'));

                //Submit aus dem Frontend oder $queryString in der Session
                if ( menu()->getRequest()->getData('frontendsearch') || $hasSearch ) {

                    $queryString = menu()->getRequest()->getData('querystring') ?? $hasSearch;


                    request()->session()->write('Visitor.frontendsearch.' . menu()->getMenuRecord('id'), $queryString);

                    if ( ! empty($queryString) ) {

                        $params = [
                          'size'  => 1000,
                          'index' => SxElasticsearchManager::getPrefixedIndexName('Articles', 'de'),
                          'body'  => SxElasticsearchManager::getSearchBody('~' . $queryString),
                        ];

                        $results = SxElasticsearchManager::getClient()->search($params);
                        $articleIds = Hash::extract($results, 'hits.hits.{n}._id');

                        if ( ! empty($articleIds) ) {
                            $query = $menusTable->find('published')->where([
                              'type not in'          => ['frontpage', 'menu', 'domain'],
                              'Menus.teaser not in'  => TEASER_WITHOUT_URL,
                              'Menus.foreign_key in' => $articleIds,
                            ]);

                            if ( $articleEntity->extended_data_cfg['search']['single_domain'] ) {
                                $query->where([
                                    'Menus.lft >'  => menu()->getDomainRecord('lft'),
                                    'Menus.rght <' => menu()->getDomainRecord('rght'),
                                  ]
                                );
                            }

                            $return['list'] = $pagingFinder($query, $limit);


                        } else {
                            $articleEntity->set('content', __('search.noresults.content'));
                            $articleEntity->set('headline', __('search.noresults.headline'));
                        }
                    }
                }


            } else {

                if ( $child == 'list' ) {

                    if ( $viewType == 'calendar' ) {

                        $query = $menusTable->find('published')->where(['Menus.parent_id' => $menu->id]);
                        $query->where($additionalConditions);
                        $query->contain($contain);
                        $parent_id = $menu->id;

                        $query = $query->matching('Events', function (Query $q) use ($parent_id) {
                            return $q->where(['menu_parent_id' => $parent_id]);
                        });

                        $query->order(['Events.date' => 'ASC']);

                        if ( Configure::read('Sx.app.language.defaultlanguage') != I18n::getLocale() ) {
                            $query->where([
                              'MenusTranslation.locale' => I18n::getLocale(),
                            ]);
                        }


                        //TODO SX4 LIMIT vs. zeitraum

                        $res = $pagingFinder($query, $limit);


                        foreach ( $res as $m ) {
                            $item = clone($m);
                            $item->path = $item->path . '#!' . $item->_matchingData['Events']->date->format('Y-m-d');
                            $return['list'][] = $item;
                        }

                    } else {

                        $query = $menusTable->find('published')->where(['Menus.teaser & 32', 'Menus.parent_id' => $menu->id]);
                        $query->where($additionalConditions);

                        $query->contain($contain)->order($order);

                        if ( Configure::read('Sx.app.language.defaultlanguage') != I18n::getLocale() ) {
                            $query->where([
                              'MenusTranslation.locale' => I18n::getLocale(),
                            ]);
                        }


                        $return['list'] = $pagingFinder($query, $limit);


                    }
                }
            }

            //Kinder
            if ( $child == 'children' ) {

                $query = $menusTable->find()->where($additionalConditions);
                $query->contain($contain);

                $secLevelChildren = $menusTable->getChildren($menu, ['level' => 3, 'order' => $order, 'query' => $query]);

                $return['children'] = $secLevelChildren;
            }
        }

        $return['data'] = $articleEntity;


        return $return;
    }


}
