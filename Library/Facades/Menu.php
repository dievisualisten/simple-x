<?php

namespace App\Library\Facades;

use App\Library\Traits\FacadeTrait;
use App\Library\Services\MenuService;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\MenuService
 *
 * @method static array|null getRouteParams()
 * @method static array getBreadcrumbs()
 * @method static mixed getDomainRecord(string|null $key = null, mixed $fallback = null)
 * @method static mixed getFrontpageRecord(string|null $key = null, mixed $fallback = null)
 * @method static mixed getMenuRecord(string|null $key = null, mixed $fallback = null)
 * @method static bool isHome()
 * @method static bool isHomeUrl(string $url = '', bool $adjustLocal = false)
 * @method static string getHomeUrl(bool $useDev = false, string|null $locale = null)
 * @method static array getMenuVisibility(int $number)
 */
class Menu
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = MenuService::class;

}
