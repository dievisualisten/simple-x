<?php

namespace App\Library\Facades;

use App\Library\Traits\FacadeTrait;
use App\Library\Services\MailerService;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\MailerService
 */
class Mailer
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = MailerService::class;

}
