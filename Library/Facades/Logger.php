<?php

namespace App\Library\Facades;

use App\Library\Traits\FacadeTrait;
use App\Library\Services\LoggerService;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\LoggerService
 *
 * @method static void start()
 * @method static array stop()
 * @method static void log(string $message, string $file = 'log')
 * @method static bool hasConfig()
 * @method static void makeConfig(string $file, array $scopes = ['*'])
 * @method static void dropConfig(string $file)
 * @method static void write(string $level, string $message, array $scopes = [], array $context = [])
 * @method static void writeFile(string $level, string $message, string $file)
 * @method static void debug(string $message, array $scopes = [])
 * @method static void debugFile(string $message, string $file = '')
 * @method static void info(string $message, array $scopes = [])
 * @method static void infoFile(string $message, string $file = '')
 * @method static void notice(string $message, array $scopes = [])
 * @method static void noticeFile(string $message, string $file = '')
 * @method static void alert(string $message, array $scopes = [])
 * @method static void alertFile(string $message, string $file = '')
 * @method static void warning(string $message, array $scopes = [])
 * @method static void warningFile(string $message, string $file = '')
 * @method static void error(string $message, array $scopes = [])
 * @method static void errorFile(string $message, string $file = '')
 * @method static void critical(string $message, array $scopes = [])
 * @method static void criticalFile(string $message, string $file = '')
 * @method static void emergency(string $message, array $scopes = [])
 * @method static void emergencyFile(string $message, string $file = '')
 */
class Logger
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = LoggerService::class;

}
