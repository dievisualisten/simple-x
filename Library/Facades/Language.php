<?php

namespace App\Library\Facades;

use App\Library\Traits\FacadeTrait;
use App\Library\Services\LanguageService;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\LanguageService
 *
 * @method static void setLanguage(string $language = null)
 * @method static string getLanguage()
 * @method static string getDefaultLanguage()
 * @method static bool isDefaultLanguage(string|null $language = null)
 * @method static array getAllowedLanguages()
 * @method static bool isAllowedLanguage(string|null $language = null)
 * @method static string getUserLanguage()
 * @method static bool hasUserLanguage()
 * @method static string getRouteLanguage()
 * @method static bool hasRouteLanguage()
 * @method static mixed inLanguage(string|null $language, \Closure $callback)
 * @method static mixed inDefaultLanguage(\Closure $callback)
 * @method static string normalizeUrl(string $url)
 */
class Language
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = LanguageService::class;

}
