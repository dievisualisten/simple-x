<?php

namespace App\Library\Facades;

use App\Library\Services\SearchService;
use App\Library\Traits\FacadeTrait;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\SearchService
 */

class Search
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = SearchService::class;

}
