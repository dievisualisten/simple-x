<?php

namespace App\Library\Facades;

use App\Library\Services\RequestService;
use App\Library\Traits\FacadeTrait;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\RequestService
 *
 * @method static \Cake\Http\ServerRequest request()
 * @method static \Cake\Http\Session session()
 * @method static string protocol(bool $forceSecure = false)
 * @method static string scheme(bool $forceSecure = false)
 * @method static string domain(bool $useDev = false)
 * @method static string path(bool $withExtension = true)
 * @method static string url(bool $useDev = false, bool $withExtension = true)
 * @method static array query(array $override = [], bool $source = true)
 * @method static string queryString(array $override = [], bool $source = true)
 * @method static string fullDomain(bool $forceSecure = false, bool $useDev = false)
 * @method static string fullPath(bool $forceSecure = false, bool $useDev = false, bool $withExtension = true)
 * @method static string fullUrl(bool $forceSecure = false, bool $useDev = false, bool $withExtension = true)
 * @method static string ext(mixed $fallback = 'html')
 * @method static boolean is(string|array $types)
 * @method static boolean isMethod(string $type = 'get')
 * @method static array segments(bool $withLocale = false, bool $withExt = true)
 * @method static string segment(int $index = 0, mixed $fallback = null)
 * @method static array params()
 * @method static string param(int $index = 0, mixed $fallback = null)
 * @method static string getCurrent(array $params = [])
 * @method static string getContextDomain(string $domain)
 * @method static string getDomainFromPath(string $path, bool $forceSecure = false)
 */
class Request
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = RequestService::class;

}
