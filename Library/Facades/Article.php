<?php

namespace App\Library\Facades;

use App\Library\Traits\FacadeTrait;
use App\Library\Services\ArticleService;

/**
 * @version 4.0.0
 */

/**
 * @see \App\Library\Services\ArticleService
 *
 * @method static \App\Model\Entity\Article runArticleParsers(\App\Model\Entity\Article &$article)
 * @method static array runChildrenParsers(\App\Model\Entity\Article &$article, array &$children)
 * @method static array getContains()
 * @method static array getMenuContains()
 * @method static \Cake\ORM\Query getArticleQuery()
 * @method static \App\Model\Entity\Article getArticleById(string $id)
 * @method static \App\Model\Entity\Article getArticleByMenuId(string $id, array $props = [])
 * @method static string getMonthRangeStart()
 * @method static string getMonthRangeEnd()
 * @method static \Cake\ORM\Query getEventQuery(\Cake\ORM\Query $query)
 * @method static \Cake\ORM\Query getMonthQuery(\Cake\ORM\Query $query)
 * @method static \Cake\ORM\Query getMenuQuery()
 * @method static \App\Model\Entity\Article getMenuById(string $id)
 * @method static \App\Model\Entity\Article getMenuByArticleId(string $id)
 * @method static array getMenuChildren(string $id, array $fallback = [])
 * @method static \App\Model\Entity\Article getArticleNextDepth(\App\Model\Entity\Article $article)
 * @method static \App\Model\Entity\Article getArticle(\App\Model\Entity\Menu $menu, $fallback = null)
 * @method static \App\Model\Entity\Article getArticleView(\Cake\Controller\Controller $controller, \App\Model\Entity\Menu $menu)
 */
class Article
{
    use FacadeTrait;

    /**
     * Service class
     *
     * @var string
     */
    protected static $serviceClass = ArticleService::class;

}
