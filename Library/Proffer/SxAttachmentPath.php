<?php

namespace App\Library\Proffer;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Proffer\Lib\ProfferPath;

/**
 * Class AttachmentPath
 */
class SxAttachmentPath extends ProfferPath
{

    protected $versions = [];

    /**
     * Construct the class and setup the defaults
     *
     * @param Table $table Instance of the table
     * @param Entity $entity Instance of the entity data
     * @param string $field The name of the upload field
     * @param array $settings Array of settings for the upload field
     */
    public function __construct(Table $table, Entity $entity, $field, array $settings)
    {
        $this->table = $table;
        $this->entity = $entity;

        if ( isset($settings['root']) ) {
            $this->setRoot($settings['root']);
        } else {
            $this->setRoot(MEDIA);
        }

        $this->setField($field);

        if ( isset($settings['thumbnailSizes']) ) {
            $this->setVersions($settings['thumbnailSizes']);
        }

        $this->setFilename($entity->get('basename'));
        $this->setSeed($entity->get('dirname'));

        $this->createPathFolder();
    }

    /**
     * @return array
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param array $versions
     */
    public function setVersions(array $versions)
    {
        foreach ( $versions as $version => $dimensions ) {
            array_push($this->versions, $version);
        }
    }

    /**
     * Return the complete absolute path to an upload. If it's an image with thumbnails you can pass the prefix to
     * get the path to the prefixed thumbnail file.
     *
     * @param string $version Thumbnail version
     * @return string
     */
    public function fullPath($version = 'uploads')
    {
        return $this->getTable()->getVersionPath($this->entity, $version);
    }

    /**
     * Return the absolute path to the containing parent folder where all the files will be uploaded
     *
     * @return string
     */
    public function getFolder()
    {
        $seed = $this->getSeed();
        $seed = ( ! empty($seed)) ? $seed . DS : null;

        return $this->getRoot() . $seed;
    }

    /**
     * Check if the upload folder has already been created and if not create it
     *
     * @return bool
     */
    public function createPathFolder()
    {
        if ( ! file_exists($this->getRoot()) ) {
            mkdir($this->getRoot(), 0755, true);
        }

        if ( ! file_exists($this->getFolder()) ) {
            mkdir($this->getFolder(), 0755, true);
        }

        if ( ! file_exists($this->getFolder() . DS . 'uploads') ) {
            mkdir($this->getFolder() . DS . 'uploads', 0755, true);
        }

        foreach ( $this->getVersions() as $version ) {
            if ( ! file_exists($this->getFolder() . DS . $version) ) {
                mkdir($this->getFolder() . DS . $version, 0755, true);
            }
        }

        return true;
    }

    /**
     * Clear out a folder and optionally delete it
     *
     * @param string $folder Absolute path to the folder
     * @param bool $rmdir If you want to remove the folder as well
     * @return void
     */
    public function deleteFiles($folder = null, $rmdir = false)
    {
        @unlink($this->getFolder() . 'uploads' . DS . $this->entity->basename);

        foreach ( $this->getVersions() as $version ) {
            @unlink($this->getFolder() . $version . DS . $this->entity->basename);
        }
    }
}
