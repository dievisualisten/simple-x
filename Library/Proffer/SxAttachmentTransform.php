<?php
/**
 * Created by PhpStorm.
 * User: Zelestin Schwinges
 * Date: 28.06.2015
 * Time: 00:01
 */

namespace App\Library\Proffer;

use App\Library\ImageManipulation;
use Cake\ORM\Table;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Proffer\Lib\ImageTransformInterface;

/**
 * @version 4.0.0
 */

class SxAttachmentTransform implements ImageTransformInterface
{
    /**
     * Store our instance of Imagine
     *
     * @var \Imagine\Image\ImageInterface $Imagine
     */
    private $Imagine;

    /**
     * @var \Cake\ORM\Table $table Instance of the table being used
     */
    protected $Table;

    /**
     * @var \App\Library\Proffer\AttachmentPath $Path Instance of the path class
     */
    protected $Path;

    /**
     * Construct the transformation class
     *
     * @param \Cake\ORM\Table $table
     * @param \App\Library\Proffer\AttachmentPath $path
     * @throws \Exception
     */
    public function __construct(Table $table = null, AttachmentPath $path = null)
    {
        $this->Table = $table;
        $this->Path = $path;

        $this->Imagine = (new ImageManipulation)->getImagine();
    }

    /**
     * Get the specified Imagine engine class
     *
     * @return \Imagine\Image\ImageInterface
     */
    public function getImagine()
    {
        return $this->Imagine;
    }

    /**
     * Set the Imagine engine class
     *
     * @param string $imagine The name of the image engine to use
     * @return void
     */
    public function setImagine($imagine = null)
    {
        if ( $imagine ) {
            $this->Imagine = $imagine;
        }
    }

    /**
     * Take an upload fields configuration and create all the thumbnails
     *
     * @param array $config The upload fields configuration
     * @return array
     */
    public function processThumbnails(array $config)
    {
        $result = [];

        foreach ($config['thumbnailSizes'] as $prefix => $dimensions) {
            $result[$prefix] = $this->makeThumbnail($prefix, $dimensions);
        }

        return $result;
    }


    /**
     * Generate and save the thumbnail
     *
     * @param string $version The thumbnail version
     * @param array $dimensions Array of thumbnail dimensions
     * @return void
     */
    public function makeThumbnail($version, array $dimensions)
    {
        $image = $this->getImagine()->open($this->Path->fullPath());

        [$image, $instruction] = $this->buildFromFocusInstructions($image, [
            'target' => $dimensions['size'], 'transform' => $dimensions['transform']['type']
        ]);

        $image->save($this->Path->fullPath($version), $dimensions['imagine']);
    }

    public function getSafeValues($size)
    {
        foreach ( array_keys($size) as $key ) {
            $size[$key] = max(0, $size[$key]);
        }

        return $size;
    }

    public function getImageSize($image)
    {
        $size = [];

        $size['w'] = $image->getSize()->getWidth();
        $size['h'] = $image->getSize()->getHeight();

        return $size;
    }

    public function getOptionsSize($image, $options, $targetRatio = true)
    {
        if ( isset($options['force']) && ! empty($options['force']) ) {
            return $this->getSafeValues($options['force']);
        }

        $offset = [
            'x' => 0, 'y' => 0
        ];


        if ( isset($options['focus']) && ! empty($options['focus']) ) {
            $offset = $options['focus'];
        }

        $size = [];

        $size['w'] = $image->getSize()->getWidth();
        $size['h'] = $image->getSize()->getHeight();

        if ( $targetRatio ) {
            $size = $this->applyRatio($size, $options['target']);
        }

        $size = $this->getOffset($image, $size,
            $offset['x'], $offset['y']);

        return $size;
    }

    public function applyRatio($size, $boundry)
    {
        $asrx = 1 / $boundry['w'] * $boundry['h'];
        $asry = 1 / $boundry['h'] * $boundry['w'];

        $scaleX = [
            'w' => max(0, $size['w']),
            'h' => max(0, $size['w'] * $asrx)
        ];

        $scaleY = [
            'w' => max(0, $size['h'] * $asry),
            'h' => max(0, $size['h'])
        ];

        if ( $scaleX['h'] > $size['h'] ) {
            return $scaleY;
        }

        return $scaleX;
    }

    public function getOffset($image, $size, $x = 0, $y = 0)
    {
        $w = $image->getSize()->getWidth();
        $h = $image->getSize()->getHeight();

        $offset = [
            'x' => abs(($x + 1) / 2) * $w - ($size['w'] / 2),
            'y' => abs(($y - 1) / 2) * $h - ($size['h'] / 2),
        ];

        $size['x'] = max(0, $offset['x'] + $size['w'] > $w ?
            $w - $size['w'] : $offset['x']);

        $size['y'] = max(0, $offset['y'] + $size['h'] > $h ?
            $h - $size['h'] : $offset['y']);

        return $size;
    }

    public function fitIntoBoundry($size, $boundry, $boundryRatio = false)
    {
        $asrx = 1 / $size['w'] * $size['h'];

        if ( $boundryRatio ) {
            $asrx = 1 / $boundry['w'] * $boundry['h'];
        }

        $asry = 1 / $size['h'] * $size['w'];

        if ( $boundryRatio ) {
            $asry = 1 / $boundry['h'] * $boundry['w'];
        }

        $scaleX = [
            'w' => max(0, $size['w']),
            'h' => max(0, $size['w'] * $asrx)
        ];

        $scaleY = [
            'w' => max(0, $size['h'] * $asry),
            'h' => max(0, $size['h'])
        ];

        if ( $scaleY['h'] <= $boundry['h'] && $scaleY['w'] <= $boundry['w'] ) {
            return $scaleY;
        }

        if ( $scaleX['h'] <= $boundry['h'] && $scaleX['w'] <= $boundry['w'] ) {
            return $scaleX;
        }

        $facx = (1 / $size['w']) * $boundry['w'];
        $facy = (1 / $size['h']) * $boundry['h'];

        $scaleX = [
            'w' => max(0, $size['w'] * $facx),
            'h' => max(0, $size['w'] * $asrx * $facx)
        ];

        $scaleY = [
            'w' => max(0, $size['h'] * $asry * $facy),
            'h' => max(0, $size['h'] * $facy)
        ];

        if ( $scaleX['h'] > $boundry['h'] ) {
            return $scaleY;
        }

        return $scaleX;
    }

    public function buildFromCropInstructions($image, $options)
    {
        $result = null;

        if ( ! isset($options['transform']) ) {
            $options['transform'] = 'scalecrop';
        }

        if ( $options['transform'] == 'fitinside' ) {
            $result = $this->makeFitImage($image, $options);
        }

        if ( $options['transform'] === 'scalecrop' ) {
            $result = $this->makeCropImage($image, $options);
        }

        if ( ! $result ) {
            throw new \Exception('Invalid instructions for buildFromCrop');
        }

        $image = $this->applyInstructions($image, $result);

        return [$image, $result];
    }

    public function makeCropImage($image, $options)
    {
        $options['size'] = $this->getOptionsSize($image, $options, true);

        $options['resize'] = $this->fitIntoBoundry($options['size'],
            $options['target'], true);

        return $options;
    }

    public function makeFitImage($image, $options)
    {
        $options['size'] = $this->getOptionsSize($image, $options, false);

        $options['resize'] = $this->fitIntoBoundry($options['size'],
            $options['target'], false);

        return $options;
    }

    public function buildFromFocusInstructions($image, $options)
    {
        $result = null;

        if ( ! isset($options['transform']) ) {
            $options['transform'] = 'scalecrop';
        }

        if ( $options['transform'] == 'fitinside' ) {
            $result = $this->makeFocusFitImage($image, $options);
        }

        if ( $options['transform'] === 'scalecrop' ) {
            $result = $this->makeFocusCropImage($image, $options);
        }

        if ( ! $result ) {
            throw new \Exception('Invalid instructions for buildFromFocus');
        }

        $image = $this->applyInstructions($image, $result);

        return [$image, $result];
    }

    public function makeFocusCropImage($image, $options)
    {
        $options['size'] = $this->getOptionsSize($image, $options, true);

        $options['resize'] = $this->fitIntoBoundry($options['size'],
            $options['target'], true);

        return $options;
    }

    public function makeFocusFitImage($image, $options)
    {
        $options['size'] = $this->getOptionsSize($image, $options, false);

        $options['resize'] = $this->fitIntoBoundry($options['size'],
            $options['target'], false);

        return $options;
    }

    public function applyInstructions($image, $options)
    {
        $targetBox = new Box(
            intval($options['size']['w']), intval($options['size']['h'])
        );

        $targetPoint = new Point(
            intval($options['size']['x']), intval($options['size']['y'])
        );

        $image = $image->copy()
            ->usePalette(new RGB);

        try {
            $image->crop($targetPoint, $targetBox);
        } catch ( \Exception $e) {
            if ( $e->getCode() !== 445 ) {
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        }

        $realBox = new Box(
            intval($options['resize']['w']), intval($options['resize']['h'])
        );

        return $image->resize($realBox);
    }

    public function whitespaceImage(ImageInterface $image, $transparent = false)
    {
        dd('TODO');
//        $size = $image->getSize();
//
//        $final = [
//            'w' => $size->getWidth() * 0.1,
//            'h' => $size->getHeight() * 0.1
//        ];
//
//        $final = [
//
//            'w' => ($final['w'] > $final['h'] ?
//                $final['w'] : $final['h']) + $size->getWidth(),
//
//            'h' => ($final['h'] > $final['w'] ?
//                $final['h'] : $final['w']) + $size->getHeight()
//
//        ];
//
//        $color = config('Media.whitespace', '#fff');
//
//        $targetBox = new Box(
//            intval($final['w']), intval($final['h'])
//        );
//
//        $limit = [
//            'x' => ($final['w'] - $size->getWidth()) / 2,
//            'y' => ($final['h'] - $size->getHeight()) / 2,
//        ];
//
//        $targetPoint = new Point(
//            intval($limit['x']), intval($limit['y'])
//        );
//
//        $palette = new RGB();
//
//        // Get color with alpha value
//        $fill = $palette->color($color, $transparent ? 0 : 100);
//
//        $clone = $this->getImagine()->create($targetBox, $fill)
//            ->paste($image, $targetPoint);
//
//        $image->usePalette($palette);
//
//        return $clone;
    }

}
