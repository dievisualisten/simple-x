<?php

namespace App\Library\Formular;

use App\Library\Traits\DataTrait;
use App\Utility\StringUtility;
use Tools\Utility\Text;

class SxFormularField implements \ArrayAccess
{
    use DataTrait;

    public $strictMode = true;

    public $formularconfig = null;

    public function __construct($data, $formularconfig = null)
    {
        if ( ! empty($formularconfig) ) {
            $this->formularconfig = $formularconfig;
        }

        $defaults = [
            'cssClass' => '',
            'colXs' => 12,
            'colSm' => 12,
            'colMd' => 12,
            'colLg' => 12,
            'prepend' => '',
            'append' => '',
            'label' => '',
            'hideLabel' => false,
            'type' => 'text',
            'inputName' => '',
            'placeholder' => '',
            'tooltip' => '',
            'infotext' => '',
            'options' => '',
            'showOnFieldName' => '',
            'showOnFieldComparator' => '==',
            'showOnFieldValue' => '',
            'notBlank' => false,
            'notBlankMessage' => '',
            'defaultValue' => '',
            'validationRules' => [],
        ];

        if ( $this->strictMode === true ) {
            $data = array_only($data, array_keys($defaults));
        }

        $data = array_map(function ($value) {
            return is_string($value) ? trim($value) : $value;
        }, $data);

        $this->data = array_merge($defaults, $data);
    }

    public function setFormularconfig($formularconfig)
    {
        $this->formularconfig = $formularconfig;
    }

    public function getUnique($expand = null)
    {
        $name = StringUtility::slug($this->getName());

        if ( ! empty($expand) ) {
            $name .= '_' . $expand;
        }

        if ( empty($this->formularconfig) ) {
            return $name;
        }

        return $this->formularconfig->id . '_' . $name;
    }

    public function getName($expand = null)
    {
        $name = mb_strtolower(Text::slug($this->inputName, ['replacement' => '_', 'preserve' => '.']));

        if ( ! empty($expand) ) {
            $name .= '[' . $expand . ']';
        }

        return $name;
    }

    public function getValidationRules()
    {
        $rules = array_filter($this->validationRules, function ($rule) {
            return ! empty($rule['validationRule']);
        });

        return $rules;
    }

    public function getShowOnOptions()
    {
        $showOnOptions = [
            'name' => $this->showOnFieldName ?: '',
            'comparator' => $this->showOnFieldComparator ?: '==',
            'value' => $this->showOnFieldValue ?: '',
        ];

        if ( ! empty($this->formularconfig) ) {
            $showOnOptions['name'] = $this->formularconfig->id . '_' .
                StringUtility::slug($this->showOnFieldName);
        }

        return $showOnOptions;
    }

    public function allowBlank()
    {
        return $this->notBlank === false;
    }

    public function denyBlank()
    {
        return $this->notBlank === false;
    }

    public function hasTypeIn($typeList)
    {
        return array_key_exists($this->type, $typeList);
    }

    public function solveTypeIn($castList, $fallback = null)
    {
        return isset($castList[$this->type]) ?
            $castList[$this->type] : $fallback;
    }
}
