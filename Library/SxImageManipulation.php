<?php

namespace App\Library;

/**
 * @version 4.0.0
 */

class SxImageManipulation
{
    private $Imagine;

    /**
     * Construct the transformation class
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setImagine();
    }

    /**
     * Get the specified Imagine engine class
     *
     * @return \Imagine\Image\ImagineInterface
     */
    public function getImagine()
    {
        return $this->Imagine;
    }

    /**
     * Set the Imagine engine class
     *
     * @param string $engine The name of the image engine to use
     * @return void
     * @throws \Exception
     */
    public function setImagine($engine = 'gd')
    {
        if ( extension_loaded('imagick') ) {
            $engine = 'imagick';
        }

        $engines = [
            'gd'      => \Imagine\Gd\Imagine::class,
            'imagick' => \Imagine\Imagick\Imagine::class,
        ];

        if ( ! in_array($engine, array_keys($engines)) ) {
            throw new \Exception("\"{$engine}\" not supported image engine");
        }

        $this->Imagine = new $engines[$engine]();
    }

}
