<?php

return [
    'Is draft' => 'Ist Entwurf',
    'Replace thumbnail' => 'Vorschaubild ersetzen',
    'Allow upscale' => 'Vergrößern erlauben',
    'Apply focuspoint' => 'Fokuspunkt anwenden',
    'Apply crop' => 'Zuschnitt anwenden',
    'All versions will be replaced with given focuspoint. Are you sure?' => 'Alle Bildversionen werden mit dem gesetzen Fokuspunkt überschrieben. Möchten Sie fortfahren?'
];
