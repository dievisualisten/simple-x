
import { Build } from "./library/build";
export { Build };

import { Config } from "./library/config";
export { Config };

import { Form } from "./library/form";
export { Form };

import { Menu } from "./library/menu";
export { Menu };

import { Ajax } from "./library/ajax";
export { Ajax };

import { Route } from "./library/route";
export { Route };

import { Auth } from "./library/auth";
export { Auth };

export const Simplex = {
    Build: Build,
    Config: Config,
    Form: Form,
    Menu: Menu,
    Ajax: Ajax,
    Route: Route,
    Auth: Auth,
}

/**
 * @const global
 */

if ( typeof global.sx === 'undefined' ) {
    global.sx = Simplex;
}

export default Simplex;
