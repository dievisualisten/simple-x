import { Arr, Any } from "@kizmann/pico-js";

export class Menu {

    static menus = [];

    static setMenus(menus)
    {
        if ( Any.isString(menus) ) {
            menus = JSON.parse(menus);
        }

        Menu.menus = menus;
    }

    static getVisibleMenus(menus = null)
    {
        return Arr.filter(menus || Menu.menus,
            menu => ! menu.hide);
    }

}

export default Menu;