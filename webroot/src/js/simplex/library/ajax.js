import { Arr, Obj, Any, Data, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export class Ajax
{
    static apis = {};

    static routes = {};

    static mergeRoutes(routes)
    {
        Obj.each(routes, (value, key) => {
            Route.set(key, value);
        });
    }

    static mergeRestRoutes(key, except = [])
    {
        if ( ! Arr.has(except, 'index') ) {
            Route.set(`${key}.index`, `/${key}.json`);
        }

        if ( ! Arr.has(except, 'delete') ) {
            Route.set(`${key}.delete`, `/${key}/delete.json`);
        }

        if ( ! Arr.has(except, 'copy') ) {
            Route.set(`${key}.copy`, `/${key}/copy.json`);
        }

        if ( ! Arr.has(except, 'total') ) {
            Route.set(`${key}.total`, `/${key}/total.json`);
        }

        if ( ! Arr.has(except, 'import') ) {
            Route.set(`${key}.import`, `/${key}/import.json`);
        }

        if ( ! Arr.has(except, 'export') ) {
            Route.set(`${key}.export`, `/${key}/export.xlsx`);
        }

        if ( ! Arr.has(except, 'show') ) {
            Route.set(`${key}.show`, `/${key}/{id}.json`);
        }

        if ( ! Arr.has(except, 'edit') ) {
            Route.set(`${key}.edit`, `/${key}/{id}.json`);
        }
    }

    static rest(input, except = []) {

        let key = Arr.first(input);

        if ( ! Arr.has(except, 'index') ) {
            this.bind(key + '-index', (ajax, query, options) => {
                return ajax.get(Route.get(key + '.index', query, query), options);
            });
        }

        if ( ! Arr.has(except, 'total') ) {
            this.bind(key + '-total', (ajax, query, options) => {
                return ajax.get(Route.get(key + '.total', query, query), options);
            });
        }

        if ( ! Arr.has(except, 'delete') ) {
            this.bind(key + '-delete', (ajax, query, options) => {
                return ajax.post(Route.get(key + '.delete', query), query, options);
            });
        }

        if ( ! Arr.has(except, 'show') ) {
            this.bind(key + '-show', (ajax, query, options) => {
                return ajax.get(Route.get(key + '.show', query), options);
            });
        }

        if ( ! Arr.has(except, 'edit') ) {
            this.bind(key + '-edit', (ajax, query, options) => {
                return ajax.post(Route.get(key + '.edit', query), query, options);
            });
        }

        if ( ! Arr.has(except, 'copy') ) {
            this.bind(key + '-copy', (ajax, query, options) => {
                return ajax.post(Route.get(key + '.copy', query, query), options);
            });
        }

        return except.length;
    }

    static has(input)
    {
        return Arr.has(this.apis, Arr.first(input));
    }

    static bind(input, api)
    {
        Ajax.apis[Arr.first(input)] = api;

        return this;
    }

    static handler()
    {
        return window.axios || window.Vue.http;
    }

    static solve(input, vars = {}, options = {})
    {
        let callback = Ajax.apis[Arr.first(input)];

        if ( ! callback ) {
            return console.error('API not found: ' + Arr.first(input));
        }

        return Ajax.apis[Arr.first(input)](this.handler(), vars, options);
    }

    static call (input, store = false, vars = {}, options = {})
    {
        let call = (resolve, reject) => {
            return Ajax.solve(input, vars, options).then((res) => {

                if ( store ) {
                    Data.set(Arr.second(input), res.data);
                }

                Event.fire(Arr.first(input));
                Event.fire(Arr.first(input) + ':query', vars);

                return resolve(res);
            }, reject);
        };

        return new Promise(call);
    }

    static form (obj)
    {
        let form = new FormData();

        let appendField = (values, keys = []) => {
            Obj.each(values, (value, index) => {

                let inner = Arr.merge([], keys, index);

                if ( Any.isPlain(value) ) {
                    return appendField(value, inner);
                }

                if ( Any.isArray(value) ) {
                    return appendField(value, inner);
                }

                let key = inner.splice(0, 1)[0];

                Arr.each(inner, (index) => {
                    key += '[' + index + ']';
                });

                if ( value !== null ) {
                    form.append(key, value);
                }
            });

            return form;
        };

        return appendField(obj);
    }

}

export default Ajax;
