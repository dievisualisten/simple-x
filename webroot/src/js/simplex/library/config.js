import { Arr, Any, Obj, Num, Data } from "@kizmann/pico-js";

export class Config
{
    static get(key, fallback = null)
    {
        let base = [
            'config', 'data'
        ];

        if ( Any.isString(key) ) {
            key = key.split('.');
        }

        return Data.get(Arr.concat(base, key).join('.'), fallback);
    }

    static domain(type, key, fallback = {})
    {
        let domains = Data.get('domains.data', fallback);

        return Arr.find(domains, { type, key }, fallback);
    }

    static domains(key, fallback = [])
    {
        let domains = Data.get('domains.data', fallback);

        return Arr.filter(domains, { type: key }) || fallback;
    }

    static domaintypes(fallback = {})
    {
        let domains = Data.get('domains.data', fallback), types = [];

        Arr.each(domains, (item) => {
            Arr.add(types, item.type);
        });

        return types;
    }


    static articlePagetypes()
    {
        let base = [
            'resource', 'modules', 'article', 'pagetypes'
        ];

        let pagetypes = this.get(base.join('.'),
            {});

        pagetypes = Obj.sortString(pagetypes, 'name');

        let result = {
            //
        };

        Obj.each(pagetypes, (value) => {

            if ( ! this.article(['types', value._key], null) ) {
                return;
            }

            if ( Num.int(value.element) ) {
                return;
            }

            result[value._key] = value.name;
        });

        return result;
    }

    static elementPagetypes()
    {
        let base = [
            'resource', 'modules', 'article', 'pagetypes'
        ];

        let pagetypes = this.get(base.join('.'),
            {});

        pagetypes = Obj.sortString(pagetypes, 'name');

        let result = {
            //
        };

        Obj.each(pagetypes, (value) => {

            if ( ! this.article(['types', value._key], null) ) {
                return;
            }

            if ( ! Num.int(value.element) ) {
                return;
            }

            result[value._key] = value.name;
        });

        return result;
    }


    static pagetypes(key, fallback = null)
    {
        let base = [
            'resource', 'modules', 'article', 'pagetypes'
        ];

        if ( Any.isString(key) ) {
            key = key.split('.');
        }

        return this.get(Arr.concat(base, key).join('.'), fallback);
    }

    static article(key, fallback = null)
    {
        let base = [
            'article', 'data'
        ];

        if ( Any.isString(key) ) {
            key = key.split('.');
        }

        return Data.get(Arr.concat(base, key).join('.'), fallback);
    }

    static find(input, value, fallback = null)
    {
        let key = Arr.first(input) + '.data';

        return super.find(key, value, fallback)
    }
}

export default Data;
