import { Arr, Any } from "@kizmann/pico-js";
import Route from "./route";

export class Build {

    static debug = true;

    static pagetitle = null;

    static locale = 'en';

    static locales = ['en'];

    static paths = {
        base: '/system', login: '/login', logout: '/logout'
    };

    static setDebug(debug)
    {
        Build.debug = Any.bool(debug);
    }

    static getDebug()
    {
        return Build.debug;
    }

    static setPagetitle(title)
    {
        Build.pagetitle = title;
    }

    static getPagetitle()
    {
        if ( ! Build.pagetitle ) {
            Build.pagetitle = document.title;
        }

        return Build.pagetitle;
    }

    static setLocale(locale)
    {
        Route.setLocale(Build.locale = locale);
    }

    static getLocale()
    {
        return Build.locale;
    }

    static setLocales(locales)
    {
        Build.locales = locales;
    }

    static getLocales()
    {
        return Build.locales;
    }

    static setPaths(paths)
    {
        if ( Any.isString(paths) ) {
            paths = JSON.parse(paths);
        }

        Build.paths = paths;
    }

    static setBasePath(basePath)
    {
        Build.paths.base = basePath;
    }

    static setLoginPath(basePath)
    {
        Build.paths.base = basePath;
    }

    static setLogoutPath(basePath)
    {
        Build.paths.base = basePath;
    }

}

export default Build;