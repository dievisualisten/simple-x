import Vue from "vue";
import VueRouter from 'vue-router';

const originalPush = VueRouter.prototype.push;

VueRouter.prototype.push = function (location, respolve, reject) {

    if (respolve || reject) {
        return originalPush.call(this, location, respolve, reject);
    }

    return originalPush.call(this, location).catch(err => err)
};

Vue.Obj.each(window.messages || {}, (path, name) => {
    Vue.Locale.set(name, path)
});

Vue.Obj.each(window.routes || {}, (path, name) => {
    Vue.Route.set(name, path)
});

Vue.Obj.each(window.imports || {}, (config, name) => {
    Vue.Ext.bind(name, config)
});

Vue.Obj.each(window.datas || {}, (value, key) => {
    Vue.Data.set(key, value)
});

let routes = [];

let locale = Vue.Dom.find('html')
    .attr('lang');

Vue.Route.setLocale(locale);

Vue.Arr.each(window.menus || [], function (menu) {

    if ( ! Vue.Obj.has(menu, 'component') ) {
        return;
    }

    let route = {
        path: menu.path, name: menu.name, component: Vue.component(menu.component)
    };

    route.meta = {
        title: menu.title, rights: menu.rights
    };

    routes.push(route);
});

Vue.Arr.each(window.menus || [], function (menu) {

    if ( ! Vue.Obj.has(menu, 'redirect') ) {
        return;
    }

    let route = {
        path: menu.path, redirect: { name: menu.redirect }
    };

    routes.push(route);
});

export default new VueRouter({
    base: window.basePath, mode: 'history', routes: routes
});
