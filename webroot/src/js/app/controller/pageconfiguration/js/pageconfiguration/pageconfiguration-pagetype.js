import DefaultEdit from "../../../default/src/edit/edit";

export default {

    name: 'SxPageconfigurationPagetype',

    inject: {

        NDataform: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        }

    },

    extends: DefaultEdit,

    render()
    {
        let renderOption = (type) => {
            return (
                <NSelectOption value={type._key} label={type.name} />
            );
        };

        return (
            <NForm form={this.value} errors={this.errors}>
                <NTabsItem>
                    <NFormItem prop="name" label={this.trans('Name')}>
                        <NInput vModel={this.value.name}></NInput>
                    </NFormItem>
                    <NFormItem prop="source" label={this.trans('Quelle')}>
                        <NSelect vModel={this.value.source} clearable={true}>
                            { this.Arr.each(this.Obj.sortString(this.SxIndexController.pagetypes, 'name'), renderOption) }
                        </NSelect>
                    </NFormItem>
                    <NFormItem prop="layout" label={this.trans('Layout')}>
                        <NInput vModel={this.value.layout}></NInput>
                    </NFormItem>
                </NTabsItem>
            </NForm>
        )
    }

}
