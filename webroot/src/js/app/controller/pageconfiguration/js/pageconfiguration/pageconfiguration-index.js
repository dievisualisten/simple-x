import DefaultIndex from "../../../default/src/index/index.js"
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxPageconfigurationIndex',

    extends: DefaultIndex,

    urls: {
        index: 'pageconfigurations.index',
        edit: 'pageconfigurations.edit',
        create: 'pageconfigurations.create'
    },

    model: 'pageconfiguration',

    computed: {

        pagetypes()
        {
            return Config.get('resource.modules.article.pagetypes', []);
        },

    },

    watch: {

        pagetype()
        {
            this.$nextTick(() => Event.fire('pagetype:change'));
        },

        element()
        {
            this.$nextTick(() => Event.fire('element:change'));
        }

    },

    data()
    {
        return {
            pagetype: '', element: false
        };
    },

    methods: {
        //
    },

    renderSelect()
    {
        let renderOption = (type) => {
            return (
                <NSelectOption value={type._key} label={type.name} />
            );
        };

        return (
            <NSelect vModel={this.pagetype} clearable={true} style="width: 200px;">
                { Arr.each(Obj.sortString(this.pagetypes, 'name'), renderOption) }
            </NSelect>
        );
    },

    renderPagetype()
    {
        return (
            <NButton onClick={() => this.showEditModal('pagetypeForm')}>
                { this.trans('Seitentyp anlegen') }
            </NButton>
        );
    },

    renderElement()
    {
        return (
            <NSwitch vModel={this.element}>{ this.trans('Elementtypen') }</NSwitch>
        );
    },

    renderTable()
    {
        let updateEvents = [
            'element:change',
            'pagetype:change',
            'pageconfigurations-edit',
            'pageconfigurations-pagetype'
        ];

        let props = {
            indexQuery: 'pageconfigurations-index',
            copyQuery: 'pageconfigurations-copy',
            deleteQuery: 'pageconfigurations-delete',
            group: ['pageconfigurations'],
            updateEvents: updateEvents,
            override: { pagetype: this.pagetype, element: this.element },
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        let slots = {
            custom: () => {
                return (
                    <div style="display: flex; flex-direction: row;">
                        <NButtonGroup>
                            { this.ctor('renderSelect')() }
                            { this.ctor('renderPagetype')() }
                        </NButtonGroup>
                        <div style="padding-left: 15px;">
                            { this.ctor('renderElement')() }
                        </div>

                    </div>
                );
            }
        };

        return (
            <NDatatable ref="table" {...props} v-slots={slots}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                }>
                {
                    (row) => {
                        return <span>{ row.item.display_name }</span>
                    }
                }
                </NTableColumn>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'default_value',
                        label: this.trans('Standardwert'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_resource',
                        label: this.trans('Resource'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'default_value',
                        label: this.trans('Standardwert')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_resource',
                        label: this.trans('Resource')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxPageconfigurationEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'pageconfigurations-show',
            editQuery: 'pageconfigurations-edit',
            newTitle: this.trans('Seitenkonfiguration erstellen'),
            editTitle: this.trans('Seitenkonfiguration bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    renderPagetypeForm()
    {

        let value = {
            id: this.UUID(), name: '', source: '', _new: true
        };

        let props = {
            use: 'SxPageconfigurationPagetype',
            localized: false,
            width: '960px',
            height: '100%',
            editQuery: 'pageconfigurations-pagetype',
            newTitle: this.trans('Seitentyp erstellen'),
            layout: ['spacer', 'close', 'save'],
            fallback: value,
            onNew: () => this.showEditModal('pagetypeForm'),
            onClose: () => this.hideEditModal('pagetypeForm'),
        };

        return (
            <NDataform ref="pagetypeForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderPagetypeForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
