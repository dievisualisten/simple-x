sx.Form.set('sx-pageconfiguration-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Seitenkonfiguration'),
                icon: 'fa fa-cog'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:-1': {
                    vShow: function(value) {
                        return pi.Obj.get(value, '_new', false);
                    },
                    $props: {
                        label: pi.Locale.trans('Seitentypen'),
                        prop: 'is_global'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'is_global', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Seitenkonfiguration bei allen Seitentypen hinzufügen')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Wert'),
                        prop: 'value'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'value'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Standardwert'),
                        prop: 'default_value'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'default_value'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Beschreibung'),
                        prop: 'description'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'description'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        prop: 'is_resource'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'is_resource', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Als Resource speichern')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
