import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('pageconfigurations');

Ajax.bind('pageconfigurations-tree', function (ajax, query, options) {
    let route = Route.get('pageconfigurations.tree', query, query);
    return ajax.get(route, options);
});

Ajax.bind('pageconfigurations-import', function (ajax, query, options) {
    let route = Route.get('pageconfigurations.import');
    return ajax.post(route, Ajax.form(query), options);
});

Ajax.bind('pageconfigurations-pagetype', function (ajax, query, options) {
    let route = Route.get('pageconfigurations.pagetype');
    return ajax.post(route, query, options);
});

require('./config/pageconfiguration-default.config');

import SxPageconfigurationIndex from "./js/pageconfiguration/pageconfiguration-index";
App.component(SxPageconfigurationIndex.name, SxPageconfigurationIndex);

import SxPageconfigurationEdit from "./js/pageconfiguration/pageconfiguration-edit";
App.component(SxPageconfigurationEdit.name, SxPageconfigurationEdit);

import SxPageconfigurationPagetype from "./js/pageconfiguration/pageconfiguration-pagetype";
App.component(SxPageconfigurationPagetype.name, SxPageconfigurationPagetype);

