import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('users');

Ajax.bind('users-import', function (ajax, query, options) {
    let route = Route.get('users.import');
    return ajax.post(route, Ajax.form(query), options);
});

Ajax.bind('users-login', function (ajax, query, options) {
    let route = Route.get('users.login', null, null);
    return ajax.post(route, query, options);
});

require('./config/user-default.config');

import SxUserIndex from "./js/user/user-index";
App.component(SxUserIndex.name, SxUserIndex);

import SxUserEdit from "./js/user/user-edit";
App.component(SxUserEdit.name, SxUserEdit);

import SxUserLogin from "./js/user/user-login";
App.component(SxUserLogin.name, SxUserLogin);

