import { Obj } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxUserLogin',

    data ()
    {
        let user = {
            login: '', password: ''
        };

        return {
            load: false, user: user, errors: {}
        }
    },

    methods: {

        authUser(event)
        {
            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            this.user.locale = this.$root.locale;

            Ajax.call(['users-login', 'auth'], false, this.user, options)
                .then(this.authUserDone, this.authUserError);
        },

        authUserDone(res)
        {
            let url = new URL(window.location.href);

            window.location = window.location.origin +
                (url.searchParams.get('redirect') || '/system');
        },

        authUserError(res)
        {
            this.errors = Obj.get(res.data, 'errors', {});
        },

        submitForm()
        {
            this.$refs.form.$el.submit();
        }

    },

    render()
    {
        return (
            <NLoader visible={this.load} class="sx-user-login">
                <NForm ref="form" class="sx-user-login__form" form={this.user} errors={this.errors}>

                    <NFormItem prop="login" label={this.trans('Benutzer')}>
                        <NInput vModel={this.user.login}></NInput>
                    </NFormItem>

                    <NFormItem prop="password" label={this.trans('Password')}>
                        <NInput native-type="password" vModel={this.user.password}></NInput>
                    </NFormItem>

                    <NFormItem class="text-right">
                        <NButton type="primary" onClick={this.authUser}>{ this.trans('Anmelden') }</NButton>
                    </NFormItem>

                </NForm>
            </NLoader>
        );
    }

}
