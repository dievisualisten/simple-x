import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxUserIndex',

    extends: DefaultIndex,

    urls: {
        index: 'users.index',
        edit: 'users.edit',
        create: 'users.create'
    },

    model: 'user',

    renderTable()
    {
        let updateEvents = [
            'users-edit'
        ];

        let props = {
            indexQuery: 'users-index',
            copyQuery: 'users-copy',
            deleteQuery: 'users-delete',
            group: ['users'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'login',
                        label: this.trans('Login'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'person.firstname',
                        label: this.trans('Vorname'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'person.lastname',
                        label: this.trans('Nachname'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'active',
                        label: this.trans('Aktiv'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'last_login',
                        label: this.trans('Letzer Login'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'from_email',
                        label: this.trans('Absender'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'email_to',
                        label: this.trans('Empfänger'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxUserEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'users-show',
            editQuery: 'users-edit',
            newTitle: this.trans('Benutzer erstellen'),
            editTitle: this.trans('{login} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
