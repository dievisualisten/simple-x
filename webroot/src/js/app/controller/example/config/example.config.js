import { Nano } from "@kizmann/pico-js";

Nano.Form.set('sx-example-example', [

    /* Example start */
    {
        element: 'n-tabs-item',
        props: {
            name: 'default',
            sort: 10,
            label: Nano.Locale.trans('Example'),
            icon: 'fa fa-check'
        },
        content: [
            {
                element: 'n-form-item',
                props: {
                    label: Nano.Locale.trans('Name'),
                    prop: 'name'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'name',
                        fallback: ''
                    },
                ]
            },
        ]
    },
    /* Example end */

]);
