import Vue from "vue";

Vue.Ajax.rest('examples');

require('./config/example.config');

import ExampleIndex from "./src/index/index.vue";
Vue.component(ExampleIndex.name, ExampleIndex);

import ExampleEdit from "./src/edit/edit.vue";
Vue.component(ExampleEdit.name, ExampleEdit);
