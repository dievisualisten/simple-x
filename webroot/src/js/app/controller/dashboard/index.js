import { Ajax, Route } from "@dievisualisten/simplex";

import SxDashboardIndex from "./js/dashboard/dashboard-index";
App.component(SxDashboardIndex.name, SxDashboardIndex);

import SxDashboardStatistic from "./js/plugins/dashboard-statistic";
App.component(SxDashboardStatistic.name, SxDashboardStatistic);

import SxDashboardCat from "./js/plugins/dashboard-cat";
App.component(SxDashboardCat.name, SxDashboardCat);

import SxDashboardDebug from "./js/plugins/dashboard-debug";
App.component(SxDashboardDebug.name, SxDashboardDebug);

import SxDashboardMaintenance from "./js/plugins/dashboard-maintenance";
App.component(SxDashboardMaintenance.name, SxDashboardMaintenance);

import SxDashboardRedirect from "./js/plugins/dashboard-redirect";
App.component(SxDashboardRedirect.name, SxDashboardRedirect);

import SxDashboardOrder from "./js/plugins/dashboard-order";
App.component(SxDashboardOrder.name, SxDashboardOrder);

Ajax.bind('system-debug', function (ajax, query, options) {
    let route = Route.get('system.debug');
    return ajax.post(route, query, options);
});