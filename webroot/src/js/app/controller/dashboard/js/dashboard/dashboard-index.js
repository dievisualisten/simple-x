import { Auth } from "@dievisualisten/simplex";

export default {

    name: 'SxDashboardIndex',

    computed: {

        salutation()
        {
            return 'Moin';
        }

    },

    renderHeader()
    {
        let name = Auth.user('person.firstname', 'хакер');

        if ( Auth.user('person.lastname') ) {
            name += ' ' + Auth.user('person.lastname')
        }

        return (
            <div class="dashboard__header">
                <h1>{ this.salutation } <b>{ name }</b></h1>
            </div>
        );
    },

    renderBody()
    {
        return (
            <div class="dashboard__body">
                <div class="grid grid--row grid--wrap grid--30-30" style="flex-flow: row wrap;">
                    <div class="col--1-1 col--4-6@md">
                        <SxDashboardStatistic />
                    </div>
                    <div class="col--1-1 col--2-6@md">
                        <SxDashboardDebug />
                    </div>
                    <div class="col--1-1 col--2-6@md">
                        <SxDashboardMaintenance />
                    </div>
                    <div class="col--1-1 col--4-6@md">
                        <SxDashboardOrder />
                    </div>
                    <div class="col--1-1 col--4-6@md">
                        <SxDashboardRedirect />
                    </div>
                    <div class="col--1-1 col--2-6@md">
                        <SxDashboardCat />
                    </div>
                </div>
            </div>
        );
    },

    render()
    {
        return (
            <NScrollbar class="sx-layout-main dashboard">
                { this.ctor('renderHeader')() }
                { this.ctor('renderBody')() }
            </NScrollbar>
        );
    }

}
