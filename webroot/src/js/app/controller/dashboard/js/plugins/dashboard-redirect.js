import { Data } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxDashboardRedirect',


    data()
    {
        return {
            load: false, redirects: []
        }
    },

    methods: {

        getRedirects(foreceUpdate = false)
        {
            if ( ! foreceUpdate && Data.has('redirect-notify') )  {
                return this.redirects = Data.get('redirect-notify');
            }

            let query = {
                notify: true
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('redirects-index', false, query, options)
                .then(this.queryDone);
        },

        reviewRedirect(redirect)
        {
            let query = {
                id: this.Obj.get(redirect, 'id'), notify: false
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('redirects-edit', false, query, options)
                .then(() => this.getRedirects(true));
        },

        queryDone(res)
        {
            Data.set('redirect-notify', this.redirects = res.data.data);
        },

        gotoRedirect(redirect)
        {
            this.$router.push({
                name: 'redirects.edit', params: redirect
            });
        },

        gotoRedirects()
        {
            this.$router.push({
                name: 'redirects.index'
            });
        }


    },

    mounted()
    {
        this.getRedirects();
    },

    renderTitle()
    {
        return (
            <div class="col--1-1 dashboard-redirect__title">
                <div class="grid grid--row grid--middle">
                    <div class="col--flex-1-1">
                        <h3>{ this.trans('Weiterleitungen') }</h3>
                    </div>
                    <div class="col--flex-0-0">
                        <NButton link={true} size="small" onClick={() => this.gotoRedirects()}>
                            { this.trans('Alle anzeigen') }
                        </NButton>
                    </div>
                </div>
            </div>
        );
    },

    renderRedirect(redirect)
    {
        return (
            <div class="dashboard-redirect__item">
                <div class="grid grid--row grid--middle grid--10">
                    <div class="col--1-3 dashboard-redirect__from text-ellipsis">
                        <span>
                            { redirect.from_url }
                        </span>
                    </div>
                    <div class="col--1-3 dashboard-redirect__target text-ellipsis">
                        <span>
                            { redirect.target_url }
                        </span>
                    </div>
                    <div class="col--1-3 dashboard-redirect__status text-ellipsis">
                        <span>
                            { redirect.status }
                        </span>
                    </div>
                    <div class="col--auto dashboard-redirect__action toolbar">
                        <NButton type="primary" size="xs" square={true} icon="fa fa-pen" onClick={() => this.gotoRedirect(redirect)} />
                        <NButton type="success" size="xs" square={true} icon="fa fa-check" onClick={() => this.reviewRedirect(redirect)} />
                    </div>
                </div>
            </div>
        );
    },

    renderEmpty()
    {
        return (
            <div class="col--1-1 dashboard-redirect__empty">
                { this.trans('Keine Konfilikte in den Weiterleitungen vorhanden') }
            </div>
        )
    },

    renderBody()
    {
        if ( ! this.redirects.length ) {
            return this.ctor('renderEmpty')();
        }

        return (
            <div class="col--1-1 dashboard-redirect__items">
                { this.Arr.each(this.redirects.slice(0, 5), this.ctor('renderRedirect')) }
            </div>
        );
    },

    render()
    {
        return (
            <div class="dashboard-redirect dashboard-bar">
                <NLoader visible={this.load} class="grid grid--col grid--wrap grid--top">
                    { this.ctor('renderTitle')() }
                    { this.ctor('renderBody')() }
                </NLoader>
            </div>
        );
    }

}
