export default {

    name: 'SxDashboardCat',

    methods: {

        getImage()
        {
            return 'https://cataas.com/cat/gif?&id=' + this.UUID();
        }

    },

    data()
    {
        return {
            image: this.getImage()
        };
    },

    render()
    {
        let classList = [
            'dashboard-cat', 'dashboard-bar'
        ];

        return (
            <div class={classList}>
                <img src={this.image} alt={this.trans('We love cats')} />
                <NButton link={true} size="large" icon="fa fa-sync" onClick={() => this.image = this.getImage()} />
            </div>
        );
    }

}
