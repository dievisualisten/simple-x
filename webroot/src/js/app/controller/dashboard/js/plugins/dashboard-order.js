export default {

    name: 'SxDashboardOrder',


    data()
    {
        return {
            load: false, orders: []
        }
    },

    methods: {

        getOrders(foreceUpdate = false)
        {
            if ( ! foreceUpdate && this.Data.has('order-notify') )  {
                return this.orders = this.Data.get('order-notify');
            }

            let query = {
                notify: true
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            this.Ajax.call('orders-index', false, query, options)
                .then(this.queryDone);
        },

        reviewOrder(order)
        {
            let query = {
                id: this.Obj.get(order, 'id'), notify: false
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            this.Ajax.call('orders-edit', false, query, options)
                .then(() => this.getOrders(true));
        },

        queryDone(res)
        {
            this.Data.set('order-notify', this.orders = res.data.data);
        },

        gotoOrder(order)
        {
            this.$router.push({
                name: 'orders.edit', params: order
            });
        }


    },

    mounted()
    {
        // this.getOrders();
    },

    renderTitle()
    {
        return (
            <div class="col--1-1 dashboard-order__title">
                <div class="grid grid--row grid--middle">
                    <div class="col--flex-1-1">
                        <h3>{ this.trans('Bestellungen') }</h3>
                    </div>
                    <div class="col--flex-0-0">
                        <NButton link={true} size="small">{ this.trans('Alle anzeigen') }</NButton>
                    </div>
                </div>
            </div>
        );
    },

    renderOrder(order)
    {
        return (
            <div class="dashboard-order__item">
                <div class="grid grid--row grid--middle grid--10">
                    { /* TBD */}
                </div>
            </div>
        );
    },

    renderEmpty()
    {
        return (
            <div class="col--1-1 dashboard-order__empty">
                { this.trans('Keine offenen Bestellungen vorhanden') }
            </div>
        )
    },

    renderBody()
    {
        if ( ! this.orders.length ) {
            return this.ctor('renderEmpty')();
        }

        return (
            <div class="col--1-1 dashboard-order__items">
                { this.Arr.each(this.orders.slice(0, 5), this.ctor('renderOrder')) }
            </div>
        );
    },

    render()
    {
        return (
            <div class="dashboard-order dashboard-bar">
                <NLoader visible={this.load} class="grid grid--col grid--wrap grid--top">
                    { this.ctor('renderTitle')() }
                    { this.ctor('renderBody')() }
                </NLoader>
            </div>
        );
    }

}
