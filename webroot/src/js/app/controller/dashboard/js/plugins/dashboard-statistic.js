import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxDashboardStatistic',


    data()
    {
        return {
            load: false, statistics: this.Data.get('stats', [])
        }
    },

    methods: {

        getStatistics()
        {
            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('stats', false, options)
                .then(this.queryDone);
        },


        queryDone(res)
        {
            this.Data.set('stats', this.statistics = res.data.data);
        }

    },

    mounted()
    {
        this.getStatistics();
    },

    renderArticle()
    {
        return (
            <div class="dashboard-statistic__item">
                <div class="dashboard-statistic__total">
                    { this.Obj.get(this.statistics, 'articles.total', 0) }
                </div>
                <div class="dashboard-statistic__name">
                    { this.trans('Seiten insgesamt') }
                </div>
                <div class="dashboard-statistic__part">
                    <div class="dashboard-statistic__part-count">
                        { this.Obj.get(this.statistics, 'articles.active', 0) }
                    </div>
                    <div class="dashboard-statistic__part-text">
                        { this.trans('Seiten sind aktiv') }
                    </div>
                </div>
            </div>
        );
    },

    renderAttachment()
    {
        return (
            <div class="dashboard-statistic__item">
                <div class="dashboard-statistic__total">
                    { this.Obj.get(this.statistics, 'attachments.total', 0) }
                </div>
                <div class="dashboard-statistic__name">
                    { this.trans('Medien insgesamt') }
                </div>
                <div class="dashboard-statistic__part">
                    <div class="dashboard-statistic__part-count">
                        {this.Obj.get(this.statistics, 'attachments.active', 0)}
                    </div>
                    <div class="dashboard-statistic__part-text">
                        {this.trans('Medien sind verknüpft')}
                    </div>
                </div>
            </div>
        );
    },

    renderMenu()
    {
        return (
            <div class="dashboard-statistic__item">
                <div class="dashboard-statistic__total">
                    { this.Obj.get(this.statistics, 'menus.total', 0) }
                </div>
                <div class="dashboard-statistic__name">
                    { this.trans('Menüs insgesamt') }
                </div>
                <div class="dashboard-statistic__part">
                    <div class="dashboard-statistic__part-count">
                        { this.Obj.get(this.statistics, 'menus.active', 0) }
                    </div>
                    <div class="dashboard-statistic__part-text">
                        { this.trans('Menüs sind aktiv') }
                    </div>
                </div>
            </div>
        );
    },

    render()
    {
        return (
            <div class="dashboard-statistic dashboard-bar">
                <NLoader visible={this.load} class="grid grid--row grid--wrap">
                    { this.ctor('renderArticle')() }
                    { this.ctor('renderMenu')() }
                    { this.ctor('renderAttachment')() }
                </NLoader>
            </div>
        );
    }

}
