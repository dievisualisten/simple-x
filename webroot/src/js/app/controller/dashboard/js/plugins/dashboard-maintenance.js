import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxDashboardMaintenance',


    data()
    {
        return {
            load: false, maintenance: this.getMaintenance()
        }
    },

    watch: {

        maintenance()
        {
            this.Data.set('config.data.app.maintenance', this.maintenance ? '1' : '0');
        }

    },

    methods: {

        getMaintenance()
        {
            return this.Data.get('config.data.app.maintenance', '0') === '1';
        },

        enableMaintenance()
        {
            let query = {
                id: '6c1562c2-e22b-4bfc-a0be-9fd318bb42b2', value: '1'
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('configurations-edit', false, query, options)
                .then(() => this.maintenance = true);
        },

        disableMaintenance()
        {
            let query = {
                id: '6c1562c2-e22b-4bfc-a0be-9fd318bb42b2', value: '0'
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('configurations-edit', false, query, options)
                .then(() => this.maintenance = false);
        }

    },

    renderOn()
    {
        if ( ! this.maintenance ) {
            return null;
        }

        return (
            <div class="col grid grid--row grid--middle">
                <div class="col">
                    <h4>
                        { this.trans('Wartung aktiv') }
                    </h4>
                    <p>
                        { this.trans('Wartungsmodus beenden, dadurch ist die Website wieder für alle Nutzer verfügbar.') }
                    </p>
                    <NButton type="warning" onClick={this.disableMaintenance}>
                        { this.trans('Wartungsmodus deaktivieren') }
                    </NButton>
                </div>
            </div>
        );

    },

    renderOff()
    {
        if ( this.maintenance ) {
            return null;
        }

        return (
            <div class="col grid grid--row grid--middle">
                <div class="col">
                    <h4>
                        { this.trans('Wartung inaktiv') }
                    </h4>
                    <p>
                        { this.trans('Wartungsmodus für die Website aktivieren. Die Website ist dadurch nur noch für Administratoren zugänglich.') }
                    </p>
                    <NButton type="primary" onClick={this.enableMaintenance}>
                        { this.trans('Wartungsmodus aktivieren') }
                    </NButton>
                </div>
            </div>
        );
    },

    render()
    {
        let classList = [
            'dashboard-maintenance', 'dashboard-bar'
        ];

        if ( this.maintenance ) {
            classList.push('dashboard-maintenance--on');
        }

        return (
            <div class={classList}>
                <NLoader visible={this.load} class="grid grid--row">
                    { this.ctor('renderOn')() }
                    { this.ctor('renderOff')() }
                </NLoader>
            </div>
        );
    }

}
