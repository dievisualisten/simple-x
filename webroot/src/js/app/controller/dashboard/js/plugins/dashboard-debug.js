import { Build, Ajax } from "@dievisualisten/simplex";


export default {

    name: 'SxDashboardDebug',

    data()
    {
        return {
            load: false, debug: Build.getDebug()
        }
    },
    watch: {

        debug(debug) {
            Build.setDebug(debug);
        }

    },


    methods: {

        enableDebug()
        {
            let query = {
                value: true
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('system-debug', false, query, options)
                .then(() => this.debug = true);
        },

        disableDebug()
        {
            let query = {
                value: false
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('system-debug', false, query, options)
                .then(() => this.debug = false);
        }

    },

    renderOn()
    {
        if ( ! this.debug ) {
            return null;
        }

        return (
            <div class="grid grid--row grid--middle">
                <div class="col--flex-auto">
                    <h4>{this.trans('Entwicklermodus aktiv')}</h4>
                    <p>{this.trans('Der Entwicklermodus ist zur Zeit aktiviert dadurch werden alle Fehlermeldungen ausgegeben und der Cache ignoriert. Dies kann dazu führen das die Seite langsamer läd.')}</p>
                    <NButton type="warning" onClick={this.disableDebug}>
                        {this.trans('Entwicklermodus deaktivieren')}
                    </NButton>
                </div>
            </div>
        );
    },

    renderOff()
    {
        if ( this.debug ) {
            return null;
        }

        return (
            <div class="grid grid--row grid--middle">
                <div class="col--flex-auto">
                    <h4>{this.trans('Livemodus aktiv')}</h4>
                    <p>{this.trans('Die Website läuft aktuell im Livemodus und wird optimiert ausgegeben. Bilder und Datenbank werden zwischengespeichert, dadurch verringert sich die Ladezeit.')}</p>
                    <NButton type="success" onClick={this.enableDebug}>
                        {this.trans('Entwicklermodus aktivieren')}
                    </NButton>
                </div>
            </div>
        );
    },

    render()
    {
        let classList = [
            'dashboard-debug', 'dashboard-bar'
        ];

        if ( this.debug ) {
            classList.push('dashboard-debug--on');
        }

        return (
            <div class={classList}>
                <NLoader visible={this.load} class="grid grid--row">
                    {this.ctor('renderOn')()}
                    {this.ctor('renderOff')()}
                </NLoader>
            </div>
        );
    }

}
