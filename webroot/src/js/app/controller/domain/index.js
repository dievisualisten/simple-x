import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('domains');

Ajax.bind('domains-import', function (ajax, query, options) {
    let route = Route.get('domains.import');
    return ajax.post(route, Ajax.form(query), options);
});

require("./config/domain-default.config");

import SxDomainIndex from "./js/domain/domain-index";
App.component(SxDomainIndex.name, SxDomainIndex);

import SxDomainEdit from "./js/domain/domain-edit";
App.component(SxDomainEdit.name, SxDomainEdit);
