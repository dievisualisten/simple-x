import DefaultIndex from "../../../default/src/index/index.js"
import { Obj } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxDomainIndex',

    extends: DefaultIndex,

    urls: {
        index: 'domains.index',
        edit: 'domains.edit',
        create: 'domains.create'
    },

    model: 'domain',

    methods: {

    },

    renderTable()
    {
        let updateEvents = [
            'domains-edit'
        ];

        let props = {
            indexQuery: 'domains-index',
            copyQuery: 'domains-copy',
            deleteQuery: 'domains-delete',
            group: ['domains'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'option',
                        prop: 'type',
                        label: this.trans('Liste'),
                        options: Config.domaintypes(),
                        optionLabel: '$value',
                        optionValue: '$value',
                        sort: true,
                        // filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'key',
                        label: this.trans('Schlüssel'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'parameter',
                        label: this.trans('Parameter'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'info',
                        label: this.trans('Info'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'sequence',
                        label: this.trans('Sortierung'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'active',
                        label: this.trans('Aktiv'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_default',
                        label: this.trans('Standard'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,

                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'type',
                        label: this.trans('Liste')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'key',
                        label: this.trans('Schlüssel')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'parameter',
                        label: this.trans('Parameter')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'info',
                        label: this.trans('Info')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'sequence',
                        label: this.trans('Sortierung')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'active',
                        label: this.trans('Aktiv')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_default',
                        label: this.trans('Standard')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxDomainEdit',
            width: '960px',
            height: '100%',
            localized: false,
            showQuery: 'domains-show',
            editQuery: 'domains-edit',
            newTitle: this.trans('Eintrag erstellen'),
            editTitle: this.trans('Eintrag bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },


    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
