sx.Form.set('sx-domain-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Listeneintrag'),
                icon: 'fa fa-list'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'active'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'active', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Listeneintrag ist aktiv')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Liste'),
                        prop: 'type'
                    },
                    content: {
                        'SxSelectDomaintype:0': {
                            model: {
                                path: 'type'
                            },
                            $props: {
                                allowCreate: true
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Schlüssel'),
                        prop: 'key'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'key'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Wert'),
                        prop: 'value'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'value'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Parameter'),
                        prop: 'parameter'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'parameter'
                            }
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Sortierung'),
                        prop: 'sequence'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'sequence'
                            }
                        }
                    }
                },
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Info'),
                        prop: 'info'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'info'
                            }
                        }
                    }
                },
            }
        }
    },
    /* Konfiguration end */

]);
