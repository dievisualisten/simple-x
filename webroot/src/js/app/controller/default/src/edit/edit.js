import { Data, Event } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    inject: {

        NDataform: {
            default: undefined
        }

    },

    props: {

        value: {
            default()
            {
                return {};
            },
            type: [Object]
        },

        errors: {
            default()
            {
                return {};
            },
            type: [Object]
        }

    },

    computed: {

        i18n()
        {
            return this.Obj.get(this.value, 'baselocale._locale',
                this.NDataform.locale) !== this.NDataform.locale;
        },

        scope()
        {
            return this;
        }

    },

    methods: {

        queryElements(query = {})
        {
            query = this.Obj.assign({}, query, {
                _locale: this.NDataform.locale
            });

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'system-element'),
                onDone: () => this.NDataform.setLoad(false, 'system-element')
            };

            Ajax.call('elements', false, query, options)
                .then((res) => this.elements = res.data.data, () => this.NDataform.$emit('close'));
        },

        queryMenuTree()
        {
            if ( Data.has('menus-tree') ) {
                return this.menus = Data.get('menus-tree.data') || [];
            }

            let query = {
                _locale: this.NDataform.locale
            };

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'menus-tree'),
                onDone: () => this.NDataform.setLoad(false, 'menus-tree')
            };

            Ajax.call('menus-tree', false, query, options)
                .then((res) => this.menus = res.data.data, () => this.NDataform.$emit('close'));
        },

    },

    provide()
    {
        return {
            SxEditController: this
        };
    },

    data()
    {
        return {
            menus: [], elements: []
        };
    },

    beforeMount()
    {
        Event.bind('store:menus-tree', (value) => {
            this.menus = value.data;
        }, this._.uid);
    },

    beforeDestroy()
    {
        this.NDataform.$off('ready');

        Event.unbind('store:menus-tree', this._.uid);
    },

    ready()
    {
        // Ready
    },

}
