
import { Obj, Any, Data, Arr } from "@kizmann/pico-js";
import { Auth } from "@dievisualisten/simplex";
import { RouteChangeConfirm } from "../../../default/src/mixins";

export default {

    model: 'component',

    urls: {
        index: 'component.index',
        edit: 'component.edit',
        create: 'component.create'
    },

    mixins: [
        RouteChangeConfirm
    ],

    beforeRouteUpdate(...args)
    {
        return RouteChangeConfirm.beforeRouteLeave(this, ...args);
    },

    beforeRouteLeave(...args)
    {
        return RouteChangeConfirm.beforeRouteLeave(this, ...args);
    },

    props: {

        routing: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        forceShowTree: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        showTree: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        forceShowInfo: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        showInfo: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        disableEdit: {
            default()
            {
                return false;
            },
            type: [Boolean]
        }

    },

    computed: {

        acos()
        {
            let acoIds = Auth.perms([
                'models', this.ctor('model'), 'acoIds'
            ], []);

            return Arr.filter(Data.get('acos.data'), (aco) => {
                return Arr.has(acoIds, aco.id);
            });
        },

    },

    provide()
    {
        return {
            SxIndexController: this
        };
    },

    data()
    {
        return {
            formRef: null, veID: null, veCurrent: null, veCloseHook: null, veRouteHook: null
        };
    },

    watch: {

        '$route': function () {
            this.updateRoute(this.$route);
        },

        'formId': function () {
            this.veID = this.formId;
        },

    },

    mounted()
    {
        if ( this.routing ) {
            this.updateRoute(this.$route);
        }
    },

    beforeUnmount()
    {
        this.Event.fire(this.ctor('name') + ':destroy');
    },

    methods: {

        showIndex()
        {
            if ( ! this.routing ) {
                return this.hideEditModal('defaultForm');
            }

            this.$router.push({
                'name': this.ctor('urls.index')
            });
        },

        newItem(id = null)
        {
            if ( ! this.routing ) {
                return this.showEditModal('defaultForm', id);
            }

            this.$router.push({
                'name': this.ctor('urls.create')
            });
        },

        editItem(id = null)
        {
            if ( this.disableEdit ) {
                return;
            }

            if ( ! this.routing ) {
                return this.showEditModal('defaultForm', id);
            }

            this.$router.push({
                'name': this.ctor('urls.edit'), params: { id }
            });
        },

        showEditModal(ref, id)
        {
            if ( this.disableEdit ) {
                return;
            }

            if ( ! ref ) {
                ref = 'defaultForm';
            }

            this.formRef = ref;

            this.$refs[ref].openModal(id);
        },

        hideEditModal(ref)
        {
            if ( ! ref ) {
                ref = 'defaultForm';
            }

            this.formRef = null;

            this.$refs[ref].closeModal();
        },

        updateRoute(route)
        {
            let id = Obj.get(route.params, 'id', null);

            if ( this.$route.name === this.ctor('urls.create') ) {
                this.showEditModal('defaultForm', id);
            }

            if ( this.$route.name === this.ctor('urls.edit') ) {
                this.showEditModal('defaultForm', id);
            }

            if ( this.$route.name === this.ctor('urls.index') ) {
                this.hideEditModal('defaultForm');
            }
        },

        setCurrent(value)
        {
            if ( value && this.$refs.info ) {
                this.$refs.info.setValue(value);
            }

            this.veCurrent = value;
        }

    }

}
