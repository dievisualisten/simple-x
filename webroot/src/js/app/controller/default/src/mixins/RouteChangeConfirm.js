import { Locale } from "@kizmann/pico-js";

export default {

    data()
    {
        return {
            showRouteConfirm: false,
            changedRouteConfirm: false,
            routePromise: null,
            routePromiseResolve: null,
        }
    },

    methods: {

        hasChanged()
        {
            return this.changedRouteConfirm;
        },

        emitChange()
        {
            this.changedRouteConfirm = true;
        },

        resetChange()
        {
            this.changedRouteConfirm = false;
        },

    },

    beforeRouteLeave(scope, from, to, next)
    {
        scope.showRouteConfirm = scope.changedRouteConfirm;

        if ( ! scope.showRouteConfirm ) {

            // Hide current ref
            scope.hideEditModal(scope.formRef);

            return next(true);
        }

        scope.routePromise = new Promise((routePromiseResolve) => {
            scope.routePromiseResolve = routePromiseResolve;
        });

        scope.routePromise.then((result) => {

            scope.changedRouteConfirm = ! result;

            if ( result && scope.formRef ) {
                scope.hideEditModal(scope.formRef);
            }

            result ? next(true) : next(false);
        });

        return scope.routePromise;
    },

    renderRouteConfirm()
    {
        let props = {
            listen: false,
            confirmText: Locale.trans('Discard changes')
        };

        props.onConfirm = () => {
            this.routePromiseResolve(true);
        };

        props.onAbort = () => {
            this.routePromiseResolve(false);
        };

        return (
            <NConfirm vModel={this.showRouteConfirm} {...props}>
                { Locale.trans('Do you want to discard changes?') }
            </NConfirm>
        );
    }

}