sx.Form.set('sx-configuration-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Konfiguration'),
                icon: 'fa fa-cog'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Wert'),
                        prop: 'value'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'value'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Standardwert'),
                        prop: 'default_value'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'default_value'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Beschreibung'),
                        prop: 'description'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'description'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        prop: 'is_resource'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'is_resource', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Als Resource speichern')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
