import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('configurations');

Ajax.bind('configurations-tree', function (ajax, query, options) {
    let route = Route.get('configurations.tree', query, query);
    return ajax.get(route, options);
});


Ajax.bind('configurations-import', function (ajax, query, options) {
    let route = Route.get('configurations.import');
    return ajax.post(route, Ajax.form(query), options);
});

require('./config/configuration-default.config');

import ConfigurationIndex from "./js/configuration/configuration-index";
App.component(ConfigurationIndex.name, ConfigurationIndex);

import ConfigurationEdit from "./js/configuration/configuration-edit";
App.component(ConfigurationEdit.name, ConfigurationEdit);

