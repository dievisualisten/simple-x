import DefaultIndex from "../../../default/src/index/index.js"
import { Obj } from "@kizmann/pico-js";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxConfigurationIndex',

    extends: DefaultIndex,

    urls: {
        index: 'configurations.index',
        edit: 'configurations.edit',
        create: 'configurations.create'
    },

    model: 'configuration',

    renderTable()
    {
        let updateEvents = [
            'configurations-edit',
            'configurations-delete'
        ];

        let props = {
            indexQuery: 'configurations-index',
            copyQuery: 'configurations-copy',
            deleteQuery: 'configurations-delete',
            group: ['configurations'],
            updateEvents: updateEvents,
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'default_value',
                        label: this.trans('Standardwert'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_resource',
                        label: this.trans('Resource'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Wert')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'default_value',
                        label: this.trans('Standardwert')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_resource',
                        label: this.trans('Resource')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxConfigurationEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'configurations-show',
            editQuery: 'configurations-edit',
            newTitle: this.trans('Konfiguration erstellen'),
            editTitle: this.trans('Konfiguration bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
