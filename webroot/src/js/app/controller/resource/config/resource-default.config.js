sx.Form.set('sx-resource-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Resource'),
                icon: 'fa fa-boxes'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Typ'),
                        prop: 'type'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'type'
                            },
                            $props: {
                                clearable: true,
                                allowCreate: true,
                                domain: 'resourcetype'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Beschreibung'),
                        prop: 'description'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'description'
                            },
                        }
                    }
                },
                'NFormItem:3': {
                    vShow: function (value) {
                        return value.type === 'model';
                    },
                    $props: {
                        label: pi.Locale.trans('Zugriffsebene'),
                        prop: 'model_accesslevel',
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'model_accesslevel'
                            },
                        }
                    }
                },
                'NFormItem:4': {
                    vShow: function (value) {
                        return value.type === 'model';
                    },
                    $props: {
                        label: pi.Locale.trans('Model'),
                        prop: 'model',
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'model'
                            },
                        }
                    }
                },
                'NFormItem:5': {
                    vShow: function (value) {
                        return value.type === 'url';
                    },
                    $props: {
                        label: pi.Locale.trans('URL'),
                        prop: 'url'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'url'
                            },
                        }
                    }
                },
                'NFormItem:6': {
                    vShow: function (value) {
                        return value.type === 'action';
                    },
                    $props: {
                        label: pi.Locale.trans('Controller'),
                        prop: 'controller'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'controller'
                            },
                        }
                    }
                },
                'NFormItem:7': {
                    vShow: function (value) {
                        return value.type === 'action';
                    },
                    $props: {
                        label: pi.Locale.trans('Action'),
                        prop: 'action'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'action'
                            },
                        }
                    }
                },
                'NFormItem:8': {
                    vShow: function (value) {
                        return value.type === 'can';
                    },
                    $props: {
                        label: pi.Locale.trans('Recht'),
                        prop: 'can'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'can'
                            },
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'role',
                sort: 20,
                label: pi.Locale.trans('Rollen'),
                icon: 'fa fa-user-tag'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'roles'
                    },
                    content: {
                        'SxTransferRole:0': {
                            model: {
                                path: 'roles'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
