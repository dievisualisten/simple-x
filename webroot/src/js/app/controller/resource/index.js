import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('resources');

require('./config/resource-default.config');

import SxResourceIndex from "./js/resource/resource-index";
App.component(SxResourceIndex.name, SxResourceIndex);

import SxResourceEdit from "./js/resource/resource-edit";
App.component(SxResourceEdit.name, SxResourceEdit);
