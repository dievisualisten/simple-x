import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxResourceIndex',

    extends: DefaultIndex,

    urls: {
        index: 'resources.index',
        edit: 'resources.edit',
        create: 'resources.create'
    },

    model: 'resource',

    renderTable()
    {
        let updateEvents = [
            'resources-edit'
        ];

        let props = {
            indexQuery: 'resources-index',
            copyQuery: 'resources-copy',
            deleteQuery: 'resources-delete',
            group: ['resources'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });


        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'description',
                        label: this.trans('Beschreibung'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'type',
                        label: this.trans('Typ'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'model',
                        label: this.trans('Model'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'model_accesslevel',
                        label: this.trans('Zugriffsebene'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'controller',
                        label: this.trans('Controller'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'action',
                        label: this.trans('Action'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'url',
                        label: this.trans('Url'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'description',
                        label: this.trans('Beschreibung')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'type',
                        label: this.trans('Typ')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'model_accesslevel',
                        label: this.trans('Zugriffsebene')
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxResourceEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'resources-show',
            editQuery: 'resources-edit',
            newTitle: this.trans('Resource erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
