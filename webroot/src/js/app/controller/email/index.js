import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('emails');

require('./config/email-default.config');

import EmailIndex from "./js/email/email-index";
App.component(EmailIndex.name, EmailIndex);

import EmailEdit from "./js/email/email-edit";
App.component(EmailEdit.name, EmailEdit);
