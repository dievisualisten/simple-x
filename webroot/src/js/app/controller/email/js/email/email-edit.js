import DefaultEdit from "../../../default/src/edit/edit";
import { Arr } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxEmailEdit',

    extends: DefaultEdit,

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-email-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        )
    }

}
