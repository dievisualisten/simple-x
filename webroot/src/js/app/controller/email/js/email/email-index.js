import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxEmailIndex',

    extends: DefaultIndex,

    urls: {
        index: 'emails.index',
        edit: 'emails.edit',
        create: 'emails.create'
    },

    model: 'email',

    renderTable()
    {
        let updateEvents = [
            'emails-edit'
        ];

        let props = {
            indexQuery: 'emails-index',
            copyQuery: 'emails-copy',
            deleteQuery: 'emails-delete',
            group: ['emails'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'from_email',
                        label: this.trans('Absender'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'email_to',
                        label: this.trans('Empfänger'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'from_email',
                        label: this.trans('Absender'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'email_to',
                        label: this.trans('Empfänger'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxEmailEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'emails-show',
            editQuery: 'emails-edit',
            newTitle: this.trans('E-Mail Konfiguration erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
