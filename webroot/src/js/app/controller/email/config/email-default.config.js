sx.Form.set('sx-email-default', [

    /* Standard start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Allgemein'),
                icon: 'fa fa-envelope'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Standard'),
                        prop: 'is_default'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'is_default', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Diese Konfiguration ist die Standardkonfiguration')
                            ]
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Zuordnung'),
                        prop: 'aco_id'
                    },
                    content: {
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Standard end */

    /* Einstellungen start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'settings',
                sort: 20,
                label: pi.Locale.trans('Einstellungen'),
                icon: 'fa fa-mail-bulk'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Absender Name'),
                        prop: 'from_name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'from_name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Absender E-Mail-Adresse'),
                        prop: 'from_email'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'from_email'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Empfänger E-Mail-Adresse'),
                        prop: 'email_to'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email_to'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('BCC'),
                        prop: 'email_bcc'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email_bcc'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Return E-Mail'),
                        prop: 'return_path'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'return_path'
                            }
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Antwort E-Mail-Adresse'),
                        prop: 'reply_to'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'reply_to'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Standard end */

    /* SMTP start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'smtp',
                sort: 30,
                label: pi.Locale.trans('SMTP'),
                icon: 'fa fa-server'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Server'),
                        prop: 'smtp_host'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'smtp_host'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Port'),
                        prop: 'smtp_port'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'smtp_port'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Benutzername'),
                        prop: 'smtp_username'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'smtp_username'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Password'),
                        prop: 'smtp_password'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'smtp_password'
                            }
                        }
                    }
                }
            }
        }
    },
    /* SMTP end */

    /* IMAP start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'imap',
                sort: 40,
                label: pi.Locale.trans('IMAP'),
                icon: 'fa fa-server'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Server'),
                        prop: 'imap_host'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'imap_host'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Port'),
                        prop: 'imap_port'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'imap_port'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Benutzername'),
                        prop: 'imap_username'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'imap_username'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Password'),
                        prop: 'imap_password'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'imap_password'
                            }
                        }
                    }
                }
            }
        }
    },
    /* IMAP end */

]);


sx.Form.set('sx-email-email2', [

    /* Standard start */
    {
        element: 'n-tabs-item',
        $props: {
            name: 'default',
            sort: 10,
            label: pi.Locale.trans('Allgemein'),
            icon: 'fa fa-envelope'
        },
        content: [
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Name'),
                    prop: 'name'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'name',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Standard'),
                    prop: 'is_default'
                },
                content: [
                    {
                        element: 'n-checkbox',
                        model: 'checked',
                        prop: 'is_default',
                        fallback: false,
                        content: pi.Locale.trans('Diese Konfiguration ist die Standardkonfiguration')
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Zuordnung'),
                    prop: 'aco_id'
                },
                content: [
                    {
                        element: 'sx-select-aco',
                        prop: 'aco_id'
                    }
                ]
            },
        ]
    },
    /* Standard end */

    /* Einstellungen start */
    {
        element: 'n-tabs-item',
        $props: {
            name: 'settings',
            sort: 20,
            label: pi.Locale.trans('Einstellungen'),
            icon: 'fa fa-mail-bulk'
        },
        content: [
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Absender Name'),
                    prop: 'from_name'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'from_name',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Absender E-Mail-Adresse'),
                    prop: 'from_email'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'from_email',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Empfänger E-Mail-Adresse'),
                    prop: 'email_to'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'email_to',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('BCC'),
                    prop: 'email_bcc'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'email_bcc',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Return E-Mail'),
                    prop: 'return_path'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'return_path',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Antwort E-Mail-Adresse'),
                    prop: 'reply_to'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'reply_to',
                        fallback: ''
                    },
                ]
            },
        ]
    },
    /* Einstellungen end */

    /* SMTP start */
    {
        element: 'n-tabs-item',
        $props: {
            name: 'smtp',
            sort: 30,
            label: pi.Locale.trans('SMTP'),
            icon: 'fa fa-server'
        },
        content: [
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Server'),
                    prop: 'smtp_host'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'smtp_host',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Port'),
                    prop: 'smtp_port'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'smtp_port',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Benutzername'),
                    prop: 'smtp_username'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'smtp_username',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Password'),
                    prop: 'smtp_password'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'smtp_password',
                        fallback: ''
                    },
                ]
            },
        ]
    },
    /* SMTP end */

    /* IMAP start */
    {
        element: 'n-tabs-item',
        $props: {
            name: 'imap',
            sort: 40,
            label: pi.Locale.trans('IMAP'),
            icon: 'fa fa-server'
        },
        content: [
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Server'),
                    prop: 'imap_host'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'imap_host',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Port'),
                    prop: 'imap_port'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'imap_port',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Benutzername'),
                    prop: 'imap_username'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'imap_username',
                        fallback: ''
                    },
                ]
            },
            {
                element: 'n-form-item',
                $props: {
                    label: pi.Locale.trans('Password'),
                    prop: 'imap_password'
                },
                content: [
                    {
                        element: 'n-input',
                        prop: 'imap_password',
                        fallback: ''
                    },
                ]
            },
        ]
    },
    /* IMAP end */

]);
