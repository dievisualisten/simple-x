import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('newsletterrecipients');

require('./config/newsletterrecipient-default.config');

import SxNewsletterrecipientIndex from "./js/newsletterrecipient/newsletterrecipient-index";
App.component(SxNewsletterrecipientIndex.name, SxNewsletterrecipientIndex);

import SxNewsletterrecipientEdit from "./js/newsletterrecipient/newsletterrecipient-edit";
App.component(SxNewsletterrecipientEdit.name, SxNewsletterrecipientEdit);
