sx.Form.set('sx-newsletterrecipient-default', [
    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Empfängerdaten'),
                icon: 'fa fa-id-card'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Adresse'),
                        prop: 'email'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Anrede'),
                        prop: 'salutation'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'salutation'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Sprache'),
                        prop: 'language'
                    },
                    content: {
                        'SxSelectLocale:0': {
                            model: {
                                path: 'language'
                            },
                            $props: {
                                clearable: true
                            },
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Aktiviert'),
                        prop: 'approved'
                    },
                    content: {
                        'NSwitch:0': {
                            model: {
                                path: 'approved', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Der Empfänger hat nachweislich dem Erhalt von Newslettern zugestimmt')
                            ]
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Bestätigungsmail'),
                        prop: 'approved_mail'
                    },
                    content: {
                        'NSwitch:0': {
                            model: {
                                path: 'approved_mail', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Sende Empfänger eine Bestätigungsmail')
                            ]
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Deaktiviert'),
                        prop: 'unsubscribed'
                    },
                    content: {
                        'NSwitch:0': {
                            model: {
                                path: 'unsubscribed', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Der Empfänger hat seine Zustimmung widerrufen')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'interests',
                sort: 20,
                label: pi.Locale.trans('Interessen'),
                icon: 'fa fa-users-class'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'newsletterinterests'
                    },
                    content: {
                        'SxTransferNewsletterinterest:0': {
                            model: {
                                path: 'newsletterinterests', fallback: []
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'person',
                sort: 40,
                label: pi.Locale.trans('Person'),
                icon: 'fa fa-id-card'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Anrede'),
                        prop: 'person.salutation'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'person.salutation'
                            },
                            $props: {
                                clearable: true,
                                domain: 'salutation'
                            },
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Titel'),
                        prop: 'person.title'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'person.title'
                            },
                            $props: {
                                clearable: true,
                                domain: 'title'
                            },
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Vorname'),
                        prop: 'person.firstname'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'person.firstname'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Nachname'),
                        prop: 'person.lastname'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'person.lastname'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Familienstand'),
                        prop: 'person.familystatus'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'person.familystatus'
                            },
                            $props: {
                                clearable: true,
                                domain: 'familystatus'
                            },
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Geburtsdatum'),
                        prop: 'person.birthday'
                    },
                    content: {
                        'NDatepicker:0': {
                            model: {
                                path: 'person.birthday'
                            },
                            $props: {
                                clearable: true
                            },
                        }
                    }
                },
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Notiz'),
                        prop: 'person.note'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'person.note'
                            },
                            $props: {
                                minRows: 1
                            },
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'address',
                sort: 50,
                label: pi.Locale.trans('Adresse'),
                icon: 'fa fa-mailbox'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Strasse'),
                        prop: 'address.street'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.street'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Hausnummer'),
                        prop: 'address.number'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.number'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Postfach'),
                        prop: 'address.box'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.box'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('PLZ'),
                        prop: 'address.zip'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.zip'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Land'),
                        prop: 'address.country'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'address.country'
                            },
                            $props: {
                                clearable: true,
                                domain: 'country'
                            },
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Längengrad'),
                        prop: 'address.lat'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.lat'
                            }
                        }
                    }
                },
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Breitengrad'),
                        prop: 'address.lon'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.lon'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'communication',
                sort: 60,
                label: pi.Locale.trans('Kommunikation'),
                icon: 'fa fa-user-tag'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Telefon'),
                        prop: 'communication.phone'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.phone'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Fax'),
                        prop: 'communication.fax'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.fax'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Mobil'),
                        prop: 'communication.cell'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.cell'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Adresse'),
                        prop: 'communication.email'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.email'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Website'),
                        prop: 'communication.website'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.website'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'bankaccount',
                sort: 70,
                label: pi.Locale.trans('Bankdaten'),
                icon: 'fa fa-piggy-bank'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Inhaber'),
                        prop: 'bankaccount.holder'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'bankaccount.holder'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Bankinstitut'),
                        prop: 'bankaccount.bankname'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'bankaccount.bankname'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('IBAN'),
                        prop: 'bankaccount.iban'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'bankaccount.iban'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('BIC'),
                        prop: 'bankaccount.bic'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'bankaccount.bic'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
