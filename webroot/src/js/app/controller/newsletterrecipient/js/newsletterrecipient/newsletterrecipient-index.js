import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxNewsletterrecipientIndex',

    extends: DefaultIndex,

    urls: {
        index: 'newsletterrecipients.index',
        edit: 'newsletterrecipients.edit',
        create: 'newsletterrecipients.create'
    },

    model: 'newsletterrecipient',

    renderTable()
    {
        let updateEvents = [
            'newsletterrecipients-edit'
        ];

        let props = {
            indexQuery: 'newsletterrecipients-index',
            deleteQuery: 'newsletterrecipients-delete',
            group: ['newsletterrecipients'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'email',
                        label: this.trans('E-Mail-Adresse'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'salutation',
                        label: this.trans('Anrede'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'person.name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'source',
                        label: this.trans('Quelle'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'approved',
                        label: this.trans('Bestätigt'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'unsubscribed',
                        label: this.trans('Abgemeldet'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'hardbounced',
                        label: this.trans('Hardbounced'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'from_email',
                        label: this.trans('Absender'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'email_to',
                        label: this.trans('Empfänger'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxNewsletterrecipientEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'newsletterrecipients-show',
            editQuery: 'newsletterrecipients-edit',
            newTitle: this.trans('Empfänger erstellen'),
            editTitle: this.trans('Empfänger bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});

