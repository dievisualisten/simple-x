import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('newsletters');

require('./config/newsletter-default.config');

import SxNewsletterIndex from "./js/newsletter/newsletter-index";
App.component(SxNewsletterIndex.name, SxNewsletterIndex);

import SxNewsletterEdit from "./js/newsletter/newsletter-edit";
App.component(SxNewsletterEdit.name, SxNewsletterEdit);
