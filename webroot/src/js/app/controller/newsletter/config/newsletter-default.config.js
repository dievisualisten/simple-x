sx.Form.set('sx-newsletter-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Mailing'),
                icon: 'fa fa-envelope'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Interner Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Betreff'),
                        prop: 'subject'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'subject'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Konfiguration'),
                        prop: 'email_id'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email_id'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Template'),
                        prop: 'template'
                    },
                    content: {
                        'NSelect:0': {
                            model: {
                                path: 'description'
                            },
                            $props: {
                                options: '$$scope.elements',
                                optionsValue: '$value.value',
                                optionsLabel: '$value.label'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Sprachfilter'),
                        tooltip: pi.Locale.trans('Wenn Sie eine Sprache wählen, wird der Newsletter (in dieser Sprache) auch nur an Empfänger dieser Sprache versandt'),
                        prop: 'language'
                    },
                    content: {
                        'SxSelectLocale:0': {
                            model: {
                                path: 'language'
                            },
                            $props: {
                                clearable: true
                            }
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Versand'),
                        prop: 'to_send'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'email_id', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Diesen Newsletter jetzt versenden')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'interests',
                sort: 20,
                label: pi.Locale.trans('Interessen'),
                icon: 'fa fa-users-class'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Schränken Sie die Empfänger durch die Auswahl von Interessen-Listen ein'),
                        prop: 'newsletterinterests'
                    },
                    content: {
                        'SxTransferNewsletterinterest:0': {
                            model: {
                                path: 'newsletterinterests', fallback: []
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'rights',
                sort: 50,
                label: pi.Locale.trans('Rechte'),
                icon: 'fa fa-key'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Dieses Mailing gehört'),
                        prop: 'aco_id'
                    },
                    content: {
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'testing',
                sort: 60,
                label: pi.Locale.trans('Testversand'),
                icon: 'fa fa-code'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Test E-Mail-Adresse'),
                        prop: 'testreciveremail'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'testreciveremail'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
