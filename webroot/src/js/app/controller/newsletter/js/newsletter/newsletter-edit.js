import DefaultEdit from "../../../default/src/edit/edit";
import { Arr } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxNewsletterEdit',

    extends: DefaultEdit,

    ready()
    {
        this.queryElements({
            path: 'email/html/newsletter'
        });
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-newsletter-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        )
    }

}
