import Vue from "vue";

Vue.Ajax.bind('system-extras', function (ajax, query, options) {
    let route = Vue.Route.get('system.extras', query);
    return ajax.get(route, options);
});

import CodeEditor from "./src/editor/editor.vue";
Vue.component(CodeEditor.name, CodeEditor);

import CodeTest from "./src/test/test.vue";
Vue.component(CodeTest.name, CodeTest);

import SxInput from "./src/test/input.vue";
Vue.component(SxInput.name, SxInput);
