sx.Form.set('sx-article-custom', [

    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'teaser.show', '0') === '1';
            },
            $props: {
                name: 'teaser',
                sort: 10,
                label: pi.Locale.trans('Teaser'),
                icon: 'fa fa-ballot'
            },
            content: {
                'NFormItem:0': {
                    vIf: function () {
                        return pi.Obj.get(this.pagetype,
                            'moretext.show', '0') === '1';
                    },
                    $props: {
                        label: pi.Locale.trans('Linktext')
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'moretext'
                            }
                        }
                    }
                },

                'NSwitch:0': {
                    vIf: function () {
                        return pi.Obj.get(this.pagetype,
                            'has_teasertext.show', '0') === '1';
                    },
                    model: {
                        path: 'has_teasertext'
                    },
                    content: [
                        pi.Locale.trans('Individueller Teasertext')
                    ]
                },

                'NFormItem:1': {
                    vIf: function (value) {
                        return !!value.has_teasertext;
                    },
                    $props: {
                        label: pi.Locale.trans('Teasertext')
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'teasertext'
                            }
                        }
                    }
                },

                'NSwitch:1': {
                    vIf: function () {
                        return pi.Obj.get(this.pagetype,
                            'has_teasertext2.show', '0') === '1';
                    },
                    model: {
                        path: 'has_teasertext2'
                    },
                    content: [
                        pi.Locale.trans('Zweiter individueller Teasertext')
                    ]
                },


                'NFormItem:2': {
                    vIf: function (value) {
                        return !!value.has_teasertext2;
                    },
                    $props: {
                        label: pi.Locale.trans('Zweiter Teasertext')
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'teasertext2'
                            }
                        }
                    }
                },

            }
        },
        // </ NTabsItem:0>
    },

    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'price.show', '0') === '1';
            },
            $props: {
                name: 'price',
                sort: 20,
                label: pi.Locale.trans('Preise'),
                icon: 'fa fa-euro-sign'
            },
            content: {

                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Preis / Min. Preis')
                    },
                    content: {
                        'NInputNumber:0': {
                            model: {
                                path: 'extended_data_cfg.price.min'
                            },
                            $props: {
                                precision: 2,
                                min: 0,
                                decimals: ',',
                                clearable: true,
                                clearValue: ''
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Max. Preis')
                    },
                    content: {
                        'NInputNumber:0': {
                            model: {
                                path: 'extended_data_cfg.price.max'
                            },
                            $props: {
                                precision: 2,
                                min: 0,
                                decimals: ',',
                                clearable: true,
                                clearValue: ''
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Preisprefix')
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'extended_data_cfg.price.prefix'
                            },
                            $props: {
                                domain: 'priceprefix',
                                clearable: true
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Preissuffix')
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'extended_data_cfg.price.suffix'
                            },
                            $props: {
                                domain: 'pricesuffix',
                                clearable: true
                            }
                        }
                    }
                }
            }
        },
        // </ NTabsItem:0>
    }
]);
