sx.Form.set('sx-article-default', [
    {
        // <NTabsItem:-1>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.domain.show', '0') === '1';
            },
            $props: {
                name: 'domain',
                sort: 20,
                label: pi.Locale.trans('Domain'),
                icon: 'fa fa-sitemap'
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    vAwait: '$$scope.menus',
                    $props: {
                        label: pi.Locale.trans('Die Startseite gehört zu folgender Domain')
                    },
                    content: {
                        'NSelect:0': {
                            model: {
                                path: 'extended_data_cfg.domain_id'
                            },
                            $props: {
                                options: '$$scope.domains',
                                optionsLabel: '$value.title',
                                optionsValue: '$value.id'
                            }
                        }
                    }
                }
                // </ NFormItem:0>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.menu.show', '0') === '1' && this.value;
            },
            $props: {
                name: 'menu',
                sort: 20,
                label: pi.Locale.trans('Verlinkung'),
                icon: 'fa fa-sitemap',
                relative: true,
                keep: true
            },
            content: {

                // <NTable:0>
                'NTable:0': {
                    vAwait: '$$scope.menus',
                    $props: {
                        items: '$$scope.menus',
                        expanded: '$$value.tabparents_expanded',
                        viewportHeight: true,
                        itemHeight: 38,
                        allowCurrent: false,
                        allowSelect: false,
                        renderExpand: true,
                        renderSelect: false,
                        useKeys: true,
                        //loadingDelay: 25
                    },
                    content: function (render) {

                        let self = this;

                        let columns = {

                            // <NTableColumn:-1>
                            'NTableColumn:-1': {
                                $props: {
                                    type: 'string',
                                    prop: 'title',
                                    label: pi.Locale.trans('Seite verlinken als ...'),
                                    fluid: true
                                },
                                content: function () {
                                    return function(props) { return Vue.h(Vue.resolveComponent('SxMenuTreeNode'), { item: props.item }) };
                                }
                            }
                            // </ NTableColumn:-1>

                        };

                        pi.Arr.each(this.teasers, function (teaser) {

                            var tooltip = teaser.name;

                            if ( teaser.tooltip !== '0' ) {
                                tooltip = teaser.tooltip;
                            }

                            var disabled = function () {
                                return function (row) {
                                    return !self.canMatrixCheck(row, teaser);
                                };
                            };

                            // <NTableColumn:-1>
                            columns['NTableColumn:' + teaser.value] = {
                                // key: teaser.value,
                                model: {
                                    path: 'tabparents'
                                },
                                $props: {
                                    type: 'matrix',
                                    // prop: 'teaser',
                                    matrixProp: 'teaser',
                                    align: 'center',
                                    fixedWidth: 100,
                                    label: teaser.name,
                                    matrix: teaser.value,
                                    tooltip: tooltip,
                                    disabled: disabled
                                }
                            };
                            // </ NTableColumn:-1>

                        });

                        return columns;
                    }
                }
                // </ NTable:0>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.media.show', '0') === '1';
            },
            vAwait: '$$value.id',
            $props: {
                name: 'media',
                sort: 30,
                label: pi.Locale.trans('Medien'),
                icon: 'fa fa-image',
                relative: true,
                keep: true
            },
            content: {

                // <SxAttachmentIndex:0>
                'SxAttachmentIndex:0': {
                    vAwait: '$$value.media',
                    $props: {
                        forceShowInfo: true,
                        routing: false,
                    },
                    content: function () {

                        var zones = {};

                        pi.Arr.each(this.dropzones, function (dropzone) {

                            // <SxAttachedItems:index>
                            zones['SxAttachedItems:' + dropzone._key] = {
                                vIf: function () {
                                    return pi.Obj.get(this.activezones,
                                        [dropzone._key, 'show'], '0') === '1';
                                },
                                $props: {
                                    prop: dropzone._key,
                                    label: dropzone.name,
                                    media: '$$value.media'
                                }
                            };
                            // </ SxAttachedItems:index>

                        }, {});

                        return zones;
                    }
                }
                // </ SxAttachmentIndex:0>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.formularconfig.show', '0') !== '0';
            },
            $props: {
                name: 'formularconfig',
                sort: 40,
                label: pi.Locale.trans('Formular'),
                icon: 'fa fa-clipboard-list'
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Formularconfig'),
                        prop: 'formularconfig_id',
                    },
                    content: {

                        // <SxSelectFormularconfig:0>
                        'SxSelectFormularconfig:0': {
                            model: {
                                path: 'formularconfig_id'
                            }
                        }
                        // </ SxSelectFormularconfig:0>

                    }
                }
                // </ NFormItem:0>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.event.show', '0') !== '0';
            },
            $props: {
                name: 'event',
                sort: 50,
                label: pi.Locale.trans('Event'),
                icon: 'fa fa-calendar'
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Eventkategorie'),
                        prop: 'extended_data_cfg.event.eventlabel'
                    },
                    content: {

                        // <SxSelectFormularconfig:0>
                        'SxSelectDomain:0': {
                            model: {
                                path: 'extended_data_cfg.event.eventlabel'
                            },
                            $props: {
                                domain: 'eventlabel'
                            }
                        }
                        // </ SxSelectFormularconfig:0>

                    }
                },
                // </ NFormItem:0>

                // <div:1>
                'div:1': {
                    class: 'grid grid--row grid--wrap grid--15',
                    content: {

                        // <NFormItem:0>
                        'NFormItem:0': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Terminbeginn'),
                                prop: 'extended_data_cfg.event.start_time'
                            },
                            content: {

                                // <NButtonGroup:0>
                                'NButtonGroup:0': {
                                    content: {

                                        // <NDatepicker:0>
                                        'NDatepicker:0': {
                                            model: {
                                                path: 'extended_data_cfg.event.start_date'
                                            },
                                            $props: {
                                                clearable: true
                                            },
                                        },
                                        // </ NDatepicker:0>

                                        // <NTimepicker:1>
                                        'NTimepicker:1': {
                                            model: {
                                                path: 'extended_data_cfg.event.start_time',
                                            },
                                            $props: {
                                                clearable: true
                                            },
                                        }
                                        // </ NTimepicker:1>

                                    }
                                }
                                // </ NButtonGroup:0>

                            }
                        },
                        // </ NFormItem:0>

                        // <NFormItem:1>
                        'NFormItem:1': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Terminende'),
                                prop: 'extended_data_cfg.event.end_time'
                            },
                            content: {

                                // <NButtonGroup:0>
                                'NButtonGroup:0': {
                                    content: {

                                        // <NDatepicker:0>
                                        'NDatepicker:0': {
                                            model: {
                                                path: 'extended_data_cfg.event.end_date'
                                            },
                                            $props: {
                                                clearable: true
                                            },
                                        },
                                        // </ NDatepicker:0>

                                        // <NTimepicker:1>
                                        'NTimepicker:1': {
                                            model: {
                                                path: 'extended_data_cfg.event.end_time',
                                            },
                                            $props: {
                                                clearable: true
                                            },
                                        }
                                        // </ NTimepicker:1>

                                    }
                                }
                                // </ NButtonGroup:0>

                            }
                        }
                        // </ NFormItem:1>

                    }
                },
                // </ div:1>

                // <NFormGroup:2>
                'NFormGroup:2': {
                    model: {
                        path: 'extended_data_cfg.event.multipleoccation',
                        fallback: false
                    },
                    $props: {
                        label: pi.Locale.trans('Interval Events'),
                        collapse: true
                    },
                    content: {

                        // <NFormItem:0>
                        'NFormItem:0': {
                            $props: {
                                label: pi.Locale.trans('Maximale Wiederholungen (Unbegrenzt wenn leer)'),
                                prop: 'extended_data_cfg.event.intervallimit'
                            },
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'extended_data_cfg.event.intervallimit'
                                    }
                                }
                                // </ NInput:0>

                            }
                        },
                        // </ NFormItem:0>

                        // <NFormGroup:1>
                        'NFormGroup:1': {
                            model: {
                                path: 'extended_data_cfg.event.weeklyoccation',
                                fallback: true
                            },
                            $props: {
                                label: pi.Locale.trans('Wöchentlich'),
                                collapse: true
                            },
                            on: {
                                'onUpdate:modelValue': function (value, entity) {
                                    pi.Obj.set(entity, 'extended_data_cfg.event.monthlyoccation', false);
                                }
                            },
                            content: {

                                // <div:0>
                                'div:0': {
                                    class: 'grid grid--row grid--wrap grid--15',
                                    content: {

                                        // <NFormItem:0>
                                        'NFormItem:0': {
                                            class: 'col--1-1 col--1-2@md',
                                            $props: {
                                                label: pi.Locale.trans('Wochentag'),
                                                prop: 'extended_data_cfg.event.weeklydays'
                                            },
                                            content: {

                                                // <NSelect:0>
                                                'NSelect:0': {
                                                    model: {
                                                        path: 'extended_data_cfg.event.weeklydays',
                                                        fallback: []
                                                    },
                                                    $props: {
                                                        multiple: true,
                                                        clearable: true
                                                    },
                                                    content: {

                                                        // <NSelectOption:0>
                                                        'NSelectOption:0': {
                                                            $props: {
                                                                label: pi.Locale.trans('Montag'),
                                                                value: 'mo',
                                                            }
                                                        },
                                                        // </ NSelectOption:0>

                                                        // <NSelectOption:1>
                                                        'NSelectOption:1': {
                                                            $props: {
                                                                label: pi.Locale.trans('Dienstag'),
                                                                value: 'tu',
                                                            }
                                                        },
                                                        // </ NSelectOption:1>

                                                        // <NSelectOption:2>
                                                        'NSelectOption:2': {
                                                            $props: {
                                                                label: pi.Locale.trans('Mittwoch'),
                                                                value: 'we',
                                                            }
                                                        },
                                                        // </ NSelectOption:2>

                                                        // <NSelectOption:3>
                                                        'NSelectOption:3': {
                                                            $props: {
                                                                label: pi.Locale.trans('Donnerstag'),
                                                                value: 'th',
                                                            }
                                                        },
                                                        // </ NSelectOption:3>

                                                        // <NSelectOption:4>
                                                        'NSelectOption:4': {
                                                            $props: {
                                                                label: pi.Locale.trans('Freitag'),
                                                                value: 'fr',
                                                            }
                                                        },
                                                        // </ NSelectOption:4>

                                                        // <NSelectOption:5>
                                                        'NSelectOption:5': {
                                                            $props: {
                                                                label: pi.Locale.trans('Samstag'),
                                                                value: 'sa',
                                                            }
                                                        },
                                                        // </ NSelectOption:5>

                                                        // <NSelectOption:5>
                                                        'NSelectOption:6': {
                                                            $props: {
                                                                label: pi.Locale.trans('Sonntag'),
                                                                value: 'su',
                                                            }
                                                        }
                                                        // </ NSelectOption:6>

                                                    }
                                                }
                                                // </ NSelect:0>

                                            }
                                        },
                                        // </ NFormItem:0>

                                        // <NFormItem:1>
                                        'NFormItem:1': {
                                            class: 'col--1-1 col--1-2@md',
                                            $props: {
                                                label: pi.Locale.trans('Wiederholung alle {n} Wochen'),
                                                prop: 'extended_data_cfg.event.weeklyinterval'
                                            },
                                            content: {

                                                // <NInput:0>
                                                'NInput:0': {
                                                    model: {
                                                        path: 'extended_data_cfg.event.weeklyinterval',
                                                        fallback: 1
                                                    }
                                                }
                                                // </ NInput:0>

                                            }
                                        },
                                        // </ NFormItem:1>

                                        // <NFormItem:2>
                                        'NFormItem:2': {
                                            class: 'col--1-1',
                                            content: function (render, entity) {

                                                var days = pi.Obj.get(entity,
                                                    'extended_data_cfg.event.weeklydays', []);

                                                days = pi.Arr.each(days, function (day) {

                                                    if ( day === 'day' ) {
                                                        day = pi.Locale.trans('Tag');
                                                    }

                                                    if ( day === 'mo' ) {
                                                        day = pi.Locale.trans('Montag');
                                                    }

                                                    if ( day === 'tu' ) {
                                                        day = pi.Locale.trans('Dienstag');
                                                    }

                                                    if ( day === 'we' ) {
                                                        day = pi.Locale.trans('Mittwoch');
                                                    }

                                                    if ( day === 'th' ) {
                                                        day = pi.Locale.trans('Donnerstag');
                                                    }

                                                    if ( day === 'fr' ) {
                                                        day = pi.Locale.trans('Freitag');
                                                    }

                                                    if ( day === 'sa' ) {
                                                        day = pi.Locale.trans('Samstag');
                                                    }

                                                    if ( day === 'su' ) {
                                                        day = pi.Locale.trans('Sonntag');
                                                    }

                                                    return day;
                                                });

                                                days = !days || !days.length ?
                                                    pi.Locale.trans('Tag') : days.join(', ');

                                                var interval = pi.Obj.get(entity,
                                                    'extended_data_cfg.event.weeklyinterval', '1');

                                                var text = [
                                                    'Erstelle einen Termin jeden :days jede Woche',
                                                    'Erstelle einen Termin jeden :days jede Woche',
                                                    'Erstelle einen Termin jeden :days alle :count Wochen',
                                                ];

                                                return [
                                                    Vue.h('pre', { class: 'pretty' }, [
                                                        pi.Locale.choice(text.join('|'), interval, { days: days })
                                                    ])
                                                ];
                                            }

                                        }
                                        // </ NFormItem:2>

                                    }
                                }
                                // </ div:0>

                            }
                        },
                        // <NFormGroup:1>

                        // <NFormGroup:2>
                        'NFormGroup:2': {
                            model: {
                                path: 'extended_data_cfg.event.monthlyoccation',
                                fallback: false
                            },
                            $props: {
                                label: pi.Locale.trans('Monatlich'),
                                collapse: true
                            },
                            $on: {
                                'onUpdate:modelValue': function (value, entity) {
                                    pi.Obj.set(this.value, 'extended_data_cfg.event.weeklyoccation', false);
                                }
                            },
                            content: {

                                // <div:0>
                                'div:0': {
                                    class: 'grid grid--row grid--wrap grid--15',
                                    content: {

                                        // <NFormItem:0>
                                        'NFormItem:0': {
                                            class: 'col--1-1 col--1-3@md',
                                            $props: {
                                                label: pi.Locale.trans('Am {n} Tag des Monats'),
                                                prop: 'extended_data_cfg.event.monthlydate'
                                            },
                                            content: {

                                                // <NSelect:0>
                                                'NSelect:0': {
                                                    model: {
                                                        path: 'extended_data_cfg.event.monthlydate',
                                                        fallback: '1'
                                                    },
                                                    $props: {
                                                        allowCreate: true
                                                    },
                                                    content: {

                                                        // <NSelectOption:0>
                                                        'NSelectOption:0': {
                                                            $props: {
                                                                label: pi.Locale.trans('Letzen'),
                                                                value: '-1',
                                                            }
                                                        },
                                                        // </ NSelectOption:0>

                                                    }
                                                }
                                                // </ NSelect:0>

                                            }
                                        },
                                        // </ NFormItem:0>

                                        // <NFormItem:1>
                                        'NFormItem:1': {
                                            class: 'col--1-1 col--1-3@md',
                                            $props: {
                                                label: pi.Locale.trans('Wochentag'),
                                                prop: 'extended_data_cfg.event.monthlyday'
                                            },
                                            content: {

                                                // <NSelect:0>
                                                'NSelect:0': {
                                                    model: {
                                                        path: 'extended_data_cfg.event.monthlyday',
                                                        fallback: []
                                                    },
                                                    content: {

                                                        // <NSelectOption:-1>
                                                        'NSelectOption:-1': {
                                                            $props: {
                                                                label: pi.Locale.trans('Tag'),
                                                                value: 'day',
                                                            }
                                                        },
                                                        // </ NSelectOption:-1>

                                                        // <NSelectOption:0>
                                                        'NSelectOption:0': {
                                                            $props: {
                                                                label: pi.Locale.trans('Montag'),
                                                                value: 'mo',
                                                            }
                                                        },
                                                        // </ NSelectOption:0>

                                                        // <NSelectOption:1>
                                                        'NSelectOption:1': {
                                                            $props: {
                                                                label: pi.Locale.trans('Dienstag'),
                                                                value: 'tu',
                                                            }
                                                        },
                                                        // </ NSelectOption:1>

                                                        // <NSelectOption:2>
                                                        'NSelectOption:2': {
                                                            $props: {
                                                                label: pi.Locale.trans('Mittwoch'),
                                                                value: 'we',
                                                            }
                                                        },
                                                        // </ NSelectOption:2>

                                                        // <NSelectOption:3>
                                                        'NSelectOption:3': {
                                                            $props: {
                                                                label: pi.Locale.trans('Donnerstag'),
                                                                value: 'th',
                                                            }
                                                        },
                                                        // </ NSelectOption:3>

                                                        // <NSelectOption:4>
                                                        'NSelectOption:4': {
                                                            $props: {
                                                                label: pi.Locale.trans('Freitag'),
                                                                value: 'fr',
                                                            }
                                                        },
                                                        // </ NSelectOption:4>

                                                        // <NSelectOption:5>
                                                        'NSelectOption:5': {
                                                            $props: {
                                                                label: pi.Locale.trans('Samstag'),
                                                                value: 'sa',
                                                            }
                                                        },
                                                        // </ NSelectOption:5>

                                                        // <NSelectOption:5>
                                                        'NSelectOption:6': {
                                                            $props: {
                                                                label: pi.Locale.trans('Sonntag'),
                                                                value: 'su',
                                                            }
                                                        }
                                                        // </ NSelectOption:6>

                                                    }
                                                }
                                                // </ NSelect:0>

                                            }
                                        },
                                        // </ NFormItem:1>

                                        // <NFormItem:2>
                                        'NFormItem:2': {
                                            class: 'col--1-1 col--1-3@md',
                                            $props: {
                                                label: pi.Locale.trans('Wiederholung alle {n} Monate'),
                                                prop: 'extended_data_cfg.event.monthlyinterval'
                                            },
                                            content: {

                                                // <NInput:0>
                                                'NInput:0': {
                                                    model: {
                                                        path: 'extended_data_cfg.event.monthlyinterval',
                                                        fallback: 1
                                                    }
                                                }
                                                // </ NInput:0>

                                            }
                                        },
                                        // </ NFormItem:2>

                                        // <NFormItem:3>
                                        'NFormItem:3': {
                                            class: 'col--1-1',
                                            content: function (render, entity) {

                                                var date = pi.Obj.get(entity,
                                                    'extended_data_cfg.event.monthlydate', '1');

                                                if ( date !== '-1' ) {
                                                    date = pi.Locale.trans('an jedem :date.', { date: date });
                                                }

                                                if ( date === '-1' ) {
                                                    date = pi.Locale.trans('an jedem letzen');
                                                }

                                                var day = pi.Obj.get(entity,
                                                    'extended_data_cfg.event.monthlyday', 'day');

                                                if ( day === 'day' ) {
                                                    day = pi.Locale.trans('Tag');
                                                }

                                                if ( day === 'mo' ) {
                                                    day = pi.Locale.trans('Montag');
                                                }

                                                if ( day === 'tu' ) {
                                                    day = pi.Locale.trans('Dienstag');
                                                }

                                                if ( day === 'we' ) {
                                                    day = pi.Locale.trans('Mittwoch');
                                                }

                                                if ( day === 'th' ) {
                                                    day = pi.Locale.trans('Donnerstag');
                                                }

                                                if ( day === 'fr' ) {
                                                    day = pi.Locale.trans('Freitag');
                                                }

                                                if ( day === 'sa' ) {
                                                    day = pi.Locale.trans('Samstag');
                                                }

                                                if ( day === 'su' ) {
                                                    day = pi.Locale.trans('Sonntag');
                                                }

                                                var interval = pi.Obj.get(entity,
                                                    'extended_data_cfg.event.monthlyinterval', '1');

                                                var text = [
                                                    'Erstelle einen Termin :date :day jeden Monat',
                                                    'Erstelle einen Termin :date :day jeden Monat',
                                                    'Erstelle einen Termin :date :day alle :count Monate',
                                                ];

                                                return [
                                                    Vue.h('pre', { class: 'pretty' }, [
                                                        pi.Locale.choice(text.join('|'), interval, { date: date, day: day })
                                                    ])
                                                ];
                                            }

                                        }
                                        // </ NFormItem:3>

                                    }
                                }
                                // </ div:0>

                            }
                        }
                        // <NFormGroup:2>

                    }
                },
                // </ NFormGroup:2>

            }
        }
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.map.show', '0') === '1';
            },
            $props: {
                name: 'map',
                sort: 50,
                label: pi.Locale.trans('Karte'),
                icon: 'fa fa-map-marked'
            },
            content: {
                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Addresse'),
                        prop: 'address'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'address',
                                    },
                                    $props: {
                                        placeholder: pi.Locale.trans('Adresse eingeben')
                                    }
                                },
                                // </ NInput:0>

                                // <NInput:0>
                                'NButton:0': {
                                    $on: {
                                        onClick: function () {
                                            this.$configRefs.marker.setMarkerByAddress(this.value.address);
                                        },
                                    },
                                    content: [
                                        pi.Locale.trans('Marker setzen')
                                    ]
                                }
                                // </ NInput:0>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:0>

                // <div:1>
                'div:1': {
                    class: 'grid grid--row grid--wrap grid--15',
                    content: {

                        // <NFormItem:0>
                        'NFormItem:0': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Latitude'),
                                prop: 'lat'
                            },
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'lat', fallback: 0
                                    },
                                    $props: {
                                        disabled: false
                                    }
                                },
                                // </ NInput:0>

                            }
                        },
                        // </ NFormItem:0>

                        // <NFormItem:1>
                        'NFormItem:1': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Longitude'),
                                prop: 'lon'
                            },
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'lon', fallback: 0
                                    },
                                    $props: {
                                        disabled: false
                                    }
                                },
                                // </ NInput:0>

                            }
                        },
                        // </ NFormItem:1>

                    }
                },
                // </ div:1>

                // <NFormItem:3>
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Karte'),
                    },
                    content: {

                        // <NMap:0>
                        'NMap:0': {
                            $props: {
                                lat: '$$value.lat',
                                lng: '$$value.lon',
                                zoom: 12
                            },
                            content: {

                                // <NMapMarker:0>
                                'NMapMarker:0': {
                                    ref: 'marker',
                                    $props: {
                                        lat: '$$value.lat',
                                        lng: '$$value.lon',
                                        drag: true
                                    },
                                    $on: {
                                        'onUpdate:lat': function (value) {
                                            this.value.lat = value;
                                        },
                                        'onUpdate:lng': function (value) {
                                            this.value.lon = value;
                                        }
                                    }
                                },
                                // </ NMapMarker:0>

                            }
                        },
                        // </ NMap:0>

                    }
                },
                // </ NFormItem:3>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.seo.show', '0') === '1';
            },
            $props: {
                name: 'seo',
                sort: 70,
                label: pi.Locale.trans('SEO'),
                icon: 'fa fa-analytics',
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Sprechende URL'),
                        prop: 'slug'
                    },
                    content: {

                        // <NInput:0>
                        'NInput:0': {
                            model: {
                                path: 'slug',
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:0>

                // <NFormItem:1>
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Seitentitel'),
                        prop: 'pagetitle'
                    },
                    content: {

                        // <NInput:0>
                        'NInput:0': {
                            model: {
                                path: 'pagetitle',
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:1>

                // <NFormItem:2>
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Metabeschreibung'),
                        prop: 'description'
                    },
                    content: {

                        // <NTextarea:0>
                        'NTextarea:0': {
                            model: {
                                path: 'description',
                            }
                        }
                        // </ NTextarea:0>

                    }
                },
                // </ NFormItem:2>


                // <NFormItem:4>
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Indexierung'),
                        prop: 'noindex'
                    },
                    content: {

                        // <NSwitch:0>
                        'NSwitch:0': {
                            model: {
                                path: 'noindex'
                            },
                            content: [
                                pi.Locale.trans('Diese Seite von der Indizierung durch die Suchmaschinen ausschließen')
                            ]
                        }
                        // </ NSwitch:0>

                    }
                },
                // </ NFormItem:4>

                // <NFormItem:5>
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Nofollow'),
                        prop: 'extended_data_cfg.article.nofollow'
                    },
                    content: {

                        // <NSwitch:0>
                        'NSwitch:0': {
                            model: {
                                path: 'extended_data_cfg.article.nofollow'
                            },
                            content: [
                                pi.Locale.trans('Alle Verlinkungen auf diese Seite auf Nofollow setzen')
                            ]
                        }
                        // </ NSwitch:0>

                    }
                },
                // </ NFormItem:5>

                // <NFormItem:6>
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Kanonische URL'),
                        prop: 'canonical'
                    },
                    content: {

                        // <NInput:0>
                        'NInput:0': {
                            model: {
                                path: 'canonical'
                            },
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:6>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.options.show', '0') === '1';
            },
            $props: {
                name: 'option',
                sort: 80,
                label: pi.Locale.trans('Einstellung'),
                icon: 'fa fa-cog'
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Zuordnung'),
                        prop: 'aco_id'
                    },
                    content: {

                        // <SxSelectAco:0>
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            },
                        }
                        // </ SxSelectAco:0>

                    }
                },
                // </ NFormItem:0>

                // <NFormItem:1>
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Status'),
                        prop: 'active'
                    },
                    content: {

                        // <NSwitch:0>
                        'NSwitch:0': {
                            model: {
                                path: 'active'
                            },
                            content: [
                                pi.Locale.trans('Diese Seite ist aktiviert')
                            ]
                        }
                        // </ NSwitch:0>

                    }
                },
                // </ NFormItem:1>

                // <NFormItem:2>
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Startzeit'),
                        prop: 'begin_publishing'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NDatepicker:0>
                                'NDatepicker:0': {
                                    model: {
                                        path: 'begin_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                },
                                // </ NDatepicker:0>

                                // <NTimepicker:1>
                                'NTimepicker:1': {
                                    model: {
                                        path: 'begin_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                }
                                // </ NTimepicker:1>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:2>

                // <NFormItem:3>
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Endzeit'),
                        prop: 'end_publishing'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NDatepicker:0>
                                'NDatepicker:0': {
                                    model: {
                                        path: 'end_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                },
                                // </ NDatepicker:0>

                                // <NTimepicker:1>
                                'NTimepicker:1': {
                                    model: {
                                        path: 'end_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                }
                                // </ NTimepicker:1>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:3>

            }
        },
        // </ NTabsItem:0>
    },
]);
