import IconMixin from "../mixins/icon-mixin";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxArticlePagetypeMarkup',

    mixins: [
        IconMixin
    ],

    props: {

        item: {
            required: true
        }

    },

    render()
    {
        return (
            <div class="sx-article-pagetype-markup">
                <span class={this.iconClass}></span> { Config.article(['types', this.item.type], '-') }
            </div>
        );
    }

}
