import DefaultEdit from "../../../default/src/edit/edit";
import { Arr, Obj, Num, Any, Event } from "@kizmann/pico-js";
import { Form, Config } from "@dievisualisten/simplex";

export default {

    name: 'SxArticleEdit',

    extends: DefaultEdit,

    inject: {

        NDataform: {
            default: undefined
        },

        NTabs: {
            default: undefined
        }

    },

    computed: {

        froala()
        {
            return Obj.assign(window.froalaDefault || {});
        },

        teasers()
        {
            let teasers = Config.get('resource.modules.menu.teaser');

            teasers = Arr.map(teasers, (item, key) => {
                return Obj.assign(item, { key: key });
            });

            teasers = Arr.filter(teasers, (item) => {
                return item.show === '1';
            });

            return Arr.sort(teasers, 'order');
        },

        pagetype()
        {
            return Config.get([
                'resource.modules.article.pagetypes',
                this.value.type,
                this.value.layout
            ], {});
        },

        pagetypes()
        {
            return Config.articlePagetypes();
        },

        pagelayouts()
        {
            return Config.article(['layouts', this.value.type], []);
        },

        dropzones()
        {
            let zones = Config.get([
                'resource', 'modules', 'article', 'panels', 'media', 'tabs'
            ], {});

            return Obj.sort(zones, 'order');
        },

        activezones()
        {
            return Config.get([
                'resource', 'modules', 'article', 'pagetypes',
                this.value.type, this.value.layout, 'panels', 'media'
            ], {});
        },

        domains()
        {
            return Arr.filter(this.menus, (menu) => {
                return Obj.get(menu, 'type') === 'domain';
            });
        }

    },

    mounted()
    {
        this.queryMenuTree();
    },

    data()
    {
        return { ready: false };
    },

    ready()
    {
        if ( Obj.get(this.value, '_new', false) ) {
            Obj.set(this.value, 'aco_id', this.$root.aco || null);
        }

        if ( Obj.get(this.value, '_new', false) ) {
            Obj.set(this.value, 'active', true);
        }

        if ( ! Obj.has(this.value, 'extended_data.menu.url') ) {
            Obj.set(this.value, 'extended_data.menu.url', '');
        }

        if ( ! Obj.has(this.value, 'extended_data_cfg.menu.blank') ) {
            Obj.set(this.value, 'extended_data_cfg.menu.blank', false);
        }

        Event.bind('locale/change:form', () => this.queryMenuTree(),
            this._.uid);

        this.ready = true;
    },

    beforeUnmount()
    {
        Event.unbind('locale/change:form', this._.uid);
    },

    methods: {

        canMatrixCheck({ item, column })
        {
            if ( item.type === 'menu' ) {
                return Arr.has([1, 2], Num.int(column.matrix));
            }

            let key = [
                item.type, 'default', 'panels', 'menu', 'teaser' + column.matrix, 'show'
            ];

            return Config.pagetypes(key, '0') !== '0';
        },

        getI18nClass()
        {
            return this.i18n ? 'n-i18n' : null;
        }

    },

    renderLinkForm()
    {
        if ( ! this.ready || Obj.get(this.value, 'type', null) !== 'link' ) {
            return null;
        }

        return (
            <NFormGroup class="col--1-1" label={this.trans('Link')}>

                <NFormItem prop="extended_data.menu.url" label={this.trans('Url')}>
                    <NInput vModel={this.value.extended_data.menu.url} />
                </NFormItem>

                <NFormItem prop="extended_data_cfg.menu.blank">
                    <NSwitch vModel={this.value.extended_data_cfg.menu.blank}>
                        { this.trans('In neuem Tab öffnen') }
                    </NSwitch>
                </NFormItem>

            </NFormGroup>
        );
    },

    renderContentForm()
    {
        if ( ! this.ready || ! Obj.has(this.value, 'content') ) {
            return null;
        }

        let html = [];

        let content1Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 4,
            label: Config.get('resource.modules.article.content.name',this.trans('Inhalt')),
            name: 'content1'
        };

        let content1Html = (
            <NTabsItem {...content1Props}>
                <SxFroala vModel={this.value.content} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content.show', '0') === '1' ) {
            html.push(content1Html);
        }

        let content2Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 6,
            label: Config.get('resource.modules.article.content2.name',this.trans('Weiterer Inhalt')),
            name: 'content2'
        };

        let content2Html = (
            <NTabsItem {...content2Props}>
                <SxFroala vModel={this.value.content2} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content2.show', '0') === '1' ) {
            html.push(content2Html);
        }

        let content3Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 6,
            label: Config.get('resource.modules.article.content3.name',this.trans('Weiterer Inhalt')),
            name: 'content3'
        };

        let content3Html = (
            <NTabsItem {...content3Props}>
                <SxFroala vModel={this.value.content3} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content3.show', '0') === '1' ) {
            html.push(content3Html);
        }

        Arr.each(Form.get('sx-article-custom', []), (config) => {
            html.push(<NConfig modelValue={this.value} config={config} scope={this} />);
        });
        
        return (
            <NTabs modelValue="content1" relative={true}>
                { html }
            </NTabs>
        );
    },

    renderPageForm()
    {
        return (
            <NTabsItem class="article-splitview clear" label={this.trans('Seite')} icon="fa fa-file" name="default" sort={10} preload={true} keep={true}>
                    <div class="grid-split">
                        <NFormGroup label={this.trans('Seitentitel')}>

                            <NFormItem class={{'n-i18n': this.i18n}} prop="title" label={this.trans('Titel')}>
                                <NInput vModel={this.value.title} placeholder={Obj.get(this.value, 'baselocale.title')} />
                            </NFormItem>

                            <NFormItem class={{'n-i18n': this.i18n}} prop="headline" label={this.trans('Überschrift')}>
                                <NInput vModel={this.value.headline} placeholder={Obj.get(this.value, 'baselocale.headline')} />
                            </NFormItem>

                        </NFormGroup>

                        <NFormGroup label={this.trans('Einstellungen')}>

                            <NFormItem prop="type" label={this.trans('Seitentyp')}>
                                <NSelect vModel={this.value.type} options={this.pagetypes} optionsValue="$index" optionsLabel="$value" />
                            </NFormItem>

                            <NFormItem prop="layout" label={this.trans('Layout')}>
                                <NSelect vModel={this.value.layout} options={this.pagelayouts} optionsValue="$value.0" optionsLabel="$value.1" />
                            </NFormItem>

                        </NFormGroup>

                        {Obj.get(this.pagetype, 'topline.show', '0') === '1' && <div class="col--1-1">

                            <NFormItem class={{'n-i18n': this.i18n}} prop="topline" label={this.trans('Topline')}>
                                <NInput vModel={this.value.topline} placeholder={Obj.get(this.value, 'baselocale.topline')} />
                            </NFormItem>

                        </div>}

                        {Obj.get(this.pagetype, 'subline.show', '0') === '1' && <div class="col--1-1">

                            <NFormItem class={{'n-i18n': this.i18n}} prop="subline" label={this.trans('Subline')}>
                                <NInput vModel={this.value.subline} placeholder={Obj.get(this.value, 'baselocale.subline')} />
                            </NFormItem>

                        </div>}

                        { this.ctor('renderLinkForm')() }

                    </div>

                    { this.ctor('renderContentForm')() }

            </NTabsItem>
        );
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-article-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
                { this.ctor('renderPageForm')() }
            </NForm>
        )
    }

}
