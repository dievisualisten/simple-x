import DefaultIndex from "../../../default/src/index/index.js"
import { Arr, Obj, Num } from "@kizmann/pico-js";
import { Config, Ajax } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxArticleIndex',

    extends: DefaultIndex,

    urls: {
        index: 'articles.index',
        edit: 'articles.edit',
        create: 'articles.create'
    },

    model: 'article',

    computed: {

        teasers()
        {
            let teasers = Config.get('resource.modules.menu.teaser');

            teasers = this.Arr.map(teasers, (item, key) => {
                return this.Obj.assign(item, { key: key });
            });

            teasers = this.Arr.filter(teasers, (item) => {
                return item.show === '1';
            });

            return this.Arr.sort(teasers, 'order');
        },

    },

    methods: {

        canMatrixCheck(item)
        {
            if ( !this.transactionData ) {
                return false;
            }

            let $draggable = this.$refs.tree.$refs.draggable;

            let { sources, target, strategy } = this.transactionData;

            if ( strategy !== 'inner' ) {
                target = $draggable.findVirtual(target.item.parent_id);
            }

            let matrixRainbow = this.Arr.each(sources, (source) => {

                if ( target.item.type === 'menu' ) {
                    return Arr.has([1, 2], Num.int(item.value));
                }

                let key = [
                    target.item.type, 'default', 'panels', 'menu', item.key, 'show'
                ];

                return Config.pagetypes(key, '0') !== '0';
            });

            return ! this.Arr.has(matrixRainbow, false);
        },

        allowDrop(source, target, strategy)
        {
            if ( this.Any.isEmpty(target) || !this.Obj.has(source, 'item.transaction') ) {
                return false;
            }

            let depthFilter = {
                default: {
                    'compare': [],
                    'default': ['inner', 'before', 'after'],
                    '0': [],
                    '1': ['inner'],
                },
                domain: {
                    'compare': [],
                    'default': [],
                    '0': ['before', 'after']
                },
                menu: {
                    'compare': ['parent_id:id', 'parent_id:parent_id'],
                    'default': [],
                    '0': ['inner'],
                    '1': ['before', 'after']
                },
                frontpage: {
                    'compare': ['parent_id:id', 'parent_id:parent_id'],
                    'default': [],
                    '0': ['inner'],
                    '1': ['before', 'after']
                }
            };

            let sourceFilter = this.Obj.get(depthFilter,
                source.item.type, depthFilter.default);

            let targetFilter = this.Obj.get(sourceFilter,
                target.value.depth, sourceFilter.default);

            let compareProp = this.Arr.each(sourceFilter.compare, (prop) => {
                return source.item[prop.split(':')[0]] === target.item[prop.split(':')[1]];
            });

            return this.Arr.has(targetFilter, strategy) &&
                (!compareProp.length || this.Arr.has(compareProp, true));
        },

        getArticleTypes()
        {
            return Config.articlePagetypes();
        },

        getArticleLayouts({ item })
        {
            return Arr.each(Config.article(['layouts', item.type], []), (value) => {
                return { value: value[0], label: value[1] };
            });
        },

        focusArticle({ value })
        {
            this.$refs.tree.$refs.draggable.highlightNode(value.id, 'foreign_key');
        },

        startTransaction(sources, target, strategy)
        {
            sources = this.Arr.clone(sources);

            let transaction = this.Obj.get(sources,
                '0.item.transaction', null);

            this.transactionData = {
                sources, target, strategy
            };

            // Reset selection
            this.transactionSelect = [];

            if ( transaction === 'menus' ) {
                return this.moveMenus();
            }

            if ( transaction === 'articles' ) {
                return this.transactionModal = true;
            }

            this.Notify(this.trans('How could this happen?'), 'danger');
        },

        commitTransaction()
        {
            let { sources, target, strategy } = this.transactionData;

            sources = this.Arr.extract(sources, 'item.id').join(',');

            let update = {
                teaser: Num.combine(this.transactionSelect)
            };

            Ajax.call('menus-insert', null, {
                source_id: sources, target_id: target.item.id, position: strategy, update
            });

            this.transactionModal = false;
        },

        abortTransaction()
        {
            this.transactionModal = false;
        },

        moveMenus()
        {
            let { sources, target, strategy } = this.transactionData;

            this.$refs.tree.queryMove(this.Arr.extract(sources, 'item.id').join(','),
                target.item.id, strategy);
        }

    },

    data()
    {
        return {
            transactionModal: false, transactionSelect: []
        };
    },

    renderTree()
    {
        let updateEvents = [
            'locale/changed',
            'menus-move',
            'menus-edit',
            'menus-delete',
            'menus-insert',
            'articles-edit',
            'articles-copy',
            'articles-delete'
        ];

        let props = {
            indexQuery: 'menus-tree',
            deleteQuery: 'menus-delete',
            moveQuery: 'menus-move',
            group: ['menus', 'articles'],
            allowGroups: ['menus', 'articles'],
            updateEvents: updateEvents,
            allowMove: false,
            allowDrop: this.allowDrop,
            onMoveraw: this.startTransaction
        };

        props.use = ({ item }) => {
            return (<SxMenuTreeNode item={item} />);
        };

        let slots = {
            custom: []
        };

        slots.custom[0] = (
            <NButton type="primary" icon="fa fa-plus" square={true} />
        );

        slots.custom[1] = (
            <NPopover trigger="click" width={170}>
                <NPopoverOption onClick={() => this.showEditModal('domainForm')}>
                    {this.trans('Domain')}
                </NPopoverOption>
                <NPopoverOption onClick={() => this.showEditModal('menuForm')}>
                    {this.trans('Menu')}
                </NPopoverOption>
                <NPopoverOption onClick={() => this.showEditModal('aliasForm')}>
                    {this.trans('Interner Link')}
                </NPopoverOption>
            </NPopover>
        );

        return (
            <SxDatatree ref="tree" {...props} v-slots={slots}/>
        );
    },

    renderTable()
    {
        let updateEvents = [
            'locale/changed',
            'menus-delete',
            'menus-edit',
            'menus-insert',
            'articles-edit',
            'articles-copy'
        ];

        let props = {
            indexQuery: 'articles-index',
            copyQuery: 'articles-copy',
            deleteQuery: 'articles-delete',
            showCopy: true,
            group: ['articles'],
            allowDrag: true,
            updateEvents: updateEvents,
            actionsWidth: 120
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        let slots = {
            actions: (props) => {
                return (
                    <NButton size="xs" type="info" icon="fa fa-bullseye-pointer" square={true} onClick={() => this.focusArticle(props)} />
                );
            }
        };

        return (
            <NDatatable ref="table" {...props} v-slots={slots}>
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'title',
                                      label: this.trans('Titel'),
                                      sort: true,
                                      filter: true,
                                      fluid: true
                                  }
                              }>
                    { ({ item }) => <SxArticleNodeMarkup item={item} /> }
                </NTableColumn>
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'headline',
                                      label: this.trans('Headline'),
                                      sort: true,
                                      filter: true,
                                      fluid: true,
                                      visible: false
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'pagetitle',
                                      label: this.trans('Seitentitel'),
                                      sort: true,
                                      filter: true,
                                      fluid: true,
                                      visible: false
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'slug',
                                      label: this.trans('Sprechende Url'),
                                      sort: true,
                                      filter: true,
                                      fluid: true,
                                      visible: false
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'option',
                                      prop: 'type',
                                      label: this.trans('Typ'),
                                      sort: false,
                                      filter: true,
                                      options: this.getArticleTypes,
                                      optionsLabel: '$value',
                                      optionsValue: '$index'
                                  }
                              }>
                    { ({ item }) => <SxArticlePagetypeMarkup item={item} /> }
                </NTableColumn>
                <NTableColumn {
                                  ...{
                                      type: 'option',
                                      prop: 'layout',
                                      label: this.trans('Layout'),
                                      sort: false,
                                      filter: true,
                                      options: this.getArticleLayouts,
                                      optionsLabel: '$value.label',
                                      optionsValue: '$value.value',
                                      visible: false
                                  }
                              } />

                <NTableColumn {
                                  ...{
                                      type: 'datetime',
                                      prop: 'modified',
                                      label: this.trans('Bearbeitet am'),
                                      sort: true,
                                      filter: true
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'datetime',
                                      prop: 'created',
                                      label: this.trans('Erstellt am'),
                                      sort: true,
                                      filter: true,
                                      visible: false
                                  }
                              } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'title',
                                     label: this.trans('Titel'),
                                 }
                             } />

                <NInfoColumn {
                                 ...{
                                     type: 'option',
                                     prop: 'type',
                                     label: this.trans('Typ'),
                                     options: this.getArticleTypes,
                                     optionsLabel: '$value',
                                     optionsValue: '$index'
                                 }
                             } />
                <NInfoColumn {
                                  ...{
                                      type: 'option',
                                      prop: 'layout',
                                      label: this.trans('Layout'),
                                      sort: false,
                                      filter: false,
                                      options: this.getArticleLayouts,
                                      optionsLabel: '$value.label',
                                      optionsValue: '$value.value'
                                  }
                              } />
                <NInfoColumn {
                                 ...{
                                     type: 'datetime',
                                     prop: 'modified',
                                     label: this.trans('Bearbeitet am'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'datetime',
                                     prop: 'created',
                                     label: this.trans('Erstellt am'),
                                 }
                             } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxArticleEdit',
            width: '1920px',
            height: '100%',
            localized: true,
            showQuery: 'articles-show',
            editQuery: 'articles-edit',
            newTitle: this.trans('Artikel erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    renderDomainForm()
    {
        let props = {
            use: 'SxMenuDomain',
            localized: false,
            width: '1920px',
            height: '100%',
            showQuery: 'menus-show',
            editQuery: 'menus-edit',
            newTitle: this.trans('Domain erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            layout: ['spacer', 'close', 'save'],
            onNew: () => this.showEditModal('domainForm'),
            onClose: () => this.hideEditModal('domainForm'),
        };

        return (
            <NDataform ref="domainForm" {...props} />
        );
    },

    renderMenuForm()
    {
        let props = {
            use: 'SxMenuEdit',
            localized: false,
            width: '640px',
            height: 'auto',
            showQuery: 'menus-show',
            editQuery: 'menus-edit',
            newTitle: this.trans('Menu erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            layout: ['spacer', 'close', 'save'],
            onNew: () => this.showEditModal('menuForm'),
            onClose: () => this.showEditModal('menuForm'),

        };

        return (
            <NDataform ref="menuForm" type="default" {...props} />
        );
    },

    renderAliasForm()
    {
        let props = {
            use: 'SxMenuAlias',
            localized: false,
            width: '1920px',
            height: '100%',
            showQuery: 'menus-show',
            editQuery: 'menus-edit',
            newTitle: this.trans('Internen Link erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            layout: ['spacer', 'close', 'save'],
            onNew: () => this.showEditModal('aliasForm'),
            onClose: () => this.showEditModal('aliasForm'),

        };

        return (
            <NDataform ref="aliasForm" {...props} />
        );
    },

    renderTransactionModal()
    {
        let slots = {};

        slots.header = () => {
            return (
                <h4 class="text-primary">{this.trans('Menueinträge erstellen')}</h4>
            );
        };

        slots.default = () => {
            return (
                <NForm>
                    <NCheckboxGroup class="sx-pagematrix" vModel={this.transactionSelect} align="vertical">
                        {
                            this.Arr.each(this.teasers, (teaser) => {

                                if ( !this.Num.int(teaser.show) ) {
                                    return null;
                                }

                                let props = {
                                    value: this.Num.int(teaser.value),
                                    disabled: !this.canMatrixCheck(teaser)
                                }

                                return (
                                    <NCheckbox class="sx-pagematrix-item" {...props}>
                                        <div>
                                            {this.Obj.get(teaser, 'name') || '-'}
                                        </div>
                                        <div>
                                            {this.Obj.get(teaser, 'tooltip') || '-'}
                                        </div>
                                    </NCheckbox>
                                );
                            })
                        }
                    </NCheckboxGroup>
                </NForm>
            );
        };

        slots.footer = () => {
            return (
                <div class="grid grid--row">
                    <div class="col--auto col--left">
                        <NButton type="secondary" onClick={this.abortTransaction}>
                            {this.trans('Abort')}
                        </NButton>
                    </div>
                    <div class="col--auto col--right">
                        <NButton type="primary" disabled={!this.transactionSelect.length} onClick={this.commitTransaction}>
                            {this.trans('Create')}
                        </NButton>
                    </div>
                </div>
            );
        };

        return (
            <NModal vModel={this.transactionModal} listen={false} width="480px" height="auto" v-slots={slots}/>
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderDomainForm')(),
            this.ctor('renderMenuForm')(),
            this.ctor('renderAliasForm')(),
            this.ctor('renderRouteConfirm')(),
            this.ctor('renderTransactionModal')()
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
