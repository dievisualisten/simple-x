import { Obj } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    computed: {

        iconClass()
        {
            if ( this.item.type === 'domain' ) {
                return 'fa fa-globe';
            }

            if ( this.item.type === 'menu' ) {
                return 'fa fa-sitemap';
            }

            if ( this.item.type === 'alias' ) {
                return 'fa fa-paperclip';
            }

            let pagetype = Config.get([
                'resource', 'modules', 'article', 'pagetypes', this.item.type
            ]);

            return Obj.get(pagetype, 'icon', 'fa fa-ghost');
        }

    }

}
