import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('articles');

require("./config/article-default.config");
require("./config/article-custom.config");

import ArticleIndex from "./js/article/article-index";
App.component(ArticleIndex.name, ArticleIndex);

import ArticleEdit from "./js/article/article-edit";
App.component(ArticleEdit.name, ArticleEdit);

import ArticleMarkup from "./js/helpers/article-node-markup";
App.component(ArticleMarkup.name, ArticleMarkup);

import SxArticleTypeMarkup from "./js/helpers/article-pagetype-markup";
App.component(SxArticleTypeMarkup.name, SxArticleTypeMarkup);
