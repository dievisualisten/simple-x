sx.Form.set('sx-formularconfig-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Formularconfig'),
                icon: 'fa fa-cog'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Identifier'),
                        prop: 'identifier'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'identifier'
                            },
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Formular'),
                        prop: 'formular_id'
                    },
                    content: {
                        'SxSelectFormular:0': {
                            model: {
                                path: 'formular_id'
                            },
                            $props: {
                                clearable: false
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Zugehörigkeit'),
                        prop: 'aco_id'
                    },
                    content: {
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            },
                        }
                    }
                },
                'NFormGroup:4': {
                    $props: {
                        legend: pi.Locale.trans('Nach dem Absenden des Formulars\n'),
                    },
                    content: {
                        'NFormItem:1': {
                            $props: {
                                label: pi.Locale.trans('Weiterleiten an'),
                                prop: 'after_send_redirect'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'after_send_redirect'
                                    },
                                }
                            }
                        },
                        'NFormItem:2': {
                            class: function () {
                                return {'n-i18n': this.i18n};
                            },
                            $props: {
                                label: pi.Locale.trans('Headline'),
                                prop: 'after_send_headline'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'after_send_headline'
                                    },
                                    $props: {
                                        placeholder: '$$value.baselocale.after_send_headline'
                                    }
                                }
                            }
                        },
                        'NFormItem:3': {
                            class: function () {
                                return {'n-i18n': this.i18n};
                            },
                            $props: {
                                label: pi.Locale.trans('Inhalt'),
                                prop: 'after_send_content'
                            },
                            content: {
                                'froala:0': {
                                    model: {
                                        path: 'after_send_content'
                                    },
                                    $props: {
                                        config: '$$scope.froala'
                                    }
                                }
                            }
                        },
                    }
                },
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'email',
                sort: 20,
                label: pi.Locale.trans('E-Mail'),
                icon: 'fa fa-envelope'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Konfiguration'),
                        prop: 'email_id'
                    },
                    content: {
                        'SxSelectEmail:0': {
                            model: {
                                path: 'email_id'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Empfänger E-Mail-Adresse'),
                        prop: 'email_to'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email_to'
                            },
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('BCC'),
                        prop: 'email_bcc'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'email_bcc'
                            },
                        }
                    }
                },
                'NFormItem:3': {
                    class: function () {
                        return {'n-i18n': this.i18n};
                    },
                    $props: {
                        label: pi.Locale.trans('Betreff'),
                        prop: 'subject'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'subject'
                            },
                            $props: {
                                placeholder: '$$value.baselocale.subject'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */


    /* Tracking start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'tracking',
                sort: 20,
                label: pi.Locale.trans('Tracking'),
                icon: 'fa fa-envelope'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Tracking verwenden'),
                        prop: 'use_conversion_tracking'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'use_conversion_tracking', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Tracking verwenden')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Tracking-Code nach dem Absenden'),
                        prop: 'conversion_tracking_code'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'conversion_tracking_code'
                            },
                        }
                    }
                },

            }
        }
    },
    /* Tracking end */

]);
