import DefaultEdit from "../../../default/src/edit/edit";
import { Arr, Obj } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxFormularconfigEdit',

    computed: {

        froala()
        {
            return Obj.assign(window.froalaDefault || {}, {
                height: 160
            });
        }

    },

    extends: DefaultEdit,

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-formularconfig-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        )
    }

}
