import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Data, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxFormularconfigIndex',

    extends: DefaultIndex,

    urls: {
        index: 'formularconfigs.index',
        edit: 'formularconfigs.edit',
        create: 'formularconfigs.create'
    },

    model: 'formularconfig',

    renderTable()
    {
        let updateEvents = [
            'formularconfigs-edit'
        ];

        let props = {
            indexQuery: 'formularconfigs-index',
            copyQuery: 'formularconfigs-copy',
            deleteQuery: 'formularconfigs-delete',
            group: ['formularconfigs'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'option',
                        prop: 'formular_id',
                        label: this.trans('Formular'),
                        sort: true,
                        filter: true,
                        fluid: true,
                        options: Data.get('formulars.data', []),
                        optionsLabel: '$value.name',
                        optionsValue: '$value.id'
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'option',
                        prop: 'aco_id',
                        label: this.trans('Zugehörigkeit'),
                        sort: true,
                        filter: true,
                        fluid: true,
                        options: this.acos,
                        optionsLabel: '$value.name',
                        optionsValue: '$value.id'
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'option',
                        prop: 'formular_id',
                        label: this.trans('Formular'),
                        options: Data.get('formulars.data', []),
                        optionsLabel: '$value.name',
                        optionsValue: '$value.id'
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'option',
                        prop: 'aco_id',
                        label: this.trans('Zugehörigkeit'),
                        options: this.acos,
                        optionsLabel: '$value.name',
                        optionsValue: '$value.id'
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxFormularconfigEdit',
            width: '1920px',
            height: '100%',
            localized: true,
            showQuery: 'formularconfigs-show',
            editQuery: 'formularconfigs-edit',
            newTitle: this.trans('Formularkonfiguration erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});