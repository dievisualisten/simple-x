import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('formularconfigs');

require('./config/formularconfig-default.config');

import SxFormularconfigIndex from "./js/formularconfig/formularconfig-index";
App.component(SxFormularconfigIndex.name, SxFormularconfigIndex);

import SxFormularconfigEdit from "./js/formularconfig/formularconfig-edit";
App.component(SxFormularconfigEdit.name, SxFormularconfigEdit);
