import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('roles');

require('./config/role-default.config');

import SxRoleIndex from "./js/role/role-index";
App.component(SxRoleIndex.name, SxRoleIndex);

import SxRoleEdit from "./js/role/role-edit";
App.component(SxRoleEdit.name, SxRoleEdit);
