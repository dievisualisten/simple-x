sx.Form.set('sx-role-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Rolle'),
                icon: 'fa fa-user-tag'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Beschreibung'),
                        prop: 'description'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'description'
                            },
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Zugriffsebene'),
                        prop: 'aco_accesslevel'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'aco_accesslevel'
                            },
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Authentifizierungs-Rolle'),
                        prop: 'auth_config_identifier'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'auth_config_identifier'
                            },
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'resources',
                sort: 20,
                label: pi.Locale.trans('Resourcen'),
                icon: 'fa fa-boxes'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'resources'
                    },
                    content: {
                        'SxTransferResource:0': {
                            model: {
                                path: 'resources'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'users',
                sort: 30,
                label: pi.Locale.trans('Benutzer'),
                icon: 'fa fa-user'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'users'
                    },
                    content: {
                        'SxTransferUser:0': {
                            model: {
                                path: 'users'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
