import DefaultEdit from "../../../default/src/edit/edit";
import { Arr, Obj, Num, Event } from "@kizmann/pico-js";
import { Form, Config } from "@dievisualisten/simplex";

export default {

    name: 'SxElementEdit',

    extends: DefaultEdit,

    inject: {

        SxEditController: {
            default: undefined
        },

        SxElementPanel: {
            default: undefined
        },

        NDataform: {
            default: undefined
        },

        NTabs: {
            default: undefined
        }

    },

    ready()
    {
        if ( Obj.get(this.value, '_new', false) ) {
            Obj.set(this.value, 'aco_id', this.$root.aco || null);
        }

        if ( Obj.get(this.value, '_new', false) ) {
            Obj.set(this.value, 'active', true);
        }

        if ( this.SxElementPanel ) {
            Obj.set(this.value, 'type', this.SxElementPanel.type);
        }

        if ( this.SxEditController ) {
            Obj.set(this.value, 'article_id', this.SxEditController.value.id);
        }
    },

    beforeUnmount()
    {
        Event.unbind('locale/change:form', this._.uid);
    },

    computed: {

        froala()
        {
            return Obj.assign(window.froalaDefault || {});
        },

        pagetype()
        {
            return Config.get([
                'resource.modules.article.pagetypes',
                this.value.type,
                this.value.layout
            ], {});
        },

        pagetypes()
        {
            return Config.elementPagetypes();
        },

        pagelayouts()
        {
            return Config.article(['layouts', this.value.type], []);
        },

        dropzones()
        {
            let zones = Config.get([
                'resource', 'modules', 'article', 'panels', 'media', 'tabs'
            ], {});

            return Obj.sort(zones, 'order');
        },

        activezones()
        {
            return Config.get([
                'resource', 'modules', 'article', 'pagetypes',
                this.value.type, this.value.layout, 'panels', 'media'
            ], {});
        },

    },

    methods: {

        getI18nClass()
        {
            return this.i18n ? 'n-i18n' : null;
        }

    },

    renderContentForm()
    {
        if ( ! Obj.has(this.value, 'content') ) {
            return null;
        }

        let html = [];

        let content1Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 4,
            label: Config.get('resource.modules.article.content.name',this.trans('Inhalt')),
            name: 'content1'
        };

        let content1Html = (
            <NTabsItem {...content1Props}>
                <SxFroala vModel={this.value.content} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content.show', '0') === '1' ) {
            html.push(content1Html);
        }

        let content2Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 6,
            label: Config.get('resource.modules.article.content2.name',this.trans('Weiterer Inhalt')),
            name: 'content2'
        };

        let content2Html = (
            <NTabsItem {...content2Props}>
                <SxFroala vModel={this.value.content2} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content2.show', '0') === '1' ) {
            html.push(content2Html);
        }

        let content3Props = {
            class: 'article-wysiwyg clear',
            style: 'min-height: 200px;',
            icon: 'fa fa-pen',
            sort: 6,
            label: Config.get('resource.modules.article.content3.name',this.trans('Weiterer Inhalt')),
            name: 'content3'
        };

        let content3Html = (
            <NTabsItem {...content3Props}>
                <SxFroala vModel={this.value.content3} config={this.froala} />
            </NTabsItem>
        );

        if ( Obj.get(this.pagetype, 'content3.show', '0') === '1' ) {
            html.push(content3Html);
        }

        Arr.each(Form.get('sx-element-custom', []), (config) => {
            html.push(<NConfig modelValue={this.value} config={config} scope={this} />);
        });
        
        return (
            <NTabs modelValue="content1" relative={true}>
                { html }
            </NTabs>
        );
    },

    renderPageForm()
    {
        return (
            <NTabsItem class="article-splitview clear" label={this.trans('Seite')} icon="fa fa-file" name="default" sort={10} preload={true} keep={true}>
                    <div class="grid-split">
                        <NFormGroup label={this.trans('Seitentitel')}>

                            <NFormItem class={{'n-i18n': this.i18n}} prop="title" label={this.trans('Titel')}>
                                <NInput vModel={this.value.title} placeholder={Obj.get(this.value, 'baselocale.title')} />
                            </NFormItem>

                            <NFormItem class={{'n-i18n': this.i18n}} prop="headline" label={this.trans('Überschrift')}>
                                <NInput vModel={this.value.headline} placeholder={Obj.get(this.value, 'baselocale.headline')} />
                            </NFormItem>

                        </NFormGroup>

                        <NFormGroup label={this.trans('Einstellungen')}>

                            <NFormItem prop="type" label={this.trans('Seitentyp')}>
                                <NSelect vModel={this.value.type} options={this.pagetypes} optionsValue="$index" optionsLabel="$value" disabled={true} />
                            </NFormItem>

                            <NFormItem prop="layout" label={this.trans('Layout')}>
                                <NSelect vModel={this.value.layout} options={this.pagelayouts} optionsValue="$value.0" optionsLabel="$value.1" />
                            </NFormItem>

                        </NFormGroup>

                        {Obj.get(this.pagetype, 'topline.show', '0') === '1' && <div class="col--1-1">

                            <NFormItem class={{'n-i18n': this.i18n}} prop="topline" label={this.trans('Topline')}>
                                <NInput vModel={this.value.topline} placeholder={Obj.get(this.value, 'baselocale.topline')} />
                            </NFormItem>

                        </div>}

                        {Obj.get(this.pagetype, 'subline.show', '0') === '1' && <div class="col--1-1">

                            <NFormItem class={{'n-i18n': this.i18n}} prop="subline" label={this.trans('Subline')}>
                                <NInput vModel={this.value.subline} placeholder={Obj.get(this.value, 'baselocale.subline')} />
                            </NFormItem>

                        </div>}

                    </div>

                    { this.ctor('renderContentForm')() }

            </NTabsItem>
        );
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-element-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
                { this.ctor('renderPageForm')() }
            </NForm>
        )
    }

}
