export default {

    computed: {

        iconClass()
        {
            if ( this.item.type === 'domain' ) {
                return 'fa fa-globe';
            }

            if ( this.item.type === 'menu' ) {
                return 'fa fa-sitemap';
            }

            if ( this.item.type === 'frontpage' ) {
                return 'fa fa-star';
            }

            if ( this.item.type === 'calendar' ) {
                return 'fa fa-calendar';
            }

            if ( this.item.type === 'event' ) {
                return 'fa fa-clock';
            }

            if ( this.item.type === 'directions' ) {
                return 'fa fa-map-marker';
            }

            if ( this.item.type === 'multipage' ) {
                return 'fa fa-box';
            }

            if ( this.item.type === 'teaserpage' ) {
                return 'fa fa-boxes';
            }

            if ( this.item.type === 'listpage' ) {
                return 'fa fa-list-alt';
            }

            if ( this.item.type === 'alias' ) {
                return 'fa fa-link';
            }

            return 'fa fa-file';
        }

    }

}
