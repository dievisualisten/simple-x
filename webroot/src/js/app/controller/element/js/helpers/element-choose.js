import { Arr, Obj, Any, Event } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";
export default {

    name: 'SxElementChoose',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        modelValue: {
            required: true
        },

        article: {
            required: true
        },

        type: {
            required: true
        }

    },

    computed: {

    },

    data()
    {
        return {
            load: false,
        };
    },

    methods: {

        openModal()
        {
            this.$emit('update:modelValue', true);
        },

        closeModal()
        {
            this.$emit('update:modelValue', false);
        },

        insertNodes()
        {
            if ( ! this.$refs.index || ! this.$refs.index.$refs.table ) {
                return;
            }

            let ids = Obj.get(this.$refs.index.$refs.table,
                'veSelected', []);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            let query = {
                element_ids: ids, article_id: this.article.id
            };

            let queryDone = () => {
                Event.fire('elements-add:done', ids);

                this.closeModal();
            };

            Ajax.call('elements-add', false, query, options)
                .then(queryDone, () => null);
        },

    },

    renderHeader()
    {
        return (
            <h4 class="text-primary">
                { this.trans('Elemente hinzufügen') }
            </h4>
        );
    },

    renderBody()
    {
        let props = {
            routing: false, disableEdit: true,
        };

        props.override = {
            pagetype: this.type, except: this.article.id
        };

        return (
            <SxElementIndex ref="index" {...props} />
        );
    },

    renderFooter()
    {
        return (
            <div class="grid grid--row grid--right grid--10">
                <div class="col">
                    <NButton type="secondary" onClick={this.closeModal} disabled={this.load}>
                        { this.trans('Close') }
                    </NButton>
                </div>
                <div class="col">
                    <NButton type="success" onClick={this.insertNodes} disabled={this.load}>
                        { this.trans('Apply') }
                    </NButton>
                </div>
            </div>
        );
    },

    render()
    {
        let classList = [
            'sx-element-choose'
        ];

        let props = {
            modelValue: true,
            listen: false,
            height: '100%',
            width: '100%',
        };

        props['onUpdate:modelValue'] = () => {
            this.closeModal();
        };

        return (
            <NModal class={classList} {...props}>
                { { header: this.ctor('renderHeader'), body: this.ctor('renderBody'), footer: this.ctor('renderFooter') } }
            </NModal>
        );
    }

}
