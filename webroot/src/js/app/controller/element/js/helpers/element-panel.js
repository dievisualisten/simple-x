import { Obj, Arr, Event } from "@kizmann/pico-js";
import { Config, Ajax } from "@dievisualisten/simplex";
import preview from "@kizmann/nano-ui/src/preview/src/preview/preview";

export default {

    name: 'SxElementPanel',

    inject: {
        SxEditController: {
            default: undefined
        }
    },

    props: {

        prop: {
            required: true
        },

        type: {
            required: true
        },

    },

    provide()
    {
        return {
            SxElementPanel: this
        };
    },

    computed: {

        modelValue()
        {
            return Obj.get(this.SxEditController.value, this.prop);
        },

        defaultLocale()
        {
            return Config.get('app.language.defaultlanguage');
        },

        rootLocale()
        {
            return this.$root.locale;
        }

    },

    data()
    {
        return {
            load: false, insertModal: false, tempDraft: [], confirmChange: null, confirmSave: null
        };
    },

    methods: {

        startAction(action, ...props)
        {
            let isNew = this.SxEditController.value._new;

            if ( ! this.dirty && ! isNew ) {
                return this[action].call(this, ...props);
            }

            let prop = 'confirmChange';

            if ( isNew ) {
                prop = 'confirmSave';
            }

            let callback = () => this[action].call(this, ...props);

            if ( isNew ) {
                callback = () => this.SxEditController.NDataform.queryApply();
            }

            this[prop] = callback;
        },

        showInsert()
        {
            this.insertModal = true;
        },

        showEdit(id = null)
        {
            this.$refs.editForm.openModal(id);
        },

        showIndex()
        {
            this.$refs.editForm.closeModal();
        },

        updateValue(value, dirty = true)
        {
            this.dirty = dirty;

            Obj.set(this.SxEditController.value, this.prop, value);
        },

        resetItem()
        {
            this.dirty = false;
        },

        updateItem(value)
        {
            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('articles-show', false, this.SxEditController.value, options)
                .then(this.queryDone, () => null);

            this.tempDraft.push(value.id);
        },

        updateItems(ids)
        {
            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('articles-show', false, this.SxEditController.value, options)
                .then(this.queryDone, () => null);

            this.tempDraft = Arr.merge(this.tempDraft, ids);
        },

        queryDone(res)
        {
            let value = Obj.get(res.data.data,
                this.prop, []);

            Arr.each(value, (draft, index) => {

                if ( !Arr.has(this.tempDraft, draft.id) ) {
                    return;
                }

                Obj.set(value[index], '_joinData.draft', 0);
            });

            this.updateValue(value, false);
        },

    },

    mounted()
    {
        Event.bind('articles-edit:query', this.resetItem,
            this._.uid);

        Event.bind('elements-edit:query', this.updateItem,
            this._.uid);

        Event.bind('elements-add:done', this.updateItems,
            this._.uid);
    },

    beforeUnmount()
    {
        Event.unbind('articles-edit:query', this._.uid);
        Event.unbind('elements-edit:query', this._.uid);
        Event.unbind('elements-add:done', this._.uid);
    },

    renderItem(props)
    {
        let bodyHtml = (
            <div class="sx-element-preview__title">
                <SxElementNodeMarkup item={props.item} useArticle={false} />
            </div>
        );

        let footerHtml = (
            <div class="sx-element-preview__draft">
                <NSwitch onType="info" vModel={props.item._joinData.draft}>
                    {this.trans('Is draft')}
                </NSwitch>
            </div>
        );

        let toolbarHtml = (
            <div class="sx-element-preview__toolbar toolbar">
                <NButton size="xs" type="primary" icon="fa fa-pencil" square={true} onClick={() => this.startAction('showEdit', props.value.id)} />
                <NButton size="xs" type="danger" icon="fa fa-unlink" square={true} onClick={props.remove} />
            </div>
        );

        let classList = [
            'sx-element-preview'
        ];

        if ( props.item._joinData.draft ) {
            classList.push('is-draft');
        }

        return (
            <div class={classList}>
                {[bodyHtml, footerHtml, toolbarHtml]}
            </div>
        );
    },

    renderToolbar()
    {
        return (
            <div class="sx-element-panel__toolbar toolbar">
                <div class="sx-element-panel__toolbar-info">
                    {this.choice('Insgesamt :count Eintrag| Insgesamt :count Einträge', this.modelValue.length)}
                </div>
                <div class="sx-element-panel__toolbar-edit">
                    <NButton type="info" onClick={() => this.startAction('showInsert')}>{this.trans('Eintrag anhängen')}</NButton>
                </div>
                <div class="sx-element-panel__toolbar-edit">
                    <NButton type="primary" onClick={() => this.startAction('showEdit')}>{this.trans('Neuer Einrtag')}</NButton>
                </div>
            </div>
        );
    },

    renderModal()
    {
        let props = {
            use: 'SxElementEdit',
            width: '1440px',
            height: '100%',
            localized: true,
            showQuery: 'elements-show',
            editQuery: 'elements-edit',
            newTitle: this.trans('Element erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="editForm" {...props} />
        );
    },

    renderChoose()
    {
        if ( !this.insertModal ) {
            return null;
        }

        let props = {
            article: this.SxEditController.value,
            type: this.type,
        }

        return (
            <SxElementChoose vModel={this.insertModal} {...props} />
        );
    },

    renderDraglist()
    {
        let props = {
            items: this.modelValue,
            renderNode: this.ctor('renderItem'),
            useKeys: true,
            itemHeight: 50,
            safezone: (height) => height * 0.51,
        };

        props['onUpdate:items'] = (value) => {
            this.updateValue(value, true);
        };

        props['onRowDblclick'] = (value) => {
            this.showEdit(value.item.id);
        };

        return (<NDraglist class="sx-element-panel__draglist" {...props} />);
    },

    renderSaveConfirm()
    {
        let props = {
            modelValue: !! this.confirmSave,
            listen: false,
        };

        props['onConfirm'] = () => {
            this.confirmSave.call();
        }

        props['onAbort'] = () => {
            this.confirmSave = null;
        }

        return (
            <NConfirm {...props}>
                { this.trans('Um Einträge anzuhängen muss der Artikel exestieren, möchten Sie den Artikel jetzt speichern?') }
            </NConfirm>
        );
    },

    renderChangeConfirm()
    {
        let props = {
            modelValue: !! this.confirmChange,
            listen: false,
        };

        props['onConfirm'] = () => {
            this.confirmChange.call();
        }

        props['onAbort'] = () => {
            this.confirmChange = null;
        }

        return (
            <NConfirm {...props}>
                { this.trans('Sie haben ungespeicherte Änderungen in der Liste die überschrieben werden, wollen Sie fortfahren?') }
            </NConfirm>
        );
    },

    render()
    {
        return (
            <NLoader visible={this.load} class="sx-element-panel">
                {this.ctor('renderToolbar')()}
                {this.ctor('renderDraglist')()}
                {this.ctor('renderChangeConfirm')()}
                {this.ctor('renderSaveConfirm')()}
                {this.ctor('renderModal')()}
                {this.ctor('renderChoose')()}
            </NLoader>
        );
    }

}
