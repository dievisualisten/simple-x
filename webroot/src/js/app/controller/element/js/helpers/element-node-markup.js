import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxElementNodeMarkup',

    props: {

        item: {
            required: true
        },

        prop: {
            default()
            {
                return 'title';
            },
            type: [String]
        },

        usePublished: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        useArticle: {
            default()
            {
                return true;
            },
            type: [Boolean]
        }

    },

    computed: {

        defaultLocale()
        {
            return Config.get('app.language.defaultlanguage');
        },

        rootLocale()
        {
            return this.$root.locale;
        }

    },

    render()
    {
        let classList = [
            'sx-article-markup'
        ];

        let published = this.Obj.get(this.item, 'published', false);

        if ( this.usePublished && ! published ) {
            this.Arr.add(classList, 'sx-not-published');
        }

        let articles = this.Obj.get(this.item, 'articles', []);

        if ( this.useArticle && ! articles.length ) {
            this.Arr.add(classList, 'sx-not-linked');
        }

        let isDefaultLocale = this.item._locale !== this.rootLocale &&
            this.defaultLocale !== this.rootLocale;

        if ( isDefaultLocale && ! this.Arr.has(['domain', 'menu'], this.item.type) ) {
            this.Arr.add(classList, 'sx-not-translated');
        }

        return (
            <span class={classList}>
                { this.$slots.default || this.Obj.get(this.item, this.prop) }
            </span>
        );
    }

}
