import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('elements');

Ajax.bind('elements-add', function (ajax, query, options) {
    let route = Route.get('elements.add', query, null);
    return ajax.post(route, query, options);
});

require("./config/element-default.config");
require("./config/element-custom.config");

import ElementIndex from "./js/element/element-index";
App.component(ElementIndex.name, ElementIndex);

import ElementEdit from "./js/element/element-edit";
App.component(ElementEdit.name, ElementEdit);

import ElementPanel from "./js/helpers/element-panel";
App.component(ElementPanel.name, ElementPanel);

import ChoosePanel from "./js/helpers/element-choose";
App.component(ChoosePanel.name, ChoosePanel);

import ElementPagetypeMarkup from "./js/helpers/element-pagetype-markup";
App.component(ElementPagetypeMarkup.name, ElementPagetypeMarkup);

import ElementNodeMarkup from "./js/helpers/element-node-markup";
App.component(ElementNodeMarkup.name, ElementNodeMarkup);