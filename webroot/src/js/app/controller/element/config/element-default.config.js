sx.Form.set('sx-element-default', [
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.media.show', '0') === '1';
            },
            vAwait: '$$value.id',
            $props: {
                name: 'media',
                sort: 30,
                label: pi.Locale.trans('Medien'),
                icon: 'fa fa-image',
                relative: true,
                keep: true
            },
            content: {

                // <SxAttachmentIndex:0>
                'SxAttachmentIndex:0': {
                    vAwait: '$$value.media',
                    $props: {
                        forceShowInfo: true,
                        routing: false,
                    },
                    content: function () {

                        var zones = {};

                        pi.Arr.each(this.dropzones, function (dropzone) {

                            // <SxAttachedItems:index>
                            zones['SxAttachedItems:' + dropzone._key] = {
                                vIf: function () {
                                    return pi.Obj.get(this.activezones,
                                        [dropzone._key, 'show'], '0') === '1';
                                },
                                $props: {
                                    prop: dropzone._key,
                                    label: dropzone.name,
                                    media: '$$value.media'
                                }
                            };
                            // </ SxAttachedItems:index>

                        }, {});

                        return zones;
                    }
                }
                // </ SxAttachmentIndex:0>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.map.show', '0') === '1';
            },
            $props: {
                name: 'map',
                sort: 50,
                label: pi.Locale.trans('Karte'),
                icon: 'fa fa-map-marked'
            },
            content: {

                'NFormItem:-1': {
                    $props: {
                        label: pi.Locale.trans('Kategorie')
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'extended_data_cfg.map.mapgroup', fallback: []
                            },
                            $props: {
                                multiple: true, domain: 'mapgroup', clearable: true
                            },
                        },
                    }
                },

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Addresse'),
                        prop: 'address'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'address',
                                    },
                                    $props: {
                                        placeholder: pi.Locale.trans('Adresse eingeben')
                                    }
                                },
                                // </ NInput:0>

                                // <NInput:0>
                                'NButton:0': {
                                    $on: {
                                        onClick: function () {
                                            this.$configRefs.marker.setMarkerByAddress(this.value.address);
                                        },
                                    },
                                    content: [
                                        pi.Locale.trans('Marker setzen')
                                    ]
                                }
                                // </ NInput:0>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:0>

                // <div:1>
                'div:1': {
                    class: 'grid grid--row grid--wrap grid--15',
                    content: {

                        // <NFormItem:0>
                        'NFormItem:0': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Latitude'),
                                prop: 'lat'
                            },
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'lat', fallback: 0
                                    },
                                    $props: {
                                        disabled: false
                                    }
                                },
                                // </ NInput:0>

                            }
                        },
                        // </ NFormItem:0>

                        // <NFormItem:1>
                        'NFormItem:1': {
                            class: 'col--1-1 col--1-2@md',
                            $props: {
                                label: pi.Locale.trans('Longitude'),
                                prop: 'lon'
                            },
                            content: {

                                // <NInput:0>
                                'NInput:0': {
                                    model: {
                                        path: 'lon', fallback: 0
                                    },
                                    $props: {
                                        disabled: false
                                    }
                                },
                                // </ NInput:0>

                            }
                        },
                        // </ NFormItem:1>

                    }
                },
                // </ div:1>

                // <NFormItem:3>
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Karte'),
                    },
                    content: {

                        // <NMap:0>
                        'NMap:0': {
                            $props: {
                                lat: '$$value.lat',
                                lng: '$$value.lon',
                                zoom: 12
                            },
                            content: {

                                // <NMapMarker:0>
                                'NMapMarker:0': {
                                    ref: 'marker',
                                    $props: {
                                        lat: '$$value.lat',
                                        lng: '$$value.lon',
                                        drag: true
                                    },
                                    $on: {
                                        'onUpdate:lat': function (value) {
                                            this.value.lat = value;
                                        },
                                        'onUpdate:lng': function (value) {
                                            this.value.lon = value;
                                        }
                                    }
                                },
                                // </ NMapMarker:0>

                            }
                        },
                        // </ NMap:0>

                    }
                },
                // </ NFormItem:3>

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.pagetype,
                    'panels.options.show', '0') === '1';
            },
            $props: {
                name: 'option',
                sort: 80,
                label: pi.Locale.trans('Einstellung'),
                icon: 'fa fa-cog'
            },
            content: {

                // <NFormItem:0>
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Zuordnung'),
                        prop: 'aco_id'
                    },
                    content: {

                        // <SxSelectAco:0>
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            },
                        }
                        // </ SxSelectAco:0>

                    }
                },
                // </ NFormItem:0>

                // <NFormItem:1>
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Status'),
                        prop: 'active'
                    },
                    content: {

                        // <NSwitch:0>
                        'NSwitch:0': {
                            model: {
                                path: 'active'
                            },
                            content: [
                                pi.Locale.trans('Diese Seite ist aktiviert')
                            ]
                        }
                        // </ NSwitch:0>

                    }
                },
                // </ NFormItem:1>

                // <NFormItem:2>
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Startzeit'),
                        prop: 'begin_publishing'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NDatepicker:0>
                                'NDatepicker:0': {
                                    model: {
                                        path: 'begin_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                },
                                // </ NDatepicker:0>

                                // <NTimepicker:1>
                                'NTimepicker:1': {
                                    model: {
                                        path: 'begin_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                }
                                // </ NTimepicker:1>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:2>

                // <NFormItem:3>
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Endzeit'),
                        prop: 'end_publishing'
                    },
                    content: {

                        // <NButtonGroup:0>
                        'NButtonGroup:0': {
                            content: {

                                // <NDatepicker:0>
                                'NDatepicker:0': {
                                    model: {
                                        path: 'end_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                },
                                // </ NDatepicker:0>

                                // <NTimepicker:1>
                                'NTimepicker:1': {
                                    model: {
                                        path: 'end_publishing'
                                    },
                                    $props: function (entity) {
                                        return {
                                            clearable: true, disabled: !entity.active
                                        };
                                    }
                                }
                                // </ NTimepicker:1>

                            }
                        }
                        // </ NButtonGroup:0>

                    }
                },
                // </ NFormItem:3>

            }
        },
        // </ NTabsItem:0>
    },
]);
