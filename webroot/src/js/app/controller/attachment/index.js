import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.bind('attachments-tree', function (ajax, query, options) {
    let route = Route.get('attachments.tree', query, query);
    return ajax.get(route, options);
});

Ajax.bind('attachments-edit', function (ajax, query, options) {
    let route = Route.get('attachments.edit', query, null);
    return ajax.post(route, Ajax.form(query), options);
});

Ajax.bind('attachments-move', function (ajax, query, options) {
    let route = Route.get('attachments.move', query, null);
    return ajax.post(route, query, options);
});

Ajax.bind('attachments-upload', function (ajax, query, options) {
    let route = Route.get('attachments.upload', null, null);
    return ajax.post(route, query, options);
});

Ajax.bind('attachments-whitespace', function (ajax, query, options) {
    let route = Route.get('attachments.whitespace', null, null);
    return ajax.post(route, query, options);
});

Ajax.bind('attachments-crop', function (ajax, query, options) {
    let route = Route.get('attachments.crop', null, null);
    return ajax.post(route, query, options);
});

Ajax.bind('attachments-focus', function (ajax, query, options) {
    let route = Route.get('attachments.focus', null, null);
    return ajax.post(route, query, options);
});

// Ajax.bind('attachments-replace', function (ajax, query, options) {
//     let route = Route.get('attachments.replace', null, null);
//     return ajax.post(route, Ajax.form(query), options);
// });

Ajax.rest('attachments', ['edit']);

require("./config/attachment-folder.config");
require("./config/attachment-video.config");
require("./config/attachment-default.config");

require("./config/attached-default.config");

import SxAttachmentIndex from "./js/attachment/attachment-index";
App.component(SxAttachmentIndex.name, SxAttachmentIndex);

import SxAttachmentEdit from "./js/attachment/attachment-edit";
App.component(SxAttachmentEdit.name, SxAttachmentEdit);

import SxAttachmentFolder from "./js/attachment/attachment-folder";
App.component(SxAttachmentFolder.name, SxAttachmentFolder);

import SxAttachmentVideo from "./js/attachment/attachment-video";
App.component(SxAttachmentVideo.name, SxAttachmentVideo);

import SxAttachmentChoose from "./js/helpers/attachment-choose";
App.component(SxAttachmentChoose.name, SxAttachmentChoose);

import SxAttachmentUpload from "./js/helpers/attachment-upload";
App.component(SxAttachmentUpload.name, SxAttachmentUpload);

import SxAttachmentTreeMarkup from "./js/helpers/attachment-tree-markup";
App.component(SxAttachmentTreeMarkup.name, SxAttachmentTreeMarkup);

import SxAttachmentTableMarkup from "./js/helpers/attachment-table-markup";
App.component(SxAttachmentTableMarkup.name, SxAttachmentTableMarkup);

import SxAttachmentTitleMarkup from "./js/helpers/attachment-title-markup";
App.component(SxAttachmentTitleMarkup.name, SxAttachmentTitleMarkup);

import SxAttachedItems from "./js/attached/attached-items";
App.component(SxAttachedItems.name, SxAttachedItems);

import SxAttachedItem from "./js/attached/attached-item";
App.component(SxAttachedItem.name, SxAttachedItem);

import SxAttachedForm from "./js/attached/attached-form";
App.component(SxAttachedForm.name, SxAttachedForm);
