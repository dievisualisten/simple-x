import { Obj } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachmentTableMarkup',

    props: {

        item: {
            required: true
        },

    },

    computed: {

        defaultLocale()
        {
            return Config.get('app.language.defaultlanguage');
        },

        rootLocale()
        {
            return this.$root.locale;
        }

    },

    render()
    {
        let classList = [
            'sx-attachment-table-markup'
        ];

        let isNotLocalized = this.item._locale !== this.rootLocale &&
            this.defaultLocale !== this.rootLocale;

        if ( isNotLocalized ) {
            classList.push('sx-not-translated');
        }

        if ( ! this.item.articles.length ) {
            classList.push('sx-not-linked');
        }

        return (
            <div class={classList}>
                <span>
                    {Obj.get(this.item, 'title') || this.trans('Kein Titel')}
                </span>
                <span>
                    {Obj.get(this.item, 'alternative') || this.trans('Kein Alternativtext')}
                </span>
            </div>

        );
    }

}
