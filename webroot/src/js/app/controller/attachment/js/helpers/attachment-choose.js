import { UUID, Arr, Obj, Any, Dom, Data, Event } from "@kizmann/pico-js";

export default {

    name: 'SxAttachmentChoose',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        modelValue: {
            required: true
        },

        allowed: {
            default()
            {
                return ['image'];
            },
            type: [Array]
        }

    },

    computed: {

        tempValue()
        {
            return this.SxIndexController.veCurrent;
        }

    },
    
    methods: {

        updateValue()
        {
            this.$emit('update:modelValue', this.tempValue);
        },

        removeValue()
        {
            this.$emit('update:modelValue', null);
        },

        canApply()
        {
            let type = Obj.get(this.tempValue, 'type', 'empty');

            return ! Arr.has(this.allowed, type);
        },

        canRemove()
        {
            return Any.isEmpty(this.modelValue);
        }

    },

    renderSource()
    {
        if ( Any.isEmpty(this.modelValue) ) {
            return (<NEmptyIcon />);
        }

        return (
            <NPreview file={this.modelValue.preview} thumb={this.modelValue.cropurl}></NPreview>
        );
    },

    renderTarget()
    {
        if ( Any.isEmpty(this.tempValue) ) {
            return (<NEmptyIcon />);
        }

        return (
            <NPreview file={this.tempValue.preview} thumb={this.tempValue.cropurl}></NPreview>
        );
    },

    renderApply()
    {
        let props = {
            type: 'primary',
            disabled: this.canApply(),
        };

        return (
            <NButton {...props} onClick={this.updateValue}>
                { this.trans('Apply') }
            </NButton>
        );
    },

    renderRemove()
    {
        let props = {
            type: 'danger',
            disabled: this.canRemove(),
        };

        return (
            <NButton {...props} onClick={this.removeValue}>
                { this.trans('Remove') }
            </NButton>
        );
    },


    render()
    {
        let classList = [
            'sx-attachment-choose'
        ];

        if ( this.canApply() ) {
            classList.push('is-disabled');
        }

        return (
            <div class={classList}>
                <div class="sx-attachment-choose__source">
                    <div className="sx-attachment-choose__info">
                        {this.trans('Aktuelle Vorschau')}
                    </div>
                    { this.ctor('renderSource')() }
                </div>
                <div class="sx-attachment-choose__button">
                    { this.ctor('renderApply')() }
                    { this.ctor('renderRemove')() }
                </div>
                <div class="sx-attachment-choose__target">
                    <div class="sx-attachment-choose__info">
                        { this.trans('Ausgewählter Ersatz') }
                    </div>
                    { this.ctor('renderTarget')() }
                </div>
            </div>
        );
    }

}
