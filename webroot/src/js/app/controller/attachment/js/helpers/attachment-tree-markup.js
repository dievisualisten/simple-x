export default {

    name: 'SxAttachmentTreeMarkup',

    inject: {

        SxIndexController: {
            default: undefined
        },

        NDatatree: {
            default: undefined
        },

        NDraggable: {
            default: undefined
        },

        NDraggableItem: {
            default: undefined
        }

    },

    props: {

        item: {
            required: true
        },

        value: {
            required: true
        }

    },

    computed: {

        folderIcon()
        {
            return this.value.depth ? 'fa fa-folder' : 'fa fa-folders';
        }

    },

    methods: {

        editItem()
        {
            if ( this.value.depth > 1 ) {
                return this.SxIndexController.showEditModal('folderForm', this.item.id);
            }
        },

        deleteItem()
        {
            this.NDatatree.deleteItem(this.item.id);
        }

    },

    renderIcon()
    {
        let icon = this.folderIcon;

        if ( this.item.type === 'image' ) {
            icon = 'fa fa-image';
        }

        if ( this.item.type === 'video' ) {
            icon = 'fa fa-video';
        }

        return (
            <div class="sx-attachment-tree-markup__icon">
                <span class={icon}></span>
            </div>
        );
    },

    renderTitle()
    {
        return (
            <div class="sx-attachment-tree-markup__title">
                <span>{ this.item.name }</span>
            </div>
        )
    },

    renderAction()
    {
        if ( this.value.depth <= 1 && ! window.ADMINMODE ) {
            return null;
        }

        return (
            <div class="sx-attachment-tree-markup__action toolbar">
                <NButton size="xs" type="primary" icon="fa fa-pen" square={true} onClick={this.editItem} />
                <NButton size="xs" type="danger" icon="fa fa-trash" square={true} onClick={this.deleteItem} />
            </div>
        );
    },

    render()
    {
        return (
            <div class="sx-attachment-tree-markup">
                { this.ctor('renderIcon')() }
                { this.ctor('renderTitle')() }
                { this.ctor('renderAction')() }
            </div>
        )
    }

}
