export default {

    name: 'SxAttachmentTitleMarkup',

    props: {

        value: {
            required: true
        },

        prop: {
            default()
            {
                return 'title';
            },
            type: [String]
        },

        usePublished: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        useMenus: {
            default()
            {
                return true;
            },
            type: [Boolean]
        }

    },

    computed: {

        defaultLocale()
        {
            return this.Data.config('app.language.defaultlanguage');
        },

        rootLocale()
        {
            return this.$root.locale;
        }

    },


    render()
    {
        let classList = [
            'sx-attachment-title-markup'
        ];

        let isNotLocalized = this.value._locale !== this.rootLocale &&
            this.defaultLocale !== this.rootLocale;

        if ( isNotLocalized ) {
            classList.push('sx-not-translated');
        }

        return (
            <span class={classList}>
                <slot>{ this.Obj.get(this.value, this.prop) || this.trans('Kein Titel') }</slot>
            </span>
        );
    }

}
