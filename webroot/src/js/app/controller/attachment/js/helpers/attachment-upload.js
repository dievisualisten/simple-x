import Axios from "axios";
import { UUID, Arr, Obj, Num, Dom, Data, Event } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachmentUpload',

    computed: {

        folder()
        {
            return this.veCascade.length >= 2 ?
                Arr.last(this.veCascade) : null;
        },

        uploadable()
        {
            return this.load === false && this.folder !== null &&
                Arr.filter(this.files, { done: false }).length !== 0;
        },

        cancelable()
        {
            return this.cancel !== null && this.load === true &&
                this.queue.length !== 0;
        },

        completed()
        {
            return this.files.length === Arr.filter(this.files,
                { done: true }).length && this.files.length !== 0;
        },

        conflicted()
        {
            return Arr.filter(this.files,
                { error: true }).length !== 0;
        }

    },

    data()
    {
        return {
            veCascade: [],
            files: [],
            cursor: -1,
            queue: [],
            visible: false,
            load: false,
            cancel: null,
            folders: []
        };
    },

    beforeMount()
    {
        Event.bind('attachments-tree', this.getFolders);

        this.updateTreeData();
    },

    mounted()
    {
        this.Dom.find(document).on('dragover', (event) => {

            event.preventDefault();

            if ( this.visible ) {
                return;
            }

            if ( Arr.has(event.dataTransfer.types, 'Files') ) {
                this.openModal();
            }

        }, this._.uid);

        this.Dom.find(document).on('drop', (event) => {

            event.preventDefault();

            if ( Arr.has(event.dataTransfer.types, 'Files') ) {
                event.stopPropagation();
            }

            if ( this.load ) {
                return;
            }

            Arr.each(Array.from(event.dataTransfer.files), (file) => {
                this.appendFile(file);
            });

        }, this._.uid);

        Event.bind('attachments/cascade', ({ ids }) => {
            if ( ! this.cancelable ) {
                this.veCascade = ids;
            }
        });

        Event.bind('upload/open', this.openModal);
    },

    methods: {

        openModal()
        {
            this.visible = true;
        },

        closeModal()
        {
            this.visible = false;
        },

        changeLegacy()
        {
            Arr.each(Array.from(this.$refs.legacy.files),
                (file) => this.appendFile(file));

            this.$refs.legacy.value = null;
        },

        appendFile(file)
        {
            let files = Obj.clone(this.files);

            let fileObject = {
                id: UUID(), progress: -1, done: false, error: false, file
            };

            this.files = Arr.add(files, fileObject);
        },

        clearFiles()
        {
            this.files = Arr.filter(this.files, { done: false });
        },

        clearAllFiles()
        {
            this.files = [];
        },

        startQueue()
        {
            this.cursor = -1;

            this.queue = Arr.filter(this.files, {
                done: false
            });

            this.continueQueue();
        },

        continueQueue()
        {
            if ( this.cursor++ < this.queue.length - 1 ) {
                return this.queueUpload();
            }

            this.load = false;

            this.Notify(this.trans('Dateiupload abgeschlossen'), 'info');

            Event.fire('upload:done');
        },

        stopQueue()
        {
            if ( this.cancel ) {
                this.cancel();
            }

            this.queue = [];
            this.load = false;
        },

        queueUpload()
        {
            let current = Obj.get(this.queue, this.cursor);

            Obj.assign(current, {
                done: false, error: false
            });

            let options = {
                onLoad: () => {
                    this.load = true
                },
                cancelToken: new Axios.CancelToken((cancel) => {
                    this.cancel = cancel;
                }),
                onUploadProgress: (event) => {
                    current.progress = Num.fixed((100 / current.file.size) * event.loaded, 0);
                }
            };

            let form = new FormData();

            console.log(this.veCascade, this.folder);
            form.append('parent_id', this.folder);
            form.append('file', current.file);

            Ajax.call(['attachments-upload'], false, form, options)
                .then(this.uploadDone, this.uploadError);
        },

        uploadDone()
        {
            let current = Obj.get(this.queue, this.cursor, {});

            Obj.assign(current, {
                progress: 100, done: true
            });

            this.continueQueue();
            this.clearFiles();
        },

        uploadError()
        {
            let current = Obj.get(this.queue, this.cursor, {});

            Obj.assign(current, {
                progress: 100, error: true
            });

            this.continueQueue();
            this.clearFiles();
        },

        updateTreeData()
        {
            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call('attachments-tree', false, { _locale: this.$root.locale }, options)
                .then(this.queryDone, () => this.$emit('close'));
        },

        queryDone(res)
        {
            Data.set('attachments-tree', res.data);
            Event.fire('attachments-tree', res.data);
        },

        getFolders()
        {
            let folders = Data.get('attachments-tree.data', []);

            folders = Arr.filter(folders, (folder) => {
                return folder.id !== '11111111-1111-1111-1111-111111111111';
            });

            this.folders = Arr.each(folders, (folder) => {
                return Obj.assign(folder, { disabled: true });
            });
        }

    },

    renderButton()
    {
        return (
            <NButton icon="fa fa-cloud" onClick={this.openModal}>
                { this.trans('Upload') }
            </NButton>
        );
    },

    renderSelect()
    {
        return (
            <div class="sx-attachment-upload__select">
                <NCascader vModel={this.veCascade} disabled={this.cancelable} options={this.folders} placeholder={this.trans('Bitte wählen Sie den Ordner aus')} labelProp="name" valueProp="id" childProp="children" trigger="hover" />
            </div>
        );
    },

    renderBody()
    {
        let props = {
            items: this.files
        };

        props['onUpdate:items'] = (items) => {
            this.files = items;
        };

        return (
            <NLoader visible={this.load} class="sx-attachment-upload__body">
                <SxFilelist {...props} />
            </NLoader>
        );
    },

    renderFooter()
    {
        return (
            <div class="sx-attachment-upload__footer">
                <div class="grid grid--row grid--10">
                    <div class="col">
                        <NButton disabled={this.cancelable} onClick={() => this.$refs.legacy.click()}>
                            { this.trans('Select files') }
                        </NButton>
                    </div>
                    <div class="col">
                        <NButton type="warning" disabled={! this.files.length}  onClick={this.clearAllFiles}>
                            { this.trans('Clear') }
                        </NButton>
                    </div>
                    <div class="col col--flex-auto">
                        { /* Spacer */ }
                    </div>
                    <div class="col">
                        <NButton type="secondary" onClick={this.closeModal}>
                            { this.trans('Close') }
                        </NButton>
                    </div>
                    <div class="col" vShow={this.cancelable}>
                        <NButton type="danger" disabled={! this.cancelable}  onClick={this.stopQueue}>
                            { this.trans('Cancel') }
                        </NButton>
                    </div>
                    <div class="col" vShow={! this.cancelable}>
                        <NButton type="success" disabled={! this.uploadable} onClick={this.startQueue}>
                            { this.trans('Upload') }
                        </NButton>
                    </div>
                </div>
            </div>
        );
    },

    renderModal()
    {
        let slots = {};

        slots.raw = () => (
            <div class="sx-attachment-upload__frame">
                { this.ctor('renderSelect')() }
                { this.ctor('renderBody')() }
                { this.ctor('renderFooter')() }
                <input ref="legacy" type="file" multiple="true" style="display: none !important;" onChange={this.changeLegacy} />
            </div>
        );

        return (
            <NModal vModel={this.visible} width="980px" height="100%" v-slots={slots} />
        );
    },

    render()
    {
        let classList = [
            'sx-attachment-upload'
        ];

        if ( this.completed ) {
            classList.push('sx-attachment-upload--completed');
        }

        if ( this.conflicted ) {
            classList.push('sx-attachment-upload--conflicted');
        }

        return (
            <div class={classList}>
                { this.ctor('renderButton')() }
                { this.ctor('renderModal')() }
            </div>
        );
    }

}
