import { Obj, Any } from "@kizmann/pico-js";


export default {

    name: 'SxAttachedItem',

    inject: {

        NDraggableItem: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        },

        SxAttachedItems: {
            default: undefined
        },

    },

    props: {

        item: {
            required: true
        },

        value: {
            required: true
        }

    },

    methods: {

        attachedEdit()
        {
            this.SxAttachedItems.editAttached({
                value: this.value, item: this.item
            });
        },

        attachmentEdit()
        {
            this.SxIndexController.showEditModal('defaultForm',
                this.item.id);
        },

        attachedRemove()
        {
            this.NDraggableItem.remove();
        },

    },

    renderThumb()
    {
        return (
            <div class="sx-attached-item__thumb">
                <NPreview file={this.item.preview} thumb={this.item.cropurl} fit="contain"></NPreview>
            </div>
        );
    },

    renderTitle()
    {
        let title = Obj.get(this.item, 'attached.title');

        if ( Any.isEmpty(title) ) {
            title = Obj.get(this.item, 'title');
        }

        if ( Any.isEmpty(title) ) {
            title = this.trans('Kein Titel');
        }

        let alternative = Obj.get(this.item, 'attached.alternative');

        if ( Any.isEmpty(alternative) ) {
            alternative = Obj.get(this.item, 'alternative');
        }

        if ( Any.isEmpty(alternative) ) {
            alternative = this.trans('Kein Alternativtext');
        }

        return (
            <div class="sx-attached-item__text">
                <div class="sx-attached-item__text-title">
                    <span class="text-ellipsis">{ title }</span>
                </div>
                <div class="sx-attached-item__text-alternative">
                    <span class="text-ellipsis">{ alternative }</span>
                </div>
            </div>
        );
    },

    renderAction()
    {
        return (
            <div class="sx-attached-item__action toolbar">
                <NButton type="info" size="xs" square={true} icon="fa fa-image" onClick={this.attachmentEdit}></NButton>
                <NButton type="primary" size="xs" square={true} icon="fa fa-pencil" onClick={this.attachedEdit}></NButton>
                <NButton type="danger" size="xs" square={true} icon="fa fa-unlink" onClick={this.attachedRemove}></NButton>
            </div>
            );
    },

    render()
    {
        if ( ! this.item ) {
            return null;
        }

        return (
            <div class="sx-attached-item" onDblclick={this.attachedEdit}>
                { this.ctor('renderThumb')() }
                { this.ctor('renderTitle')() }
                { this.ctor('renderAction')() }
            </div>
        );
    }

}
