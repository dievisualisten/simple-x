import { Obj, Dom } from "@kizmann/pico-js";

export default {

    name: 'SxAttachedItems',

    props: {

        label: {
            required: true,
            type: [String]
        },

        prop: {
            required: true,
            type: [String]
        },

        media: {
            default() {
                return {};
            },
            type: [Object]
        }

    },

    provide()
    {
        return {
            SxAttachedItems: this
        };
    },

    computed: {

        items() {
            return Obj.get(this.media, this.prop, []);
        }

    },

    data()
    {
        return {
            editNode: null, collapsed: true, clearConfirm: false
        };
    },

    methods: {

        clearList(prop)
        {
            this.media[prop] = [];
        },

        transformData(data)
        {
            let attached = {
                attachment_id: data.id, title: '', alternative: '', description: ''
            };

            return this.Obj.assign({ attached }, data);
        },

        toggleCollapse(event)
        {
            let isButton = Dom.find(event.target)
                .closest('.n-button');

            if ( ! isButton ) {
                this.collapsed = ! this.collapsed;
            }
        },

        clearModal()
        {
            this.clearConfirm = true;
        },

        editAttached(props)
        {
            this.editNode = props;
        },

        applyAttached(props)
        {
            Obj.set(this, props.value.route, props.item);

            this.editNode = null;
        },

        closeAttached()
        {
            this.editNode = null;
        },

    },

    renderHeader()
    {
        return (
            <div class="sx-attached-items__header" onClick={this.toggleCollapse}>
                <div class="sx-attached-items__title">
                    <span>{ this.label }</span> <span>({ this.media[this.prop].length })</span>
                </div>
                <div class="sx-attached-items__action" vShow={this.media[this.prop].length}>
                    <NButton size="sm" type="danger" link={true} onClick={this.clearModal}>
                        { this.trans('Clear') }
                    </NButton>
                </div>
                <div class="sx-attached-items__angle">
                    <span class="fa fa-angle-down"></span>
                </div>
            </div>
        );
    },

    renderBody()
    {
        let props = {
            items: this.media[this.prop],
            group: ['attached'],
            allowGroups: ['attachments', 'attached'],
            itemHeight: 100,
            useKeys: true,
            transformDrop: this.transformData
        };

        props.renderNode = ({ item, value }) =>{
            return (<SxAttachedItem item={item} value={value}/>);
        };

        props.safezone = (height) => {
            return height * 0.51;
        };

        props['onUpdate:items'] = (input) => {
            this.media[this.prop] = input;
        };

        return (
            <div class="sx-attached-items__body">
                <NDraglist {...props} />
            </div>
        );
    },

    renderConfirm()
    {
        let props = {
            listen: false, type: 'danger'
        };

        props.onConfirm = () => {
            this.clearList(this.prop);
        };

        return (
            <NConfirm vModel={this.clearConfirm} {...props}>
                { this.trans('Liste :label wirklich leeren?', { label: this.label }) }
            </NConfirm>
        )
    },

    renderForm()
    {
        if ( ! this.editNode ) {
            return null;
        }

        let props = {
            onClose: this.closeAttached,
            onApply: this.applyAttached,
        };

        return (
            <SxAttachedForm {...props} {...this.editNode} />
        );
    },

    render()
    {
        let classList = [
            'sx-attached-items'
        ];

        if ( this.collapsed ) {
            classList.push('sx-attached-items--collapsed');
        }

        return (
            <div class={classList}>
                { this.ctor('renderHeader')() }
                { this.ctor('renderBody')() }
                { this.ctor('renderConfirm')() }
                { this.ctor('renderForm')() }
            </div>
        )
    }

}
