import { Arr, Obj, Any } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachedForm',

    inheritAttrs: false,

    inject: {

        SxAttachedItems : {
            default: null
        }

    },

    props: {

        item: {
            required: true
        },

        value: {
            required: true
        }

    },

    data()
    {
        return {
            tempItem: Obj.clone(this.item)
        };
    },

    methods: {

        closeModal()
        {
            this.$emit('close');
        },

        applyModal()
        {
            this.$emit('apply', {
                item: this.tempItem, value: this.value
            });
        }

    },

    renderHeader()
    {
        return (
            <div slot="header">
                <h4 class="text-primary">{this.trans('Verknüpfung bearbeiten')}</h4>
            </div>
        );
    },

    renderFooter()
    {
        return (
            <div slot="footer" class="grid grid--row grid--right grid--10">
                <div class="col">
                    <NButton type="secondary" onClick={this.closeModal}>
                        {this.trans('Abort')}
                    </NButton>
                </div>
                <div class="col">
                    <NButton type="success" onClick={this.applyModal}>
                        {this.trans('Apply')}
                    </NButton>
                </div>
            </div>
        );
    },

    render()
    {
        let slots = {
            header: this.ctor('renderHeader')(),
            footer: this.ctor('renderFooter')()
        };

        slots.default = (
            <NForm form={this.tempItem}>
                {
                    Arr.each(Form.get('sx-attached-default', []), (config) => {
                        return (
                            <NConfig modelValue={this.tempItem} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        );

        let props = {
            modelValue: true,
            width: '960px',
            listen: false,
            update: false,
            onClose: this.closeModal,
        };

        return (
            <NModal ref="modal" {...props} v-slots={slots} />
        );
    }

}
