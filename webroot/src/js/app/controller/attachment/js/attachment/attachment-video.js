import DefaultEdit from "../../../default/src/edit/edit"
import { Arr, Data, Event } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachmentVideo',

    extends: DefaultEdit,

    inject: {

        NDataform: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        }

    },

    computed: {

        formConfig()
        {
            return Form.get('sx-attachment-video', []);
        }

    },

    data()
    {
        return {
            folders: Data.get('attachments-tree.data', [])
        };
    },

    mounted()
    {
        Event.bind('attachments-tree', () => {
            this.folders = Data.get('attachments-tree.data', []);
        });

        this.NDataform.setOverride({
            type: 'video', parent_id: Arr.last(this.SxIndexController.veCascade)
        });
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(this.formConfig, (config) => {
                        return (
                            <NConfig modelValue={this.value} config={config} scope={this} />
                        );
                    })
                }
            </NForm>
        )
    }

}
