import DefaultEdit from "../../../default/src/edit/edit"

import { Arr, Obj, Data } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachmentFolder',

    extends: DefaultEdit,

    inject: {

        NDataform: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        }

    },

    computed: {

        formConfig()
        {
            return Form.get('sx-attachment-folder', []);
        }

    },

    data()
    {
        return {
            folders: Data.get('attachments-tree.data', [])
        };
    },

    mounted()
    {
        this.Event.bind('attachments-tree', () => {
            this.folders = Data.get('attachments-tree.data', []);
        });
    },

    ready()
    {
        let parent_id = Arr.last(this.SxIndexController.veCascade);

        this.NDataform.setOverride({
            type: 'folder', parent_id
        });
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(this.formConfig, (config) => {
                        return (<NConfig modelValue={this.value} config={config} scope={this} />);
                    })
                }
            </NForm>
        )
    }

}
