import DefaultIndex from "../../../default/src/index/index.js"
import { Arr, Obj, Num, Any, Event } from "@kizmann/pico-js";
import { Config, Ajax } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxAttachmentIndex',

    extends: DefaultIndex,

    urls: {
        index: 'attachments.index',
        edit: 'attachments.edit',
        create: 'attachments.create'
    },

    model: 'attachment',

    data()
    {
        return {
            veFolder: null, veCascade: []
        };
    },

    mounted()
    {
        Event.bind('attachments-delete:query', (query) => {

            if ( !Any.isArray(query.ids) ) {
                query.ids = [query.ids];
            }

            // if ( this.Arr.has(query.ids, this.veFolder) ) {
            //     this.setBaseFolder();
            // }
        });

        this.veCascade = Obj.get(this.$refs.tree, 'current.cascade', []);
    },

    watch: {

        'veFolder': function () {
            Any.delay(() => Event.fire('attachments:changed', this.veFolder));
        }

    },

    methods: {

        setBaseFolder()
        {
            this.$refs.tree.setCurrent('11111111-1111-1111-1111-111111111111');
        },

        setFolder(props)
        {
            this.veFolder = props.value.id;

            Event.fire('attachments/cascade', {
                ids: this.veCascade = props.value.cascade
            });
        },

        allowDrag(source)
        {
            if ( window.ADMINMODE ) {
                return true;
            }

            return source.value.depth > 1;
        },

        allowDrop(source, target, strategy)
        {
            if ( window.ADMINMODE ) {
                return true;
            }

            if ( ! target ) {
                return false;
            }

            if ( source.item.type !== 'folder' ) {
                return target.value.depth >= 1 && strategy === 'inner';
            }

            return target.value.depth >= 1 && strategy === 'inner' ||
                target.value.depth > 1;
        },

    },

    renderTree()
    {
        let updateEvents = [
            'attachments-edit',
            'attachments-move',
            'attachments-delete',
        ];

        let props = {
            indexQuery: 'attachments-tree',
            deleteQuery: 'attachments-delete',
            moveQuery: 'attachments-move',
            group: ['attachments'],
            allowGroups: ['attachments'],
            updateEvents: updateEvents,
            allowDrag: this.allowDrag,
            allowDrop: this.allowDrop,
            insertNode: false
        };

        props.use = ({ item, value }) => {
            return (<SxAttachmentTreeMarkup item={item} value={value} />);
        };

        Obj.assign(props, {
            'onRowClick': this.setFolder,
        });

        let slots = {};

        slots.custom = () => {
            return (
                <NButton icon="fa fa-folder" disabled={this.veCascade.length < 2} square={true} onClick={() => this.showEditModal('folderForm')} />
            )
        };

        return (
            <SxDatatree ref="tree" {...props} v-slots={slots} />
        );
    },

    renderTable()
    {
        let updateEvents = [
            'locale/change',
            'upload:done',
            'attachments:changed',
            'attachments-edit',
            'attachments-move'
        ];

        let props = {
            indexQuery: 'attachments-index',
            deleteQuery: 'attachments-delete',
            group: ['attachments'],
            updateEvents: updateEvents,
            sortProp: 'modified',
            sortDir: 'desc',
            itemHeight: 140,
            allowDrag: true,
            showNew: false,
            removeNode: false,
            override: { parent_id: this.veFolder }
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        let slots = {};

        slots.video = () => (
            <NButton icon="fa fa-camcorder" disabled={this.veCascade.length < 2} onClick={() => this.showEditModal('videoForm')}>
                {this.trans('Videos')}
            </NButton>
        );

        slots.file = () => (
            <NButton icon="fa fa-cloud" disabled={this.veCascade.length < 2} onClick={() => Event.fire('upload/open')}>
                {this.trans('Hochladen')}
            </NButton>
        );
        props.layout = [
            'search', 'new', 'delete', 'video', 'file', 'spacer', 'reset', 'refresh'
        ];

        return (
            <NDatatable {...props} v-slots={slots}>
                <NTableColumn {
                                  ...{
                                      type: 'image',
                                      prop: 'cropurl',
                                      label: this.trans('Vorschau'),
                                      previewProp: 'preview',
                                      sort: false,
                                      filter: false,
                                      fixedWidth: 140
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'title',
                                      label: this.trans('Titel'),
                                      sort: true,
                                      filter: true,
                                      fluid: true,
                                  }
                              }>
                    {({ item }) => Vue.h(Vue.resolveComponent('SxAttachmentTableMarkup'), { item })}
                </NTableColumn>
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'type',
                                      label: this.trans('Typ'),
                                      sort: true,
                                      filter: true
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'string',
                                      prop: 'copyright',
                                      label: this.trans('Copyright'),
                                      sort: true,
                                      filter: true,
                                      visible: false,
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'datetime',
                                      prop: 'modified',
                                      label: this.trans('Bearbeitet am'),
                                      sort: true,
                                      filter: true,
                                      fluid: true,
                                      visible: false
                                  }
                              } />
                <NTableColumn {
                                  ...{
                                      type: 'datetime',
                                      prop: 'created',
                                      label: this.trans('Erstellt am'),
                                      sort: true,
                                      filter: true,
                                      visible: false
                                  }
                              } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        if ( this.$slots.default ) {
            return this.$slots.default();
        }

        return (
            <NInfo ref="info">
                <NInfoColumn {
                                 ...{
                                     type: 'image',
                                     prop: 'cropurl'
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'name',
                                     label: this.trans('Name'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'title',
                                     label: this.trans('Titel'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'basename',
                                     label: this.trans('Dateiname'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'copyright',
                                     label: this.trans('Copyright'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'preview',
                                     label: this.trans('Vorschau'),
                                 }
                             }>
                    {({ item }) => {

                        if ( Any.isEmpty(item.preview) ) {
                            return this.trans('Keine Vorschau vorhanden');
                        }

                        return (
                            <a href={item.preview} target="_blank">{this.trans('Vorschau anzeigen')}</a>
                        );
                    }}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'originalurl',
                                     label: this.trans('Download'),
                                 }
                             }>
                    {({ item }) => <a href={item.originalurl} target="_blank">{this.trans('Original anzeigen')}</a>}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'articles',
                                     label: this.trans('Verwendet in'),
                                 }
                             }>
                    {({ item }) => {

                        let articles = Obj.get(item, 'articles');

                        articles = this.Arr.filter(articles, (article, index) => {
                            return this.Arr.findIndex(articles, { id: article.id }) === this.Num.int(index);
                        });

                        let result = this.Arr.each(articles, (article) => {
                            return [<router-link to={{ name: 'articles.edit', params: article }}>{article.title}</router-link>, <br />];
                        });

                        if ( !result.length ) {
                            result = this.trans('Keine Verknüpfung vorhanden');
                        }

                        return result;
                    }}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'width',
                                     label: this.trans('Dimensionen'),
                                 }
                             }>
                    {({ item }) => {

                        if ( !item.width || !item.height ) {
                            return this.trans('Keine Daten verfügbar');
                        }

                        return `${item.width}px, ${item.height}px`;
                    }}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'metadata.ColorSpace',
                                     label: this.trans('Farbraum'),
                                 }
                             }>
                    {({ item }) => {

                        let colorspace = Obj.get(item,
                            'metadata.ColorSpace', -1);

                        if ( colorspace > 0 ) {
                            this.trans('CMYK');
                        }

                        if ( colorspace === 1 ) {
                            return this.trans('sRGB');
                        }

                        if ( colorspace === 65535 ) {
                            return this.trans('AdobeRGB');
                        }

                        return this.trans('Unbekannt');
                    }}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'string',
                                     prop: 'metadata.FileSize',
                                     label: this.trans('Dateigröße'),
                                 }
                             }>
                    {({ item }) => {

                        let filesize = Obj.get(item,
                            'metadata.FileSize', 0);

                        return this.Str.filesize(filesize);
                    }}
                </NInfoColumn>
                <NInfoColumn {
                                 ...{
                                     type: 'datetime',
                                     prop: 'modified',
                                     label: this.trans('Bearbeitet am'),
                                 }
                             } />
                <NInfoColumn {
                                 ...{
                                     type: 'datetime',
                                     prop: 'created',
                                     label: this.trans('Erstellt am'),
                                 }
                             } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxAttachmentEdit',
            width: '1920px',
            height: '100%',
            localized: true,
            showQuery: 'attachments-show',
            editQuery: 'attachments-edit',
            newTitle: this.trans('Datei erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    renderFolderForm()
    {
        let props = {
            use: 'SxAttachmentFolder',
            width: '640px',
            height: 'auto',
            localized: false,
            type: 'default',
            showQuery: 'attachments-show',
            editQuery: 'attachments-edit',
            newTitle: this.trans('Ordner erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            layout: ['spacer', 'close', 'save'],
            onNew: () => this.showEditModal('folderForm'),
            onClose: () => this.hideEditModal('folderForm'),
        };

        return (
            <NDataform ref="folderForm" {...props} />
        );
    },

    renderVideoForm()
    {
        let props = {
            use: 'SxAttachmentVideo',
            width: '768px',
            height: 'auto',
            localized: false,
            type: 'default',
            showQuery: 'attachments-show',
            editQuery: 'attachments-edit',
            newTitle: this.trans('Video erstellen'),
            editTitle: this.trans('{title} bearbeiten'),
            layout: ['spacer', 'close', 'save'],
            onNew: () => this.showEditModal('videoForm'),
            onClose: () => this.hideEditModal('videoForm'),
        };

        return (
            <NDataform ref="videoForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderFolderForm')(),
            this.ctor('renderVideoForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain forceShowInfo={this.forceShowInfo} {...props} v-slots={slots} />
        );
    }

});
