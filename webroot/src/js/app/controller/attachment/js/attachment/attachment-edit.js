import DefaultEdit from "../../../default/src/edit/edit";
import { Arr, Obj, Any } from "@kizmann/pico-js";
import { Form, Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxAttachmentEdit',

    extends: DefaultEdit,

    inject: {

        NDataform: {
            default: undefined
        }

    },

    computed: {

        version()
        {
            return Obj.get(this.value, ['versions', this.selected]);
        },

        instruction()
        {
            return Obj.get(this.value, ['instructions', this.selected]);
        },

        hasImage()
        {
            return ! Obj.empty(this.value, 'cropurl');
        }

    },

    data()
    {
        return {
            file: null, selected: 'xl'
        };
    },

    methods: {

        replaceFile()
        {
            let query = {
                id: this.value.id, file: this.file
            };

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'attachments-replace'),
                onDone: () => this.NDataform.setLoad(false, 'attachments-replace'),
            };

            Ajax.call('attachments-replace', false, query, options);
        },

        whitespaceImage()
        {
            let query = {
                id: this.value.id
            };

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'attachments-whitespace'),
                onDone: () => this.NDataform.setLoad(false, 'attachments-whitespace'),
            };

            Ajax.call('attachments-whitespace', false, query, options)
                .then(this.NDataform.queryDone);
        },

        focusImage(data)
        {
            let query = {
                id: this.value.id, focus: data
            };

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'attachments-focus'),
                onDone: () => this.NDataform.setLoad(false, 'attachments-focus'),
            };

            Ajax.call('attachments-focus', false, query, options)
                .then(this.NDataform.queryDone);
        },

        cropImage(data)
        {
            let query = {
                id: this.value.id, versions: { [this.selected]: data }
            };

            let options = {
                onLoad: () => this.NDataform.setLoad(true, 'attachments-crop'),
                onDone: () => this.NDataform.setLoad(false, 'attachments-crop'),
            };

            Ajax.call('attachments-crop', false, query, options)
                .then(this.NDataform.queryDone);
        }

    },

    renderReplace()
    {
        if ( Any.isEmpty(this.value.basename) ) {
            return null;
        }

        return (
            <NTabsItem relative={true} sort={20} label={this.trans('Datei ersetzen')} name="replace" icon="fa fa-copy">
                <NFormItem label={this.trans('Datei ersetzen')} prop="file">
                    <NFile vModel={this.value.file}></NFile>
                </NFormItem>
            </NTabsItem>
        );
    },

    renderFocuspoint()
    {
        if ( this.value.type !== 'image' ) {
            return null;
        }

        return (
            <NTabsItem relative={true} sort={100} label={this.trans('Fokuspunkt')} name="focus" icon="fa fa-crosshairs">
                <SxFocuspoint class="absolute" instructions={this.value.instructions} vModel={this.value.cropurl} onApply={this.focusImage} />
            </NTabsItem>
        );
    },

    renderCropper()
    {
        if ( this.value.type !== 'image' ) {
            return null;
        }

        let slots = {};

        slots.custom = (
            <NSelect slot="custom" vModel={this.selected} style="width: 180px;">
                {
                    Arr.each(Obj.get(this.value, 'versions', []), (version, key) => {
                        return (
                            <NSelectOption label={version.info.name} value={key} />
                        );
                    })
                }
            </NSelect>
        );

        return (
            <NTabsItem relative={true} sort={200} keepAlive={false} label={this.trans('Bildausschnitt')} name="crop" icon="fa fa-crop">
                <SxCropper class="absolute" key={this.selected} width={this.value.width} height={this.value.height} version={this.version} instruction={this.instruction} vModel={this.value.cropurl} onApply={this.cropImage} v-slots={slots} />
            </NTabsItem>
        );
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-attachment-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
                { this.ctor('renderReplace')() }
                { this.ctor('renderFocuspoint')() }
                { this.ctor('renderCropper')() }
            </NForm>
        )
    }

}
