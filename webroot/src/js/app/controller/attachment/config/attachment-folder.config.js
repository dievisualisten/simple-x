sx.Form.set('sx-attachment-folder', [
    {

        // <NTabsItem:0>
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Ordner'),
                icon: 'fa fa-folder',
                relative: true,
            },
            content: {

                // <NTabsForm:0>
                'NFormItem:0': {
                    vIf: function (value) {
                        return pi.Obj.get(value, '_new', false);
                    },
                    $props: {
                        label: pi.Locale.trans('Zielordner')
                    },
                    content: {

                        // <NInput:0>
                        'NCascader:0': {
                            $props: {
                                options: '$$scope.folders',
                                modelValue: '$$scope.SxIndexController.veCascade',
                                disabled: true,
                                labelProp: 'name',
                                valueProp: 'id'
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:0>

                // <NTabsForm:0>
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {

                        // <NInput:0>
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:0>

            }
        }
        // </ NTabsItem:0>

    }

]);
