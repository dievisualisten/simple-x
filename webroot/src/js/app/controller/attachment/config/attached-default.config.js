sx.Form.set('sx-attached-default', [

    {
        'NFormItem:0': {
            $props: {
                label: pi.Locale.trans('Titel'),
                prop: 'attached.title'
            },
            content: {

                // <NInput:0>
                'NInput:0': {
                    model: {
                        path: 'attached.title'
                    }
                }
                // </ NInput:0>

            }
        }
    },
    {
        'NFormItem:0': {
            $props: {
                label: pi.Locale.trans('Alternativtext'),
                prop: 'attached.alternative'
            },
            content: {

                // <NInput:0>
                'NInput:0': {
                    model: {
                        path: 'attached.alternative'
                    }
                }
                // </ NInput:0>

            }
        }
    },
    {
        'NFormItem:0': {
            $props: {
                label: pi.Locale.trans('Beschreibung'),
                prop: 'attached.description'
            },
            content: {

                // <NInput:0>
                'NTextarea:0': {
                    model: {
                        path: 'attached.description'
                    }
                }
                // </ NInput:0>

            }
        }
    }

]);
