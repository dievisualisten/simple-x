sx.Form.set('sx-attachment-video', [
    {

        // <NTabsItem:0>
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Video'),
                icon: 'fa fa-film',
                relative: true,
            },
            content: {

                // <NTabsForm:1>
                'NFormItem:0': {
                    vIf: function () {
                        return ! pi.Any.isEmpty(this.value.videourl);
                    },
                    $props: {
                        label: pi.Locale.trans('Video')
                    },
                    content: {

                        // <div:0>
                        'NPreviewVideo:0': {
                            $props: {
                                src: '$$value.videourl'
                            }
                        }
                        // </ div:0>

                    }
                },
                // </ NFormItem:1>

                // <NTabsForm:0>
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Zielordner')
                    },
                    content: {

                        // <NInput:0>
                        'NCascader:0': {
                            $props: {
                                options: '$$scope.folders',
                                modelValue: '$$scope.SxIndexController.veCascade',
                                disabled: true,
                                labelProp: 'name',
                                valueProp: 'id'
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:0>

                // <NTabsForm:0>
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Youtube oder Vimeo URL'),
                        prop: 'videourl'
                    },
                    content: {

                        // <NInput:0>
                        'NInput:0': {
                            model: {
                                path: 'videourl'
                            }
                        }
                        // </ NInput:0>

                    }
                },
                // </ NFormItem:0>

            }
        }
        // </ NTabsItem:0>

    }

]);
