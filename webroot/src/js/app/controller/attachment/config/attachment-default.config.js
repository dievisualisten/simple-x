sx.Form.set('sx-attachment-default', [
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Datei'),
                icon: 'fa fa-file',
                preload: true
            },
            content: {

                'NFormGroup': {
                    $props: {
                        label: pi.Locale.trans('Informationen')
                    },
                    content: {

                        // <div:0>
                        'div:0': {
                            class: 'grid grid--row grid--wrap grid--20',
                            content: {

                                // <NFormItem:1>
                                'NFormItem:1': {
                                    class: function () {
                                        return {'col--1-1': true, 'n-i18n': this.i18n};
                                    },
                                    $props: {
                                        label: pi.Locale.trans('Titel'),
                                        prop: 'title'
                                    },
                                    content: {

                                        // <NInput:0>
                                        'NInput:0': {
                                            model: {
                                                path: 'title'
                                            },
                                            $props: {
                                                placeholder: '$$value.baselocale.title'
                                            }
                                        }
                                        // </ NInput:0>

                                    }
                                },
                                // </ NFormItem:1>

                                // <NFormItem:2>
                                'NFormItem:2': {
                                    class: function () {
                                        return {'col--1-1': true, 'n-i18n': this.i18n};
                                    },
                                    $props: {
                                        label: pi.Locale.trans('Alternative'),
                                        prop: 'alternative'
                                    },
                                    content: {

                                        // <NInput:0>
                                        'NInput:0': {
                                            model: {
                                                path: 'alternative'
                                            },
                                            $props: {
                                                placeholder: '$$value.baselocale.alternative'
                                            }
                                        }
                                        // </ NInput:0>

                                    }
                                },
                                // </ NFormItem:2>

                                // <NFormItem:3>
                                'NFormItem:3': {
                                    class: function () {
                                        return {'col--1-1': true, 'n-i18n': this.i18n};
                                    },
                                    $props: {
                                        label: pi.Locale.trans('Copyright'),
                                        prop: 'copyright'
                                    },
                                    content: {

                                        // <NInput:0>
                                        'NInput:0': {
                                            model: {
                                                path: 'copyright'
                                            },
                                            $props: {
                                                placeholder: '$$value.baselocale.copyright'
                                            }
                                        }
                                        // </ NInput:0>

                                    }
                                },
                                // </ NFormItem:3>

                                // <NFormItem:4>
                                'NFormItem:4': {
                                    class: function () {
                                        return {'col--1-1': true, 'n-i18n': this.i18n};
                                    },
                                    $props: {
                                        label: pi.Locale.trans('Beschreibung'),
                                        prop: 'description'
                                    },
                                    content: {

                                        // <NTextarea:0>
                                        'NTextarea:0': {
                                            model: {
                                                path: 'description'
                                            },
                                            $props: {
                                                placeholder: '$$value.baselocale.description'
                                            }
                                        }
                                        // </ NTextarea:0>

                                    }
                                },
                                // </ NFormItem:4>

                                // <NFormItem:0>
                                'NFormItem:0': {
                                    class: 'col--1-1',
                                    $props: {
                                        label: pi.Locale.trans('Dateiname'),
                                        prop: 'name'
                                    },
                                    content: {

                                        // <NInput:0>
                                        'NInput:0': {
                                            model: {
                                                path: 'name'
                                            },
                                        }
                                        // </ NInput:0>

                                    }
                                },
                                // </ NFormItem:0>

                                // <NFormItem:-1>
                                'NFormItem:-1': {
                                    class: 'col--1-1',
                                    $props: {
                                        label: pi.Locale.trans('Orginal Dateiname'),
                                        prop: 'name_original'
                                    },
                                    content: {

                                        // <NInput:0>
                                        'NInput:0': {
                                            $props: {
                                                disabled: true
                                            },
                                            model: {
                                                path: 'name_original'
                                            },
                                        }
                                        // </ NInput:0>

                                    }
                                },
                                // </ NFormItem:-1>

                            }
                        },
                        // </ div:0>

                    }
                }

            }
        },
        // </ NTabsItem:0>
    },
    {
        // <NTabsItem:0>
        'NTabsItem:0': {
            vIf: function () {
                return pi.Obj.get(this.value, 'type') !== 'image';
            },
            vAwait: '$$value.id',
            $props: {
                name: 'media',
                sort: 30,
                label: pi.Locale.trans('Vorschaubild'),
                icon: 'fa fa-image',
                relative: true,
                keep: true
            },
            content: {

                // <SxAttachmentIndex:0>
                'SxAttachmentIndex:0': {
                    vAwait: '$$value.thumbnail',
                    $props: {
                        forceShowInfo: true,
                        routing: false,
                        disableEdit: true,
                    },
                    content: {
                        'SxAttachmentChoose:0': {
                            model: {
                                path: 'thumbnail'
                            }
                        }
                    }
                }
                // </ SxAttachmentIndex:0>

            }
        },
        // </ NTabsItem:0>
    },
]);
