import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxFormularIndex',

    extends: DefaultIndex,

    urls: {
        index: 'formulars.index',
        edit: 'formulars.edit',
        create: 'formulars.create'
    },

    model: 'formular',

    renderTable()
    {
        let updateEvents = [
            'formulars-edit'
        ];

        let props = {
            indexQuery: 'formulars-index',
            copyQuery: 'formulars-copy',
            deleteQuery: 'formulars-delete',
            group: ['formulars'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'extended_data.fields',
                        label: this.trans('Felder'),
                        sort: false,
                        filter: false,
                        fluid: true
                    }
                }>
                    {
                        ({ item }) => {
                            return Obj.get(item, 'extended_data.fields.length', 0);
                        }
                    }
                </NTableColumn>
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'extended_data.fields',
                        label: this.trans('Felder')
                    }
                }>
                    {
                        ({ item }) => {
                            return Obj.get(item, 'extended_data.fields.length', 0);
                        }
                    }
                </NInfoColumn>
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'formularconfigs',
                        label: this.trans('Konfigurationen'),
                    }
                }>
                    {
                        ({ item }) => {
                            return Obj.get(item, 'formularconfigs.length', 0);
                        }
                    }
                </NInfoColumn>
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxFormularEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'formulars-show',
            editQuery: 'formulars-edit',
            newTitle: this.trans('E-Mail Konfiguration erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
