import { Obj, Any } from "@kizmann/pico-js";

export default {

    name: 'SxFormularFieldItems',

    props: {

        modelValue: {
            default()
            {
                return [];
            },
            type: [Array]
        }

    },

    provide()
    {
        return {
            SxFormularFieldItems: this
        };
    },

    data()
    {
        return {
            editNode: null, clearConfirm: false
        };
    },

    methods: {

        addField()
        {
            let items = Obj.clone(this.modelValue);

            let index = this.$refs.draglist
                .getCurrentIndex();

            if ( index === -1 ) {
                index = this.modelValue.length;
            }

            let item = {
                id: this.UUID(),
                label: '',
                type: '',
                inputName: '',
                validationRules: []
            };

            items.splice(index + 1, 0, item);

            Any.delay(() => {
                this.$refs.draglist.scrollToIndex(index + 1);
            }, 150);

            this.$emit('update:modelValue', items);
        },

        editField(props)
        {
            this.editNode = props;
        },

        applyField(props)
        {
            Obj.set({ items: this.modelValue }, props.value.route,
                props.item);

            this.editNode = null;
        },

        closeField()
        {
            this.editNode = null;
        },

    },

    renderForm()
    {
        if ( ! this.editNode ) {
            return null;
        }

        let props = {
            onClose: this.closeField,
            onApply: this.applyField,
        };

        return (
            <SxFormularFieldForm {...props} {...this.editNode} />
        );
    },

    render()
    {
        let props = {
            items: this.modelValue,
            group: ['formularfield'],
            allowGroups: ['formularfield'],
            useKeys: true,
            itemHeight: 50,
            threshold: 100,
        };

        props['onUpdate:items'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        props.renderNode = ({ item, value }) => {
            return (<SxFormularFieldItem item={item} value={value} />);
        };

        props.safezone = (height) => {
            return height * 0.51;
        };

        return (
            <div class="sx-formular-field-items">
                <div class="sx-formular-field-items__body">
                    <NDraglist ref="draglist" {...props} />
                </div>
                <div class="sx-formular-field-items__footer">
                    <NButton onClick={this.addField}>
                        { this.trans('Feld hinzufügen') }
                    </NButton>
                </div>
                { this.ctor('renderForm')() }
            </div>
        );
    }

}
