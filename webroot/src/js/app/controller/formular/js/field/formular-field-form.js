import { Arr, Obj, Str, Any } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxFormularFieldForm',

    inheritAttrs: false,

    inject: {

        SxFormularFieldItems : {
            default: null
        }

    },

    props: {

        item: {
            required: true
        },

        value: {
            required: true
        }

    },

    computed: {

        fields()
        {
            return Arr.filter(this.SxFormularFieldItems.modelValue, (item) => {
                return ! Any.isEmpty(item.inputName);
            });
        }

    },

    data()
    {
        return {
            tempItem: Obj.clone(this.item)
        };
    },

    beforeMount()
    {
        if ( ! Obj.has(this.item, 'validationRules') ) {
            this.tempItem.validationRules = [];
        }
    },

    methods: {

        closeModal()
        {
            this.$emit('close');
        },

        applyModal()
        {
            this.$emit('apply', {
                item: this.tempItem, value: this.value
            });
        },

        addRule()
        {
            this.tempItem.validationRules.push({
                validationMessage: '', validationParam1: '', validationParam2: '', validationRule: null
            });

            this.$nextTick(() => {
                this.$refs.modal.$refs.scrollbar.scrollTo(0, 99999);
            });
        },

        removeRule(index)
        {
            Arr.removeIndex(this.tempItem.validationRules, index);
        },

    },

    renderHeader()
    {
        return (
            <div slot="header">
                <h4 class="text-primary">{this.trans('Feld bearbeiten')}</h4>
            </div>
        );
    },

    renderFooter()
    {
        return (
            <div slot="footer" class="grid grid--row grid--right grid--10">
                <div class="col col--left">
                    <NButton type="primary" onClick={this.addRule}>
                        {this.trans('Validierungsregel hinzufügen')}
                    </NButton>
                </div>
                <div class="col">
                    <NButton type="secondary" onClick={this.closeModal}>
                        {this.trans('Abort')}
                    </NButton>
                </div>
                <div class="col">
                    <NButton type="success" onClick={this.applyModal}>
                        {this.trans('Apply')}
                    </NButton>
                </div>
            </div>
        );
    },

    renderRule(rule, index)
    {
        return (
            <div class="sx-formular-validation">

                <div class="grid grid--row grid--middle" style="margin-bottom: 10px;">
                    <div class="col--flex-1-1">
                        <h4>{this.choice('Regel :count', index + 1)}</h4>
                    </div>
                    <div class="col--flex-0-0">
                        <NButton type="danger" size="xs" square={true} icon="fa fa-times" onClick={() => this.removeRule(index)} />
                    </div>
                </div>

                <div class="grid grid--row grid--wrap grid--middle grid--20">
                    <div class="col--1-1 col--1-3@sm">
                        <NFormItem label={this.trans('Validierungsregel')}>
                            <SxSelectDomain vModel={this.tempItem.validationRules[index].validationRule} domain="validationrule" allowCreate={true}></SxSelectDomain>
                        </NFormItem>
                    </div>
                    <div class="col--1-1 col--1-3@sm">
                        <NFormItem label={this.trans('1. Parameter')}>
                            <NInput vModel={this.tempItem.validationRules[index].validationParam1}></NInput>
                        </NFormItem>
                    </div>
                    <div class="col--1-1 col--1-3@sm">
                        <NFormItem label={this.trans('2. Parameter')}>
                            <NInput vModel={this.tempItem.validationRules[index].validationParam2}></NInput>
                        </NFormItem>
                    </div>
                    <div class="col--1-1">
                        <NFormItem label={this.trans('Fehlermeldung')}>
                            <NInput vModel={this.tempItem.validationRules[index].validationMessage}></NInput>
                        </NFormItem>
                    </div>
                </div>

            </div>
        )
    },

    renderValidation()
    {
        if ( Any.isEmpty(this.tempItem.validationRules) ) {
            return null;
        }

        return (
            <NFormGroup label={this.trans('Validierung')} icon="fa fa-spell-check">
                { Arr.each(this.tempItem.validationRules, this.ctor('renderRule')) }
            </NFormGroup>
        );
    },

    render()
    {
        let slots = {
            header: this.ctor('renderHeader')(),
            footer: this.ctor('renderFooter')()
        };

        slots.default = (
            <NForm form={this.tempItem} relative={true}>
                {
                    Arr.each(Form.get('sx-formular-field', []), (config) => {
                        return (
                            <NConfig modelValue={this.tempItem} config={config} scope={this} />
                        )
                    })
                }
                { this.ctor('renderValidation')() }
            </NForm>
        );

        let props = {
            modelValue: true,
            width: '1280px',
            height: '100%',
            listen: false,
            update: false,
            onClose: this.closeModal,
        };

        return (
            <NModal ref="modal" {...props} v-slots={slots} />
        );
    }

}
