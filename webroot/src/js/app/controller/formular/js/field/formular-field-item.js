import { Obj, Any } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxFormularFieldItem',

    inject: {

        NDraggableItem: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        },

        SxFormularFieldItems: {
            default: undefined
        },

    },

    props: {

        item: {
            required: true
        },

        value: {
            required: true
        },

    },

    computed: {

        type()
        {
            return Config.domain('formfield', this.item.type, {
                key: 'undefined', value: this.trans('Kein Typ gewählt')
            });
        }

    },

    methods: {

        fieldEdit()
        {
            this.SxFormularFieldItems.editField({
                value: this.value, item: this.item
            });
        },

        fieldCopy()
        {
            this.NDraggableItem.copy();
        },

        fieldRemove()
        {
            this.NDraggableItem.remove();
        },

    },

    renderIcon()
    {
        let icon = 'question-circle';

        if ( this.type.key.match(/^h[1-6]$/) ) {
            icon = 'heading';
        }

        if ( this.type.key.match(/^hr$/) ) {
            icon = 'horizontal-rule';
        }

        if ( this.type.key.match(/^br$/) ) {
            icon = 'page-break';
        }

        if ( this.type.key.match(/^(number|text|textarea)$/) ) {
            icon = 'keyboard';
        }

        if ( this.type.key.match(/^password$/) ) {
            icon = 'key';
        }

        if ( this.type.key.match(/^(select|multiselect|selectsuggest)$/) ) {
            icon = 'ballot-check';
        }

        if ( this.type.key.match(/^element$/) ) {
            icon = 'box';
        }

        if ( this.type.key.match(/^(containeropen|containerclose)$/) ) {
            icon = 'code';
        }

        if ( this.type.key.match(/^checkbox$/) ) {
            icon = 'check-square';
        }

        if ( this.type.key.match(/^radio$/) ) {
            icon = 'check-circle';
        }

        if ( this.type.key.match(/^confirm$/) ) {
            icon = 'window';
        }

        if ( this.type.key.match(/^(date|time|datetime)$/) ) {
            icon = 'clock';
        }

        if ( this.type.key.match(/^(paragraph|textreplacement)$/) ) {
            icon = 'text';
        }

        return (
            <div class="sx-formular-field-item__icon">
                <span class={['fa', 'fa-' + icon]}></span>
            </div>
        );
    },

    renderLabel()
    {
        let fieldLabel = this.trans('Kein Label angegeben');

        if ( ! Any.isEmpty(this.item.label) ) {
            fieldLabel = this.item.label;
        }

        return (
            <div class="sx-formular-field-item__title">
                <span>{fieldLabel}</span> <span class="text-muted">{this.type.value}</span>
            </div>
        );
    },

    renderName()
    {
        if ( ! this.item.inputName ) {
            return null;
        }

        return (
            <div class="sx-formular-field-item__name">
                <span>{this.item.inputName}</span>
            </div>
        );
    },

    renderAction()
    {
        return (
            <div class="sx-formular-field-item__action toolbar">
                <NButton type="primary" size="xs" square={true} icon="fa fa-pen" onClick={this.fieldEdit} />
                <NButton type="warning" size="xs" square={true} icon="fa fa-copy" onClick={this.fieldCopy} />
                <NButton type="danger" size="xs" square={true} icon="fa fa-trash" onClick={this.fieldRemove} />
            </div>
        );
    },

    renderForm()
    {
        return (
            <SxFormularFieldForm></SxFormularFieldForm>
        );
    },

    render()
    {
        let classList = [
            'sx-formular-field-item',
        ];

        if ( Obj.get(this.item, 'notBlank', false) ) {
            classList.push('sx-formular-field-item--required');
        }

        return (
            <div class={classList} onDblclick={this.fieldEdit}>
                { this.ctor('renderIcon')() }
                { this.ctor('renderLabel')() }
                { this.ctor('renderName')() }
                { this.ctor('renderAction')() }
            </div>
        );
    }

}
