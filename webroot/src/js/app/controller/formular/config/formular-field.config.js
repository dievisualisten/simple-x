sx.Form.set('sx-formular-field', [

    /* Konfiguration start */
    {
        'NFormGroup:0': {
            $props: {
                label: pi.Locale.trans('Feld'),
            },
            content: {
                'div:0': {
                    class: 'grid grid--wrap grid--20',
                    content: {
                        'NFormItem:0': {
                            class: 'col--1-2',
                            $props: {
                                label: pi.Locale.trans('Typ'),
                                prop: 'type'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'type', fallback: null
                                    },
                                    $props: {
                                        domain: 'formfield'
                                    }
                                }
                            }
                        },
                        'NFormItem:4': {
                            class: 'col--1-2',
                            $props: {
                                label: pi.Locale.trans('Name / ID'),
                                prop: 'inputName'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'inputName', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:1': {
                            class: 'col--1-1',
                            $props: {
                                prop: 'hideLabel'
                            },
                            content: {
                                'NSwitch:0': {
                                    model: {
                                        path: 'hideLabel', fallback: false
                                    },
                                    content: [
                                        pi.Locale.trans('Label verbergen')
                                    ]
                                }
                            }
                        },
                        'NFormItem:2': {
                            class: 'col--1-1',
                            $props: {
                                label: pi.Locale.trans('Label'),
                                prop: 'label'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'label', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:3': {
                            class: 'col--1-2',
                            $props: {
                                label: pi.Locale.trans('Platzhalter'),
                                prop: 'placeholder'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'placeholder', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:5': {
                            class: 'col--1-2',
                            $props: {
                                label: pi.Locale.trans('Standardwert'),
                                prop: 'defaultValue'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'defaultValue', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:6': {
                            class: 'col--1-1',
                            $props: {
                                label: pi.Locale.trans('Optionen (für Select, Radio usw.)'),
                                prop: 'options'
                            },
                            content: {
                                'NTextarea:0': {
                                    vIf: function (value) {
                                        return ! pi.Arr.has(['select', 'checkbox', 'radio'], value.type);
                                    },
                                    model: {
                                        path: 'options', fallback: ''
                                    },
                                    $props: {
                                        autoRows: true,
                                        minRows: 1
                                    }
                                },
                                'SxSelectDomaintype:0': {
                                    vIf: function (value) {
                                        return pi.Arr.has(['select', 'checkbox', 'radio'], value.type);
                                    },
                                    model: {
                                        path: 'options', fallback: ''
                                    },
                                    $props: {
                                        allowCreate: true
                                    }
                                }
                            }
                        },
                        'NFormItem:7': {
                            class: 'col--1-1',
                            $props: {
                                label: pi.Locale.trans('Tooltip'),
                                prop: 'tooltip'
                            },
                            content: {
                                'NTextarea:0': {
                                    model: {
                                        path: 'tooltip', fallback: ''
                                    },
                                    $props: {
                                        autoRows: true,
                                        minRows: 1
                                    }
                                }
                            }
                        },
                        'NFormItem:8': {
                            class: 'col--1-1',
                            $props: {
                                label: pi.Locale.trans('Info'),
                                prop: 'infotext',
                                tooltip: pi.Locale.trans('Info wird im Frontend nicht angezeigt')
                            },
                            content: {
                                'NTextarea:0': {
                                    model: {
                                        path: 'infotext', fallback: ''
                                    },
                                    $props: {
                                        autoRows: true,
                                        minRows: 1
                                    }
                                }
                            }
                        },
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NFormGroup:0': {
            $props: {
                label: pi.Locale.trans('Sichtbarkeit'),
                icon: 'fa fa-eye'
            },
            content: {
                'div:0': {
                    class: 'grid grid--wrap grid--20',
                    content: {
                        'NFormItem:0': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('Label'),
                                prop: 'showOnFieldName'
                            },
                            content: {
                                'NSelect:0': {
                                    model: {
                                        path: 'showOnFieldName', fallback: ''
                                    },
                                    $props: {
                                        clearable: true,
                                        options: '$$scope.fields',
                                        optionsValue: '$value.inputName',
                                        optionsLabel: '$value.label'
                                    }
                                }
                            }
                        },
                        'NFormItem:1': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('... durch diesen Vergleich ...'),
                                prop: 'showOnFieldComparator'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'showOnFieldComparator', fallback: ''
                                    },
                                    $props: {
                                        domain: 'operator'
                                    }
                                }
                            }
                        },
                        'NFormItem:2': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('... den folgenden Wert hat'),
                                prop: 'showOnFieldValue'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'showOnFieldValue', fallback: ''
                                    }
                                }
                            }
                        },
                    }
                }

            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NFormGroup:0': {
            $props: {
                label: pi.Locale.trans('Pflichtfeld'),
                icon: 'fa fa-exclamation'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        prop: 'notBlank'
                    },
                    content: {
                        'NSwitch:0': {
                            model: {
                                path: 'notBlank', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Dieses Feld muss ausgefüllt werden')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Fehlermeldung, wenn das Feld leer ist'),
                        prop: 'notBlankMessage'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'notBlankMessage', fallback: ''
                            }
                        }
                    }
                },
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NFormGroup:0': {
            $props: {
                label: pi.Locale.trans('Layout'),
                icon: 'fa fa-border-outer'
            },
            content: {
                'div:0': {
                    class: 'grid grid--wrap grid--20',
                    content: {
                        'NFormItem:0': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('CSS Klassen'),
                                prop: 'cssClass'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'cssClass', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:1': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('Prepend'),
                                prop: 'prepend'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'prepend', fallback: ''
                                    }
                                }
                            }
                        },
                        'NFormItem:2': {
                            class: 'col--4-12',
                            $props: {
                                label: pi.Locale.trans('Append'),
                                prop: 'append'
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'append', fallback: ''
                                    }
                                }
                            }
                        },
                    }
                },
                'div:1': {
                    class: 'grid grid--wrap grid--20',
                    content: {
                        'NFormItem:0': {
                            class: 'col--3-12',
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (xs)'),
                                prop: 'colXs'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'colXs', fallback: ''
                                    },
                                    $props: {
                                        clearable: true,
                                        domain: 'column'
                                    }
                                }
                            }
                        },
                        'NFormItem:1': {
                            class: 'col--3-12',
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (sm)'),
                                prop: 'colSm'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'colSm', fallback: ''
                                    },
                                    $props: {
                                        clearable: true,
                                        domain: 'column'
                                    }
                                }
                            }
                        },
                        'NFormItem:2': {
                            class: 'col--3-12',
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (md)'),
                                prop: 'colMd'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'colMd', fallback: ''
                                    },
                                    $props: {
                                        clearable: true,
                                        domain: 'column'
                                    }
                                }
                            }
                        },
                        'NFormItem:3': {
                            class: 'col--3-12',
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (lg)'),
                                prop: 'colLg'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'colLg', fallback: ''
                                    },
                                    $props: {
                                        clearable: true,
                                        domain: 'column'
                                    }
                                }
                            }
                        },
                    }
                }
            }
        }
    },
    /* Konfiguration end */


]);
