sx.Form.set('sx-formular-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Formular'),
                icon: 'fa fa-stream'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Komponente'),
                        prop: 'component'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'component'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Text auf Absendebutton'),
                        prop: 'btnname'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'btnname'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'fields',
                sort: 20,
                label: pi.Locale.trans('Felder'),
                icon: 'fa fa-list',
                relative: true
            },
            content: {
                'SxFormularFieldItems:0': {
                    class: 'absolute',
                    model: {
                        path: 'extended_data.fields', fallback: []
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'layout',
                sort: 30,
                label: pi.Locale.trans('Layout'),
                icon: 'fa fa-columns'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Labels'),
                        prop: 'hidelabels'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'hidelabels', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Labels verbergen')
                            ]
                        }
                    }
                },
                'div:2': {
                    attrs: {
                        class: 'grid grid--row grid--wrap grid--20'
                    },
                    content: {

                        'NFormItem:1': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Labelposition'),
                                prop: 'horizontal'
                            },
                            content: {
                                'NSelect:0': {
                                    model: {
                                        path: 'horizontal'
                                    },
                                    content: {
                                        'NSelectOption:0': {
                                            $props: {
                                                value: true,
                                                label: pi.Locale.trans('Horizontal (links)')
                                            }
                                        },
                                        'NSelectOption:1': {
                                            $props: {
                                                value: false,
                                                label: pi.Locale.trans('Vertikal (oben)')
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        'NFormItem:2': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Formular-Ausrichtung'),
                                prop: 'alignment'
                            },
                            content: {
                                'NSelect:0': {
                                    model: {
                                        path: 'alignment'
                                    },
                                    content: {
                                        'NSelectOption:0': {
                                            $props: {
                                                value: 'left',
                                                label: pi.Locale.trans('Linksbündig')
                                            }
                                        },
                                        'NSelectOption:1': {
                                            $props: {
                                                value: 'center',
                                                label: pi.Locale.trans('Zentriert')
                                            }
                                        },
                                        'NSelectOption:2': {
                                            $props: {
                                                value: 'right',
                                                label: pi.Locale.trans('Rechtsbündig')
                                            }
                                        },
                                    }
                                }
                            }
                        },
                    }
                },
                'div:3': {
                    attrs: {
                        class: 'grid grid--row grid--wrap grid--20'
                    },
                    content: {
                        'NFormItem:1': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@sm col--1-4@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (xs)'),
                                prop: 'extended_data.colXs'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'extended_data.colXs'
                                    },
                                    $props: {
                                        domain: 'column', clearable: true
                                    }
                                }
                            }
                        },
                        'NFormItem:2': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@sm col--1-4@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (sm)'),
                                prop: 'extended_data.colSm'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'extended_data.colSm'
                                    },
                                    $props: {
                                        domain: 'column', clearable: true
                                    }
                                }
                            }
                        },
                        'NFormItem:3': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@sm col--1-4@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (md)'),
                                prop: 'extended_data.colMd'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'extended_data.colMd'
                                    },
                                    $props: {
                                        domain: 'column', clearable: true
                                    }
                                }
                            }
                        },
                        'NFormItem:4': {
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@sm col--1-4@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (lg)'),
                                prop: 'extended_data.colLg'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'extended_data.colLg'
                                    },
                                    $props: {
                                        domain: 'column', clearable: true
                                    }
                                }
                            }
                        },
                        'NFormItem:5': {
                            vIf: false,
                            attrs: {
                                class: 'n-form-item col--1-1 col--1-2@sm col--1-4@md'
                            },
                            $props: {
                                label: pi.Locale.trans('Spaltenbreite (xl)'),
                                prop: 'extended_data.colXl'
                            },
                            content: {
                                'SxSelectDomain:0': {
                                    model: {
                                        path: 'extended_data.colXl'
                                    },
                                    $props: {
                                        domain: 'column', clearable: true
                                    }
                                }
                            }
                        },
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'options',
                sort: 40,
                label: pi.Locale.trans('Optionen'),
                icon: 'fa fa-cogs'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Captcha'),
                        prop: 'captcha'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'captcha', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Captcha verwenden')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('E-Mail'),
                        prop: 'send_email'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'send_email', fallback: false
                            },
                            content: [
                                pi.Locale.trans('E-Mail versenden')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'uploads',
                sort: 50,
                label: pi.Locale.trans('Uploads'),
                icon: 'fa fa-cloud'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Versenden'),
                        prop: 'send_attachments'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'send_attachments', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Uploads per E-Mail versenden')
                            ]
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Speichern'),
                        prop: 'save_attachments'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'save_attachments', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Uploads auf dem Server speichern')
                            ]
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Bildversionen'),
                        prop: 'generate_attachments'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'generate_attachments', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Bildversionen aus den Uploads generieren')
                            ]
                        }
                    }
                },
            }
        }
    },
    /* Konfiguration end */

]);
