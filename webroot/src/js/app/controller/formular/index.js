import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('formulars');

require("./config/formular-default.config");
require("./config/formular-field.config");

import SxFormularIndex from "./js/formular/formular-index";
App.component(SxFormularIndex.name, SxFormularIndex);

import SxFormularEdit from "./js/formular/formular-edit";
App.component(SxFormularEdit.name, SxFormularEdit);

import SxFormularFieldItems from "./js/field/formular-field-items";
App.component(SxFormularFieldItems.name, SxFormularFieldItems);

import SxFormularFieldItem from "./js/field/formular-field-item";
App.component(SxFormularFieldItem.name, SxFormularFieldItem);

import SxFormularFieldForm from "./js/field/formular-field-form";
App.component(SxFormularFieldForm.name, SxFormularFieldForm);
