import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.bind('acos-tree', function (ajax, query, options) {
    let route = Route.get('acos.tree', query, query);
    return ajax.get(route, options);
});

Ajax.rest('acos');

require('./config/aco-default.config');

import SxAcoIndex from "./js/aco/aco-index";
App.component(SxAcoIndex.name, SxAcoIndex);

import SxAcoEdit from "./js/aco/aco-edit";
App.component(SxAcoEdit.name, SxAcoEdit);

import SxAcoNodeMarkup from "./js/helpers/aco-node-markup";
App.component(SxAcoNodeMarkup.name, SxAcoNodeMarkup);
