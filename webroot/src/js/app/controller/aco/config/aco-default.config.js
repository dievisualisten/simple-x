sx.Form.set('sx-aco-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Access Control Object'),
                icon: 'fa fa-key'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Medien'),
                        prop: 'has_attachmentfolder'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'has_attachmentfolder', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Medienodner erstellen')
                            ]
                        }
                    }
                },
                'NFormItem:2': {
                    vIf: function (value) {
                        return !! value.has_attachmentfolder;
                    },
                    $props: {
                        label: pi.Locale.trans('Medienordner')
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'attachmentfolder'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Übergeordneter Eintrag'),
                        prop: 'parent_id'
                    },
                    content: {
                        'SxSelectAco:0': {
                            model: {
                                path: 'parent_id'
                            },
                            props: function(value) {
                                return { disabled: ! pi.Obj.get(value, '_new', false) };
                            },
                            content: [
                                pi.Locale.trans('Medienodner erstellen')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'company',
                sort: 20,
                label: pi.Locale.trans('Firma'),
                icon: 'fa fa-building'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Firmenname'),
                        prop: 'company.company'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'company.company'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Umsatzsteuer ID'),
                        prop: 'company.ustid'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'company.ustid'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Steuernummer'),
                        prop: 'company.taxid'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'company.taxid'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Handelsregisternummer'),
                        prop: 'company.regnr'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'company.regnr'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Registergericht'),
                        prop: 'company.court'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'company.court'
                            }
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Notiz'),
                        prop: 'company.note'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'company.note'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'address',
                sort: 50,
                label: pi.Locale.trans('Adresse'),
                icon: 'fa fa-mailbox'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Strasse'),
                        prop: 'address.street'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.street'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Hausnummer'),
                        prop: 'address.number'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.number'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Postfach'),
                        prop: 'address.box'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.box'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('PLZ'),
                        prop: 'address.zip'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.zip'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Land'),
                        prop: 'address.country'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                                path: 'address.country'
                            },
                            $props: {
                                clearable: true,
                                domain: 'country'
                            },
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Längengrad'),
                        prop: 'address.lat'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.lat'
                            }
                        }
                    }
                },
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Breitengrad'),
                        prop: 'address.lon'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'address.lon'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'communication',
                sort: 60,
                label: pi.Locale.trans('Kommunikation'),
                icon: 'fa fa-user-tag'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Telefon'),
                        prop: 'communication.phone'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.phone'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Fax'),
                        prop: 'communication.fax'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.fax'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Mobil'),
                        prop: 'communication.cell'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.cell'
                            }
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Adresse'),
                        prop: 'communication.email'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.email'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Website'),
                        prop: 'communication.website'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'communication.website'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
