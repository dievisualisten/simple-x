import DefaultEdit from "../../../default/src/edit/edit";
import { Arr, Obj, Data } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxAcoEdit',

    extends: DefaultEdit,

    inject: {

        NDataform: {
            default: undefined
        },

        SxIndexController: {
            default: undefined
        }

    },

    ready()
    {
        if ( Obj.get(this.value, '_new', false) ) {
            this.value.parent_id = this.SxIndexController.veAco;
        }

        console.log(this.value,  this.SxIndexController);
    },

    beforeDestroy()
    {
        Data.unset('attachments-tree');
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-aco-default', []), (config, index) => {
                        return (
                            <NConfig key={index} modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        );
    }

}
