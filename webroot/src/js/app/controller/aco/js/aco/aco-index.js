import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Data, Event, Any } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxAcoIndex',

    extends: DefaultIndex,

    urls: {
        index: 'acos.index',
        edit: 'acos.edit',
        create: 'acos.create'
    },

    model: 'aco',

    data()
    {
        return {
            veAco: null, veCascade: []
        };
    },

    mounted()
    {
        this.veCascade = Obj.get(this.$refs.tree, 'current.cascade', []);
    },

    watch: {

        'veAco': function () {
            Any.delay(() => Event.fire('acos:changed', this.veAco));
        }

    },

    methods: {

        setAco(props)
        {
            this.veAco = props.value.id;

            Event.fire('acos/cascade', {
                ids: this.veCascade = props.value.cascade
            });
        },

        allowDrag(source)
        {
            return source.value.depth > 0;
        },

        allowDrop(source, target, strategy)
        {
            return strategy === 'inner' || target.value.depth > 0;
        },

    },

    renderTree()
    {
        let updateEvents = [
            'acos-edit',
            'acos-delete'
        ];

        let props = {
            indexQuery: 'acos-tree',
            deleteQuery: 'acos-delete',
            moveQuery: 'acos-move',
            group: ['acos'],
            allowGroups:  ['acos'],
            updateEvents: updateEvents,
            allowMove: false,
            allowDrag: false,
            allowDrop: false,
        };

        props.use = ({ item }) => {
            return (<SxAcoNodeMarkup item={item} />);
        };

        Obj.assign(props, {
            'onRowClick': this.setAco,
        });

        let slots = {
            custom: []
        };

        slots.custom[0] = (
            <NButton icon="fa fa-plus" disabled={this.veCascade.length < 1} square={true} onClick={() => this.newItem()}/>
        );

        return (
            <SxDatatree ref="tree" {...props} v-slots={slots}/>
        );
    },

    renderTable()
    {
        let updateEvents = [
            'acos-edit',
            'acos-delete',
            'acos-move'
        ];

        let props = {
            indexQuery: 'acos-tree',
            deleteQuery: 'acos-delete',
            group: ['acos'],
            updateEvents: updateEvents,
            allowDrag: false,
            showNew: true,
            removeNode: false,
            renderExpand: true,
            renderFooter: false,
            deleteMessage: 'Alle dazugehörigen Medienordner sowie alle Bilder werden ebenfalls gelöscht. Möchten Sie wirklich fortfahren?'
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onRowClick': this.setAco,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: false,
                        filter: false,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        if ( this.$slots.default ) {
            return this.$slots.default();
        }

        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxAcoEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'acos-show',
            editQuery: 'acos-edit',
            newTitle: this.trans('Access Control Object erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            // tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            // this.ctor('renderDomainForm')(),
            // this.ctor('renderMenuForm')(),
            // this.ctor('renderAliasForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
