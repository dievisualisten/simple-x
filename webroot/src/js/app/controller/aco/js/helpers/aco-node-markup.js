export default {

    name: 'SxAcoNodeMarkup',

    inject: {

        SxIndexController: {
            default: undefined
        },

        NDatatree: {
            default: undefined
        }

    },

    props: {

        item: {
            required: true
        }

    },

    methods: {

        editItem()
        {
            this.SxIndexController.editItem(this.item.id);
        },

        deleteItem()
        {
            this.NDatatree.deleteItem(this.item.id);
        }

    },

    renderIcon()
    {
        return (
            <div class="sx-aco-node-markup__icon">
                <span class="fa fa-key"></span>
            </div>
        );
    },

    renderTitle()
    {
        return (
            <div class="sx-aco-node-markup__title">
                <span>{this.item.name}</span>
            </div>
        );
    },

    renderAction()
    {
        let confirmHtml = (
            <NConfirm type="warning" onConfirm={this.deleteItem}>
                { this.trans('Alle dazugehörigen Medienordner sowie alle Bilder werden ebenfalls gelöscht. Möchten Sie fortfahren?') }
            </NConfirm>
        );

        return (
            <div class="sx-aco-node-markup__action">
                <NButton size="mini" type="primary" icon="fa fa-pen" square={true} onClick={this.editItem} />
                <NButton size="mini" type="danger" icon="fa fa-trash" square={true} />
                { confirmHtml }
            </div>
        );
    },

    render()
    {
        return (
            <div class="sx-aco-node-markup" obDblclick={this.editItem}>
                { this.ctor('renderIcon')() }
                { this.ctor('renderTitle')() }
                { this.ctor('renderAction')() }
            </div>
        )
    }

}
