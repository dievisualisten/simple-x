import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('texts');

Ajax.bind('texts-import', function (ajax, query, options) {
    let route = Route.get('texts.import');
    return ajax.post(route, Ajax.form(query), options);
});

Ajax.bind('texts-generate', function (ajax, query, options) {
    let route = Route.get('texts.generate');
    return ajax.post(route, query, options);
});

require('./config/text-default.config');

import SxTextIndex from "./js/text/text-index";
App.component(SxTextIndex.name, SxTextIndex);

import SxTextEdit from "./js/text/text-edit";
App.component(SxTextEdit.name, SxTextEdit);
