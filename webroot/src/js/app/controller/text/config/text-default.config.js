sx.Form.set('sx-text-default', [

    /* Text start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Text'),
                icon: 'fa fa-language'
            },
            content: {
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Text / Identifier'),
                        prop: 'name'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        prop: 'is_html'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'is_html'
                            },
                            content: [
                                pi.Locale.trans('Mit Editor bearbeiten')
                            ]
                        }
                    }
                },
                'NFormItem:-1': {
                    vIf: false,
                    $props: {
                        label: pi.Locale.trans('Standard'),
                        prop: 'default_value'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'default_value'
                            },
                            $props: {
                                disabled: true, minRows: 1, autoRows: true
                            }
                        }
                    }
                },
                'NFormItem:-2': {
                    content: {
                        'NButton:0': {
                            on: {
                                click: function () {
                                    this.value.value = this.value.default_value;
                                }
                            },
                            content: [
                                pi.Locale.trans('Wert auf Standard zurücksetzen')
                            ]
                        }
                    }
                },
                'NFormItem:3': {
                    vIf: function (value) {
                        return !pi.Obj.get(value, 'is_html', false);
                    },
                    class: function () {
                        return {'n-i18n': this.i18n};
                    },
                    $props: {
                        label: pi.Locale.trans('Text / Übersetzung / Korrektur'),
                        prop: 'value'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'value'
                            },
                            attrs: {
                                style: 'height: 191px;'
                            },
                            $props: {
                                placeholder: '$$value.baselocale.value'
                            }
                        }
                    }
                },
                'NFormItem:4': {
                    vIf: function (value) {
                        return pi.Obj.get(value, 'is_html', false);
                    },
                    class: function () {
                        return {'n-i18n': this.i18n};
                    },
                    $props: {
                        label: pi.Locale.trans('Text / Übersetzung / Korrektur'),
                        prop: 'value'
                    },
                    content: {
                        'froala:0': {
                            model: {
                                path: 'value'
                            },
                            $props: {
                                config: '$$scope.froala'
                            },
                        }
                    }
                }
            }
        }
    },
    /* Text end */

]);
