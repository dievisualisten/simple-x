import DefaultIndex from "../../../default/src/index/index.js"
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxTextIndex',

    extends: DefaultIndex,

    urls: {
        index: 'texts.index',
        edit: 'texts.edit',
        create: 'texts.create'
    },

    model: 'text',

    methods: {

        queryGenerate()
        {
            let options = {
                onLoad: () => this.$refs.table.load = true,
                onDone: () => this.$refs.table.load = false
            };

            Ajax.call('texts-generate', false, {}, options)
                .then(() => this.$refs.table.queryIndex());
        }

    },

    renderTable()
    {
        let updateEvents = [
            'texts-edit', 'locale/change'
        ];

        let props = {
            indexQuery: 'texts-index',
            copyQuery: 'texts-copy',
            deleteQuery: 'texts-delete',
            exportUrl: this.Route.get('texts.export'),
            group: ['texts'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        let slots = {
            custom: () => {
                return (
                    <NButton icon="fa fa-magic" onClick={this.queryGenerate}>{ this.trans('Texte generieren') }</NButton>
                );
            }
        };

        return (
            <NDatatable ref="table" {...props} v-slots={slots}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Text / Identifier'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'value',
                        label: this.trans('Text / Übersetzung / Korrektur'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'source',
                        label: this.trans('Quelle'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'type',
                        label: this.trans('Typ'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'in_use',
                        label: this.trans('Verwendet?'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'is_html',
                        label: this.trans('HTML verwenden?'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Text / Identifier')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Text / Übersetzung / Korrektur')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'quelle',
                        label: this.trans('Quelle')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'type',
                        label: this.trans('Typ')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxTextEdit',
            width: '1280px',
            height: '100%',
            localized: true,
            showQuery: 'texts-show',
            editQuery: 'texts-edit',
            newTitle: this.trans('Text erstellen'),
            editTitle: this.trans('Text bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
