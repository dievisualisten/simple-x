export default {

    name: 'SxError',

    render()
    {
        let message = this.trans('Bitte versuchen Sie es erneut oder gehen Sie <a href="javascript:window.history.back()">zurück</a>.');

        return (
            <div class="sx-error">
                <div class="sx-error__text">
                    <h1>{ this.trans('Oops, Seite nicht gefunden') }</h1>
                    <p v-html={message}></p>
                </div>
            </div>
        );
    }
}
