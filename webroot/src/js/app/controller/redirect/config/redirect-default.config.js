sx.Form.set('sx-redirect-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Redirect'),
                icon: 'fa fa-external-link'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Quelle'),
                        tooltip: pi.Locale.trans('URL die der Nutzer aufruft'),
                        prop: 'from_url'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'from_url'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Ziel'),
                        tooltip: pi.Locale.trans('URL auf die Weitergeleitet wird'),
                        prop: 'target_url'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'target_url'
                            },
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Status'),
                        prop: 'active'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'active', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Weiterleitung ist aktiv')
                            ]
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Statuslog'),
                        prop: 'status'
                    },
                    content: {
                        'NTextarea:0': {
                            $props: {
                                disabled: true,
                                autoRows: true
                            },
                            model: {
                                path: 'status'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
