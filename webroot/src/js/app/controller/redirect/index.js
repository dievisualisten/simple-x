import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.rest('redirects');

Ajax.bind('redirects-import', function (ajax, query, options) {
    let route = Route.get('redirects.import');
    return ajax.post(route, Ajax.form(query), options);
});

require('./config/redirect-default.config');

import SxRedirectIndex from "./js/redirect/redirect-index";
App.component(SxRedirectIndex.name, SxRedirectIndex);

import SxRedirectEdit from "./js/redirect/redirect-edit";
App.component(SxRedirectEdit.name, SxRedirectEdit);
