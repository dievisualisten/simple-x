import DefaultIndex from "../../../default/src/index/index.js"
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Ajax, Config } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxCrontaskIndex',

    extends: DefaultIndex,

    urls: {
        index: 'crontasks.index',
        edit: 'crontasks.edit',
        create: 'crontasks.create'
    },

    model: 'crontask',

    data()
    {
        return {
            consoleLoad: false, consoleOutput: null
        };
    },

    methods: {

        runConsole()
        {
            this.consoleOutput = 'Loading...';

            let options = {
                onLoad: () => this.consoleLoad = true,
                onDone: () => this.consoleLoad = false
            };

            Ajax.call('crontasks-run', false, null, options)
                .then(this.consoleDone, () => null);

            this.$refs.console.openModal();
        },

        consoleDone(res)
        {
            this.consoleOutput = Obj.get(res, 'data.output');
        }

    },

    renderTable()
    {
        let updateEvents = [
            'crontasks-edit'
        ];

        let props = {
            indexQuery: 'crontasks-index',
            copyQuery: 'crontasks-copy',
            deleteQuery: 'crontasks-delete',
            group: ['crontasks'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        let slots = {};

        slots.custom = () => {
            return (
                <NButton icon="fa fa-terminal" onClick={this.runConsole}>{this.trans('Ausführen')}</NButton>
            );
        }

        return (
            <NDatatable ref="table" {...props} v-slots={slots}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Aufgabe'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'method',
                        label: this.trans('Methode'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'active',
                        label: this.trans('Aktiv'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'option',
                        prop: 'execute',
                        label: this.trans('Interval'),
                        sort: true,
                        filter: true,
                        options: Config.domains('interval'),
                        optionsLabel: '$value.value',
                        optionsValue: '$value.key'
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'status',
                        label: this.trans('Status'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'last_start',
                        label: this.trans('Gestartet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'last_executed',
                        label: this.trans('Beendet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'life_sign',
                        label: this.trans('Lebenszeichen'),
                        sort: true,
                        filter: true

                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'log',
                        label: this.trans('Log'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                        visible: false
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Aufgabe')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'method',
                        label: this.trans('Methode')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'active',
                        label: this.trans('Aktiv')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'option',
                        prop: 'execute',
                        label: this.trans('Interval'),
                        options: Config.domains('interval'),
                        optionsLabel: '$value.value',
                        optionsValue: '$value.key'
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'status',
                        label: this.trans('Status')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'last_start',
                        label: this.trans('Gestartet am')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'last_executed',
                        label: this.trans('Beendet am')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'life_sign',
                        label: this.trans('Lebenszeichen')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'log',
                        label: this.trans('Log')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxCrontaskEdit',
            width: '1920px',
            height: '100%',
            localized: false,
            showQuery: 'crontasks-show',
            editQuery: 'crontasks-edit',
            newTitle: this.trans('Hintergrundaufgabe erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    renderConsoleModal()
    {
        return (
            <NModal ref="console" type="preview" listen={false} title={this.trans('Statuslog')}>
                <pre>{ this.consoleOutput }</pre>
            </NModal>
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
            this.ctor('renderConsoleModal')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
