sx.Form.set('sx-crontask-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Hintergrundaufgaben'),
                icon: 'fa fa-stopwatch'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Name'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Methode'),
                        prop: 'method'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'method'
                            },
                            $props: {
                                disabled: function (value) {
                                    return ! pi.Obj.get(value, '_new', true);
                                }
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Aktiv'),
                        prop: 'active'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'active', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Hintergrundaufgabe ist aktiv')
                            ]
                        }
                    }
                },
                'NFormItem:3': {
                    $props: {
                        label: pi.Locale.trans('Auto-Neustart'),
                        prop: 'restartable'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'restartable', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Hintergrundaufgabe wird bei Fehlern neugestartet')
                            ]
                        }
                    }
                },
                'NFormItem:4': {
                    $props: {
                        label: pi.Locale.trans('Ausführungsintervall'),
                        prop: 'execute'
                    },
                    content: {
                        'SxSelectDomain:0': {
                            model: {
                               path: 'execute'
                            },
                            $props: {
                                domain: 'interval'
                            }
                        }
                    }
                },
                'NFormItem:5': {
                    $props: {
                        label: pi.Locale.trans('Parameter'),
                        prop: 'parameter'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'parameter'
                            }
                        }
                    }
                },
                'NFormItem:6': {
                    $props: {
                        label: pi.Locale.trans('Status'),
                        prop: 'status'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'status'
                            }
                        }
                    }
                },
                'NFormItem:7': {
                    $props: {
                        label: pi.Locale.trans('Log'),
                        prop: 'log'
                    },
                    content: {
                        'NTextarea:0': {
                            model: {
                                path: 'log'
                            },
                            $props: {
                                disabled: true
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
