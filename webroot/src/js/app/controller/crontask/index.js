import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.bind('crontasks-run', function (ajax, query, options) {
    let route = Route.get('crontasks.run');
    return ajax.get(route, options);
});

Ajax.rest('crontasks');

require("./config/crontask-default.config");

import CrontaskIndex from "./js/crontask/crontask-index";
App.component(CrontaskIndex.name, CrontaskIndex);

import CrontaskEdit from "./js/crontask/crontask-edit";
App.component(CrontaskEdit.name, CrontaskEdit);
