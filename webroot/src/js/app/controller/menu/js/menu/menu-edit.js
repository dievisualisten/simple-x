import DefaultEdit from "../../../default/src/edit/edit";

import { Arr } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxMenuEdit',

    extends: DefaultEdit,

    mounted()
    {
        this.NDataform.setOverride({
            type: 'menu'
        });
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-menu-default', []), (config) => {
                        return (
                            <NConfig modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        )
    }

}
