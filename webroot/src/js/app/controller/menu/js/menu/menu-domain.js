import DefaultEdit from "../../../default/src/edit/edit";

import { Obj, Arr } from "@kizmann/pico-js";
import { Form } from "@dievisualisten/simplex";

export default {

    name: 'SxMenuDomain',

    extends: DefaultEdit,

    mounted()
    {
        this.NDataform.setOverride({
            type: 'domain'
        });
    },

    ready()
    {
        if ( Obj.get(this.value, '_new', false) ) {
            Obj.set(this.value, 'aco_id', this.$root.aco || null);
        }
    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                {
                    Arr.each(Form.get('sx-menu-domain', []), (config) => {
                        return (
                            <NConfig modelValue={this.value} config={config} scope={this} />
                        )
                    })
                }
            </NForm>
        )
    }

}
