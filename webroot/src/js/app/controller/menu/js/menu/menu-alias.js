import DefaultEdit from "../../../default/src/edit/edit"
import { Arr, Obj } from "@kizmann/pico-js";
import { Config, Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxMenuAlias',

    extends: DefaultEdit,

    computed: {

        teasers()
        {
            let teasers = Config.get('resource.modules.menu.teaser');

            teasers = Arr.map(teasers, (item, key) => {
                return Obj.assign(item, { key: key });
            });

            teasers = Arr.filter(teasers, (item) => {
                return item.show === '1';
            });

            return Arr.sort(teasers, 'order');
        },

    },

    data()
    {
        return {
            menus: []
        };
    },

    mounted()
    {
        this.NDataform.setOverride({
            type: 'alias'
        });
    },

    ready()
    {
        if ( ! Obj.has(this.value, 'tabmenus') ) {
            Obj.set(this.value, 'tabmenus', []);
        }

        if ( ! Obj.has(this.value, 'tabparents') ) {
            Obj.set(this.value, 'tabparents', []);
        }

        this.queryMenuTree();
    },

    methods: {

        canSourceCheck(row)
        {
            let disabledTypes = [
                'domain', 'menu', 'alias'
            ];

            if ( this.value._new ) {
                return Arr.has(disabledTypes, row.type)
            }

            return true;
        },

        canMatrixCheck(row, item)
        {
            let key = [
                row.type, 'default', 'panels', 'menu', item.key, 'show'
            ];

            return Config.pagetypes(key, '0') === '0' &&
                row.type !== 'menu';
        }

    },

    render()
    {
        return (
            <NForm form={this.value} errors={this.errors}>
                <NTabsItem label={this.trans('Verlinkung')} icon="fa fa-sitemap" name="default" sort={10} relative={true}>
                    <div class="sx-menu-alias">
                        <div className="sx-menu-alias__left">
                            <NTable class="col--flex-1-1" expanded={this.value.tabmenus_expanded} items={this.menus} renderExpand={true} renderSelect={false} allowDrag={false} loadingDelay={20}>
                                <NTableColumn type="string" label={this.trans('Seiten')} fluid={true} useKeys={true}>
                                    {
                                        ({item}) => <SxMenuTreeNode item={item} />
                                    }
                                </NTableColumn>
                                <NTableColumn vModel={this.value.tabmenus} type={'matrix'} prop="teaser" align="center" fixedWidth={100} matrix={-1} label={this.trans('Auswahl')} allowUncheck={false} disabled={({ item }) => this.canSourceCheck(item)} />
                            </NTable>
                        </div>
                        <div className="sx-menu-alias__right">
                            <NTable class="col--flex-1-1" expanded={this.value.tabparents_expanded} items={this.menus} label={this.trans('Seiten verlinken unter ...')} renderExpand={true} renderSelect={false} allowDrag={false} loadingDelay={25} useKeys={true}>
                                <NTableColumn type="string" label={this.trans('Seiten')} fluid={true}>
                                    {
                                        ({item}) => <SxMenuTreeNode item={item} />
                                    }
                                </NTableColumn>
                                {
                                    Arr.each(this.teasers, (teaser) => {

                                        var tooltip = teaser.name;

                                        if ( teaser.tooltip !== '0' ) {
                                            tooltip = teaser.tooltip;
                                        }

                                        return (
                                            <NTableColumn vModel={this.value.tabparents} type="matrix" matrixProp="teaser" label={teaser.name} tooltip={tooltip} prop="teaser" align="center" fixedWidth={100} matrix={teaser.value} disabled={({ item }) => this.canMatrixCheck(item, teaser)} />
                                        );
                                    })
                                }

                            </NTable>
                        </div>

                    </div>
                </NTabsItem>
            </NForm>
        )
    }

}
