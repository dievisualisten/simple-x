import IconMixin from "../../../article/js/mixins/icon-mixin";
import { Arr, Num } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxMenuTreeNode',

    mixins: [
        IconMixin
    ],

    inject: {

        SxIndexController: {
            default: undefined
        },

        NDatatree: {
            default: undefined
        },

        NTable: {
            default: undefined
        }

    },

    props: {

        item: {
            required: true
        }

    },

    computed: {

        matrix()
        {
            return Num.matrix(this.item.teaser)
        },

        canPreview()
        {
            return ! Arr.has(['menu'], this.item.type) &&
                this.item.teaser / 1024 % 1 !== 0;
        }

    },

    data()
    {
        return { init: false };
    },

    mounted()
    {
        this.timeout = setTimeout(() =>
            this.init = true, 50);
    },

    beforeUnmount()
    {
        if ( ! this.init ) {
            clearTimeout(this.timeout);
        }
    },

    methods: {

        getTooltip()
        {
            let teasers = Config.get('resource.modules.menu.teaser');

            teasers = Arr.filter(teasers, (item) => {
                return Arr.has(this.matrix, Num.int(item.value));
            });

            return Arr.map(teasers, (item) => item.name).join(', ');
        },

        editItem()
        {
            if ( this.NTable ) {
                return;
            }

            if ( this.item.type === 'alias' ) {
                return this.SxIndexController.showEditModal('aliasForm', this.item.id);
            }

            if ( this.item.type === 'menu' ) {
                return this.SxIndexController.showEditModal('menuForm', this.item.id);
            }

            if ( this.item.type === 'domain' ) {
                return this.SxIndexController.showEditModal('domainForm', this.item.id);
            }

            if ( ! this.item.foreign_key ) {
                return;
            }

            return this.SxIndexController.editItem(this.item.foreign_key);
        },

        deleteItem()
        {
            this.NDatatree.deleteItem(this.item.id);
        },

        previewItem()
        {
            window.open(this.item.previewPath);
        }

    },

    renderIcon()
    {
        return (
            <div class="sx-menu-tree-node__icon">
                <span class={this.iconClass}></span>
            </div>
        );
    },

    renderTitle()
    {
        let slot = this.item.title;

        if ( this.init ) {
            slot = (<SxArticleNodeMarkup useMenus={false} item={this.item} />);
        }

        return (
            <div ref="title" class="sx-menu-tree-node__title">
                { slot }
            </div>
        );
    },

    renderTooltip()
    {
        if ( ! this.init ) {
            return null;
        }

        return (
            <NPopover trigger="hover" size="xs" type="tooltip" selector={this.$refs.title}>
                { this.getTooltip() }
            </NPopover>
        );
    },

    renderContext()
    {
        if ( ! this.init ) {
            return null;
        }

        if ( this.NTable || true ) {
            return null;
        }

        return (
            <NPopover trigger="context" width={170} selector={this.$refs.title}>
                <NPopoverGroup>
                    { this.tooltip }
                </NPopoverGroup>
                <NPopoverOption>
                    { this.trans('Add page inside') }
                </NPopoverOption>
                <NPopoverOption>
                    { this.trans('Add page below') }
                </NPopoverOption>
            </NPopover>
        );
    },

    renderAction()
    {
        if ( this.NTable ) {
            return null;
        }

        let previewHtml = (
            <NButton size="xs" type="info" icon="fa fa-eye" square={true} onClick={() => this.previewItem()} />
        );

        if ( ! this.canPreview ) {
            previewHtml = null;
        }

        return (
            <div class="sx-menu-tree-node__action toolbar">
                { previewHtml }
                <NButton type="primary" size="xs" icon="fa fa-pen" square={true} onClick={() => this.editItem()} />
                <NButton type="danger" size="xs" icon="fa fa-trash" square={true} onClick={() => this.deleteItem()} />
            </div>
        );
    },

    render()
    {
        let classList = [
            'sx-menu-tree-node'
        ];

        if ( this.item.teaser >= 1024 && this.item.teaser < 4096 ) {
            classList.push('sx-segment');
        }

        if ( Arr.has(this.matrix, 2) && this.item.type !== 'menu') {
            classList.push('sx-menulink');
        }

        return (
            <div class={classList} onDblclick={() => this.editItem()}>
                { this.ctor('renderIcon')() }
                { this.ctor('renderTitle')() }
                { this.ctor('renderTooltip')() }
                { this.ctor('renderAction')() }
            </div>
        )
    }

}
