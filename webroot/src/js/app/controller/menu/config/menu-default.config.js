sx.Form.set('sx-menu-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                relative: true,
                label: pi.Locale.trans('Menü'),
                icon: 'fa fa-sitemap'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Titel'),
                        prop: 'title'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'title'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
