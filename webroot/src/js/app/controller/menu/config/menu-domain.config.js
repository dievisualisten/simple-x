sx.Form.set('sx-menu-domain', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Domain'),
                icon: 'fa fa-globe'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Domain'),
                        prop: 'title'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'title'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Seitentitel'),
                        prop: 'extended_data.menu.pagetitle'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'extended_data.menu.pagetitle'
                            }
                        }
                    }
                },
                'NFormItem:2': {
                    $props: {
                        label: pi.Locale.trans('Theme'),
                        prop: 'extended_data_cfg.menu.theme'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'extended_data_cfg.menu.theme'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'api',
                sort: 20,
                label: pi.Locale.trans('API Schlüssel'),
                icon: 'fa fa-analytics'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Google Analytics'),
                        prop: 'extended_data_cfg.menu.google_analytics_id'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'extended_data_cfg.menu.google_analytics_id'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'language',
                sort: 30,
                label: pi.Locale.trans('Sprache'),
                icon: 'fa fa-language'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Sprache'),
                        prop: 'extended_data.menu.locale'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'extended_data.menu.locale'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        label: pi.Locale.trans('Land'),
                        prop: 'extended_data.menu.country'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'extended_data.menu.country'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'email',
                sort: 40,
                label: pi.Locale.trans('E-Mail'),
                icon: 'fa fa-envelope'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('E-Mail-Konfiguration'),
                        prop: 'email_id',
                        tooltip: pi.Locale.trans('Bitte wählen Sie eine E-Mail Konfiguration aus')
                    },
                    content: {
                        'SxSelectEmail:0': {
                            model: {
                                path: 'email_id'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'aco',
                sort: 50,
                label: pi.Locale.trans('Rechte'),
                icon: 'fa fa-user-shield'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Zuordnung'),
                        prop: 'aco_id',
                        tooltip: pi.Locale.trans('Bitte geben Sie an wem diese Domain gehört')
                    },
                    content: {
                        'SxSelectAco:0': {
                            model: {
                                path: 'aco_id'
                            }
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);

