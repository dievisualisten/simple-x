import { Ajax, Route } from "@dievisualisten/simplex";

Ajax.bind('menus-tree', function (ajax, query, options) {
    let route = Route.get('menus.tree', query, query);
    return ajax.get(route, options);
});

Ajax.bind('menus-move', function (ajax, query, options) {
    let route = Route.get('menus.move', query, null);
    return ajax.post(route, query, options);
});

Ajax.bind('menus-insert', function (ajax, query, options) {
    let route = Route.get('menus.insert', query, null);
    return ajax.post(route, query, options);
});

Ajax.rest('menus');

// Store.add('menus', (setter) => {
//     Ajax.call('menus-tree', false, { _locale: window.App.locale })
//         .then((res) => setter(res.data.data), () => setter([]));
// });
//
// Event.bind('locale/change', () => {
//     Store.refresh('menus');
// });

require("./config/menu-default.config");
require("./config/menu-domain.config");
// require("./config/menu-alias.config");

import SxMenuEdit from "./js/menu/menu-edit";
App.component(SxMenuEdit.name, SxMenuEdit);

import SxMenuDomain from "./js/menu/menu-domain";
App.component(SxMenuDomain.name, SxMenuDomain);

import SxMenuAlias from "./js/menu/menu-alias";
App.component(SxMenuAlias.name, SxMenuAlias);

import SxMenuTreeNode from "./js/helpers/menu-tree-node";
App.component(SxMenuTreeNode.name, SxMenuTreeNode);
