import { Ajax } from "@dievisualisten/simplex";

Ajax.rest('newsletterinterests');

require('./config/newsletterinterest-default.config');

import SxNewsletterinterestIndex from "./js/newsletterinterest/newsletterinterest-index";
App.component(SxNewsletterinterestIndex.name, SxNewsletterinterestIndex);

import SxNewsletterinterestEdit from "./js/newsletterinterest/newsletterinterest-edit";
App.component(SxNewsletterinterestEdit.name, SxNewsletterinterestEdit);
