import DefaultIndex from "../../../default/src/index/index.js";
import { Arr, Obj, Event } from "@kizmann/pico-js";
import { Route } from "@dievisualisten/simplex";

export default Obj.assign({}, DefaultIndex, {

    name: 'SxNewsletterinterestIndex',

    extends: DefaultIndex,

    urls: {
        index: 'newsletterinterests.index',
        edit: 'newsletterinterests.edit',
        create: 'newsletterinterests.create'
    },

    model: 'newsletterinterest',

    renderTable()
    {
        let updateEvents = [
            'newsletterinterests-edit'
        ];

        let props = {
            indexQuery: 'newsletterinterests-index',
            deleteQuery: 'newsletterinterests-delete',
            group: ['newsletterinterests'],
            updateEvents: updateEvents
        };

        Obj.assign(props, {
            'onUpdate:current': this.setCurrent,
            'onNewClick': () => this.newItem(),
            'onRowDblclick': (item) => this.editItem(item.id)
        });

        return (
            <NDatatable ref="table" {...props}>
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name'),
                        sort: true,
                        filter: true,
                        fluid: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'boolean',
                        prop: 'selectable',
                        label: this.trans('Auswählbar'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'string',
                        prop: 'newslettercount',
                        label: this.trans('Mailings'),
                        sort: true,
                        filter: true
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                        sort: true,
                        filter: true,
                    }
                } />
                <NTableColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                        sort: true,
                        filter: true,
                    }
                } />
            </NDatatable>
        );
    },

    renderInfo()
    {
        return (
            <NInfo ref="info">
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'name',
                        label: this.trans('Name')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'boolean',
                        prop: 'selectable',
                        label: this.trans('Auswählbar')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'string',
                        prop: 'newslettercount',
                        label: this.trans('Mailings')
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'modified',
                        label: this.trans('Bearbeitet am'),
                    }
                } />
                <NInfoColumn {
                    ...{
                        type: 'datetime',
                        prop: 'created',
                        label: this.trans('Erstellt am'),
                    }
                } />
            </NInfo>
        );
    },

    renderDefaultForm()
    {
        let props = {
            use: 'SxNewsletterinterestEdit',
            width: '1280px',
            height: '100%',
            localized: false,
            showQuery: 'newsletterinterests-show',
            editQuery: 'newsletterinterests-edit',
            newTitle: this.trans('Interesse erstellen'),
            editTitle: this.trans('{name} bearbeiten'),
            onNew: () => this.newItem(),
            onClose: () => this.showIndex(),
        };

        return (
            <NDataform ref="defaultForm" {...props} />
        );
    },

    render()
    {
        let slots = {
            tree: this.ctor('renderTree'),
            info: this.ctor('renderInfo')
        };

        slots.default = [
            this.ctor('renderTable')(),
            this.ctor('renderDefaultForm')(),
            this.ctor('renderRouteConfirm')(),
        ];

        let props = {
            storeKey: this.ctor('name')
        }

        return (
            <SxLayoutMain {...props} v-slots={slots} />
        );
    }

});
