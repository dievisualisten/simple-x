sx.Form.set('sx-newsletterinterest-default', [

    /* Konfiguration start */
    {
        'NTabsItem:0': {
            $props: {
                name: 'default',
                sort: 10,
                label: pi.Locale.trans('Interessen'),
                icon: 'fa fa-user-class'
            },
            content: {
                'NFormItem:0': {
                    $props: {
                        label: pi.Locale.trans('Interesse oder Liste'),
                        prop: 'name'
                    },
                    content: {
                        'NInput:0': {
                            model: {
                                path: 'name'
                            }
                        }
                    }
                },
                'NFormItem:1': {
                    $props: {
                        prop: 'selectable'
                    },
                    content: {
                        'NCheckbox:0': {
                            model: {
                                path: 'selectable', fallback: false
                            },
                            content: [
                                pi.Locale.trans('Durch den Newsletterempfänger im Frontend auswählbar')
                            ]
                        }
                    }
                }
            }
        }
    },
    /* Konfiguration end */

]);
