import { Arr, Data } from "@kizmann/pico-js";

export default {

    name: 'SxSelectAco',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        modelValue: {
            required: true
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return false;
            },
            type: [Boolean]
        }

    },

    computed: {

        items()
        {
            if ( this.SxIndexController !== undefined ) {
                return this.SxIndexController.acos;
            }

            return Data.get('acos.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            options: this.items,
            optionsLabel: '$value.pathname',
            optionsValue: '$value.id',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
