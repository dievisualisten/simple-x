import Nano from "@kizmann/pico-js";

let { Arr, Data } = Nano;

export default {

    name: 'SxSelectEmail',

    props: {

        modelValue: {
            required: true
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return false;
            },
            type: [Boolean]
        }

    },

    computed: {

        items()
        {
            return Data.get('emails.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            options: this.items,
            optionsLabel: '$value.name',
            optionsValue: '$value.id',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
