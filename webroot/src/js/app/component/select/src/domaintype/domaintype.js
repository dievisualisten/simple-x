import { Arr, Data } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxSelectDomaintype',

    props: {

        modelValue: {
            required: true
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        allowCreate: {
            default()
            {
                return true;
            },
            type: [Boolean]
        }

    },

    computed: {

        items()
        {
            return Config.domaintypes(this.domain);
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            allowCreate: this.allowCreate,
            options: this.items,
            optionsLabel: '$value',
            optionsValue: '$value',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
