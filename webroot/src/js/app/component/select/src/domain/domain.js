import { Arr, Data } from "@kizmann/pico-js";
import { Config } from "@dievisualisten/simplex";

export default {

    name: 'SxSelectDomain',

    props: {

        modelValue: {
            required: true
        },

        domain: {
            required: true
        },

        sorted: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        collapse: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return false;
            },
            type: [Boolean]
        }

    },

    computed: {

        items()
        {
            if ( this.sorted ) {
                return Arr.sortString(Config.domains(this.domain), 'value');
            }

            return Arr.sort(Config.domains(this.domain), 'order');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            collapse: this.collapse,
            options: Arr.filter(this.items, { active: true }),
            optionsLabel: '$value.value',
            optionsValue: '$value.key',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
