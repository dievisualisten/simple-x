import { Arr, Obj, Data } from "@kizmann/pico-js";

export default {

    name: 'SxSelectLocale',

    props: {

        modelValue: {
            required: true
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return false;
            },
            type: [Boolean]
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            options: this.items,
            optionsLabel: '$value.value',
            optionsValue: '$value.key',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
