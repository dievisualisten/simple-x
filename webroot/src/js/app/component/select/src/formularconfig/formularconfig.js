import Nano from "@kizmann/pico-js";

let { Arr, Data } = Nano;

export default {

    name: 'SxSelectFormularconfig',

    props: {

        modelValue: {
            required: true
        },

        disabled: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        multiple: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        clearable: {
            default()
            {
                return true;
            },
            type: [Boolean]
        }

    },

    computed: {

        items()
        {
            return Data.get('formularconfigs.data', []);
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            disabled: this.disabled,
            clearable: this.clearable,
            multiple: this.multiple,
            options: this.items,
            optionsLabel: '$value.name',
            optionsValue: '$value.id',
            undefinedText: this.trans('Ausserhalb Ihrer Sichtbarkeit'),
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return [
            <NSelect {...props} />
        ];
    }

};
