import SelectDomain from "./src/domain/domain";
App.component(SelectDomain.name, SelectDomain);

import SelectDomaintype from "./src/domaintype/domaintype";
App.component(SelectDomaintype.name, SelectDomaintype);

import SelectLocale from "./src/locale/locale";
App.component(SelectLocale.name, SelectLocale);

import SelectAco from "./src/aco/aco";
App.component(SelectAco.name, SelectAco);

import SelectRole from "./src/role/role";
App.component(SelectRole.name, SelectRole);

import SelectEmail from "./src/email/email";
App.component(SelectEmail.name, SelectEmail);

import SelectFormular from "./src/formular/formular";
App.component(SelectFormular.name, SelectFormular);

import SelectFormularconfig from "./src/formularconfig/formularconfig";
App.component(SelectFormularconfig.name, SelectFormularconfig);
