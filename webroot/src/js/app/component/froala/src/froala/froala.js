import { Dom, Any } from "@kizmann/pico-js";
require('froala-editor/js/languages/de');

export default {

    name: 'SxFroala',

    inject: {

        NTabs: {
            default: undefined
        },

        NTabsItem: {
            default: undefined
        }

    },

    props: {

        modelValue: {
            default()
            {
                return '';
            },
            type: [String]
        },

        config: {
            default()
            {
                return {};
            },
            type: [Object]
        },

    },

    data()
    {
        return {
            isVisible: false, veConfig: this.Obj.assign({}, window.froalaDefault, this.config)
        };
    },

    mounted()
    {
        this.watchVisible();
    },

    methods: {

        watchVisible()
        {
            clearTimeout(this.timer);

            if ( this.isVisible ) {
                return;
            }

            this.isVisible = this.$el.clientHeight &&
                this.$el.clientHeight > 1;

            this.timer = setTimeout(this.watchVisible, 150);
        },

    },

    render()
    {
        if ( ! this.isVisible ) {
            return <div style="height: 10px;"></div>;
        }

        let props = {
            modelValue: this.modelValue,
            config: this.veConfig,
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (<froala ref="editor" {...props}></froala>)
    }

}
