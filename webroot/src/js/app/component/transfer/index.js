
import TransferAco from "./src/aco/aco";
App.component(TransferAco.name, TransferAco);

import TransferUser from "./src/user/user";
App.component(TransferUser.name, TransferUser);

import TransferRole from "./src/role/role";
App.component(TransferRole.name, TransferRole);

import TransferResource from "./src/resource/resource";
App.component(TransferResource.name, TransferResource);

import TransferNewsletterinterest from "./src/newsletterinterest/newsletterinterest";
App.component(TransferNewsletterinterest.name, TransferNewsletterinterest);
