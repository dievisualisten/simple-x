import { Data, Locale } from "@kizmann/pico-js";

export default {

    name: 'SxTransferRole',

    props: {

        modelValue: {
            required: true
        },

        sourceLabel: {
            default()
            {
                return Locale.trans('Verfügbare Rollen');
            },
            type: [String]
        },

        targetLabel: {
            default()
            {
                return Locale.trans('Ausgewählte Rollen');
            },
            type: [String]
        }

    },

    computed: {

        items()
        {
            return Data.get('roles.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            options: this.items,
            sourceLabel: this.sourceLabel,
            targetLabel: this.targetLabel,
            labelProp: 'name',
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (
            <NTransfer {...props} />
        );
    }

};
