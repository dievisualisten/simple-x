import { Data, Locale } from "@kizmann/pico-js";

export default {

    name: 'SxTransferNewsletterinterest',

    props: {

        modelValue: {
            required: true
        },

        sourceLabel: {
            default()
            {
                return Locale.trans('Verfügbare Interessen');
            },
            type: [String]
        },

        targetLabel: {
            default()
            {
                return Locale.trans('Ausgewählte Interessen');
            },
            type: [String]
        }

    },

    computed: {

        items()
        {
            return Data.get('newsletterinterests.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            options: this.items,
            sourceLabel: this.sourceLabel,
            targetLabel: this.targetLabel,
            labelProp: 'name',
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (
            <NTransfer {...props} />
        );
    }

};
