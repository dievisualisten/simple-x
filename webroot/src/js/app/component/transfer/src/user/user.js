import { Data, Locale } from "@kizmann/pico-js";

export default {

    name: 'SxTransferUser',

    props: {

        modelValue: {
            required: true
        },

        sourceLabel: {
            default()
            {
                return Locale.trans('Verfügbare Benutzer');
            },
            type: [String]
        },

        targetLabel: {
            default()
            {
                return Locale.trans('Ausgewählte Benutzer');
            },
            type: [String]
        }

    },

    computed: {

        items()
        {
            return Data.get('users.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            options: this.items,
            sourceLabel: this.sourceLabel,
            targetLabel: this.targetLabel,
            labelProp: 'login',
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (
            <NTransfer {...props} />
        );
    }

};
