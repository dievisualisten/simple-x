import { Data, Locale } from "@kizmann/pico-js";

export default {

    name: 'SxTransferResource',

    props: {

        modelValue: {
            required: true
        },

        sourceLabel: {
            default()
            {
                return Locale.trans('Verfügbare Resource');
            },
            type: [String]
        },

        targetLabel: {
            default()
            {
                return Locale.trans('Ausgewählte Resource');
            },
            type: [String]
        }

    },

    computed: {

        items()
        {
            return Data.get('resources.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            options: this.items,
            sourceLabel: this.sourceLabel,
            targetLabel: this.targetLabel,
            labelProp: 'name',
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (
            <NTransfer {...props} />
        );
    }

};
