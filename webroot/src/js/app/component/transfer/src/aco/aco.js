import { Data, Locale } from "@kizmann/pico-js";

export default {

    name: 'SxTransferAco',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        modelValue: {
            required: true
        },

        sourceLabel: {
            default()
            {
                return Locale.trans('Verfügbare ACO\'s');
            },
            type: [String]
        },

        targetLabel: {
            default()
            {
                return Locale.trans('Ausgewählte ACO\'s');
            },
            type: [String]
        }

    },

    computed: {

        items()
        {
            if ( this.SxIndexController !== undefined ) {
                return this.SxIndexController.acos;
            }

            return Data.get('acos.data');
        }

    },

    render()
    {
        let props = {
            modelValue: this.modelValue,
            options: this.items,
            sourceLabel: this.sourceLabel,
            targetLabel: this.targetLabel,
            labelProp: 'name',
        };

        props['onUpdate:modelValue'] = (value) => {
            this.$emit('update:modelValue', value);
        };

        return (
            <NTransfer {...props} />
        );
    }

};
