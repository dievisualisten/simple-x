
import FileList from './src/filelist/filelist';
App.component(FileList.name, FileList);

import FileListItem from './src/filelist-item/filelist-item';
App.component(FileListItem.name, FileListItem);