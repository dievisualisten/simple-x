import { Arr, Obj, Str, Any } from "@kizmann/pico-js";

export default {

    name: 'SxFilelistItem',

    inject: {

        SxFilelist: {
            default: undefined
        }

    },

    props: {

        value: {
            required: true
        },

        item: {
            required: true
        },

        copy: {
            required: true
        },

        remove: {
            required: true
        }

    },

    computed: {

        file()
        {
            return Obj.get(this.item, this.SxFilelist.fileProp);
        },

        done()
        {
            return Obj.get(this.item, this.SxFilelist.doneProp);
        },

        error()
        {
            return Obj.get(this.item, this.SxFilelist.errorProp);
        },

        progress()
        {
            return Obj.get(this.item, this.SxFilelist.progressProp);
        },

        exceed()
        {
            return this.SxFilelist.fileLimit &&
                (this.item.file.size / 1024) > this.SxFilelist.fileLimit;
        }

    },

    data()
    {
        return {
            classList: []
        };
    },

    renderPreview()
    {
        return (
            <span class="fa fad fa-file"></span>
        );
    },

    renderName()
    {
        if ( ! this.file ) {
            return null;
        }

        return (
            <span>{ this.file.name }</span>
        );
    },

    renderMeta()
    {
        let body = Str.filesize(this.file.size);

        if ( ! this.file ) {
            return null;
        }

        let limit = {
            size: (this.SxFilelist.fileLimit || this.SxFilelist.imageLimit) / 1000
        };

        if ( this.exceed ) {
            body = this.trans('File exceeds filelimit of :size mb', limit);
        }

        return (
            <span>{ body }</span>
        );
    },

    render($render)
    {
        let classList = [
            'sx-filelist-item'
        ];

        if ( this.exceed === true ) {
            classList.push('sx-filelist-item--exceed');
        }

        if ( this.done === true ) {
            classList.push('sx-filelist-item--done');
        }

        if ( this.error === true ) {
            classList.push('sx-filelist-item--error');
        }

        return (
            <div class={classList}>
                <div class="sx-filelist-item__preview">
                    <NPreview file={this.file}></NPreview>
                </div>
                { this.SxFilelist.useProgress &&
                <div class="sx-filelist-item__progress">
                    <span style={{ width: this.progress + '%' }}></span>
                </div>
                }
                <div class="sx-filelist-item__name">
                    { this.ctor('renderName')() }
                </div>
                <div class="sx-filelist-item__meta">
                    { this.ctor('renderMeta')() }
                </div>
                <a class="sx-filelist-item__remove" href="javascript:void(0)" onClick={this.remove}>
                    <span class={nano.Icons.times}></span>
                </a>
            </div>
        );
    }

}
