
import { h, resolveComponent } from "vue";
import { Arr, Obj, Any } from "@kizmann/pico-js";

export default {

    name: 'SxFilelist',

    props: {

        items: {
            default()
            {
                return [];
            },
            type: [Array]
        },

        useProgress: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        fileProp: {
            default()
            {
                return 'file';
            },
            type: [String]
        },

        uniqueProp: {
            default()
            {
                return 'id';
            },
            type: [String]
        },

        doneProp: {
            default()
            {
                return 'done';
            },
            type: [String]
        },

        errorProp: {
            default()
            {
                return 'error';
            },
            type: [String]
        },

        progressProp: {
            default()
            {
                return 'progress';
            },
            type: [String]
        },

        imageLimit: {
            default()
            {
                return 16000;
            },
            type: [Number]
        },

        fileLimit: {
            default()
            {
                return 0;
            },
            type: [Number]
        },

    },

    provide()
    {
        return {
            SxFilelist: this
        };
    },

    renderFile(props)
    {
        return (
            <SxFilelistItem {...props} />
        );
    },

    render()
    {
        if ( this.init ) {
            return null;
        }

        let props = {
            items: this.items,
            itemWidth: 160,
            itemHeight: 220,
            deathzone: 20,
            renderNode: this.ctor('renderFile')
        };

        props['onUpdate:items'] = (items) => {
            this.$emit('update:items', items);
        };

        return (
            <NDraggrid class="sx-filelist" {...props} />
        );
    }

}
