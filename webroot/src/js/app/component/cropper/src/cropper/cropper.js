import { Num, Arr, Obj, Any, Dom, Locale, Str } from "@kizmann/pico-js";
import Croppr from "croppr";

export default {

    name: 'SxCropper',

    props: {

        modelValue: {
            required: true
        },

        width: {
            required: true
        },

        height: {
            required: true
        },

        version: {
            required: true
        },

        instruction: {
            required: true
        },

        options: {
            default()
            {
                return {};
            },
            type: [Object]
        },

        layout: {
            default()
            {
                return [
                    'custom', 'reset', 'upscale', 'spacer', 'apply'
                ];
            },
            type: [Array]
        },

    },

    computed: {

        asrx()
        {
            return Any.float(1 /
                this.version.size.w * this.version.size.h);
        },

        asry()
        {
            return Any.float(1 /
                this.version.size.h * this.version.size.w);
        },

        upscale()
        {
            return this.meta.s || false;
        }

    },

    watch: {

        // modelValue()
        // {
        //     Dom.find(this.$refs.image)
        //         .loaded(this.refresh);
        // },
        //
        // selected()
        // {
        //     Dom.find(this.$refs.image)
        //         .loaded(this.refresh);
        // },
        //
        upscale()
        {
            this.updateCropper();
        }

    },

    data()
    {
        return {

            viewport: {
                width: 0, height: 0
            },

            crop: {
                width: 0, height: 0
            },

            minimum: {
                width: 0, height: 0
            },

            position: {
                x: 0, y: 0
            },

            factor: {
                u: 1, d: 1
            },

            meta: {},
            changed: false
        }
    },

    beforeMount()
    {
        Croppr.prototype.attachOverlayEvents = () => {};
    },

    mounted()
    {
        Dom.find(this.$refs.image)
            .loaded(this.refreshCropper);
    },

    beforeUnmount()
    {
        if ( this.cropper ) {
            this.cropper.destroy();
        }
    },

    methods: {

        resetMeta()
        {
            this.meta = Obj.assign({
                w: null, h: null, x: null, y: null, s: false
            }, this.instruction);

            this.updateCropper();

            this.changed = false;
        },

        applyMeta()
        {
            Obj.assign({}, this.version, {
                crop: this.meta
            });

            this.$emit('apply', this.meta);
        },

        refreshCropper()
        {
            // Init new instance
            this.initCropper();

            // Upadte instance
            this.updateCropper();
        },

        initCropper()
        {
            if ( this.cropper !== undefined ) {
                this.cropper.destroy();
            }

            this.meta = Obj.assign({
                w: null, h: null, x: null, y: null, s: false
            }, this.instruction);

            this.viewport = {
                width: Dom.find(this.$refs.viewport).width(),
                height: Dom.find(this.$refs.viewport).height()
            };

            let xscale = {
                width: this.viewport.width, height: this.viewport.width * this.asrx
            };

            let yscale = {
                width: this.viewport.height * this.asry, height: this.viewport.height
            };

            this.factor = {
                d: 1 / this.width * this.viewport.width,
                u: 1 / this.viewport.width * this.width,
            };

            this.crop = {

                width: this.meta.w !== null || this.meta.h !== null ? this.meta.w * this.factor.d :
                    (xscale.height > this.viewport.height ? yscale.width : xscale.width),

                height: this.meta.w !== null || this.meta.h !== null ? this.meta.h * this.factor.d :
                    (xscale.height > this.viewport.height ? yscale.height : xscale.height)

            };

            let useViewport = this.version.transform.type === 'fitinside' &&
                Any.isEmpty(this.instruction);

            if ( useViewport ) {
                this.crop = Obj.clone(this.viewport);
            }

            this.position = {

                x: this.meta.x !== null || this.meta.y !== null ? this.meta.x * this.factor.d :
                    (this.viewport.width - this.crop.width) * 0.5,

                y: this.meta.x !== null || this.meta.y !== null ? this.meta.y * this.factor.d :
                    (this.viewport.height - this.crop.height) * 0.5

            };

            if ( this.version.transform.preserveratio === true ) {
                this.options.aspectRatio = this.asrx;
            }

            if ( this.version.transform.preserveratio === false ) {
                delete this.options.aspectRatio;
            }

            this.options.startSize = [
                this.crop.width, this.crop.height, 'px'
            ];

            let options = Obj.assign({
                onCropEnd: this.onCropEnd
            }, this.options);

            this.cropper = new Croppr(this.$refs.image, options);
        },

        updateCropper()
        {
            this.minimum = {
                width: this.version.size.w * this.factor.d,
                height: this.version.size.h * this.factor.d
            };

            if (
                this.version.size.w * this.factor.d > this.viewport.width ||
                this.version.size.h * this.factor.d > this.viewport.height
            ) {
                this.minimum = {
                    width: this.crop.width, height: this.crop.height
                };
            }

            let minimum = Obj.assign({}, this.minimum);

            if ( this.upscale ) {
                minimum = {
                    width: 0, height: 0
                };
            }

            if ( this.asrx > 1 ) {
                minimum.height = minimum.height * this.asry
            }

            this.cropper.options.minSize = minimum;

            this.cropper
                .resizeTo(this.crop.width, this.crop.height)
                .moveTo(this.position.x, this.position.y);
        },

        onCropEnd()
        {
            let box = {
                w: this.cropper.box.x2 - this.cropper.box.x1,
                h: this.cropper.box.y2 - this.cropper.box.y1,
                x: this.cropper.box.x1,
                y: this.cropper.box.y1
            };

            Obj.assign(this.meta, {
                w: box.w * this.factor.u, h: box.h * this.factor.u,
                x: box.x * this.factor.u, y: box.y * this.factor.u,
            });

            this.changed = true;
        },

        updateCrop()
        {

            // let data = Obj.assign({}, this.version, {
            //     crop: this.meta
            // });
            //
            // this.$emit('crop', { [this.selected]: data });
        },

    },

    renderSlot(slot)
    {
        if ( Any.isEmpty(this.$slots[slot]) ) {
            return null;
        }

        return (
            <div class={'sx-cropper__toolbar-' + slot}>
                { this.$slots[slot]() }
            </div>
        );
    },

    renderReset()
    {
        return (
            <div classN="sx-cropper__toolbar-reset">
                <NButton type="primary" icon="fa fa-eraser" disabled={!this.changed} onClick={this.resetMeta}>
                    { Locale.trans('Reset') }
                </NButton>
            </div>
        );
    },

    renderApply()
    {
        return (
            <div class="sx-cropper__toolbar-apply">
                <NButton type="success" icon="fa fa-save" onClick={this.applyMeta}>
                    { Locale.trans('Apply crop') }
                </NButton>
            </div>
        );
    },

    renderUpscale()
    {
        let disabled = Obj.get(this.version,
            'transform.allowupscale', false) === false;

        return (
            <div class="sx-cropper__toolbar-upscale">
                <NCheckbox vModel={this.meta.s} disabled={disabled} onUpdate:checked={this.updateCropper}>
                    { Locale.trans('Allow upscale') }
                </NCheckbox>
            </div>
        );
    },

    renderSpacer()
    {
        return (
            <div class="sx-cropper__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    render()
    {
        return (
            <div class="sx-cropper n-inverse">
                <div class="sx-cropper__wrapper">
                    <div class="sx-cropper__toolbar">
                        {
                            Arr.each(this.layout, (render) => {
                                return this.ctor('render' + Str.ucfirst(render),
                                    this.ctor('renderSlot'))(render);
                            })
                        }
                    </div>
                    <div class="sx-cropper__viewport">
                        <div ref="viewport" class="sc-cropper__viewport-inner">
                            <img ref="image" src={this.modelValue} />
                        </div>
                    </div>
                    <div class="sx-cropper__metadata">
                        <div class="sx-cropper__metadata-i">
                            <span>{ this.width + 'px' } x { this.height + 'px' }</span>
                        </div>
                        <div class="sx-cropper__metadata-w">
                            <span class={this.meta.w < this.version.size.w && 'sx-cropper__metadata--danger'}>
                                W: { Num.fixed(this.meta.w, 0) + 'px' }
                            </span>
                        </div>
                        <div class="sx-cropper__metadata-h">
                            <span class={this.meta.h < this.version.size.h && 'sx-cropper__metadata--danger'}>
                                H: { Num.fixed(this.meta.h, 0) + 'px' }
                            </span>
                        </div>
                        <div class="sx-cropper__metadata-x">
                            <span class={100 / this.meta.w * this.version.size.w > 100 && 'sx-cropper__metadata--danger'}>
                                X: { Num.fixed(100 / this.meta.w * this.version.size.w, 2) + '%' }
                            </span>
                        </div>
                        <div class="sx-cropper__metadata-y">
                            <span class={100 / this.meta.h * this.version.size.h > 100 && 'sx-cropper__metadata--danger'}>
                                Y: { Num.fixed(100 / this.meta.h * this.version.size.h, 2) + '%' }
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
