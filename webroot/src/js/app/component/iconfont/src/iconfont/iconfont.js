import Nano from "@kizmann/pico-js";

let { Data } = Nano;

export default {

    name: 'SxIconfont',

    props: {

        value: {
            required: true
        },

        prefix: {
            default()
            {
                return 'sx-customicon-';
            },
            type: [String]
        },

        enabled: {
            default()
            {
                return [
                    'whatsapp',
                    'facebook',
                    'guidedog',
                    'off'
                ];
            },
            type: [Array]
        },

        clearable: {
            default()
            {
                return true;
            },
            type: [Boolean]
        }

    },

    data()
    {
        let icons = [
            {
                code: 'guidedog', name: 'Blindenhund'
            },
            {
                code: 'whatsapp', name: 'Whatsapp'
            },
            {
                code: 'left-circled', name: 'left-circled'
            },
            {
                code: 'right-circled', name: 'right-circled'
            },
            {
                code: 'up-circled', name: 'up-circled'
            },
            {
                code: 'down-circled', name: 'down-circled'
            },
            {
                code: 'angle-circled-left', name: 'angle-circled-left'
            },
            {
                code: 'angle-circled-right', name: 'angle-circled-right'
            },
            {
                code: 'angle-circled-up', name: 'angle-circled-up'
            },
            {
                code: 'angle-circled-down', name: 'angle-circled-down'
            },
            {
                code: 'zoom-in', name: 'Lupe mit Plus'
            },
            {
                code: 'zoom-out', name: 'Lupe mit Minus'
            },
            {
                code: 'off', name: 'Aus'
            },
            {
                code: 'play', name: 'Play'
            },
            {
                code: 'play-circled', name: 'Play im Kreis'
            },
            {
                code: 'play-circled2', name: 'Play im Kreis'
            },
            {
                code: 'facebook', name: 'Facebook'
            },
            {
                code: 'circle', name: 'Kreis'
            },
            {
                code: 'circle-empty', name: 'Kreis leer'
            },
            {
                code: 'dot-circled', name: 'Punkt im Kreis'
            },
            {
                code: 'basket', name: 'Warenkorb'
            },
            {
                code: 'bag', name: 'Einkaufstasche'
            },
            {
                code: 'lock-open', name: 'Schloss offen'
            },
            {
                code: 'lock', name: 'Schloss'
            },
        ];

        icons = this.Arr.filter(icons, (icon) => {
            return this.Arr.has(this.enabled, icon.code);
        });

        return { icons };
    },

    renderIcon()
    {
        if ( this.Any.isEmpty(this.value) ) {
            return null;
        }

        return (
            <div class="sx-iconfont__icon">
                <i class={this.prefix + this.value}></i>
            </div>
        );
    },

    renderSelect()
    {
        let events = this.Obj.assign({}, this.$listeners, {
            'input': (value) => this.$emit('input', value)
        });

        return (
            <NSelect props={this.$props} on={events}>
                {
                    this.Arr.each(this.icons, this.ctor('renderOption'))
                }
            </NSelect>
        );
    },

    renderOption(icon)
    {
        return (
            <NSelectOption value={icon.code} label={icon.name}>
                <div class="sx-iconfont__option">
                    <i class={this.prefix + icon.code}></i> { icon.name }
                </div>
            </NSelectOption>
        );
    },

    render()
    {
        return (
            <div class="sx-iconfont">
                { this.ctor('renderIcon')() }
                { this.ctor('renderSelect')() }
            </div>
        );
    }

};
