import { FocusedImage, FocusPicker } from "image-focus";

export class SxFocusPicker extends FocusPicker
{
    destroy()
    {
        this.container.removeEventListener("mousedown", this.startDragging);
        this.container.removeEventListener("mousemove", this.handleMove);
        this.container.removeEventListener("mouseup", this.stopDragging);
        this.container.removeEventListener("mouseleave", this.stopDragging);
        this.container.removeEventListener("touchend", this.stopDragging);

        this.container.removeEventListener("touchstart", this.startDragging, { passive: true });
        this.container.removeEventListener("touchmove", this.handleMove, { passive: true });
    }
}

export class SxFocusedImage extends FocusedImage
{
    destroy()
    {

    }
}

export default { SxFocusPicker, SxFocusedImage };
