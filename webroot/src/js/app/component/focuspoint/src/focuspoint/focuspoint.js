import { Num, Obj, Locale, Any, Arr, Str } from "@kizmann/pico-js";
import { SxFocusPicker } from "../image-focus";

export default {

    name: 'SxFocuspoint',

    props: {

        modelValue: {
            required: true,
            type: [String]
        },

        instructions: {
            required: true
        },

        options: {
            default()
            {
                return {};
            },
            type: [Object]
        },

        layout: {
            default()
            {
                return [
                    'custom', 'reset', 'spacer', 'apply'
                ];
            },
            type: [Array]
        },

    },

    methods: {

        resetMeta()
        {
            this.focus.setFocus(this.initialMeta);

            this.changed = false;
        },

        applyMeta()
        {
            this.$emit('apply', this.meta);
        },

        onChange(data)
        {
            if ( data.x !== this.initialMeta.x || data.y !== this.initialMeta.y ) {
                this.changed = true;
            }

            this.meta = Obj.assign({}, this.meta, data);
        }
    },

    data()
    {
        return {
            initialMeta: {}, nativeOptions: {}, meta: {}, changed: false
        };
    },

    beforeMount()
    {
        this.meta = this.initialMeta = Obj.get(this.instructions, 'focuspoint', {
            x: 0, y: 0
        });

        Obj.assign(this.nativeOptions, this.options, {
            onChange: Any.debounce(this.onChange, 200), focus: this.meta
        });
    },

    mounted()
    {
        this.$watch('data', () => {
            this.$emit('update:focuspoint', this.meta);
        }, { deep: true });

        this.Dom.find(this.$refs.image).loaded((el) => {
            this.focus = new SxFocusPicker(el, this.nativeOptions);
        });
    },

    beforeUnmount()
    {
        if ( this.focus ) {
            this.focus.destroy();
        }
    },

    renderSlot(slot)
    {
        if ( Any.isEmpty(this.$slots[slot]) ) {
            return null;
        }

        return (
            <div class={'sx-focuspoint__toolbar-' + slot}>
                { this.$slots[slot]() }
            </div>
        );
    },

    renderReset()
    {
        return (
            <div class="sx-focuspoint__toolbar-reset">
                <NButton type="primary" icon="fa fa-eraser" disabled={!this.changed} onClick={this.resetMeta}>
                    {Locale.trans('Reset')}
                </NButton>
            </div>
        );
    },

    renderApply()
    {
        return (
            <div class="sx-focuspoint__toolbar-apply">
                <NButton type="success" icon="fa fa-save">
                    { Locale.trans('Apply focuspoint') }
                </NButton>
                <NConfirm onConfirm={this.applyMeta}>
                    { this.trans('All versions will be replaced with given focuspoint. Are you sure?') }
                </NConfirm>
            </div>
        );
    },

    renderSpacer()
    {
        return (
            <div class="sx-focuspoint__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    render()
    {
        return (
            <div class="sx-focuspoint">
                <div class="sx-focuspoint__wrapper">
                    <div class="sx-focuspoint__toolbar">
                        {
                            Arr.each(this.layout, (render) => {
                                return this.ctor('render' + Str.ucfirst(render),
                                    this.ctor('renderSlot'))(render);
                            })
                        }
                    </div>
                    <div class="sx-focuspoint__viewport">
                        <div class="sx-focuspoint__viewport-inner">
                            <img ref="image" src={this.modelValue} />
                        </div>
                    </div>
                    <div class="sx-focuspoint__metadata">
                        <div class="sx-focuspoint__metadata-x">
                            <span>
                                X: { Num.fixed(50 + (50 * this.meta.x * +1), 2) + '%' }
                            </span>
                        </div>
                        <div class="sx-focuspoint__metadata-y">
                            <span>
                                Y: { Num.fixed(50 + (50 * this.meta.y * +1), 2) + '%' }
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
