/**
 * @const Notify
 */

let requestLoad = function (req) {

    if ( req.onLoad !== undefined ) {
        req.onLoad();
    }

    return req;
};

let requestError = function (req) {

    let errors = pi.Obj.get(req, 'request.data.errors');

    if ( errors !== null ) {
        req.request.data.errors = pi.Obj.map(errors, error => {
            return pi.Any.vals(error).join(', ');
        });
    }

    let message = pi.Obj.get(req, 'request.data.message');

    if ( message !== null ) {
        Notify(message, 'danger');
    }

    if ( req.config.onDone !== undefined ) {
        req.config.onDone();
    }

    return Promise.reject(req.request || req);
};

window.axios.interceptors.request.use(requestLoad, requestError);

let responseLoad = function (res) {

    let message = pi.Obj.get(res, 'data.message', null);

    if ( message !== null ) {
        Notify(message, 'success');
    }

    if ( res.config.onDone !== undefined ) {
        res.config.onDone();
    }

    return res;
};

let responseError = function (res, ...args) {

    if ( ! pi.Obj.has(res, 'response') ) {
        console.log(res, ...args);
        return Notify('Unknown error', 'danger');
    }

    if ( res.response.status === 403 || res.response.status === 401 ) {
        return window.location.href = '/login?redirect=' + window.location.pathname;
    }

    let errors = pi.Obj.get(res, 'response.data.errors');

    if ( errors !== null ) {
        res.response.data.errors = pi.Obj.map(errors, (error) => {
            return pi.Any.vals(error).join(', ');
        });
    }

    let message = pi.Obj.get(res, 'response.data.message');

    if ( message !== null ) {
        Notify(message, 'danger');
    }

    if ( res.config.onDone !== undefined ) {
        res.config.onDone();
    }

    return Promise.reject(res.response || res);
};

window.axios.interceptors.response.use(responseLoad, responseError);
