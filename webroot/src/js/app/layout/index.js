import LayoutRoot from "./src/layout-root/layout-root";
App.component(LayoutRoot.name, LayoutRoot);

import LayoutLogin from "./src/layout-login/layout-login";
App.component(LayoutLogin.name, LayoutLogin);

import LayoutMenu from "./src/layout-menu/layout-menu";
App.component(LayoutMenu.name, LayoutMenu);

import LayoutAco from "./src/layout-aco/layout-aco";
App.component(LayoutAco.name, LayoutAco);

import LayoutLocale from "./src/layout-locale/layout-locale";
App.component(LayoutLocale.name, LayoutLocale);

import LayoutMain from "./src/layout-main/layout-main";
App.component(LayoutMain.name, LayoutMain);

import LayoutLocation from "./src/layout-location/layout-location";
App.component(LayoutLocation.name, LayoutLocation);
