import { Arr, Obj, Dom } from "@kizmann/pico-js";
import { Menu, Auth } from "@dievisualisten/simplex";

export default {

    name: 'SxLayoutMenu',

    inject: {

        SxLayoutRoot: {
            default: undefined
        }

    },

    props: {

        items: {
            default()
            {
                return Menu.menus;
            },
            type: [Array]
        },

        depth: {
            default()
            {
                return 0;
            },
            type: [Number]
        }

    },

    computed: {

        base()
        {
            return window.basePath;
        }

    },

    methods: {

        isVisible(item)
        {
            return ! Obj.get(item, 'hide', false) && Auth.can(item.rights);
        },

        hasVisibleChilds(item)
        {
            return !! Arr.filter(item.children || [], this.isVisible).length;
        },

        eventToggle(event)
        {
            let target = Dom.find(event.target)
                .closest('li');

            Dom.find(this.$el.childNodes).each((el) => {

                if ( this.SxLayoutRoot ) {
                    return Dom.find(el).is(target) && Dom.find(el).toggleClass('sx-open');
                }

                Dom.find(el).is(target) ? Dom.find(el).toggleClass('sx-open') :
                    Dom.find(el).removeClass('sx-open');
            });
        }

    },

    renderLink(item, renderChilds = true)
    {
        let iconHtml = (
            <div class={'nav__item-icon ' + item.icon}></div>
        );

        let textHtml = (
            <div class="n-sidebar__text">{item.title}</div>
        );

        return ({ href, route, navigate, isActive, isExactActive }) => {

            let classList = [];

            let isSame = window.location.pathname === href;

            if ( isSame ) {
                classList.push('sx-active');
            }

            let hasChild = Arr.find(item.children,
                { name: this.$route.name });

            if ( hasChild ) {
                classList.push('sx-active');
            }

            let props = {
                href: href, onClick: navigate
            };

            if ( renderChilds ) {
                props.href = 'javascript:void(0)';
            }

            if ( renderChilds ) {
                props.onClick = this.eventToggle;
            }

            return (
                <a class={classList} {...props}>
                    { [(item.icon && ! this.depth) && iconHtml, textHtml] }
                </a>
            )
        };
    },

    renderItem(item, renderChilds = true) {

        if ( ! this.isVisible(item) ) {
            return null;
        }

        renderChilds = false;

        if ( this.depth <= 1 ) {
            renderChilds = this.hasVisibleChilds(item);
        }

        return (
            <li class="nav__item">
                <RouterLink to={item.path} custom={true}>
                    { this.ctor('renderLink')(item, renderChilds) }
                </RouterLink>
                { renderChilds &&
                    <SxLayoutMenu items={item.children} depth={this.depth + 1} />
                }
            </li>
        );
    },

    render()
    {
        return (
            <ul class={'nav__layer--' + this.depth}>
                { Arr.each(this.items, this.ctor('renderItem')) }
            </ul>
        );
    }


}
