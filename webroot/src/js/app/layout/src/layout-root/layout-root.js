import { Dom, Cookie, Data, UUID } from "@kizmann/pico-js";
import { Auth } from "@dievisualisten/simplex";

export default {

    name: 'SxLayoutRoot',

    computed: {

        index()
        {
            return ! this.login && ! this.error;
        }

    },

    data()
    {
        return {
            fixedNav: Cookie.get('fixedNav', false, 'boolean'),
        };
    },

    provide()
    {
        return {
            SxLayoutRoot: this
        }
    },

    methods: {

        toggleNav()
        {
            this.Cookie.set('fixedNav', this.fixedNav = ! this.fixedNav);

            Dom.find(window).fire('resize');

            this.eventMouseleave();
        },

        eventMouseenter()
        {
            if ( this.sidebarHideDelay ) {
                clearTimeout(this.sidebarHideDelay);
            }

            this.sidebarShowDelay = setTimeout(() => {
                Dom.find(this.$refs.sidebar).addClass('sx-open');
            }, 200);
        },

        eventMouseleave()
        {
            if ( this.sidebarShowDelay ) {
                clearTimeout(this.sidebarShowDelay);
            }

            this.sidebarHideDelay = setTimeout(() => {

                if ( ! this.fixedNav ) {
                    Dom.find(this.$refs.sidebar).find('.sx-open').removeClass('sx-open');
                }

                Dom.find(this.$refs.sidebar).removeClass('sx-open');

            }, 50);
        }

    },

    render()
    {
        let events = {
            onMouseenter: this.eventMouseenter,
            onMouseleave: this.eventMouseleave,
        };

        let sidebarHtml = (
            <aside ref="sidebar" class="sx-sidebar" {...events}>
                <div class="sx-sidebar__inner">

                    <div class="sx-sidebar__logo">
                        <RouterLink to="/">
                            <span class="n-sidebar__text"></span>
                        </RouterLink>
                    </div>

                    <div class="sx-sidebar__nav">
                        <NScrollbar>
                            <nav class="nav">
                                <SxLayoutMenu />
                            </nav>
                        </NScrollbar>
                    </div>

                </div>
                <div class="sx-sidebar__toggle" onClick={this.toggleNav}>
                    <i class="fa fa-angle-right"></i>
                </div>
            </aside>
        );

        let classList = [
            'sx-root'
        ];

        if ( this.fixedNav ) {
            classList.push('sx-root--fixed-nav');
        }


        return (
            <div class={classList}>

                { sidebarHtml }

                <header class="sx-header grid grid--row grid--middle col--1-1">

                    <div class="sx-header__location col--flex-0-1">
                        <SxLayoutLocation />
                    </div>

                    <div class="sx-header__spacer col--flex-1-1">
                        { /* SPACER */ }
                    </div>

                    <div class="sx-header__aco col--flex-0-0">
                        <SxLayoutAco />
                    </div>

                    <div class="sx-header__locale col--flex-0-0">
                        <SxLayoutLocale />
                    </div>

                    <div class="sx-header__upload col--flex-0-0">
                        <SxAttachmentUpload />
                    </div>

                </header>

                <main class="sx-root__main">
                    <RouterView />
                </main>

            </div>
        );
    }

}
