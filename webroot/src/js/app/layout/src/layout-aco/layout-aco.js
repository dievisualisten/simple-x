import { Arr, Any, Event, Data } from "@kizmann/pico-js";
import { Auth } from "@dievisualisten/simplex";

export default {

    name: 'SxLayoutAco',

    data()
    {
        return {
            aco: Auth.user('aco_id'), acos: []
        };
    },

    methods: {

        getAcos()
        {
            let acoIds = Auth.perms('models.aco.acoIds', []);

            let acoFilter = (aco) => {
                return Arr.has(acoIds, aco.id);
            };

            this.acos = Arr.filter(Data.get('acos.data', []), acoFilter);
        }

    },

    beforeMount()
    {
        Event.bind('acos', this.getAcos);
    },

    mounted()
    {
        this.getAcos();
    },

    render()
    {
        if ( Any.isEmpty(this.acos) ) {
            return null;
        };

        let props = {
            modelValue: this.aco,
            options: this.acos,
            optionsValue: '$value.id',
            optionsLabel: '$value.pathname'
        }

        props['onUpdate:modelValue'] = (value) =>{
            Event.fire('aco/change', value);
        };

        return (
            <NSelect class="sx-layout-aco" {...props}>
                {/* EMPTY */}
            </NSelect>
        );
    }

}
