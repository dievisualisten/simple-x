import { Str, Obj, Dom, Locale, Cookie } from "@kizmann/pico-js";

export default {

    name: 'SxLayoutMain',

    props: {

        storeKey: {
            default()
            {
                return null;
            }
        },

        forceShowTree: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        showTree: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        treeWidth: {
            default()
            {
                return 320;
            },
            type: [Number]
        },

        treeMinWidth: {
            default()
            {
                return 320;
            },
            type: [Number]
        },

        treeMaxWidth: {
            default()
            {
                return 640;
            },
            type: [Number]
        },

        forceShowInfo: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        showInfo: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        infoWidth: {
            default()
            {
                return 420;
            },
            type: [Number]
        },

        infoMinWidth: {
            default()
            {
                return 320;
            },
            type: [Number]
        },

        infoMaxWidth: {
            default()
            {
                return 640;
            },
            type: [Number]
        },

    },

    methods: {

        getState(data)
        {
            data = Obj.clone(data);

            if ( ! this.storeKey ) {
                return data;
            }

            let storeData = Str.objectify(
                Cookie.get('LayoutMain.' + this.storeKey)
            );

            storeData = Obj.assign({}, data, storeData);

            if ( this.forceShowTree ) {
                storeData.tree.show = true;
            }

            storeData.tree.width = Math.max(storeData.tree.width, this.treeMinWidth);
            storeData.tree.width = Math.min(storeData.tree.width, this.treeMaxWidth);

            if ( this.forceShowInfo ) {
                storeData.info.show = true;
            }

            storeData.info.width = Math.max(storeData.info.width, this.infoMinWidth);
            storeData.info.width = Math.min(storeData.info.width, this.infoMaxWidth);

            return storeData;
        },

        setState(data)
        {
            data = Obj.clone(data);

            if ( ! this.storeKey ) {
                return null;
            }

            let storeData = Str.objectify(
                Cookie.get('LayoutMain.' + this.storeKey)
            );

            // Merge data to store
            storeData = Obj.assign({}, data, storeData);

            if ( this.forceShowTree ) {
                data.tree.show = storeData.tree.show;
            }

            if ( this.forceShowInfo ) {
                data.info.show = storeData.info.show;
            }

            Cookie.set('LayoutMain.' + this.storeKey,
                Str.stringify(data), { expires: 30 });
        },

        eventClickTree()
        {
            this.tree.show = ! this.tree.show;
        },

        eventClickInfo()
        {
            this.info.show = ! this.info.show;
        }

    },

    provide()
    {
        return {
            SxLayoutMain: this
        };
    },

    data()
    {
        let tree = {
            show: this.showTree,
            width: this.treeWidth
        };

        let info = {
            show: this.showInfo,
            width: this.infoWidth
        };

        return this.getState({ tree, info });
    },

    updated()
    {
        this.setState({ tree: this.tree, info: this.info })
    },

    renderTreeBar()
    {
        if ( ! this.$slots.tree || this.forceShowTree ) {
            return null;
        }

        let props = {
            onClick: this.eventClickTree
        };

        return (
            <div class="sx-main__tree-bar" {...props}>
                <span>{Locale.trans('Struktur')}</span>
            </div>
        );
    },

    renderTree()
    {
        if ( ! this.$slots.tree || ! this.tree.show ) {
            return null;
        }

        let props = {
            position: 'right',
            minWidth: this.treeMinWidth,
            maxWidth: this.treeMaxWidth,
        };

        props['onUpdate:modelValue'] = (value) => {
            Dom.find(window).fire('resize', this.tree.width = value);
        };

        return (
            <NResizer ref="tree" class="sx-main__tree" {...props}>
                <div class="sx-main__tree-inner">{ this.$slots.tree() }</div>
            </NResizer>
        );
    },

    renderInfoBar()
    {
        if ( ! this.$slots.info || this.forceShowInfo ) {
            return null;
        }

        let events = {
            onClick: this.eventClickInfo
        };

        return (
            <div class="sx-main__info-bar" {...events}>
                <span>{Locale.trans('Information')}</span>
            </div>
        );
    },

    renderInfo()
    {
        if ( ! this.$slots.info || ! this.info.show ) {
            return null;
        }

        let props = {
            position: 'left',
            minWidth: this.infoMinWidth,
            maxWidth: this.infoMaxWidth
        };

        props['onUpdate:modelValue'] = (value) => {
            Dom.find(window).fire('resize', this.info.width = value);
        };

        return (
            <NResizer ref="info" class="sx-main__info" {...props}>
                <div class="sx-main__info-inner">{ this.$slots.info() }</div>
            </NResizer>
        );
    },

    renderBody()
    {
        return (
            <section class="sx-main__body">
                { this.$slots.default && this.$slots.default() }
            </section>
        );
    },

    render()
    {
        return (
            <div class="sx-main">
                { this.ctor('renderTreeBar')() }
                { this.ctor('renderTree')() }
                { this.ctor('renderBody')() }
                { this.ctor('renderInfo')() }
                { this.ctor('renderInfoBar')() }
            </div>
        );
    }

}
