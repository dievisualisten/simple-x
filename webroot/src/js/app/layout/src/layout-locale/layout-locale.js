import { Arr, Obj, Event, Data } from "@kizmann/pico-js";
import { Auth } from "../../../../simplex/library/auth";

export default {

    name: 'SxLayoutLocale',

    data()
    {
        return {
            locales: []
        };
    },

    methods: {

        getLocales()
        {
            this.locales = Data.get('locales.data', []);
        }

    },

    beforeMount()
    {
        Event.bind('locales', this.getLocales);
    },

    mounted()
    {
        this.getLocales();
    },

    render()
    {
        let props = {
            modelValue: this.$root.locale,
            options: this.locales,
            optionsValue: '$value.key',
            optionsLabel: '$value.value'
        };

        props['onUpdate:modelValue'] = (value) => {
            Event.fire('locale/change', value);
        };

        return (
            <NSelect class="sx-layout-locale" {...props} />
        );
    }

}
