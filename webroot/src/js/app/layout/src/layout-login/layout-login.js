import { Dom } from "@kizmann/pico-js";

export default {

    name: 'SxLayoutLogin',

    data()
    {
        return {
            locale: 'de', aco: ''
        };
    },

    created()
    {
        Dom.title(this.trans('Login'));
    },

    render()
    {
        return (
            <div class="sx-layout-login">
                <div class="sx-layout-login__body">
                    <SxUserLogin />
                </div>
                <div class="sx-layout-login__image">
                    <span></span>
                </div>
            </div>
        );
    }

}
