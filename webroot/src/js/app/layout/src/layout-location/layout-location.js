import { Arr } from "@kizmann/pico-js";
import { Build, Menu } from "@dievisualisten/simplex";

export default {

    name: 'SxLayoutLocation',

    computed: {

        nav()
        {
            return Menu.menus;
        }

    },

    renderItem(item)
    {
        let route = {
            name: item.redirect || item.name
        };

        return (
            <div class="sx-layout-location__item">
                <RouterLink to={route}>{ item.title }</RouterLink>
            </div>
        );
    },

    render()
    {
        let breadcrumbs = [
            { name: 'dashboard.index', title: Build.getPagetitle() }
        ];

        Arr.recursive(Menu.menus, 'children', (item, cascade) => {
            if ( this.$route.name && item.name === this.$route.name ) {
                breadcrumbs = Arr.merge(breadcrumbs, cascade, item);
            }
        });

        return (
            <div class="sx-layout-location">
                {
                    Arr.each(breadcrumbs, this.ctor('renderItem'))
                }
            </div>
        )
    }

}
