/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import { Obj, Arr, Any, Dom, Data, Locale, Event } from "@kizmann/pico-js";
import { Build, Auth, Menu, Ajax, Route } from "@dievisualisten/simplex";
import { Install as VueNano } from "@kizmann/nano-ui";
import VueFroalaWysiwyg  from "vue-froala-wysiwyg/src/vue-froala";


Ajax.bind('elements', function (ajax, query, options) {
    let route = Route.get('system.elements', null, null);
    return ajax.post(route, Ajax.form(query), options);
});

Ajax.bind('stats', function (ajax, query, options) {
    let route = Route.get('system.stats', null, null);
    return ajax.get(route, query, options);
});

Ajax.bind('article', function (ajax, query, options) {
    let route = Route.get('system.article', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'pageconfigurations-edit',
    'pageconfigurations-delete',
], () => {
    Ajax.call('article', true);
});

Ajax.bind('auth', function (ajax, query, options) {
    let route = Route.get('system.auth', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'acos-edit',
    'acos-delete',
    'roles-edit',
    'roles-delete',
    'users-edit',
    'users-delete',
    'resources-edit',
    'resources-delete',
], () => {
    Ajax.call('auth', true);
});

Ajax.bind('config', function (ajax, query, options) {
    let route = Route.get('system.config', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'configurations-edit',
    'configurations-delete',
    'pageconfigurations-edit',
    'pageconfigurations-delete',
], () => {
    Ajax.call('config', true);
});

Ajax.bind('locales', function (ajax, query, options) {
    let route = Route.get('system.locales', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'configurations-edit',
    'configurations-delete',
], () => {
    Ajax.call('locales', true);
});

Ajax.bind('acos', function (ajax, query, options) {
    let route = Route.get('system.acos', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'acos-edit',
    'acos-delete',
    'roles-edit',
    'roles-delete',
    'resources-edit',
    'resources-delete',
], () => {
    Ajax.call('acos', true);
});

Ajax.bind('users', function (ajax, query, options) {
    let route = Route.get('system.users', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'acos-edit',
    'acos-delete',
    'roles-edit',
    'roles-delete',
    'resources-edit',
    'resources-delete',
], () => {
    Ajax.call('users', true);
});

Ajax.bind('roles', function (ajax, query, options) {
    let route = Route.get('system.roles', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'acos-edit',
    'acos-delete',
    'roles-edit',
    'roles-delete',
    'resources-edit',
    'resources-delete',
], () => {
    Ajax.call('roles', true);
});

Ajax.bind('emails', function (ajax, query, options) {
    let route = Route.get('system.emails', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'emails-edit',
    'emails-delete',
], () => {
    Ajax.call('emails', true);
});

Ajax.bind('formulars', function (ajax, query, options) {
    let route = Route.get('system.formulars', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'formulars-edit',
    'formulars-delete',
], () => {
    Ajax.call('formulars', true);
});

Ajax.bind('formularconfigs', function (ajax, query, options) {
    let route = Route.get('system.formularconfigs', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'formularconfigs-edit',
    'formularconfigs-delete',
], () => {
    Ajax.call('formularconfigs', true);
});

Ajax.bind('domains', function (ajax, query, options) {
    let route = Route.get('system.domains', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'domains-edit',
    'domains-delete',
], () => {
    Ajax.call('domains', true);
});

Ajax.bind('newsletterinterests', function (ajax, query, options) {
    let route = Route.get('system.newsletterinterests', null, null);
    return ajax.get(route, query, options);
});

Event.bind([
    'newsletterinterests-edit',
    'newsletterinterests-delete',
], () => {
    Ajax.call('newsletterinterests', true);
});

require('./config/axios');

Dom.ready(function () {

    window.ADMINMODE = false;

    Dom.find(document).on('keydown', (event) => {
        if ( event.which === 119 ) {
            window.ADMINMODE = ! window.ADMINMODE;

            console.log('Admin mode is ' + ( window.ADMINMODE ? 'on' : 'off'));
        }
    });

    let delay = 1000;

    if ( Build.getDebug() ) {
        delay = 0;
    }

    Any.delay(() => {
        Dom.find('.loader').addClass('hidden');
    }, delay);

    Build.setLocales(Data.get('locales.data'));

    const App = window.App = Vue.createApp({
        data()
        {
            return {
                locale: Build.getLocale(), aco: Auth.user('aco_id')
            };
        },
        mounted()
        {
            Event.bind('aco/change', (aco) => {
                Event.fire('aco/changed', this.aco = aco);
            }, this._.uid);

            Event.bind('locale/change', (locale) => {
                Event.fire('locale/changed', this.locale = locale);
            }, this._.uid);
        },
        render() {

            if ( window.location.href.match(/\/login(\?|$)/) ) {
                return (<SxLayoutLogin></SxLayoutLogin>);
            }

            return (<SxLayoutRoot></SxLayoutRoot>);
        }
    });

    App.config.devtools = true;

    require("../nano/pro/bootstrap");
    require("./component/cropper/index");
    require("./component/focuspoint/index");
    require("./component/froala/index");
    require("./component/select/index");
    require("./component/transfer/index");
    require("./component/filelist/index");
    require("./layout/index");
    require("./controller/dashboard/index");
    require("./controller/article/index");
    require("./controller/element/index");
    require("./controller/attachment/index");
    require("./controller/domain/index");
    require("./controller/menu/index");
    require("./controller/user/index");
    require("./controller/configuration/index");
    require("./controller/pageconfiguration/index");
    require("./controller/text/index");
    require("./controller/crontask/index");
    require("./controller/redirect/index");
    require("./controller/email/index");
    require("./controller/user/index");
    require("./controller/resource/index");
    require("./controller/role/index");
    require("./controller/aco/index");
    require("./controller/newsletter/index");
    require("./controller/newsletterinterest/index");
    require("./controller/newsletterrecipient/index");
    require("./controller/formular/index");
    require("./controller/formularconfig/index");
    require("./controller/error/index");

    App.use(VueFroalaWysiwyg);

    App.use(function (App) {
        App.config.globalProperties.$http = axios;
    });

    let routes = [];

    Arr.recursive(Menu.menus, 'children', (menu, cascade) => {

        // Add root menu to meta
        let root = Arr.first(cascade) || menu;

        // Merge data to route object
        let data = Obj.assign({ meta: { root, menu } }, menu);

        if ( data.redirect ) {
            data.redirect = { name: data.redirect };
        }

        if ( data.component ) {
            data.component = App.component(data.component);
        }

        routes.push(data);
    });

    let history = VueRouter.createWebHistory(
        sx.Build.paths.base);

    const AppRouter = VueRouter.createRouter({
        history, mode: 'history', routes: routes,
    });

    AppRouter.afterEach((to) => {
        Dom.title(to.meta.menu.title + ' - ' + Build.getPagetitle());
    });

    nano.Icons = Obj.assign(nano.Icons, {
        copy: 'fa fa-copy',
    });

    App.use(AppRouter);
    App.use(VueNano);

    App.mount('#app');
});


// Vue.config.performance = true;
//
// import VueNano from 'nano-ui';
// Vue.use(VueNano);
//
// window.Nano.install(Vue.prototype);
//
// import VueRouter from "vue-router";
// Vue.use(VueRouter);
//
// require('./config/axios');
//
// require('../nano/pro/bootstrap');
//
// require('./component/cropper/index');
// require('./component/focuspoint/index');
// require('./component/select/index');
// require('./component/transfer/index');
// require('./component/froala/index');
// require('./component/iconfont/index');
//
// require('./controller/error/index');
// require('./controller/dashboard/index');
// require('./controller/menu/index');
// require('./controller/article/index');
// require('./controller/attachment/index');
// require('./controller/aco/index');
// require('./controller/user/index');
// require('./controller/role/index');
// require('./controller/resource/index');
// require('./controller/newsletter/index');
// require('./controller/newsletterrecipient/index');
// require('./controller/newsletterinterest/index');
// require('./controller/formular/index');
// require('./controller/formularconfig/index');
// require('./controller/configuration/index');
// require('./controller/pageconfiguration/index');
// require('./controller/text/index');
// require('./controller/crontask/index');
// require('./controller/redirect/index');
// require('./controller/domain/index');
// require('./controller/email/index');
// require('./controller/code/index');
//
// require('./layout/index');
//
//
//
// Vue.Dom.ready(() => {
//
//     Vue.Element.observe('ready');
//
//     Vue.Event.bind('SxAcoIndex:destroy', () => {
//         Vue.Ajax.call(['acos-total', 'acos'], true);
//     });
//
//     Vue.Event.bind('SxRoleIndex:destroy', () => {
//         Vue.Ajax.call(['roles-total', 'roles'], true);
//     });
//
//     Vue.Event.bind('SxResourceIndex:destroy', () => {
//         Vue.Ajax.call(['resources-total', 'resources'], true);
//     });
//
//     Vue.Event.bind('SxDomainIndex:destroy', () => {
//         Vue.Ajax.call(['domains-total', 'domains'], true);
//     });
//
//     Vue.Event.bind('SxEmailIndex:destroy', () => {
//         Vue.Ajax.call(['emails-total', 'emails'], true);
//     });
//
//     Vue.Event.bind('SxFormularIndex:destroy', () => {
//         Vue.Ajax.call(['formulars-total', 'formulars'], true);
//     });
//
//     Vue.Event.bind('SxFormularconfigIndex:destroy', () => {
//         Vue.Ajax.call(['formularconfigs-total', 'formularconfigs'], true);
//     });
//
//     Vue.Event.bind([
//         'acos-total', 'roles-total', 'domains-total', 'resources-total',
//         'emails-total', 'formulars-total', 'formularconfigs-total'
//     ], () => {
//         Vue.Ajax.call(['auth-cache', 'auth'], true);
//     });
//
//     let RootComponent = Vue.component('SxLayoutRoot');
//
//     if ( window.location.pathname.match(/^(\/[a-z]{2})?\/login/) ) {
//         RootComponent = Vue.component('SxLayoutLogin');
//     }
//
//     window.App = new Vue(RootComponent).$mount('#app');
//
// });
