import { Str, Arr, Obj, Any, Locale, Data, UUID } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'NDataform',

    model: {
        prop: 'id'
    },

    props: {

        id: {
            default()
            {
                return UUID();
            }
        },

        use: {
            default()
            {
                return null;
            }
        },

        localized: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        layout: {
            default()
            {
                return ['copy', 'spacer', 'close', 'apply', 'save'];
            },
            type: [Array]
        },

        showQuery: {
            default()
            {
                return null;
            }
        },

        editQuery: {
            default()
            {
                return null;
            }
        },

        copyQuery: {
            default()
            {
                return null;
            }
        },

        newTitle: {
            default()
            {
                return Locale.trans('Create');
            },
            type: [String]
        },

        editTitle: {
            default()
            {
                return Locale.trans('Edit');
            },
            type: [String]
        },

        newProp: {
            default()
            {
                return '_new';
            }
        },

        localeProp: {
            default()
            {
                return '_locale';
            }
        },

        fallback: {
            default()
            {
                return {};
            }
        }

    },

    computed: {

        title()
        {
            if ( this.isNew === null ) {
                return Locale.trans('Loading');
            }

            let editTitle = this.editTitle;

            Arr.each(editTitle.match(/{(.*?)}/g) || [], (match) => {

                let result = Obj.get(this.value, 'data.' + match.replace(/{|}/g, ''));

                editTitle = editTitle.replace(match, result || this.trans('(Empty)'));
            });

            return this.isNew ? this.newTitle : editTitle;
        },

        isNew()
        {
            return Obj.get(this.value, ['data', this.newProp]);
        }

    },

    methods: {

        eventReady(data)
        {
            this.$nextTick(() => {
                if ( this.$refs.tabs ) {
                    this.$refs.tabs.getTab(true);
                }
            });

            this.$nextTick(() => {
                Any.delay(this.clearHistory, 250);
            });

            this.$emit('ready', data);
        },

        callNext()
        {
            Arr.each(this.nextActions, (next, index) => {
                if ( next ) {
                    this.$nextTick(() => next(index+1 === this.nextActions.length));
                }
            });

            this.nextActions = [];
        },

        clearHistory()
        {
            // this._changed = Any.md5(this.value);
        },

        setLoad(value, key)
        {
            let loading = Obj.set(this.loading, key, value);

            loading = Any.vals(this.loading);

            this.load = Arr.has(loading, true);
        },

        setOverride(override)
        {
            this.override = override;
        },

        queryDone(res, raw = false)
        {
            if ( raw ) {
                res = { data: res };
            }

            if ( this.value === null ) {
                this.$nextTick(() => this.eventReady(res.data));
            }

            // Set value
            this.value = res.data;

            if ( Obj.get(this.value, ['data', this.newProp], false) ) {
                this.tab = 'default'
            }

            this.temporaryLocale = this.locale = Obj.get(res.data,
                ['data', this.localeProp], this.locale);


            // Store for changed comparison
            this.$nextTick(() => {

                // Reset errors
                this.errors = {};

                Any.delay(this.clearHistory, 250);
            });
        },

        queryRedirect(res)
        {
            this.$emit('redirect', { row: res.data.data });
        },

        queryClose()
        {
            this.clearHistory();
            this.$emit('close');
        },

        queryError(res)
        {
            this.errors = res.data.errors;
        },

        queryShow(override = {})
        {
            let defaultValue = {
                data: Obj.clone(this.fallback)
            };

            if ( Any.isEmpty(this.showQuery) ) {
                return this.queryDone(defaultValue, true);
            }

            let query = Obj.assign(Obj.get(this.value, 'data', {}), override);

            if ( this.id ) {
                query = Obj.assign(query, { id: this.id })
            }

            if ( Any.isEmpty(this.override) === false ) {
                query = Obj.assign(query, this.override);
            }

            query[this.localeProp] = this.locale;

            let options = {
                onLoad: () => this.setLoad(true, this.showQuery),
                onDone: () => this.setLoad(false, this.showQuery)
            };

            Ajax.call(this.showQuery, false, query, options)
                .then(this.queryDone);
        },

        queryApply(override = {})
        {
            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            if ( ! Any.isEmpty(this.override) && this.isNew ) {
                query = Obj.assign(query, this.override);
            }

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.editQuery, false, query, options)
                .then(this.queryDone, this.queryError);
        },

        querySave(override = {})
        {
            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            if ( ! Any.isEmpty(this.override) && this.isNew ) {
                query = Obj.assign(query, this.override);
            }

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.editQuery, false, query, options)
                .then(this.queryClose, this.queryError);
        },

        queryCopy(override = {})
        {
            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.copyQuery, false, query, options)
                .then(this.queryRedirect, this.queryError);
        },

        openCopy()
        {
            // if ( this._changed === Any.md5(this.value) ) {
            //     return this.queryCopy();
            // }

            this.confirmCopy = true;
        },

        closeForm()
        {
            this.nextActions.push(next);

            // if ( this._changed === Any.md5(this.value) ) {
            //     return ! this.callNext();
            // }

            return ! (this.confirmClose = true);
        },

        closeForce(next)
        {
            this.nextActions = [];

            // this._changed = Any.md5(this.value);

            return next();
        },

        changeLocale()
        {
            // if ( this._changed === Any.md5(this.value) ) {
            //     return this.$emit('locale');
            // }

            this.confirmLocale = true;
        }

    },

    provide()
    {
        return {
            NDataform: this
        };
    },

    data()
    {
        let store = Data.get('NDataform:' + (this.use || this.showQuery), {});

        let defaults = {
            query: null,
            tab: 'default'
        };

        let override = {
            locale: 'en',
            temporaryLocale: 'en',
            override: null,
            value: null,
            errors: null,
            confirmCopy: false,
            confirmClose: false,
            confirmLocale: false,
            loading: {},
            load: false,
            nextActions: []
        };

        return Obj.assign(defaults, store, override);
    },

    mounted()
    {
        if ( ! Any.isEmpty(this.$root.locale) ) {
           this.locale = this.temporaryLocale = this.$root.locale;
        }

        this.queryShow();

        // this.$on('locale', () => {
        //     this.Event.fire('locale/change:form', this.locale = this.temporaryLocale);
        // });

        this.$watch('locale', () => this.queryShow());
    },

    beforeDestroy()
    {
        Data.set('NDataform:' + (this.use || this.showQuery), this.$data);
    },

    renderSlot(slot)
    {
        if ( Any.isEmpty(this.$slots[slot]) ) {
            return null;
        }

        return (
            <div class={'n-dataform__toolbar-' + slot}>
                { this.$slots[slot] }
            </div>
        );
    },

    renderApply()
    {
        return (
            <div class="n-dataform__toolbar-apply">
                <NButton type="success" onClick={() => this.queryApply()}>
                    { Locale.trans('Apply') }
                </NButton>
            </div>
        );
    },

    renderSave()
    {
        return (
            <div class="n-dataform__toolbar-save">
                <NButton type="primary" onClick={() => this.querySave()}>
                    { Locale.trans('Save') }
                </NButton>
            </div>
        );
    },

    renderClose()
    {
        return (
            <div class="n-dataform__toolbar-close">
                <NButton type="secondary" onClick={() => this.$emit('close')}>
                    { Locale.trans('Close') }
                </NButton>
            </div>
        );
    },

    renderCopy()
    {
        if ( this.copyQuery === null ) {
            return null;
        }

        return (
            <div class="n-dataform__toolbar-copy">
                <NButton type="warning" onClick={() => this.openCopy()}>
                    { Locale.trans('Copy') }
                </NButton>
            </div>
        );
    },

    renderNew()
    {
        return (
            <div class="n-dataform__toolbar-new">
                <NButton type="primary" onClick={() => this.$emit('new')}>
                    { Locale.trans('New') }
                </NButton>
            </div>
        );
    },

    renderSpacer()
    {
        return (
            <div class="n-dataform__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    renderLocalize()
    {
        if ( ! this.localized ) {
            return null;
        }

        return (
            <div class="n-dataform__locale">
                <NSelect ref="select" vModel={this.temporaryLocale} disabled={this.isNew} onInput={() => this.changeLocale()}>
                    {
                        Arr.each(Obj.get(this.$root, 'locales', []), (locale) => {
                            return <NSelectOption value={locale.key}>{locale.value}</NSelectOption>
                        })
                    }
                </NSelect>
            </div>
        );
    },

    renderHeader()
    {

        return [
            <div class="n-dataform__title">
                <span>{ this.title }</span>
            </div>,
            this.ctor('renderLocalize')(),
            <div class="n-dataform__close" onClick={() => this.$emit('close')}>
                <span class="fa fa-times"></span>
            </div>
        ];
    },

    renderBody()
    {
        let props = {
            value: Obj.get(this.value, 'data', {}), errors: this.errors || {}
        };

        let component = Vue.resolveComponent(this.use);

        let render = Vue.h(component, {
            ...props, slots: this.$slots
        });

        return (
            <NTabs ref="tabs" vModel={this.tab} relative={true} class="n-dataform__tabs">
                { ! this.use ? this.$scopedSlots.default(props) : render }
            </NTabs>
        );
    },

    renderFooter()
    {
        return (
            <div class="n-dataform__toolbar">
                {
                    Arr.each(this.layout, (render) => {
                        return this.ctor('render' + Str.ucfirst(render), this.ctor('renderSlot'))(render);
                    })
                }
            </div>
        );
    },

    render()
    {
        return (
            <NLoader class="n-dataform" visible={this.load || Any.isNull(this.value)}>
                <div class="n-dataform__header">
                    { this.ctor('renderHeader')() }
                </div>
                <div class="n-dataform__body">
                    { this.ctor('renderBody')() }
                </div>
                <div class="n-dataform__footer">
                    { this.ctor('renderFooter')() }
                </div>
                <NConfirm type="warning" closable={false} vModel={this.confirmCopy} listen={false} onConfirm={() => this.queryCopy()}>
                    { Locale.trans('Copy without saving? Changes will not be saved!') }
                </NConfirm>
                <NConfirm type="warning" closable={false} vModel={this.confirmLocale} listen={false} onConfirm={() => this.$emit('locale')} onAbort={() => this.temporaryLocale = this.locale}>
                    { Locale.trans('Change lanugage without saving?') }
                </NConfirm>
            </NLoader>
        );
    }
}
