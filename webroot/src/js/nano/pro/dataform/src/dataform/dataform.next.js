import { h, resolveComponent } from "vue";
import { Str, Arr, Obj, Any, Locale, Data, Event, UUID } from "@kizmann/pico-js";
import { Build, Ajax } from "@dievisualisten/simplex";

export default {

    name: 'NDataform',

    inheritAttrs: false,

    inject: {
        SxIndexController: {
            default: undefined
        }
    },

    props: {

        width: {
            default()
            {
                return '50%';
            },
            type: [String]
        },

        height: {
            default()
            {
                return 'auto';
            },
            type: [String]
        },

        type: {
            default()
            {
                return 'form';
            },
            type: [String]
        },

        use: {
            default()
            {
                return null;
            }
        },

        localized: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        layout: {
            default()
            {
                return ['copy', 'spacer', 'close', 'apply', 'save'];
            },
            type: [Array]
        },

        showQuery: {
            default()
            {
                return null;
            }
        },

        editQuery: {
            default()
            {
                return null;
            }
        },

        copyQuery: {
            default()
            {
                return null;
            }
        },

        newTitle: {
            default()
            {
                return Locale.trans('Create');
            },
            type: [String]
        },

        editTitle: {
            default()
            {
                return Locale.trans('Edit');
            },
            type: [String]
        },

        newProp: {
            default()
            {
                return '_new';
            }
        },

        localeProp: {
            default()
            {
                return '_locale';
            }
        },

        fallback: {
            default()
            {
                return {};
            }
        }

    },

    computed: {

        title()
        {
            if ( this.isNew === null ) {
                return Locale.trans('Loading');
            }

            let editTitle = this.editTitle;

            Arr.each(editTitle.match(/{(.*?)}/g) || [], (match) => {

                let result = Obj.get(this.value, 'data.' + match.replace(/{|}/g, ''));

                editTitle = editTitle.replace(match, result || this.trans('(Empty)'));
            });

            return this.isNew ? this.newTitle : editTitle;
        },

        isNew()
        {
            return Obj.get(this.value, ['data', this.newProp]);
        }

    },

    data()
    {
        let store = Data.get('NDataform:' +
            (this.use || this.showQuery), {});

        let defaults = {
            query: null,
            tab: 'default'
        };

        let override = {
            id: null,
            locale: this.$root.locale,
            tempLocale: this.$root.locale,
            override: null,
            value: null,
            errors: null,
            confirmEdit: false,
            confirmCopy: false,
            confirmLocale: false,
            loading: {},
            load: false,
            nextActions: []
        };

        return Obj.assign(defaults, store, override);
    },

    watch: {

        'id': function () {
            this.id && this.queryShow();
        }

    },

    mounted()
    {
        Event.bind('locale/changed', (locale) => {
            this.tempLocale = this.locale = locale;
        }, this._.uid);

        let changeEvent = () => {
            this.id && this.SxIndexController.emitChange();
        };

        this.$watch('value', changeEvent, { deep: true });
    },

    beforeUnmount()
    {
        Data.set('NDataform:' +
            (this.use || this.showQuery), this.$data);

        Event.unbind('locale/changed', this._.uid);
    },

    methods: {

        openModal(id)
        {
            this.id = id || UUID();
        },

        closeModal()
        {
            this.id = null;
        },

        eventReady()
        {
            if ( ! this.$refs.form ) {
                return Any.delay(this.eventReady, 100);
            }

            Any.delay(() => {
                this.$refs.tabs && this.$refs.tabs.getTab(true);
            }, 100);

            this.$refs.form.ctor('ready')();
        },

        setLoad(value, key)
        {
            let loading = Obj.set(this.loading, key, value);

            loading = Any.vals(this.loading);

            this.load = Arr.has(loading, true);
        },

        setOverride(override)
        {
            this.override = override;
        },

        queryDone(res, raw = false)
        {
            if ( raw ) {
                res = { data: res };
            }

            if ( ! this.value ) {
                Any.delay(this.eventReady, 100);
            }

            // Set value
            this.value = res.data;

            if ( Obj.get(this.value, ['data', this.newProp], false) ) {
                this.tab = 'default'
            }

            this.tempLocale = this.locale = Obj.get(res.data,
                ['data', this.localeProp], this.locale);

            // Store for changed comparison
            Any.delay(() => {
                this.SxIndexController.resetChange();
            }, 250);
        },

        queryRedirect(res)
        {
            this.$emit('redirect', { row: res.data.data });
        },

        queryClose()
        {
            this.SxIndexController.resetChange();
            this.$emit('close');
            this.id = null;
            this.tempLocale = this.$root.locale;
            this.value = null;
        },

        queryError(res)
        {
            this.errors = res.data.errors;
        },

        queryShow(override = {})
        {
            if ( this.id === true ) {
                return;
            }

            this.SxIndexController.resetChange();

            this.errors = {};

            let defaultValue = {
                data: Obj.clone(this.fallback)
            };

            if ( Any.isEmpty(this.showQuery) ) {
                return this.queryDone(defaultValue, true);
            }

            let query = Obj.assign(Obj.get(this.value, 'data', {}), override);

            if ( this.id ) {
                query = Obj.assign(query, { id: this.id })
            }

            if ( Any.isEmpty(this.override) === false ) {
                query = Obj.assign(query, this.override);
            }

            query[this.localeProp] = this.locale = this.tempLocale;

            let options = {
                onLoad: () => this.setLoad(true, this.showQuery),
                onDone: () => this.setLoad(false, this.showQuery)
            };

            Ajax.call(this.showQuery, true, query, options)
                .then(this.queryDone);
        },

        queryApply(override = {})
        {
            this.SxIndexController.resetChange();

            this.errors = {};

            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            if ( ! Any.isEmpty(this.override) && this.isNew ) {
                query = Obj.assign(query, this.override);
            }

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.editQuery, false, query, options)
                .then(this.queryDone, this.queryError);
        },

        querySave(override = {})
        {
            this.SxIndexController.resetChange();

            this.errors = {};

            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            if ( ! Any.isEmpty(this.override) && this.isNew ) {
                query = Obj.assign(query, this.override);
            }

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.editQuery, false, query, options)
                .then(this.queryClose, this.queryError);
        },

        queryCopy(override = {})
        {
            this.SxIndexController.resetChange();

            this.errors = {};

            let query = Obj.get(this.value, 'data', {});

            query = Obj.assign(query, { id: this.id }, override);

            query[this.localeProp] = Obj.get(this.value,
                ['data', this.localeProp], this.locale);

            let options = {
                onLoad: () => this.setLoad(true, this.editQuery),
                onDone: () => this.setLoad(false, this.editQuery)
            };

            Ajax.call(this.copyQuery, false, query, options)
                .then(this.queryRedirect, this.queryError);
        },

        closeItem()
        {
            if ( ! this.SxIndexController.hasChanged() ) {
                return this.queryClose();
            };

            Any.delay(() => this.confirmEdit = true, 100);
        },

        copyItem()
        {
            if ( ! this.SxIndexController.hasChanged() ) {
                return this.queryCopy();
            }

            Any.delay(() => this.confirmCopy = true, 100);
        },

        localizeItem()
        {
            if ( ! this.SxIndexController.hasChanged() ) {
                return this.queryShow();
            }

            Any.delay(() => this.confirmLocale = true, 100);
        }

    },

    provide()
    {
        return {
            NDataform: this
        };
    },

    renderSlot(slot)
    {
        if ( Any.isEmpty(this.$slots[slot]) ) {
            return null;
        }

        return (
            <div class={'n-dataform__toolbar-' + slot}>
                { this.$slots[slot]() }
            </div>
        );
    },

    renderApply()
    {
        return (
            <div class="n-dataform__toolbar-apply">
                <NButton type="success" onClick={() => this.queryApply()}>
                    { Locale.trans('Apply') }
                </NButton>
            </div>
        );
    },

    renderSave()
    {
        return (
            <div class="n-dataform__toolbar-save">
                <NButton type="primary" onClick={() => this.querySave()}>
                    { Locale.trans('Save') }
                </NButton>
            </div>
        );
    },

    renderClose()
    {
        return (
            <div class="n-dataform__toolbar-close">
                <NButton type="secondary" onClick={() => this.closeItem('button')}>
                    { Locale.trans('Close') }
                </NButton>
            </div>
        );
    },

    renderCopy()
    {
        if ( ! this.copyQuery ) {
            return null;
        }

        return (
            <div class="n-dataform__toolbar-copy">
                <NButton type="info" onClick={() => this.copyItem()}>
                    { Locale.trans('Copy') }
                </NButton>
            </div>
        );
    },

    renderNew()
    {
        return (
            <div class="n-dataform__toolbar-new">
                <NButton type="primary" onClick={() => this.$emit('new')}>
                    { Locale.trans('New') }
                </NButton>
            </div>
        );
    },

    renderSpacer()
    {
        return (
            <div class="n-dataform__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    renderLocalize()
    {
        if ( ! this.localized ) {
            return null;
        }

        return (
            <div class="n-dataform__locale">
                <NSelect ref="select" vModel={this.tempLocale} disabled={this.isNew} onUpdate:modelValue={() => this.localizeItem()}>
                    {
                        Arr.each(Build.getLocales(), (locale) => {
                            return <NSelectOption value={locale.key} label={locale.value} />
                        })
                    }
                </NSelect>
            </div>
        );
    },

    renderHeader()
    {

        return [
            <div class="n-dataform__title">
                <span>{ this.title }</span>
            </div>,
            this.ctor('renderLocalize')(),
            <div class="n-dataform__close" onClick={this.closeItem}>
                <span class="fa fa-times"></span>
            </div>
        ];
    },

    renderBody()
    {
        let props = {
            ref: 'form', value: Obj.get(this.value, 'data', {}), errors: this.errors || {}
        };

        let render = h(resolveComponent(this.use), props, this.$slots);

        let tabsProps = {
            class: 'n-dataform__tabs',
            modelValue: 'none',
            relative: false
        };

        if ( this.value ) {
            tabsProps.modelValue = this.tab;
        }

        tabsProps['onUpdate:modelValue'] = (value) => {
            this.tab = value;
        };

        return (
            <NTabs ref="tabs" {...tabsProps}>
                { ! this.use ? this.$slots.default(props) : render }
            </NTabs>
        );
    },

    renderFooter()
    {
        return (
            <div class="n-dataform__toolbar">
                {
                    Arr.each(this.layout, (render) => {
                        return this.ctor('render' + Str.ucfirst(render), this.ctor('renderSlot'))(render);
                    })
                }
            </div>
        );
    },

    render()
    {
        let slots = {
            raw: []
        };

        slots.raw[0] = (
            <NLoader class="n-dataform" visible={this.load || ! this.value}>
                <div class="n-dataform__header">
                    {this.ctor('renderHeader')()}
                </div>
                <div class="n-dataform__body">
                    {this.ctor('renderBody')()}
                </div>
                <div class="n-dataform__footer">
                    {this.ctor('renderFooter')()}
                </div>
            </NLoader>
        );

        let changeEditProps = {
            listen: false,
        };

        changeEditProps.onConfirm = () => {
            this.queryClose();
        };

        slots.raw[1] = (
            <NConfirm vModel={this.confirmEdit} {...changeEditProps}>
                { Locale.trans('Do you want to discard changes?') }
            </NConfirm>
        );

        let changeCopyProps = {
            listen: false,
        };

        changeCopyProps.onConfirm = () => {
            this.queryCopy();
        };

        slots.raw[2] = (
            <NConfirm vModel={this.confirmCopy} {...changeCopyProps}>
                { Locale.trans('Copy without saving? Changes will not be saved!') }
            </NConfirm>
        );

        let changeLocaleProps = {
            listen: false,
        };

        changeLocaleProps.onConfirm = () => {
            this.queryShow();
        };

        changeLocaleProps.onAbort = () => {
            this.tempLocale = this.locale;
        };

        slots.raw[3] = (
            <NConfirm vModel={this.confirmLocale} {...changeLocaleProps}>
                { Locale.trans('Change lanugage without saving?') }
            </NConfirm>
        );

        let modalProps = {
            modelValue: !! this.id,
            listen: false,
            update: false,
            type: this.type,
            height: this.height,
            width: this.width,
            onClose: this.closeItem
        }

        return (
            <NModal {...modalProps} v-slots={slots} />
        );
    }
}
