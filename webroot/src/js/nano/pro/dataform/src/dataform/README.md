# NDataform

```vue
<n-dataform></n-dataform>
```

### Properties

**id**  
required: true  
types: String, Number  
_ID to get entity_

**showQuery**  
default: null  
types: Array, String  
_Show query request (defined in Nano.Ajax)_

**editQuery**  
default: null  
types: Array, String  
_Delete query request (defined in Nano.Ajax)_

**copyQuery** (not final)  
default: null  
types: Array, String  
_Copy query request (defined in Nano.Ajax)_

**use**  
default: null  
types: String  
_Component which will be rendered instead of default scoped slot_

**localized**  
default: false  
types: Boolean  
_Shows language selector and modifies query_

**localeProp**  
default: '_locale'  
types: String  
_Locale prop which will be send with each request_

**newProp**  
default: '_new'  
types: String  
_New prop to identify if a row is new_

**newTitle**  
default: Nano.Locale.trans('Create')  
types: String  
_Title for creation / new entities_

**editTitle**  
default: Nano.Locale.trans('Edit')  
types: String  
_Title for edition / existing entities_

**layout**  
default: ['copy', 'spacer', 'close', 'apply', 'save']  
types: Array  
_All elements which will be rendered in footer_
