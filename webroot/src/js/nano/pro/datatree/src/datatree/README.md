# NDatatree

```vue
<n-datatree></n-datatree>
```

### Properties

**adaptHeight**  
default: null  
types: String, Element  
_Adapt height from target or just parent if true_

**indexQuery**  
default: null  
types: Array, String  
_Index query request (defined in Nano.Ajax)_

**deleteQuery**  
default: null  
types: Array, String  
_Delete query request (defined in Nano.Ajax)_

**moveQuery**  
default: null  
types: Array, String  
_Move query request (defined in Nano.Ajax)_

**use**  
default: null  
types: String  
_Component which will be rendered instead of default scoped slot (Draggable)_

**useBefore**  
default: null  
types: String  
_Component which will be rendered instead of before scoped slot (Non draggable)_

**useAfter**  
default: null  
types: String  
_Component which will be rendered instead of after scoped slot (Non draggable)_

**updateEvents**  
default: []  
types: Array  
_Nano.Events which will trigger a refresh_

**localeProp**  
default: '_locale'  
types: String  
_Locale prop which will be send with each request_

**uniqueProp**  
default: 'id'  
types: String  
_Unique prop to identify a row_

**itemHeight**  
default: 32  
types: Number  
_Row height to prevent scrollbar hopping while loading items in chunks_

**layout**  
default: ['search', 'custom', 'refresh']  
types: Array  
_All elements which will be rendered in header_

**group**  
default: ['default']  
types: Array  
_Draggable group_

**insertNode**  
default: true  
types: Boolean, Function  
_Determines if node will be added to list or just emits move_

**removeNode**  
default: true  
types: Boolean, Function  
_Determines if node will be removed from list or just emits move_

**allowDrag**  
default: (item, depth) => true  
types: Boolean, Function  
_Determines if node is draggable_

**allowDrop**  
default: (item, target, move, depth) => true  
types: Boolean, Function  
_Determines if node is droppable_
