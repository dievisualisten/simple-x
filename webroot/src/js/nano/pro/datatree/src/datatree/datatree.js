import { Str, Arr, Obj, Any, Dom, Locale, Data, Event } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

export default {

    name: 'SxDatatree',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        indexQuery: {
            default()
            {
                return null;
            }
        },

        deleteQuery: {
            default()
            {
                return null;
            }
        },

        moveQuery: {
            default()
            {
                return null;
            }
        },

        updateEvents: {
            default()
            {
                return [];
            },
            type: [Array]
        },

        localeProp: {
            default()
            {
                return '_locale';
            },
            type: [String]
        },

        adaptHeight: {
            default()
            {
                return null;
            }
        },

        layout: {
            default()
            {
                return ['search', 'custom', 'refresh'];
            },
            type: [Array]
        },

        use: {
            default()
            {
                return null;
            }
        },

        group: {
            default()
            {
                return ['default'];
            },
            type: [Array]
        },

        allowGroups: {
            default()
            {
                return ['default'];
            },
            type: [Array]
        },

        safezone: {
            default()
            {
                return (height) => height * 0.235;
            }
        },

        itemHeight: {
            default()
            {
                return 36;
            },
            type: [Number]
        },

        insertNode: {
            default()
            {
                return false;
            }
        },

        removeNode: {
            default()
            {
                return false;
            }
        },

        allowMove: {
            default()
            {
                return true;
            }
        },

        allowSelect: {
            default()
            {
                return true;
            }
        },

        allowDrag: {
            default()
            {
                return true;
            }
        },

        allowDrop: {
            default()
            {
                return true;
            }
        },

    },

    methods: {

        getValueState()
        {
            return this.value !== null && this.value !== undefined &&
                this.value !== false;
        },

        getStoreName()
        {
            let storeName = this.indexQuery;

            if ( this.query && ! Any.isEmpty(this.query.search) ) {
                storeName += ':searched';
            }

            return storeName;
        },

        setCurrent(...args)
        {
            this.$refs.draggable.setCurrent(...args);
        },

        bindObserver()
        {
            let element = this.adaptHeight !== true ?
                this.adaptHeight : this.$el.parentNode;

            Dom.find(element).observerResize(() => {
                this.height = Dom.find(element).height();
            })(element);
        },

        queryDone(res)
        {
            Data.set(this.getStoreName(), res.data);

            if ( Any.isEmpty(this.expanded) ) {
                this.expanded = Obj.get(res.data, 'expanded', []);
            }

            Event.fire(this.indexQuery);

            this.value = res.data;
        },

        queryIndex(override = {})
        {
            let query = Obj.assign({}, this.query || {});

            query[this.localeProp] = this.locale =
                this.$root.locale;

            let extra = {
                search: this.search
            };

            this.query = Obj.assign(query, extra, override);

            if ( Any.isEmpty(query.search) ) {
                delete query.search;
            }

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.indexQuery, true, query, options)
                .then(this.queryDone);
        },

        queryMove(source, target, position)
        {
            if ( ! Any.isArray(source) ) {
                source = [source];
            }

            let query = {
                source_id: source.join(','), target_id: target, position: position
            };

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.moveQuery, false, query, options)
                .then(() => {}, () => this.queryIndex());
        },

        queryDelete(override = {})
        {
            let query = {
                ids: this.selected
            };

            if ( ! Any.isEmpty(this.$root.locale) ) {
                query[this.localeProp] = this.locale = this.$root.locale;
            }

            query = Obj.assign(query, override);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.deleteQuery, false, query, options)
                .then(() => this.queryIndex());
        },

        deleteItem(item)
        {
            this.selected = item;
            this.confirm = true;
        }

    },

    provide()
    {
        return {
            NDatatree: this
        };
    },

    data()
    {
        let store = Data.get('NDatatree:' + this.indexQuery, {});

        let defaults = {
            query: null,
            locale: this.$root.locale,
            search: '',
            expanded: [],
            current: null,
        };

        let override = {
            value: null,
            height: null,
            load: false,
            confirm: false,
            selected: null
        };

        return Obj.assign(defaults, store, override);
    },

    beforeMount()
    {
        if ( ! Data.has(this.getStoreName()) ) {
            Data.set(this.getStoreName(), false);
        }
    },

    mounted()
    {
        Event.bind(this.updateEvents,
            () => this.queryIndex(), this._.uid);

        Any.delay(this.ctor('delayedMount'));
    },

    delayedMount()
    {
        this.value = Data.get(this.indexQuery, null);

        if ( ! this.getValueState() || this.$root.locale !== this.locale ) {
            this.queryIndex();
        }

        this.$watch('search', () => {
            this.$nextTick(() => this.queryIndex());
        });
    },

    updated()
    {
        Data.set('NDatatree:' + this.indexQuery, this.$data);
    },

    beforeUnmount()
    {
        Event.unbind(this.updateEvents, this._.uid);
    },

    renderSlot(slot)
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( Any.isEmpty(this.$slots[slot]) ) {
            return null;
        }

        return (
            <div class={'n-datatree__toolbar-' + slot}>
                { this.$slots[slot]() }
            </div>
        );
    },

    renderSearch()
    {
        let props = {
            modelValue: this.search,
            placeholder: Locale.trans('Search'),
            icon: 'fa fa-times',
            iconDisabled: Any.isEmpty(this.search)
        };

        props['onKeydown'] = (event) => {
            if ( event.which === 13 ) {
                this.search = event.target.value;
            }
        };

        props['onIconClick'] = (event) => {
            this.search = '';
        };

        return (
            <div class="n-datatree__toolbar-search">
                <NInput {...props} />
            </div>
        );
    },

    renderRefresh()
    {
        let props = {
            icon: 'fa fa-sync',
            square: true,
        };

        props.onClick = () => {
            this.queryIndex();
        };

        return (
            <div class="n-datatable__toolbar-refresh">
                <NButton {...props} />
            </div>
        );
    },

    renderSpacer()
    {
        return (
            <div class="n-datatree__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    renderHeader()
    {
        return (
            <div class="n-datatree__toolbar toolbar">
                {
                    Arr.each(this.layout, (render) => {
                        return this.ctor('render' + Str.ucfirst(render),
                            this.ctor('renderSlot'))(render);
                    })
                }
            </div>
        );
    },

    renderBody()
    {
        let props = {
            items: Obj.get(this.value, 'data', []),
            renderNode: this.use,
            group: this.group,
            allowGroups: this.allowGroups,
            // disableMove: true,
            renderExpand: true,
            itemHeight: this.itemHeight,
            insertNode: this.insertNode,
            removeNode: this.removeNode,
            allowDrag: this.allowDrag,
            safezone: this.safezone,
            allowDrop: this.allowDrop,
            current: this.current,
            expanded: this.expanded,
            onMove: (...args) => this.$emit('move', ...args),
            onMoveraw: (...args) => this.$emit('moveraw', ...args),
            onRowClick: (...args) => this.$emit('row-click', ...args),
        };

        Obj.assign(props, {
            'onUpdate:current': (value) => {
                this.current = value;
                this.$emit('update:current', value);
            },
            'onUpdate:expanded': (value) => {
                this.expanded = value;
                this.$emit('update:expanded', value);
            }
        });

        if ( this.moveQuery && this.allowMove ) {
            props.onMove = this.queryMove;
        }

        return <NDraglist ref="draggable" {...props} v-slots={this.$slots} />;
    },

    render()
    {
        let style = {};

        if ( Any.isNull(this.height) === false ) {
            style.height = this.height + 'px';
        }

        return (
            <NLoader class="n-datatree" visible={this.load || ! this.getValueState()} style={style}>
                <div class="n-datatree__header">
                    { this.ctor('renderHeader')() }
                </div>
                <div class="n-datatree__body">
                    { this.ctor('renderBody')() }
                </div>
                <NConfirm type="danger" vModel={this.confirm} onConfirm={() => this.queryDelete()} listen={false}>
                    { Locale.trans('Do you really want to delete this item?') }
                </NConfirm>
            </NLoader>
        );
    }
}
