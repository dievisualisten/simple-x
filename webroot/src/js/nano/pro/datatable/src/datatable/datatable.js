import { Str, Arr, Obj, Any, Data, Event, Locale } from "@kizmann/pico-js";
import { Ajax } from "@dievisualisten/simplex";

let defaultImportForm = {
    'NFormItem:0': {
        props: {
            label: Locale.trans('File'),
            prop: 'file'
        },
        content: {
            'NFile:0': {
                model: {
                    path: 'file'
                }
            }
        }
    },
    'NFormItem:1': {
        props: {
            label: Locale.trans('Replace'),
            prop: 'replace'
        },
        content: {
            'NSwitch:0': {
                model: {
                    path: 'replace'
                },
                content: [
                    Locale.trans('Replace existing items')
                ]
            }
        }
    }
};

export default {

    name: 'NDatatable',

    inject: {

        SxIndexController: {
            default: undefined
        }

    },

    props: {

        forceReload: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        storeData: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        current: {
            default()
            {
                return null;
            }
        },

        override: {
            default()
            {
                return {};
            },
            type: [Object]
        },

        indexQuery: {
            default()
            {
                return null;
            }
        },

        deleteQuery: {
            default()
            {
                return null;
            }
        },

        copyQuery: {
            default()
            {
                return null;
            }
        },

        importQuery: {
            default()
            {
                return null;
            }
        },

        importConfig: {
            default()
            {
                return defaultImportForm;
            }
        },

        exportUrl: {
            default()
            {
                return null;
            }
        },

        updateEvents: {
            default()
            {
                return [];
            },
            type: [Array]
        },

        showNew: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        showDelete: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },

        showCopy: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        copyMessage: {
            default()
            {
                return 'Do you really want to copy :count item?|Do you really want to copy :count items?';
            },
            type: [String]
        },

        deleteMessage: {
            default()
            {
                return 'Do you really want to delete :count item?|Do you really want to delete :count items?';
            },
            type: [String]
        },

        actionsWidth: {
            default()
            {
                return 90;
            },
            type: [Number]
        },

        localeProp: {
            default()
            {
                return '_locale';
            },
            type: [String]
        },

        uniqueProp: {
            default()
            {
                return 'id';
            },
            type: [String]
        },

        virtualProp: {
            default()
            {
                return '_virtual';
            },
            type: [String]
        },

        itemHeight: {
            default()
            {
                return 40;
            },
            type: [Number]
        },

        adaptHeight: {
            default()
            {
                return null;
            }
        },

        page: {
            default()
            {
                return 1;
            },
            type: [Number]
        },

        limit: {
            default()
            {
                return 50;
            },
            type: [Number]
        },

        sortProp: {
            default()
            {
                return 'modified';
            },
            type: [String]
        },

        sortDir: {
            default()
            {
                return 'desc';
            },
            type: [String]
        },

        filterProps: {
            default()
            {
                return [];
            },
            type: [Array]
        },

        layout: {
            default()
            {
                return [
                    'search', 'new', 'copy', 'delete', 'import', 'custom', 'spacer', 'reset', 'refresh'
                ];
            },
            type: [Array]
        },

        group: {
            default()
            {
                return ['default'];
            },
            type: [Array]
        },

        insertNode: {
            default()
            {
                return false;
            }
        },

        removeNode: {
            default()
            {
                return false;
            }
        },

        allowDrag: {
            default()
            {
                return false;
            }
        },

        allowDrop: {
            default()
            {
                return false;
            }
        },

        renderExpand: {
            default()
            {
                return false;
            },
            type: [Boolean]
        },

        renderFooter: {
            default()
            {
                return true;
            },
            type: [Boolean]
        },


        threshold: {
            default()
            {
                return 100;
            },
            type: [Number]
        },

        loadingDelay: {
            default()
            {
                return 0;
            },
            type: [Number]
        },

        loadingMax: {
            default()
            {
                return 1250;
            },
            type: [Number]
        },

        loadingMin: {
            default()
            {
                return 450;
            },
            type: [Number]
        },

    },

    computed: {

        sort()
        {
            return [
                { property: this.veSortProp, direction: this.veSortDir }
            ];
        },

        filter()
        {
            return Arr.filter(this.veFilterProps, (filter) => {
                return ! Any.isEmpty(filter.value);
            });
        }

    },

    methods: {

        getValueState()
        {
            return this.value !== null && this.value !== undefined &&
                this.value !== false;
        },

        getStoreName()
        {
            let storeName = this.indexQuery;

            if ( this.query && ! Any.isEmpty(this.query.search) ) {
                storeName += ':searched';
            }

            return storeName;
        },

        clickRow(props)
        {
            if ( ! Obj.get(props.item, this.virtualProp) ) {
                this.$emit('row-click', props);
            }
        },

        editRow(props)
        {
            if ( ! Obj.get(props.item, this.virtualProp) ) {
                this.$emit('row-dblclick', props.item);
            }
        },

        setCurrent(...args)
        {
            this.$refs.table.setCurrent(...args);
        },

        bindObserver()
        {
            // let element = Dom.find(this.$el).parent().get(0);
            //
            // if ( this.adaptHeight !== true ) {
            //     element = this.adaptHeight;
            // }
            //
            // Dom.find(element).observerResize((el) => {
            //     this.height = Dom.find(el).innerHeight();
            // })(element);
        },

        queryDone(res)
        {
            if ( this.$refs.table ) {
                this.$refs.table.$refs.draggable.unselectAll();
            }

            if ( this.storeData ) {
                Data.set(this.getStoreName(), res.data);
            }

            Event.fire(this.indexQuery);

            if ( Any.isEmpty(res.data.data) ) {
                res.data.data = [];
            }

            if ( this.preChangeState !== (this.preChangeState = false) ) {
                // this.$refs.table.scrollTo(0);
            }

            this.value = res.data;
        },

        queryIndex(override = {})
        {
            let query = Obj.assign({}, this.query || {});

            query[this.localeProp] = this.locale =
                this.$root.locale;

            let extra = {
                search: this.search,
                sort: this.sort,
                filter: this.filter,
                page: this.nativePage,
                limit: this.nativeLimit,
            };

            this.query = Obj.assign(query, extra, this.override, override);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.indexQuery, false, query, options)
                .then(this.queryDone);
        },

        queryDelete(override = {})
        {
            let query = {
                ids: this.veSelected.join(',')
            };

            if ( Any.isEmpty(this.$root.locale) === false ) {
                query[this.localeProp] = this.locale = this.$root.locale;
            }

            query = Obj.assign(query, override);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.deleteQuery, false, query, options)
                .then(() => this.queryIndex());
        },

        queryCopy(override = {})
        {
            let query = {
                ids: this.veSelected.join(',')
            };

            if ( Any.isEmpty(this.$root.locale) === false ) {
                query[this.localeProp] = this.locale = this.$root.locale;
            }

            query = Obj.assign(query, override);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.copyQuery, false, query, options)
                .then(() => this.queryIndex());
        },

        queryImport(override = {})
        {
            let query = Obj.assign(this.importForm, override);

            let options = {
                onLoad: () => this.load = true,
                onDone: () => this.load = false
            };

            Ajax.call(this.importQuery, false, query, options)
                .then(() => this.queryIndex());
        },

        updateSearch()
        {
            this.preChangeState = true;

            this.queryIndex({ page: this.nativePage = 1 });
        },

        updateFilters()
        {
            this.preChangeState = true;

            this.queryIndex({ page: this.nativePage = 1 });
        },

        updatePaginate()
        {
            this.preChangeState = true;

            this.queryIndex();
        },

        resetFilters()
        {
            this.$refs.table.resetFilter();
        },

        copyItems(keys = null)
        {
            if ( keys !== null ) {
                this.veSelected = keys;
            }

            this.confirmCopy = true;
        },

        deleteItems(keys = null)
        {
            if ( keys !== null ) {
                this.veSelected = keys;
            }

            this.confirmDelete = true;
        }

    },

    data()
    {
        let store = Data.get('NDatatable:' + this.indexQuery, {});

        let defaults = {
            query: null,
            locale: this.$root.locale,
            search: '',
            currentKey: null,
            veSelected: [],
            veExpanded: [],
            visibleColumns: [],
            nativePage: this.page,
            nativeLimit: this.limit,
            veSortProp: this.sortProp,
            veSortDir: this.sortDir,
            veFilterProps: this.filterProps
        };

        let override = {
            value: null,
            height: null,
            load: false,
            confirmCopy: false,
            confirmDelete: false,
        };

        override.importForm = {
            file: null, replace: false
        };

        return Obj.assign(defaults, store, override);
    },

    beforeMount()
    {
        if ( ! Data.has(this.getStoreName()) ) {
            Data.set(this.getStoreName(), false);
        }
    },

    mounted()
    {
        if ( this.adaptHeight !== null ) {
            this.$nextTick(this.bindObserver);
        }

        Event.bind(this.updateEvents, () => this.queryIndex(), this._.uid);

        Any.delay(this.ctor('delayedMount'));
    },

    delayedMount()
    {
        this.value = Data.get(this.indexQuery, null);

        if ( ! this.getValueState() || this.forceReload || this.$root.locale !== this.locale ) {
            this.queryIndex();
        }

        this.$watch('search', () => this.updateSearch());
        this.$watch('veSortDir', () => this.queryIndex());
        this.$watch('veSortProp', () => this.queryIndex());
        this.$watch('veFilterProps', () => this.queryIndex());
    },

    updated()
    {
        Data.set('NDatatable:' + this.indexQuery, this.$data);
    },

    beforeUnmount()
    {
        Event.unbind(this.updateEvents, this._.uid);
    },

    renderSlot(slot)
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        let slotHtml = null;

        if ( this.$slots[slot] ) {
            slotHtml = this.$slots[slot]();
        }

        return (
            <div class={'n-datatable__toolbar-' + slot}>
                { slotHtml }
            </div>
        );
    },

    renderSearch()
    {
        let props = {
            modelValue: this.search,
            placeholder: Locale.trans('Search'),
            icon: 'fa fa-times',
            iconDisabled: Any.isEmpty(this.search)
        };

        props['onKeydown'] = (event) => {
            if ( event.which === 13 ) {
                this.search = event.target.value;
            }
        };

        props['onIconClick'] = () => {
            this.search = '';
        };

        return (
            <div class="n-datatable__toolbar-search">
                <NInput {...props} />
            </div>
        );
    },

    renderNew()
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( this.showNew !== true ) {
            return null;
        }

        return (
            <div class="n-datatable__toolbar-new">
                <NButton type="primary" icon="fa fa-file" onClick={() => this.$emit('new-click')}>
                    { Locale.trans('New') }
                </NButton>
            </div>
        );
    },

    renderCopy()
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( this.showCopy !== true ) {
            return null;
        }

        return (
            <NButton type="info" icon="fa fa-copy" disabled={this.veSelected.length === 0} onClick={() => this.copyItems()}>
                { Locale.trans('Copy') }
            </NButton>
        );
    },

    renderDelete()
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( this.showDelete !== true ) {
            return null;
        }

        return (
            <NButton type="danger" icon="fa fa-trash" disabled={this.veSelected.length === 0} onClick={() => this.deleteItems()}>
                { Locale.trans('Remove') }
            </NButton>
        );
    },

    renderRefresh()
    {
        return (
            <NButton icon="fa fa-sync" square={true} onClick={() => this.queryIndex()} />
        );
    },

    renderReset()
    {
        return (
            <NButton icon="fa fa-eraser" disabled={! this.$refs.table ||this.filter.length === 0} onClick={this.resetFilters}>
                { Locale.trans('Reset') }
            </NButton>
        );
    },

    renderImport()
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( ! this.importQuery && ! this.exportUrl ) {
            return null;
        }

        return (
            <div class="n-datatable__toolbar-import">
                <NButtonGroup>
                    { this.ctor('renderImportButton')() }
                    { this.ctor('renderExportButton')() }
                </NButtonGroup>

            </div>
        );
    },

    renderExportButton()
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        if ( ! this.exportUrl ) {
            return null;
        }

        return (
            <NButton type="secondary" icon="fa fa-cloud-download" onClick={() => window.open(this.exportUrl)}>
                { Locale.trans('Export') }
            </NButton>
        );
    },

    renderImportButton()
    {
        if ( ! this.importQuery ) {
            return null;
        }

        let importButton = (
            <NButton type="secondary" icon="fa fa-cloud-upload">
                { Locale.trans('Import') }
            </NButton>
        );

        let importModal = (
            <NModal ref="importModal" width="50%" title={Locale.trans('Import')}>
                <NLoader visible={this.load}>
                    <NForm form={this.importForm}>
                        {
                            Arr.each(this.importConfig, (config, key) => {
                                return (
                                    <NConfig value={this.importForm} config={{ [key]: config }} scope={this} />
                                );
                            })
                        }
                    </NForm>
                </NLoader>
                <div class="grid grid--row" slot="footer">
                    <NButton class="col--auto col--left" type="secondary" disabled={this.load} onClick={() => this.$refs.importModal.close()}>
                        { Locale.trans('Abort') }
                    </NButton>
                    <NButton class="col--auto col--right" type="primary" disabled={this.load} onClick={() => this.queryImport()}>
                        { Locale.trans('Import file') }
                    </NButton>
                </div>
            </NModal>
        );

        return [
            importButton, importModal
        ];
    },

    renderSpacer()
    {
        return (
            <div class="n-datatable__toolbar-spacer">
                <span></span>
            </div>
        );
    },

    renderHeader()
    {
        return (
            <div class="n-datatable__toolbar toolbar">
                {
                    Arr.each(this.layout, (render) => {
                        return this.ctor('render' + Str.ucfirst(render),
                            this.ctor('renderSlot'))(render);
                    })
                }
            </div>
        );
    },

    renderActions(props)
    {
        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            return null;
        }

        let unique = Obj.get(props.value, this.uniqueProp);
        let virtual = Obj.get(props.value, this.virtualProp);

        let editButtonProps = {
            size: 'xs',
            type: 'primary',
            square: true,
            icon: 'fa fa-pencil'
        };

        let editRow = () => {
            this.$emit('row-dblclick', props.item);
        };

        let editButton = (
            <NButton {...editButtonProps} onClick={editRow}></NButton>
        );

        if ( virtual ) {
            editButton = null;
        }

        let deleteButtonProps = {
            size: 'xs',
            type: 'danger',
            square: true,
            icon: 'fa fa-trash'
        };

        let deleteButton = (
            <NButton {...deleteButtonProps} onClick={() => this.deleteItems([unique])}></NButton>
        );

        if ( virtual ) {
            deleteButton = null;
        }


        let slot = null;

        if ( this.$slots.actions ) {
            slot = this.$slots.actions(props);
        }

        return (
            <div class="n-datatable__actions toolbar">
                { [slot, editButton, deleteButton] }
            </div>
        );
    },

    renderBody()
    {
        let props = {
            items: Obj.get(this.value, 'data', {}),
            group: this.group,
            uniqueProp: this.uniqueProp,
            insertNode: this.insertNode,
            removeNode: this.removeNode,
            allowDrag: this.allowDrag,
            allowDrop: this.allowDrop,
            renderExpand: this.renderExpand,
            threshold: this.threshold,
            ghostMode: true,
            viewportHeight: true,
            sortOnLabel: true,
            closeFilterOnEnter: true,
            scrollTopOnChange: false,
            itemHeight: this.itemHeight,
            sortProp: this.veSortProp,
            sortDir: this.veSortDir,
            filter: this.veFilterProps,
            expanded: this.veExpanded,
            selected: this.veSelected,
            visibleColumns: this.visibleColumns,
            keyDebounce: 90,
            loadingInit: 0
        };

        Obj.assign(props, {
            'onRowClick': this.clickRow,
            'onRowDblclick': this.editRow,
            'onUpdate:current': (value) => this.$emit('update:current', value),
            'onUpdate:visibleColumns': (value) => this.visibleColumns = value,
            'onUpdate:selected': (value) => this.veSelected = value,
            'onUpdate:sortProp': (value) => this.veSortProp = value,
            'onUpdate:sortDir': (value) => this.veSortDir = value,
            'onUpdate:filter': (value) => this.veFilterProps = value,
        });

        let actionHtml = (
            <NTableColumn label={Locale.trans('Actions')} align="center" fixedWidth={this.actionsWidth}>
                {{ default: this.ctor('renderActions') }}
            </NTableColumn>
        );

        if ( this.SxIndexController && this.SxIndexController.disableEdit ) {
            actionHtml = null;
        }

        return (
            <NTable ref="table" {...props}>
                { this.$slots.default && this.$slots.default() } { actionHtml }
            </NTable>
        );
    },

    renderFooter()
    {
        let events = Obj.assign({}, {
            'onPaginate': this.updatePaginate,
            'onUpdate:page': (value) => this.nativePage = value,
            'onUpdate:limit': (value) => this.nativeLimit = value
        });

        return (
            <NPaginator ref="paginator" page={this.nativePage} limit={this.nativeLimit} total={Obj.get(this.value, 'total', 0)} {...events} />
        )
    },

    render()
    {
        let style = {};

        if ( this.height > 0 ) {
            style.height = this.height + 'px';
        }

        return (
            <NLoader class="n-datatable" visible={this.load || ! this.getValueState()} style={style}>
                <div ref="header" class="n-datatable__header">
                    { this.ctor('renderHeader')() }
                </div>
                <div ref="body" class="n-datatable__body">
                    { this.ctor('renderBody')() }
                </div>
                { this.renderFooter &&
                <div ref="footer" class="n-datatable__footer">
                    {this.ctor('renderFooter')()}
                </div>
                }
                <NConfirm vModel={this.confirmCopy} listen={false} type="info" onConfirm={() => this.queryCopy()}>
                    { Locale.choice(this.copyMessage, this.veSelected.length) }
                </NConfirm>
                <NConfirm vModel={this.confirmDelete} listen={false} type="danger" onConfirm={() => this.queryDelete()}>
                    { Locale.choice(this.deleteMessage, this.veSelected.length) }
                </NConfirm>
            </NLoader>
        );
    }
}
