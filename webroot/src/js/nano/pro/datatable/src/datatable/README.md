# NDatatable

```vue
<n-datatable></n-datatable>
```

### Properties

**adaptHeight**  
default: null  
types: String, Element  
_Adapt height from target or just parent if true_

**indexQuery**  
default: null  
types: Array, String  
_Index query request (defined in Nano.Ajax)_

**deleteQuery**  
default: null  
types: Array, String  
_Delete query request (defined in Nano.Ajax)_

**importQuery**  
default: null  
types: Array, String  
_Import query request (defined in Nano.Ajax)_

**exportUrl**  
default: null  
types: Array, String  
_Export url (defined in Nano.Route)_

**override**  
default: {}  
types: Object  
_Override index query_

**updateEvents**  
default: []  
types: Array  
_Nano.Events which will trigger a refresh_

**showNew**  
default: true  
types: Boolean  
_If create new button will be shown_

**showDelete**  
default: true  
types: Boolean  
_If delete item button will be shown_

**localeProp**  
default: '_locale'  
types: String  
_Locale prop which will be send with each request_

**uniqueProp**  
default: 'id'  
types: String  
_Unique prop to identify a row_

**sortProp**  
default: 'modified'  
types: String  
_Default sort property_

**sortDir**  
default: 'desc'  
types: String  
_Default sort direction_

**filterProps**  
default: []  
types: Array  
_Default filter properties_

**itemHeight**  
default: 40  
types: Number  
_Row height to prevent scrollbar hopping while loading items in chunks_

**current**  
default: null  
types: Object  
_Current selected item_

**page**  
default: 1  
types: Number  
_Default page_

**limit**  
default: 50  
types: Number  
_Default limit_

**layout**  
default: ['search', 'new', 'delete', 'import', 'custom', 'spacer', 'reset', 'refresh']  
types: Array  
_All elements which will be rendered in header_

**group**  
default: ['default']  
types: Array  
_Draggable group_

**insertNode**  
default: true  
types: Boolean, Function  
_Determines if node will be added to list or just emits move_

**removeNode**  
default: true  
types: Boolean, Function  
_Determines if node will be removed from list or just emits move_

**allowDrag**  
default: (item, depth) => true  
types: Boolean, Function  
_Determines if node is draggable_

**allowDrop**  
default: (item, target, move, depth) => true  
types: Boolean, Function  
_Determines if node is droppable_
