import Ajax from "./library/ajax";
import Asset from "./library/asset";
import Auth from "./library/auth";
import Data from "./library/data";
import Form from "./library/form";
import Element from "./library/element";
import Event from "./library/event";
import Extension from "./library/extension";
import Locale from "./library/locale";
import Queue from "./library/queue";
import Route from "./library/route";
import Store from "./library/store";

export {
    Ajax, Asset, Auth, Data, Form, Element, Event, Extension, Locale, Queue, Route, Store
};

export const Nano = {
    Ajax, Asset, Auth, Data, Form, Element, Event, Extension, Locale, Queue, Route, Store
};


import ReadyElement from 'nano-js/src/element/ready';
Nano.Element.alias('ready', ReadyElement);

export default Nano;
