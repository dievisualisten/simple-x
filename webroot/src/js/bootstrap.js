window._ = require("lodash");

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require("jquery");

    require("velocity-animate");
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require("axios");

/**
 * Froala editor.
 */

window.FroalaEditor = require("froala-editor");

require("froala-editor/js/plugins/align.min.js");
require("froala-editor/js/plugins/char_counter.min.js");
require("froala-editor/js/plugins/code_beautifier.min.js");
require("froala-editor/js/plugins/code_view.min.js");
require("froala-editor/js/plugins/colors.min.js");
require("froala-editor/js/plugins/draggable.min.js");
require("froala-editor/js/plugins/entities.min.js");
require("froala-editor/js/plugins/file.min.js");
require("froala-editor/js/plugins/font_family.min.js");
require("froala-editor/js/plugins/font_size.min.js");
require("froala-editor/js/plugins/fullscreen.min.js");
require("froala-editor/js/plugins/help.min.js");
require("froala-editor/js/plugins/image.min.js");
require("froala-editor/js/plugins/image_manager.min.js");
require("froala-editor/js/plugins/inline_class.min.js");
require("froala-editor/js/plugins/line_breaker.min.js");
require("froala-editor/js/plugins/line_height.min.js");
require("froala-editor/js/plugins/link.min.js");
require("froala-editor/js/plugins/lists.min.js");
require("froala-editor/js/plugins/paragraph_format.min.js");
require("froala-editor/js/plugins/paragraph_style.min.js");
require("froala-editor/js/plugins/print.min.js");
require("froala-editor/js/plugins/quick_insert.min.js");
require("froala-editor/js/plugins/quote.min.js");
require("froala-editor/js/plugins/special_characters.min.js");
require("froala-editor/js/plugins/table.min.js");
require("froala-editor/js/plugins/url.min.js");
require("froala-editor/js/plugins/video.min.js");
require("froala-editor/js/plugins/word_paste.min.js");

/**
 * Vue
 */
const Vue = require('vue/dist/vue.esm-bundler');
global.Vue = Vue;

/**
 * Vue Router
 */
const VueRouter = require('vue-router/dist/vue-router.esm-bundler');
global.VueRouter = VueRouter;

/**
 * Pico
 */
import { Pico } from "@kizmann/pico-js";
global.pi = Pico;

/**
 * Nano
 */
import { Nano } from '@kizmann/nano-ui';
global.nano = Nano;


/**
 * SX Library
 */
import { Simplex } from "./simplex/index";
global.sx = Simplex;


require("./app/app");


