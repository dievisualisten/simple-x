const path = require("path");
const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { VueLoaderPlugin } = require('vue-loader');

new webpack.DefinePlugin({
    __VUE_OPTIONS_API__: false,
    __VUE_PROD_DEVTOOLS__: false,
});

let config = {
    entry: ["./src/js/bootstrap.js"],
    module: {
        rules: [
            {
                test: /.jsx?$/,
                include: [
                    path.resolve('src/js'),
                    path.resolve('node_modules/@kizmann/pico-js/src'),
                    path.resolve('node_modules/@kizmann/nano-ui/src'),
                ],
                loader: 'babel-loader',
                options: {
                    configFile: path.resolve(__dirname, 'babel.config.js')
                },
            },
            {
                test: /\.vue$/,
                include: [
                    path.resolve('src/js'),
                ],
                use: ['vue-loader']
            }
        ],
    },
    resolve: {
        alias: {
            '@dievisualisten/simplex$': path.resolve(__dirname, 'src/js/simplex/index.js'),
            'froala-editor$': path.resolve(__dirname, 'node_modules/froala-editor/js/froala_editor.pkgd.min.js'),
        },
        extensions: [
            '.js', '.jsx', '.vue'
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/vendor.css'
        }),
        new CopyPlugin([
            { from: '**/*', to: 'img/', context: 'src/img/' },
            { from: '**/*', to: 'fonts/', context: 'node_modules/@fortawesome/fontawesome-pro/webfonts/' },
        ]),
        new VueLoaderPlugin()
    ]
};

let style = {
    entry: ["./src/sass/bootstrap.scss"],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader?url=false', 'postcss-loader', 'sass-loader'
                ]
            }
        ],

    },
    resolve: {
        alias: {
            '@simplex': path.resolve(__dirname, 'src')
        },
        extensions: ['.scss', '.css']
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/index.css'
        }),
    ]
};

module.exports = function (env, argv) {

    config.mode = argv.mode;

    if ( argv.mode === 'development' ) {
        config.devtool = 'eval-source-map';
    }

    if ( argv.mode === 'production' ) {
        config.devtool = 'source-map';
    }

    /**
     * @const __dirname
     */

    let globalPackage = Object.assign({

        output: {
            publicPath: '/simple-x/dist/js/',
            filename: 'js/index.js',
            path: path.resolve(__dirname, 'dist'),
        }

    }, config);

    let stylePackage = Object.assign({

        output: {
            filename: ".ignore.js",
            path: path.resolve(__dirname, 'dist'),
        }

    }, style);

    if ( argv.mode === 'development' ) {
        return [globalPackage, stylePackage];
    }

    let loaderOptions = new webpack.LoaderOptionsPlugin({
        minimize: true
    });

    globalPackage.plugins.push(loaderOptions);
    stylePackage.plugins.push(loaderOptions);

    let terserOptions = {
        mangle: true
    };

    let terser = new TerserPlugin({
        terserOptions, extractComments: false,
    });

    let optimization = {
        minimize: true, minimizer: []
    };

    optimization.minimizer.push(terser);

    globalPackage.optimization = optimization;
    stylePackage.optimization = optimization;

    return [globalPackage, stylePackage];
}
