<?php

namespace App;

use App\Library\Facades\Article;
use App\Library\Facades\Language;
use App\Library\Facades\Logger;
use App\Library\Facades\Mailer;
use App\Library\Facades\Menu;
use App\Library\Facades\Request;
use App\Library\Facades\Search;
use App\Policy\RequestPolicy;
use App\Routing\SxRouter;
use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Identifier\IdentifierInterface;
use Authentication\Middleware\AuthenticationMiddleware;
use Authorization\AuthorizationService;
use Authorization\AuthorizationServiceInterface;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Middleware\RequestAuthorizationMiddleware;
use Authorization\Policy\MapResolver;
use Cake\Event\EventManagerInterface;
use Cake\Http\BaseApplication;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Http\ServerRequest;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Console\CommandCollection;
use App\Command\CrontasksCommand;
use App\Error\Middleware\ErrorHandlerMiddleware;
use App\Maintenance\Middleware\MaintenanceHandlerMiddleware;
use App\Library\Middleware\SxSetupMiddleware;
use Cake\Routing\RouteBuilder;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class SxApplication extends BaseApplication implements AuthenticationServiceProviderInterface, AuthorizationServiceProviderInterface
{
    protected $baseDir = '/';

    protected $routesInitialized = false;

    /**
     * {@inheritDoc}
     */
    public function __construct($configDir, EventManagerInterface $eventManager = null)
    {
        $this->baseDir = dirname($configDir);

        parent::__construct($configDir, $eventManager);
    }

    /**
     * {@inheritDoc}
     */
    public function bootstrap(): void
    {
        $this->bootstrapCli();

        /**
         * Cakephp auth plugin
         */
        $this->addPlugin('Authentication');

        /**
         * Upload plugin for cake
         */
        $this->addPlugin('Proffer', [
            'bootstrap' => false,
        ]);

        /**
         * Soft delete support for cake
         */
        $this->addPlugin('Muffin/Trash', [
            'bootstrap' => false,
        ]);

        /**
         * Extended support for cake mails
         */
//        $this->addPlugin('Gourmet/Email', [
//            'bootstrap' => false,
//        ]);

        /**
         * Duplicatable behavior for cake
         */
        $this->addPlugin('Duplicatable', [
            'bootstrap' => false,
        ]);

        /**
         * Dunno
         */
        $this->addPlugin('Tools', [
            'bootstrap' => true,
        ]);



        if ( file_exists($this->baseDir . DS . 'Config' . DS . 'bootstrap.php') ) {
            require_once $this->baseDir . DS . 'Config' . DS . 'bootstrap.php';
        }

        if ( file_exists($this->baseDir . DS . 'Bootstrap' . DS . 'bootstrap.php') ) {
            require_once $this->baseDir . DS . 'Bootstrap' . DS . 'bootstrap.php';
        }

        if ( config('debugbar') === true && config('env') === 'local' ) {

            /*
             * Only try to load DebugKit in development mode
             * Debug Kit should not be installed on a production system
             */
            $this->addPlugin('DebugKit', [
                'bootstrap' => true,
                'ignoreAuthorization' => true,
            ]);

        }

        $this->addPlugin('WyriHaximus/MinifyHtml', [
            'bootstrap' => true,
        ]);

    }

    protected function bootstrapCli()
    {
        $this->addPlugin('Migrations');
        $this->addPlugin('Bake');
    }

    /**
     * {@inheritDoc}
     */
    public function middleware(MiddlewareQueue $middleware): MiddlewareQueue
    {
        $middleware->add(
            new SxSetupMiddleware()
        );

        // Handle plugin/theme assets like CakePHP normally does
        $middleware->add(
            new AssetMiddleware()
        );

        // Replace ErrorHandlerMiddleware with
        $middleware->add(
            new ErrorHandlerMiddleware()
        );

        // Parse body for any type
        $middleware->add(
            new BodyParserMiddleware()
        );

        // Add the middleware to the middleware queue
        $middleware->add(
            new AuthenticationMiddleware($this)
        );

        // Add routing middleware
        $middleware->add(
            new RoutingMiddleware($this)
        );

        // Add the middleware to the middleware queue
        $middleware->add(
            new AuthorizationMiddleware($this)
        );

        // Add the middleware to the middleware queue
        $middleware->add(
            new RequestAuthorizationMiddleware()
        );

        // Replace MaintenanceHandlerMiddleware with
        $middleware->add(
            new MaintenanceHandlerMiddleware()
        );

        return $middleware;
    }

    /**
     * {@inheritDoc}
     */
    public function routes(RouteBuilder $routes): void
    {
        if ( SxRouter::$initialized == true ) {
            return;
        }

        if ( file_exists($this->baseDir . DS . 'Config' . DS . 'routes.php') ) {
            require_once $this->baseDir . DS . 'Config' . DS . 'routes.php';
        }

        if ( file_exists($this->baseDir . DS . 'Bootstrap' . DS . 'routes.php') ) {
            require_once $this->baseDir . DS . 'Bootstrap' . DS . 'routes.php';
        }

        SxRouter::$initialized = true;
    }

    /**
     * {@inheritDoc}
     */
    public function console(CommandCollection $commands): CommandCollection
    {
        $request = new ServerRequest();

        Request::setupFacade($request);
        Logger::setupFacade($request);
        Language::setupFacade($request);
        Menu::setupFacade($request);
        Mailer::setupFacade($request);
        Article::setupFacade($request);
        Search::setupFacade($request);

        $commands = parent::console($commands);

        $commands->add(
            'crontasks', CrontasksCommand::class
        );

        return $commands;
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        $resolver = new MapResolver();
        $resolver->map(ServerRequest::class, RequestPolicy::class);

        return new AuthorizationService($resolver);
    }

    /**
     * Returns a service provider instance.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request Request
     * @return \Authentication\AuthenticationServiceInterface
     */
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        $service = new AuthenticationService();

        $fields = [
            IdentifierInterface::CREDENTIAL_USERNAME => 'login',
            IdentifierInterface::CREDENTIAL_PASSWORD => 'password',
        ];


        // Load the authenticators. Session should be first.
        $service->loadAuthenticator('Authentication.Session');

        $service->loadAuthenticator('Authentication.Form', [
            'fields' => $fields,
            'loginUrl' => '/users/login',
        ]);

        // Load identifiers
        $service->loadIdentifier('Authentication.Password', [
            'fields' => $fields,
        ]);


        return $service;
    }

}
