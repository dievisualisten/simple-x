<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxEmailsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('emails');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
        ]);
        $this->hasMany('Formularconfigs', [
            'foreignKey' => 'email_id',
        ]);
        $this->hasMany('Menus', [
            'foreignKey' => 'email_id',
        ]);
        $this->hasMany('Newsletters', [
            'foreignKey' => 'email_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->boolean('is_default')
            ->allowEmptyString('is_default');

        $validator
            ->scalar('email_to')
            ->maxLength('email_to', 255)
            ->allowEmptyString('email_to');

        $validator
            ->scalar('from_name')
            ->maxLength('from_name', 255)
            ->allowEmptyString('from_name');

        $validator
            ->scalar('from_email')
            ->maxLength('from_email', 255)
            ->allowEmptyString('from_email');

        $validator
            ->scalar('email_bcc')
            ->allowEmptyString('email_bcc');

        $validator
            ->scalar('return_path')
            ->maxLength('return_path', 255)
            ->allowEmptyString('return_path');

        $validator
            ->scalar('reply_to')
            ->maxLength('reply_to', 255)
            ->allowEmptyString('reply_to');

        $validator
            ->scalar('smtp_host')
            ->maxLength('smtp_host', 255)
            ->allowEmptyString('smtp_host');

        $validator
            ->scalar('smtp_port')
            ->maxLength('smtp_port', 255)
            ->allowEmptyString('smtp_port');

        $validator
            ->scalar('smtp_username')
            ->maxLength('smtp_username', 255)
            ->allowEmptyString('smtp_username');

        $validator
            ->scalar('smtp_password')
            ->maxLength('smtp_password', 255)
            ->allowEmptyString('smtp_password');

        $validator
            ->scalar('imap_email')
            ->maxLength('imap_email', 255)
            ->allowEmptyString('imap_email');

        $validator
            ->scalar('imap_host')
            ->maxLength('imap_host', 255)
            ->allowEmptyString('imap_host');

        $validator
            ->scalar('imap_port')
            ->maxLength('imap_port', 255)
            ->allowEmptyString('imap_port');

        $validator
            ->scalar('imap_username')
            ->maxLength('imap_username', 255)
            ->allowEmptyString('imap_username');

        $validator
            ->scalar('imap_password')
            ->maxLength('imap_password', 255)
            ->allowEmptyString('imap_password');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
            'errorField' => 'aco_id'
        ]);

        return $rules;
    }

}
