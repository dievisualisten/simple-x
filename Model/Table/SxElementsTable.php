<?php

namespace App\Model\Table;

use App\Model\Entity\Article;
use App\Model\Table\AppTraits\AttachedTrait;
use App\Model\Table\AppTraits\PagetypeTrait;
use App\Model\Table\AppTraits\PublishedTrait;
use Cake\Core\Configure;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Library\AuthManager;
use DateTime;
use When\When;

/**
 * @version 4.0.0
 */

class SxElementsTable extends AppTable
{
    use AttachedTrait, PublishedTrait, PagetypeTrait;

    /**
     * Default searchable fields
     *
     * @var array
     */
    public $defaultSearchFields = [
        'id',
        'headline',
        'subline',
        'topline',
        'teasertext',
        'content',
        'content2',
        'title',
        'type',
    ];

    /**
     * Default relations
     *
     * @var array
     */
    public $defaultContains = [
        'Acos', 'Users.Persons', 'Attached', 'Attached.Attachments', 'Attached.Attachments.Thumbnail', 'Articles'
    ];

    public $dublicateContains = [
        'Attached', 'Translation'
    ];

    /**
     * Export defaults
     *
     * @var array
     */
    public $exportDefaults = [
        'strategy' => 'remove',
        'fields' => [
            'content',
            'extended_data',
            'extended_data_cfg',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Articles', [
            'foreignKey' => 'element_id',
            'targetForeignKey' => 'article_id',
            'dependent' => true,
            'through' => 'ArticlesElements',
        ]);

        $this->addBehavior('Translation', [
            'defaultLocale' => Configure::read('Sx.app.language.defaultlanguage')
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
        ]);

        $this->hasMany('Attached', [
            'foreignKey' => 'foreign_key',
            'dependent' => true,
            'saveStrategy' => 'replace',
            'sort' => 'Attached.sequence ASC',
//            'cascadeCallbacks' => true,
        ]);

        $this->belongsToMany('Attachments', [
            'foreignKey' => 'foreign_key',
            'targetForeignKey' => 'attachment_id',
            'through' => 'Attached',
            'sort' => [
                'Attached.sequence' => 'ASC',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('identifier');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('layout')
            ->maxLength('layout', 255)
            ->allowEmptyString('layout');

        $validator
            ->boolean('has_teasertext')
            ->allowEmptyString('has_teasertext');

        $validator
            ->scalar('teasertext')
            ->maxLength('teasertext', 429496729)
            ->allowEmptyString('teasertext');

        $validator
            ->boolean('has_teasertext2')
            ->allowEmptyString('has_teasertext2');

        $validator
            ->scalar('teasertext2')
            ->maxLength('teasertext2', 429496729)
            ->allowEmptyString('teasertext2');

        $validator
            ->scalar('moretext')
            ->maxLength('moretext', 255)
            ->allowEmptyString('moretext');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->notBlank('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->allowEmptyString('slug');

        $validator
            ->scalar('content')
            ->allowEmptyString('content');

        $validator
            ->scalar('content2')
            ->allowEmptyString('content2');

        $validator
            ->scalar('headline')
            ->maxLength('headline', 255)
            ->allowEmptyString('headline');

        $validator
            ->scalar('subline')
            ->maxLength('subline', 255)
            ->allowEmptyString('subline');

        $validator
            ->scalar('topline')
            ->maxLength('topline', 255)
            ->allowEmptyString('topline');

        $validator
            ->scalar('address')
            ->allowEmptyString('address');

        $validator
            ->scalar('lat')
            ->maxLength('lat', 255)
            ->allowEmptyString('lat');

        $validator
            ->scalar('lon')
            ->maxLength('lon', 255)
            ->allowEmptyString('lon');

        $validator
            ->scalar('keywords')
            ->maxLength('keywords', 255)
            ->allowEmptyString('keywords');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('canonical')
            ->maxLength('canonical', 429496729)
            ->allowEmptyString('canonical');

        $validator
            ->boolean('noindex')
            ->allowEmptyString('noindex');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->dateTime('begin_publishing')
            ->allowEmptyDateTime('begin_publishing');

        $validator
            ->dateTime('end_publishing')
            ->allowEmptyDateTime('end_publishing');

        $validator
            ->scalar('component')
            ->maxLength('component', 255)
            ->allowEmptyString('component');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['user_id'], 'Users'), [
            'errorField' => 'user_id'
        ]);

        //$rules->add($rules->existsIn(['creator_id'], 'Creators'), ['errorField' => 'creator_id']);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
            'errorField' => 'aco_id'
        ]);

//        $rules->add($rules->existsIn(['formularconfig_id'], 'Formularconfigs'), [
//            'errorField' => 'formularconfig_id'
//        ]);

        //$rules->add($rules->existsIn(['parent_id'], 'ParentElements'), ['errorField' => 'parent_id']);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $entity->set('element', 1);

        parent::beforeSave($event, $entity, $options);
    }

    /**
     * @inheritdoc
     */
    public function beforeFind(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        $query->where([
            'element' => 1
        ]);

        parent::beforeFind($event, $query, $options, $primary);
    }

    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');
        $schema->setColumnType('extended_data_cfg', 'json');

        return $schema;
    }

}
