<?php

namespace App\Model\Table;

use App\Library\AuthManager;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxRolesTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id',  'name',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Users.Persons',
        'Resources'
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsToMany('Resources', [
            'className' => 'Resources',
            'foreignKey' => 'role_id',
            'targetForeignKey' => 'resource_id',
            'joinTable' => 'resources_roles',
            'sort' => ['Resources.name' => 'ASC'],
        ]);

        $this->belongsToMany('Users', [
            'className' => 'Users',
            'foreignKey' => 'role_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'roles_users',
            'sort' => ['Users.login' => 'ASC'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('aco_accesslevel')
            ->allowEmptyString('aco_accesslevel');

        $validator
            ->scalar('auth_config_identifier')
            ->maxLength('auth_config_identifier', 255)
            ->requirePresence('auth_config_identifier', 'create')
            ->notEmptyString('auth_config_identifier');

        $validator
            ->scalar('start_controller')
            ->maxLength('start_controller', 255)
            ->allowEmptyString('start_controller');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), [
            'errorField' => 'name'
        ]);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    function beforeFind2(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        if ( $primary && AuthManager::isActive() ) {
            $query->where(['Roles.aco_accesslevel >=' => AuthManager::getAccessLevel('role')]);
        }
    }

}
