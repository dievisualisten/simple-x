<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxNewsletterinterestsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newsletterinterests');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsToMany('Newsletterrecipients', [
            'foreignKey' => 'newsletterinterest_id',
            'targetForeignKey' => 'newsletterrecipient_id',
            'joinTable' => 'newsletterinterests_newsletterrecipients',
        ]);

        $this->belongsToMany('Newsletters', [
            'foreignKey' => 'newsletterinterest_id',
            'targetForeignKey' => 'newsletter_id',
            'joinTable' => 'newsletterinterests_newsletters',
        ]);

        $this->hasMany('NewsletterinterestsNewsletterrecipients', [
            //
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->boolean('selectable')
            ->allowEmptyString('selectable');

        return $validator;
    }

}
