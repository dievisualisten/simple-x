<?php

namespace App\Model\Table;

use App\Model\Entity\Article;
use Cake\ORM\RulesChecker;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxArticlesElementsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles_elements');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Elements', [
            'foreignKey' => 'element_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['article_id'], 'Articles'), [
//            'errorField' => 'article_id'
//        ]);

        $rules->add($rules->existsIn(['element_id'], 'Elements'), [
            'errorField' => 'element_id'
        ]);

        return $rules;
    }

    public function saveArticle(Article $article)
    {
        $this->deleteAll([
            'article_id' => $article->id
        ]);

        $result = true;

        foreach ( $article->elements as $relation => $items ) {

            foreach ( $items as $index => $item ) {

                $joinData = data_get($item, '_joinData');

                if ( empty($joinData) ) {
                    throw new \Exception(__d('system', 'Elementverbindung ist leer.'));
                }

                $entity = $this->newEntity($joinData);

                // Override sequence
                $entity->set('sequence', $index);

                $result &= !! $this->save($entity);
            }

        }

        if ( ! $result ) {
            throw new \Exception(__d('system', 'Elemente konnten nicht gespeichert werden.'));
        };
    }

}
