<?php

namespace App\Model\Table;

/**
 * @version 4.0.0
 */

class SxTextsTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('texts_translations');
        $this->setDisplayField('locale');
        $this->setPrimaryKey(['locale', 'id']);
        $this->addBehavior('Timestamp');
    }

}
