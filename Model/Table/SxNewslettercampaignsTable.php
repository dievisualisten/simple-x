<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxNewslettercampaignsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newslettercampaigns');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Newsletters', [
            'foreignKey' => 'newsletter_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Newsletterrecipients', [
            'foreignKey' => 'newsletterrecipient_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->allowEmptyString('status');

        $validator
            ->boolean('send')
            ->allowEmptyString('send');

        $validator
            ->boolean('read')
            ->allowEmptyString('read');

        $validator
            ->boolean('clicked')
            ->allowEmptyString('clicked');

        $validator
            ->boolean('unsubscribed')
            ->allowEmptyString('unsubscribed');

        $validator
            ->integer('hardbounced')
            ->allowEmptyString('hardbounced');

        $validator
            ->integer('softbounced')
            ->allowEmptyString('softbounced');

        $validator
            ->dateTime('senddate')
            ->allowEmptyDateTime('senddate');

        $validator
            ->dateTime('readdate')
            ->allowEmptyDateTime('readdate');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['newsletter_id'], 'Newsletters'), [
            'errorField' => 'newsletter_id'
        ]);

        $rules->add($rules->existsIn(['newsletterrecipient_id'], 'Newsletterrecipients'), [
            'errorField' => 'newsletterrecipient_id'
        ]);

        return $rules;
    }

}
