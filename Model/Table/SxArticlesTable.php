<?php

namespace App\Model\Table;

use App\Model\Table\AppTraits\AttachedTrait;
use App\Model\Table\AppTraits\ElementsTrait;
use App\Model\Table\AppTraits\EventsTrait;
use App\Model\Table\AppTraits\PagetypeTrait;
use App\Model\Table\AppTraits\PublishedTrait;
use App\Model\Table\AppTraits\TabparentsTrait;
use App\Model\Table\Traits\ExtenderTrait;
use Cake\Core\Configure;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Library\AuthManager;

/**
 * @version 4.0.0
 */

class SxArticlesTable extends AppTable
{
    use ExtenderTrait, TabparentsTrait, AttachedTrait, PublishedTrait, EventsTrait, ElementsTrait, PagetypeTrait;

    /**
     * Default searchable fields
     *
     * @var array
     */
    public $defaultSearchFields = [
        'id',
        'headline',
        'subline',
        'topline',
        'teasertext',
        'content',
        'content2',
        'title',
        'type',
    ];

    /**
     * Default relations
     *
     * @var array
     */
    public $defaultContains = [
        // Autofill
    ];

    public $defaultElements = [
        // Autofill
    ];

    public $dublicateContains = [
        'Tabparents', 'Attached', 'Translation', //'Elements'
    ];

    /**
     * Export defaults
     *
     * @var array
     */
    public $exportDefaults = [
        'strategy' => 'remove',
        'fields' => [
            'content',
            'extended_data',
            'extended_data_cfg',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Translation', [
            'defaultLocale' => Configure::read('Sx.app.language.defaultlanguage')
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);

        /*$this->belongsTo('Creators', [
            'foreignKey' => 'creator_id',
        ]);*/

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
        ]);

        $this->belongsTo('Formularconfigs', [
            'foreignKey' => 'formularconfig_id',
        ]);

        $this->hasMany('Menus', [
            'foreignKey' => 'foreign_key',
            'dependent' => true,
            'sort' => 'Menus.created ASC',
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('Attached', [
            'foreignKey' => 'foreign_key',
            'dependent' => true,
            'saveStrategy' => 'replace',
            'sort' => 'Attached.sequence ASC',
        ]);

        $this->belongsToMany('Attachments', [
            'foreignKey' => 'foreign_key',
            'targetForeignKey' => 'attachment_id',
            'through' => 'Attached',
            'sort' => [
                'Attached.sequence' => 'ASC',
            ],
        ]);


        if ( ! in_array('Menus', $this->defaultContains) ) {
            array_push($this->defaultContains, 'Menus');
        }

        if ( ! in_array('Users.Persons', $this->defaultContains) ) {
            array_push($this->defaultContains, 'Users.Persons');
        }

        if ( ! in_array('Attached.Attachments.Thumbnail', $this->defaultContains) ) {
            array_push($this->defaultContains, 'Attached.Attachments.Thumbnail');
        }

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('identifier');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('layout')
            ->maxLength('layout', 255)
            ->allowEmptyString('layout');

        $validator
            ->boolean('has_teasertext')
            ->allowEmptyString('has_teasertext');

        $validator
            ->scalar('teasertext')
            ->maxLength('teasertext', 429496729)
            ->allowEmptyString('teasertext');

        $validator
            ->boolean('has_teasertext2')
            ->allowEmptyString('has_teasertext2');

        $validator
            ->scalar('teasertext2')
            ->maxLength('teasertext2', 429496729)
            ->allowEmptyString('teasertext2');

        $validator
            ->scalar('moretext')
            ->maxLength('moretext', 255)
            ->allowEmptyString('moretext');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->notBlank('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->allowEmptyString('slug');

        $validator
            ->scalar('content')
            ->allowEmptyString('content');

        $validator
            ->scalar('content2')
            ->allowEmptyString('content2');

        $validator
            ->scalar('headline')
            ->maxLength('headline', 255)
            ->allowEmptyString('headline');

        $validator
            ->scalar('subline')
            ->maxLength('subline', 255)
            ->allowEmptyString('subline');

        $validator
            ->scalar('topline')
            ->maxLength('topline', 255)
            ->allowEmptyString('topline');

        $validator
            ->scalar('address')
            ->allowEmptyString('address');

        $validator
            ->scalar('lat')
            ->maxLength('lat', 255)
            ->allowEmptyString('lat');

        $validator
            ->scalar('lon')
            ->maxLength('lon', 255)
            ->allowEmptyString('lon');

        $validator
            ->scalar('keywords')
            ->maxLength('keywords', 255)
            ->allowEmptyString('keywords');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('canonical')
            ->maxLength('canonical', 429496729)
            ->allowEmptyString('canonical');

        $validator
            ->boolean('noindex')
            ->allowEmptyString('noindex');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->dateTime('begin_publishing')
            ->allowEmptyDateTime('begin_publishing');

        $validator
            ->dateTime('end_publishing')
            ->allowEmptyDateTime('end_publishing');

        $validator
            ->scalar('component')
            ->maxLength('component', 255)
            ->allowEmptyString('component');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['user_id'], 'Users'), [
            'errorField' => 'user_id'
        ]);

        //$rules->add($rules->existsIn(['creator_id'], 'Creators'), ['errorField' => 'creator_id']);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
            'errorField' => 'aco_id'
        ]);

        $rules->add($rules->existsIn(['formularconfig_id'], 'Formularconfigs'), [
            'errorField' => 'formularconfig_id'
        ]);

        //$rules->add($rules->existsIn(['parent_id'], 'ParentArticles'), ['errorField' => 'parent_id']);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $entity->set('element', 0);

        if ( ! empty($entity->title) && empty($entity->slug) ) {
            $entity->set('slug', $entity->title);
        }

        parent::beforeSave($event, $entity, $options);
    }

    public function beforeFind(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        $query->where([
            'element' => 0
        ]);

        parent::beforeFind($event, $query, $options, $primary);
    }

    function deleteTranslation($articleId, $locale)
    {
        $translationsTable = TableRegistry::getTableLocator()
            ->get('ArticlesTranslations');

        $translatedEntity = $translationsTable->find()->where([
            'id' => $articleId, 'locale' => $locale,
        ])->first();

        return $translationsTable->delete($translatedEntity);
    }

    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');
        $schema->setColumnType('extended_data_cfg', 'json');

        return $schema;
    }

    public function getElasticSearchIndexQuery(Query $query = null)
    {
        return $this->find()->contain(false)->all();
    }

}
