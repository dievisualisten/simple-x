<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxNewsletterinterestsNewslettersTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newsletterinterests_newsletters');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Newsletterinterests', [
            'foreignKey' => 'newsletterinterest_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Newsletters', [
            'foreignKey' => 'newsletter_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['newsletterinterest_id'], 'Newsletterinterests'), [
            'errorField' => 'newsletterinterest_id'
        ]);

        $rules->add($rules->existsIn(['newsletter_id'], 'Newsletters'), [
            'errorField' => 'newsletter_id'
        ]);

        return $rules;
    }

}
