<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxMenusTemporaryTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('menus_temporary');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        //Fixme Table falsche Cake Convention bzgl. id
        $this->hasOne('Menu', [
            'className' => 'Menus',
            'foreignKey' => 'id',
        ]);

        $this->hasMany('MenuTranslation', [
            'className' => 'MenusTranslations',
            'foreignKey' => 'id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 10)
            ->allowEmptyString('locale');

        $validator
            ->uuid('foreign_key')
            ->allowEmptyString('foreign_key');

        $validator
            ->scalar('path')
            ->allowEmptyString('path');

        return $validator;
    }

}
