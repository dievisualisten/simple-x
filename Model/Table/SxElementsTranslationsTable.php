<?php

namespace App\Model\Table;

use Cake\Database\Schema\TableSchemaInterface;

/**
 * @version 4.0.0
 */

class SxElementsTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles_translations');
        $this->setDisplayField('title');
        $this->setPrimaryKey(['id', 'locale']);
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');

        return $schema;
    }

}
