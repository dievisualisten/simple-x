<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxNewslettersTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newsletters');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Emails', [
            'foreignKey' => 'email_id',
        ]);

        $this->belongsTo('Menus', [
            'foreignKey' => 'menu_id',
        ]);

        $this->hasMany('Newslettercampaigns', [
            'foreignKey' => 'newsletter_id',
        ]);

        $this->belongsToMany('Newsletterinterests', [
            'foreignKey' => 'newsletter_id',
            'targetForeignKey' => 'newsletterinterest_id',
            'joinTable' => 'newsletterinterests_newsletters',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 20)
            ->allowEmptyString('locale');

        $validator
            ->scalar('template')
            ->maxLength('template', 255)
            ->allowEmptyString('template');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 255)
            ->allowEmptyString('domain');

        $validator
            ->scalar('list')
            ->maxLength('list', 255)
            ->allowEmptyString('list');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 255)
            ->allowEmptyString('subject');

        $validator
            ->scalar('content')
            ->maxLength('content', 429496729)
            ->allowEmptyString('content');

        $validator
            ->scalar('testreceiveremail')
            ->maxLength('testreceiveremail', 255)
            ->allowEmptyString('testreceiveremail');

        $validator
            ->boolean('to_send')
            ->allowEmptyString('to_send');

        $validator
            ->boolean('send')
            ->allowEmptyString('send');

        $validator
            ->dateTime('senddate')
            ->allowEmptyDateTime('senddate');

        $validator
            ->integer('sendcount')
            ->allowEmptyString('sendcount');

        $validator
            ->integer('delivered')
            ->allowEmptyString('delivered');

        $validator
            ->integer('readcount')
            ->allowEmptyString('readcount');

        $validator
            ->integer('softbounces')
            ->allowEmptyString('softbounces');

        $validator
            ->integer('hardbounces')
            ->allowEmptyString('hardbounces');

        $validator
            ->integer('unsubscribes')
            ->allowEmptyString('unsubscribes');

        $validator
            ->integer('clickers')
            ->allowEmptyString('clickers');

        $validator
            ->boolean('campaigncreated')
            ->allowEmptyString('campaigncreated');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
            'errorField' => 'aco_id'
        ]);

        $rules->add($rules->existsIn(['email_id'], 'Emails'), [
            'errorField' => 'email_id'
        ]);

        $rules->add($rules->existsIn(['menu_id'], 'Menus'), [
            'errorField' => 'menu_id'
        ]);

        return $rules;
    }

}
