<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxBankaccountsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('bankaccounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('foreign_key');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model');

        $validator
            ->scalar('holder')
            ->maxLength('holder', 255)
            ->allowEmptyString('holder');

        $validator
            ->scalar('bankname')
            ->maxLength('bankname', 255)
            ->allowEmptyString('bankname');

        $validator
            ->scalar('iban')
            ->maxLength('iban', 255)
            ->allowEmptyString('iban');

        $validator
            ->scalar('bic')
            ->maxLength('bic', 255)
            ->allowEmptyString('bic');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        $entity->set('type', $event->getSubject()->getAlias());
    }

}
