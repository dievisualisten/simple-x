<?php

namespace App\Model\Table;

use App\Library\AuthManager;
use App\Library\Facades\Article;
use App\Library\Facades\Language;
use App\Model\Entity\AppEntity;
use App\Model\Entity\Menu;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxMenusTable extends AppTable
{
    /**
     * @var bool $stopAfterSaveOnSavedMenus
     */
    private $stopAfterSaveOnSavedMenus = false;

    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
      'title', 'path',
    ];

    protected $_clipboard = [
      'begin_publishing',
      'end_publishing',
      'active',
      'type',
      'aco_id',
      'title',
      'slug',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('menus');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree', [
          'recoverOrder' => 'lft', 'level' => 'level',
        ]);
        $this->addBehavior('Translation', [
          'defaultLocale' => Language::getDefaultLanguage(),
        ]);

        $this->addBehavior('Frontendmenu');

        $this->belongsTo('Aliases', [
          'foreignKey' => 'alias_id',
        ]);

        $this->belongsTo('ParentMenus', [
          'className'  => 'Menus',
          'foreignKey' => 'parent_id',
        ]);

        $this->belongsTo('Acos', [
          'foreignKey' => 'aco_id',
        ]);

        $this->belongsTo('Emails', [
          'foreignKey' => 'email_id',
        ]);

        $this->hasMany('Actionviews', [
          'foreignKey' => 'menu_id',
        ]);

        $this->hasMany('ChildMenus', [
          'className'  => 'Menus',
          'foreignKey' => 'parent_id',
        ]);

        $this->hasMany('Newsletters', [
          'foreignKey' => 'menu_id',
        ]);

        $this->hasMany('Orders', [
          'foreignKey' => 'menu_id',
        ]);

        $this->hasMany('Events', [
          'foreignKey' => 'menu_id',
          'dependent'  => true,
        ]);

        $this->belongsTo('Articles', [
          'className'  => 'Articles',
          'foreignKey' => 'foreign_key',
        ]);

    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        // $validator->requirePresence('title')
        //     ->notEmptyString('title', __('Bitte geben Sie eine Domain ein'));

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function validationDefault1(Validator $validator): Validator
    {
        $validator
          ->uuid('foreign_key')
          ->allowEmptyString('foreign_key');

        $validator
          ->scalar('extended_data')
          ->maxLength('extended_data', 429496729)
          ->allowEmptyString('extended_data');

        $validator
          ->scalar('type')
          ->maxLength('type', 255)
          ->allowEmptyString('type');

        $validator
          ->integer('level')
          ->allowEmptyString('level');

        $validator
          ->scalar('slug')
          ->maxLength('slug', 255)
          ->allowEmptyString('slug');

        $validator
          ->scalar('title')
          ->maxLength('title', 255)
          ->allowEmptyString('title');

        $validator
          ->scalar('url')
          ->allowEmptyString('url');

        $validator
          ->boolean('blank')
          ->allowEmptyString('blank');

        $validator
          ->scalar('path')
          ->allowEmptyString('path');

        $validator
          ->scalar('controller')
          ->maxLength('controller', 255)
          ->allowEmptyString('controller');

        $validator
          ->scalar('action')
          ->maxLength('action', 255)
          ->allowEmptyString('action');

        $validator
          ->scalar('model')
          ->maxLength('model', 255)
          ->allowEmptyString('model');

        $validator
          ->scalar('params')
          ->allowEmptyString('params');

        $validator
          ->integer('teaser')
          ->allowEmptyString('teaser');

        $validator
          ->boolean('active')
          ->allowEmptyString('active');

        $validator
          ->dateTime('begin_publishing')
          ->allowEmptyDateTime('begin_publishing');

        $validator
          ->dateTime('end_publishing')
          ->allowEmptyDateTime('end_publishing');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function validationDomain(Validator $validator): Validator
    {
        $validator->requirePresence('title')
          ->notEmptyString('title', __('Bitte geben Sie eine Domain ein'));

        $validator->add('title', [
          'url' => [
            'rule'    => ['url', false],
            'message' => __('Bitte geben Sie eine gültige Domain ein'),
          ],
        ]);

        $validator->add('slug', [
          'slug' => [
            'rule'    => function ($value, $options) {
                return $this->_uniqueInType($value, $options);
            },
            'message' => __('Bitte geben Sie eine gültige Domain ein'),
          ],
        ]);

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function validationMenu(Validator $validator): Validator
    {
        $validator->requirePresence('title')
          ->notEmptyString('title', __('Bitte geben Sie einen Namen für das Menü ein.'), false);

        $validator->add('slug', [
          'title' => [
            'rule'    => function ($value, $options) {
                return $this->_uniqueInType($value, $options);
            },
            'message' => __('Bitte geben Sie eine gültige Domain ein'),
          ],
        ]);

        return $validator;
    }

    protected function _uniqueInType($value, $options)
    {
        if ( empty($options['data']['type']) ) {
            return false;
        }

        if ( ! $options['newRecord'] ) {
            return true;
        }

        $conditions = [
          'type' => $options['data']['type'],
          'slug' => $value,
        ];

        return ! $this->find()->where($conditions)->count();
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        // $rules->add($rules->existsIn(['alias_id'], 'Aliases'), ['errorField' => 'alias_id']);
        // $rules->add($rules->existsIn(['parent_id'], 'ParentMenus'), ['errorField' => 'parent_id']);

//        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
//            'errorField' => 'aco_id',
//        ]);
//
//        $rules->add($rules->existsIn(['email_id'], 'Emails'), [
//            'errorField' => 'email_id',
//        ]);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    function findFormated(Query $query, $options = ['mode' => 'article'])
    {
        $query = $query->order(['Menus.lft' => 'ASC']);
        $query->formatResults(function ($results) use ($options) {
            return $results->map(function ($node) use ($options) {

                $mode = $options['mode'];
                $type = $node['type'];
                $allowDrag = true;
                $allowDrop = true;
                $disabled = false;
                $leaf = false;

                $cls = 'icon-page-default icon-' . $type . ' fa fa-' . $type;

                if ( $type == 'menu' || $type == 'domain' || $type == 'frontpage' ) {
                    $allowDrag = (bool) Configure::read('Sx.app.adminmode');
                }

                if ( $type == 'domain' ) {
                    $allowDrop = (bool) Configure::read('Sx.app.adminmode');
                }

                if ( $type == 'link' || $type == 'alias' || $type == 'domain' ) {
                    $disabled = true;
                }

                if ( $type == 'link' || $type == 'alias' ) {
                    $leaf = true;
                }

                if ( $mode == 'alias' ) {
                    if ( $type == 'menu' || $type == 'frontpage' ) {
                        $disabled = true;
                    }
                }

                $layout = $node->article ? $node->article->layout : 'default';

                unset($node->article);

                $data = [
                  "iconCls"   => 'fal ' . $cls,
                  "leaf"      => $leaf, //nur wenn es wirklich keine kinder geben kann
                  "allowDrop" => $allowDrop,
                  "allowDrag" => $allowDrag,
                  "disabled"  => $disabled,
                  'published' => $node->published,
                  'layout'    => $layout,
                  'children'  => [],
                  'path'      => str_replace(Configure::read('localDomain'), request()->domain(true), $node->get('path')),
                ];

                $node['title'] = strip_tags($node['title']);

                if ( in_array($node['teaser'], TEASER_WITHOUT_URL) ) {
                    $node['title'] = '[' . $node['title'] . ']';
                }

                $node = $this->patchEntity($node, $data);

                return $node;
            });

        });

        return $query->find('threaded');

    }

    /**
     * @inheritdoc
     */
    public function getParentPaths($menus, $key = 'parent_id', $aliase = true)
    {
        $temp = $menus;

        if ( empty($temp) ) {
            return [];
        }

        if ( ! $aliase ) {
            $menus = [];
            foreach ( $temp as $k => $checkedNode ) {
                if ( $checkedNode['type'] != 'alias' ) {
                    $menus[] = $checkedNode;
                }
            }
        }

        $ret = [];

        foreach ( $menus as $k => $checkedNode ) {

            $stringpath = "/root";
            $path = $this->find('path', ['fields' => ['id', 'type', 'teaser'], 'for' => $checkedNode[$key]])->toArray();

            $myNode = $path[count($path) - 1];
            foreach ( $path as $node ) {
                $stringpath = $stringpath . '/' . $node['id'];
            }

            $myNode->parent_path = $stringpath;
            $myNode->teaser = $checkedNode->teaser;
            $ret[$k] = $myNode;
        }

        return $ret;
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        /** @var \App\Model\Entity\Menu $entity */
        $this->updatePath($entity);
        $this->makeTranslations($entity);
        $this->makeEvents($entity);
    }


    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $table = TableRegistry::getTableLocator()
          ->get('MenusTranslations');

        $table->deleteAll([
          'id' => $entity->get('id'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function afterTranslationDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->deleteTranslationPath($entity);
    }


    /**
     * Generate path for given menu entity
     *
     * @param \App\Model\Entity\Menu $entity
     * @param bool $useOriginal
     * @return string
     */
    public function generatePathFor($entity, $useOriginal = false)
    {
        $id = $entity->get('id');

        // Get all parents up to root
        $descandants = $this->find('path', ['for' => $id])
          ->toArray();

        $pathChunks = array_map(function ($chunk) use ($id, $entity, $useOriginal) {

            if ( $chunk->id === $id ) {

                // If chunk is final entity use given data
                $chunk = $entity;
            }

            // Get domain state
            $isDomain = $chunk->get('type') === 'domain';

            if ( ! $chunk->canHaveUniquePath() && ! $isDomain ) {

                // Return incase entity is not allowed to generate a url
                return null;
            }

            // Get real slug if its a iteration
            $path = preg_replace('/(.*?)([^\/]+)$/',
              '$2', $chunk->get('path'));

            if ( $chunk->id === $id || empty($path) ) {

                // If path is empty or final element use slug
                $path = $chunk->get('slug');
            }

            if ( $useOriginal ) {

                // Same as above
                $path = preg_replace('/(.*?)([^\/]+)$/',
                  '$2', $chunk->getOriginal('path'));

                if ( $chunk->id === $id || empty($path) ) {

                    // If path is empty or final element use slug
                    $path = $chunk->getOriginal('slug');
                }
            }

            if ( $isDomain && ! Language::isDefaultLanguage() ) {

                // Append language for localized domains
                $path = str_join('/', $path, Language::getLanguage());
            }

            return $path;

        }, $descandants);

        // Join chunks into final route
        return str_join('/', ...$pathChunks);
    }

    /**
     * Generate a unique path if needed
     *
     * @param \App\Model\Entity\Menu $entity
     * @return \App\Model\Entity\Menu|null
     */
    public function generateNextPath($entity)
    {
        if ( ! $entity->canHaveUniquePath() ) {

            // If path is passed just return
            return $entity;
        }

        $orginalPath = $entity->getOriginal('path');

        $useOriginal = ! $entity->isDirty('slug') &&
          ! $entity->isDirty('parent_id') && Language::isDefaultLanguage();

        if ( $useOriginal ) {

            // Override path incase nothing real changed
            $entity->set('path', $orginalPath);
        }

        $conditions = [
          'path'        => $entity->get('path'),
          'id !='       => $entity->get('id'),
          'alias_id !=' => $entity->get('id'),
        ];

        $conditions['OR'] = [
          'lft <'  => $entity->get('lft'),
          'rght >' => $entity->get('rght'),
        ];

        if ( $entity->isDefaultLanguage() ) {

            // Not sure maybe the filter is required in all languages?
            $conditions['type !='] = 'alias';
        }

        // Count routes with same path except childs and self
        $count = $this->find()->where($conditions)->count();

        if ( ! $count ) {

            // Return if path is unique
            return $entity;
        }

        // Get last counter segment
        preg_match('/(?<=\-\-)[0-9]+$/', $entity->get('path'), $match);

        // Remove old counter segment and increase by one
        $path = preg_replace('/\-\-[0-9]+$/', '', $entity->get('path')) .
          '--' . (intval(@$match[0] ?: 0) + 1);

        $entity->set('path', $path);

        // Run loop again to check if next step is unique
        return $this->generateNextPath($entity);
    }

    /**
     * Update path for entity and all childs below
     *
     * @param \App\Model\Entity\Menu $entity
     * @return \App\Model\Entity\Menu|null
     */
    public function updateEntityPath($entity)
    {
        $finder = TableRegistry::getTableLocator()
          ->get('MenusTranslations');

        if ( Language::isDefaultLanguage() ) {

            // Use MenuTable in default
            $finder = $this;
        }

        // Not sure if this still matters
        $entity->setCurrentLanguage();

        // Get provisional path
        $path = $this->generatePathFor($entity);

        if ( ! empty($path) ) {

            // Set path if not empty
            $entity->set('path', $path);
        }

        // Get final path
        $entity = $this->generateNextPath($entity);

        // Set final path
        $path = $entity->get('path');

        // Get origial path in entity
        $oldPath = $entity->getOriginal('path');

        if ( empty($oldPath) || ! Language::isDefaultLanguage() ) {

            // Generate old path for entity
            $oldPath = $this->generatePathFor($entity, true);
        }

        if ( $path === $oldPath && ! empty($orgPath) ) {

            // Return if path has not changed and is not first time
            return $entity;
        }

        if ( empty($childs = $this->getPathChilds($entity)) ) {

            // Get child ids and return if no childs present
            return $entity;
        }

        $expression = new QueryExpression(
          sprintf('Replace(path, "%s", "%s")', $oldPath, $path)
        );

        $setter = [
          'path' => $expression,
        ];

        $filter = [
          'id IN' => $childs,
        ];

        if ( ! Language::isDefaultLanguage() ) {

            // If localized filter on language
            $filter['locale'] = Language::getLanguage();
        }

        // Update all childs
        $finder->updateAll($setter, $filter);

        return $entity;
    }

    public function deleteTranslationPath($entity)
    {
        /** @var \App\Model\Entity\Menu $source */
        $source = $this->forceFirstOrNull($entity);

        if ( ! $source ) {
            return;
        }

        $path = $this->generatePathFor($source);

        $expression = new QueryExpression(
          sprintf('Replace(path, "%s", "%s")', $entity->path, $path)
        );

        $setter = [
          'path' => $expression,
        ];

        $filter = [
          'id IN'  => $this->getPathChilds($source),
          'locale' => Language::getLanguage(),
        ];


        // Update all childs
        $entity->getTable()->updateAll($setter, $filter);
    }

    /**
     * Get Article for given menu or alias
     *
     * @param \App\Model\Entity\Menu $entity
     * @return \App\Model\Entity\Article|null
     */
    public function getSourceArticle($entity)
    {
        return Language::inDefaultLanguage(function () use ($entity) {

            /** @var \App\Model\Table\ArticlesTable $this ->Articles */

            $conditions = [
              'id' => $entity->get('foreign_key'),
            ];

            return $this->Articles->find('translations')
              ->where($conditions)->first();
        });
    }

    /**
     * Get children of menu entity
     *
     * @param \App\Model\Entity\Menu $entity
     * @return array
     */
    public function getPathChilds($entity)
    {
        return Language::inDefaultLanguage(function () use ($entity) {

            $conditions = [
              'lft >'  => $entity->get('lft'),
              'rght <' => $entity->get('rght'),
            ];

            $children = $this->find()->where($conditions)->extract('id')
              ->toArray();

            if ( empty($children) ) {

                // Return empty array if no childs given
                return $children;
            }

            $conditions = [
              'alias_id IN' => $children,
            ];

            // Find all ids for aliases
            $relations = $this->find()->where($conditions)->extract('id')
              ->toArray();

            return array_unique(array_merge($children, $relations));
        });
    }

    /**
     * Create translations for menu entity
     *
     * @param \App\Model\Entity\Menu $entity
     * @return void
     */
    public function makeTranslations(Menu $entity)
    {
        AuthManager::pause();

        if ( empty($entity->get('foreign_key')) ) {

            // If its not a linked just return
            return;
        }

        // Loop all article translations for a alias or menu entity
        $translations = $this->getSourceArticle($entity)
            ->get('_translations', []);

        foreach ( $translations as $locale => $version ) {

            /** @var \App\Model\Entity\ArticlesTranslation $version */

            Language::inLanguage($locale, function () use ($entity, $version, $locale) {

                /** @var \App\Model\Entity\Menu $target */
                $target = $this->forceFirstOrFail($entity);

                if ( $entity->get('type') === 'alias' ) {
                    $target = $this->forceFirstOrFail(['id' => $entity->get('alias_id')]);
                }

                /** @var \App\Model\Table\MenusTranslationsTable $translationsTable */
                $translationsTable = TableRegistry::getTableLocator()
                  ->get('MenusTranslations');

                $conditions = [
                  'id'     => $entity->get('id'),
                  'locale' => $version->get('locale'),
                ];

                $translation = $translationsTable->find()
                  ->where($conditions)->first();

                $target->set('slug', $version->get('slug'));

                if ( $target->get('type') !== 'alias' ) {

                    // Skip update on path for aliases
                    $target = $this->updateEntityPath($target);
                }

                $storeData = [
                  'title' => $version->get('title'),
                  'slug'  => $target->get('slug'),
                  'path'  => $target->get('path'),
                ];

                if ( empty($translation) ) {
                    $translation = $translationsTable->newEntity($conditions);
                }

                $translation->set($storeData);

                // Final save
                $translationsTable->save($translation);

                // Update aliases
                $this->updateAliases($entity);

            });
        }

        AuthManager::resume();
    }

    /**
     * Update path for the base entity
     *
     * @param \App\Model\Entity\Menu $entity
     * @param boolean $reloadEntity
     */
    public function updatePath($entity, $reloadEntity = true)
    {
        AuthManager::pause();

        if ( $entity->get('type') === 'alias' ) {
            return;
        }

        if ( $reloadEntity ) {

            // Reload entity incase its not the original language
            $reloadEntity = ! $entity->isDefaultLanguage();
        }

        $entity = Language::inDefaultLanguage(function () use ($entity, $reloadEntity) {

            if ( $reloadEntity ) {
                $entity = $this->forceFirstOrFail($entity);
            }

            // Update paths of entity and all childs
            $this->updateEntityPath($entity);

            $setter = [
              'path' => $entity->get('path'),
            ];

            $filter = [
              'id' => $entity->get('id'),
            ];

            $this->updateAll($setter, $filter);

            // Update aliases
            $this->updateAliases($entity);

            return $entity;
        });

        // From here on we guess the entity has no translations so all childs
        // which have a translation use the base slug of this entity.

        // In this step we take the base path update it and replace
        // the child translations.

        foreach ( Language::getAllowedLanguages() as $locale ) {
            Language::inLanguage($locale, function () use ($locale, $entity) {

                if ( $locale === Language::getDefaultLanguage() ) {
                    return;
                }

                $translationsTable = TableRegistry::getTableLocator()
                  ->get('MenusTranslations');

                $translation = $translationsTable->find()
                  ->where([
                    'id'     => $entity->id,
                    'locale' => $locale,
                  ])
                  ->first();

                if ( ! empty($translation) ) {

                    // Skip translated items
                    return;
                }

                $this->updateEntityPath($entity);
            });
        }

        AuthManager::resume();
    }

    /**
     * Update all aliases for this menu
     *
     * @param \App\Model\Entity\Menu $entity
     * @return void
     */
    function updateAliases($entity)
    {
        AuthManager::pause();

        if ( empty($entity->get('foreign_key')) || $entity->get('type') === 'alias' ) {
            return;
        }

        /** @var \App\Model\Entity\Menu $final */
        $final = $this->forceFirstOrNew($entity);

        $aliases = $this->find()
          ->where([
            'type'     => 'alias',
            'alias_id' => $entity->get('id'),
          ]);

        if ( ! $aliases->count() ) {

            // If there are no aliases just skip
            return;
        }

        $override = [
          'type' => 'alias',
          'path' => $final->get('path'),
        ];

        $clipboard = array_merge($this->_clipboard, ['path']);

        // Merge clipboard data to alias
        $data = collect($final->toArray())
          ->only($clipboard)->merge($override)->toArray();

        $data['url'] = $final->get('extended_data.menu.url', '');
        $data['blank'] = $final->get('extended_data_cfg.blank', '');

        foreach ( $aliases as $alias ) {
            $this->patchAndSave($alias, $data);
        }

        AuthManager::resume();
    }

    /**
     * Create events for a menu entity
     *
     * @param \App\Model\Entity\Menu $entity
     * @return void
     */
    public function makeEvents($entity)
    {
        AuthManager::pause();

        /** @var \App\Model\Entity\Article $article */
        $article = Article::getArticleByMenuId($entity->id);

        if ( empty($article) ) {
            return;
        }

        /** @var \App\Model\Table\EventsTable $Events */
        $Events = TableRegistry::getTableLocator()
            ->get('Events');

        $Events->deleteAll([
          'article_id' => $article->get('id'),
          'menu_id'    => $entity->get('id'),
        ]);

        foreach ( $article->get('custom._events', []) as $date ) {

            $event = $Events->newEntity([
              'article_id'     => $article->get('id'),
              'menu_id'        => $entity->get('id'),
              'menu_parent_id' => $entity->get('parent_id'),
              'date'           => Date::parse($date),
            ]);

            $result = $Events->save($event);

            if ( ! $result ) {
                throw new \Exception(__d('system', 'Event kann nicht gespeichert werden.'));
            }
        }

        AuthManager::resume();
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function getInDomainConditions($entity)
    {
        $pathdata = [];
        if ( ! empty($entity) ) {
            $pathdata = $path = $this->find('path', ['for' => $entity->get('id')])->toArray();
        }

        if ( empty($pathdata) ) {
            throw new \Cake\Http\Exception\NotFoundException('Keinen Root gefunden');
        }

        return ['lft >= ' => $pathdata[0]->get('lft'), 'rght <= ' => $pathdata[0]->get('rght')];
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function saveAlias($data)
    {
        AuthManager::pause();

        Language::inDefaultLanguage(function () use ($data) {

            foreach ( $data['tabmenus'] as $source ) {

                $entity = $this->firstOrFail($source);

                $this->patchEntity($entity, [
                  'tabparents' => $data['tabparents'],
                ]);

                $this->saveMenus($entity, 'Menu', ['type' => 'alias']);
            }

        });

        AuthManager::resume();

        return true;
    }

    public function getFrontpageParents($entity)
    {
        $id = $entity->get('Custom.domain_id');

        if ( empty($id) ) {
            throw new \Exception('No domain id provided.');
        }

        $parents = $entity['tabparents'];

        $parents = array_filter($parents, function ($tabparent) use ($id) {
            return $tabparent['id'] !== $id;
        });

        $parents = array_map(function ($tabparent) {
            return array_merge($tabparent, ['teaser' => 0]);
        }, $parents);

        $parents[] = $this->get($id)
          ->set(['teaser' => 1])->toArray();

        $entity->_setTabparents($parents);

        return $entity;
    }

    /**
     * Speichert die Menueinträge zum aktuell gespeicherten Artikel
     * Dabei werden die aktuellen Einträge erhalten, die nicht mehr gesetzen gelöscht und die neuen eingefügt.
     * alles findet auf der Defaultsprache statt
     */

    /**
     * @param AppEntity $entity
     * @param string $source
     * @param array $override
     * @throws \Exception
     */
    public function saveMenus($entity, $source = 'Article', $override = [])
    {
        AuthManager::pause();

        Language::inDefaultLanguage(function () use ($entity, $source, $override) {

            $finder = $this;

            if ( $source === 'Article' ) {
                $finder = $this->Articles;
            }

            $final = $entity->isNew() ? $entity :
              $finder->forceFirstOrNew($entity);

            if ( $entity->get('type', 'page') === 'frontpage' ) {
                $entity = $this->getFrontpageParents($entity);
            }

            if ( ! isset ($entity['tabparents']) ) {
                return;
            }

            foreach ( $entity['tabparents'] as $tabparent ) {

                $conditions = [
                  'parent_id' => $tabparent['id'],
                ];

                if ( $entity->get('type') === 'frontpage' ) {
                    $conditions['type'] = $entity->get('type');
                }

                if ( $source === 'Menu' && $entity->get('type') !== 'frontpage' ) {
                    $conditions['alias_id'] = $entity->get('id');
                }

                if ( $source === 'Article' && $entity->get('type') !== 'frontpage' ) {
                    $conditions['foreign_key'] = $entity->get('id');
                }

                $item = $this->find()->where($conditions)
                  ->first();

                if ( empty($item) ) {
                    $item = $this->newEntity($conditions);
                }

                $clipboard = $this->_clipboard;

                if ( $source === 'Menu' ) {
                    $clipboard = array_merge($clipboard, ['path']);
                }

                $data = collect($final->toArray())->only($clipboard)
                  ->merge($override)->toArray();

                $data['url'] = $final->get('extended_data.menu.url', '');
                $data['blank'] = $final->get('extended_data_cfg.menu.blank', '');

                if ( $source === 'Menu' ) {
                    $data = array_merge($data, [
                      'foreign_key' => $final->foreign_key,
                    ]);
                }

                if ( $source === 'Article' ) {
                    $data = array_merge($data, [
                      'foreign_key' => $entity->id,
                      'controller'  => 'articles',
                      'action'      => "view_{$final->type}",
                    ]);
                }

                $data = array_merge($data, [
                  'model'     => $source,
                  'parent_id' => $tabparent['id'],
                  'teaser'    => $tabparent['teaser'],
                ]);

                $this->patchEntity($item, $data);

                $item->setDirty('slug', true);

                if ( $item->get('teaser', 0) === 0 ) {
                    $this->deleteOrSkip($item);
                }

                if ( $item->get('teaser', 0) !== 0 ) {
                    $this->save($item, ['checkRules' => false]);
                }

            }
        });

        AuthManager::resume();
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function saveDomain($data)
    {
        AuthManager::pause();

        $saved = Language::inDefaultLanguage(function () use ($data) {

            $saved = true;

            if ( isset($data['title']) ) {
                $data['slug'] = $data['title'];
            }

            $entity = $this->firstOrNew($data);

            $data = collect($data)
              ->merge([
                'type'   => 'domain',
                'model'  => 'Menus',
                'teaser' => 1,
                'slug'   => $data['title'],
                'active' => true,
              ]);

            $entity = $this->patchEntity($entity, $data->toArray(),
              ['validate' => 'domain']);

            if ( ! empty($entity->getErrors()) ) {
                return false;
            }

            if ( $entity->isDirty('aco_id') && ! $entity->isNew() ) {

                $setter = [
                  'aco_id' => $entity->get('aco_id'),
                ];

                $filter = [
                  'parent_id' => $entity->get('id'),
                  'type'      => 'menu',
                ];

                $this->updateAll($setter, $filter);
            }

            // Get menus to save if entity is a new one
            $menus = ! $entity->isNew() ? [] :
              $this->find('menus')->toArray();

            // Save entity and store result
            $saved = $saved && ! ! $this->save($entity);

            foreach ( $menus as $menu ) {

                /** @var \App\Model\Entity\Menu $menu */

                $saveKeys = [
                  'type', 'title', 'slug', 'active', 'model', 'teaser',
                ];

                $clone = $this->newEntity($menu->extract($saveKeys));

                $menuData = [
                  'parent_id' => $entity->get('id'),
                  'aco_id'    => $entity->get('aco_id'),
                ];

                $saved = $saved && ! ! $this->patchAndSave($clone, $menuData,
                    ['validate' => false], ['callbacks' => false]);
            }

            return $saved;
        });

        AuthManager::resume();

        return $saved;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function saveMenu($data)
    {
        AuthManager::pause();

        $saved = Language::inDefaultLanguage(function () use ($data) {

            $saved = true;

            if ( isset($data['title']) && empty($data['slug']) ) {
                $data['slug'] = $data['title'];
            }

            $entity = $this->firstOrNew($data);

            $data = collect($data)
              ->merge([
                'type'   => 'menu',
                'model'  => 'Menus',
                'teaser' => 1,
                'active' => true,
              ]);

            $entity = $this->patchEntity($entity, $data->toArray(),
              ['validate' => 'menu']);

            if ( ! empty($entity->getErrors()) ) {
                return false;
            }

            $neighbors = [];

            foreach ( $this->find('domains')->toArray() as $domain ) {

                /** @var \App\Model\Entity\Menu $domain */

                $domainData = [
                  'parent_id' => $domain->get('id'),
                  'aco_id'    => $domain->get('aco_aco_id'),
                ];

                $neighbors[] = $this->newEntity($domainData);
            }

            if ( ! $entity->isNew() ) {

                $condition = [
                  'slug' => $entity->get('slug'),
                  'type' => 'menu',
                ];

                $neighbors = $this->find()->where($condition)->toArray();
            }

            $saveData = $data->only([
              'type', 'title', 'slug', 'active', 'model', 'teaser',
            ])->toArray();

            foreach ( $neighbors as $neighbor ) {
                $saved = $saved && ! ! $this->patchAndSave($neighbor, $saveData,
                    [], ['callbacks' => false]);
            }

            return $saved;

        });

        AuthManager::resume();

        return $saved;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function findDomains(Query $query, array $options = [])
    {
        return $query->where(['type' => 'domain']);
    }

    public function findMenus(Query $query, array $options = [])
    {
        return $query
          ->where([
            'type' => 'menu',
          ])
          ->order([
            'lft' => 'asc',
          ])
          ->group([
            'title',
          ]);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function findPublished(Query $query, $options = [])
    {
        $alias = $query->getRepository()->getAlias();

        if ( config('preview', false) ) {
            return $query;
        }

        $conditions = [
          [
            'OR' => [
              "{$alias}.begin_publishing IS" => null,
              "{$alias}.begin_publishing <=" => date('Y-m-d H:i:s'),
            ],
          ],
          [
            'OR' => [
              "{$alias}.end_publishing IS" => null,
              "{$alias}.end_publishing >=" => date('Y-m-d H:i:s'),
            ],
          ],
        ];

        $query->where([
          "{$alias}.active" => true, 'AND' => $conditions,
        ]);

        return $query;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function findUnpublished(Query $query, $options)
    {
        $alias = $query->getRepository()->getAlias();

        if ( config('preview', false) ) {
            return $query;
        }

        $conditions = [
          [
            "{$alias}.begin_publishing IS NOT" => null,
            "{$alias}.begin_publishing >"      => date('Y-m-d H:i:s'),
          ],
          [
            "{$alias}.end_publishing IS NOT" => null,
            "{$alias}.end_publishing <"      => date('Y-m-d H:i:s'),
          ],
        ];

        $query->where([
          'OR' => ["{$alias}.active" => false, 'OR' => $conditions],
        ]);

        return $query;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function getChildren($parent, $options = [])
    {
        $defaultOptions = [
          'level'   => false,
          'contain' => [],
          'order'   => ['Menus.lft' => 'ASC'],
          'query'   => $this->find(),
        ];

        $options = array_merge($defaultOptions, $options);

        if ( empty($parent) ) {
            return [];
        }


        $query = $options['query'];

        $query->where([
          'Menus.lft >'  => $parent->lft,
          'Menus.rght <' => $parent->rght,
        ]);

        if ( $options['level'] ) {
            $levels = [];
            for ( $i = 1; $i <= $options['level']; $i++ ) {
                $levels[] = $parent->level + $i;
            }

            $query->where(['Menus.level IN' => $levels]);
        }

        if ( ! empty($options['contain']) ) {
            $query->contain($options['contain']);
        }
        if ( ! Language::isDefaultLanguage() ) {
            $query->where([
              'MenusTranslation.locale' => Language::getLanguage(),
            ]);
        }

        if ( AuthManager::isActive() ) {
            $deniedUrls = AuthManager::getPerms('urls.deniedUrls');
            if ( ! empty($deniedUrls) ) {
                $query->where(function ($exp, $q) use ($deniedUrls) {
                    return $exp->notIn('Menus.path', $deniedUrls);
                });
            }
        }

        $collection = $query->find('published')->order($options['order']);


        $tree = $collection->nest('id', 'parent_id')->toArray();

        foreach ( $tree as $k => $node ) {
            if ( $node->parent_id != $parent->id || ! $node->published ) {
                unset($tree[$k]);
            }
        }

        return array_values($tree);

    }

    function deleteTranslation($articleId, $locale)
    {
        $translationsTable = TableRegistry::getTableLocator()
          ->get('MenusTranslations');

        $menus = $this->find()->where(['foreign_key' => $articleId])->all();

        foreach ( $menus as $menu ) {
            $translatedEntity = $translationsTable->find()->where([
              'id'     => $menu->id,
              'locale' => $locale,
            ])->first();

            if ( ! empty($translatedEntity) ) {
                $translationsTable->delete($translatedEntity);
            }
        }
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getCanonicalUrl($data)
    {

        if ( count($data->menus) > 1 ) {
            $current = request()->url();

            $urls = [];

            foreach ( $data->menus as $k => $menu ) {
                $urls[] = $menu->path;
            }

            usort($urls, function ($a, $b) {
                return strlen($a) - strlen($b);
            });

            if ( isset($urls[0]) && $current != $urls[0] ) {
                return request()->scheme() . $urls[0];
            }
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');
        $schema->setColumnType('extended_data_cfg', 'json');

        return $schema;
    }

}
