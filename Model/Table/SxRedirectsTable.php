<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxRedirectsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'from_url',
    ];

    /**
     * @var array $exportDefaults
     */
    public $exportDefaults = [

        'strategy' => 'except',

        'fields' => [
            'generated',
            'locked',
            'notify',
            'status',
            'created',
            'modified',
        ],

        'contain' => [

        ],

    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('redirects');
        $this->setDisplayField('from_url');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('from_url')
            ->allowEmptyString('from_url');

        $validator
            ->scalar('target_url')
            ->allowEmptyString('target_url');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('generated')
            ->notEmptyString('generated');

        $validator
            ->boolean('locked')
            ->notEmptyString('locked');

        $validator
            ->boolean('notify')
            ->notEmptyString('notify');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->allowEmptyString('status');


        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        $data['notify'] = ! empty($options['notify']) ? $options['notify'] : false;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( isset($options['callbacks']) && $options['callbacks'] === false ) {
            return;
        }

        if ( $entity->isDirty('from_url') === false ) {
            return;
        }

        if ( $entity->isDirty('target_url') === false ) {
            return;
        }

        $entity->set('locked', true);
        $entity->set('status', 'Manuell geändert');
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getRedirectByPath($path)
    {
        $query = $this->find()
            ->where([
                'active' => 1,
                'from_url' => $path,
            ]);

        $redirect = $query->first();

        if ( $redirect == null ) {
            return $redirect;
        }

        return $this->getRealTargetUrl($redirect);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getRealTargetUrl(EntityInterface $entity)
    {
        $query = $this->find()
            ->where([
                'active' => 1,
                'from_url' => $entity->target_url,
            ]);

        $newEntity = $query->first();

        if ( $newEntity == null ) {
            return $entity;
        }

        $entity->target_url = $newEntity->target_url;

        return $this->getRealTargetUrl($entity);
    }

}
