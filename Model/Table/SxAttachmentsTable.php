<?php

namespace App\Model\Table;

use App\Library\AuthManager;
use App\Library\Facades\Language;
use App\Library\Proffer\AttachmentPath;
use App\Library\Proffer\AttachmentTransform;
use App\Model\Entity\Attachment;
use App\Utility\StringUtility;
use Cake\Core\Configure;
use Cake\Database\Expression\QueryExpression;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Utility\Text;
use mm\Mime\Type;
use PHPExif\Reader\Reader;
use Proffer\Lib\ProfferPath;

/**
 * @version 4.0.0
 */
class SxAttachmentsTable extends AppTable
{
    /**
     * @var array
     */
    public $defaultSearchFields = [
        'id', 'name', 'title', 'alternative',
    ];

    public $defaultContains = [
        'Thumbnail',
    ];

    /**
     * @inheritdoc
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('file', 'proffer.file');

        return $schema;
    }

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attachments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree', [
            'recoverOrder' => ['lft'],
            'priority'     => 11,
        ]);

        $this->belongsTo('ParentAttachments', [
            'className'  => 'Attachments',
            'foreignKey' => 'parent_id',
        ]);

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('Attached', [
            'foreignKey' => 'attachment_id',
        ]);

        $this->belongsTo('Thumbnail', [
            'className'  => 'Attachments',
            'foreignKey' => 'thumb_id',
        ]);

        $this->hasMany('ChildAttachments', [
            'className'  => 'Attachments',
            'foreignKey' => 'parent_id',
        ]);

        $this->belongsToMany('Articles', [
            'foreignKey'       => 'attachment_id',
            'targetForeignKey' => 'foreign_key',
            'joinTable'        => 'attached',
            'strategy'         => 'subquery',
        ]);

        // Add the behaviour and configure any options you want
        $this->addBehavior('Proffer.Proffer', [
            // The name of your upload field
            'file' => [
                'root'           => MEDIA,
                // Customise the root upload folder here, or omit to use the default
                'dir'            => 'dirname',

                // The name of the field to store the folder
                'thumbnailSizes' => Configure::read('Media.formats'),
                'pathClass'      => AttachmentPath::class,
                'transformClass' => AttachmentTransform::class,
            ],
        ]);

        $this->addBehavior('Translation', [
            'defaultLocale' => Language::getDefaultLanguage(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeMarshal(EventInterface $event, $data, $options)
    {
        if ( ! isset($data['file']) || ! empty($data['file']) ) {
            return;
        }

        unset($data['file']);
    }

    /**
     * @inheritdoc
     */
    public function afterRules(Event $event, EntityInterface $entity, $result)
    {
        $parentId = $entity->get('parent_id');

        if ( $parentId && $parent = $this->get($parentId) ) {
            $entity->set(['aco_id' => $parent->get('aco_id'), 'dirname' => $parent->get('dirname')]);
        }

        if ( $entity->get('file') ) {
            $this->generateFromFile($entity);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $isDirtyName = ! $entity->isNew() &&
            $entity->isDirty('name');

        $isDirtyPath = ! $entity->isNew() &&
            $entity->isDirty('dirname');

        if ( $isDirtyName || $isDirtyPath ) {
            $this->renameAllVersions($entity);
        }

        if ( $entity->isNew() && empty($entity->title) ) {
            $entity->title = StringUtility::human($entity->name);
        }

        return parent::beforeSave($event, $entity, $options);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getVersionPath($entity, $version = 'uploads')
    {
        return MEDIA . $entity->dirname . DS . $version . DS . $entity->basename;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getVersionUrl($entity, $version = 'uploads')
    {
        return MEDIA_URL . $entity->dirname . '/' . $version . '/' . $entity->basename;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function convertUrlToFile($entity, $options = [])
    {
        $options += [
            'file' => 'file', 'name' => 'name',
        ];

        // Get file from entity
        $file = $entity->get($options['file']);

        // Get name from entity
        $name = $entity->get($options['name']);

        if ( empty($name) ) {
            $name = pathinfo($file, PATHINFO_BASENAME);
        }

        // Get file extension
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        // Move file to server
        copy($file, $path = TMP . Text::uuid() . '.' . $extension);

        $entity->set('file', [
            'name'     => $name . '.' . $extension,
            'tmp_name' => $path,
            'error'    => UPLOAD_ERR_OK,
            'size'     => filesize($path),
            'type'     => mime_content_type($path),
        ]);

        return $entity;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function convertObjectToFile($entity, $options = [])
    {
        /** @var \Zend\Diactoros\UploadedFile $file */

        $options += [
            'file' => 'file', 'name' => 'name',
        ];

        // Get file from entity
        $file = $entity->get($options['file']);

        // Get name from entity
        $name = $entity->get($options['name']);

        if ( ! $entity->isNew() || empty($name) ) {
            $name = pathinfo($file->getClientFilename(), PATHINFO_FILENAME);
        }

        // Get file extension
        $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);

        // Move file to server
        $file->moveTo($path = $path = TMP . Text::uuid() . '.' . $extension);

        $entity->set('file', [
            'name'     => $name . '.' . $extension,
            'tmp_name' => $path,
            'error'    => UPLOAD_ERR_OK,
            'size'     => filesize($path),
            'type'     => mime_content_type($path),
        ]);

        return $entity;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function generateFromFile(Attachment $entity)
    {
        $file = $entity->get('file');

        if ( is_string($file) ) {
            $this->convertUrlToFile($entity);
        }

        if ( is_object($file) ) {
            $this->convertObjectToFile($entity);
        }

        // Get name from file
        $fileName = $entity->get('file.name');

        $entity->set('name_original', pathinfo($fileName,
            PATHINFO_BASENAME));

        if ( ! $entity->isNew() ) {
            return $this->replaceFile($entity);
        }

        $entity->set('name', pathinfo($fileName,
            PATHINFO_FILENAME));

        // Guess filetype
        $fileType = Type::guessName($fileName);

        if ( empty($entity->type) ) {
            $entity->set('type', $fileType);
        }

        for ( $i = 0; $i === 0 || file_exists($entity->getPath()); $i++ ) {
            $entity->set('basename', StringUtility::file($fileName, $fileName, $i));
        }

        $entity->set('thumb', null);

        $hasThumbnail = $entity->get('type') === 'image' ||
            $entity->get('provider', null);

        if ( $hasThumbnail ) {
            $entity->set('thumb', $entity->getUrl('crop'));
        }

        if ( $entity->get('type') === 'svg' ) {
            $entity->set('thumb', $entity->getUrl('uploads'));
        }

        if ( $entity->get('type') === 'gif' ) {
            $entity->set('thumb', $entity->getUrl('uploads'));
        }

        $filePath = $entity->get('file.tmp_name');

        $metaData = [
            'FileSize'  => filesize($filePath),
            'Extension' => pathinfo($filePath, PATHINFO_EXTENSION),
        ];

        $exif = Reader::factory(Reader::TYPE_NATIVE)
            ->getExifFromFile($filePath);

        if ( $exif && empty($entity->copyright) ) {
            $entity->set('copyright', trim(str_replace('©', ' ', $exif->getCopyright())) ?: null);
        }

        if ( $exif ) {
            $metaData = array_merge($metaData, (array) $exif->getData());
        }

        $entity->set('metadata', $metaData);

        return $entity;
    }

    public function replaceFile(Attachment $entity)
    {
        // Get name from file
        $fileName = $entity->get('file.name');

        // Guess filetype
        $fileType = Type::guessName($fileName);

        if ( $entity->type !== $fileType ) {
            throw new \Exception('File can only be replaced by the same type (' . $entity->type . ')');
        }

        $this->deleteImageCache($entity->get('basename'));

        return $entity;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function whitespaceImage($data = [])
    {
        ini_set('memory_limit', '512M');

        /** @var \App\Model\Entity\Attachment $entity */
        $entity = $this->get($data['id']);

        $transform = new AttachmentTransform;

        $image = $transform->getImagine()
            ->open($entity->getPath());

        $image = $transform->whitespaceImage($image);

        $image->save($this->getVersionPath($entity), [
            'quality' => 100,
        ]);

        $crop = config('Media.formats.crop', null);

        if ( $crop !== null ) {

            if ( $crop['transform']['type'] === 'scalecrop' ) {
                $clone = $transform->scalecropImage($image, $crop['size']);
            }

            if ( $crop['transform']['type'] === 'fitinside' ) {
                $clone = $transform->fitinsideImage($image, $crop['size']);
            }

            if ( isset($clone) === true ) {
                $clone->save($this->getVersionPath($entity, 'crop'), $crop['imagine']);
            }

        }

        $entity->set('thumb', $entity->getUrl('crop'));

        return $this->save($entity);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function cropImage($data, $force = true)
    {
        ini_set('memory_limit', '2048M');

        /** @var \App\Model\Entity\Attachment $entity */
        $entity = $this->get($data['id']);

        $config = $this->getBehavior('Proffer')->getConfig('file');

        $path = new AttachmentPath($this, $entity, 'file', $config);

        $transform = new AttachmentTransform($this, $path);

        $image = $transform->getImagine()->open($entity->getPath());

        $versions = $entity->get('versions');

        foreach ( $data['versions'] as $key => $value ) {

            $filePath = $this->getVersionPath($entity, $key);

            if ( $key === 'crop' ) {
                continue;
            }

            if ( ! $force && file_exists($filePath) ) {
                continue;
            }

            foreach ( ['w', 'h', 'x', 'y'] as $index ) {
                if ( isset($value[$index]) ) {
                    $value[$index] = round($value[$index], 2);
                }
            }

            $config = config("Media.formats.{$key}");

            [$clone, $instruction] = $transform->buildFromCropInstructions($image, [
                'force' => $value, 'target' => $config['size'], 'transform' => $config['transform']['type']
            ]);

            if ( ! isset($clone) ) {
                continue;
            }

            $entity->set("instructions.{$key}", $instruction['size']);

            $clone->save($filePath, $config['imagine']);
        }

        $this->deleteImageCache($entity->get('basename'));

        return $this->save($entity);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function focusImage($data, $resize = true)
    {
        ini_set('memory_limit', '512M');

        /** @var \App\Model\Entity\Attachment $entity */
        $entity = $this->get($data['id']);

        $config = $this->getBehavior('Proffer')->getConfig('file');

        $path = new AttachmentPath($this, $entity, 'file', $config);

        $transform = new AttachmentTransform($this, $path);

        $image = $transform->getImagine()
            ->open($entity->getPath());

        $instructions = [];

        foreach ( config('Media.formats') as $key => $config ) {

            $filePath = $this->getVersionPath($entity, $key);

            if ( $key === 'crop' ) {
                continue;
            }

            [$clone, $instruction] = $transform->buildFromFocusInstructions($image, [
                'focus' => $data['focus'], 'target' => $config['size'], 'transform' => $config['transform']['type']
            ]);

            if ( ! isset($clone) ) {
                continue;
            }

            $entity->set("instructions.{$key}", $instruction['size']);

            if ( ! $resize ) {
                continue;
            }

            $clone->save($filePath, $config['imagine']);
        }

        $entity->set('instructions.focuspoint', $data['focus']);

        $this->deleteImageCache($entity->get('basename'));

        return $this->save($entity);
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function renameAllVersions(Attachment $entity)
    {
        if ( $entity->isEmpty('basename') ) {
            return;
        }

        $this->deleteImageCache($entity->get('basename'));

        /* @var \App\Model\Entity\Attachment $original */
        $original = $this->newEntity($entity->getOriginalValues());

        $isDirty = $entity->get('name') === $original->get('name') &&
            $entity->get('dirname') === $original->get('dirname');

        if ( $isDirty ) {
            return $entity;
        }

        for ( $i = 0; file_exists($entity->getPath()); $i++ ) {
            $entity->set('basename', StringUtility::file($entity->get('name'),
                $entity->get('basename'), $i));
        }

        foreach ( config('Media.formats') as $key => $value ) {
            if ( file_exists($original->getPath($key)) ) {
                rename($original->getPath($key), $entity->getPath($key));
            }
        }

        if ( file_exists($original->getPath()) ) {
            rename($original->getPath(), $entity->getPath());
        }

        $entity->set('thumb', $entity->getUrl('crop'));

        return $entity;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function addOrRenameStandardFolderSet($data)
    {
        $baseFolder = $this->find()
            ->where([
                'aco_id' => $data['aco_id'], 'parent_id IS NULL',
            ])
            ->first();

        if ( empty($baseFolder) ) {
            $baseFolder = $this->newEntity($data);
        }

        $data['type'] = 'folder';

        if ( empty($baseFolder->user_id) ) {
            $data['user_id'] = AuthManager::getUser('id');
        }

        if ( empty($baseFolder->dirname) ) {
            $data['dirname'] = StringUtility::slug($data['name']);
        }

        $baseFolder = $this->patchEntity($baseFolder, $data);

        $hasChanged = ! $baseFolder->isNew() &&
            $baseFolder->isDirty('dirname');

        $sourcePath = 'media/' . $baseFolder->getOriginal('dirname') . '/';
        $targetPath = 'media/' . $baseFolder->get('dirname') . '/';

        if ( $hasChanged ) {
            rename(WWW_ROOT . $sourcePath, WWW_ROOT . $targetPath);
        }

        if ( ! $this->save($baseFolder) ) {
            throw new \Exception(__d('system', 'Der Hauptordner kann nicht gespeichert werden,'));
        }

        if ( $hasChanged ) {

            $expression = new QueryExpression("REPLACE(thumb, '{$sourcePath}'" .
                ", '{$targetPath}')");

            $replaceData = [
                'thumb'   => $expression,
                'dirname' => $baseFolder->get('dirname'),
            ];

            $replaceCondition = [
                'aco_id' => $baseFolder->get('aco_id'),
            ];

            $this->updateAll($replaceData, $replaceCondition);
        }

        foreach ( ['Bilder', 'Dokumente', 'Videos'] as $name ) {

            $context = array_merge($data, [
                'parent_id' => $baseFolder->id,
                'name'      => $name,
                'type'      => 'folder',
                'dirname'   => '',
            ]);

            $query = $this->find()
                ->where([
                    'aco_id'    => $context['aco_id'],
                    'parent_id' => $context['parent_id'],
                    'name'      => $context['name'],
                ]);

            $folder = $query->first();

            if ( $folder ) {
                $folder = $this->patchEntity($folder, $context);
            } else {
                $folder = $this->newEntity($context);
            }

            if ( $this->save($folder) ) {
                continue;
            }

            throw new \Exception(__d('system', 'Die Unterordner konnten nicht gespeichert werden,'));
        }


        return true;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function addSharedFolderSet()
    {
        $this->addOrRenameStandardFolderSet([
            'name' => 'Gemeinsam genutzte Dateien', 'aco_id' => SHARED_FOLDER_ACO,
        ]);
    }

    // TODO: was ist das?
    function generateVersions()
    {
        $proffer = $this->behaviors()->get('Proffer');

        $config = $proffer->config();

        foreach ( $config as $field => $settings ) {
            $records = $this->{$this->setTable->getAlias()}->find()->select([$this->setTable->getPrimaryKey(), $field, $settings['dir']])->where([
                "$field IS NOT NULL",
                "$field != ''",
            ]);

            foreach ( $records as $item ) {
                if ( $this->param('verbose') ) {
                    $this->out(__('Processing ' . $this->setTable->getAlias() . ' ' . $item->get($this->setTable->getPrimaryKey())));
                }

                if ( ! empty($this->param('path-class')) ) {
                    $class = (string) $this->param('path-class');
                    $path = new $class($this->setTable, $item, $field, $settings);
                } else {
                    $path = new ProfferPath($this->setTable, $item, $field, $settings);
                }

                if ( ! empty($this->param('image-class')) ) {
                    $class = (string) $this->param('image-class');
                    $transform = new $class($this->setTable, $path);
                } else {
                    $transform = new ImageTransform($this->setTable, $path);
                }

                $transform->processThumbnails($settings);

                if ( $this->param('verbose') ) {
                    $this->out(__('Thumbnails regenerated for ' . $path->fullPath()));
                } else {
                    $this->out(__('Thumbnails regenerated for ' . $this->setTable->getAlias() . ' ' . $item->get($field)));
                }
            }
        }
    }

    /**
     * @param \Cake\Event\Event $event
     * @param \Cake\Datasource\EntityInterface $entity
     * @param \ArrayObject $options
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {

        foreach ( $entity->get('children', []) as $item ) {
            $attachmentPath = new AttachmentPath($this, $item, null, []);
            $attachmentPath->deleteFiles();
            $this->deleteImageCache($item->get('basename', false));
        }

        $this->deleteImageCache($entity->get('basename', false));

        return true;

    }

    /**
     * @param \Cake\Event\Event $event
     * @param \Cake\Datasource\EntityInterface $entity
     * @param \ArrayObject $options
     */
    public function beforeDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $entity->set('children', []);
        if ( $entity->get('type') === 'folder' ) {
            $filesToDelete = $this->find()->where([
                'lft >'   => $entity->get('lft'),
                'rght <'  => $entity->get('rght'),
                'type !=' => 'folder',
            ])->all()->toArray();

            $entity->set('children', $filesToDelete);
        }

        return true;
    }

    /**
     * Delete all cached files or one specific
     *
     * @param bool $allOrFile
     */
    function deleteImageCache($allOrFile = true)
    {
        $baseDir = WWW_ROOT . 'dist' . DS . 'cache' . DS . 'img';

        if ( $allOrFile === true ) {
            $windows = defined('PHP_WINDOWS_VERSION_MAJOR');
            if ( $windows ) {
                system("rmdir " . escapeshellarg($baseDir) . " /s /q");
            } else {
                exec('rm -rf ' . $baseDir);
            }
        } else {
            if ( ! empty($allOrFile) ) {
                $paths = glob($baseDir . '/*/*/*/*/' . $allOrFile);
                foreach ( $paths as $path ) {
                    unlink($path);
                }
            }
        }
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function findAttached(Query $query, $options = [])
    {
        $alias = $query->getRepository()->getAlias();

        $conditions = [
            "(SELECT COUNT(*) FROM attached WHERE attachment_id = {$alias}.id) != 0",
        ];

        $query->where($conditions);

        return $query;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function findDetached(Query $query, $options = [])
    {
        $alias = $query->getRepository()->getAlias();

        $conditions = [
            "(SELECT COUNT(*) FROM attached WHERE attachment_id = {$alias}.id) == 0",
        ];

        $query->where($conditions);

        return $query;
    }

}
