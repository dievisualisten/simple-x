<?php

namespace App\Model\Table;

use Cake\Database\Schema\TableSchemaInterface;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxFormularsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'name',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Formularconfigs'
    ];

    /**
     * @inheritdoc
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');
        $schema->setColumnType('extended_data_cfg', 'json');

        return $schema;
    }

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('formulars');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('Formularconfigs', [
            'foreignKey' => 'formular_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('component')
            ->maxLength('component', 255)
            ->allowEmptyString('component');

        $validator
            ->boolean('horizontal')
            ->allowEmptyString('horizontal');

        $validator
            ->scalar('alignment')
            ->maxLength('alignment', 255)
            ->allowEmptyString('alignment');

        $validator
            ->scalar('btnname')
            ->maxLength('btnname', 255)
            ->allowEmptyString('btnname');

        $validator
            ->boolean('captcha')
            ->allowEmptyString('captcha');

        $validator
            ->boolean('use_login')
            ->allowEmptyString('use_login');

        $validator
            ->boolean('send_email')
            ->allowEmptyString('send_email');

        $validator
            ->boolean('send_attachments')
            ->allowEmptyString('send_attachments');

        $validator
            ->boolean('save_attachments')
            ->allowEmptyString('save_attachments');

        $validator
            ->boolean('generate_attachments')
            ->allowEmptyString('generate_attachments');

        $validator
            ->allowEmptyString('extended_data');

        $validator
            ->boolean('use_conversion_tracking')
            ->allowEmptyString('use_conversion_tracking');

        $validator
            ->scalar('conversion_tracking')
            ->allowEmptyString('conversion_tracking');

        return $validator;
    }

}
