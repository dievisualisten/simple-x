<?php

namespace App\Model\Table;

use App\Library\Facades\Language;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


/**
 * @version 4.0.0
 */
class SxMenusTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('menus_translations');
        $this->setDisplayField('title');
        $this->setPrimaryKey(['id', 'locale']);
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('extended_data', 'json');

        return $schema;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        TableRegistry::getTableLocator()->get('Menus')
            ->afterTranslationDelete($event, $entity, $options);
    }

}
