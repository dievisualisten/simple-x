<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxFormularconfigsTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('formularconfigs_translations');
        $this->setDisplayField('id');
        $this->setPrimaryKey(['id', 'locale']);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Formularconfigs', [
            'foreignKey' => 'id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 10)
            ->allowEmptyString('locale', null, 'create');

        $validator
            ->scalar('after_send_headline')
            ->maxLength('after_send_headline', 255)
            ->allowEmptyString('after_send_headline');

        $validator
            ->scalar('after_send_content')
            ->maxLength('after_send_content', 429496729)
            ->allowEmptyString('after_send_content');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 255)
            ->allowEmptyString('subject');

        return $validator;
    }

}
