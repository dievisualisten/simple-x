<?php

namespace App\Model\Table;

use App\Library\Facades\Language;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxAttachedTable extends AppTable
{
    public $defaultContains = [
        'Attachments', 'Attachments.Thumbnail',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attached');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Translation', [
            'defaultLocale' => Language::getDefaultLanguage(),
            'forceRealLocale' => true,
        ]);

        $this->belongsTo('Attachments', [
            'foreignKey' => 'attachment_id',
            'joinType' => 'INNER',
        ]);

    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->uuid('foreign_key')
//            ->requirePresence('foreign_key', 'create')
            ->notEmptyString('foreign_key');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->requirePresence('model', 'create')
            ->notEmptyString('model');

        $validator
            ->integer('sequence')
            ->notEmptyString('sequence');

        $validator
            ->scalar('alternative')
            ->maxLength('alternative', 255)
            ->allowEmptyString('alternative');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['attachment_id'], 'Attachments'), [
            'errorField' => 'attachment_id',
        ]);

        return $rules;
    }
}
