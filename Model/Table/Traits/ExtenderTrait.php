<?php

namespace App\Model\Table\Traits;

trait ExtenderTrait {

    protected function _getTraitName($object): string
    {
        $traitName = preg_replace('/^(.*?)([^\\\]+)?$/', '$2', $object);

        return preg_replace('/Trait$/', '', $traitName);
    }

    protected function _hasTraitMethod($object, $method): bool
    {
        return in_array($method, get_class_methods($object));
    }

    public function loopTraits($method, $args = []): void
    {
        foreach ( class_uses(self::class) as $trait ) {

            $finalMethod = $method . $this->_getTraitName($trait);

            if ( ! $this->_hasTraitMethod($trait, $finalMethod) ) {
                continue;
            }

            $this->{$finalMethod}(...$args);
        }
    }

}