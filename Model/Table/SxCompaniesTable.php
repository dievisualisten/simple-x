<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxCompaniesTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('foreign_key');

        /* $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->requirePresence('model', 'create')
            ->notEmptyString('model');*/

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->allowEmptyString('company');

        $validator
            ->scalar('vatid')
            ->maxLength('vatid', 255)
            ->allowEmptyString('vatid');

        $validator
            ->scalar('taxid')
            ->maxLength('taxid', 255)
            ->allowEmptyString('taxid');

        $validator
            ->scalar('regnr')
            ->maxLength('regnr', 255)
            ->allowEmptyString('regnr');

        $validator
            ->scalar('court')
            ->maxLength('court', 255)
            ->allowEmptyString('court');

        $validator
            ->scalar('note')
            ->allowEmptyString('note');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        $entity->set('type', $event->getSubject()->getAlias());


    }

}
