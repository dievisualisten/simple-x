<?php

namespace App\Model\Table\AppTraits;

use App\Model\Entity\Article;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Time;
use Safe\DateTime;
use When\When;

trait EventsTrait
{

    public function beforeSaveEvents(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( $entity->get('extended_data_cfg.event.singleoccation', false) ) {
            $this->createSingleEvent($entity);
        }

        if ( $entity->get('extended_data_cfg.event.multipleoccation', false) ) {
            $this->createMultipleEvent($entity);
        }
    }

    public function createSingleEvent($entity)
    {
        $eventStartDate = $entity->get('extended_data_cfg.event.start_date', null);

        if ( empty($eventStartDate) ) {
            return;
        }

        $events = [
            Time::parse($eventStartDate)->format('Y-m-d'),
        ];

        $entity->set('extended_data_cfg._events', $events);
    }

    public function createMultipleEvent(Article $entity)
    {
        $eventStartDate = $entity->get('extended_data_cfg.event.start_date', null);

        if ( empty($eventStartDate) ) {
            return;
        }

        $eventEndDate = $entity->get('extended_data_cfg.event.end_date', null);

        if ( empty($eventEndDate) ) {
            $eventEndDate = 'now+5years';
        }

        // Start date time
        $startDate = new DateTime($eventStartDate);

        // End date time
        $endDate = new DateTime($eventEndDate);

        // Create date builder for occurences
        $dateBuilder = (new When);

        if ( $entity->get('extended_data_cfg.event.weeklyoccation', false) ) {

            $days = $entity->get('extended_data_cfg.event.weeklydays', []);

            if ( empty($days) ) {
                $days = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];
            }

            $interval = $entity->get('extended_data_cfg.event.weeklyinterval', 0);

            if ( ! empty($interval) ) {
                $dateBuilder->interval($interval);
            }

            $dateBuilder->freq('weekly')->byday($days);
        }

        if ( $entity->get('extended_data_cfg.event.monthlyoccation', false) ) {

            $date = $entity->get('extended_data_cfg.event.monthlydate', 1);

            if ( $date === 'last' ) {
                $date = -1;
            }

            $day = $entity->get('extended_data_cfg.event.monthlyday', 'day');

            if ( $day === 'day' ) {
                $dateBuilder->bymonthday($date);
            }

            if ( $day !== 'day' ) {
                $dateBuilder->byday($date . $day);
            }

            $interval = $entity->get('extended_data_cfg.event.monthlyinterval', 0);

            if ( ! empty($interval) ) {
                $dateBuilder->interval($interval);
            }

            $dateBuilder->freq('monthly');
        }

        $intervalLimit = $entity->get('extended_data_cfg.event.intervallimit', 0);

        if ( ! empty($intervalLimit) ) {
            $dateBuilder->count($intervalLimit);
        }

        for ( $i = 0; $i < config('app.calendar.limit.default', 3650); $i++ ) {
            try {

                $dateBuilder->startDate($startDate->modify('+1day'))
                    ->generateOccurrences();

                break;

            } catch ( \Exception $exception ) {
                continue;
            }
        }

        $events = [];

        foreach ( $dateBuilder->getOccurrencesBetween($startDate, $endDate) as $date ) {
            /** @var Time $date */
            $events[] = $date->format('Y-m-d');
        }

        $entity->set('extended_data_cfg._events', $events);
    }

}