<?php

namespace App\Model\Table\AppTraits;

use App\Library\AuthManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

trait ElementsTrait
{

    public $defaultElements = [];

    /**
     * @inheritdoc
     */
    public function beforeSaveElements(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function afterSaveElements(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( ! isset($this->ArticlesElements) ) {
            return;
        }

        AuthManager::unprotected(function () use ($entity) {

            /**
             * @var \App\Model\Entity\Article $entity
             */

            if ( ! isset($entity->elements) ) {
                $entity->makeElements();
            }

            $this->ArticlesElements->saveArticle($entity);
        });
    }

    public function getDefaultElements(): array
    {
        return $this->defaultElements;
    }

    public function addElementType($type, $options = [])
    {
        $attributeName = Inflector::pluralize($type);

        array_push($this->defaultElements,
            $attributeName);

        $relationName = ucfirst($attributeName);

        array_push($this->defaultContains,
            $relationName,
            "{$relationName}.Attached",
            "{$relationName}.Attached.Attachments",
            "{$relationName}.Attached.Attachments.Thumbnail");

        $defaultOptions = [
            'className' => 'Elements',
            'targetForeignKey' => 'element_id',
            'dependent' => false,
            'saveStrategy' => 'replace',
            'through' => 'ArticlesElements',
            'sort' => 'ArticlesElements.sequence ASC',
        ];

        $defaultOptions['conditions'] = [
            "{$relationName}.type" => $type
        ];

        $options = array_merge($defaultOptions, $options);

        $this->belongsToMany($relationName, $options);
    }

}