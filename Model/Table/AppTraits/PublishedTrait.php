<?php

namespace App\Model\Table\AppTraits;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;

trait PublishedTrait
{

    /**
     * @inheritdoc TODO: DOC
     */
    function findPublished(Query $query, $options = [])
    {
        $alias = $query->getRepository()->getAlias();

        if ( config('preview', false) ) {
            return $query;
        }

        $conditions = [
            [
                'OR' => [
                    "{$alias}.begin_publishing IS" => null,
                    "{$alias}.begin_publishing <=" => date('Y-m-d H:i:s'),
                ],
            ],
            [
                'OR' => [
                    "{$alias}.end_publishing IS" => null,
                    "{$alias}.end_publishing >=" => date('Y-m-d H:i:s'),
                ],
            ],
        ];

        $query->where([
            "{$alias}.active" => true, 'AND' => $conditions,
        ]);

        return $query;
    }

    /**
     * @inheritdoc TODO: DOC
     */
    function findUnpublished(Query $query, $options)
    {
        $alias = $query->getRepository()->getAlias();

        if ( config('preview', false) ) {
            return $query;
        }

        $conditions = [
            [
                "{$alias}.begin_publishing IS NOT" => null,
                "{$alias}.begin_publishing >"      => date('Y-m-d H:i:s'),
            ],
            [
                "{$alias}.end_publishing IS NOT" => null,
                "{$alias}.end_publishing <"      => date('Y-m-d H:i:s'),
            ],
        ];

        $query->where([
            'OR' => ["{$alias}.active" => false, 'OR' => $conditions],
        ]);

        return $query;
    }

}