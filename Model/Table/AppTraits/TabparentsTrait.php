<?php

namespace App\Model\Table\AppTraits;

use App\Library\AuthManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

trait TabparentsTrait
{
    /**
     * @inheritdoc
     */
    public function beforeSaveTabparents(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( isset($entity->tabparents) || ! isset($entity->id) || ! @PHPUNIT_MODE ) {
            return;
        }

        /**
         * Added auto fill for tabparents in test mode
         */
        $parents = TableRegistry::getTableLocator()->get('Menus')->find()
            ->where([
                'foreign_key' => $entity->id,
            ])
            ->select([
                'id' => 'parent_id',
                'teaser' => 'teaser',
            ]);

        $entity->tabparents = $parents->enableHydration(false)->toArray();
    }

    /**
     * @inheritdoc
     */
    public function afterSaveTabparents(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        AuthManager::unprotected(function () use ($entity) {

            /**
             * @var \App\Model\Entity\Article $entity
             */

            if ( ! isset($entity->tabparents) ) {
                $entity->makeTabparents();
            }

            $this->Menus->saveMenus($entity, 'Article');
        });
    }

}