<?php

namespace App\Model\Table\AppTraits;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;

trait AttachedTrait
{
    /**
     * @inheritdoc
     */
    public function beforeFindAttached(Event $event, Query $query, \ArrayObject $options, $primary): void
    {
        $query->formatResults(function ($results) {
            return $results->filter()->map([$this, 'prepareAttached']);
        }, $query::APPEND);
    }

    /**
     * Update attached relation with given media
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @param array $media
     * @return \Cake\Datasource\EntityInterface
     */
    public function patchAttachedWithMedia(EntityInterface $entity, array $media): EntityInterface
    {
        $result = [];

        foreach ( $media as $key => $attachments ) {

            $preset = [
                'type' => $key, 'model' => 'Article',
            ];

            foreach ( $attachments as $index => $attachment ) {

                $result[] = array_merge($attachment['attached'], $preset,
                    ['attachment_id' => $attachment['id'], 'sequence' => $index]);
            }

        }

        return $this->patchEntity($entity, ['attached' => $result]);
    }

    /**
     * Update attached relation with given media
     *
     * @param \Cake\Datasource\EntityInterface $entity
     * @return \Cake\Datasource\EntityInterface
     */

    public function prepareAttached(EntityInterface $entity): EntityInterface
    {
        if ( ! isset($entity->attached) ) {
            return $entity;
        }

        foreach ( $entity->attached as $key => $attached ) {

            $data = [
                'locale' => $entity->locale,
            ];

            if ( $entity->locale !== $attached->locale ) {
                $data += [
                    'alternative' => '',
                    'title' => '',
                    'description' => '',
                ];
            }

            $this->Attached->patchEntity($attached, $data);
        }

        return $entity;
    }

}