<?php

namespace App\Model\Table\AppTraits;

use App\Library\AuthManager;
use Cake\Event\Event;
use Cake\ORM\Query;

trait PagetypeTrait
{
    public function getTypeCondition(Query $query)
    {
        if ( ! AuthManager::isActive() ) {
            return $query;
        }

        $query->where([
            "{$this->getAlias()}.type IN" => array_keys($this->getTypes())
        ]);

        return $query;
    }

    /**
     * Get article page layouts
     *
     * @param bool $includeAll
     * @return array
     */
    public function getTypeLayouts($includeAll = false): array
    {
        $articleLayouts = [];

        foreach ( $this->getTypes($includeAll) as $key => $val ) {

            $pageLayouts = AuthManager::getPerms("rights.resource.modules.article.pagetypes.{$key}");

            if ( $includeAll ) {
                $pageLayouts = AuthManager::getPerms("Sx.resource.modules.article.pagetypes.{$key}");
            }

            foreach ( (array) $pageLayouts as $layout => $show ) {

                if ( ! is_array($show) ||  empty($show['show']) ) {
                    continue;
                }

                $articleLayouts[$key][] = [
                    $layout, config("Sx.resource.modules.article.pagetypes.{$key}.{$layout}.name"),
                ];
            }

        }

        return $articleLayouts;
    }

    /**
     * Get article page types
     *
     * @param bool $includeAll
     * @return array
     */
    public function getTypes($includeAll = false): array
    {
        $pageTypes = AuthManager::getPerms('rights.resource.modules.article.pagetypes');

        if ( $includeAll ) {
            $pageTypes = config('Sx.resource.modules.article.pagetypes');
        }

        $articleTypes = [];

        foreach ( (array) $pageTypes as $key => $value ) {

            if ( ! is_array($value) || empty($value['default']['show']) ) {
                continue;
            }

            $articleTypes[$key] = config("Sx.resource.modules.article.pagetypes.{$key}.name");
        }

        asort($articleTypes);

        return $articleTypes;
    }

}

