<?php

namespace App\Model\Table;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxUsersTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'users.login', 'persons.firstname', 'persons.lastname',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Roles',
        'Acos',
        'Persons',
        'Communications',
        'Addresses',
        'Bankaccounts',
    ];

    /**
     * @var array $dublicateContains
     */
    public $dublicateContains = [
        'Persons',
        'Communications',
        'Addresses',
        'Bankaccounts',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Baseacos', [
            'className'  => 'Acos',
            'foreignKey' => 'aco_id',
        ]);

        $this->hasOne('Persons', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Persons.model' => 'Users'],
            'dependent'  => true,
        ]);

        $this->hasOne('Communications', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Communications.model' => 'Users'],
            'dependent'  => true,
        ]);

        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Addresses.model' => 'Users'],
            'dependent'  => true,
        ]);

        $this->hasOne('Bankaccounts', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Bankaccounts.model' => 'Users'],
            'dependent'  => true,
        ]);

        $this->hasOne('Attachments', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Attachments.model' => 'Users'],
            'dependent'  => true,
        ]);

        $this->belongsToMany('Acos', [
            'foreignKey'       => 'user_id',
            'saveStrategy'     => 'replace',
            'targetForeignKey' => 'aco_id',
            'joinTable'        => 'acos_users',
        ]);

        $this->belongsToMany('Roles', [
            'className'        => 'Roles',
            'foreignKey'       => 'user_id',
            'saveStrategy'     => 'replace',
            'targetForeignKey' => 'role_id',
            'sort'             => ['Roles.name' => 'ASC'],
            'joinTable'        => 'roles_users',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->setProvider('simplex', 'App\Model\Validation\Validation');

        $validator
            ->requirePresence('login')
            ->notEmptyString('login', false, 'Bitte geben Sie einen Benutzernamen an.');

        $validator
            ->requirePresence('newpassword', 'create')
            ->notEmptyString('newpassword', false, 'Bitte geben Sie ein Passwort ein');

        $validator
            ->requirePresence('aco_id', 'create')
            ->notEmptyString('aco_id', false, __('Bitte wählen Sie eine Stammzugehörigkeit aus.'));


        $validator
            ->add('newpassword', 'myCustomRuleNameCheckPassword', [
                'rule'     => 'checkPassword',
                'provider' => 'simplex',
                'message'  => __('Bitte geben Sie ein Passwort ein, das den festgelegten Regeln entspricht.'),
            ])
            ->allowEmptyString('newpassword', 'update');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        //$rules = parent::buildRules($rules);

        $rules->add($rules->isUnique(['login']));
        $rules->add($rules->existsIn(['aco_id'], 'Acos'));

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        //Das Baseaco wird in die acos gespeichert und das alte Baseaco wird aus den Acos entfernt.
        if ( isset($data['acos']) ) {

            $acoIds = [];
            $oldBaseAcoId = null;

            if ( isset($data['aco_id']) ) {
                $acoIds[] = $data['aco_id'];
                $userBeforeSave = $this->find()->where(['id' => $data['id']])->first();

                //Wechsel des Baseacos erfolgt, aus den Acos nehmen
                if ( ! empty($userBeforeSave) && $userBeforeSave->aco_id != $data['aco_id'] ) {
                    $oldBaseAcoId = $userBeforeSave->aco_id;
                }
            }

            if ( ! empty($data['acos']) ) {
                foreach ( $data['acos'] as $aco ) {
                    $acoId = is_array($aco) ? $aco['id'] : $aco;
                    if ( $acoId != $oldBaseAcoId ) {
                        $path = $this->Acos->find('path', ['for' => $acoId])->toArray();
                        $acoIds = array_unique(Hash::merge($acoIds, Hash::extract($path, '{n}.id')));
                    }
                }
            }

            if ( ! empty($oldBaseAcoId) ) {
                unset($acoIds[$oldBaseAcoId]);
            }

            $data['acos'] = [
                '_ids' => $acoIds,
            ];


        }


        $polymorphicAssocs = ['address', 'person', 'communication', 'bankaccount'];

        foreach ( $polymorphicAssocs as $polymorphicAssoc ) {
            $data[$polymorphicAssoc]['model'] = $this->getAlias();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        if ( $entity->isNew() ) {
            $entity->set('type', $event->getSubject()->getAlias());
        }

        if ( ! empty($entity->newpassword) ) {
            $entity->password = (new DefaultPasswordHasher())->hash($entity->newpassword);
        }
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function getUser($id, $aco_id = null)
    {
        $contain = [
            'Baseacos', 'Communications', 'Addresses', 'Persons', 'Acos', 'Roles.Resources', 'Attachments'
        ];

        $data = ['user' => [], 'perms' => ['controllers' => [], 'models' => [], 'actions' => [], 'rights' => [], 'urls' => ['deniedUrls' => []]]];

        if ( ! empty($id) ) {
            $user = $this->find('all', [
                'contain'    => $contain,
                'conditions' => ['Users.id' => $id],
            ])->first()->toArray();

            if ( ! empty($user) ) {

                $user['acoIds'] = [];
                $user['acoDomain'] = [];

                if ( ! empty($user['acos']) ) {
                    foreach ( $user['acos'] as $aco ) {
                        $user['acoIds'][] = $aco['id'];
                        $user['acoDomain'][$aco['id']] = $aco['name'];
                    }
                } else {
                    $user['acos'] = [];
                }

                //CurrentAco default
                if ( ! empty($user['acos']) ) {
                    $user['currentaco'] = $user['acos'][0];
                }

                //CurrentAco aus Baseaco
                if ( ! empty($user['baseaco']) && ! empty($user['baseaco']['id']) ) {
                    $user['currentaco'] = $user['baseaco'];
                }

                //CurrentAco aus Parameter
                if ( ! is_null($aco_id) ) {
                    foreach ( $user['acos'] as $of ) {
                        if ( $of['id'] == $aco_id ) {
                            $user['currentaco'] = $of;
                            break;
                        }
                    }
                }

                $user['resources'] = [];
                if ( ! empty($user['roles']) ) {
                    foreach ( $user['roles'] as $k => $role ) {
                        if ( ! empty($role['resources']) ) {
                            foreach ( $role['resources'] as $res ) {
                                $user['resources'][] = $res;
                            }
                            unset($user['roles'][$k]['resources']);
                        }
                    }
                }

            }

            $data['user'] = $user;

            $foundDeniedUrls = $this->Roles->Resources->find()->where(['type' => 'url'])->all()->extract('url')->reduce(function ($output, $value) {
                if ( ! in_array($value, $output) ) {
                    $output[] = str_replace('//', '/', strtolower(request()->domain() . '/' . $value));
                }

                return $output;
            }, []);

            foreach ( $foundDeniedUrls as $deniedUrl ) {
                $data['perms']['urls']['deniedUrls'][] = $deniedUrl;
            }

            foreach ( $data['user']['resources'] as $res ) {


                switch ( strtolower($res['type']) ) {
                    case "model":
                        if ( ! isset($data['perms']['models'][strtolower($res['model'])]) || $res['model_accesslevel'] < $data['perms']['models'][strtolower($res['model'])]['accessLevel'] ) {
                            $data['perms']['models'][strtolower($res['model'])]['accessLevel'] = $res['model_accesslevel'];
                            $data['perms']['models'][strtolower($res['model'])]['acoIds'] = [];

                            //Welche Acos darf der User als Currentaco nutzen, sprich unter welchen darf er im Backend arbeiten
                            /* if ($res['model'] == 'Aco') {
                                 foreach ($data['user']['acos'] as $aco) {
                                     if ($aco['level'] >= $res['model_accesslevel']) {
                                         $data['user']['acoIds'][$aco['id']] = $aco['name'];
                                     }
                                 }
                             }
                            */


                            foreach ( $data['user']['acos'] as $aco ) {
                                if ( $aco['level'] == $res['model_accesslevel'] ) {
                                    $query = $this->Acos->find()->where([
                                        'lft >='  => $aco['lft'],
                                        'rght <=' => $aco['rght'],
                                    ])->select('id');

                                    $query->cache(function ($query) {
                                        return md5(serialize($query->clause('where')));
                                    }, 'dbResults');
                                    $allowedAcos = $query->all()->toArray();

                                    $allowedAcos = Hash::extract($allowedAcos, '{n}.id');
                                    $data['perms']['models'][strtolower($res['model'])]['acoIds'] = $allowedAcos;
                                    break;
                                }
                            }

                        }
                        break;

                    case "url":
                        $url = strtolower(request()->domain() . '/' . $res['url']);
                        if ( ($key = array_search($url, $data['perms']['urls']['deniedUrls'])) !== false ) {
                            unset($data['perms']['urls']['deniedUrls'][$key]);
                        }
                        break;

                    case "action":
                        $data['perms']['controllers'][strtolower($res['controller'])] = true;
                        $data['perms']['actions'][strtolower($res['controller'])][strtolower($res['action'])] = true;
                        if ( $res['action'] == '*' ) {
                            $result = $this->Roles->Resources->find()->where(['controller' => $res['controller']])->all();
                            foreach ( $result as $perm ) {
                                $data['perms']['actions'][strtolower($res['controller'])][strtolower($perm->action)] = true;
                            }
                        }
                        break;

                    case "can":

                        //Wenn es das Recht nicht in der Config gibt
                        if ( ! Configure::check('Sx.' . strtolower($res['can'])) ) {
                            Configure::write('TempRights.' . strtolower($res['can']), true);
                            //Wenn das Recht global deaktiviert ist, dann hier auch deaktivieren
                        } else {
                            if ( Configure::read('Sx.' . strtolower($res['can'])) ) {
                                Configure::write('TempRights.' . strtolower($res['can']), true);
                            }
                        }

                        break;

                    default:
                        break;
                }
            }

            //für ist denn das?
            if ( ! isset($data['perms']['models']['attachment']) ) {
                $data['perms']['models']['attachment'] = ['acoIds' => []];
            }

            $data['perms']['models']['attachment']['acoIds'][] = SHARED_FOLDER_ACO;

            $data['perms']['rights'] = Configure::read('TempRights');
            Configure::delete('TempRights');


        }

        return $data;
    }

}
