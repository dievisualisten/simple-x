<?php

namespace App\Model\Table;

use App\Library\Facades\Language;
use Cake\Core\Configure;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxFormularconfigsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id',
        'name',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('formularconfigs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Translation', [
            'defaultLocale' => Language::getDefaultLanguage()
        ]);

        $this->belongsTo('Acos', [
            'foreignKey' => 'aco_id',
        ]);
        $this->belongsTo('Emails', [
            'foreignKey' => 'email_id',
        ]);
        $this->belongsTo('Formulars', [
            'foreignKey' => 'formular_id',
        ]);
        $this->hasMany('Articles', [
            'foreignKey' => 'formularconfig_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('email_to')
            ->maxLength('email_to', 255)
            ->allowEmptyString('email_to');

        $validator
            ->scalar('email_bcc')
            ->maxLength('email_bcc', 255)
            ->allowEmptyString('email_bcc');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 255)
            ->allowEmptyString('subject');

        $validator
            ->boolean('use_conversion_tracking')
            ->notEmptyString('use_conversion_tracking');

        $validator
            ->scalar('conversion_tracking_code')
            ->maxLength('after_send_redirect', 429496729)
            ->notEmptyString('conversion_tracking_code');

        $validator
            ->scalar('after_send_redirect')
            ->maxLength('after_send_redirect', 429496729)
            ->allowEmptyString('after_send_redirect')
            ->urlWithProtocol('after_send_redirect');

        $validator
            ->scalar('after_send_content')
            ->maxLength('after_send_content', 429496729)
            ->allowEmptyString('after_send_content');

        $validator
            ->scalar('after_send_headline')
            ->maxLength('after_send_headline', 255)
            ->allowEmptyString('after_send_headline');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->allowEmptyString('identifier');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['aco_id'], 'Acos'), [
            'errorField' => 'aco_id'
        ]);

        $rules->add($rules->existsIn(['email_id'], 'Emails'), [
            'errorField' => 'email_id'
        ]);

        $rules->add($rules->existsIn(['formular_id'], 'Formulars'), [
            'errorField' => 'formular_id'
        ]);

        return $rules;
    }

}
