<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxPersonsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('persons');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('foreign_key');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model');

        $validator
            ->scalar('salutation')
            ->maxLength('salutation', 255)
            ->allowEmptyString('salutation');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 255)
            ->allowEmptyString('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255)
            ->allowEmptyString('lastname');

        $validator
            ->dateTime('birthday')
            ->allowEmptyDateTime('birthday');

        $validator
            ->scalar('familystatus')
            ->maxLength('familystatus', 255)
            ->allowEmptyString('familystatus');

        $validator
            ->scalar('note')
            ->maxLength('note', 429496729)
            ->allowEmptyString('note');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        $entity->set('type', $event->getSubject()->getAlias());
    }

}
