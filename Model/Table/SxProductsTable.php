<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @inheritdoc
 */

class SxProductsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->uuid('foreign_key')
            ->requirePresence('foreign_key', 'create')
            ->notEmptyString('foreign_key');

        $validator
            ->boolean('sale')
            ->requirePresence('sale', 'create')
            ->notEmptyString('sale');

        $validator
            ->scalar('ean')
            ->allowEmptyString('ean');

        $validator
            ->scalar('sku')
            ->allowEmptyString('sku');

        $validator
            ->scalar('producer_sku')
            ->allowEmptyString('producer_sku');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmptyString('price');

        $validator
            ->numeric('purchase_price')
            ->allowEmptyString('purchase_price');

        $validator
            ->numeric('producer_price')
            ->allowEmptyString('producer_price');

        $validator
            ->numeric('taxrate')
            ->requirePresence('taxrate', 'create')
            ->notEmptyString('taxrate');

        $validator
            ->scalar('depth')
            ->maxLength('depth', 255)
            ->allowEmptyString('depth');

        $validator
            ->scalar('height')
            ->maxLength('height', 255)
            ->allowEmptyString('height');

        $validator
            ->scalar('width')
            ->maxLength('width', 255)
            ->allowEmptyString('width');

        $validator
            ->scalar('weight')
            ->maxLength('weight', 255)
            ->allowEmptyString('weight');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmptyString('quantity');

        $validator
            ->integer('quantity_alert')
            ->requirePresence('quantity_alert', 'create')
            ->notEmptyString('quantity_alert');

        $validator
            ->numeric('shipping_rate')
            ->allowEmptyString('shipping_rate');

        $validator
            ->scalar('shipping_time')
            ->maxLength('shipping_time', 255)
            ->allowEmptyString('shipping_time');

        $validator
            ->scalar('extended_data')
            ->allowEmptyString('extended_data');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), [
            'errorField' => 'id'
        ]);

        $rules->add($rules->existsIn(['parent_id'], 'ParentProducts'), [
            'errorField' => 'parent_id'
        ]);

        return $rules;
    }

}
