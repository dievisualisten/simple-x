<?php

namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;


/*
 * TODO Refactor key / Value in der DB, sind reservierte Wörter :(. => k => name?
 * */

/**
 * @version 4.0.0
 */

class SxDomainsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'Domains.key',
        'Domains.value',
        'Domains.info',
        'Domains.type',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setTable('domains');
        $this->setDisplayField('value');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('sequence')
            ->allowEmptyString('sequence');

        $validator
            ->allowEmptyString('key');

        $validator
            ->scalar('value')
            ->maxLength('value', 255)
            ->allowEmptyString('value');

        $validator
            ->scalar('info')
            ->maxLength('info', 255)
            ->allowEmptyString('info');

        $validator
            ->scalar('parameter')
            ->maxLength('parameter', 255)
            ->allowEmptyString('parameter');

        $validator
            ->scalar('type')
            ->maxLength('type', 60)
            ->allowEmptyString('type');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->boolean('is_default')
            ->allowEmptyString('is_default');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->saveConfigFile();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->saveConfigFile();
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function saveConfigFile()
    {
        Configure::write('SxDomain', []);

        $domains = $this->find()->order(['sequence' => 'ASC'])
            ->all()->toArray();

        foreach ( collect($domains)->groupBy('type') as $group => $items ) {
            foreach ( $items as $item ) {
                Configure::write("SxDomain.{$group}.{$item->key}", $item->value);
            }
        }

        Configure::dump('sxdomain', 'default', ['SxDomain']);
    }

}
