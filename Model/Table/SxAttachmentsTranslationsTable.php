<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxAttachmentsTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attachments_translations');
        $this->setDisplayField('id');
        $this->setPrimaryKey(['id', 'locale']);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Attachments', [
            'foreignKey' => 'id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        return parent::buildRules($rules);
    }

}
