<?php

namespace App\Model\Table;

use Exception;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use Cake\Datasource\EntityInterface;

/**
 * @version 4.0.0
 */

class SxPageconfigurationsTable extends ConfigurationsTable
{
    public $defaultSearchFields = [
        'id', 'name', 'value'
    ];

    public $treeDisplay = 'display_name';

    /**
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->hasOne('Resources', [
            'dependent' => true,
            'foreignKey' => 'configuration_id',
        ]);
    }


    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( ! empty($entity->get('name')) ) {
            $entity->set('name', str_replace('resource.modules.article.pagetypes.', '', $entity->get('name')));
            $entity->set('name', 'resource.modules.article.pagetypes.' . $entity->name);
        }

        $entity->set('is_pageconfiguration', true);

        parent::beforeSave($event, $entity, $options);
    }

    /**
     * Add a new config to all exiting pagestypes and layouts
     *
     * @param $data
     * @throws \Exception
     * @return bool
     */
    public function addNewConfigToAllPagetypes($data)
    {
        $pagesTypes = $this->getAllPagetypes();

        foreach ( $pagesTypes as $pagesType => $layouts ) {
            foreach ( $layouts as $layout => $nothing ) {

                $splited = explode('.', $data['name']);

                $key = implode('.', array_slice($splited, 5));

                if ( count($splited) > 6 ) {
                    $key = implode('.', array_slice($splited, 6));
                }

                $segment = $pagesType;

                if ( count($splited) > 6 ) {
                    $segment .= '.' . $layout;
                }

                $name = 'resource.modules.article.pagetypes.' .
                    $segment . '.' . $key;

                $config = $this->find()
                    ->where([
                        'name' => $name
                    ])
                    ->first();

                $savedata = [
                    'name' => $name,
                    'value' => $data['value'],
                    'description' => $data['description'],
                ];

                if ( ! $config ) {
                    $config = $this->newEntity($savedata);
                }

                $this->patchEntity($config, $savedata);

                if ( ! $this->save($config) ) {
                    throw new \Exception(__d('system', 'Es sind Fehler aufgetreten.'), 422);
                };
            }
        }

        return true;
    }

    /**
     * TODO SX4, ungetestet
     * add a new pagetype based on exiting pagetypeconfigs
     *
     * @param $data
     * @return bool
     */
    public function addNewPagetype($data)
    {
        if ( empty($data['name']) ) {
            throw new \Exception(__d('system', 'Es sind Fehler aufgetreten.'), 422);
        }

        // Normalize target
        $target = str_replace('resource.modules.article.pagetypes', 
            '', strtolower($data['name']));

        $layout = strtolower(data_get($data, 'layout') 
            ?: 'default');

        $source = strtolower(data_get($data, 'source') 
            ?: 'page');

        $finalSource = "resource.modules.article.pagetypes.{$source}";
        $finalTarget = "resource.modules.article.pagetypes.{$target}";

        // Get all subitems of the base
        $query = $this->find()
            ->where([
                'name LIKE' => "{$finalSource}%"
            ])
            ->all();

        foreach ( $query as $config ) {

            $targetName = str_replace("{$finalSource}.default", 
                "{$finalTarget}.{$layout}", $config->name);

            $targetName = str_replace("{$finalSource}.name", 
                "{$finalTarget}.name", $targetName);

            $targetName = str_replace("{$finalSource}.icon",
                "{$finalTarget}.icon", $targetName);

            $targetName = str_replace("{$finalSource}.element",
                "{$finalTarget}.element", $targetName);

            $entity = $this->find()->where([
                'name' => $targetName
            ])
            ->first();

            if ( ! empty($entity) ) {
                continue;
            }

            $entity = $this->newEntity($config->toArray());

            $entity->set([
                'id' => Text::uuid(), 'name' => $targetName
            ]);

            $result = $this->save($entity);

            if ( ! $result ) {
                throw new \Exception(__d('system', 'Es sind Fehler aufgetreten.'), 422);
            }

        }

        $rootEntity = $this->find()
            ->where([
                'name ' => "{$finalTarget}.name"
            ])
            ->first();

        $rootEntity->set([
            'value' => ucfirst($target)
        ]);


        return $this->save($rootEntity);
    }

    /**
     * Get all pagetypes and layouts
     *
     * @return array
     */
    public function getAllPagetypes()
    {

        $query = $this->find()->where(['is_pageconfiguration' => true]);

        $pagesTypes = [];

        foreach ( $query as $item ) {
            $p = explode('resource.modules.article.pagetypes.', $item->name);
            $p = explode('.', $p[1]);

            if ( empty($pagesTypes[$p[0]]) ) {
                $pagesTypes[$p[0]] = [];
            }
            if ( $p[1] != 'name' ) {
                if ( empty($pagesTypes[$p[0]][$p[1]]) ) {
                    $pagesTypes[$p[0]][$p[1]] = [];
                }
            }
        }

        return $pagesTypes;
    }

}
