<?php

namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxCrontasksTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        $this->setTable('crontasks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name', __('Bitte geben Sie einen Namen ein.'), false);

        $validator
            ->scalar('method')
            ->maxLength('method', 255)
            ->allowEmptyString('method', __('Bitte geben Sie einen Methode ein.'), false);

        $validator
            ->scalar('parameter')
            ->maxLength('parameter', 255)
            ->allowEmptyString('parameter');

        $validator
            ->scalar('execute')
            ->maxLength('execute', 255)
            ->allowEmptyString('execute', __('Bitte wählen Sie ein Ausführungsintervall.'), false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->boolean('restartable')
            ->allowEmptyString('restartable');

        $validator
            ->scalar('status')
            ->maxLength('status', 429496729)
            ->allowEmptyString('status');

        $validator
            ->scalar('log')
            ->maxLength('log', 429496729)
            ->allowEmptyString('log');

//        $validator
//            ->dateTime('last_start')
//            ->allowEmptyDateTime('last_start');
//
//        $validator
//            ->dateTime('last_executed')
//            ->allowEmptyDateTime('last_executed');
//
//        $validator
//            ->dateTime('life_sign')
//            ->allowEmptyDateTime('life_sign');


        return $validator;
    }

    /**
     * @inheritdoc TODO: DOC
     */
// gibt den Crontask Typen zurück
    public function getType($ct)
    {
        $cnt = count(explode(':', $ct->execute));

        // sofort
        if ( $ct->execute == 'now' ) {
            return 'now';

            //immer
        } else {
            if ( $ct->execute == 'always' ) {
                return 'always';

                // float - regelmässige Ausführung in diesem Abstand
            } else {
                if ( $cnt == 1 ) {
                    return 'periodic';

                    // feste Uhrzeit - täglich zu dieser Uhrzeit
                } else {
                    if ( $cnt == 2 ) {
                        return 'daily';

                    }
                }
            }
        }

        return '';

    }

    /**
     * @inheritdoc TODO: DOC
     */
// prüft, ob ein Crontask jetzt auszuführen ist
    public
    function checkExecution(
        $task,
        $timestamp = null
    ) {
        $interval = explode(':', $task->execute);

        if ( $timestamp == null ) {
            $timestamp = Time::now();
        }

        if ( $task->execute == 'now' ) {
            return true;
        }

        if ( $task->execute == 'always' ) {
            return true;
        }

        if ( count($interval) == 1 ) {

            $diff = $task->last_start->diffInMinutes($timestamp);

            if ( $diff >= ($task->execute * 60) ) {
                return true;
            }

            return false;
        }

        if ( count($interval) == 2 ) {

            $past = $task->last_start->addDay()->lt($timestamp);

            if ( $past ) {
                return true;
            }

            return false;
        }

        return false;
    }

}
