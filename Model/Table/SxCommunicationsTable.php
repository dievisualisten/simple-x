<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxCommunicationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('communications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('foreign_key');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('email2')
            ->maxLength('email2', 255)
            ->allowEmptyString('email2');

        $validator
            ->scalar('website')
            ->maxLength('website', 255)
            ->allowEmptyString('website');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->allowEmptyString('phone');

        $validator
            ->scalar('phone2')
            ->maxLength('phone2', 255)
            ->allowEmptyString('phone2');

        $validator
            ->scalar('cell')
            ->maxLength('cell', 36)
            ->allowEmptyString('cell');

        $validator
            ->scalar('cell2')
            ->maxLength('cell2', 255)
            ->allowEmptyString('cell2');

        $validator
            ->scalar('fax')
            ->maxLength('fax', 36)
            ->allowEmptyString('fax');

        return $validator;
    }

    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);
        $entity->set('type', $event->getSubject()->getAlias());
    }
}
