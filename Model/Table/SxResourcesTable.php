<?php

namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxResourcesTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id',
        'name',
        'description',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Roles',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('resources');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Configurations', [
            'foreignKey' => 'configuration_id',
        ]);

        $this->belongsToMany('Roles', [
            // 'through' => 'ResourcesRoles',
            'className'        => 'Roles',
            'foreignKey'       => 'resource_id',
            'targetForeignKey' => 'role_id',
            'joinTable'        => 'resources_roles',
            'saveStrategy'     => 'replace',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type', 'Bitte wählen Sie eine Alternative aus.', false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('controller')
            ->maxLength('controller', 255)
            ->allowEmptyString('controller', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'action';
            });

        $validator
            ->scalar('action')
            ->maxLength('action', 255)
            ->allowEmptyString('action', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'action';
            });

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->allowEmptyString('url', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'url';
            });

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'model';
            });

        $validator
            ->integer('model_accesslevel')
            ->allowEmptyString('model_accesslevel', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'model';
            });

        $validator
            ->scalar('can')
            ->maxLength('can', 255)
            ->allowEmptyString('can', 'error', function ($context) {
                return ! isset($context['data']['type']) || $context['data']['type'] != 'can';
            });

        $validator
            ->scalar('modul')
            ->maxLength('modul', 255)
            ->allowEmptyString('modul');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), [
            'errorField' => 'name',
        ]);

        // Geht nicht da die resource erstellt wird waerend die config nicht existiert

        // $rules->add($rules->existsIn(['configuration_id'], 'Configurations'), [
        //     'errorField' => 'configuration_id'
        // ]);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);


        if ( ! empty($entity->model) ) {
            $entity->model = ucfirst($entity->model);
        }
        if ( ! empty($entity->action) ) {
            $entity->action = mb_strtolower($entity->action);
        }
        if ( ! empty($entity->controller) ) {
            $entity->controller = mb_strtolower($entity->controller);
        }

        if ( empty($entity->modul) ) {
            $entity->modul = ucfirst($entity->model);
        }

        //model
        if ( $entity->type == 'model' ) {
            $modelAccesslevel = $entity->model_accesslevel;

            $levelNames = Configure::read('Sx.app.aco.levelnames');
            if ( empty($entity->name) ) {
                if ( $modelAccesslevel == 0 ) {
                    $entity->name = $entity->modul . ' | Daten: (0) alle Datensätze';
                } else {
                    $entity->name = $entity->modul . ' | Daten: (' . $modelAccesslevel . ') ' . $levelNames[$modelAccesslevel] . '-Datensätze und darunter';
                }
            }
            if ( empty($entity->description) ) {
                $entity->description = $entity->name . ' - Level: ' . $modelAccesslevel;
            }
            $entity->unsetProperty(['action', 'controller']);
        }

        //Action
        if ( $entity->type == 'action' ) {
            $name = $entity->controller . '/' . $entity->action;
            if ( empty($entity->description) ) {
                if ( $entity->action == '*' ) {
                    $entity->description = 'Zugriff auf alle Actions des Controllers ' . $entity->controller;
                } else {
                    $entity->description = 'Zugriff auf ' . $name;
                }
            }


            $entity->modul = ucfirst(Inflector::singularize($entity->controller));
            $entity->name = $entity->modul . ' | Action: ' . $name;
            $entity->unsetProperty(['model']);
        }

        //url
        if ( $entity->type == 'url' ) {

            $isStarUrl = strpos($entity->url, '*') || strpos($entity->url, '/*');
            if ( empty($entity->description) ) {
                if ( $isStarUrl === false ) {
                    $entity->description = 'Zugriff auf ' . $entity->url . ' und darunter';
                } else {
                    $entity->description = 'Zugriff auf ' . $entity->url;
                }
            }

            $entity->name = 'Url | Url: ' . $entity->url;
            $entity->unsetProperty(['action', 'controller', 'model', 'modul']);
        }

        //Recht
        if ( $entity->type == 'can' ) {
            $entity->unsetProperty(['action', 'controller', 'model']);

            $modul = explode('.', $entity->can);

            if ( empty($entity->name) ) {
                if ( count($modul) > 2 ) {
                    $entity->modul = $entity->name = ucfirst($modul[2]);
                }

                if ( count($modul) === 4 ) {
                    $entity->name = 'Modul';

                    if ( empty($entity->description) ) {
                        $entity->description = 'Kann ' . $entity->modul . ' Modul im Backend sehen';
                    }
                }

                if ( count($modul) > 4 ) {
                    for ( $i = 3; $i <= count($modul) - 2; $i++ ) {
                        $entity->name = $entity->name . ' > ' . ucfirst($modul[$i]);
                    }
                }

                if ( empty($entity->description) ) {
                    $entity->description = 'Kann ' . $entity->name . ' im Backend sehen';
                }

                $tmp = explode(' > ', $entity->name);
                array_shift($tmp);

                $entity->name = $entity->modul . ' | ' . $entity->name . ' ist sichtbar';

            }
        }

        $entity->is_default = Configure::read('Sx.app.adminmode');
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->saveConfigFile();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->saveConfigFile();
    }

    /**
     * @inheritdoc
     */
    public function saveConfigFile()
    {
        Configure::write('SxResources', []);

        $items = $this->find()->where([
            'type' => 'url',
        ])->all()->toArray();

        foreach ( $items as $index => $item ) {
            Configure::write("SxResources.{$index}", $item->url);
        }

        Configure::dump('sxresources', 'default', ['SxResources']);
    }

}
