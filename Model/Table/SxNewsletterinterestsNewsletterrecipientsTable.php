<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxNewsletterinterestsNewsletterrecipientsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newsletterinterests_newsletterrecipients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Newsletterinterests', [
            'foreignKey' => 'newsletterinterest_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Newsletterrecipients', [
            'foreignKey' => 'newsletterrecipient_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['newsletterinterest_id'], 'Newsletterinterests'), [
            'errorField' => 'newsletterinterest_id'
        ]);

        $rules->add($rules->existsIn(['newsletterrecipient_id'], 'Newsletterrecipients'), [
            'errorField' => 'newsletterrecipient_id'
        ]);

        return $rules;
    }

}
