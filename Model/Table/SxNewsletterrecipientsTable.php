<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxNewsletterrecipientsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'email',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Newsletterinterests',
        'Persons',
        'Communications',
        'Addresses',
        'Bankaccounts',
    ];

    /**
     * @var array $exportDefaults
     */
    public $exportDefaults = [

        'strategy' => 'except',

        'fields' => [
            'id',
            'address.id',
            'address.foreign_key',
            'address.type',
            'address.model',
            'communication.id',
            'communication.foreign_key',
            'communication.type',
            'communication.model',
            'communication.email2',
            'communication.phone',
            'communication.phone2',
            'communication.cell2',
            'person.id',
            'person.foreign_key',
            'person.type',
            'person.model',
        ],

        'contain' => [
            'Newsletterinterests', // multiple
            'Persons',
            'Communications',
            'Addresses',
        ],

    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('newsletterrecipients');
        $this->setDisplayField('email');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('Newslettercampaigns', [
            'foreignKey' => 'newsletterrecipient_id',
        ]);

        $this->belongsToMany('Newsletterinterests', [
            'foreignKey' => 'newsletterrecipient_id',
            'targetForeignKey' => 'newsletterinterest_id',
            'joinTable' => 'newsletterinterests_newsletterrecipients',
        ]);

        $this->hasOne('Persons', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Persons.model' => 'Newsletterrecipients'],
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);

        $this->hasOne('Communications', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Communications.model' => 'Newsletterrecipients'],
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);

        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Addresses.model' => 'Newsletterrecipients'],
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);

        $this->hasOne('Bankaccounts', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['Bankaccounts.model' => 'Newsletterrecipients'],
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email')
            ->allowEmptyString('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('salutation')
            ->maxLength('salutation', 255)
            ->allowEmptyString('salutation');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 255)
            ->allowEmptyString('locale');

        $validator
            ->boolean('approved')
            ->allowEmptyString('approved');

        $validator
            ->dateTime('approved_date')
            ->allowEmptyDateTime('approved_date');

        $validator
            ->boolean('approved_mail')
            ->allowEmptyString('approved_mail');

        $validator
            ->dateTime('approved_mail_date')
            ->allowEmptyDateTime('approved_mail_date');

        $validator
            ->boolean('unsubscribed')
            ->allowEmptyString('unsubscribed');

        $validator
            ->dateTime('unsubscribed_date')
            ->allowEmptyDateTime('unsubscribed_date');

        $validator
            ->boolean('hardbounced')
            ->allowEmptyString('hardbounced');

        $validator
            ->scalar('bouncestatus')
            ->maxLength('bouncestatus', 255)
            ->allowEmptyString('bouncestatus');

        $validator
            ->scalar('source')
            ->maxLength('source', 255)
            ->allowEmptyString('source');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->isUnique(['email']), [
            'errorField' => 'email'
        ]);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        $polymorphicAssocs = ['address', 'person', 'communication', 'bankaccount'];

        foreach ( $polymorphicAssocs as $polymorphicAssoc ) {
            if ( $entity->{$polymorphicAssoc} ) {
                $entity->{$polymorphicAssoc}->set('model', $this->getAlias());
            }
        }

        if ( empty($entity->salutation) ) {
            $entity->salutation = __('Sehr geehrte Damen und Herren');
            //TODO SX4 generieren
        }

        if ( empty($entity->path) ) {
            $entity->source = 'manuell';
        }
    }

}
