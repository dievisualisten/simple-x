<?php

namespace App\Model\Table;

use App\Library\AuthManager;
use App\Library\Facades\Language;
use App\Model\Table\Traits\ExtenderTrait;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;

/**
 * @version 4.0.0
 */

class SxAppTable extends Table
{
    use ExtenderTrait;

    public $defaultSearchFields = [
        'id',
    ];

    public $defaultContains = [
        //
    ];

    public $dublicateContains = [
        //
    ];

    public $exportDefaults = [
        //
    ];

    public $treeDisplay = 'name';

    /**
     * @inheritdoc
     */
    function begin()
    {
        $connection = ConnectionManager::get($this->defaultConnectionName());
        $connection->begin();
    }

    /**
     * @inheritdoc
     */
    function commit()
    {
        $connection = ConnectionManager::get($this->defaultConnectionName());
        $connection->commit();
    }

    /**
     * @inheritdoc
     */
    function rollback()
    {
        $connection = ConnectionManager::get($this->defaultConnectionName());
        $connection->rollback();
    }

    /**
     * @inheritdoc
     */
    public function truncate()
    {
        $conn = ConnectionManager::get($this->defaultConnectionName());

        return $conn->execute('TRUNCATE TABLE ' . $this->getTable() . ';');
    }

    public function patchAndSave($entity, $data, $patchOptions = [], $saveOptions = [])
    {
        $this->patchEntity($entity, $data, $patchOptions);

        return $this->save($entity, $saveOptions);
    }

    public function forceFirstOrNew($data, $contain = null, $options = [])
    {
        if ( $contain == null ) {
            $contain = $this->getDefaultContains();
        }

        $primary = $this->getPrimaryKey();

        if ( ! isset($data[$primary]) ) {
            return $this->newEntity($data, $options);
        }

        $query = $this->find()
            ->disableBufferedResults()
            ->where([
                $this->getAlias() . '.' . $primary => $data[$primary],
            ])
            ->contain($contain);

        $entity = $query->first();

        if ( $entity == null ) {
            $entity = $this->newEntity($data, $options);
        }

        return $entity;
    }

    function firstOrNew($data, $contain = null, $options = [])
    {
        if ( is_a($data, $this->getEntityClass()) ) {
            return $data;
        }

        return $this->forceFirstOrNew($data, $contain, $options);
    }

    /**
     * Find first entity or throw new exception
     *
     * @param mixed $data
     * @param array|null $contain
     * @param array $options
     * @return \Cake\Datasource\EntityInterface|null
     * @throws \Exception
     */
    public function forceFirstOrFail($data, $contain = null, $options = [])
    {
        $entity = $this->forceFirstOrNew($data, $contain, $options);

        if ( $entity->isNew() ) {
            throw new \Exception('Entity does not exists');
        }

        return $entity;
    }

    /**
     * Find first entity or return null
     *
     * @param mixed $data
     * @param array|null $contain
     * @param array $options
     * @return \Cake\Datasource\EntityInterface|null
     * @throws \Exception
     */
    public function forceFirstOrNull($data, $contain = null, $options = [])
    {
        $entity = $this->forceFirstOrNew($data, $contain, $options);

        if ( $entity->isNew() ) {
            return null;
        }

        return $entity;
    }

    public function firstOrFail($data, $contain = null, $options = [])
    {
        $entity = $this->firstOrNew($data, $contain, $options);

        if ( $entity->isNew() ) {
            throw new \Exception('Entity does not exists');
        }

        return $entity;
    }

    public function deleteOrSkip($entity, $options = [])
    {
        if ( ! $entity->isNew() ) {
            $this->delete($entity, $options);
        }
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        // Add validation rules
        $rules->add(function ($entity) {
            $data = $entity->extract($this->getSchema()->columns(), true);
            $validator = $this->getValidator('default');
            $errors = $validator->validate($data, $entity->isNew());
            $entity->getErrors($errors);

            return empty($errors);
        });

        return $rules;
    }

    function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( $entity->isNew() && $this->hasField('id') ) {
            if ( ! isset($entity->id) || empty($entity->id) ) {
                $entity->set('id', Text::uuid());
            }
        }

        if ( $entity->isNew() && $this->hasField('aco_id') ) {
            if ( ! isset($entity->aco_id) || empty($entity->aco_id) ) {
                $entity->set('aco_id', AuthManager::getUser('currentaco.id'));
            }
        }

        $this->loopTraits('beforeSave', [$event, $entity, $options]);
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->loopTraits('afterSave', [$event, $entity, $options]);
    }

    function beforeFind(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        if ( $primary ) {
            $query = AuthManager::getConditions($this, $query);
        }

        $this->loopTraits('beforeFind', [$event, $query, $options, $primary]);
    }

    public function getDefaultSearchFields()
    {
        return $this->defaultSearchFields;
    }

    public function setDefaultSearchFields($fields = [])
    {
        $this->defaultSearchFields = $fields;
    }

    public function getDefaultContains()
    {
        return $this->defaultContains;
    }

    public function setDefaultContains($contains = [])
    {
        $this->defaultContains = $contains;
    }

    public function getDublicateContains()
    {
        return $this->dublicateContains;
    }

    public function setDublicateContains($contains = [])
    {
        $this->dublicateContains = $contains;
    }

    public function getExportDefaults()
    {
        return $this->exportDefaults;
    }

    public function setExportDefaults($options = [])
    {
        $this->exportDefaults = $options;
    }

    public function getTreeDisplay()
    {
        return $this->treeDisplay;
    }

    public function setTreeDisplay($value)
    {
        $this->treeDisplay = $value;
    }

    public function convertToElasticSearchData(EntityInterface $entity)
    {

        $ret = $entity->toArray();
        $data = json_encode(array_values($entity->toArray()));
        $ret['searchcontent'] = str_replace(',', ' ', str_replace('"', ' ', strip_tags($data)));
        $ret['id'] = $entity->get('id');


        return $ret;
    }

    public function getElasticSearchIndexQuery(Query $query = null)
    {
        return $this->find()->all();
    }

}
