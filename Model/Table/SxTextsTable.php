<?php

namespace App\Model\Table;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\I18n\I18n;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxTextsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'name', 'value',
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('texts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Translation', [
            'defaultLocale' => Configure::read('Sx.app.language.defaultlanguage')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('source')
            ->maxLength('source', 255)
            ->allowEmptyString('source');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('value')
            ->maxLength('value', 429496729)
            ->allowEmptyString('value');

        $validator
            ->scalar('default_value')
            ->maxLength('default_value', 429496729)
            ->allowEmptyString('default_value');

        $validator
            ->boolean('is_html')
            ->allowEmptyString('is_html');

        $validator
            ->boolean('in_use')
            ->allowEmptyString('in_use');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ( config('Sx.app.adminmode') ) {
            $entity->set('default_value', $entity->value);
        }

        if ( empty($entity->source) ) {
            $entity->set('source', 'text');
        }

        if ( empty($entity->type) ) {
            $entity->set('type', 'text');
        }

        parent::beforeSave($event, $entity, $options);
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->writePos();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->writePos();
    }

    /**
     * @inheritdoc TODO: DOC
     */
    public function writePos()
    {
        $_currentLocale = I18n::getLocale();
        $locales = (array) Configure::read('Sx.app.language.accept');
        foreach ( $locales as $locale ) {
            $default = new File(APP . 'Locale' . DS . $locale . DS . 'default.po', true);
            $default->write('msgid ""' . "\n");
            $default->append('msgstr ""' . "\n\n");
            I18n::setLocale($locale);
            $texts = $this->find()->all();
            foreach ( $texts as $i => $text ) {
                if ( $text->_locale == $locale ) {
                    $default->append('msgid "' . str_replace("\n", '\n', addslashes($text->name)) . '"' . "\n");
                    $default->append('msgstr "' . str_replace("\n", '\n', addslashes($text->value)) . '"' . "\n\n");
                }
            }
        }
        I18n::setLocale($_currentLocale);
        Cache::clear('_cake_core_');
    }

}
