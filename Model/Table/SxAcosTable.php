<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */
class SxAcosTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'name',
    ];

    /**
     * @var array $defaultContains
     */
    public $defaultContains = [
        'Communications',
        'Addresses',
        'Companies'
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('acos');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree', [
            'level' => 'level'
        ]);

        $this->hasOne('Addresses', [
            'className'  => 'Addresses',
            'foreignKey' => 'foreign_key',
            'conditions' => ['Addresses.model' => 'Acos', 'Addresses.type' => 'Addresses'],
            'dependent'  => true,
        ]);

        $this->hasOne('Companies', [
            'className'  => 'Companies',
            'foreignKey' => 'foreign_key',
            'conditions' => ['Companies.model' => 'Acos', 'Companies.type' => 'Companies'],
            'dependent'  => true,
        ]);

        $this->hasOne('Communications', [
            'className'  => 'Communications',
            'foreignKey' => 'foreign_key',
            'conditions' => ['Communications.model' => 'Acos', 'Communications.type' => 'Communications'],
            'dependent'  => true,
        ]);

        $this->belongsTo('ParentAcos', [
            'className'  => 'Acos',
            'foreignKey' => 'parent_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('name', __d('system', 'Bitte geben Sie einen Namen ein.'), false);

        $validator
            ->allowEmptyString('parent_id', __d('system', 'Bitte wählen Sie einen übergeordneten Eintrag aus.'), false);

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn('parent_id', 'ParentAcos'));

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $polymorphicAssocs = ['address', 'company', 'communication'];

        foreach ( $polymorphicAssocs as $polymorphicAssoc ) {
            if ( $entity->{$polymorphicAssoc} ) {
                $entity->{$polymorphicAssoc}->set('model', $this->getAlias());
            }
        }

        $path = [];

        if ( $entity->parent_id ) {
            $path = $this->find('path', ['for' => $entity->parent_id])->extract('name')->toArray();
            array_shift($path);
        }

        if ( ! empty($path) ) {
            $entity->pathname = implode(' / ', $path) . ' / ' . $entity->name;
        } else {
            $entity->pathname = $entity->name;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $isDirty = $entity->isDirty('has_attachmentfolder') ||
            $entity->isDirty('attachmentfolder') || $entity->isDirty('name');

        if ( $entity->get('has_attachmentfolder', false) && $isDirty ) {

            $attachmentData = [
                'name' => $entity->name,
                'dirname' => $entity->attachmentfolder,
                'aco_id' => $entity->id
            ];

            TableRegistry::getTableLocator()->get('Attachments')
                ->addOrRenameStandardFolderSet($attachmentData);
        }

    }

    /**
     * @inheritdoc TODO SX 4
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        //delete Attachment files and folders
        //TableRegistry::getTableLocator()->get('Attachments')
    }

    /**
     * @inheritdoc
     */
    public function findLeafs($query)
    {
        $query->where(function ($exp, $q) {
            return $exp->notIn('id', $this->find()->where(function ($exp, $q) {
                return $exp->isNotNull('parent_id');
            })->select([
                'Acos.parent_id',
            ])->group(['Acos.parent_id']));
        });

        return $query;
    }

    /**
     * @inheritdoc
     */
    function addSharedAco()
    {
        $query = $this->find()->where([
            'id' => SHARED_FOLDER_ACO,
            'parent_id IS' => null
        ]);

        $aco = $query->first();

        if ( empty($aco) ) {
            $aco = $this->newEntity();
            $aco->name = 'Gemeinsam genutzter Ordner';
            $aco->id = SHARED_FOLDER_ACO;
            $aco->has_attachmentfolder = true;
            $aco->parent_id = 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee';
            $aco = $this->save($aco);
        }

        return $aco;
    }

}
