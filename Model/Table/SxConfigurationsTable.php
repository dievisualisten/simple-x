<?php

namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxConfigurationsTable extends AppTable
{
    /**
     * @var array $defaultSearchFields
     */
    public $defaultSearchFields = [
        'id', 'name', 'value'
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('configurations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasOne('Resources', [
            'dependent' => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 429496729)
            ->allowEmptyString('description');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('value')
            ->allowEmptyString('value');

        $validator
            ->scalar('default_value')
            ->maxLength('default_value', 255)
            ->allowEmptyString('default_value');

        $validator
            ->boolean('is_resource')
            ->allowEmptyString('is_resource');

        $validator
            ->boolean('is_default')
            ->allowEmptyString('is_default');

        $validator
            ->boolean('is_pageconfiguration')
            ->allowEmptyString('is_pageconfiguration');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->isUnique(['name']), [
            'errorField' => 'name'
        ]);

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        parent::beforeSave($event, $entity, $options);

        if ( Configure::read('Sx.app.adminmode') ) {
            $entity->set('default_value', $entity->value);
        }

        $entity->is_default = Configure::read('Sx.app.adminmode');
    }

    /**
     * @inheritdoc
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $resources = TableRegistry::getTableLocator()->get('Resources');
        $roles = TableRegistry::getTableLocator()->get('Roles');

        $resource = $resources->find()
            ->where([
                'configuration_id' => $entity->id
            ])
            ->first();

        if ( $entity->is_resource ) {

            $saveData = [
                'type' => 'can',
                //'name' => $entity->name,
                'can' => $entity->name,
                'model' => null,
                'controller' => null,
                'action' => null,
                'configuration_id' => $entity->id,
            ];

            if ( ! $resource ) {
                $resource = $resources->newEntity($saveData);
            }

            $resources->patchEntity($resource, $saveData);

            if ( $resource->isNew() ) {
                $resource->set('roles', [$roles->get(SUPER_ADMIN_ROLE_ID)->toArray()]);
            }

            $entity = $resources->save($resource);

        } else {
            if ( $resource ) {
                $resources->delete($resource);
            }
        }

        $this->saveConfigFile();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $this->saveConfigFile();
    }

    /**
     * @inheritdoc TODO: DOCS
     */
    public function saveConfigFile()
    {

        //reset
        Configure::write('Sx', []);
        $settings = $this->find()->order(['name' => 'ASC'])->all();

        foreach ( $settings as $variable ) {
            if ( trim($variable->value) != ',' ) {
                $v = explode(',', $variable->value);
            } else {
                $v = [','];
            }
            if ( count($v) == 1 ) {
                $v = trim($v[0]);
            } else {
                foreach ( $v as $k => $w ) {
                    $v[$k] = trim($w);
                }
                $v = array_values($v);
            }
            Configure::write('Sx.' . $variable->name, $v);
        }
        Configure::dump('sxconfig', 'default', ['Sx']);
    }

}
