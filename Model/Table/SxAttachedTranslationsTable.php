<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * @version 4.0.0
 */

class SxAttachedTranslationsTable extends AppTable
{
    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attached_translations');
        $this->setDisplayField('title');
        $this->setPrimaryKey(['id', 'locale']);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Attached', [
            'foreignKey' => 'id',
        ]);

        $this->belongsTo('Attachments', [
            'foreignKey' => 'Attached.attachment_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 10)
            ->allowEmptyString('locale', null, 'create');

        $validator
            ->scalar('alternative')
            ->maxLength('alternative', 255)
            ->allowEmptyString('alternative');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('description')
            ->maxLength('description', 429496729)
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules = parent::buildRules($rules);

        $rules->add($rules->existsIn(['attachment_id'], 'Attachments'));

        return $rules;
    }

}
