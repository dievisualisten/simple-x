<?php
namespace App\Model\Validation;

/*
$context Param
data: The original data passed to the validation method, useful if you plan to create rules comparing values.
providers: The complete list of rule provider objects, useful if you need to create complex rules by calling multiple providers.
newRecord: Whether the validation call is for a new record or a preexisting one.
field: fieldname
*/

use Cake\I18n\Date;
use PasswordPolicy\Policy;
use mm\Mime\Type;

class SxValidation
{
    public static function _notBlank($value, $context = [])
    {
        if ( is_object($value) ) {
            return true;
        }

        return ! is_null($value) && (is_string($value) &&
            ! empty($value) && $value !== '-1');
    }

    public static function checked($value, $context = [])
    {
        return format_type('boolean', $value);
    }

    public static function file($value, $type = null, $size = null)
    {
        if ( empty($value) || ! $value->getSize() ) {
            return false;
        }

        $filePasses = true;

        if ( ! empty($size) ) {
            $filePasses &= $value->getSize() <= ($size * 1024);
        }

        $fileName = Type::guessType($value->getClientFilename());

        if ( ! empty($type) ) {
            $filePasses &= preg_match('/^' . str_replace('\*', '(.*?)',
                    preg_quote($type, '/')) . '$/', $fileName);
        }

        return !! $filePasses;
    }

    /**
     * Checks zipcodes for Germany
     *
     * @param string $value The value to check.
     * @return boolean
     * @access public
     */
    public static function postal($value)
    {
        $pattern = '/^[0-9]{5}$/';

        return !! preg_match($pattern, $value);
    }

    /**
     * Checks an adress (street and number) for Germany.
     * That is what is called "Straße und Hausnummer",
     * the first line of a german formal address block.
     *
     * @param string $value The value to check.
     * @return boolean
     * @access public
     */
    public static function address($value)
    {
        $pattern = '/[a-zA-ZäöüÄÖÜß \.]+ [0-9]+[a-zA-Z]?/';

        return !! preg_match($pattern, $value);
    }

    /**
     * Checks phone number for Germany
     *
     * @param string $value The value to check.
     * @return boolean
     * @access public
     */
    public static function phone($value)
    {
        $pattern = '/[0-9\/. \-]*/';

        return !! preg_match($pattern, $value);
    }

    public static function time($value)
    {
        $pattern = '/(^|\s)[0-9]{2}:[0-9]{2}(:[0-9]{2})?$/';

        return !! preg_match($pattern, $value);
    }

    public static function date($value)
    {
        $pattern = '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2}|[0-9]{2}\.[0-9]{2}\.[0-9]{4})($|\s)/';

        return !! preg_match($pattern, $value);
    }

    public static function minage($check, $limit)
    {
        if ( empty($check) ) {
            return false;
        }

        $date = Date::now()->startOfDay();

        return Date::parse($check)->startOfDay()->subYears($limit)
            ->isPast($date);
    }

    public static function dateinfuture($check, $limit)
    {
        if ( empty($check) ) {
            return false;
        }

        $date = Date::now()->startOfDay();

        return Date::parse($check)->startOfDay()->subDays($limit)
            ->isFuture($date);
    }

    public static function validuntil($check)
    {
        $valid = preg_match('/^([0-9]{2})\/([0-9]{4})$/', $check, $matches);

        if ( ! $valid ) {
            return false;
        }

        $date = Date::now()->startOfDay();

        return $date->format('Y') <= (int) $matches[2] &&
            $date->format('n') <= (int) $matches[1];
    }

    public static function cc($check, $target = null)
    {
        $credittype = null;

        $check = str_replace(['-', ' '], ['', ''], $check);

        if ( preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/', $check) ) {
            $credittype = 'visa';
        }

        if ( preg_match('/^5[1-5][0-9]{14}$/', $check) ) {
            $credittype = 'mastercard';
        }

        if ( preg_match('/^3[47][0-9]{13}$/', $check) ) {
            $credittype = 'amex';
        }

        if ( preg_match('/^6(?:011|5[0-9]{2})[0-9]{12}$/', $check) ) {
            $credittype = 'discover';
        }

        return ! empty($target) ? $credittype === $target :
            ! empty($credittype);
    }

    public static function checkPassword($blankNewPw)
    {
        $policy = new Policy;
        $policy->contains('lowercase', $policy->atLeast(1));
        $result = $policy->test($blankNewPw);
        $hasLowercase = $result->result;
        /*
                $policy = new Policy;
                $policy->contains('uppercase', $policy->atLeast(1));
                $result = $policy->test($blankNewPw);
                $hasUppercase = $result->result;

                $policy = new Policy;
                $policy->contains('digit', $policy->atLeast(1));
                $result = $policy->test($blankNewPw);
                $hasNumber = $result->result;

                $policy = new Policy;
                $policy->contains('symbol', $policy->atLeast(1));
                $result = $policy->test($blankNewPw);
                $hasSymbol = $result->result;

                $rulesCheck = ((int)$hasLowercase) + ((int)$hasUppercase) + ((int)$hasNumber) + ((int)$hasSymbol);
        */

        $rulesCheck = (int) $hasLowercase;

        if ( $rulesCheck < 1 ) {
            return false;
        }

        return true;
    }


}
