<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior\TranslateBehavior;
use App\Library\Translation\ShadowTableStrategy;

class SxTranslationBehavior extends TranslateBehavior
{
    /**
     * Default strategy class name.
     */
    protected static $defaultStrategyClass = ShadowTableStrategy::class;

    /**
     * Merge config data to strategy
     *
     * @param $values
     * @return $this
     */
    public function mergeConfig($values)
    {
        /** @var ShadowTableStrategy $strategy */
        $strategy = $this->getStrategy();

        $strategy->mergeConfig($values);

        return $this;
    }

}
