<?php

namespace App\Model\Behavior;


use App\Library\AuthManager;
use App\Library\Facades\Menu;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Utility\Text;

// TODO: Alles

class SxFrontendmenuBehavior extends Behavior
{
    protected $_cacheConfig = null;

    protected $_duration = null;

    protected $_languageAccept = null;

    protected $_defaultConfig = [
        'field' => 'title',
        'slug' => 'slug',
        'replacement' => '-',
    ];

    public function initialize(array $config): void
    {
        $this->_cacheConfig = [
            'duration' => '+365 days',
            'prefix'   => 'menu',
            'engine'   => 'File',
            'path'     => CACHE . 'menu' . DS,
        ];

        $this->_languageAccept = config('Sx.app.language.accept');

        if (!is_array($this->_languageAccept)) {
            $this->_languageAccept = [$this->_languageAccept];
        }

        foreach ($this->_languageAccept as $k => $v) {
            $this->_languageAccept[$k] = '/' . $v;
        }

    }

    public function deleteCache()
    {
        Cache::drop('sx_menu');
        Cache::setConfig('sx_menu', $this->_cacheConfig);
        Cache::clear( 'sx_menu');
//        Cache::gc();
    }

    public function beforeSave(Event $event, EntityInterface $entity)
    {
        $this->deleteCache();
    }

    public function afterSave(Event $event, EntityInterface $entity)
    {
        $this->deleteCache();
    }

    /*
     * gibt die Gültigkeit des Menü-Caches zurück
     */
    private function _getCacheDuration()
    {

        $now = date('Y-m-d H:i:s');

        if ( ! empty($this->_duration) ) {
            return $this->_duration;
        }

        $nextEndPublishing = $this->_table->find('published')->select('end_publishing')->where([
            'end_publishing IS NOT' => null,
            'end_publishing >' => $now,
        ])->order(['end_publishing' => 'ASC'])->first();

        $nextBeginPublishing = $this->_table->find('published')->select('begin_publishing')->where([
            'begin_publishing IS NOT' => null,
            'begin_publishing >' => $now,
        ])->order(['begin_publishing' => 'ASC'])->first();

        if (!empty($nextEndPublishing) && !empty($nextBeginPublishing)) {
            if ($nextEndPublishing->end_publishing < $nextBeginPublishing->begin_publishing) {
                $then = $nextEndPublishing->end_publishing;
            } else {
                $then = $nextBeginPublishing->begin_publishing;
            }

        } else {
            if (!empty($nextEndPublishing)) {
                $then = $nextEndPublishing->end_publishing;

            } else {
                if (!empty($nextBeginPublishing)) {
                    $then = $nextEndPublishing->begin_publishing;
                }
            }
        }

        if (!empty($then)) { //pr($then);
            $then = strtotime($then);
            $duration = $then - time();
        } else {
            $duration = '+365 days';
        }

        $this->_duration = $duration;

        return $duration;
    }

    public function buildMenu($menuName, $type = 'Main')
    {
        $buildedMenu = AuthManager::unprotected(function () use ($menuName) {
            return $this->getFrontendMenu($menuName);
        });

        return $buildedMenu;
    }

    public function buildMenu2($menuName = "", $type = "submenu")
    {
        $this->_cacheConfig['prefix'] = strtolower(Text::slug('menu-' . Configure::read('Config.language') . '-' . Text::slug(request()->domain()) . '-' . $menuName . '-'));
        $this->_cacheConfig['duration'] = $this->_getCacheDuration();

        Cache::drop('sx_menu');
        Cache::setConfig('sx_menu', $this->_cacheConfig);

        if (Configure::read('debug')) {
            //Cache::clearAll();
        }

        return call_user_func([$this, '_startbuildung_' . $type], $menuName);
    }

    public function _getHoleTree($menuName)
    {
        $cachekey = strtolower(Text::slug('holemenu' . '-' . (AuthManager::getUser('User.type') ? AuthManager::getUser('User.type') : 'na')));
        $tree = Cache::read($cachekey, 'sx_menu');

        if (empty($tree)) {

            $tree = $this->getFrontendMenu($menuName);

            Cache::write($cachekey, $tree, 'sx_menu');
        }

        return $tree;
    }

    /*
     * der gesamte baum
     */
    public function _startbuildung_mainmenu($menuName)
    {
        $cachekey = strtolower(Text::slug('mainmenu-' . (AuthManager::getUser('User.type') ? AuthManager::getUser('User.type') : 'na')));
        $tree = Cache::read($cachekey, 'sx_menu');
        if (empty($tree)) {
            $tree = $this->_getHoleTree($menuName);
            Cache::write($cachekey, $tree, 'sx_menu');
        }

        return $tree;
    }

    /**
     * Komplettes Menü und der pfad ist aufgeklappt inkl. der Kinder des aktuellen Menüs
     *
     */
    public function _startbuildung_aufklappnavi($menuName)
    {

        $c = menu()->getMenuRecord();
        $currentId = null;

        if (empty($c)) {
            $currentId = $c->id;
        }

        $cachekey = strtolower(Text::slug('aufklappnavi-' . $currentId . '-' . (AuthManager::getUser('User.type') ? AuthManager::getUser('User.type') : 'na')));
        $tree = Cache::read($cachekey, 'sx_menu');

        if (empty($tree)) {
            $tree = $this->_getHoleTree($menuName);
            $slugs = request()->segments();

            $tree = $this->_aufklappnavi($tree, $slugs);
            Cache::write($cachekey, $tree, 'sx_menu');
        }

        return $tree;
    }

    public function _startbuildung_submenu($menuName)
    {

        $c = menu()->getMenuRecord();
        $currentId = null;

        if (!empty($c)) {
            $currentId = $c->id;
        }

        $cachekey = strtolower(Text::slug('submenu-' . $currentId . '-' . (AuthManager::getUser('User.type') ? AuthManager::getUser('User.type') : 'na')));
        $tree = Cache::read($cachekey, 'sx_menu');

        if (empty($tree)) {
            $tree = $this->_getHoleTree($menuName);
            $slugs = request()->segments();
            $tree = $this->_aufklappnavi($tree, $slugs);
            if (isset($slugs[0])) {
                foreach ($tree as $item) {
                    if ($item->slug == $slugs[0]) {
                        $tree = $item->children;
                        break;
                    }
                }
            }

            Cache::write($cachekey, $tree, 'sx_menu');
        }

        return $tree;
    }

    /* 1. Ebene => Kinder , 2. Ebene */
    public function _startbuildung_portalnavi($Model, $menustructur_id)
    {
        $c = menu()->getMenuRecord();

        if (empty($c)) {
            $c['Menu']['id'] = null;
        }

        $cachekey = 'portalnavi_' . $c['Menu']['id'] . '_' . (AuthManager::getUser('User.type') ? strtolower(AuthManager::getUser('User.type')) : 'na');
        $tree = Cache::read($cachekey, 'dv_menu');

        if ($tree) {
            return $tree;
        } else {
            $data = $this->_getHoleTree($Model, $menustructur_id);

            $slugs = request()->segments();

            $submenu = [];
            if (count($slugs) == 1) {
                foreach ($data as $k => $item) {
                    if ($item['Menu']['slug'] == $slugs[0]) {
                        $submenu = $item['children'];
                        foreach ($submenu as $k => $item) {
                            unset($submenu[$k]['children']);
                        }
                        break;
                    }
                }

            } else {
                if (count($slugs) > 1) {
                    foreach ($data as $item) {
                        if ($item['Menu']['slug'] == $slugs[0]) {
                            array_shift($slugs);
                            $submenu = $this->_aufklappnavi($Model, $item['children'], $slugs);
                            break;
                        }
                    }

                    //startseite
                } else {
                    if (count($slugs) == 0) {
                        foreach ($data as $k => $item) {
                            if ($item['Menu']['url'] == '/') {
                                $submenu = $item['children'];
                                foreach ($submenu as $k => $item) {
                                    unset($submenu[$k]['children']);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            if ($Model->useCache) {
                Cache::write($cachekey, $submenu, 'dv_menu');
            }

            return $submenu;
        }
    }

    /* Auf der Stratseite, alle Kinder, ab der 2. Ebene, aktueller Punkt ohne Nachbarn und darunter die Kinder */
    public function _startbuildung_raphael($Model, $menustructur_id)
    {
        $c = menu()->getMenuRecord();
        if (empty($c)) {
            $c['Menu']['id'] = null;
        }
        $cachekey = 'raphael_' . $c['Menu']['id'] . '_' . (AuthManager::getUser('User.type') ? strtolower(AuthManager::getUser('User.type')) : 'na');
        $tree = Cache::read($cachekey, 'dv_menu');
        if ($tree) {
            return $tree;
        } else {
            $data = $this->_getHoleTree($Model, $menustructur_id);
            $slugs = request()->segments();
            if (count($slugs) > 0) {
                foreach ($data as $k => $menu) {
                    if ($menu['Menu']['slug'] != $slugs[0]) {
                        unset($data[$k]);
                    }
                }
            }
            $data = $this->_raphaelmenu($Model, $data, $slugs);
            if ($Model->useCache) {
                Cache::write($cachekey, $data, 'dv_menu');
            }

            return $data;
        }
    }

    //aufklappnavi
    public function _aufklappnavi($data, $slugs)
    {
        $ndata = [];
        foreach ($data as $k => $item) {
            if (!empty($slugs) && $item->slug == $slugs[0]) {
                array_shift($slugs);
                if (!empty($slugs)) {
                    $item->children = $this->_aufklappnavi($item->children, $slugs);
                } else {
                    $nitems = [];
                    foreach ($item->children as $children) {
                        unset ($children['children']);
                        $nitems[] = $children;
                    }
                    $item['children'] = $nitems;
                }
                $ndata[$k] = $item;
            } else {
                $item['children'] = [];
                $ndata[$k] = $item;
            }
        }

        return $ndata;
    }



    /**
     * Raphael Style
     *
     *
     */
    //aufklappnavi
    public function _raphaelmenu($Model, $data, $slugs)
    {
        $ndata = [];
        foreach ($data as $k => $item) {
            if (!empty($slugs) && $item['Menu']['slug'] == $slugs[0]) {
                array_shift($slugs);
                if (!empty($slugs)) {
                    $item['children'] = $this->_raphaelmenu($Model, $item['children'], $slugs);
                } else {
                    $nitems = [];
                    foreach ($item['children'] as $children) {
                        unset ($children['children']);
                        $nitems[] = $children;
                    }
                    $item['children'] = $nitems;
                }
                $ndata[] = $item;
            } else {
                unset ($item['children']);
                $ndata[] = $item;
            }
        }

        return $ndata;
    }

    function _submenu($data, $slugs)
    {
        $ndata = [];
        foreach ($data as $k => $item) {
            if (!empty($slugs) && $item['Menu']['slug'] == $slugs[0]) {
                array_shift($slugs);
                if (!empty($slugs)) {
                    $item['children'] = $this->_raphaelmenu($item['children'], $slugs);
                } else {
                    $nitems = [];
                    foreach ($item['children'] as $children) {
                        unset ($children['children']);
                        $nitems[] = $children;
                    }
                    $item['children'] = $nitems;
                }
                $ndata[$k] = $item;
            } else {
                unset ($item['children']);
                $ndata[$k] = $item;
            }
        }

        return $ndata;
    }

    /**
     * Einfach Meer Style
     *
     */
    public function _einfachmeersubmenu($Model, $data)
    {
        $tmp = [];
        $found = false;
        foreach ($data as $k => $n) {
            $ndata = ['Menu' => $n['Menu']];
            $c = menu()->getMenuRecord();

            if (empty($c)) {
                $c['Menu']['id'] = null;
            }

            if ($n['Menu']['id'] == $c['Menu']['id']) {
                $found = true;
                foreach ($n['children'] as $childs) {
                    $ndata['children'][] = ['Menu' => $childs['Menu']];
                }
            }
            $tmp[] = $ndata;
        }
        if ($found) {
            return $tmp;
        } else {
            if (!$found) {
                foreach ($data as $k => $n) {
                    if (array_key_exists('children', $n) && !empty($n['children'])) {

                        $ret = $this->_einfachmeersubmenu($Model, $n['children']);
                        if (!is_null($ret)) {
                            return $ret;
                        }
                    }
                }
            }
        }

        return null;

    }

    public function getFrontendMenu($menuName, $fallback = [])
    {
        $domain = Menu::getDomainRecord();

        if ( empty($domain) ) {
            return $fallback;
        }

        $conditions = [
            'Menus.title'     => $menuName,
            'Menus.parent_id' => $domain->id,
        ];

        $menuItem = $this->getTable()->find('published')
            ->where($conditions)->first();

        $query = $this->getTable()->find()
            ->where(['Menus.teaser & 2']);

        $options = [
            'query'   => $query,
            'contain' => 'Articles',
        ];

        return $this->getTable()->getChildren($menuItem, $options);
    }

}
