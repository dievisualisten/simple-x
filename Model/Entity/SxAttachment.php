<?php

namespace App\Model\Entity;

use App\Utility\VimeoUtility;
use App\Utility\YoutubeUtility;
use App\Model\Entity\Traits\ExtendedDataTrait;

/**
 * @version 4.0.0
 */
class SxAttachment extends AppEntity
{
    use ExtendedDataTrait;

    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
//        'type', /* folder or MediaType (image, document, audio, video, exct. ) */
//        'name_original', /* refactor to basename_original ?*/
//        'thumb' /* obsolet */
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'published',
        'editable',
        'originalurl',
        'cropurl',
        'width',
        'height',
        'preview',
        'versions',
        'file',
        '_locale'
    ];

    /**
     * Casted fields
     *
     * @var array
     */
    protected $_casts = [
        'instructions'      => 'array',
        'metadata'          => 'object',
        'extended_data'     => 'object',
        'extended_data_cfg' => 'object',
    ];

    /**
     * Get attachment path
     *
     * @param string $folder
     * @return string
     */
    public function getPath($folder = 'uploads'): string
    {
        return $this->getTable()->getVersionPath($this, $folder);
    }

    /**
     * Get attachment url
     *
     * @param string $folder
     * @return string
     */
    public function getUrl($folder = 'uploads'): string
    {
        return $this->getTable()->getVersionUrl($this, $folder);
    }

    /**
     * Set trimmed title
     *
     * @param $prop
     * @param $value
     * @return string
     */
    protected function _setTitle($value): string
    {
        return trim($value);
    }

    /**
     * Attachment replacement file
     *
     * @return mixed
     */
    protected function _getFile()
    {
        return ! isset($this->_fields['file']) ? null :
            $this->_fields['file'];
    }

    /**
     * Attachment replacement file
     *
     * @return null
     */
    protected function _setFile($value)
    {
        return $this->_fields['file'] = $value;
    }

    /**
     * Attachment is deletable
     *
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return isset($this->_fields['deletable']) ?
            $this->_fields['deletable'] : true;
    }

    /**
     * Is editable attachment
     *
     * @return bool
     */
    protected function _getEditable(): bool
    {
        return $this->get('parent_id') !== null;
    }

    /**
     * Get original url
     *
     * @return string
     */
    protected function _getOriginalurl(): string
    {
        return $this->getUrl('uploads');
    }

    protected function _getThumbnail()
    {
        if ( empty($this->_fields['thumb_id']) ) {
            return null;
        }

        return $this->_fields['thumbnail'];
    }

    protected function _getThumb()
    {
        if ( empty($this->_fields['thumb_id']) ) {
            return $this->_fields['thumb'];
        }

        return $this->get('thumbnail.thumb');
    }

    protected function _setThumb($value)
    {
        if ( ! empty($this->_fields['thumb_id']) ) {
            return $this->_fields['thumb'] = null;
        }

        return $this->_fields['thumb'] = $value;
    }

    /**
     * Cropurl of attachment
     *
     * @return string|null
     */
    protected function _getCropurl()
    {
        if ( ! $this->get('thumb') ) {
            return null;
        }

        $extension = @filesize($this->getPath('crop'));

        if ( $this->get('thumbnail') ) {
            $extension = @filesize($this->get('thumbnail')->getPath('crop'));
        }

        return $this->get('thumb', '') . '?v=' . $extension;
    }

    /**
     * Preview of attachment
     *
     * @return string|null
     */
    protected function _getPreview()
    {
        $extension = '';

        if ( $this->get('type') === 'image' ) {
            $extension = '?v=' . @filesize($this->getPath('crop'));
        }

        if ( $this->get('provider') === 'vimeo' ) {
            return VimeoUtility::getUrl($this);
        }

        if ( $this->get('provider') === 'youtube' ) {
            return YoutubeUtility::getUrl($this);
        }

        if ( $this->get('type') === 'image' ) {
            return $this->getUrl('xl') . $extension;
        }

        return $this->getUrl();
    }


    /**
     * Attachment width
     *
     * @return int
     */
    protected function _getWidth()
    {
        if ( $this->isNew() ) {
            return null;
        }

        if ( empty($this->thumb) ) {
            return null;
        }

        if ( $this->type !== 'image' ) {
            return null;
        }

        if ( $this->get('metadata.width') ) {
            return $this->get('metadata.width');
        }

        $path = $this->getPath('uploads');

        if ( ! file_exists($path) ) {
            return null;
        }

        $size = getimagesize($path);

        return $size[0];
    }

    /**
     * Attachment height
     *
     * @return int
     */
    protected function _getHeight()
    {
        if ( $this->isNew() ) {
            return null;
        }

        if ( empty($this->thumb) ) {
            return null;
        }

        if ( $this->type !== 'image' ) {
            return null;
        }

        if ( $this->get('metadata.height') ) {
            return $this->get('metadata.height');
        }

        $path = $this->getPath('uploads');

        if ( ! file_exists($path) ) {
            return null;
        }

        $size = getimagesize($this->getPath('uploads'));

        return $size[1];
    }

    /**
     * Get media versions
     *
     * @return array
     */
    protected function _getVersions()
    {
        if ( $this->get('thumb', '') === '' ) {
            return null;
        }

        $versions = [];

        foreach ( config('Media.formats') as $key => $version ) {

            if ( $key === 'crop' ) {
                continue;
            }

            $ratio = 1 / $version['size']['w'] * $version['size']['h'];

            $version['minsize'] = $version['warnsize'] = [
                'w' => $version['size']['w'] < $version['size']['h'] ?
                    50 : 50 * $ratio,
                'h' => $version['size']['w'] < $version['size']['h'] ?
                    50 * $ratio : 50,
            ];

            if ( $version['transform']['type'] == 'fitinside' ) {
                $version['transform']['preserveratio'] = false;
            }

            if (
                $version['transform']['type'] == 'fitinside' &&
                ! isset($version['transform']['allowupscale'])
            ) {
                $version['transform']['allowupscale'] = true;
            }

            if ( $version['transform']['type'] == 'scalecrop' ) {
                $version['transform']['preserveratio'] = true;
            }

            if (
                $version['transform']['type'] == 'scalecrop' &&
                ! isset($version['transform']['allowupscale'])
            ) {
                $version['transform']['allowupscale'] = false;
            }

            if ( ! isset($version['info']['name']) ) {
                $version['info']['name'] = $key;
            }

            $versions[$key] = $version;
        }

        return $versions;
    }

}
