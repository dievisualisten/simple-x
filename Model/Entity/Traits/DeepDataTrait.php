<?php

namespace App\Model\Entity\Traits;

use App\Utility\StringUtility;

/**
 * @version 4.0.0
 */

trait DeepDataTrait
{
    /**
     * Get getter methodname
     *
     * @param $property
     * @return string
     */
    protected static function __getMethod($property)
    {
        return '_get' . StringUtility::grizzly($property);
    }

    /**
     * Get setter methodname
     *
     * @param $property
     * @return string
     */
    protected static function __setMethod($property)
    {
        return '_set' . StringUtility::grizzly($property);
    }

    /**
     * Set deep data in fields
     *
     * @param $property
     * @param null $value
     * @param array $options
     * @return $this
     */
    public function set($property, $value = null, array $options = [])
    {
        $guard = ! is_string($property);

        if ( is_array($property) === false ) {
            $options = (array) $value;
        }

        if ( is_string($property) === true ) {
            $property = [$property => $value];
        }

        $options += ['setter' => true, 'guard' => $guard];

        foreach ( (array) $property as $key => $value ) {

            // Get foo.bar.test - foo
            $flatKey = preg_replace('/^(.*?)\.(.*?)$/', '$1', $key);

            // Get foo.bar.test - bar.test
            $deepKey = preg_replace('/^(.*?)\.(.*?)$/', '$2', $key);

            if ( $options['guard'] && ! $this->isAccessible($flatKey) ) {
                continue;
            }

            // Get original value
            $original = $this->get($flatKey);

            // Set entity to dirty
            $this->setDirty($flatKey, true);

            if ( ! array_key_exists($flatKey, $this->_original) && $original !== $value ) {
                $this->_original[$flatKey] = $original;
            }

            if ( $flatKey !== $deepKey ) {
                $value = data_set($original, $deepKey, $value);
            }

            if ( method_exists($this, '_castableSet') ) {
                $value = $this->_castableSet($flatKey, $value);
            }

            $method = self::_accessor($flatKey, 'set');

            if ( $options['setter'] === true && method_exists($this, $method) ) {
                $value = $this->{$method}($value, $options);
            }

            $this->_fields[$flatKey] = $value;
        }

        return $this;
    }

    /**
     * Get deep data in fields
     *
     * @param $property
     * @param null $fallback
     * @return mixed|null
     */
    public function &get($property, $fallback = null)
    {
        if ( ! strlen((string) $property) ) {
            throw new \InvalidArgumentException('Cannot get an empty property');
        }

        // Get foo.bar.test - foo
        $flatKey = preg_replace('/^(.*?)\.(.*?)$/', '$1', $property);

        // Get foo.bar.test - bar.test
        $deepKey = preg_replace('/^(.*?)\.(.*?)$/', '$2', $property);

        $value = null;

        if ( isset($this->_fields[$property]) ) {
            $value =& $this->_fields[$property];
        }

        if ( method_exists($this, '_castableGet') ) {
            $value = $this->_castableGet($flatKey);
        }

        $method = self::_accessor($flatKey, 'get');

        if ( method_exists($this, $method) ) {
            $value = $this->{$method}($value);
        }

        if ( $flatKey !== $deepKey ) {
            $value = data_get($value, $deepKey);
        }

        if ( is_null($value) === true ) {
            $value = $fallback;
        }

        return $value;
    }

    /**
     * Returns true if key is empty
     *
     * @param $key
     * @return bool
     */
    public function isEmpty($key): bool
    {
        return empty($this->get($key, null));
    }

}
