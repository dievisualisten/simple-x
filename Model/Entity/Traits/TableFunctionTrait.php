<?php

namespace App\Model\Entity\Traits;

use Cake\ORM\TableRegistry;

/**
 * @version 4.0.0
 */

trait TableFunctionTrait
{
    /**
     * Return entity table
     *
     * @return \Cake\ORM\Table
     */
    public function getTable()
    {
        return TableRegistry::getTableLocator()
            ->get($this->getSource());
    }

    /**
     * Return entity schema
     *
     * @return \Cake\Database\Schema\TableSchemaInterface
     */
    public function getSchema()
    {
        return $this->getTable()->getSchema();
    }

}
