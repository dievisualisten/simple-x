<?php

namespace App\Model\Entity\Traits;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\Utility\Text;

/**
 * @version 4.0.0
 */

trait ClonableTrait
{
    /**
     * Clone entity with all given props
     *
     * @param array $props
     * @return $this
     */
    public function clone($props = [])
    {
        $clone = clone $this;

        foreach ( $props as $prop ) {
            if ( ! empty($this->{$prop}) ) {
                $this->{$prop} = clone $this->{$prop};
            }
        }

        return $clone;
    }

}
