<?php

namespace App\Model\Entity\Traits;

/**
 * @version 4.0.0
 */

trait CastableTrait
{
    /**
     * Casts are initialized
     *
     * @var bool
     */
    protected $initCasts = false;

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        //
    ];

    /**
     * Forget field casts
     *
     * @var array
     */
    protected $_forgetCasts = [
        'id', 'modified', 'created'
    ];

    /**
     * Deny casts for type
     *
     * @var array
     */
    protected $_denyCasts = [
        'datetime'
    ];

    /**
     * Create schema by casts
     */
    protected function _createCastableSchema()
    {
        $casts = $this->getSchema()->typeMap();

        $casts = array_filter($casts, function($format) {
            return ! in_array($format, $this->_denyCasts);
        });

        $casts = array_filter($casts, function($key) {
            return ! in_array($key, $this->_forgetCasts);
        }, ARRAY_FILTER_USE_KEY);

        $this->_casts = array_merge($casts, $this->_casts);

        foreach ( $this->_casts as $key => $format ) {

            if ( $format === 'array' ) {
                $format = 'json';
            }

            if ( $format === 'object' ) {
                $format = 'json';
            }

            $this->getSchema()->setColumnType($key, $format);
        }

        $this->initCasts = true;
    }

    /**
     * Get casted field
     *
     * @param $property
     * @return mixed
     */
    public function &_castableGet($property)
    {
        if ( $this->initCasts === false ) {
            $this->_createCastableSchema();
        }

        $result =& $this->_fields[$property];

        if ( isset($this->_casts[$property]) ) {
            $result = $this->_castGetAttribute($property);
        }

        return $result;
    }

    /**
     * Set casted field
     *
     * @param $property
     * @param $value
     * @return mixed
     */
    public function _castableSet($property, $value)
    {
        if ( $this->initCasts === false ) {
            $this->_createCastableSchema();
        }

        $result = $value;

        if ( isset($this->_casts[$property]) ) {
            $result = $this->_castSetAttribute($property, $value);
        }

        return $this->_fields[$property] = $result;
    }

    /**
     * Getter exists for field
     *
     * @param $property
     * @return bool
     */
    protected function _hasGetter($property)
    {
        $method = self::_accessor($property, 'get');

        return method_exists($this, $method);
    }

    /**
     * Call field getter
     *
     * @param $property
     * @return mixed
     */
    protected function _callGetter($property)
    {
        $method = self::_accessor($property, 'get');

        return $this->{$method}($property);
    }

    /**
     * Setter exists for field
     *
     * @param $property
     * @return bool
     */
    protected function _hasSetter($property)
    {
        $method = self::_accessor($property, 'get');

        return method_exists($this, $method);
    }

    /**
     * Call field setter
     *
     * @param $property
     * @param $value
     * @return mixed
     */
    protected function _callSetter($property, $value)
    {
        $method = self::_accessor($property, 'get');

        return $this->{$method}($property, $value);
    }

    /**
     * Cast get attribute
     *
     * @param $property
     * @return mixed
     */
    protected function &_castGetAttribute($property)
    {
        $value =& $this->_fields[$property];

        if ( isset($this->_casts[$property]) ) {
            $value = format_type($this->_casts[$property], $value);
        }

        return $value;
    }

    /**
     * Cast set attribute
     *
     * @param $property
     * @param $value
     * @return mixed
     */
    protected function _castSetAttribute($property, $value)
    {
        if ( isset($this->_casts[$property]) ) {
            $value = format_type($this->_casts[$property], $value);
        }

        return $this->_fields[$property] = $value;
    }

    /**
     * Get visible fields
     *
     * @return array
     */
    public function getVisible(): array
    {
        if ( $this->initCasts === false ) {
            $this->_createCastableSchema();
        }

        return array_merge(parent::getVisible(), array_keys($this->_casts));
    }

}
