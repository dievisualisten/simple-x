<?php

namespace App\Model\Entity\Traits;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Entity;

/**
 * @version 4.0.0
 */

trait ClearableTrait
{
    /**
     * Get clearable fields from schema
     *
     * @return array
     */
    public function getClearableFields()
    {
        $schema = $this->getSchema();

        $keys = array_merge(
            $schema->primaryKey(), $schema->constraints(), $schema->indexes()
        );

        return array_diff($schema->columns(), $keys);
    }

    /**
     * Clear fields in entity
     *
     * @return $this
     */
    public function clear()
    {
        foreach ($this->getTable()->associations() as $relation) {

            $field = $relation->getProperty();

            if (
                is_a($relation, HasMany::class) ||
                is_a($relation, BelongsToMany::class)
            ) {
                $this->{$field} = null;
            }

            if (
                is_a($relation, HasOne::class) ||
                is_a($relation, BelongsTo::class)
            ) {
                if (is_a($this->{$field}, Entity::class)) {
                    $this->{$field}->clear();
                }
            }

        }

        foreach ($this->getClearableFields() as $field) {
            $this->{$field} = null;
        }

        return $this;
    }

}
