<?php

namespace App\Model\Entity\Traits;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;

/**
 * @version 4.0.0
 */

trait AutofillableTrait
{
    /**
     * Autofill entity including relations
     *
     * @return $this
     */
    public function autofill()
    {
        foreach ( $this->getTable()->associations() as $association ) {

            $contains = $this->getTable()->getDefaultContains();

            if ( ! in_array($association->getName(), $contains) ) {
                continue;
            }

            $prop = $association->getProperty();

            if ( $this->{$prop} !== null ) {
                continue;
            }

            if ( get_class($association) === HasMany::class ) {
                $this->{$prop} = [];
            }

            if ( get_class($association) === BelongsToMany::class ) {
                $this->{$prop} = [];
            }

            $target = $association->getTarget();

            if ( get_class($association) === HasOne::class ) {
                $this->{$prop} = $target->newEntity([])->toArray();
            }

            if ( get_class($association) === BelongsTo::class ) {
                $this->{$prop} = $target->newEntity([])->toArray();
            }

            continue;
        }

        return $this;
    }

}
