<?php

namespace App\Model\Entity\Traits;

/**
 * @version 4.0.0
 */

trait ExtendedDataTrait
{
    public function _getCustom()
    {
        return collect($this->_fields['extended_data_cfg'])
            ->merge($this->_fields['extended_data'])->toArray();
    }

}
