<?php

namespace App\Model\Entity\Traits;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\Utility\Text;

/**
 * @version 4.0.0
 */

trait DublicatableTrait
{
    /**
     * Dublicate entity with all relations
     *
     * @return $this
     */
    public function dublicate()
    {
        $primary = $this->getTable()->getPrimaryKey();

        if ( is_array($primary) ){
            $primary = array_shift($primary);
        }

        $clone = $this->getTable()->newEntity([]);

        $clone->set($this->_fields);

        if ( $primary === 'id' ) {
            $clone->set($primary, Text::uuid());
        }

        $clone->isNew(true);

        foreach ( $this->getTable()->associations() as $association ) {

            $contains = $this->getTable()->getDublicateContains();

            if ( ! in_array($association->getName(), $contains) ) {
                continue;
            }

            $foreignKey = $association->getForeignKey();

            $prop = $association->getProperty();

            if ( $this->{$prop} === null ) {
                continue;
            }

            if ( get_class($association) === HasMany::class ) {
                 foreach ( $this->{$prop} as $item ) {

                     if ( $foreignKey ) {
                         $clone->unset($foreignKey);
                     }

                     $clone->{$prop}[] = $item->dublicate();
                 }
            }

            if ( get_class($association) === BelongsToMany::class ) {
                foreach ( $this->{$prop} as $item ) {

                    if ( $foreignKey ) {
                        $clone->unset($foreignKey);
                    }

                    $clone->{$prop}[] = $item->dublicate();
                }
            }

            if ( get_class($association) === HasOne::class ) {

                if ( $foreignKey ) {
                    $clone->unset($foreignKey);
                }

                $clone->{$prop} = $this->{$prop}->dublicate();
            }

            if ( get_class($association) === BelongsTo::class ) {

                if ( $foreignKey ) {
                    $this->unset($foreignKey);
                }

                $clone->{$prop} = $this->{$prop}->dublicate();
            }

            continue;
        }

        return $clone;
    }

}
