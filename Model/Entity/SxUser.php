<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxUser extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'newpassword'
    ];

    /**
     * Set trimmed login
     *
     * @param $key
     * @param $value
     * @return string
     */
    protected function _setLogin($value)
    {
        return trim($value);
    }

    /**
     * Set trimmed password
     *
     * @param $key
     * @param $value
     * @return string
     */
    protected function _setPassword($value)
    {
        return trim($value);
    }

    /**
     * Set trimmed password
     *
     * @param $key
     * @param $value
     * @return string
     */
    protected function _setNewpassword($value)
    {
        return trim($value);
    }



}
