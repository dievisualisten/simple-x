<?php

namespace App\Model\Entity;

use App\Library\Facades\Language;
use App\Model\Entity\Traits\ClonableTrait;
use Cake\Log\LogTrait;
use Cake\ORM\Entity;
use Cake\Utility\Text;
use App\Model\Entity\Traits\TableFunctionTrait;
use App\Model\Entity\Traits\DeepDataTrait;
use App\Model\Entity\Traits\CastableTrait;
use App\Model\Entity\Traits\DublicatableTrait;
use App\Model\Entity\Traits\AutofillableTrait;
use App\Model\Entity\Traits\ClearableTrait;

/**
 * @version 4.0.0
 */

class SxAppEntity extends Entity
{
    use LogTrait, TableFunctionTrait, DeepDataTrait, CastableTrait, DublicatableTrait, AutofillableTrait, ClearableTrait, ClonableTrait;

    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    protected $_locale = null;

    /**
     * Append dragid and new state if not present
     *
     * @param array $properties
     * @param array $options
     */
    public function __construct(array $properties = [], array $options = [])
    {
        if ( ! in_array('dragid', $this->_virtual) ) {
            array_push($this->_virtual, 'dragid');
        }

        if ( ! in_array('_new', $this->_virtual) ) {
            array_push($this->_virtual, '_new');
        }

        parent::__construct($properties, $options);
    }

    public function getLocale()
    {
        return $this->_get_locale();
    }

    public function isDefaultLanguage()
    {
        return Language::isDefaultLanguage($this->_locale);
    }

    public function setCurrentLanguage()
    {
        return $this->_locale = Language::getLanguage();
    }

    public function setDefaultLanguage()
    {
        return $this->_locale = Language::getDefaultLanguage();
    }

    public function getDefaultTranslation()
    {
        if ( ! $this->getTable()->hasBehavior('Translation') ) {
            return $this;
        }

        return Language::inDefaultLanguage(function () {
            return $this->getTable()->forceFirstOrNull($this);
        });
    }

    public function getExistingTranslations()
    {
        if ( ! $this->getTable()->hasBehavior('Translation') ) {
            return [];
        }

        return Language::inDefaultLanguage(function () {

            $conditions = [
                'id' => $this->get('id'),
            ];

            $entity = $this->getTable()->find('translations')
                ->where($conditions)->first();

            return data_get($entity, '_translations', []);
        });
    }

    /**
     * Get dragid for backend
     *
     * @return string
     */
    protected function _getDragid()
    {
        return Text::uuid();
    }

    /**
     * Get new state for backend
     *
     * @return bool
     */
    protected function _get_new()
    {
        return $this->isNew();
    }

    /**
     * Get locale for backend
     *
     * @return string
     * @throws \Exception
     */
    protected function _get_locale()
    {
        if ( ! isset($this->_locale) ) {
            $this->_locale = Language::getDefaultLanguage();
        }

        return $this->_locale;
    }

    /**
     * Get locale for backend
     *
     * @param $value
     * @return string
     */
    protected function _set_locale($value)
    {
        return $this->_locale = $value;
    }

}
