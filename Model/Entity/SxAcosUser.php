<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * @version 4.0.0
 */

/**
 * AcosUser Entity
 *
 * @property int $id
 * @property string $aco_id
 * @property string $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Aco $aco
 * @property \App\Model\Entity\User $user
 */
class SxAcosUser extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'aco_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'aco' => true,
        'user' => true,
    ];
}
