<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxFormularconfig extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        '_locale'
    ];

}
