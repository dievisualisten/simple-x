<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxAttachedTranslation extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

}
