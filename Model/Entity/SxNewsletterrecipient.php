<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxNewsletterrecipient extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Get first or new person
     *
     * @return mixed
     */
    protected function _getPerson2()
    {
        if ( ! empty($this->_fields['person']) ) {
            return $this->_fields['person'];
        }

        return $this->getTable()->getAssociation('Persons')->newEntity();
    }

    /**
     * Get salutation attribute
     *
     * @return string
     */
    protected function _getSalutation()
    {

        if ( empty($this->_fields['salutation']) ) {
            // TODO generate salutation
        }

        return @$this->_fields['salutation'] ?: null;
    }

}
