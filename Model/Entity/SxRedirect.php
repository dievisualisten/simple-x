<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxRedirect extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        'from_url'      => 'string',
        'target_url'    => 'string',
        'active'        => 'boolean',
        'generated'     => 'boolean',
        'locked'        => 'boolean',
        'notify'        => 'boolean',
        'status'        => 'string',
        'created'       => 'timestamp',
        'modified'      => 'timestamp',
    ];

}
