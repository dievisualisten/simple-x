<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxNewsletterinterest extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'newslettercount'
    ];

    /**
     * Get newsletter count
     *
     * @return mixed
     */
    protected function _getNewslettercount()
    {
        $statement = `SELECT COUNT(*) from newsletterinterests_newsletters WHERE newsletterinterest_id = '%s'`;

        if ( $this->newsletters !== null ) {
            return $this->newsletters->count();
        }

        return $this->getTable()->find()
            ->execute(sprintf($statement, $this->_fields['id']))->count();
    }

}
