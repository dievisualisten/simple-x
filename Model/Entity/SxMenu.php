<?php

namespace App\Model\Entity;

use App\Library\AuthManager;
use App\Library\Facades\Language;
use App\Library\Facades\Menu as MenuService;
use App\Utility\StringUtility;
use Cake\I18n\Time;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * @version 4.0.0
 */

class SxMenu extends AppEntity
{
    use TranslateTrait;

    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'fullPath',
        'previewPath',
        'published',
        'transaction',
        '_locale',
    ];

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        'extended_data'     => 'object',
        'extended_data_cfg' => 'object',
    ];

    public function canHaveUniquePath($blockedTypes = ['menu', 'frontpage', 'alias'])
    {
        if ( in_array($this->get('type', 'page'), $blockedTypes) ) {
            return false;
        }

        $teaser = MenuService::getMenuVisibility($this->get('teaser'));

        if ( empty(array_diff($teaser, TEASER_WITHOUT_URL)) ) {
            return false;
        }

        return true;
    }

    public function getExistingLocales()
    {
        return AuthManager::unprotected(function () {

            $entity = Language::inDefaultLanguage(function () {
                return $this->getTable()->forceFirstOrFail($this);
            });

            $available = [
                Language::getDefaultLanguage() => $entity
            ];

            if ( empty($this->locales) ) {
                return $available;
            }

            $locales = collect($this->locales)->filter(function ($value, $key) {
                return $key === Language::getDefaultLanguage() || $value->get('_locale') !== Language::getDefaultLanguage();
            });

            return $locales->merge($available)->toArray();
        });
    }

    /**
     * Convert slug in matching format
     *
     * @param $key
     * @param $value
     * @return string
     */
    protected function _setSlug($value)
    {
        if ( $this->_fields['type'] == 'domain' ) {
            return StringUtility::lower($value);
        }

        return StringUtility::slug($value);
    }

    /**
     * Get published state
     *
     * @return bool
     */
    protected function _getPublished()
    {
        if ( config('preview', false) ) {
            return true;
        }

        if ( ! $this->get('active', false) ) {
            return false;
        }

        return (! ($beginDate = $this->get('begin_publishing')) || $beginDate->lt(Time::now()))
            && (! ($endDate = $this->get('end_publishing')) || $endDate->gte(Time::now()));
    }

    /**
     * Get transaction type
     *
     * @return string
     */
    protected function _getTransaction()
    {
        return 'menus';
    }

    

    /**
     * Get exploded params
     * FixMe Eddy, wofür ist das?
     * @return array
     */
    protected function _getParams1()
    {
        $params = preg_split('/\s*,\s*/', $this->_fields['params']);

        return array_filter($params);
    }

    /**
     * Get full menu path
     *
     * @return string
     * @throws \Exception
     */
    protected function _getFullPath()
    {
        if ( $this->has('path') === false ) {
            return null;
        }

        return request()->scheme() . request()->getContextDomain($this->get('path'));
    }

    /**
     * Get path with preview query
     *
     * @return string
     * @throws \Exception
     */
    protected function _getPreviewPath()
    {
        if ( $this->isEmpty('path') ) {
            return '';
        }

        return $this->_getFullPath() . '?' . request()->queryString(['preview' => true], false);
    }

}
