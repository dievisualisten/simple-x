<?php

namespace App\Model\Entity;

use App\Model\Entity\AppTraits\ElementsTrait;
use App\Model\Table\Traits\ExtenderTrait;
use App\Utility\StringUtility;
use App\Model\Entity\AppTraits\MediaTrait;
use App\Model\Entity\AppTraits\PublishedTrait;
use App\Model\Entity\AppTraits\TabparentsTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use App\Model\Entity\AppTraits\ExtendedTrait;

/**
 * @version 4.0.0
 */
class SxArticle extends AppEntity
{
    use TranslateTrait, ExtendedTrait, MediaTrait, PublishedTrait, TabparentsTrait, ElementsTrait, ExtenderTrait;

    /**
     * Media tabs which will be fillable
     *
     * @var string
     */
    protected $_mediaKey = 'Sx.resource.modules.article.panels.media.tabs';

    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        //
    ];

    /**
     * Field cassts
     *
     * @var array
     */
    protected $_casts = [
        'lat'               => 'float',
        'lon'               => 'float',
        'attached'          => 'array',
        'extended_data'     => 'object',
        'extended_data_cfg' => 'object',
        'begin_publishing'  => 'datetime',
        'end_publishing'    => 'datetime',
    ];

    public function __construct(array $properties = [], array $options = [])
    {
        if ( ! in_array('domain_id', $this->_virtual) ) {
            array_push($this->_virtual, 'domain_id');
        }

        if ( ! in_array('published', $this->_virtual) ) {
            array_push($this->_virtual, 'published');
        }

        if ( ! in_array('media', $this->_virtual) ) {
            array_push($this->_virtual, 'media');
        }

        if ( ! in_array('transaction', $this->_virtual) ) {
            array_push($this->_virtual, 'transaction');
        }

        if ( ! in_array('_locale', $this->_virtual) ) {
            array_push($this->_virtual, '_locale');
        }

        $this->loopTraits('initialize', [$properties, $options]);

        parent::__construct($properties, $options);
    }

    /**
     * Slugify slug field
     *
     * @param $value
     * @return string
     */
    protected function _setSlug($value): string
    {
        return StringUtility::slug($value);
    }

    /**
     * Use fallback type if not set
     *
     * @param $value
     * @return string
     */
    protected function _getType($value): string
    {
        return empty($value) ? 'page' : $value;
    }

    /**
     * Use fallback layout if not set
     *
     * @param $value
     * @return string
     */
    protected function _getLayout($value): string
    {
        return empty($value) ? 'default' : $value;
    }

    /**
     * Get transaction type
     *
     * @return string
     */
    protected function _getTransaction(): string
    {
        return 'articles';
    }

    /**
     * @return array|mixed
     */
    public function _getDomainId()
    {
        return data_get($this->_fields, 'extended_data_cfg.domain_id', null);
    }

    public function _setDomainId($value): void
    {
        data_set($this->_fields['extended_data_cfg'], 'domain_id', $value);
    }

}
