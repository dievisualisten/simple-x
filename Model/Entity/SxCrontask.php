<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

/**
 * Accessible fields
 *
 * @var array
 */
class SxCrontask extends AppEntity
{
    protected $_accessible = [
        '*' => true,
    ];
}
