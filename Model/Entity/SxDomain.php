<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxDomain extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        'sequence' => 'integer'
    ];

}
