<?php

namespace App\Model\Entity\AppTraits;

trait TabparentsTrait {


    /**
     * Tabparents store
     * 
     * @var array
     */
    protected $_tabparents = [];

    public function _getTabparents()
    {
        if ( empty($this->_tabparents) ) {
            $this->makeTabparents();
        }

        return $this->_tabparents;
    }

    public function _setTabparents($value)
    {
        $this->_tabparents = $value;
    }

    /**
     * Make tabparents for element
     */
    public function makeTabparents(): void
    {
        if ( ! isset($this->menus) ) {
            return;
        }

        $this->tabparents = $this->getTable()->Menus
            ->getParentPaths($this->menus, 'parent_id', false);

        $parentPaths = [];

        foreach ( $this->tabparents as $tab ) {
            $parentPaths = array_merge($parentPaths, explode('/', $tab->parent_path));
        }

        $this->tabparents_expanded = array_values(
            array_filter(array_unique($parentPaths))
        );

        $this->unset('menus');
    }

}