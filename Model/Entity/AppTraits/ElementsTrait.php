<?php

namespace App\Model\Entity\AppTraits;

/**
 * Trait ElementsTrait
 *
 * @package App\Model\Entity\AppTraits
 * @property array $_virtual
 */
trait ElementsTrait {


    /**
     * Tabparents store
     * 
     * @var array
     */
    protected $_elements = [];

    public function initializeElements(): void
    {
        if ( ! in_array('elements', $this->_virtual) ) {
            array_push($this->_virtual, 'elements');
        }
    }

    public function _getElements(): array
    {
        if ( empty($this->_elements) ) {
            $this->makeElements();
        }

        return $this->_elements;
    }

    public function _setElements($value): void
    {
        $this->_elements = $value;
    }

    /**
     * Make tabparents for element
     */
    public function makeElements(): void
    {
        $elements = [];

        foreach ( $this->getTable()->getDefaultElements() as $relation ) {

            $elements[$relation] = [];

            if ( ! $this->{$relation} ) {
                continue;
            }

            $elements[$relation] = $this->{$relation};

            $this->unset($relation);
        }

        $this->_elements = $elements;
    }

}