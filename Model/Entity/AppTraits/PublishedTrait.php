<?php

namespace App\Model\Entity\AppTraits;

use Cake\I18n\Time;

trait PublishedTrait {

    /**
     * Is published conditioning
     *
     * @return bool
     */
    protected function _getPublished(): bool
    {
        if ( config('preview', false) ) {
            return true;
        }

        if ( ! $this->active ) {
            return false;
        }

        return (! ($beginDate = $this->begin_publishing) || $beginDate->lt(Time::now()))
            && (! ($endDate = $this->end_publishing) || $endDate->gte(Time::now()));
    }

}