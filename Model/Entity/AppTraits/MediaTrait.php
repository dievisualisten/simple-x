<?php

namespace App\Model\Entity\AppTraits;

use App\Utility\ArrayUtility;

trait MediaTrait {

    /**
     * Media fix for Cake (if not stored in entity the media will not be updated)
     *
     * @var array
     */
    protected $_media = [];

    /**
     * Get media key data from config
     *
     * @return array
     * @throws \Exception
     */
    public function getMediaKeyConfig(): array
    {
        if ( isset($this->_mediaKey) ) {
            return config($this->_mediaKey, []);
        }

        throw new \Exception('No media key given.');
    }

    /**
     * Set media store
     *
     * @param $value
     */
    protected function _setMedia($value): void
    {
        $this->_media = $value;
    }

    /**
     * Get media store or fetch media
     *
     * @return array
     * @throws \Exception
     */
    protected function _getMedia(): array
    {
        if ( ! empty($this->_media) ) {
            return $this->_media;
        }

        $result = [];

        foreach ( $this->getMediaKeyConfig() as $key => $value ) {
            $result[$key] = [];
        }

        if ( empty($this->attached) ) {
            return $result;
        }

        foreach ( ArrayUtility::sort($this->attached, 'sequence') as $attached ) {

            if ( empty($attached->attachment) ) {
                continue;
            }

            $value = $attached->attachment;

            if ( ! is_array($value) ) {
                $value = $value->toArray();
            }

            $result[$attached->type][] = array_merge($value, [
                'attached' => array_except($attached->toArray(), ['attachment'])
            ]);
        }

        return $result;
    }

}