<?php

namespace App\Model\Entity\AppTraits;

/**
 * @version 4.0.0
 */

trait ExtendedTrait
{
    /**
     * @return array
     */
    public function _getCustom(): array
    {
        return collect($this->_fields['extended_data_cfg'])
            ->merge($this->_fields['extended_data'])->toArray();
    }

}
