<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxProduct extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Field castings
     *
     * @var array
     */
    protected $_casts = [
        'id'                => 'uuid',
        'sale'              => 'boolean',
        'ean'               => 'string',
        'sku'               => 'string',
        'producer_sku'      => 'string',
        'price'             => 'float',
        'purchase_price'    => 'float',
        'producer_price'    => 'float',
        'depth'             => 'string',
        'height'            => 'string',
        'width'             => 'string',
        'weight'            => 'string',
        'quantity'          => 'integer',
        'quantity_alert'    => 'integer',
        'shipping_rate'     => 'float',
        'shipping_time'     => 'string',
        'extended_data'     => 'array',
        'created'           => 'datetime',
        'modified'          => 'datetime'
    ];

}
