<?php

namespace App\Model\Entity;

use App\Library\DomainManager;

/**
 * @version 4.0.0
 */

class SxPerson extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'name',
        'fullname',
    ];

    /**
     * Get full person name wit salutation
     *
     * @return string
     */
    protected function _getFullname()
    {
        if (!empty($this->_fields['salutation']) && isset($this->_fields['firstname']) && isset($this->_fields['lastname'])) {

            return trim(DomainManager::get('salutation', $this->_fields['salutation']) . ' ' . trim($this->_fields['firstname'] . ' ' . $this->_fields['lastname']));
        }

        return '';
    }

    /**
     * Get full person name
     * @return string
     */
    protected function _getName()
    {
        if (isset($this->_fields['firstname']) && isset($this->_fields['lastname'])) {
            return trim($this->_fields['firstname'] . ' ' . $this->_fields['lastname']);
        }

        return '';
    }

//    protected function _setBirthday($prop, $value)
//    {
//        dd($prop, $value);
//    }

}
