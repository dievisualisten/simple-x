<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxFormular extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    protected function _getUseMathCaptcha()
    {
        if ( $this->isNew() ) {
            return true;
        }

        return $this->_fields['captcha'];
    }

    protected function _getSendEmail()
    {
        if ( $this->isNew() ) {
            return true;
        }

        return $this->_fields['send_email'];
    }

}
