<?php

namespace App\Model\Entity;

use App\Model\Entity\AppTraits\PublishedTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use App\Model\Entity\AppTraits\MediaTrait;
use App\Model\Entity\AppTraits\ExtendedTrait;

/**
 * @version 4.0.0
 */

class SxElement extends AppEntity
{
    use TranslateTrait, ExtendedTrait, MediaTrait, PublishedTrait;

    /**
     * Media tabs which will be fillable
     *
     * @var string
     */
    protected $_mediaKey = 'Sx.resource.modules.article.panels.media.tabs';

    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'published', 'media', '_locale',
    ];

    /**
     * Field cassts
     *
     * @var array
     */
    protected $_casts = [
        'attached'          => 'array',
        'extended_data'     => 'object',
        'extended_data_cfg' => 'object',
        'begin_publishing'  => 'datetime',
        'end_publishing'    => 'datetime',
    ];

    /**
     * Use fallback type if not set
     *
     * @param $value
     * @return string
     */
    protected function _getType($value): string
    {
        return empty($value) ? 'page' : $value;
    }

    /**
     * Use fallback layout if not set
     *
     * @param $value
     * @return string
     */
    protected function _getLayout($value): string
    {
        return empty($value) ? 'default' : $value;
    }



}
