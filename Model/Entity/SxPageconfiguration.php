<?php

namespace App\Model\Entity;

/**
 * @version 4.0.0
 */

class SxPageconfiguration extends AppEntity
{
    /**
     * Accessible fields
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Virtual fields
     *
     * @var array
     */
    protected $_virtual = [
        'page_type', 'is_global', 'display_name'
    ];

    protected $_displayName = '';

    protected function _getPageType()
    {
        if ($this->get('name')) {
            $pageType = str_replace('resource.modules.article.pagetypes.', '', $this->get('name'));
            $pageType = explode('.', ucfirst($pageType));

            return $pageType[0];
        }

        return "";
    }

    protected function _getIsGlobal()
    {
        return false;
    }

    protected function _getDisplayName()
    {
        if ( ! $this->_displayName ) {
            $this->_displayName = str_replace('resource.modules.article.pagetypes.', '', $this->get('name'));
        }

        return $this->_displayName;
    }

    protected function _setDisplayName($value)
    {
        $this->_displayName = $value;
    }
}
