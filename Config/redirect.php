<?php


return [

    'Redirect' => [

        'states' => [
            'updated'     => 'Manuell geändert',
            'moved'       => 'Artikel verschoben oder Url geändert',
            'changed'     => 'Ziel-Artikel in Struktur gefunden',
            'removed'     => 'Kein Ziel gefunden',
            'activated'   => 'Aktiviert, Quelle in der Struktur nicht vorhanden',
            'deactivated' => 'Deaktiviert, Quelle in der Struktur vorhanden',
            'refrenced'   => 'Selbstreferenz',
            'dublicated'  => 'Mehrfachverweis einer Quelle',
        ],

    ],

];
