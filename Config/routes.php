<?php

use App\Routing\Router;
use App\Routing\Route\DatabaseRoute;

Router::locale(function ($routes) {

    /** @var \App\Routing\RouteBuilder $routes */

    $routes->setExtensions([
        'json', 'html', 'xml', 'xlsx', 'txt'
    ]);

    /**
     * SX2 routing
     */

    $formatID = [
        Router::UUID, '(' . Router::UUID . '(,|$))+',
    ];

    $routes->resources(':controller', [
        '_ext' => 'json', 'id' => implode('|', $formatID),
    ]);

    $routes->connect('/:controller/:action?/*?', [
        'action' => 'index',
    ]);

    $routes->connect('/system/fetch/:action?/*?', [
      'action' => 'index', 'controller' => 'system'
    ]);

    $routes->connect('/system/*?', [
        '_ext' => 'html', 'controller' => 'system', 'action' => 'index'
    ]);

    $routes->connect('/login', [
        'controller' => 'users', 'action' => 'login',
    ]);

    $routes->connect('/logout', [
        'controller' => 'users', 'action' => 'logout',
    ]);

    $routes->connect('/robots', [
        'controller' => 'sitemaps', 'action' => 'robots'
    ]);#
    $routes->connect('/sitemap', [
        'controller' => 'sitemaps', 'action' => 'index'
    ]);

    $routes->fallbacks(DatabaseRoute::class);
});
