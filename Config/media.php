<?php

return [

    'Media' => [

        'whiteSpaceAndBorderColor' => '#ff0000', //RGB WERTE

        'debug' => [
            'url' => env('MEDIA_URL', ''),
            'image' => env('MEDIA_IMAGE', ''),
            'is_sx2' => env('MEDIA_IS_SX2', 'true')
        ],

        'formats' => [

            'crop' => [

                'info' => [
                    'name' => 'Vorschau',
                    'description' => 'Vorschaubild im Backend',
                ],

                'size' => [
                    'w' => 640,
                    'h' => 640,
                ],

                'transform' => [
                    'type' => 'fitinside',
                    'allowupscale' => true
                ],

                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
                'orientate' => true,

            ],

            'xl' => [

                'info' => [
                    'name' => 'Bildgergalerie',
                    'description' => 'Bildergalerie Großansicht',
                ],

                'size' => [
                    'w' => 1500,
                    'h' => 1168,
                ],

                'transform' => [
                    'type' => 'fitinside',
                    'allowupscale' => false
                ],

                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ]

            ],

        ],
    ],

];

