<?php

return [

    'Spreadsheet' => [

        'filters' => [

            'string' => [
                ['de', 'Deutsch'],
                ['de', 'German'],

                ['en', 'Englisch'],
                ['en', 'English'],

                // Replace string with value
            ],

            'boolean' => [

                // True cases
                ['true', 'ja'],
                ['true', 'true'],
                ['true', 'wahr'],
                ['true', '1'],

                // False cases
                ['false', 'nein'],
                ['false', 'false'],
                ['false', 'falsch'],
                ['false', '0'],

            ],

        ],

    ],

];
