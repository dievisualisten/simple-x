<?php

return [

    /**
     * Configures logging options
     */
    'Log' => [

        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'debug',
            'levels' => ['debug'],
            'scopes' => [],
        ],

        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'error',
            'levels' => ['notice', 'warning', 'error', 'critical', 'alert', 'emergency','info'],
        ],

        '404' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => '404',
            'levels' => ['notice', 'warning', 'error', 'critical', 'alert', 'emergency','info'],
            'scopes' => ['404'],
        ],

        'queries' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'queries',
            'scopes' => ['queriesLog'],
        ],


    ],
];
