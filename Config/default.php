<?php

return [

    /**
     * Debug mode (true|false)
     */
    'debug' =>  filter_var(env('APP_DEBUG', false), FILTER_VALIDATE_BOOLEAN),

    /**
     * Debugbar (true|false)
     */
    'debugbar' => filter_var(env('APP_DEBUGBAR', false), FILTER_VALIDATE_BOOLEAN),

    /**
     * Minify mode (true|false)
     */
    'minify' => filter_var(env('APP_MINIFY', true), FILTER_VALIDATE_BOOLEAN),

    /**
     * Minify mode (true|false)
     */
    'whoops' => filter_var(env('APP_WHOOPS', true), FILTER_VALIDATE_BOOLEAN),

    /**
     * Enviroment mode (local|live)
     */
    'env' => env('APP_ENV', 'live'),

    /**
     * Ip's wich can always access debug mode
     */
    'ips' => ['::1', '85.183.69.51'],

    /**
     * Frontend domain - used for url generation
     */
    'localDomain' => env('APP_ENV') === 'local' ? env('LOCAL_DOMAIN', @$_SERVER['SERVER_NAME']) :
        (@$_SERVER['SERVER_NAME'] ?: env('LOCAL_DOMAIN')),

    /**
     * Development domain for testing
     */
    'liveDomain' => env('APP_ENV') === 'local' ? env('LIVE_DOMAIN', @$_SERVER['SERVER_NAME']) :
        (@$_SERVER['SERVER_NAME'] ?: env('LIVE_DOMAIN')),

    /**
     * Preview mode - no clue for what
     */
    'preview' => isset($_REQUEST['preview']),

];
