<?php

return [

    'Cronjob' => [
        'debug' => filter_var(env('CRONJOB_DEBUG', true), FILTER_VALIDATE_BOOLEAN),
        'mail' => filter_var(env('CRONJOB_MAIL', false), FILTER_VALIDATE_BOOLEAN),
    ]

];
