<?php

namespace App\Error\Middleware;

use App\Library\Facades\Request;
use Cake\Http\Response;
use Psr\Http\Message\ResponseInterface;
use App\Error\ExceptionRenderer;
use App\Error\Traits\WhoopsTrait;
use Cake\Datasource\ModelAwareTrait;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * @version 4.0.0
 */
class SxErrorHandlerMiddleware extends ErrorHandlerMiddleware
{
    use ModelAwareTrait, WhoopsTrait;

    /**
     * @var bool $forceDebug
     */
    protected $forceDebug;

    /**
     * @var \App\Model\Table\RedirectsTable $Redirects
     */
    public $Redirects;

    /**
     * @inheritdoc
     */
    public function __construct($exceptionRenderer = null)
    {
        parent::__construct(config('Error'));

        $this->loadModel('Redirects');
    }

    /**
     * @inheritdoc
     */
    public function handleException(Throwable $exception, ServerRequestInterface $request): ResponseInterface
    {
        $path = request()->fullPath(false, true);

//         dd($exception);

        /**
         * Redirect to login if not allowed
         */
        if ( request()->ext() === 'html' && $exception->getCode() === 401 ) {
            //fixMe
            return new RedirectResponse(config('Menu.loginPage', '/login') . '?redirect=' . request()->path());
        }

        $response = (new Response);

        /**
         * Reroute path if a redirect exists.
         */


        if ( $exception->getCode() === 404 && $redirect = $this->Redirects->getRedirectByPath($path) ) {
            return $response->withLocation(Request::getContextDomain($redirect->target_url))->withStatus(301);
        }


        if ( (config('debug') === false || config('whoops') === false || PHP_SAPI === 'cli') ) {

            /**
             * Log exception.
             */

            if ( $exception->getCode() === 404 ) {
                logger()->error($path, '404');
            } else {
                logger()->error($exception->getMessage());
            }

            try {
                return $this->getErrorHandler()->getRenderer($exception, $request)->render();
            } catch ( \Exception $exception ) {
                logger()->error('ExceptionRenderer - ' . $exception->getMessage());
                die();
            }
        }

        /* @var \Whoops\Run $whoops */
        $whoops = $this->getWhoopsInstance();

        /* @var \Whoops\Handler\PrettyPageHandler */
        $whoops->pushHandler($this->getHtmlHandler());

        /**
         * Handle ajax response
         */
        if ( request()->ext() === 'json' ) {
            $whoops->pushHandler($this->getJsonHandler());
        }

        /**
         * Handle xml response
         */
        if ( request()->ext() === 'xml' ) {
            $whoops->pushHandler($this->getXmlHandler());
        }

        if ( $exception->getCode() >= 400 && $exception->getCode() <= 600 ) {

            // Send code to browser if possible
            $whoops->sendHttpCode($exception->getCode());

            if ( in_array(request()->ext(), ['svg', 'jpg', 'jpeg', 'png', 'gif']) ) {
                $response->withStatus(404);
            }
        }

        $whoops->handleException($exception);

        return $response;
    }

}
