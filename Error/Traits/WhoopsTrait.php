<?php

namespace App\Error\Traits;

/**
 * @version 4.0.0
 */

trait WhoopsTrait
{
    /**
     * @inheritdoc
     */
    protected $_whoops;

    /**
     * @inheritdoc
     */
    public function getWhoopsInstance()
    {
        return $this->_whoops = $this->_whoops ?: new \Whoops\Run();
    }

    /**
     * @inheritdoc
     */
    public function getJsonHandler()
    {
        return new \App\Error\Whoops\JsonResponseHandler();
    }

    /**
     * @inheritdoc
     */
    public function getXmlHandler()
    {
        return new \Whoops\Handler\XmlResponseHandler();
    }

    /**
     * @inheritdoc
     */
    public function getHtmlHandler()
    {
        return new \Whoops\Handler\PrettyPageHandler();
    }

}
