<?php

namespace App\Error;

use Cake\Error\ExceptionRenderer;

/**
 * @version 4.0.0
 */

class SxExceptionRenderer extends ExceptionRenderer
{
    /**
     * @var array $allowedErrors
     */
    protected $allowedErrors = [
        400, 403, 404, 500,
    ];

    /**
     * @inheritdoc
     */
    protected function _template(\Throwable $exception, string $method, int $code): string
    {
        $code = array_intersect([$exception->getCode()], $this->allowedErrors);

        return $this->template = 'error' . (reset($code) ?: 500);
    }

}
