<?php

namespace App\Error\Whoops;

use Whoops\Handler\Handler;
use Whoops\Handler\JsonResponseHandler as WhoopsJsonResponseHandler;
use Whoops\Exception\Formatter;

/**
 * @version 4.0.0
 */

class JsonResponseHandler extends WhoopsJsonResponseHandler
{
    /**
     * @inheritdoc
     */
    public function handle()
    {
        $response = Formatter::formatExceptionAsDataArray(
            $this->getInspector(), $this->addTraceToOutput(true)
        );

        echo json_encode($response, defined('JSON_PARTIAL_OUTPUT_ON_ERROR') ? JSON_PARTIAL_OUTPUT_ON_ERROR : 0);

        return Handler::QUIT;
    }
}
