<?php
declare(strict_types=1);

namespace App\Error;

use Cake\Core\Exception\Exception as CakeException;
use Cake\Error\ErrorHandler;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use App\Error\Traits\WhoopsTrait;

/**
 * @version 4.0.0
 */

class SxErrorHandler extends ErrorHandler
{
    use WhoopsTrait;

    /**
     * @inheritdoc
     */
    protected function _code(CakeException $exception)
    {
        $code = 500;

        $exception = $this->_unwrap($exception);
        $errorCode = $exception->getCode();

        if ($errorCode >= 400 && $errorCode < 600) {
            $code = $errorCode;
        }

        return $code;
    }

    /**
     * @inheritdoc
     */
    protected function _displayError(array $error, bool $debug): void
    {
        if ( $debug === false ) {
            parent::_displayError($error, $debug); return;
        }

        /* @var \Whoops\Run $whoops */
        $whoops = $this->getWhoopsInstance();

        /* @var \Whoops\Handler\PrettyPageHandler */
        $whoops->pushHandler($this->getHtmlHandler());

        /**
         * Handle ajax response
         */
        if ( request() && request()->ext() === 'json' ) {
            $whoops->pushHandler($this->getJsonHandler());
        }

        /**
         * Handle xml response
         */
        if ( request() && request()->ext() === 'xml' ) {
            $whoops->pushHandler($this->getXmlHandler());
        }


        $whoops->handleError($error['level'], $error['description'], $error['file'], $error['line']);
    }

    /**
     * @inheritdoc
     */
    protected function _displayException(\Throwable $exception): void
    {
        if ( config('debug') === false ) {
            parent::_displayException($exception); return;
        }

        /* @var \Whoops\Run $whoops */
        $whoops = $this->getWhoopsInstance();

        /* @var \Whoops\Handler\PrettyPageHandler */
        $handler = $this->getHtmlHandler();

        /**
         * Handle ajax response
         */
        if ( request() && request()->ext() === 'json' ) {
            $whoops->pushHandler($this->getJsonHandler());
        }

        /**
         * Handle xml response
         */
        if ( request() && request()->ext() === 'xml' ) {
            $whoops->pushHandler($this->getXmlHandler());
        }

        // Include all attributes defined in Cake Exception as a data table
        if ( $exception instanceof CakeException ) {
            $handler->addDataTable('Cake Exception', $exception->getAttributes());
        }

        // Include all request parameters as a data table
        $request = Router::getRequest(true);

        if ( $request instanceof ServerRequest) {
            $handler->addDataTable('Cake Request', $request->getQueryParams());
        }

        $whoops->pushHandler($handler)->handleException($exception);
    }

}
