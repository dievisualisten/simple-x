<?php
$anfrageueber = "Über das Formular auf $cleanFullUrl wurde folgende Anfrage gestellt:";

$fields = $data->formularconfig->formular->extended_data['fields'];
$formFields = [];

foreach ($fields as $name => $field) {
    $formFields[$field['inputName']] = $field;
}

if (is_array($to)) {
    $to = \Tools\Utility\Text::toList($to, $and = ',');
}

echo '<table border="0" cellpadding="2" cellspacing="2"><tr><td valign="top" colspan="2">' . $anfrageueber . '</td></tr><tr><td valign="top">Empfänger dieser E-Mail:</td><td valign="top">' . $to . '</td></tr>';

foreach ($formdata as $name => $value) {
    if (strpos($name, 'Display') !== false) {
        $nameey = str_replace('Display', '', $name);
        $formdata[$nameey] = $value;
        unset($formdata[$name]);
    }
}

foreach ($formdata as $name => $value) {

    $fieldtype = 'text';

    if (isset($formFields[$name]['type'])) {
        $fieldtype = $formFields[$name]['type'];
    }

    $fieldlabel = ucfirst($name);

    if (isset($formFields[$name]['label'])) {
        $fieldlabel = $formFields[$name]['label'];
    }

    //test auf Domainfield
    if (!empty($formFields[$name]['options'])) {
        if (!is_array($value)) {
            $value = (array)$value;
        }
        $ret = [];
        foreach ($value as $listValue) {
            $optionsname = \App\Library\DomainManager::get($formFields[$name]['options'], $listValue);
            if (!empty($optionsname)) {
                $ret[] = $optionsname;
            }
        }
        $value = \Tools\Utility\Text::toList($ret, ',');
    }

    if (!is_array($value)) {
        if (strtolower($fieldtype) == 'checkbox' || (strtolower($fieldtype) == 'element' && $name == strtolower('newsletter'))) {

            if ($value == '1') {
                $value = __('Ja');
            } else {
                $value = __('Nein');
            }
        }

        echo '<tr><td valign="top">' . $fieldlabel . '</td><td valign="top">' . $value . '</td></tr>';
    } else {
        // file
        if (array_key_exists('tmp_name', $value)) {
            continue;
        }

        // date, datetime, time
        echo '<tr><td>' . $fieldlabel . '</td><td>';
        if (array_key_exists('day', $value) && array_key_exists('month', $value) && array_key_exists('year',
                $value) && !empty($value['day']) && !empty($value['month']) && !empty($value['year'])) {
            echo $value['day'] . '.' . $value['month'] . '.' . $value['year'] . ' ';
        }
        //valid until
        if (array_key_exists('month', $value) && array_key_exists('year', $value) && !empty($value['month']) && !empty($value['year'])) {
            echo $value['month'] . '/' . $value['year'] . ' ';
        }

        if (array_key_exists('hour', $value) && array_key_exists('min', $value) && !empty($value['hour']) && !empty($value['min'])) {
            echo $value['hour'] . ':' . $value['min'] . 'h';
        }
        echo "</td></tr>";
    }
}
echo "</table>";
?>
