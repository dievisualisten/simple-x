<?php
App::import('Vendor', 'tcpdf/tcpdf');

class Voucherpdf
{
    public $content_abstand_oben = 125;
    public $margin_left = 20;
    public $margin_right = 20;
    public $strichbreite = 210;
    public $footer_abstand_oben = 257;
    public $font_size_footer = 9;
    public $font_size_content = 10;
    public $font_size_headline = 14;
    public $auto_page_break = 55; //Abstand von unten
    public $textcolor = [
        100,
        100,
        100,
    ];

    public $strichcolor = [
        200,
        200,
        200,
    ];
    public $font = 'helvetica';
    public $pdf = null;

    function setBgImg()
    {
        $this->pdf->SetAutoPageBreak(true, 0);
        $this->pdf->SetMargins(0, 0);
        if ($this->vorschau) {
            $this->pdf->Image(VOUCHER_PATH_LAYOUTS . $this->order['Voucher']['voucherlayout'] . '-v.jpg', $x = 0, $y = 0, $w = 210, $h = 297, $type = 'JPEG',
                $link = '', $align = '', $resize = false, $dpi = 150, $palign = '', $ismask = false, $imgmask = false, $border = 0);
        } else {
            $this->pdf->Image(VOUCHER_PATH_LAYOUTS . $this->order['Voucher']['voucherlayout'] . '.jpg', $x = 0, $y = 0, $w = 210, $h = 297, $type = 'JPEG',
                $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0);
        }
        $this->pdf->SetMargins($this->margin_left, $this->content_abstand_oben, $this->margin_right);
        $this->pdf->SetAutoPageBreak(true, $this->auto_page_break);
    }

    function setFooter($page, $pageCount)
    {
        $this->pdf->SetAutoPageBreak(true, 0);
        $this->pdf->SetFont($this->font, '', $this->font_size_footer);
        $this->pdf->MultiCell($w = '', $h = 0, __('Gültig bis ') . date('d.m') . '.' . (date('Y') + 2) . ' - ' . __('Seite') . ' ' . $page . '/' . $pageCount,
            $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = $this->footer_abstand_oben, $reseth = true, $stretch = 0, $ishtml = true,
            $autopadding = true, $maxh = 0);
        if ($this->vorschau) {
            $this->pdf->write1DBarcode(__('Ungültig - Vorschau'), 'C128', '', '', 50, 23, 0.4, $this->style, 'N');
        } else {
            $this->pdf->write1DBarcode($this->order['Voucher']['code'], 'C128', '', '', 50, 23, 0.4, $this->style, 'N');
        }
        $this->pdf->SetAutoPageBreak(true, $this->auto_page_break);
        $this->pdf->SetFont($this->font, '', $this->font_size_content);
    }

    function setItem($item, $y = "")
    {
        //TODO SX4
        SxShop::setCurrentProduct($item);
        $text = str_replace('<li>', '<li><br/>- ', SxShop::getCurrentProductProperty('Product.vouchertext'));
        $text = str_replace('<p>', '<br/>', $text);
        $text = '<span style="font-size:8pt;">' . strip_tags($text, '<br><b><strong><em><i>') . '</span>';
        $this->pdf->MultiCell($w = '', $h = 0, $item['count'] . 'x ' . strip_tags(SxShop::getCurrentProductProperty('Article.title'), '') . '<br/>' . $text,
            $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = $y, $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0);
        $this->pdf->ln(2);
        $this->pdf->Rect($this->pdf->GetX(), $this->pdf->GetY(), $this->strichbreite, '0.1', 'F', [], $this->strichcolor);
        $this->pdf->ln(2);
    }

    function __construct($order = [], $vorschau = false)
    {

        $this->strichbreite = $this->strichbreite - $this->margin_left - $this->margin_right;
        // define barcode style
        $this->style = [
            'position' => '',
            'align' => 'L',
            'stretch' => false,
            'vpadding' => 1,
            'fgcolor' => $this->textcolor,
            'bgcolor' => false,
            'text' => true,
            'font' => $this->font,
            'fontsize' => $this->font_size_footer,
            'stretchtext' => 0,
        ];

        $this->order = $order;
        $this->vorschau = $vorschau;

        //ein Durchlauf zum Seitenanzahl testen, ein durchlauf zum Inhalt setzen
        for ($s = 0; $s <= 1; $s++) {

            if ($s == 0) {
                $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $this->pdf->setPrintFooter(false);
                $this->pdf->setPrintHeader(false);

                $this->pdf->SetFont($this->font, '', $this->font_size_content);
                $this->pdf->SetTextColorArray($this->textcolor);
                $this->pdf->setCellPaddings(0, 0, 0, 0);
                $this->pdf->setCellMargins(0, 0, 0, 0);

                $this->pdf->startTransaction();
                $this->pdf->AddPage();
                $pages = 0;

            } else {
                for ($i = 1; $i <= $pages; $i++) {
                    $this->pdf->AddPage();
                    $this->pdf->setPage($i);
                    $this->setBgImg();
                    $this->setFooter($i, $pages);
                }
                $this->pdf->setPage(1);
            }

            //für den Content
            $this->pdf->SetAutoPageBreak(true, $this->auto_page_break);
            $this->pdf->SetMargins($this->margin_left, $this->content_abstand_oben, $this->margin_right);

            //Widmung
            $this->pdf->MultiCell($w = '', $h = 0, $this->order['Voucher']['dedication'], $border = 0, $align = 'C', $fill = false, $ln = 1, $x = '',
                $y = $this->content_abstand_oben, $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0);
            $this->pdf->ln(5);

            //Überschrift
            $this->pdf->SetFont($this->font, '', $this->font_size_headline);
            $this->pdf->MultiCell($w = '', $h = 0, __('Der Gutschein enthält'), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '',
                $reseth = true, $stretch = 0, $ishtml = true, $autopadding = true, $maxh = 0);
            $this->pdf->SetFont($this->font, '', $this->font_size_content);

            //Linie
            $this->pdf->ln(2);
            $this->pdf->Rect($this->pdf->GetX(), $this->pdf->GetY(), $this->strichbreite, '0.1', 'F', [], $this->strichcolor);
            $this->pdf->ln(2);

            foreach ($this->order['Card']['items'] as $gutschein_id => $item) {
                //Test ob Artikel auch als Gutschein kaufbar ist
                SxShop::setCurrentProduct($item);
                $voucherable = SxShop::getCurrentProductProperty('Product.voucherable');
                if (!$voucherable) {
                    continue;
                }

                $this->setItem($item);

                //Seitenanzahl zählen;
                if ($s == 0) {
                    $pages = $this->pdf->getPage();
                }
            }

            if ($s == 0) {
                $this->pdf = $this->pdf->rollbackTransaction();
            } else {
                $this->pdf->commitTransaction();
            }
        }

        if ($vorschau) {
            $this->pdf->Output(VOUCHER_PATH_PDF_PREVIEWS . $this->order['Voucher']['code'] . '.pdf', 'F');
        } else {
            $this->pdf->Output(VOUCHER_PATH_PDFS . $this->order['Voucher']['code'] . '.pdf', 'F');
        }
    }
}
