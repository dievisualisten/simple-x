<?php

use App\Library\AuthManager;

$debugMessages = [
  'Entwicklermodus aktiv',
  'Happy Coding Hour',
  'Oops something went wrong mode',
  'Boar wat läd das denn so lange',
];

$controllers = [
  'articles',
  'elements',
  'menus',
  'attachments',
  'newsletters',
  'newsletterrecipients',
  'newsletterinterests',
  'formulars',
  'formularconfigs',
  'users',
  'roles',
  'resources',
  'acos',
  'configurations',
  'pageconfigurations',
  'texts',
  'crontasks',
  'redirects',
  'emails',
  'domains',
];

?>
<!DOCTYPE html>
<html lang="<?php echo language()->getLanguage(); ?>">
<head>
    <!-- Charset UTF-8 -->
    <?= $this->Html->charset(); ?>

    <!-- Viewport resizing -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Page title with real name -->
    <title><?php echo __('Loading'); ?></title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/simple-x/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/simple-x/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/simple-x/favicon-16x16.png">
    <link rel="manifest" href="/simple-x/site.webmanifest">
    <link rel="mask-icon" href="/simple-x/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/simple-x/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="simple X">
    <meta name="application-name" content="simple X">
    <meta name="msapplication-TileColor" content="#16253f">
    <meta name="msapplication-config" content="/simple-x/browserconfig.xml">
    <meta name="theme-color" content="#16253f">

    <link href="https://fonts.googleapis.com/css?family=Inter:200,300,400,600,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/simple-x/dist/css/index.css">

</head>

<body>

<div class="loader loader--slide">
    <div class="loader__logo">
        <span></span>
    </div>
    <div class="loader__background">
        <span></span>
    </div>
    <?php if ( config('debug') ) : ?>
        <div class="loader__info">
            <span><?php echo $debugMessages[array_rand($debugMessages)]; ?></span>
        </div>
    <?php endif; ?>
</div>

<div id="app"></div>

<script src="/simple-x/dist/js/index.js"></script>

<script>

    <?php

    echo "sx.Build.setDebug(" . (config('debug') ? 'true' : 'false') . ");";

    $baseLocale = language()->getLanguage();

    echo "sx.Build.setLocale('{$baseLocale}');";

    $basePath = 'system';

    if ( ! language()->isDefaultLanguage() ) {
        $basePath = $baseLocale . '/' . $basePath;
    }

    echo "sx.Build.setBasePath('{$basePath}');";

    $baseTitle = config('Sx.app.cmsname');

    echo "sx.Build.setPagetitle('{$baseTitle}');";

    ?>

    sx.Menu.setMenus([
        {
            title: 'Dashboard',
            path: '/',
            icon: 'sx-icon-dashboard',
            name: 'dashboard.index',
            component: 'SxDashboardIndex',
            rights: ['article'],
        },
        {
            title: 'Seiten',
            path: '/articles',
            icon: 'sx-icon-article',
            name: 'articles.index',
            component: 'SxArticleIndex',
            rights: ['article'],
            children: [
                {
                    hide: true,
                    title: 'Artikel erstellen',
                    path: '/articles/create',
                    name: 'articles.create',
                    component: 'SxArticleIndex',
                    rights: ['article'],
                },
                {
                    hide: true,
                    title: 'Artikel bearbeiten',
                    path: '/articles/edit/:id',
                    name: 'articles.edit',
                    component: 'SxArticleIndex',
                    rights: ['article'],
                }
            ]
        },
        {
            title: 'Elemente',
            path: '/elements',
            icon: 'sx-icon-element',
            name: 'elements.index',
            component: 'SxElementIndex',
            rights: ['article'],
            children: [
                {
                    hide: true,
                    title: 'Element erstellen',
                    path: '/elements/create',
                    name: 'elements.create',
                    component: 'SxElementIndex',
                    rights: ['article'],
                },
                {
                    hide: true,
                    title: 'Element bearbeiten',
                    path: '/elements/edit/:id',
                    name: 'elements.edit',
                    component: 'SxElementIndex',
                    rights: ['article'],
                }
            ]
        },
        {
            title: 'Medien',
            path: '/attachments',
            icon: 'sx-icon-media',
            name: 'attachments.index',
            component: 'SxAttachmentIndex',
            rights: ['attachment'],
            children: [
                {
                    hide: true,
                    title: 'Medien bearbeiten',
                    path: '/attachments/edit/:id',
                    name: 'attachments.edit',
                    component: 'SxAttachmentIndex',
                    rights: ['attachment'],
                }
            ]
        },
        {
            title: 'Mailing',
            path: '/newsletters',
            icon: 'sx-icon-mailing',
            redirect: 'newsletters.index',
            rights: ['newsletter'],
            children: [
                {
                    title: 'Mailing',
                    path: '/newsletters',
                    icon: 'fa fa-envelope',
                    name: 'newsletters.index',
                    component: 'SxNewsletterIndex',
                    rights: ['newsletter'],
                    children: [
                        {
                            hide: true,
                            title: 'Mailing erstellen',
                            path: '/newsletters/create',
                            name: 'newsletters.create',
                            component: 'SxNewsletterIndex',
                            rights: ['newsletter'],
                        },
                        {
                            hide: true,
                            title: 'Mailing bearbeiten',
                            path: '/newsletters/edit/:id',
                            name: 'newsletters.edit',
                            component: 'SxNewsletterIndex',
                            rights: ['newsletter'],
                        }
                    ]
                },
                {
                    title: 'Empfänger',
                    path: '/newsletterrecipients',
                    icon: 'fa fa-id-card',
                    name: 'newsletterrecipients.index',
                    component: 'SxNewsletterrecipientIndex',
                    rights: ['newsletterrecipient'],
                    children: [
                        {
                            hide: true,
                            title: 'Empfänger erstellen',
                            path: '/newsletterrecipients/create',
                            name: 'newsletterrecipients.create',
                            component: 'SxNewsletterrecipientIndex',
                            rights: ['newsletterrecipient'],
                        },
                        {
                            hide: true,
                            title: 'Empfänger bearbeiten',
                            path: '/newsletterrecipients/edit/:id',
                            name: 'newsletterrecipients.edit',
                            component: 'SxNewsletterrecipientIndex',
                            rights: ['newsletterrecipient'],
                        }
                    ]
                },
                {
                    title: 'Interessen',
                    path: '/newsletterinterests',
                    icon: 'fa fa-users-class',
                    name: 'newsletterinterests.index',
                    component: 'SxNewsletterinterestIndex',
                    rights: ['newsletterinterest'],
                    children: [
                        {
                            hide: true,
                            title: 'Interesse erstellen',
                            path: '/newsletterinterests/create',
                            name: 'newsletterinterests.create',
                            component: 'SxNewsletterinterestIndex',
                            rights: ['newsletterinterest'],
                        },
                        {
                            hide: true,
                            title: 'Interesse bearbeiten',
                            path: '/newsletterinterests/edit/:id',
                            name: 'newsletterinterests.edit',
                            component: 'SxNewsletterinterestIndex',
                            rights: ['newsletterinterest'],
                        }
                    ]
                }
            ]
        },
        {
            title: 'Shop',
            path: '/orders',
            icon: 'sx-icon-shop',
            redirect: 'orders.index',
            rights: ['order', 'voucher'],
            children: [
                {
                    title: 'Bestellungen',
                    path: '/orders',
                    icon: 'fa fa-shopping-bag',
                    name: 'orders.index',
                    component: 'SxOrderIndex',
                    rights: ['order'],
                },
                {
                    title: 'Gutscheine',
                    path: '/vouchers',
                    icon: 'fa fa-gift-card',
                    name: 'vouchers.index',
                    component: 'SxVoucherIndex',
                    rights: ['voucher'],
                }
            ]
        },
        {
            title: 'Benutzer',
            path: '/users',
            icon: 'sx-icon-user',
            rights: ['user'],
            redirect: 'users.index',
            children: [
                {
                    title: 'Benutzer',
                    path: '/users',
                    icon: 'fa fa-user',
                    name: 'users.index',
                    component: 'SxUserIndex',
                    rights: ['user'],
                    children: [
                        {
                            hide: true,
                            title: 'Benutzer erstellen',
                            path: '/users/create',
                            name: 'users.create',
                            component: 'SxUserIndex',
                            rights: ['user'],
                        },
                        {
                            hide: true,
                            title: 'Benutzer bearbeiten',
                            path: '/users/edit/:id',
                            name: 'users.edit',
                            component: 'SxUserIndex',
                            rights: ['user'],
                        },
                    ],
                },
                {
                    title: 'Resourcen',
                    path: '/resources',
                    icon: 'fa fa-cube',
                    name: 'resources.index',
                    component: 'SxResourceIndex',
                    rights: ['resource'],
                    children: [
                        {
                            hide: true,
                            title: 'Resource erstellen',
                            path: '/resources/create',
                            name: 'resources.create',
                            component: 'SxResourceIndex',
                            rights: ['resource'],
                        },
                        {
                            hide: true,
                            title: 'Resource bearbeiten',
                            path: '/resources/edit/:id',
                            name: 'resources.edit',
                            component: 'SxResourceIndex',
                            rights: ['resource'],
                        },
                    ],
                },
                {
                    title: 'Rollen',
                    path: '/roles',
                    icon: 'fa fa-user-tag',
                    name: 'roles.index',
                    component: 'SxRoleIndex',
                    rights: ['role'],
                    children: [
                        {
                            hide: true,
                            title: 'Rolle erstellen',
                            path: '/roles/create',
                            name: 'roles.create',
                            component: 'SxRoleIndex',
                            rights: ['role'],
                        },
                        {
                            hide: true,
                            title: 'Rolle bearbeiten',
                            path: '/roles/edit/:id',
                            name: 'roles.edit',
                            component: 'SxRoleIndex',
                            rights: ['role'],
                        },
                    ],
                },
                {
                    title: 'Access Control Object',
                    path: '/acos',
                    icon: 'fa fa-key',
                    name: 'acos.index',
                    component: 'SxAcoIndex',
                    rights: ['aco'],
                    children: [
                        {
                            hide: true,
                            title: 'Access Control Object erstellen',
                            path: '/acos/create',
                            name: 'acos.create',
                            component: 'SxAcoIndex',
                            rights: ['aco'],
                        },
                        {
                            hide: true,
                            title: 'Access Control Object bearbeiten',
                            path: '/acos/edit/:id',
                            name: 'acos.edit',
                            component: 'SxAcoIndex',
                            rights: ['aco'],
                        },
                    ],
                },
            ]
        },
        {
            title: 'Formular',
            path: '/formulars',
            icon: 'sx-icon-formular',
            redirect: 'formulars.index',
            rights: ['formular', 'formularconfig'],
            children: [
                {
                    title: 'Formulare',
                    path: '/formulars',
                    icon: 'fa fa-stream',
                    name: 'formulars.index',
                    component: 'SxFormularIndex',
                    rights: ['formular'],
                    children: [
                        {
                            hide: true,
                            title: 'Formular erstellen',
                            path: '/formulars/create',
                            name: 'formulars.create',
                            component: 'SxFormularIndex',
                            rights: ['formular'],
                        },
                        {
                            hide: true,
                            title: 'Formular bearbeiten',
                            path: '/formulars/edit/:id',
                            name: 'formulars.edit',
                            component: 'SxFormularIndex',
                            rights: ['formular'],
                        },
                    ],
                },
                {
                    title: 'Konfiguration',
                    path: '/formulars/formularconfigs',
                    icon: 'fa fa-cog',
                    name: 'formularconfigs.index',
                    component: 'SxFormularconfigIndex',
                    rights: ['formularconfig'],
                    children: [
                        {
                            hide: true,
                            title: 'Formularkonfiguration erstellen',
                            path: '/formulars/formularconfigs/create',
                            name: 'formularconfigs.create',
                            component: 'SxFormularconfigIndex',
                            rights: ['formularconfig'],
                        },
                        {
                            hide: true,
                            title: 'Formularkonfiguration bearbeiten',
                            path: '/formulars/formularconfigs/edit/:id',
                            name: 'formularconfigs.edit',
                            component: 'SxFormularconfigIndex',
                            rights: ['formularconfig'],
                        },
                    ],
                },
            ]
        },
        {
            title: 'System',
            path: '/configurations',
            icon: 'sx-icon-system',
            redirect: 'configurations.index',
            rights: ['configuration', 'pageconfiguration', 'domain', 'text', 'crontask', 'actionview', 'redirect'],
            children: [
                {
                    title: 'Konfiguration',
                    path: '/configurations',
                    icon: 'fa fa-cog',
                    name: 'configurations.index',
                    component: 'SxConfigurationIndex',
                    rights: ['configuration'],
                    children: [
                        {
                            hide: true,
                            title: 'Konfiguration erstellen',
                            path: '/configurations/create',
                            name: 'configurations.create',
                            component: 'SxConfigurationIndex',
                            rights: ['configuration'],
                        },
                        {
                            hide: true,
                            title: 'Konfiguration bearbeiten',
                            path: '/configurations/:id',
                            name: 'configurations.edit',
                            component: 'SxConfigurationIndex',
                            rights: ['configuration'],
                        },
                    ],
                },
                {
                    title: 'Seitenkonfiguration',
                    path: '/pageconfigurations',
                    icon: 'fa fa-cogs',
                    name: 'pageconfigurations.index',
                    component: 'SxPageconfigurationIndex',
                    rights: ['pageconfiguration'],
                    children: [
                        {
                            hide: true,
                            title: 'Seitenkonfiguration erstellen',
                            path: '/pageconfigurations/create',
                            name: 'pageconfigurations.create',
                            component: 'SxPageconfigurationIndex',
                            rights: ['pageconfiguration'],
                        },
                        {
                            hide: true,
                            title: 'Seitenkonfiguration bearbeiten',
                            path: '/configurations/:id',
                            name: 'pageconfigurations.edit',
                            component: 'SxPageconfigurationIndex',
                            rights: ['pageconfiguration'],
                        },
                    ],
                },
                {
                    title: 'Listen',
                    path: '/domains',
                    icon: 'fa fa-list',
                    name: 'domains.index',
                    component: 'SxDomainIndex',
                    rights: ['domain'],
                    children: [
                        {
                            hide: true,
                            title: 'Liste erstellen',
                            path: '/domains/create',
                            name: 'domains.create',
                            component: 'SxDomainIndex',
                            rights: ['domain'],
                        },
                        {
                            hide: true,
                            title: 'Liste bearbeiten',
                            path: '/domains/:id',
                            name: 'domains.edit',
                            component: 'SxDomainIndex',
                            rights: ['domain'],
                        },
                    ],
                },
                {
                    title: 'Texte',
                    path: '/texts',
                    icon: 'fa fa-language',
                    name: 'texts.index',
                    component: 'SxTextIndex',
                    rights: ['text'],
                    children: [
                        {
                            hide: true,
                            title: 'Text erstellen',
                            path: '/texts/create',
                            name: 'texts.create',
                            component: 'SxTextIndex',
                            rights: ['text'],
                        },
                        {
                            hide: true,
                            title: 'Text bearbeiten',
                            path: '/texts/:id',
                            name: 'texts.edit',
                            component: 'SxTextIndex',
                            rights: ['text'],
                        },
                    ],
                },
                {
                    title: 'Hintergrundaufgaben',
                    path: '/crontasks',
                    icon: 'fa fa-stopwatch',
                    name: 'crontasks.index',
                    component: 'SxCrontaskIndex',
                    rights: ['crontask'],
                    children: [
                        {
                            hide: true,
                            title: 'Hintergrundaufgabe erstellen',
                            path: '/crontasks/create',
                            name: 'crontasks.create',
                            component: 'SxCrontaskIndex',
                            rights: ['crontask'],
                        },
                        {
                            hide: true,
                            title: 'Hintergrundaufgabe bearbeiten',
                            path: '/crontasks/:id',
                            name: 'crontasks.edit',
                            component: 'SxCrontaskIndex',
                            rights: ['crontask'],
                        },
                    ],
                },
                {
                    title: 'Seitenzuordnung',
                    path: '/actionviews',
                    icon: 'fa fa-route',
                    name: 'actionviews.index',
                    component: 'SxActionViewIndex',
                    rights: ['actionview'],
                },
                {
                    title: 'Weiterleitung',
                    path: '/redirects',
                    icon: 'fa fa-external-link',
                    name: 'redirects.index',
                    component: 'SxRedirectIndex',
                    rights: ['redirect'],
                    children: [
                        {
                            hide: true,
                            title: 'Weiterleitung erstellen',
                            path: '/redirects/create',
                            name: 'redirects.create',
                            component: 'SxRedirectIndex',
                            rights: ['redirect'],
                        },
                        {
                            hide: true,
                            title: 'Weiterleitung bearbeiten',
                            path: '/redirects/:id',
                            name: 'redirects.edit',
                            component: 'SxRedirectIndex',
                            rights: ['redirect'],
                        },
                    ],
                },
                {
                    title: 'E-Mail Konfiguaration',
                    path: '/emails',
                    icon: 'fa fa-envelope',
                    name: 'emails.index',
                    component: 'SxEmailIndex',
                    rights: ['email'],
                    children: [
                        {
                            hide: true,
                            title: 'E-Mail Konfiguaration erstellen',
                            path: '/emails/create',
                            name: 'emails.create',
                            component: 'SxEmailIndex',
                            rights: ['email'],
                        },
                        {
                            hide: true,
                            title: 'E-Mail Konfiguaration bearbeiten',
                            path: '/emails/:id',
                            name: 'emails.edit',
                            component: 'SxEmailIndex',
                            rights: ['email'],
                        },
                    ],
                },
            ]
        },
        // {
        //     hide: true,
        //     title: 'Code',
        //     path: '/code',
        //     icon: 'fa fa-code',
        //     name: 'style.editor',
        //     component: 'SxCodeEditor',
        //     rights: ['error'],
        // },
        {
            title: 'Abmelden',
            path: '/logout',
            icon: 'sx-icon-logout',
            component: 'SxError',
            rights: ['error'],
            beforeEnter: function() {
                window.location.replace('/logout');
                return false;
            }
        },
        {
            hide: true,
            title: 'Error',
            path: '/error',
            icon: 'fa fa-times',
            name: 'app.error',
            component: 'SxError',
            rights: ['error'],
        },
        <?php if ( AuthManager::get('id') ) : ?>
        {
            hide: true,
            title: 'Error',
            path: '/:error(.*)*',
            icon: 'fa fa-times',
            redirect: 'app.error',
            rights: ['error'],
        },
        <?php endif; ?>
    ]);

    pi.Data.data = {
        <?php foreach (@$export ?: [] as $key => $data) : ?>
        "<?php echo $key; ?>": {
            data: <?php echo json_encode($data); ?>
        },
        <?php endforeach; ?>
        // nav: navigation
    };

    <?php

    $localeNanoFile = WWW_ROOT . 'simple-x/locales/' .
      language()->getLanguage() . '/nano.php';

    $localeNano = [];

    if ( file_exists($localeNanoFile) ) {
        $localeNano = require $localeNanoFile;
    }

    $localeSimplexFile = WWW_ROOT . 'simple-x/locales/' .
      language()->getLanguage() . '/simplex.php';

    $localeSimplex = [];

    if ( file_exists($localeSimplexFile) ) {
        $localeSimplex = require $localeSimplexFile;
    }

    $messages = array_merge($localeNano, $localeSimplex);

    ?>

    pi.Locale.locales = <?php echo json_encode($messages); ?>;

    <?php foreach ($controllers as $controller) : ?>
    sx.Ajax.mergeRestRoutes('<?php echo $controller; ?>');
    <?php endforeach; ?>

    sx.Ajax.mergeRoutes({
        'system.extras': '/system/fetch/extras.json',
        'system.elements': '/system/fetch/elements.json',
        'system.auth': '/system/fetch/auth.json',
        'system.config': '/system/fetch/config.json',
        'system.article': '/system/fetch/article.json',
        'system.locales': '/system/fetch/locales.json',
        'system.stats': '/system/fetch/stats.json',
        'system.domains': '/system/fetch/domains.json',
        'system.acos': '/system/fetch/acos.json',
        'system.roles': '/system/fetch/roles.json',
        'system.users': '/system/fetch/users.json',
        'system.resources': '/system/fetch/resources.json',
        'system.emails': '/system/fetch/emails.json',
        'system.formulars': '/system/fetch/formulars.json',
        'system.formularconfigs': '/system/fetch/formularconfigs.json',
        'system.newsletterinterests': '/system/fetch/newsletterinterests.json',
        'system.debug': '/system/fetch/debug.json',
        'users.login': '/users/login.json',
        'users.logout': '/users/logout.json',
        'elements.add': '/elements/add.json',
        'acos.tree': '/acos/tree.json',
        'acos.move': '/acos/move.json',
        'menus.tree': '/menus/tree.json',
        'menus.move': '/menus/move.json',
        'menus.insert': '/menus/insert.json',
        'attachments.tree': '/attachments/tree.json',
        'attachments.move': '/attachments/move.json',
        'attachments.upload': '/attachments/upload.json',
        'attachments.whitespace': '/attachments/whitespace.json',
        'attachments.crop': '/attachments/crop.json',
        'attachments.focus': '/attachments/focus.json',
        'attachments.replace': '/attachments/replace.json',
        'attachments.video': '/attachments/video.json',
        'texts.generate': '/texts/generate.json',
        'configurations.tree': '/configurations/tree.json',
        'pageconfigurations.tree': '/pageconfigurations/tree.json',
        'pageconfigurations.pagetype': '/pageconfigurations/pagetype.json',
        'crontasks.run': '/crontasks/run.json',
    });

</script>

<?php echo $this->fetch('content') ?>

<?php include_once(WWW_ROOT . 'src/simple-x/froala/default.php'); ?>
<?php include_once(WWW_ROOT . 'src/simple-x/config/article.php'); ?>
<?php include_once(WWW_ROOT . 'src/simple-x/config/custom.php'); ?>
<?php include_once(WWW_ROOT . 'src/simple-x/config/element.php'); ?>

<script src="https://cdn.jsdelivr.net/npm/resize-observer-polyfill@1.5.1/dist/ResizeObserver.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvJ7NNFrrSL13b3xEB07Itw0luJSHDTmE" async></script>

</body>
</html>
