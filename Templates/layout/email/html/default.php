<?= $this->Email->doctype(); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php
    echo $this->Email->charset();
    echo $this->Email->viewport();
    ?>
    <title>E-Mail</title>
    <style type="text/css">
        <? include (WWW_ROOT.'css'.DS.'email'.DS.'email.css')?>
    </style>
</head>
<body>
<table width="580" border="0" cellspacing="0" cellpadding="0" class="main">
    <tr>
        <td>
            <img src="<?= $base_href ?>/img/email/default/titleimg.jpg" border="0">
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#ffffff" valign="top">
            <?= $this->Text->autoLink($this->fetch('content'), ['escape' => false]) ?>
        </td>
    </tr>
</table>
</body>
</html>