<!DOCTYPE html>
<html lang="<?= language()->getLanguage() ?>">
<head>
    <?php echo $this->element('html/head'); ?>
</head>
<body class="layout-error">

<?= $this->Flash->render() ?>
<?= $this->fetch('content'); ?>

</body>
</html>
