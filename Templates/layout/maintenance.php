<!DOCTYPE html>
<html lang="<?= language()->getLanguage() ?>">
<head>
    <?=$this->Html->charset(); ?>
    <title><?php echo config('Sx.app.cmsname'); ?></title>
</head>
<body class="layout-maintenance">
    <div id="container">
        <div id="header">
            <h1><?= __('Maintenance') ?></h1>
        </div>
        <div id="content">
            <?= $this->Flash->render() ?>

            <?= $this->fetch('content') ?>
        </div>
        <div id="footer">
            <?= $this->Html->link(__('Back'), 'javascript:history.back()') ?>
        </div>
    </div>
</body>
</html>
