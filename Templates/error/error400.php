<div class="error error--400">
    <h1><?= h($message) ?></h1>
    <p class="error-msg error-msg--400">
        <strong><?= __('Fehler') ?>: </strong>
        <?= __('Sie haben kein Zugriff auf {0}.', "<strong>'{$url}'</strong>") ?>
    </p>
</div>
