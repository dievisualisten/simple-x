<div class="error error--404">
    <h1><?= h($message) ?></h1>
    <p class="error-msg error-msg--404">
        <strong><?= __('Fehler') ?>: </strong>
        <?= __('Die angeforderte Adresse {0} wurde nicht gefunden.', "<strong>'{$url}'</strong>") ?>
    </p>
</div>
