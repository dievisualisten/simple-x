<div class="error error--500">
    <h1><?= __('Es ist ein interner Fehler aufgetreten.') ?></h1>
    <p class="error-msg error-msg--500">
        <?= h($message) ?>
    </p>
</div>
