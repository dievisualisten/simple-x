<div class="error error--403">
    <h1><?= h($message) ?></h1>
    <p class="error-msg error-msg--403">
        <strong><?= __('Fehler') ?>: </strong>
        <?= __('Sie haben kein Zugriff auf {0}.', "<strong>'{$url}'</strong>") ?>
    </p>
</div>
