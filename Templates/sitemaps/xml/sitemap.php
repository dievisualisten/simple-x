<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:image="https://www.google.com/schemas/sitemap-image/1.1/sitemap-image.xsd">
    <? foreach ( $data as $item ) : ?>
        <url>
            <loc><?= request()->scheme() . request()->getContextDomain($item->get('path')) ?></loc>
            <lastmod><?= trim($this->Time->toAtom($item->get('modified'))) ?></lastmod>
            <? foreach ( $this->Media->getAttachments($item, ['pick' => ['all']]) as $attachment ) : ?>
                <image:image>
                    <image:loc><?= $this->Media->getVersionUrl($attachment, 'xl') ?></image:loc>
                    <image:title><![CDATA[<?= $attachment->get('name') ?>]]></image:title>
                    <? if ( ! empty($attachment->get('description')) ) : ?>
                        <image:caption><![CDATA[<?= $attachment->get('description') ?>]]></image:caption>
                    <? endif; ?>
                </image:image>
            <? endforeach; ?>
        </url>
    <? endforeach; ?>
</urlset>
