<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
    <? foreach ( $counter as $language => $cnt ) :
        for ( $i = 1; $i <= $cnt; $i++ ) : ?>
            <sitemap>
                <loc><?= request()->scheme() . request()->domain(true) . '/' . $language . '/sitemaps/sitemap/' . $i . '.xml' ?></loc>
            </sitemap>
        <? endfor;
    endforeach; ?>
</sitemapindex>
