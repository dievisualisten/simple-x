<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

?>

<?= $this->element('media/keyvisual', [
    //
]); ?>

<?php if ( $data->get('type', 'page') !== 'frontpage' ) : ?>
    <?= $this->element('system/breadcrumb'); ?>
<?php endif; ?>

<main>
    <div class="container">
        <article class="article">
            <div class="article__body">

                <?= $this->element('system/message', [
                    //
                ]); ?>

                <?= $this->element('article/headline', [
                    'tag' => 'h1', 'property' => 'headline',
                ]); ?>

                <?= $this->element('article/iconfont', [
                    'property' => 'custom.custom.icon',
                ]); ?>

                <?= $this->element('article/price', [
                    //
                ]); ?>

                <?= $this->element('article/eventdate', [
                    //
                ]); ?>

                <?= $this->element('article/content', [
                    'property' => 'content',
                ]); ?>

                <?= $this->element('media/downloads', [
                    //
                ]); ?>

                <?= $this->element('article/eventdates', [
                    //
                ]); ?>

                <?= $this->element('article/content', [
                    'property' => 'content2',
                ]); ?>

                <?= $this->element('items-galerie', [
                    //
                ]); ?>

            </div>
        </article>
    </div>
</main>

<?= $this->element('system/element'); ?>
<?= $this->element('formular'); ?>
<?= $this->element('items/cta'); ?>
<?= $this->element('items/faq'); ?>
<?= $this->element('items/event'); ?>
<?//= $this->element('blog/list'); ?>
<?= $this->element('items/list'); ?>

<?= $this->element('items/teaser'); ?>
<?= $this->element('items/slider'); ?>
<?php if ( ! empty($searchresult) ) : ?>
    <?= $this->element('items/list', ['items' => $searchresult]); ?>
<?php endif; ?>
<?= $this->element('article/multipage'); ?>
<?= $this->element('maps/directions'); ?>
<?= $this->element('maps/list'); ?>
