<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

?>

<?= $this->element('media/keyvisual', [
    //
]); ?>

<?php if ( $data->get('type', 'page') !== 'frontpage' ) : ?>
    <?= $this->element('system/breadcrumb'); ?>
<?php endif; ?>

<main>
    <div class="container">
        <article class="article">
            <div class="article__body">

                <?= $this->element('system/message', [
                    //
                ]); ?>

                <?= $this->element('article/headline', [
                    'tag' => 'h1', 'property' => 'headline',
                ]); ?>

                <?= $this->element('article/price', [
                    //
                ]); ?>

                <?= $this->element('article/eventdate', [
                    //
                ]); ?>

                <?= $this->element('article/content', [
                    'property' => 'content',
                ]); ?>

                <?= $this->element('media/downloads', [
                    //
                ]); ?>

                <?= $this->element('article/eventdates', [
                    //
                ]); ?>

                <?= $this->element('article/content', [
                    'property' => 'content2',
                ]); ?>


            </div>
        </article>
    </div>
</main>
