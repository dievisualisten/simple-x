<?php //pr($order); exit;
App::import('Vendor', 'tcpdf/tcpdf');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintFooter(false);
$pdf->setPrintHeader(false);


$pdf->AddPage();
$pdf->SetMargins(0, 0);

$pdf->Image(INVOICE_PATH_LAYOUTS . 'rechnung-header.jpg', $x = 0, $y = 0, $w = 210, $h = 45, $type = 'JPEG', $link = '', $align = '', $resize = false,
    $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0);

$pdf->SetY(0);
$pdf->SetMargins(15, 15);
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(100, 0, __('invoice.biller'), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '45', $reseth = true);

$pdf->SetFont('helvetica', '', 10);

//Anschrift
$content = '';


if (!empty($order['Formdata']['company'])) {
    $content .= $order['Formdata']['company'] . "<br/>";
}
if (!empty($order['Formdata']['salutation'])) {
    $content .= DomainManager::get('salutation', $order['Formdata']['salutation']) . " ";
}
if (!empty($order['Formdata']['firstname'])) {
    $content .= $order['Formdata']['firstname'] . ' ';
}
if (!empty($order['Formdata']['lastname'])) {
    $content .= $order['Formdata']['lastname'] . "<br/>";
}
if (!empty($order['Formdata']['street'])) {
    $content .= $order['Formdata']['street'] . ' ';
}
if (!empty($order['Formdata']['number'])) {
    $content .= $order['Formdata']['number'] . "<br/>";
}
if (!empty($order['Formdata']['zip'])) {
    $content .= $order['Formdata']['zip'] . ' ';
}
if (!empty($order['Formdata']['city'])) {
    $content .= $order['Formdata']['city'];
}
if (!empty($order['Formdata']['country'])) {
    $content .= "<br/>" . DomainManager::get('country', $order['Formdata']['country']);
}

$pdf->MultiCell(100, 0, $content, $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '50', $reseth = true, $stretch = 0, $ishtml = true);


//Rechts
$pdf->MultiCell(50, 0, __('Rechnungs-Nr.') . ': ' . $order['Order']['rnr'], $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '140', $y = '50',
    $reseth = true);
$pdf->MultiCell(50, 0, __('Datum') . ': ' . date('d.m.Y', strtotime($order['Order']['created'])), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '140',
    $y = '', $reseth = true);
$pdf->MultiCell(50, 0, __('Seite') . ': ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), $border = 0, $align = 'L', $fill = false, $ln = 1,
    $x = '140', $y = '', $reseth = true);


//Überschrift
$pdf->SetY(95);
$pdf->SetFont('helvetica', 'B', 14);
$pdf->MultiCell(100, 0, __('Rechnung'), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '');
$pdf->SetFont('helvetica', '', 10);

$tbl = '<table cellspacing="0" cellpadding="3" border="0" style="width:100%;">';

$tbl = $tbl . '<tr>';

$tbl = $tbl . ' <td style="width:15mm; border-bottom:0.1mm solid ##333333;"  align="right"><strong><br/>' . __('Menge') . '</strong></td>';
$tbl = $tbl . ' <td style="width:90mm; border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;"> </td>';
$tbl = $tbl . ' <td style="width:30mm; border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right"><strong>' . __('Einzelpreis') . '<br/>' . __('(netto)') . '</strong></td>';
$tbl = $tbl . ' <td style="width:15mm; border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right"><strong><br/>' . __('MwSt.') . '</strong></td>';
$tbl = $tbl . ' <td style="width:30mm; border-bottom:0.1mm solid ##333333;" align="right"><strong><br/>' . __('Gesamtpreis') . ' </strong></td>';
$tbl = $tbl . '</tr>';

// Items
foreach ($order['Card']['items'] as $gutschein_id => $item) {

    //TODO SX4
    SxShop::setCurrentProduct($item);
    $tbl = $tbl . '<tr>';
    $tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;"  align="right">' . $item['count'] . '</td>';
    // //TODO SX4
    $tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;">' . nl2br(SxShop::getCurrentProductProperty('Article.title')) . '</td>';
    $tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right" >' . $item['price_netto_formated'] . '</td>';
    $tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right" >' . $item['taxrate'] . '%</td>';
    $tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;" align="right" >' . $item['amount_formated'] . '</td>';
    $tbl = $tbl . '</tr>';
}


$tbl = $tbl . '<tr>';
$tbl = $tbl . ' <td colspan="2" style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;">' . __('Versandkosten') . ' - ' . $order['Card']['shipping_label'] . ' </td>';
$tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right" >' . $order['Card']['shipping_costs_netto_formated'] . '</td>';
$tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;border-right:0.1mm solid ##333333;" align="right" >' . $order['Card']['shipping_costs_taxrate'] . '%</td>';
$tbl = $tbl . ' <td style="border-bottom:0.1mm solid ##333333;" align="right" >' . $order['Card']['shipping_costs_formated'] . '</td>';
$tbl = $tbl . '</tr>';


//Zwischensummen Netto
foreach ($order['Card']['taxrates'] as $rate => $ok) {
    $tbl = $tbl . '<tr>';
    $tbl = $tbl . ' <td style="border-right:0.1mm solid ##333333;" colspan ="3" align="right">' . __('Zwischensumme (netto)') . '</td>';
    $tbl = $tbl . ' <td style="border-right:0.1mm solid ##333333;" align="right">' . $rate . '%</td>';
    $tbl = $tbl . ' <td align="right" >' . $order['Card']['total_amount_netto_formated'][$rate] . '</td>';
    $tbl = $tbl . '</tr>';
}

foreach ($order['Card']['taxrates'] as $rate => $ok) {
    $tbl = $tbl . '<tr>';
    $tbl = $tbl . ' <td style="border-right:0.1mm solid ##333333;" colspan ="3" align="right">' . __('Zzgl. MwSt.') . '</td>';
    $tbl = $tbl . ' <td style="border-right:0.1mm solid ##333333;"  align="right">' . $rate . '%</td>';
    $tbl = $tbl . ' <td align="right" >' . $order['Card']['total_amount_vat_formated'][$rate] . '</td>';
    $tbl = $tbl . '</tr>';
}

$tbl = $tbl . '<tr>';
$tbl = $tbl . ' <td colspan ="2"></td>';
$tbl = $tbl . ' <td colspan ="2"  style="border-right:0.1mm solid ##333333; border-top:0.3mm solid ##333333;"   align="right"><strong>' . __('Gesamtbetrag') . '</strong></td>';
$tbl = $tbl . ' <td style="border-top:0.3mm solid ##333333;" align="right"><strong>' . $order['Card']['total_amount_with_shipping_costs_formated'] . '</strong></td>';
$tbl = $tbl . '</tr>';

$tbl = $tbl . '</table>';


//Footer

$pdf->writeHTML($tbl, true, false, false, false, '');
$pdf->ln(5);

//Wenn bezahlt
if ($order['Order']['is_paid']) {
    $pdf->MultiCell(0, 0, \Tools\Utility\Text::insert(__('invoice.paid'),
        ['paymentmethod' => DomainManager::get('paymentmethod', $order['Order']['paymentmethod'])]), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '',
        $y = '', $reseth = '', $stretch = '', $ishtml = true);

} else {
    if ($order['Order']['paymentmethod'] == 'rechnung' || $order['Order']['paymentmethod'] == 'vorkasse') {
        //Zahlungsbedingung
        $pdf->MultiCell(0, 0, \Tools\Utility\Text::insert(__('invoice.paymentinfo'),
            ['rnr' => $order['Order']['rnr'], 'total_amount_with_shipping_costs_formated' => $order['Card']['total_amount_with_shipping_costs_formated']]),
            $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = '', $stretch = '', $ishtml = true);
    }
}

$pdf->ln(5);
//Danke
$pdf->MultiCell(0, 0, __('invoice.thanks'), $border = 0, $align = 'L', $fill = false, $ln = 1, $x = '', $y = '');


$pdf->SetMargins(0, 0);
$pdf->SetY(0);
$pdf->SetAutoPageBreak(true, 0);
$pdf->Image(INVOICE_PATH_LAYOUTS . 'rechnung-footer.jpg', $x = 0, $y = 262, $w = '210', $h = '35', $type = 'JPEG', $link = '', $align = '', $resize = true,
    $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0);

$pdf->Output(INVOICE_PATH_PDFS . $order['Order']['rnr'] . '-invoice.pdf', 'F');
?>
