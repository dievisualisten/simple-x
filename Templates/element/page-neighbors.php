<?

//Fixme Config für Elternelement
if (empty($options)) {
    $options = [
        'back_text' => 'Vorheriger Artikel',
        'next_text' => 'Nächster Artikel',
        'top_text' => 'zurück',
    ];
}

$defaults = [
    'show_text' => true,
    'show_prev' => true,
    'show_top' => true,
    'img' => 'false',
    'folder' => 's',
    'show' => 'all',
];


extract($defaults, EXTR_SKIP);
?>
<? if (!empty($neighbors['prev']) || !empty($neighbors['top']) || !empty($neighbors['next'])) { ?>
    <div class="neighbors">
        <?= $this->element('page-neighbors-prev', $options); ?>
        <?= $this->element('page-neighbors-next', $options); ?>
        <?= $this->element('page-neighbors-top', $options); ?>
    </div>
<? } ?>
