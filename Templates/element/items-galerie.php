<?
$defaults = [
    'format' => '1_1 1_2@sm 4_3@md',
    'pick' => ['galerie'],
    'groupId' => Text::uuid(),
];

extract($defaults, EXTR_SKIP);

$attachments = $this->Media->getAttachments($data, ['pick' => $pick]);

//dd($attachments);

if (!empty($attachments)) { ?>
    <div class="items items-galerie grid grid--row grid--wrap grid--10-10" sx-light-gallery="counter: false; download: false; zoom: false; share: false; thumbnail: false;">
        <?php foreach ($attachments as $attachment) : ?>
            <?php $attachment = $attachment->get('attachment'); ?>
        <?php if ( ! $attachment->isEmpty('thumb') ) : ?>
            <div class="col--1-1 col--1-2@sm col--1-3@md col--1-4@lg col--1-5@xl">
                <div class="item-galerie">
                    <?
                    // Video?
                    if (!empty($attachment->provider)) {

                        if ($attachment->provider == 'youtube') {
                            $videourl = 'https://www.youtube-nocookie.com/watch?v=' . $attachment->identifier;
                        } else {
                            $videourl = 'https://vimeo.com/' . $attachment->identifier ;
                        }
                        ?>
                        <a class="gallery__item" href="<?= $videourl ?>" data-sub-html="<?= $attachment->description ?>">
                            <?= $this->element('media/default', [
                                'data' => $attachment,
                                'pick' => $pick,
                                'format' => $format,
                                'link' => false,
                                'view' => 'image'
                            ]); ?>
                        </a>

                    <? } else { ?>

                        <a class="gallery__item" href="<?= $this->Media->getVersionUrl($attachment, 'xl') ?>"
                           data-sub-html="<?= $attachment->description ?>">
                            <?= $this->element('media/default', [
                                'data' => $attachment,
                                'pick' => $pick,
                                'format' => $format,
                                'link' => false,
                                'view' => 'image'
                            ]); ?>
                        </a>

                    <? } ?>
                </div>
            </div>
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <?php
}
?>
