<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className' => 'teaser',
    'items'     => $this->Article->matrix(16, $data->children),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */

?>

<?php if ( ! realempty($items) && count($items) ) : ?>
    <div class="<?= $className ?> <?= $className ?>--cta">
        <?php foreach ( $items as $item ) : ?>

            <?= $this->Article->openLink($item, [
                'class' => "{$className}__item grid grid--col",
            ]); ?>

            <?= $this->element('article/readmore', [
                'className' => $className,
                'data'      => $item,
                'linkText'  => false,
                'moreText'  => $this->Fe->getArticleEntity($item)->get('title' ?: __('mehr erfahren')),
            ]); ?>

            <?= $this->Article->closeLink($item); ?>

        <?php endforeach; ?>
    </div>
<?php endif; ?>

