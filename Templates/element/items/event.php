<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

use Cake\I18n\Time;

$defaults = [
    'className'   => 'teaser',
    'items'       => $data->get('events'),
];

$monthNames = [
    1  => __('Januar'),
    2  => __('Februar'),
    3  => __('März'),
    4  => __('April'),
    5  => __('Mai'),
    6  => __('Juni'),
    7  => __('Juli'),
    8  => __('August'),
    9  => __('September'),
    10 => __('Oktober'),
    11 => __('November'),
    12 => __('Dezember'),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */


?>

<?php if ( $data->get('type') === 'calendar' ) : ?>
    <section class="<?= $className ?> <?= $className ?>--event">
        <div class="container">

            <?= $this->element('items/event-daterange', compact('items', 'className')); ?>

            <?php foreach ( $this->Article->paginateMonthRange($items, 'extended_data_cfg.event.start_date') as $group => $list ) : ?>
                <div class="<?= $className ?>__group">
                    <div class="grid grid--row grid--wrap grid--center grid--30-30">

                        <div class="<?= $className ?>__group-title col--1-1">
                            <h2><?= $monthNames[Time::parse($group)->format('n')]; ?> <?= Time::parse($group)->format('Y'); ?></h2>
                        </div>

                        <?php foreach ( $list as $item ) : ?>
                            <div class="animate col--1-1 col--1-3@md col--1-2@sm">
                                <?= $this->element('items/teaser-item-small', compact('item', 'className')); ?>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </section>
<?php endif; ?>