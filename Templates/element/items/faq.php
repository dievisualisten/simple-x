<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className'   => 'teaser',
    'items'       => $this->Article->matrix(4096, $data->children),
    'openFirst'   => $data->get('custom.custom.openFirstFaqItem', false)
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 * @var bool $openFirst
 */

?>

<?php if ( ! realempty($items) && count($items) ) : ?>
    <section class="<?= $className ?> <?= $className ?>--faq">
        <div class="container">
            <div class="grid grid--row grid--wrap grid--center grid--10-10" uk-accordion="toggle: .<?= $className ?>__toggle; content: .<?= $className ?>__content;">
                <?php foreach ( $items as $index => $item ) : ?>
                    <div class="animate col--1-1 <?= $openFirst && ! $index ? 'uk-open' : '' ?>">
                        <?= $this->element('items/faq-item', compact('data', 'item', 'className')) ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
