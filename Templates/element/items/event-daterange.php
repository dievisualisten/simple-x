<?php

use App\Library\Facades\Request;

/**
 * @version 4.0.0
 */

$defaults = [
  'className' => 'teaser',
  'items'     => null,
];

extract($defaults, EXTR_SKIP);

$defaultPages = [
  'first', 'prev', 'next', 'last',
];

/**
 * @var array $items
 * @var string $className
 */

?>

<form id="eventRangeForm" class="<?= $className ?>__daterange" method="GET">
    <div class="grid grid--row grid--wrap grid--center grid--10-10">
        <div class="col--1-2">
            <div class="<?= $className ?>__daterange-from form-group">
                <label for="eventFrom"><?= __('Von'); ?></label>
                <input id="eventFrom" type="text" class="input" sx-datepicker="format: L; minDate: -7; display: #eventRangeFrom; focusNext: #eventTo;">
                <input id="eventRangeFrom" type="hidden" value="<?= $this->Article->getMonthRangeStart() ?>">
            </div>
        </div>
        <div class="col--1-2">
            <div class="<?= $className ?>__daterange-to form-group">
                <label for="eventTo"><?= __('Bis'); ?></label>
                <input id="eventTo" type="text" class="input" sx-datepicker="format: L; display: #eventRangeTo;">
                <input id="eventRangeTo" type="hidden" value="<?= $this->Article->getMonthRangeEnd() ?>">
            </div>
        </div>
        <div class="col--1-1">
            <div class="<?= $className ?>__daterange-submit">
                <button id="eventRangeSubmit" class="button button--primary" type="submit" disabled><?= __('Suchen'); ?></button>
            </div>
        </div>
    </div>
</form>


<?php
$this->append('scriptBottom');
?>

<script>
    (function ($) {
        pi.Dom.ready(function () {

            var submitAction = function () {
                window.location.href = '<?= Request::fullUrl(); ?>/from:' +
                  $('#eventRangeFrom').val() + '/to:' + $('#eventRangeTo').val();
            };

            $('#eventFrom, #eventTo').on('keydown', function (event) {
                if ( event.which === 13 ) {
                    submitAction();
                }
            });

            $('#eventRangeForm').on('submit', function (event) {

                event.preventDefault();
                event.stopPropagation();

                submitAction();
            });

            $('#eventRangeSubmit').prop('disabled', false);
        });
    })(jQuery);
</script>

<?
$this->end();
?>