<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 */

$defaults = [
    'className' => 'teaser',
    'item'      => null,
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var \App\Model\Entity\Article $item
 */

?>

<?php if ( ! empty($item) ) : ?>
    <?= $this->Article->openLink($item, ['class' => "{$className}__item grid grid--col"]); ?>

        <?php

        $imageEl = $this->element('media/default', [
            'data'        => $item,
            'placeholder' => true,
            'pick'        => ['teaser', 'all'],
        ]);

        ?>

        <?php if ( ! realempty($imageEl) ) : ?>
            <div class="<?= $className ?>__image col--flex-0-0">
                <?= $imageEl; ?>
            </div>
        <?php endif; ?>

        <div class="<?= $className ?>__body col--middle">

            <h3><?= $this->Fe->getTitle($item); ?></h3>

            <?= $this->element('article/price', [
                'className' => $className,
                'data'      => $item,
            ]); ?>

            <?= $this->element('article/eventdate', [
                'className' => $className,
                'data'      => $item,
            ]); ?>

            <?= $this->element('article/teasertext', [
                'className'    => $className,
                'data'         => $item,
                'useShortText' => true,
            ]); ?>

            <?= $this->element('article/readmore', [
                'className' => $className,
                'data'      => $item,
                'linkText'  => false,
            ]); ?>

        </div>

    <?= $this->Article->closeLink($item); ?>
<?php endif; ?>
