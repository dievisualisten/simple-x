<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className'   => 'teaser',
    'items'       => $this->Article->matrix(8, $data->children),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */

?>

<?php if ( ! realempty($items) && count($items) ) : ?>

    <section class="<?= $className ?> <?= $className ?>--slider flickity-frame">
        <div class="container">
            <div class="<?= $className ?>__slider" sx-flickity="hideOnEmpty: .<?= $className ?>__navigation;">
                <?php foreach ( $items as $item ) : ?>
                    <div class="col--1-1">
                        <?= $this->element('items/teaser-item-large', compact('data', 'item', 'className')); ?>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="<?= $className ?>__navigation">
                <a href="javascript:void(0)" class="<?= $className ?>__prev" sx-flickity-prev="el: .<?= $className ?>__slider"></a>
                <a href="javascript:void(0)" class="<?= $className ?>__next" sx-flickity-next="el: .<?= $className ?>__slider"></a>
            </div>
        </div>
    </section>

<?php endif; ?>
