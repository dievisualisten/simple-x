<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className' => 'teaser',
    'items'     => $this->Article->matrix(4, $data->children),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */

?>

<?php if ( ! realempty($items) && count($items) ) : ?>
    <section class="<?= $className ?> <?= $className ?>--teaser">
        <div class="container">
            <div class="grid grid--row grid--wrap grid--center grid--30-30">
                <?php foreach ( $items as $item ) : ?>
                    <div class="animate col--12 col--1-3@md">
                        <?= $this->element('items/teaser-item-small', compact('data', 'item', 'className')); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
