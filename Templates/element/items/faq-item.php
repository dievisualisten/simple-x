<?php

/**
 * @version 4.0.0
 */

$defaults = [
    'className' => 'teaser',
    'item'      => null
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var \App\Model\Entity\Article $item
 */

?>

<?php if ( ! empty($item) ) : ?>
    <div class="<?= $className ?>__item">

        <div class="<?= $className ?>__toggle">
            <h3><?= $this->Fe->getTitle($item); ?></h3>
        </div>

        <div class="<?= $className ?>__content uk-closed">

            <?php

            $imageEl = $this->element('media/default', [
                'data'        => $item,
                'placeholder' => false,
                'pick'        => ['teaser', 'all'],
            ]);

            ?>

            <?php if ( ! realempty($imageEl) ) : ?>
                <div class="<?= $className ?>__image">
                    <?= $imageEl ?>
                </div>
            <?php endif; ?>

            <?php

            $contentEl = $this->element('article/content', [
                'className'    => $className,
                'data'         => $item
            ]);

            ?>

            <?php if ( ! realempty($contentEl) ) : ?>
                <div class="<?= $className ?>__body">
                    <?= $contentEl ?>
                </div>
            <?php endif; ?>

        </div>

    </div>
<?php endif; ?>
