<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className'   => 'teaser',
    'items'       => $this->Article->matrix(32, $data->children),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */

$paginated = $this->Article->paginate($items);

?>
<?php if ( ! realempty($paginated) && count($paginated) ) : ?>
    <section class="<?= $className ?> <?= $className ?>--list">
        <div class="<?= $className ?>__group">
            <div class="grid grid--row grid--wrap grid--30-30">
                <?php foreach ( $paginated as $item ) : ?>
                    <div class="animate col--1-1">
                        <?= $this->element('items/teaser-item-large', compact('data', 'item', 'className')); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?= $this->element('items/list-pagination', compact('items', 'className')); ?>
    </section>
<?php endif; ?>
