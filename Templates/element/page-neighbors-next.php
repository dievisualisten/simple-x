<?
if (empty($options)) {
    $options = [
        'next_text' => __('Nächster Artikel', true),
    ];
}
$defaults = ['show_text' => true, 'show_next' => true, 'img' => 'false', 'folder' => 's', 'show' => 'all'];

extract($defaults, EXTR_SKIP);
?>
<? if ($show_next && !empty($neighbors['next'])) { ?>
    <div class="page-neighbors-next">
        <?
        if ($show_text) {
            ?>
            <div class="page-neighbors-headline">
                <?= $next_text ?>
            </div>
            <?
        }
        ?>
        <?
        if ($img) {
            echo $this->element('img', [
                'link' => $neighbors['next']['Menu']['path'],
                'folder' => $folder,
                'show' => $show,
                'data' => $neighbors['next'],
            ]);
        }
        ?>
        <h3><?= $this->Fe->link($neighbors['next']); ?></h3>
    </div>
<? } ?>
