<?php

use App\Utility\StringUtility;

// Get formular
$form = $data->get('formularconfig');

?>
<?php if ( ! empty($data->formularconfig) ) : ?>
    <div id="formular" class="formular formular--<?= $form->get('formular.horizontal') ? 'horizontal' : 'vertical'; ?>">
        <div class="container">

            <?php

            $classList = [
                'grid', 'grid--row', 'grid--wrap', 'grid--15-15',
            ];

            // Add formular alignment
            $classList[] = 'grid--' . $form->get('formular.alignment') ?: 'center';

            // Add formular prefix
            $classList[] = 'formdata-' .
                StringUtility::slug($form->identifier ?: $form->name);

            echo $this->Form->create($Formdata, [
                'type' => 'post', 'enctype' => 'multipart/form-data', 'class' => $classList, 'id' => $form->get('id'), 'url' => '#' . $form->get('id'),
            ]);

            echo $this->Form->hidden('formularconfig_id', [
                'value' => $form->get('id'),
            ]);

            ?>

            <?php foreach ( $Formdata->getFields() as $field ) : ?>
                <?php

                /* @var \App\Library\Formular\FormularField $field */

                $field->setFormularconfig($form);

                $fieldClassList = [
                    "col--1-1",
                    "col--" . ($field->colXs ?: $form->get('formular.extended_data.colXs') ?: 12) . "-12@xs",
                    "col--" . ($field->colSm ?: $form->get('formular.extended_data.colSm') ?: 12) . "-12@sm",
                    "col--" . ($field->colMd ?: $form->get('formular.extended_data.colMd') ?: 12) . "-12@md",
                    "col--" . ($field->colLg ?: $form->get('formular.extended_data.colLg') ?: 12) . "-12@lg",
                ];

                if ( $field->cssClass !== '' ) {
                    $fieldClassList[] = $field->cssClass;
                }

                if ( $field->type === 'containeropen' ) {

                    // Open containers
                    echo '<div class="' . implode(' ', $fieldClassList) . '">'
                        . '<div class="' . implode(' ', $classList) . '">';

                    continue;
                }

                if ( $field->type === 'containerclose' ) {

                    // Close containers
                    echo '</div></div>';

                    continue;
                }

                if ( $field->type === 'br' ) {

                    // Close containers
                    echo '<div style="flex: 0 0 auto; width: 100%; height: 0; padding: 0; margin: 0;"></div>';

                    continue;
                }

                $attrs = [];

                if ( ! realempty($field->showOnFieldName) ) {
                    $attrs['sx-form-visibility'] = StringUtility::jsAttr(
                        $field->getShowOnOptions()
                    );
                }

                $fieldClassList[] = 'formdata-' .
                    StringUtility::slug($field->name ?: $field->label);

                $attrs['class'] = implode(' ', $fieldClassList);

                ?>

                <div <?php echo StringUtility::domAttr($attrs); ?>>
                    <?php
                    // Set used filetype
                    $fieldType = $field->type;

                    if ( $form->formular->get('hidelabels', false) ) {

                        // Hide label if defined in formular
                        $field->set('hideLabel', true);
                    }

                    if ( $field->hideLabel && empty($field->placeholder) ) {

                        // Apply label to placeholder if label is hidden
                        $field->set('placeholder', $field->label .
                            ($field->notBlank ? ' *' : ''));
                    }

                    // Make custom field string.
                    $customField = "{$field->type}-{$field->getName()}";

                    if ( $this->elementExists("formular/{$customField}") ) {

                        // Rewrite type to field.
                        $fieldType = $customField;
                    }

                    if ( $field->type === 'element' ) {

                        $filePath = $field->inputName;

                        if ( ! $this->elementExists($filePath) ) {
                            echo "<pre>Element missing \"{$filePath}\"</pre>";
                        }

                        if ( $this->elementExists($filePath) ) {
                            echo $this->element($filePath, ['field' => $field, 'form' => $form]);
                        }

                    } else {

                        $filePath = "formular/{$fieldType}";

                        if ( ! $this->elementExists($filePath) ) {
                            echo "<pre>Element missing \"{$filePath}\"</pre>";
                        }

                        if ( $this->elementExists($filePath) ) {
                            echo $this->element($filePath, ['field' => $field, 'form' => $form]);
                        }

                        // Render field.

                    }


                    ?>
                </div>

            <?php endforeach; ?>

            <?php

            echo $this->element("formular/honeypot", [
                'form' => $form,
            ]);

            echo $this->element("formular/requiredtext", [
                'form' => $form,
            ]);

            echo $this->element("formular/submit", [
                'form' => $form,
            ]);

            ?>

            <?php echo $this->Form->end(); ?>
        </div>
    </div>
<?php endif; ?>
