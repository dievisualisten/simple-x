<?
if (empty($options)) {
    $options = [
        'back_text' => __('Vorheriger Artikel', true),
    ];
}
$defaults = ['show_text' => true, 'show_prev' => true, 'img' => 'false', 'folder' => 's', 'show' => 'all'];

extract($defaults, EXTR_SKIP);

?>
<? if ($show_prev && !empty($neighbors['prev'])) { ?>
    <div class="page-neighbors-prev">
        <?
        if ($show_text) {
            ?>
            <div class="page-neighbors-headline">
                <?= $back_text ?>
            </div>
            <?
        }
        ?>
        <?
        if ($img) {
            echo $this->element('img', [
                'link' => $neighbors['prev']['Menu']['path'],
                'folder' => $folder,
                'show' => $show,
                'data' => $neighbors['prev'],
            ]);
        }
        ?>
        <h3><?= $this->Fe->link($neighbors['prev']); ?></h3>
    </div>
<? } ?>
