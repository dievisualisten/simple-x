<?
if (!empty($use_conversion_tracking)) {

    if (!empty($conversion_tracking)) {
        echo $conversion_tracking;
    }
    ?>

    <script>
        <? //Tracking von Formularen
        ?>
        ga('send', 'event', 'formular', 'submit', '<?=$data['Formularconfig']['name']?>');
        <?
        ?>

        <? //Tracking von Newsletteranmeldungen
        if (!empty($data['Formdata']['newsletter'])){
        ?>
        ga('send', 'event', 'formular', 'submit', 'newsletter subscription');
        <?
        }
        ?>

        <?
        //Wenn Shop
        if (!empty($data['Order']['Card'])){
        ?>

        ga('require', 'ecommerce', 'ecommerce.js');
        ga('ecommerce:addTransaction', {
            'id': '<?=$data['Order']['Order']['rnr'] ?>'                     		// Transaction ID. Required.
            //'affiliation': 'Acme Clothing'  // Affiliation or store name.
            , 'revenue': '<?=$data['Order']['Card']['total_amount'] ?>'          // Grand Total.
            , 'shipping': '<?=$data['Order']['Card']['shipping_costs'] ?>'       // Shipping.
            //,'tax' : '0'
        });

        /* TODO Produkte*/
         ga('ecommerce:addItem', {
         'id': '<?=$data['Order']['Order']['rnr'] ?>',                     // Transaction ID. Required.
         'name': 'Fluffy Pink Bunnies',    // Product name. Required.
         'sku': 'DD23444',                 // SKU/code.
         'category': 'Party Toys',         // Category or variation.
         'price': '11.99',                 // Unit price.
         'quantity': '1'                   // Quantity.
         });

        ga('ecommerce:send');
        <?
        }
        ?>
    </script>
    <?

}
?>
