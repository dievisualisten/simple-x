<?
if (!empty($list)) {


    $pagingParams = $this->Paginator->params();
    if (!empty($pagingParams['pageCount'] > 1)) {
        ?>
        <nav>
            <ul class="pagination paging-bar ">
                <?
                $this->Paginator->options([
                    'url' => ['lang' => false, 'action' => request()->path(), 'controller' => '/'],
                ]);

                $this->Paginator->templates([
                    'nextActive' => '<li class="page-item"><a class="page-link" rel="next" aria-label="Next" href="{{url}}"><span>{{text}}</span></a></li>',
                    'nextDisabled' => '<li class="page-item disabled"><a class="page-link" tabindex="-1"><span>{{text}}</span></a></li>',
                    'prevActive' => '<li class="page-item"><a class="page-link" rel="prev" aria-label="Previous" href="{{url}}"><span>{{text}}</span></a></li>',
                    'prevDisabled' => '<li class="page-item disabled"><a class="page-link" tabindex="-1"><span>{{text}}</span></a></li>',
                    'current' => '<li class="page-item active"><a class="page-link" href="#">{{text}} <span class="sr-only">(current)</span></a></li>',
                    'first' => '<li class="page-item first"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'last' => '<li class="page-item last"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                ]);

                echo $this->Paginator->prev('<< ' . __('zurück', true));
                echo $this->Paginator->first(__('Erste Seite', true));
                echo $this->Paginator->numbers([
                    'separator' => '',
                    'modulus' => 3,
                ]);
                echo $this->Paginator->last(__('Letzte Seite', true));
                echo $this->Paginator->next(__('weiter', true) . ' >>');
                ?>
            </ul>
        </nav>
        <?
    }
}
?>
