<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
  'className'    => 'article',
  'property'     => 'content',
  'content'      => '',
  'stripEmptyPs' => true,
  'wrap'         => true,
  'before'       => '',
  'after'        => '',
];

/**
 * @var string $className
 * @var string $property
 * @var string $content
 * @var bool $stripEmptyPs
 * @var bool $wrap
 * @var string $before
 * @var string $after
 */

extract($defaults, EXTR_SKIP);

if ( empty($content) ) {
    $content = $this->Fe->getArticleEntity($data)->get($property);
}

if ( ! $stripEmptyPs ) {
    $content = preg_replace("/<p[^>]*>[\s|&nbsp;]*<\/p>/", '', $content);
}

$outerHtml = '%s';

if ( $wrap ) {
    $outerHtml = '<div class="' . $className . '__' . $property . '">%s</div>';
}

$innerHtml = $before . $content . $after;

if ( realempty($innerHtml) ) {
    return;
}

echo sprintf($outerHtml, $innerHtml);