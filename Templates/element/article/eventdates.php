<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'headline' => __('Alle Termine zu diesem Event'),
    'bemBlock' => 'eventdates',
    'bemBlockMod' => '',
];

extract($defaults, EXTR_SKIP);

?>
<?php if ( ! $data->isEmpty('menu.events') ) : ?>
    <div class="grid grid--row grid--center grid--wrap grid--10-10">
        <?php foreach ( $data->get('menu.events') as $event ) : ?>
            <div class="col--1-1 col--1-3@sm col--1-4@md col--1-6@lg">
                <?= $this->element('article/eventdate', [
                    'startDate'   => $event->get('date')->format('Y-m-d'),
                    'markToday'   => true,
                    'markCurrent' => true,
                ]); ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

