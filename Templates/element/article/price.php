<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\DomainManager;
use App\Utility\FormatUtility;

$formatDefaults = [
    'places'    => 2,
    'escape'    => false,
    'before'    => ' € ',
    'decimals'  => ',',
    'thousands' => '.',
];

$defaults = [
    'className'      => 'teaser',
    'rangeSeperator' => '-',
    'showPrefix'     => true,
    'showSuffix'     => true,
    'showMinPrice'   => true,
    'showMaxPrice'   => false,
    'format'         => []
];

/**
 * @var string $className
 * @var string $rangeSeperator
 * @var bool $showPrefix
 * @var bool $showSuffix
 * @var bool $showMinPrice
 * @var bool $showMaxPrice
 * @var array $format
 */

extract($defaults, EXTR_SKIP);

// Merge format defaults
$format = array_merge($formatDefaults, $format);

?>
<?php if ( ! $data->isEmpty('custom.price.min') || ! $data->isEmpty('custom.price.max') ) : ?>
    <div class="<?= $className ?>__price">
        <?php

        $minPrice = $data->get('custom.price.min', null);
        $maxPrice = $data->get('custom.price.max', null);

        if ( ! $showMinPrice ) {
            $minPrice = null;
        }

        if ( $minPrice ) {
            $minPrice = FormatUtility::currency($minPrice, $format);
        }

        if ( ! $showMaxPrice ) {
            $maxPrice = null;
        }

        if ( $maxPrice ) {
            $maxPrice = FormatUtility::currency($maxPrice, $format);
        }

        $prefixPrice = $data->get('custom.price.prefix', null);
        $suffixPrice = $data->get('custom.price.suffix', null);

        if ( ! $showPrefix ) {
            $prefixPrice = null;
        }

        if ( ! $showSuffix ) {
            $suffixPrice = null;
        }

        if ( $prefixPrice ) {
            $prefixPrice = __(DomainManager::get('priceprefix', $prefixPrice));
        }

        if ( $suffixPrice ) {
            $suffixPrice = __(DomainManager::get('pricesuffix', $suffixPrice));
        }

        $finalString = str_join(" {$rangeSeperator} ", $minPrice, $maxPrice);

        $finalString = str_join(' ', $prefixPrice, $finalString, $suffixPrice);

        echo "<span>{$finalString}</span>";

        ?>
    </div>
<?php endif; ?>
