<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'className'  => 'teaser',
    'moreText'   => __('mehr erfahren'),
    'beforeText' => '',
    'afterText'  => '',
    'linkText'   => false,
];

/**
 * @var string $className
 * @var string $moreText
 * @var string $beforeText
 * @var string $afterText
 * @var bool $linkText
 */

extract($defaults, EXTR_SKIP);

if ( ! $data->isEmpty('moretext') ) {
    $moreText = $data->get('moretext');
}

?>

<div class="<?= $className ?>__readmore">
        <?php

        if ( ! empty($beforeText) ) {
            echo $beforeText;
        }

        if ( $linkText ) {
            echo $this->Article->openLink($data);
        }

        echo "<span>{$moreText}</span>";

        if ( $linkText ) {
            echo $this->Article->closeLink($data);
        }

        if ( ! empty($afterText) ) {
            echo $afterText;
        }

        ?>
</div>
