<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'prefix'   => 'sx-',
    'property' => 'cstom.custom.icon',
];

/**
 * @var string $prefix
 * @var string $property
 */

extract($defaults, EXTR_SKIP);


?>
<?php if ( ! $data->isEmpty($property) ) : ?>
    <i class="<?= $prefix . $data->get($property) ?>"></i>
<?php endif; ?>
