<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaultPriceOptions = [
    'show' => false, 'link' => false
];

$defaultEventOptions = [
    'show' => false, 'link' => false
];

$defaults = [
    'className'    => 'article',
    'tag'          => 'h1',
    'property'     => 'headline',
    'class'        => '',
    'fallback'     => '',
    'title'        => '',
    'before'       => '',
    'after'        => '',
    'priceOptions' => $defaultPriceOptions,
    'eventOptions' => $defaultEventOptions,
];

/**
 * @var string $className
 * @var string $tag
 * @var string $property
 * @var string $fallback
 * @var string $title
 * @var string $class
 * @var string $before
 * @var string $after
 * @var array $priceOptions
 * @var array $eventOptions
 */

extract($defaults, EXTR_SKIP);

if ( $property === 'headline' && empty($fallback) ) {
    $fallback = 'title';
}

if ( empty($title) ) {
    $title = $this->Fe->getArticleEntity($data)->get($property);
}

if ( empty($title) && ! empty($fallback) ) {
    $title = $this->Fe->getArticleEntity($data)->get($fallback);
}

if ( $eventOptions['show'] ) {
    $after = $this->element('article/eventdate', $eventOptions) . ' ' . $after;
}

if ($priceOptions['show']) {
    $after = $this->element('article/price', $priceOptions) . ' ' . $after;
}

echo '<' . $tag . ' class="' . $className . '__' . $property . ' ' . $class .'">' . $before . $title  . $after . '</' . $tag . '>';
