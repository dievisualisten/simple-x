<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\Facades\Request;
use Cake\I18n\Date;

// Date fallback
$fallback = Request::param('event');

$defaults = [
    'className'      => 'teaser',
    'classList'      => [],
    'startDate'      => $fallback,
    'endDate'        => null,
    'dateFormat'     => __('d.m.Y'),
    'timeFormat'     => __('H:i'),
    'rangeSeperator' => ' - ',
    'timeSeperator'  => ', ',
    'showDateOnly'   => false,
    'showStartOnly'  => false,
    'markToday'      => false,
    'markCurrent'    => false
];

/**
 * @var string $className
 * @var string $startDate
 * @var string $endDate
 * @var string $dateFormat
 * @var string $timeFormat
 * @var string $rangeSeperator
 * @var string $timeSeperator
 * @var bool $showDateOnly
 * @var bool $showStartOnly
 * @var bool $markToday
 * @var bool $markCurrent
 */

extract($defaults, EXTR_SKIP);

if ( $markToday && $startDate === date('Y-m-d') ) {
    $classList[] = 'is-today';
}

if ( $markCurrent && $startDate === $fallback ) {
    $classList[] = 'is-current';
}

?>
<?php if ( ! $data->isEmpty('menu.events') ) : ?>
    <div class="<?= $className ?>__eventdate <?= implode(' ', $classList); ?>">
        <?php

        if ( ! $startDate ) {
            $startDate = $data->get('custom.event.start_date', null);
        }

        $startTime = $data->get('custom.event.start_time', null);

        if ( ! $endDate ) {
            $endDate = $data->get('custom.event.end_date', null);
        }

        $endTime = $data->get('custom.event.end_time', null);

        $startString = Date::parse($startDate)->format($dateFormat);
        $endString = Date::parse($endDate)->format($dateFormat);

        if ( ! $showStartOnly && ! empty($endDate) && $startDate !== $endDate ) {
            $startString .= $rangeSeperator . $endString;
        }

        if ( ! $showDateOnly && ! empty($startTime) ) {
            $startString .= $timeSeperator . Date::parse($startTime)->format($timeFormat);
        }

        if ( ! $showDateOnly && ! empty($startTime) && ! empty($endTime) ) {
            $startString .= $rangeSeperator . Date::parse($endTime)->format($timeFormat);
        }

        echo "<span>{$startString}</span>";

        ?>
    </div>
<?php endif; ?>
