<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'className' => 'article',
    'items'     => $this->Article->matrix(1024, $data->children),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 */

?>

<?php if ( ! empty($items) && count($items) ) : ?>
    <section class="multipage">
        <?php foreach ( $items as $item ) : ?>

            <div class="container">
                <article id="<?= $item->get('slug') ?>" class="<?= $className ?> <?= $className ?>__<?= $item->get('type') ?>-<?= $item->get('layout') ?>">

                    <?= $this->element('media/default', [
                        'format' => '2_1', 'pick' => ['teaser', 'all'], 'data' => $item
                    ]); ?>

                    <div class="<?= $className ?>__body">
                        <?= $this->element('article/headline', [
                            'className' => $className, 'tag' => 'h2', 'data' => $item
                        ]); ?>
                        <?= $this->element('article/content', [
                            'className' => $className, 'data' => $item
                        ]); ?>
                        <?= $this->element('media/downloads', [
                            'className' => $className, 'data' => $item
                        ]); ?>
                        <?= $this->element('items-galerie', [
                            'className' => $className, 'data' => $item
                        ]); ?>
                    </div>

                </article>
            </div>

            <?= $this->element('items/faq', [
                'data' => $item
            ]); ?>

            <?= $this->element('items/slider', [
                'data' => $item
            ]); ?>

            <?= $this->element('items/teaser', [
                'data' => $item
            ]); ?>

            <?= $this->element('maps/directions', [
                'data' => $item
            ]); ?>

        <?php endforeach; ?>
    </section>
<?php endif; ?>
