<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use Cake\Utility\Text;

$defaults = [
    'className'    => 'article',
    'content'      => '',
    'useContent'   => true,
    'useContent2'   => true,
    'property'     => 'teasertext',
    'showMore'     => true,
    'maxChars'     => 200,
    'allowedTags'  => '<br><em><i><p>',
    'stripEmptyPs' => true,
    'wrap'         => true,
    'before'       => '',
    'after'        => '',
];

/**
 * @var string $className
 * @var string $content
 * @var string $property
 * @var bool $useContent
 * @var bool $useContent2
 * @var bool $showMore
 * @var int $maxChars
 * @var string $allowedTags
 * @var bool $stripEmptyPs
 * @var bool $wrap
 * @var string $before
 * @var string $after
 */


extract($defaults, EXTR_SKIP);

if ( empty($content) && $data->get('has_' . $property) ) {
    $content = nl2br($data->get($property));
}

if ( empty($content) && $useContent ) {

    $content = strip_tags($data->get('content'), $allowedTags);

    if ( empty($content) && $useContent2) {
        $content = strip_tags($data->get('content2'), $allowedTags);
    }

    $truncateOptions = [
        'exact' => false, 'html' => true,
    ];

    $content = Text::truncate($content, $maxChars, $truncateOptions);
}

if ( ! $stripEmptyPs ) {
    $content = preg_replace("/<p[^>]*>[\s|&nbsp;]*<\/p>/", '', $content);
}

$outerHtml = '%s';

if ( $wrap ) {
    $outerHtml = '<div class="' . $className . '__teasertext">%s</div>';
}

$innerHtml = $before . $content . $after;

if ( realempty($innerHtml) ) {
    return;
}

echo sprintf($outerHtml, $innerHtml);

