<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

/**
 * stellt entweder ein Bild da oder wählt das erste passende aus einer Liste aus.
 */

$imageFallback = [
    'alt'     => true,
    'title'   => true,
    'src'     => '',
    'class'   => '',
    'display' => true,
];

$defaults = [
    'className'   => 'image',
    'data'        => [],
    'pick'        => ['all'],
    'format'      => '2_1',
    'wrap'        => true,
    'placeholder' => false,
    'lazyload'    => true,
    'width'       => null,
    'image'       => $imageFallback,
];


extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $pick
 * @var array $format
 * @var bool $wrap
 * @var bool $placeholder
 * @var bool $lazyload
 * @var mixed $width
 * @var string $before
 * @var string $after
 * @var array $image
 */

if ( isset($image) ) {
    $image = array_merge($imageFallback, $image);
}

$attachment = $this->Media->getRealAttachment($data, [
  'pick' => $pick
]);

if ( $attachment->type !== 'image' && $attachment->get('thumbnail') ) {
    $attachment = $attachment->get('thumbnail');
}

$mediaQuery = $this->Media->getMediaQuery($format);

if ( $attachment->type === 'svg' ) {
    $image['src'] = $attachment->getUrl();
}

if ( empty($image['src']) && ! empty($attachment) ) {

    foreach ( $mediaQuery as $key => $media ) {
        $image[$key] = $this->Media->getVersionUrl($attachment,
          $media, '{width}');
    }

    if ( $image['alt'] === true ) {
        $image['alt'] = $attachment->get('alternative');
    }

    if ( $image['title'] === true ) {
        $image['title'] = $attachment->get('title');
    }

}

if ( empty($image['src']) ) {
    $image['display'] = $placeholder;
}

if ( $lazyload ) {

    $lazloadOptions = [
        'src'                   => '/src/static/img/spacer.gif',
        'data-sizes'            => 'auto',
        'data-parent-container' => 'html',
        'data-parent-fit'       => 'contain',
    ];

    foreach ( array_keys($mediaQuery) as $key ) {

        $lazloadOptions["data-{$key}"] = data_get($image, $key);

        unset($image[$key]);
    }

    $image = array_merge($image, $lazloadOptions);

    if ( empty($image['data-src']) ) {
        $image['class'] = str_join(' ', $image['class'], 'empty');
    }

    $image['class'] = str_join(' ', $image['class'],
      "lazyload mime-{$attachment->type}");
}

if ( $width && ! $lazyload ) {
    $image['src'] = str_join('?', $image['src'], "w={$width}");
}

foreach ( $mediaQuery as $key => $media ) {

    $temporary = str_replace('src', $media,
      str_replace('-', '@', $key));

    $className .= ' ratio--' .
      str_replace('_', '-', $temporary);
}

$className = str_replace('{type}', 'image', $className);

?>
<?php if ( $image['display'] ) : ?>
    <?php if ( $wrap ) : ?>
        <div class="<?= $className; ?> <?= $image['class']; ?>">
    <?php endif; ?>
    <?= $this->Html->image($image['src'], $image, [
        'escape' => false
    ]); ?>
    <?php if ( $wrap ) : ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
