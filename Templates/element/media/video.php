<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

//dd($data);

$imageFallback = [
  'alt'     => '',
  'title'   => '',
  'src'     => '',
  'class'   => '',
  'display' => true,
];

$defaults = [
  'className'   => 'video',
  'data'        => [], //menu, article, attachments oder attachment
  'pick'        => ['all'],
  'format'      => '2_1',
  'wrap'        => true,
  'placeholder' => false,
  'lazyload'    => true,
  'width'       => null,
  'image'       => $imageFallback,
];


extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $pick
 * @var string $format
 * @var bool $wrap
 * @var bool $placeholder
 * @var bool $lazyload
 * @var mixed $width
 * @var string $before
 * @var string $after
 * @var array $image
 */

if ( isset($image) ) {
    $image = array_merge($imageFallback, $image);
}

$attachment = $this->Media->getRealAttachment($data, [
  'pick' => $pick
]);

$mediaQuery = $this->Media->getMediaQuery($format);

if ( empty($image['src']) && ! empty($attachment) ) {

    $image['src'] = $attachment->get('originalurl');

    if ( $image['alt'] === true ) {
        $image['alt'] = $attachment->get('alternative');
    }

    if ( $image['title'] === true ) {
        $image['title'] = $attachment->get('title');
    }

    $image['ext'] = preg_replace('/^(.*?)\.([^.]+)$/',
      '$2', $image['src']);
}

foreach ( $mediaQuery as $key => $media ) {

    $temporary = str_replace('src', $media,
      str_replace('-', '@', $key));

    $className .= ' ratio--' .
      str_replace('_', '-', $temporary);
}

$className = str_replace('{type}',
    'video', $className);

?>
<?php if ( $image['display'] ) : ?>
    <?php if ( $wrap ) : ?>
        <div class="<?= $className; ?> <?= $image['class']; ?>" sx-ratio="ratio: 0.5633;">
    <?php endif; ?>

    <?php if ( $attachment->get('thumbnail') ) : ?>
        <div class="video__image">
            <?= $this->element('media/image', [
                'data' => $attachment->get('thumbnail'), 'wrap' => false
            ]); ?>
        </div>
    <?php endif; ?>


    <div class="video__wrap">

        <?php if ( ! $attachment->get('provider', null) ) : ?>
            <video width="320" height="240" autoplay muted loop sx-video="delay: 1000;">
                <source  src="<?= $image['src']; ?>" type="video/<?= $image['ext']; ?>">
            </video>
        <?php endif; ?>

        <?php if ( $attachment->get('provider', null) === 'youtube' ) : ?>
            <div sx-youtube="id: <?= $attachment->get('identifier'); ?>; delay: 1000;"></div>
            <?php $this->Asset->script('https://www.youtube.com/iframe_api', false, ['async']); ?>
        <?php endif; ?>

        <?php if ( $attachment->get('provider', null) === 'vimeo' ) : ?>
            <div sx-vimeo="id: <?= $attachment->get('identifier'); ?>; delay: 1000;"></div>
            <?php $this->Asset->script('https://player.vimeo.com/api/player.js', false, ['async']); ?>
        <?php endif; ?>

    </div>
    <?php if ( $wrap ) : ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
