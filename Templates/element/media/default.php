<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$imageFallback = [
    'alt'     => true,
    'title'   => true,
    'src'     => '',
    'class'   => '',
    'display' => true,
];

$defaults = [
    'className'   => '{type}',
    'view'        => '',
    'data'        => null,
    'pick'        => ['all'],
    'format'      => ['1_1', '2_1@md'],
    'wrap'        => true,
    'placeholder' => false,
    'lazyload'    => true,
    'width'       => null,
    'image'       => $imageFallback,
];


extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var string $view
 * @var array $pick
 * @var array $format
 * @var bool $wrap
 * @var bool $placeholder
 * @var bool $lazyload
 * @var mixed $width
 * @var string $before
 * @var string $after
 * @var array $image
 */

if ( isset($image) ) {
    $image = array_merge($imageFallback, $image);
}

$attachment = $this->Media->getRealAttachment($data, [
    'pick' => $pick,
]);

if ( empty($attachment) ) {
    return;
}

$options = [
  'className'   => $className,
  'data'        => $data,
  'pick'        => $pick,
  'format'      => $format,
  'wrap'        => $wrap,
  'placeholder' => $placeholder,
  'lazyload'    => $lazyload,
  'width'       => $width,
  'image'       => $image,
];

if ( ! empty($view) && $view === 'image' ) {
    echo $this->element('media/image', $options);
}

if ( empty($view) && $attachment->type === 'image' ) {
    echo $this->element('media/image', $options);
}

if ( empty($view) && $attachment->type === 'video' ) {
    echo $this->element('media/video', $options);
}

if ( empty($view) && $attachment->type === 'svg' ) {
    echo $this->element('media/image', $options);
}
