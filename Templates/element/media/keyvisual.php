<?php

/**
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className' => 'keyvisual',
    'format'    => '2_1',
    'pick'      => ['video', 'titleimg'],
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var string $format
 * @var array $pick
 */

$attachments = $this->Media->getAttachments($data, [
    'pick' => $pick,
]);
?>

<?php if ( ! empty($attachments) ) : ?>

    <div class="<?= $className ?> flickity-frame">
        <div class="<?= $className ?>__slider" sx-flickity="autoPlay: 3000; hideOnEmpty: .<?= $className ?>__navigation;">
            <?php foreach ( $attachments as $attachment ) : ?>
                <div class="<?= $className ?>__slide col--1-1">
                    <?= $this->element('media/default', [
                        'link'   => false,
                        'data'   => $attachment,
                        'width'  => '{width}',
                        'format' => $format,
                    ]); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="<?= $className ?>__navigation">
            <a href="javascript:void(0)" class="<?= $className ?>__prev" sx-flickity-prev="el: .<?= $className ?>__slider"></a>
            <a href="javascript:void(0)" class="<?= $className ?>__next" sx-flickity-next="el: .<?= $className ?>__slider"></a>
        </div>

        <?= $this->element('media/stoerer', [
            //
        ]);
        ?>
    </div>

<?php else: ?>

    <div class="no-kv"></div>

<?php endif; ?>