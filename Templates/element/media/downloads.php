<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Article $data
 */

$defaults = [
    'className'     => 'downloads',
    'pick'          => ['download'],
    'showExtension' => true,
    'title'         => __('Downloads'),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $pick
 * @var bool $showExtension
 * @var string $title
 */

$attachments = $this->Media->getAttachments($data, [
    'pick' => $pick,
]);

?>

<?php if ( ! empty($attachments) ) : ?>
    <div class="<?= $className; ?>">

        <?php if ( ! empty($title) ) : ?>
            <h3><?= $title; ?></h3>
        <?php endif; ?>

        <ul class="<?= $className; ?>__items">
            <?php foreach ( $attachments as $attachment ) : ?>

                <?php
                $extension = null;

                if ( $showExtension && ! $attachment->isEmpty('attachment.metadata.Extension') ) {
                    $extension = '(' . $attachment->get('attachment.metadata.Extension') . ')';
                }
                ?>

                <li>
                    <a target="_blank" href="<?= $this->Fe->getMediaUrl($attachment->attachment, 'uploads') ?>">
                        <?= str_join(' ', $attachment->get('attachment.title'), $extension) ?>
                    </a>
                </li>

            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

