<?


if ( empty($options['networks']) ) {
    $options['networks'] = [];
}

$defaults = [
  'shareIcon' => 'sx-share',
  'networks'  => [
    'email'     => true,
    'facebook'  => true,
    'google'    => true,
    'twitter'   => true,
    'whatsapp'  => true,
    'linkedin'  => false,
    'pinterest' => false,
  ],
  'icons'     => [
    'emailIcon'     => 'sx-email',
    'facebookIcon'  => 'sx-facebook',
    'googleIcon'    => 'sx-googleplus',
    'twitterIcon'   => 'sx-twitter',
    'whatsappIcon'  => 'sx-whatsapp',
    'linkedinIcon'  => 'sx-linkedin',
    'pinterestIcon' => 'sx-pinterest',
  ],
  'mailText'  => __('Hey,\nwäre das nicht was für Dich?\n\nViele Grüße!\n\n'),
  'mailTitle' => '',
];

$options['networks'] = Hash::merge($defaults['networks'], $options['networks']);


extract($defaults, EXTR_SKIP);

// Social Networks für Plugin Initialisierung als String schreiben

$sites = "";

foreach ( $networks as $key => $network ) {

    if ( $network == 1 ) {
        $sites = $sites . $key . ",";
    }
}

?>

<div class="share-button">
    <div class="shareThis"></div>
    <i class="<?= $shareIcon ?> toggle-share"></i>
</div>

<?php
$this->append('scriptBottom');
?>
<script>

    $(document).ready(function () {

        var shareOpen = false;

        var sites = "<?=$sites?>";

        $("div.shareThis").simpleSocialShare({
            sites: sites,
            url: "",
            title: "<?=$mailTitle?>",
            description: "<?=$mailText?>",
            shareType: "none",
            triggerButtonActiveState: false,
            buttonSide: "left",
            orientation: "horizontal"
        });

        // Icons austauschen

        $('.simpleSocialShareSitesContainer > ul > li').css('background-image', 'none');

        <? if ($networks['email'] === true) { ?>
        $('li.simpleSocialShareEmail').append('<i class="<?=$icons['emailIcon']?>"></i>');
        <? } ?>
        <? if ($networks['facebook'] === true) { ?>
        $('li.simpleSocialShareFacebook').append('<i class="<?=$icons['facebookIcon']?>"></i>');
        <? } ?>
        <? if ($networks['google'] === true) { ?>
        $('li.simpleSocialShareGoogle').append('<i class="<?=$icons['googleIcon']?>"></i>');
        <? } ?>
        <? if ($networks['twitter'] === true) { ?>
        $('li.simpleSocialShareTwitter').append('<i class="<?=$icons['twitterIcon']?>"></i>');
        <? } ?>
        <? if ($networks['whatsapp'] === true) { ?>
        $('li.simpleSocialShareWhatsapp').append('<i class="<?=$icons['whatsappIcon']?>"></i>');
        <? } ?>
        <? if ($networks['linkedin'] === true) { ?>
        $('li.simpleSocialShareLinkedin').append('<i class="<?=$icons['linkedinIcon']?>"></i>');
        <? } ?>
        <? if ($networks['pinterest'] === true) { ?>
        $('li.simpleSocialSharePinterest').append('<i class="<?=$icons['pinterestIcon']?>"></i>');
        <? } ?>

        // Popup öffnen/schließen

        $('.toggle-share').click(function () {

            if ( !shareOpen ) {
                $('.shareThis').addClass('visible');
                shareOpen = true;
            } else {
                $('.shareThis').removeClass('visible');
                shareOpen = false
            }

        });

        // Popup schließen, wenn irgendwo auf der Seite geklickt wird

        $(document).on('mouseup', function (e) {

            if ( shareOpen == true
              && !$('.shareThis ').is(e.target)
              && $('.shareThis ').has(e.target).length === 0
              && !$('.toggle-share').is(e.target) // if the target of the click isn't the container...
              && $('.toggle-share').has(e.target).length === 0 // ... nor a descendant of the container
              && !isMobile() ) {

                $('.shareThis').removeClass('visible');
                shareOpen = false;
            }

        });


    });
</script>
<?
$this->end();
?>
