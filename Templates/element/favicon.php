<?php

/* @see https://realfavicongenerator.net 260px x 260px */

use App\Library\Facades\Menu;

/**
 * @version 4.0.0
 */

?>

<link rel="apple-touch-icon" sizes="180x180" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/favicon-16x16.png">
<link rel="manifest" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/site.webmanifest">
<link rel="mask-icon" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/safari-pinned-tab.svg" color="#000000">
<link rel="shortcut icon" href="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-config" content="/src/theme/<?=Menu::getDomainRecord('theme','default')?>/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
