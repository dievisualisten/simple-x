<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\Formular\FormularField;

if ( $form->get('captcha', false) === false ) {
    return;
}

$honeypots = [
  [
    'type'      => 'text',
    'label'     => 'E-Mail wiederholen',
    'inputName' => 'emails-repeat',
  ],
  [
    'type'      => 'text',
    'label'     => 'Notiz',
    'inputName' => 'users-note',
  ],
  [
    'type'      => 'text',
    'label'     => 'Nachname',
    'inputName' => 'persons-surname',
  ],
  [
    'type'      => 'text',
    'label'     => 'Nachricht',
    'inputName' => 'pages-text',
  ],
  [
    'type'      => 'text',
    'label'     => 'Captcha',
    'inputName' => 'forms-captcha',
  ],
];

$honeypots = array_reduce($honeypots, function ($merge, $honeypot) use ($form) {

    // Convert hoteypots to match real form.
    return array_merge($merge, [new FormularField($honeypot)]);

}, []);

?>
<?php foreach ( $honeypots as $field ) : ?>
    <div class="col--1-1 formular-donut">
        <?php
        echo $this->element("formular/{$field->type}", [
          'field' => $field, 'form' => $form,
        ]);
        ?>
    </div>
<?php endforeach; ?>
