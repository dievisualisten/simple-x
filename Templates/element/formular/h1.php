<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

?>
<h1 class="<?php echo $field->get('cssClass', ''); ?>">
    <?php echo $field->get('label'); ?>
</h1>
