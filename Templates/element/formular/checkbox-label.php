<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'classList' => ['checkbox']
];

extract($defaults, EXTR_SKIP);

$required = $this->element('formular/required', [
    'form' => $form, 'field' => $field
]);

$tooltip = $this->element('formular/tooltip', [
    'form' => $form, 'field' => $field
]);

$data = [
    'required' => false, 'id' => $field->getUnique()
];

?>
<div class="<?php echo implode(' ', $classList); ?>">
    <label class="checkbox-label" for="<?php echo $field->getUnique(); ?>">
        <?php echo $this->Form->checkbox($field->getName(), $data); ?> <span><?php echo $field->label; ?></span><?php if ( $field->hideLabel ) echo $required ?><?php if ( $field->hideLabel ) echo $tooltip ?>
    </label>
</div>

