<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'input', 'input--block',
    ];

    $attrs = [
        'placeholder' => $field->placeholder, 'sx-touch-spin' => '',
    ];

    if ( $field->get('notBlank', false) === true ) {
        $attrs['required'] = 'required';
    }

    $attrs['default'] = $field->defaultValue ?: '';

    $data = array_merge([
        'id' => $field->getUnique(), 'class' => $classList,
    ], $attrs);

    echo '<div class="input-group">';

    if ( ! realempty($field->prepend) ) {
        echo '<div class="input-group-prepend"><span class="input-group-text">' . $field->prepend . '</span></div>';
    }

    echo $this->Form->text($field->getName(), $data);

    if ( ! realempty($field->append) ) {
        echo '<div class="input-group-append"><span class="input-group-text">' . $field->append . '</span></div>';
    }

    echo '</div>';

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>


<?
return;
$min = "0";
$max = "10000";
if ( empty($input['validationParam1']) && $input['validationParam1'] === '0' ) {
    $min = 0;
} else {
    if ( ! empty($input['validationParam1']) ) {
        $min = intval($input['validationParam1']);
    }
}

if ( empty($input['validationParam2']) && $input['validationParam2'] === '0' ) {
    $max = 0;
} else {
    if ( ! empty($input['validationParam2']) ) {
        $max = intval($input['validationParam2']);
    }
}

?>

<script type="text/javascript">
    $(function () {
        $("#<?=\Cake\Utility\Inflector::camelize($input['inputName'])?>").TouchSpin({
            decimals: 0,
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: <?=$min?>,
            max: <?=$max?>
        });
    });
</script>
