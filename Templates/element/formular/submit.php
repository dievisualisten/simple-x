<div class="col--1-1">
    <div class="formular__requiredtext text-center">
        <?php
            echo $this->Form->button($form->get('formular.btnname') ?: __('Abschicken'), [
                'class' => 'button button--primary button--submit', 'type' => 'submit'
            ]);
        ?>
    </div>
</div>
