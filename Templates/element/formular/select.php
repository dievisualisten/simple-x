<?php

use App\Library\DomainManager;
use App\Utility\ArrayUtility;

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item formular-item--select form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'chosen-pseudo',
    ];

    $attrs = [
        'required' => false, 'data-placeholder' => $field->placeholder ?: __('Bitte auswählen'), 'sx-chosen' => '',
    ];

    $attrs['default'] = $field->defaultValue ?: '';

    $data = array_merge([
        'id' => $field->getUnique(), 'class' => $classList,
    ], $attrs);

    $options = explode('|', $field->options);

    if ( count($options) === 1 && ! empty($options[0]) ) {
        $options = DomainManager::getDomain($field->options, null, false);
    }

    if ( count($options) !== 1 && ! empty($options[0]) ){
        $options = ArrayUtility::toAssoc($options);
    }

    // Append default to select options
    array_unshift($options, [-1 => '']);

    echo $this->Form->select($field->getName(), $options, $data);

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>
