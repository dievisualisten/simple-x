<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item formular-item--date form-group">
    <?php

    use App\Utility\StringUtility;

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'input', 'input--block',
    ];

    $datepicker = [
        'format' => 'L', 'display' => "#{$field->getUnique()}",
    ];

    $attrs = [
        'required' => false, 'placeholder' => $field->placeholder, 'sx-datepicker' => StringUtility::jsAttr($datepicker),
    ];

    $attrs['default'] = $field->defaultValue ?: '';

    $data = array_merge([
        'id' => "{$field->getUnique()}Display", 'class' => $classList,
    ], $attrs);

    echo '<div class="input-group">';

    if ( ! realempty($field->prepend) ) {
        echo '<div class="input-group-prepend"><span>' . $field->prepend . '</span></div>';
    }

    echo $this->Form->text("{$field->getName()}Display", $data);

    if ( ! realempty($field->append) ) {
        echo '<div class="input-group-append"><span>' . $field->append . '</span></div>';
    }

    echo '</div>';

    echo $this->Form->hidden($field->getName(), [
        'id' => $field->getUnique(),
    ]);

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>
