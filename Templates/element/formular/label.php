<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'showRequired' => true,
    'showTooltip' => true,
    'hideLabel' => $field->hideLabel,
    'notBlank' => $field->notBlank,
    'classList' => ['formular-item__label']
];

extract($defaults, EXTR_SKIP);

/**
 * @var bool $showRequired
 * @var bool $showTooltip
 * @var bool $hideLabel
 * @var bool $notBlank
 * @var array $classList
 */

if ( $hideLabel === true ) {
    return;
}

if ( $notBlank === true ) {
    $classList[] = 'required';
}

$tooltip = $this->element('formular/tooltip', [
    'form' => $form, 'field' => $field
]);

$required = $this->element('formular/required', [
    'form' => $form, 'field' => $field
]);

?>
<label class="<?php echo implode(' ', $classList); ?>" for="<?php echo $field->getUnique(); ?>">
    <span><?php echo $field->label; ?></span><?php if ( $showRequired ) echo $required ?><?php if ( $showTooltip ) echo $tooltip ?>
</label>
