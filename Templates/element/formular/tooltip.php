<?php

    if ( $field->tooltip === '' ) {
        return;
    }

?>
<span class="formular-item__tooltip" uk-tooltip="<?php echo $field->tooltip; ?>">
    <i class="fa fa-question-circle"></i>
</span>
