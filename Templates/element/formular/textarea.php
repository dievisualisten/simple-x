<?php if ( ! isset($form) || ! isset($field) ) return; ?>
<div class="formular-item formular-item--textarea form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'textarea', 'textarea--block'
    ];

    $attrs = [
        'placeholder' => $field->placeholder
    ];

    if ( $field->notBlank === true ) {
        $attrs['required'] = 'required';
    }

    $attrs['default'] = $field->defaultValue ?: '';

    $data = array_merge([
        'id' => $field->getUnique(), 'class' => $classList
    ], $attrs);

    echo '<div class="input-group">';

    if ( ! realempty($field->prepend) ) {
        echo '<div class="input-group__prepend"><span>' . $field->prepend . '</span></div>';
    }

    echo $this->Form->textarea($field->getName(), $data);

    if ( ! realempty($field->append) ) {
        echo '<div class="input-group__append"><span>' . $field->append . '</span></div>';
    }

    echo '</div>';

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field
    ]);

    echo '</div>';

    ?>
</div>
