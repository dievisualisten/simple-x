<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

if ( realempty($field->infotext) ) {
    return;
}

?>
<small class="formular-item__infotext form-text">
    <?php echo $field->infotext; ?>
</small>
