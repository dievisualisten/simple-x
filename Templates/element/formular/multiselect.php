<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\DomainManager;
use App\Utility\ArrayUtility;

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item formular-item--multiselect form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'form-control',
    ];

    $attrs = [
        'required' => false, 'data-placeholder' => $field->placeholder ?: __('Bitte auswählen'), 'sx-chosen' => '',
    ];

    $attrs['default'] = $field->defaultValue ?
        explode('|', $field->defaultValue) : false;

    $data = array_merge([
        'id' => $field->getUnique(), 'class' => $classList, 'multiple' => true,
    ], $attrs);

    $options = explode('|', $field->options);

    if ( count($options) === 1 && ! empty($options[0]) ) {
        $options = DomainManager::getDomain($field->options, null, false);
    }

    if ( count($options) !== 1 && ! empty($options[0]) ){
        $options = ArrayUtility::toAssoc($options);
    }

    array_unshift($options, [-1 => '']);

    echo $this->Form->select($field->getName(), $options, $data);

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>
