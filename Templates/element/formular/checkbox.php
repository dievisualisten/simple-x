<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\DomainManager;
use App\Utility\ArrayUtility;

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item formular-item--checkbox form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $options = array_filter(explode('|', $field->options));

    if ( count($options) === 1 && ! empty($options[0]) ) {
        $options = DomainManager::getDomain($field->options, null, false);
    }

    if ( count($options) !== 1 && ! empty($options[0]) ) {
        $options = ArrayUtility::toAssoc($options);
    }

    if ( empty($options) ) {
        $options = [$field->options ?: $field->placeholder ?: $field->label];
    }

    $classList = [
        'formular-item__checkbox',
    ];

    if ( count($options) === 1 ) {
        $classList[] = 'formular-item__checkbox--single';
    }

    echo '<div class="' . implode(' ', $classList) . '">';

    echo $this->Form->select($field->getName(), $options, [
        'multiple' => 'checkbox', 'required' => false, 'id' => $field->getUnique(),
    ]);

    echo '</div>';

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>
