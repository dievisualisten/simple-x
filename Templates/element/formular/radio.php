<?php

use App\Library\DomainManager;
use App\Utility\ArrayUtility;

if ( ! isset($form) || ! isset($field) ) {
    return;
}

?>
<div class="formular-item formular-item--radio form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field
    ]);

    echo '<div class="formular-item__body">';

    $options = array_filter(explode('|', $field->options));

    if ( count($options) === 1 && ! empty($options[0]) ) {
        $options = DomainManager::getDomain($field->options, null, false);
    }

    if ( count($options) !== 1 && ! empty($options[0]) ){
        $options = ArrayUtility::toAssoc($options);
    }

    if ( empty($options) ) {
        $options = [$field->options ?: $field->placeholder ?: $field->label];
    }

    $classList = [
        'formular-item__radio',
    ];

    if ( count($options) === 1 ) {
        $classList[] = 'formular-item__radio--single';
    }

    $data = [
        'type' => 'radio', 'required' => false, 'id' => $field->getUnique()
    ];

    echo '<div class="' . implode(' ', $classList) . '">';

    echo '<div class="radio">';
    echo $this->Form->radio($field->getName(), $options, $data);
    echo '</div>';

    echo '</div>';

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field
    ]);

    echo '</div>';

    ?>
</div>
