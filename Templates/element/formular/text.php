<?php if ( ! isset($form) || ! isset($field) ) {
    return;
} ?>
<div class="formular-item form-group">
    <?php

    echo $this->element('formular/label', [
        'form' => $form, 'field' => $field,
    ]);

    echo '<div class="formular-item__body">';

    $classList = [
        'formular-item__input', 'input', 'input--block',
    ];

    $attrs = [
        'required' => false, 'placeholder' => $field->placeholder,
    ];

    $attrs['default'] = $field->defaultValue ?: '';

    $config = array_merge([
        'id' => $field->getUnique(), 'class' => $classList,
    ], $attrs);

    echo '<div class="input-group">';

    if ( ! realempty($field->prepend) ) {
        echo '<div class="input-group-prepend"><span class="input-group-text">' . $field->prepend . '</span></div>';
    }

    echo $this->Form->text($field->getName(), $config);

    if ( ! realempty($field->append) ) {
        echo '<div class="input-group-append"><span class="input-group-text">' . $field->append . '</span></div>';
    }

    echo '</div>';

    echo $this->element('formular/infotext', [
        'form' => $form, 'field' => $field,
    ]);

    echo $this->element("formular/validation", [
        'form' => $form, 'field' => $field,
    ]);

    echo '</div>';

    ?>
</div>
