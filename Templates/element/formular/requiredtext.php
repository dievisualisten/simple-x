<?php

    $defaults = [
        'text' => __('Wir bitten Sie die mit einem * gekennzeichneten Felder auszufüllen. Vielen Dank!')
    ];

    extract($defaults, EXTR_SKIP);

?>

<div class="col--1-1">
    <div class="formular__requiredtext text-center">
        <p>
            <?php echo str_replace('*', '<span class="formular-item__required">*</span>' , $text); ?>
        </p>
    </div>
</div>
