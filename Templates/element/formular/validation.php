<?php if ( ! isset($field) ) return; ?>
<?php if ( $this->Form->isFieldError($field->getName()) ) : ?>
    <div class="fomular-item__error">
        <?php echo $this->Form->error($field->getName()); ?>
    </div>
<?php endif; ?>
