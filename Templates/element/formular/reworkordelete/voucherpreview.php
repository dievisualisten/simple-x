<div class="voucherpreview">
    <div class="voucherpreview-getpreview">
        <a href="javascript:Shop.getPreview(this)"><i
                class="sx-gift-1 i-2x"></i><?= __('Klicken Sie hier um eine Vorschau Ihres Gutscheines zu erstellen.') ?>
        </a>
    </div>
    <div class="voucherpreview-download">
        <a href="#" target="_blank"><i
                class="sx-download i-2x"></i><?= __('Jetzt Gutschein Vorschau als PDF herunterladen.'); ?></a>
    </div>
    <div class="voucherpreview-loader">
        <i class="sx-spin5 i-2x"></i> <?= __('Bitte haben Sie einen Moment Geduld. Die Gutschein Vorschau steht gleich für Sie bereit.'); ?>
    </div>
</div>
