<?
if (!isset($options)) {
    $options = [];
}

$defaults = [
    'cityInputName' => 'city',
    'countryInputName' => 'country',
];


extract($defaults, EXTR_SKIP);

$input['autocomplete'] = 'off';
$input['type'] = 'text';

echo $this->Form->input($field['inputName'], $input);

$this->append('scriptBottom');
?>
<script type="text/javascript">
    $(function () {

        var displayField = $("#<?=$formularId?> input[name='plz']");
        var countryField = $("#<?=$formularId?> select[name='country']");
        var cityField = $("#<?=$formularId?> input[name='<?=$cityInputName?>']");

        displayField.typeahead({
            displayText: function (item) {
                return item.plz;
            },
            afterSelect: function (item) {
                if (cityField.val() == '') {
                    cityField.val(item.city);
                }
            },

            source: function (query, process) {
                var country = countryField.val() || 'de';
                $.ajax({
                    url: '/domains/autocomplete',
                    data: {
                        model: 'plzs',
                        displayfield: 'plz',
                        keyfield: 'plz',
                        findAll: 'false',
                        ordered: true,
                        limit: 5,
                        keyvalue: false,
                        "extraconditions#country": country,
                        query: query
                    },
                    dataType: 'json'
                })
                    .done(function (response) {
                        return process(response.data);
                    });
            }
        });

    });
</script>
<?
$this->end();
?>
