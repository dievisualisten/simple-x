<script src="https://secure.pay1.de/client-api/js/ajax.js" type="text/javascript"></script>

<script type="text/javascript">

    function processPayoneResponse(response) {
        if (response.status == 'VALID') {
            $('#FormdataFakenumber').val('');
            $('#FormdataNumber').val(response.pseudocardpan);
            //console.log(response.pseudocardpan);

        } else {

            $('#FormdataNumber').val('');
            /*
             console.log(response.status);
             console.log(response.errorcode);
             console.log(response.errormessage);
             console.log(response.customermessage);
             */
        }
        document.forms[0].submit();
    }

    $("#formular").submit(function (event) {
        var me = this;
        var paymentmethod = $('input[name="data[Formdata][paymentmethod]"]:checked').val();

        if (paymentmethod == 'payonecreditcard') {
            var jqxhr = $.ajax({
                type: "POST",
                url: "/orders/getPaymentproviderHash",
                dataType: "json",
                data: {
                    type: 'payone'
                }
            });

            jqxhr.done(function (response) {

                var creditkarttypes = {
                    'visa': 'V',
                    'mc': 'M',
                    'amex': 'A',
                    'diners': 'D',
                    'jbc': 'J',
                    'maestrointernational': 'O',
                    'discover': 'C',
                    'cartebleue': 'B',
                }

                var data = {
                    request: 'creditcardcheck',
                    responsetype: 'JSON',
                    mode: '<?=PAYONE_MODE?>',
                    mid: '<?=PAYONE_MID?>',
                    aid: '<?=PAYONE_AID?>',
                    portalid: '<?=PAYONE_PID?>',
                    encoding: '<?=PAYONE_ENCODING?>',
                    storecarddata: 'yes',
                    hash: response.data,
                    cardholder: $('#FormdataHolder').val(),
                    cardpan: $('#FormdataFakenumber').val(),
                    cardtype: creditkarttypes[$('#FormdataCc').val()],
                    cardexpiremonth: $('#FormdataValiduntilMonth').val(),
                    cardexpireyear: $('#FormdataValiduntilYear').val(),
                    cardcvc2: $('#FormdataCvc').val(),
                    language: 'de'
                };

                var options = {
                    return_type: 'JSON',
                    callback_function_name: 'processPayoneResponse'
                };

                var request = new PayoneRequest(data, options);
                request.checkAndStore();

            }).fail(function () {
                alert('<?=__("Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einaml.")?>');
            });

            event.preventDefault();

        }

    });</script>
