<?
if (isset($field['showOnFieldName']) && !empty($field['showOnFieldName'])) {
    $values = explode('|', $field['showOnFieldValue']);
    $myFormDivSelector = '#' . $formularId . ' .' . $input['name'];
    $myFieldSelector = '#' . $formularId . ' *[name="' . $field['showOnFieldName'] . '"]';

    $fdF = 'fDF' . str_replace('-', '', Text::uuid());

    if (empty($field['showOnFieldComparator'])) {
        $field['showOnFieldComparator'] = '==';
    }
    $this->append('scriptBottom');
    ?>

    <script type="text/javascript">
        $(function () {
            function <?=$fdF?> () {
                var ii = $('<?=$myFieldSelector?>');
                if (ii.prop('type') == 'checkbox') {
                    var i = $('<?=$myFieldSelector?>:checked').length.toString();
                } else {
                    //radio?
                    var ina = $('<?=$myFieldSelector?>:checked');
                    var i = ina.length ? ina.val() : (ii.length ? ii.val() : '');
                }

                var comp = parseFloat(i) == 'NaN' ? i.toLowerCase() : parseFloat(i);

                <?


                foreach ($values as $v){
                $compareVal = is_numeric($v) ? $v : '"' . strtolower($v) . '"';
                ?>
                (comp <?=$field['showOnFieldComparator']?> <?=$compareVal?>) ||
                <?
                }
                ?>
                false ? $('<?=$myFormDivSelector?>').show() : $('<?=$myFormDivSelector?>').hide();
            }

            $('<?=$myFieldSelector?>').change(function () {
                <?=$fdF?>();
            });
            <?=$fdF?>();
        });
    </script>
    <?
    $this->end();
}
?>
