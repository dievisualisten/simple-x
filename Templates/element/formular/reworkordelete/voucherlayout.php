<?
function getVoucherLayouts()
{
    $folder = new Folder(WWW_ROOT . 'img/vouchers/');
    $contents = $folder->find();
    asort($contents);
    $layouts = [];
    foreach ($contents as $t) {
        $name = explode('.', $t);
        if (count($name) == 2 && $name[1] == 'jpg') {
            $layouts[$name[0]] = '<span class="voucherlayout-choose-text">' . __('Auswählen') . '</span><div class="voucherlayout-conatiner"><img src="/img/vouchers/' . $t . '" /><div class="voucherlayout-preview"><a class="fresco" target="_blank" href="/img/vouchers/preview/' . $t . '"><i class="sx-zoom-in"></i>' . __('Vorschau') . '</a></div></div>';
        }
    }

    return $layouts;
}

echo $this->Form->input($input['inputName'],
    ['label' => $input['label'], 'type' => 'radio', 'options' => getVoucherLayouts()]);

?>
