<script type="text/javascript">
    // Initialize the widget when the DOM is ready
    $(function () {
        $("#uploader").pluploadQueue({
            // General settings
            runtimes: 'html5,flash,silverlight,html4',
            url: '/upload.php',

            // Select multiple files at once
            multi_selection: false, // if set to true, html4 will always fail, since it can't select multiple files

            // Maximum file size
            max_file_size: '5mb',

            // User can upload no more then 20 files in one go (sets multiple_queues to false)
            max_file_count: 5,

            unique_names: true,


            chunks: {
                size: '1mb',
                send_chunk_number: false // set this to true, to send chunk and total chunk numbers instead of offset and total bytes
            },

            // Specify what files to browse for
            filters: [
                {title: "Image files", extensions: "jpg,jpeg,gif,png"},
                {title: "Zip files", extensions: "zip,avi"},
                {title: "Documents", extensions: "doc,docx,txt"}
            ],

            // Rename files by clicking on their titles
            rename: true,

            // Sort files
            sortable: true,

            // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
            dragdrop: true,

            // Views to activate
            views: {
                list: true,
                active: 'list'
            },

            // Flash settings
            flash_swf_url: '/js/simple-x/upload/Moxie.xap',
            // Silverlight settings
            silverlight_xap_url: '/js/simple-x/upload/Moxie.xap'
        });

        // Handle the case when form was submitted before uploading has finished
        $('form').submit(function (e) {
            var uploader = $('#uploader').pluploadQueue();
            if (uploader.files.length > 0) {
                uploader.bind('StateChanged', function () {
                    if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                        $('form')[0].submit();
                    }
                });
                uploader.start();
            } else {
                alert("You must have at least one file in the queue.");
            }
            return false;
        });
    });
</script>

<div id="uploader">

</div>
