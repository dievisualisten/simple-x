<?
echo $this->Form->input($input['inputName'], [
    'label' => $input['label'],
    'type' => 'date',
    'dateFormat' => 'MY',
    'minYear' => date('Y'),
    'monthNames' => false,
    'maxYear' => date('Y') + 10,
    'orderYear' => 'asc',
    'separator' => ' / ',
]);
?>
