<?
$input['label'] = \App\Library\__('formfield.newsletter.text');

if (!empty($input['infotext'])) {
    $input['originalLabel'] = __($input['label']);
    $input['label'] = $input['label'] . ' <span title = "' . __($input['infotext']) . '" class="sx-icon-info-kreis icon-info-field" data-toggle="tooltip"></span>';
}
echo $this->element('formfield-confirm', ['input' => $input, 'formtype' => $formtype]);
?>
