<div class="confirm-<?= $input['inputName'] ?>">
    <?= $this->Form->input($input['inputName'], [
        'label' => (empty($formtype) || !empty($input['labelaligntop'])) ? false : '&nbsp',
        'checkboxLabel' => $input['label'],
        'type' => 'checkbox',
    ]); ?>
    <!-- Modal -->
    <div class="modal fade" id="<?= $input['inputName'] ?>-dialog" data-backdrop="static" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <?
                    $text = \App\Library\__($input['inputName'] . '.text');
                    if (empty($text)) {
                        $text = $this->element('formfield-confirmtext-' . $input['inputName']);
                    }
                    echo $text;
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default cancel" aria-hidden="true"
                            data-dismiss="modal"><?= __('Abbrechen') ?></button>
                    <button type="button" class="btn btn-primary ok" aria-hidden="true"
                            data-dismiss="modal"><?= __('OK') ?></button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $(".confirm-<?=$input['inputName']?> .confirm-link").click(function () {
                $("#<?=$input['inputName']?>-dialog").appendTo("body").modal('show');
                return false;
            });

            $(".confirm-<?=$input['inputName']?> .ok").click(function () {
                $("#Formdata<?=\Cake\Utility\Inflector::camelize($input['inputName'])?>").attr('checked', true);
            });

            $(".confirm-<?=$input['inputName']?> .cancel").click(function () {
                $("#Formdata<?=\Cake\Utility\Inflector::camelize($input['inputName'])?>").attr('checked', false);
            });
        });
    </script>
</div>
