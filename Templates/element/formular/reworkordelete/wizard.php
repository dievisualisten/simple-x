<?
$wizardId = 'wizard' . \Tools\Utility\Text::uuid();
?>
<script type="text/javascript">

    $(document).ready(function () {

        $('form .formular-wizard-tab-start').each(function (index) {

            var tabTxt = $(this).nextUntil('form .formular-wizard-tab-end').find('h3').html();  // h3 auslesen
            var tab = '<li><a href="#tab' + (index + 1) + '" data-toggle="tab">' + tabTxt + '</a></li>'; // h3 als Tab hinzufügen
            $(tab).appendTo('ul.tabs');

            $(this).nextUntil('form .formular-wizard-tab-end').find('h3').parent().remove();    // h3 wiedder entfernen

            $(this).nextUntil('form .formular-wizard-tab-start').appendTo('.tab-content').wrapAll('<div class="tab-pane clearfix" id="tab' + (index + 1) + '"></div>');    // Abschnitt als neuen Tab hinzufügen

            $(this).remove();

        });

        // Wizard Initialisierung und Funktionen

        $('#rootwizard').bootstrapWizard({

            onTabClick: function (tab, navigation, index, clickIndex) {

                if (clickIndex > index) {
                    return false;
                }

            },
            onNext: function (tab, navigation, index) {

            },
            onPrevious: function (tab, navigation, index) {

            }
        }).promise().done(function () {

            $('#rootwizard').bootstrapWizard('unbindNext');

        });

        $('li.next').click(function () {

            checkInputs();

        });

    });
</script>

<div id="rootwizard" class="clearfix">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <ul class="tabs">

                </ul>
            </div>
        </div>
    </div>
    <div class="tab-content clearfix">
    </div>
    <ul class="pager wizard">
        <li class="previous first" style="display:none;"><a href="#">First</a></li>
        <li class="previous"><a href="#">Previous</a></li>
        <li class="next last" style="display:none;"><a href="#">Last</a></li>
        <li class="next"><a href="#">Next</a></li>
    </ul>
</div>
