<?php

use App\Utility\StringUtility;

/**
 * @version 4.0.0
 * @var \App\Model\Entity\Article $data
 */

$theme = menu()->getDomainRecord('theme', 'default');

$defaults = [
    'className' => 'directions',
    'items'     => $this->Article->markers($data->children),
    'lat'       => null,
    'lon'       => null,
    'address'   => $data->get('address', null) ?: menu()->getDomainRecord('title'),
    'zoom'      => 17,
    'stylePath' => WWW_ROOT . 'src/theme/' . $theme . '/map/styles.json',
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var array $items
 * @var float $lat
 * @var float $lon
 * @var string $address
 * @var float $zoom
 * @var string $stylePath
 */


if ( empty($lat) ) {
    $lat = $data->get('lat') ?: 53.565000;
}

if ( empty($lon) ) {
    $lon = $data->get('lon') ?: 10.042280;
}

$map = StringUtility::random(16);

$groups = $this->Article->groupByDomain(
    'custom.map.mapgroup', 'mapgroup', $items
);

?>

<?php if ( ! empty($address) && count($items) ) : ?>

    <?php
        echo $this->Html->script('//maps.googleapis.com/maps/api/js?key=' . GMAP_CLIENT_API_KEY, ['async' => true, 'block' => 'scriptBottom']);
        echo $this->Html->script( '/dist/bundle/js/markerclustererplus.min.js', ['async' => true, 'block' => 'scriptBottom']);
    ?>

    <div id="<?= $map ?>" class="<?= $className ?> <?= $className ?>--list">
        <div class="grid grid--row grid--wrap">

            <div class="<?= $className ?>__map col--1-1 col--1-2@sm col--2-3@md">
                <div><!-- Google Maps --></div>
            </div>

            <div class="<?= $className ?>__legend col--1-1 col--1-2@sm col--1-3@md">
                <div class="grid grid--col" uk-accordion="toggle: .<?= $className ?>-legend__title; content: .<?= $className ?>-legend__items; collapsible: false;">

                    <?php foreach ( $groups as $index => $group ) : ?>
                        <div class="<?= $className ?>-legend col--1-1 <?= ! $index ? 'uk-open' : '' ?>">

                            <div class="<?= $className ?>-legend__title">
                                <h4><a href="javascript:void(0)" data-map-group="<?= $group['domain'] ?>"><?= $group['title']; ?></a></h4>
                            </div>

                            <div class="<?= $className ?>-legend__items">
                                <?php foreach ( $group['items'] as $item ) : ?>
                                    <div class="<?= $className ?>-legend__item">
                                        <a href="javascript:void(0)" data-map-marker="<?= $item->id ?>"><?= $this->Fe->getTitle($item); ?></a>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                    <?php endforeach; ?>

                    <?php if ( false ) : ?>
                        <div class="<?= $className ?>-legend__reset col--1-1">
                            <a data-map-group="*"><?= __('Alle anzeigen'); ?></a>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
    <?php $this->append('scriptBottom'); ?>
    <script>
        (function ($) {
            pi.Dom.required(function () {

                if ( ! window.Maps ) {
                    window.Maps = {};
                }

                if ( ! window.Clusters ) {
                    window.Clusters = {};
                }

                $el = $('#<?= $map ?>');

                <?php if ( file_exists($stylePath) ) : ?>
                pi.Map.setMapStyle(<?= file_get_contents($stylePath) ?>);
                <?php endif; ?>

                var size = {
                    width: 30, height: 42,
                };

                pi.Map.setMarkerStyle('default', pi.Obj.assign(size, {
                    default: '/src/theme/<?=$theme?>/map/marker-default.svg',
                    hover: '/src/theme/<?=$theme?>/map/marker-hover.svg',
                    active: '/src/theme/<?=$theme?>/map/marker-active.svg'
                }));

                window.Maps['<?= $map ?>'] = new pi.Map($el.find('.<?= $className ?>__map').get(0), {
                    lat: pi.Any.float('<?= $lat ?>'), lng: pi.Any.float('<?= $lon ?>')
                });

                window.Maps['<?= $map ?>'].clusterMarkers({
                    imagePath: '/src/theme/<?=$theme?>/map/cluster-'
                });

                <?php if ( ! empty($lat) && ! empty($lon) ) : ?>

                let rootMarker = window.Maps['<?= $map ?>'].createMarker('<?= $data->id; ?>', {
                    lat: pi.Num.float('<?= $lat; ?>'), lng: pi.Num.float('<?= $lon; ?>'), html: '<?= $address ?>'
                });

                <?php endif; ?>

                var groupMarker = {};

                <?php foreach ( $items as $item ) : ?>

                var options = {
                    lat: pi.Num.float('<?= $item->lat; ?>'),
                    lng: pi.Num.float('<?= $item->lon; ?>')
                };

                <?php /** Html content which will be rendered in infowindow */; ?>
                options.html = '<?= $item->address; ?>';

                groupMarker['<?= $item->id ?>'] = window.Maps['<?= $map ?>'].createMarker('<?= $item->id; ?>', options);

                $('[data-map-marker="<?= $item->id ?>"]').on('click', function () {

                    $('[data-map-marker]').each(function (index, item) {
                        $(item).data('map-marker') === '<?= $item->id ?>' ?
                          $(item).addClass('active') : $(item).removeClass('active');
                    });

                    window.Maps['<?= $map ?>'].focusMarkers(function (item) {
                        return item.key === '<?= $item->id ?>';
                    });

                    window.Maps['<?= $map ?>'].toggleInfo('<?= $item->id; ?>');
                });

                $('[data-map-marker="<?= $item->id ?>"]').on('mouseenter', function () {
                    window.Maps['<?= $map ?>'].styleMarker('<?= $item->id; ?>', 'hover');
                });

                $('[data-map-marker="<?= $item->id ?>"]').on('mouseleave', function () {
                    window.Maps['<?= $map ?>'].styleMarker('<?= $item->id; ?>');
                });

                groupMarker['<?= $item->id ?>'].marker.addListener('click', function () {

                    $('[data-map-marker]').each(function (index, item) {
                        window.Maps['<?= $map ?>'].getInfoVisibility('<?= $item->id ?>') ?
                          $(item).addClass('active') : $(item).removeClass('active');
                    });

                });

                groupMarker['<?= $item->id ?>'].info.addListener('closeclick', function () {
                    $el.find('[data-map-marker="<?= $item->id ?>"]').removeClass('active');
                });

                <?php endforeach; ?>

                <?php foreach ( $groups as $index => $group ) : ?>

                $('[data-map-group="<?= $group['domain'] ?>"]').on('click', function () {

                    window.Maps['<?= $map ?>'].showMarkers(function (item) {

                        if ( rootMarker && rootMarker.key === item.key ) {
                            return true;
                        }

                        return pi.Arr.includes(<?= json_encode($group['ids'], true) ?>, item.key);
                    });

                    $('[data-map-marker]').removeClass('active');

                    window.Maps['<?= $map ?>'].focusMarkers();
                });

                <?php if ( ! $index ) : ?>
                $('[data-map-group="<?= $group['domain'] ?>"]').trigger('click');
                <?php endif; ?>

                <?php endforeach; ?>

                $('[data-map-group="*"]').click(function () {
                    window.Maps['<?= $map ?>'].showMarkers();
                    window.Maps['<?= $map ?>'].focusMarkers();
                });

                window.Maps['<?= $map ?>'].focusMarkers();
            }, ['google']);
        })(jQuery);
    </script>
    <?php $this->end(); ?>
<?php endif; ?>
