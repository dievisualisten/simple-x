<?php

use App\Utility\StringUtility;

/**
 * @version 4.0.0
 *
 * @var \App\Model\Entity\Article $data
 */

$theme = menu()->getDomainRecord('theme', 'default');

$defaults = [
  'className' => 'directions',
  'lat'       => null,
  'lon'       => null,
  'address'   => $data->get('address', null),
  'zoom'      => 17,
  'stylePath' => WWW_ROOT . 'src/theme/' . $theme . '/map/styles.json',
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var float $lat
 * @var float $lon
 * @var string $address
 * @var float $zoom
 * @var string $stylePath
 */

if ( empty($lat) ) {
    $lat = $data->get('lat') ?: 53.565000;
}

if ( empty($lon) ) {
    $lon = $data->get('lon') ?: 10.042280;
}

$map = StringUtility::random(16);

?>

<?php if ( ! empty($address) ) : ?>

    <?php
        echo $this->Html->script('//maps.googleapis.com/maps/api/js?key=' . GMAP_CLIENT_API_KEY, ['async' => true, 'block' => 'scriptBottom']);
        echo $this->Html->script('/dist/bundle/js/markerclustererplus.min.js', ['async' => true, 'block' => 'scriptBottom']);
    ?>

    <div id="<?= $map ?>" class="<?= $className ?>">

        <div class="<?= $className ?>__map">
            <div><!-- Google Maps --></div>
        </div>

        <div class="container">

            <form class="<?= $className ?>__form">
                <div class="grid grid--col grid--10-10">

                    <div class="col--1-1">
                        <h3><?= __('Anfahrt'); ?></h3>
                    </div>

                    <div class="col--1-1">
                        <input class="<?= $className ?>__input input" type="text" placeholder="<?= __('Startadresse') ?>">
                    </div>

                    <div class="<?= $className ?>__error col--1-1">
                        <span><?= __('Bitte geben Sie eine gültige Adresse ein'); ?></span>
                    </div>

                    <div class="col--1-1">
                        <button type="submit" class="<?= $className ?>__submit button button--primary"><?= __('Berechnen') ?></button>
                    </div>

                </div>

            </form>


            <div class="<?= $className ?>__panel" style="display: none;">
                <!-- Google Directions -->
            </div>

        </div>

    </div>

    <?php $this->append('scriptBottom'); ?>

    <script>
        (function ($) {
            pi.Dom.required(function () {

                if ( !window.Maps ) {
                    window.Maps = {};
                }

                $el = $('#<?= $map ?>');

                <?php if ( file_exists($stylePath) ) : ?>
                    pi.Map.setMapStyle(<?= file_get_contents($stylePath) ?>);
                <?php endif; ?>

                var size = {
                    width: 30, height: 42,
                };

                pi.Map.setMarkerStyle('default', pi.Obj.assign(size, {
                    default: '/src/theme/<?=$theme?>/map/marker-default.svg',
                    hover: '/src/theme/<?=$theme?>/map/marker-hover.svg',
                    active: '/src/theme/<?=$theme?>/map/marker-active.svg'
                }));

                window.Maps['<?= $map ?>'] = new pi.Map($el.find('.<?= $className ?>__map').get(0), {
                    lat: pi.Any.float('<?= $lat ?>'), lng: pi.Any.float('<?= $lon ?>')
                });

                <?php if ( ! empty($lat) && ! empty($lon) ) : ?>
                    window.Maps['<?= $map ?>'].createMarker('<?= $data->id; ?>', {
                        lat: pi.Num.float('<?= $lat; ?>'), lng: pi.Num.float('<?= $lon; ?>'), html: '<?= $address ?>'
                    });
                <?php endif; ?>

                $el.find('.<?= $className ?>__form').submit(function (event) {

                    event.preventDefault();
                    event.stopPropagation();

                    var origin = $('#<?= $map ?>').find('.<?= $className ?>__input').val();

                    var success = function () {
                        $('#<?= $map ?>').find('.<?= $className ?>__error').hide();
                        $('#<?= $map ?>').find('.<?= $className ?>__panel').show();

                        window.Maps['<?= $map ?>'].hideMarker('<?= $data->id; ?>');
                    };

                    var error = function () {
                        $('#<?= $map ?>').find('.<?= $className ?>__error').show();
                        $('#<?= $map ?>').find('.<?= $className ?>__panel').hide();

                        window.Maps['<?= $map ?>'].showMarker('<?= $data->id; ?>');
                    };

                    window.Maps['<?= $map ?>'].renderDirections({
                        origin: origin, destination: '<?= $address ?>', panel: $('#<?= $map ?>').find('.<?= $className ?>__panel').get(0)
                    }).then(success, error);

                });

            }, ['google']);
        })(jQuery);
    </script>

    <?php $this->end(); ?>
<?php endif; ?>
