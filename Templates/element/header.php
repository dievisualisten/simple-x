<?php

use App\Library\Facades\Menu;

/**
 * @version 4.0.0
 */

/**
 * @var array $menumain
 */

 ?>
<header class="header">
    <div class="container">
        <div class="grid grid--row grid--middle grid--20">

            <div class="header__logo col--flex-0-0 col--left@md">
                <a href="<?= Menu::getHomeUrl() ?>" title="<?= Menu::getDomainRecord('title') ?>">
                    <img src="" alt=""> <?= Menu::getDomainRecord('title') ?>
                </a>
            </div>

            <div class="header__navigation col--flex-0-1 col--left@md">
                <?= $this->Tree->generate($menumain, ['alias' => 'title', 'maxDepth' => 0, 'element' => 'system/menuitem']) ?>
            </div>

            <div class="header__search col--flex-0-0">
                <?= $this->element('search/frontendsearch'); ?>
            </div>

            <div class="header__language col--flex-0-0">
                <?= $this->element('system/language'); ?>
            </div>

        </div>

    </div>
</header>
