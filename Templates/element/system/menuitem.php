<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Menu $data
 */

use App\Library\AuthManager;
use App\Library\Facades\Request;

$defaults = [
    'classList'          => [],
    'firstChild'         => false,
    'lastChild'          => false,
    'hasChildren'        => false,
    'hasVisibleChildren' => false,
];

extract($defaults, EXTR_SKIP);

/**
 * @var array $classList
 * @var bool $firstChild
 * @var bool $lastChild
 * @var bool $hasChildren
 * @var bool $hasVisibleChildren
 */

$urlPath = $data->get('path');

if ( ! AuthManager::isUrlAllowed($urlPath) ) {
    return;
}

if ( Request::url(true) === $urlPath ) {
    $classList[] = 'current';
}

if ( strpos(Request::url(true), $urlPath) !== false ) {
    $classList[] = 'active';
}

if ( $firstChild ) {
    $classList[] = 'first';
}

if ( $lastChild ) {
    $classList[] = 'last';
}

if ( $hasChildren ) {
    $classList[] = 'has-children';
}

if ( $hasVisibleChildren ) {
    $classList[] = 'has-visible-children';
}

$this->Tree->addItemAttribute('class', implode(' ', $classList));

?>
<?= $this->Article->openLink($data); ?>
<?= $this->Fe->getTitle($data); ?>
<?= $this->Article->closeLink($data); ?>
