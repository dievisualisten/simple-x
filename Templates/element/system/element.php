<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

if ( ! $data->isEmpty('custom.custom.element') ) {
    echo $this->element($data->get('custom.custom.element'));
}

