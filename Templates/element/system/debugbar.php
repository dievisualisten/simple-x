<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

?>
<?php if ( config('debugbar') === true && config('env') === 'local' ) : ?>
    <div style="height: 50px; position: fixed; bottom: 0; background: #D33C44;"></div>
<?php endif; ?>
