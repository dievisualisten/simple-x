<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\DomainManager;
use App\Library\Facades\Language;
use App\Library\Facades\Menu;

/**
 * @version 4.0.0
 */

$defaults = [
    'showCurrent' => true,
    'showNames' => false,
    'showIcons' => false,
    'iconPath' => '/dist/languages/%s.png',
];

extract($defaults, EXTR_SKIP);

/**
 * @var bool $showCurrent
 * @var bool $showNames
 * @var bool $showIcons
 * @var bool $iconPath
 */

$locales = Language::getAllowedLanguages();

if ( ! $showCurrent ) {
    $locales = array_diff($locales, [Language::getLanguage()]);
}

// Get current menurecord
$menuLocales = Menu::getFrontpageRecord()
    ->getExistingLocales();

if ( ! empty($menuRecord = Menu::getMenuRecord()) ) {
    $menuLocales = $menuRecord->getExistingLocales();
}

// Get frontpage record
$frontpageLocales = Menu::getFrontpageRecord()
    ->getExistingLocales();

$locales = array_filter(array_keys($frontpageLocales), function ($locale) use ($locales) {
    return in_array($locale, $locales);
});

?>

<?php if ( config('Sx.app.language.multilingual') && count($locales) > 1 ) : ?>
    <div class="language">
        <ul>
            <?php foreach ( $frontpageLocales as $locale => $frontpage ) : ?>
                <?php
                    $localizedLink = null;

                    if ( isset($frontpageLocales[$locale]) ) {
                        $localizedLink = $this->Article->url($frontpageLocales[$locale]);
                    }

                    if ( isset($menuLocales[$locale]) ) {
                        $localizedLink = $this->Article->url($menuLocales[$locale]);
                    }

                    if ( empty($localizedLink) ) {
                        continue;
                    }
                ?>
                <li>
                    <?php if ( Language::getLanguage() === $locale ) : ?>
                        <span>
                            <?= $showNames ? DomainManager::get('language', $locale, $locale) : $locale ?>
                        </span>
                    <?php else : ?>
                        <a href="<?= $localizedLink; ?>" rel="alternate" hreflang="<?= $locale ?>">
                            <?= $showNames ? DomainManager::get('language', $locale, $locale) : $locale ?>
                        </a>
                    <?php endif; ?>
                </li>

            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
