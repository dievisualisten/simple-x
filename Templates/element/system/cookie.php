<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

?>
<div class="cookie" sx-cookie="toggle: .cookie__toggle">
    <div class="cookie__inner">
        <div class="grid grid--row grid--wrap grid--10-10">
            <div class="col--1-1">
                <p><?php echo __('Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.') ?></p>
            </div>
            <?php if ( true === true ) : ?>
                <div class="cookie__config col--1-1 hide">
                    <div class="form-group form-check checkbox">
                        <label class="form-check-label">
                            <input class="form-check-input" name="cookie.required" type="checkbox" checked disabled>
                            <?php echo __('Notwendig') ?>
                        </label>
                    </div>
                    <div class="form-group form-check checkbox">
                        <label class="form-check-label">
                            <input class="form-check-input" name="cookie.performance" type="checkbox" checked>
                            <?php echo __('Performance') ?>
                        </label>
                    </div>
                    <div class="form-group form-check checkbox">
                        <label class="form-check-label">
                            <input class="form-check-input" name="cookie.statistics" type="checkbox" checked>
                            <?php echo __('Statistik') ?>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col--1-1">
                <a class="cookie__toggle button button--block button--primary" href="javascript:void(0)">
                    <?php echo __('Akzeptieren') ?>
                </a>
            </div>
        </div>
    </div>
</div>
