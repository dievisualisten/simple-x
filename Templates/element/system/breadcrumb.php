<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

use App\Library\Facades\Menu;

$defaults = [
    'className' => 'breadcrumb',
    'showHome'  => true,
    'hereText'  => __('Du bist hier'),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $className
 * @var bool $showHome
 * @var string $hereText
 */

?>

<nav class="<?= $className ?>">
    <div class="container">
        <ol class="grid grid--row grid--wrap grid--middle">

            <?php if ( ! realempty($hereText) ) : ?>
                <li class="<?= $className ?>__item <?= $className ?>__item--here col--flex-0">
                    <span><?= $hereText ?></span>
                </li>
            <?php endif; ?>

            <?php if ( $showHome ) : ?>
                <li class="<?= $className ?>__item <?= $className ?>__item--home col--flex-0">
                    <?= $this->element('system/menuitem', ['data' => Menu::getFrontpageRecord()]) ?>
                </li>
            <?php endif; ?>

            <?php foreach ( Menu::getBreadcrumbs() as $breadcrumb ) : ?>
                <?php if ( ! Menu::isHomeUrl($breadcrumb->path, true) ) : ?>
                    <li class="<?= $className ?>__item col--flex-0">
                        <?= $this->element('system/menuitem', ['data' => $breadcrumb]) ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>

        </ol>
    </div>
</nav>
