<?php
if ( ! AuthManager::getPerms('rights.resource.modules.admintoolbar.show') ) {
    return;
}

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Menu $data
 */

$defaults = [
    'pagetype' => data_get($data, 'type'),
    'article' => data_get($data, 'id'),
    'formular' => data_get($data, 'formularconfig.formular.id'),
    'layout' => data_get($data, 'layout'),
];

extract($defaults, EXTR_SKIP);

/**
 * @var string $pagetype
 * @var string $article
 * @var mixed $formular
 * @var mixed $layout
 */

use App\Library\AuthManager; ?>

<div class="systembar">
    <div class="container">
        <div class="grid grid--row grid--middle">
            <div class="systembar-pagetype">
                <h4><?= config("Sx.resource.modules.article.pagetypes.{$pagetype}.name") . ' / ' . config("Sx.resource.modules.article.pagetypes.{$pagetype}.{$layout}.name"); ?></h4>
            </div>
            <ul class="col col--left">
                <li class="systembar-icon--dashboard">
                    <?php if ( AuthManager::getPerms('controllers.system') ) : ?>
                        <a target="_blank" href="<?= $this->Article->link('system'); ?>">
                            <?= __d('system', 'Dashboard'); ?>
                        </a>
                    <?php endif; ?>
                </li>
                <?php if ( $article && AuthManager::getPerms('rights.resource.modules.article.show') ) : ?>
                    <li class="systembar-icon--article">
                        <a target="_blank" href="<?= $this->Article->link('system/articles/edit/%s', $article); ?>">
                            <?= __d('system', 'Article'); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if ( $formular && AuthManager::getPerms('rights.resource.modules.formular.show') ) : ?>
                    <li class="systembar-icon--formular">
                        <a target="_blank" href="<?= $this->Article->link('system/formulars/edit/%s', $formular); ?>">
                            <?= __d('system', 'Formular'); ?>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            <ul class="col col--right">
                <?php if ( $article && AuthManager::getPerms('rights.resource.modules.attachment.show') ) : ?>
                    <li class="systembar-icon--attachment">
                        <a target="_blank" href="<?= $this->Article->link('system/attachments'); ?>">
                            <?= __d('system', 'Medien'); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if ( AuthManager::getPerms('rights.resource.modules.configuration.show') ) : ?>
                    <li class="systembar-icon--configuration">
                        <a target="_blank" href="<?= $this->Article->link('system/configurations'); ?>">
                            <?= __d('system', 'Konfiguration'); ?>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="systembar-spacer">
    <!-- Spacer -->
</div>
