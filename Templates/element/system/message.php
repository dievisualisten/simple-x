<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\Menu $data
 */

$mapping = [
    'error' => 'error', 'warning' => 'warning', 'success' => 'success', 'info' => 'info',
];

$messages = $this->Flash->getView()->getRequest()->getSession()->read('Flash');

?>
<?php if ( ! empty($messages) ) : ?>
    <div class="messages">
        <?php foreach ( $messages as $key => $messageType ) : ?>


            <?php $this->Flash->getView()->getRequest()->getSession()->delete("Flash." . $key); ?>

            <?php foreach ( $messageType as $message ) : ?>

                <div onclick="this.classList.add('hidden')" class="message message--<?= data_get($mapping, $key, $key) ?>">
                    <h3><?= $message['params']['msgTitel'] ?></h3>
                    <p>
                        <?= $message['message'] ?>
                    </p>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
