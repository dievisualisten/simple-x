<?php

/**
 * @version 4.0.0
 */

/**
 * @var array $menufooter
 */

?>
<footer class="footer">
    <div class="container">
        <div class="grid grid--row grid--middle grid--20">

            <div class="footer__navigation col--flex-0-1 col--center@md">
                <?= $this->Tree->generate($menufooter, [
                    'alias' => 'title', 'maxdepth' => 0, 'element' => 'system/menuitem'
                ]) ?>
            </div>

        </div>

    </div>
</footer>
