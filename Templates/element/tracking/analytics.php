<?php
$this->append('scriptBottom');

$trackingId = menu()->getDomainRecord('extended_data_cfg.menu.google_analytics_id');
if ( ! empty($trackingId) ) { ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $trackingId ?>"></script>

    <script>

        var disableStr = 'ga-disable-<?=$trackingId?>';
        if ( document.cookie.indexOf(disableStr + '=true') > - 1 ) {
            window[disableStr] = true;
        }

        $(function () {

            $('.gaoptout').click(function () {
                gaOptout();
            });

            function gaOptout()
            {
                document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableStr] = true;
                alert('Google Analytics wurde deaktiviert.');
            }
        });



        window.dataLayer = window.dataLayer || [];

        function gtag()
        {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '<?=$trackingId?>', {
            'anonymize_ip': true,
            'linker': {
                'domains': [
                    'onepagebooking.com',
                    'cbooking.com',
                    'vbooking.com',
                    '<?= request()->domain()?>',
                ]
            }
        });

    </script>
    <?php
}
$this->end();
?>
