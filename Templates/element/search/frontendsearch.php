<?php

/**
 * @version 4.0.0
 */

$inputDefault = [
    'placeholder' => __('Suchbegriff'), 'class' => 'input input--block'
];

$buttonDefault = [
    'value' => __('Suchen'), 'class' => 'button button--primary'
];

$defaults = [
    'input'   => [],
    'button'  => [],
    'enabled' => true, /* $data->get('extended_data_cfg.search.enable', false), */
    'formTargetUrl' => request()->fullUrl(false)
];

extract($defaults, EXTR_SKIP);

/**
 * @var array $input
 * @var array $button
 * @var bool $enabled
 * @var string $formTargetUrl
 */

$sessionKey = 'Visitor.frontendsearch.' . menu()->getMenuRecord('id');

$input = array_merge($inputDefault, $input, [
     'value' => request()->session()->read($sessionKey)
]);

$button = array_merge($buttonDefault, $button, [
    // Override something
]);



?>
<?php if ($enabled) : ?>
    <div class="frontendsearch">

        <?= $this->Form->create(null, [
            'url' => $formTargetUrl
        ]); ?>

        <?= $this->Form->hidden('search.performed', [
            'value' => true
        ]) ?>

        <?= $this->Form->hidden('search.methode', [
            'value' => 'fulltext'
        ]) ?>

        <?= $this->Form->hidden('search.model', [
            'value' => 'Articles'
        ]) ?>

        <div class="button-group">
            <?= $this->Form->input('search.fields.query', $input); ?>
            <?= $this->Form->button($button['value'], $button); ?>
        </div>

        <?= $this->Form->end(); ?>

    </div>
<?php endif; ?>
