<?
if (!isset($options)) {
    $options = [];
}

$defaults = [
    'button_text' => __('Suchen'),
    'search_single_domain' => true,
];


extract($defaults, EXTR_SKIP);
?>
<div class="search-bar floatbox">
    <?
    $prefix = '';
    if (Cake\Core\Configure::read('Config.language') != Cake\Core\Configure::read('Sx.app.language.defaultlanguage')) {
        $prefix = '/' . Cake\Core\Configure::read('Config.language');
    }

    echo $this->Form->create('Frontendsearch', ['url' => request()->fullUrl(true)]);

    $lastSessionValues = $this->Session->read('Visitor.frontendsearch.' . menu()->getMenuRecord('id'));

    ?>
    <div class="search-left text">
        <input class="search-input" value="<?= @$lastSessionValues['search'] ?>" name="data[Frontendsearch][search]"
               type="text"/>
    </div>
    <div class="search-left text">
        <input class="search-input" value="<?= @$lastSessionValues['location'] ?>" name="data[Frontendsearch][location]"
               type="text"/>
        <input class="search-input" value="<?= @$lastSessionValues['distance'] ?>" name="data[Frontendsearch][distance]"
               type="text"/>
        <input class="search-input" value="<?= @$lastSessionValues['country'] ?>" name="data[Frontendsearch][country]"
               type="text"/>
    </div>
    <div class="search-left text">
        <input class="search-input" value="<?= @$lastSessionValues['originaldata.Article.type'] ?>"
               name="data[Frontendsearch][originaldata.Article.type]" type="text"/>
    </div>
    <div class="search-right">
        <?= $this->Form->submit($button_text); ?>
    </div>
    <?= $this->Form->end(); ?>
</div>
