<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'className'   => 'teaser',
    'itemsPerRow' => 1,
    'items'       => $this->Article->matrix(4, $data->children),
];

extract($defaults, EXTR_SKIP);

?>
<?php if ( ! empty($items) && count($items) ) : ?>
    <section class="<?= $className ?> <?= $className ?>--list">
        <div class="<?= $className ?>__group">
            <div class="grid grid--row grid--wrap grid--30-30">
                <?php foreach ( $this->Article->paginate($items) as $item ) : ?>
                    <div class="animate col--1-1">
                        <?= $this->element('items/teaser-item-large', compact('data', 'item', 'className')); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?= $this->element('items/list-pagination', compact('items', 'className')); ?>
    </section>
<?php endif; ?>
