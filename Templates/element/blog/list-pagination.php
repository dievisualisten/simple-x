<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

$defaults = [
    'className'   => 'teaser',
    'items'       => null,
];

extract($defaults, EXTR_SKIP);

$defaultPages = [
    'first', 'prev', 'next', 'last'
];

/**
 * @var array $items
 */

?>

<?php if ( ! empty($items) && count($items) ) : ?>
    <div class="<?= $className ?>__pagination">
        <div class="grid grid--row grid--center grid--10">
            <?php foreach ( $this->Article->pages($items) as $page => $options ) : ?>
                <?php if ( in_array($page, $defaultPages) ) : ?>
                    <div class="col--flex-0-0">
                        <a class="button button--primary" href="<?= $options['url']; ?>" <?= $options['disabled'] ? 'disabled' : '' ?>>
                            <?= data_get($options, 'title', $page); ?>
                        </a>
                    </div>
                <?php else : ?>
                    <div class="col--flex-0-0">
                        <a class="button button--secondary" href="<?= $options['url']; ?>" <?= $options['disabled'] ? 'disabled' : '' ?>>
                            <?= data_get($options, 'title', $page); ?>
                        </a>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
