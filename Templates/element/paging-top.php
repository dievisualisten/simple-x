<?
if (!empty($list)) {
    $pagingParams = $this->Paginator->params();

    if (!empty($pagingParams['pageCount'] > 1)) {
        ?>
        <div class="paging-top clearfix">
            <?= $this->Paginator->counter(__('Seite {{page}} von {{pages}}')); ?>
        </div>
        <?
    }
}
?>
