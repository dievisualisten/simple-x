<?
if (empty($options)) {
    $options = [
        'top_text' => __('Zur Übersicht', true),

    ];
}
$defaults = ['show_text' => true, 'show_next' => true, 'img' => 'false', 'folder' => 's', 'show' => 'all'];

extract($defaults, EXTR_SKIP);
?>
<? if ($show_top && !empty($neighbors['top'])) { ?>
    <div class="page-neighbors-top">
        <?
        if ($show_top) {
            ?>
            <div class="page-neighbors-headline">
                <?= $top_text ?>
            </div>
            <?
        }
        ?>
        <?
        if ($img) {
            echo $this->element('img', [
                'link' => $neighbors['top']['Menu']['path'],
                'folder' => $folder,
                'show' => $show,
                'data' => $neighbors['top'],
            ]);
        }
        ?>
        <h3><?= $this->Fe->link($neighbors['top']); ?></h3>
    </div>
<? } ?>
