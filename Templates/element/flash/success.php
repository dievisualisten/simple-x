<?php

/**
 * @version 4.0.0
 * @var \App\View\ProtoView $this
 * @var \App\Model\Entity\AppEntity $data
 */

/**
 * @var array $params
 * @var string $message
 */

if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}

?>
<div class="message success" onclick="this.classList.add('hidden')"><?= $message ?></div>
