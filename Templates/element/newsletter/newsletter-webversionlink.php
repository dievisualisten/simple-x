<?
use App\Library\ServiceManager;

$defaults = [
    'webversion' => false,
];

extract($defaults, EXTR_SKIP);

/** @var bool $webversion */
/** @var string $base_href */

/** @var \App\Model\Entity\Newsletter $newsletter */

if ($webversion) {
    $link = $base_href . '/newsletters/webview/' . $newsletter->id;
} else {
    $link = request()->getDomainFromPath($newsletter->menu);
}

?>
<a href="<?= $link ?>"><?= __('Wird dieser Newsletter nicht richtig angezeigt, klicken Sie bitte hier.'); ?></a>
