<?

$defaults = [

];

extract($defaults, EXTR_SKIP);
?>


<!-- titleimg -->
<tr>
    <td align="center" valign="top">
        <table width="<?= $table_width ?>" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="responsive-img" align="center"><?= $this->element('img', [

                            'data' => $data,
                            'lazyLoad' => false,
                            'link' => false,
                            'wrap' => false,
                            'width' => $table_width,
                            'format' => '2_1',
                            'pick' => ['titleimg'],

                    ]); ?>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- titleimg ENDE -->


<!-- Content -->
<tr>
    <td class="content">
        <table class="newsletter-content" width="<?= $table_width ?>" border="0" cellspacing="0" cellpadding="0">

            <tr>
                <td><?= $this->element('newsletter-spacer', ['height' => 25]); ?></td>
            </tr>
            <tr>
                <td align="center" valign="top" class="content">
                    <h3><?= $this->Fe->link($data); ?></h3>
                </td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer', ['height' => 8]); ?></td>
            </tr>
            <tr>
                <td align="center" valign="top" class="content"><?= $this->element('content',
                        ['data' => $data->article]); ?></td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer',  ['height' => 25]); ?></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Content Ende -->

<?= $this->element('newsletter-trenner', ['folder' => $folder, 'content_width' => $content_width]); ?>
