<?

$defaults = [

];

extract($defaults, EXTR_SKIP);
?>

<!-- Logo -->
<tr>
    <td class="content" align="center" valign="top">
        <table width="<?= $table_width ?>" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><?= $this->element('newsletter-spacer',  ['height' => 25]); ?></td>

            </tr>
            <tr>
                <td align="center">
                    <a href="<?= $base_href ?>">
                        <img class="responsive-img"
                             src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/titleimg.jpg" width="520"
                             height="105" alt=""/>
                    </a>
                </td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer',['height' => 25]); ?></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Logo ENDE -->

<!-- titleimg -->
<tr>
    <td align="center" valign="top" class="title-img">
        <?= $this->element('img', [

                'lazyLoad' => false,
                'link' => true,
                'linkoptions' => ['url' => $base_href],
                'wrap' => false,
                'width' => $table_width,
                'format' => '2_1',
                'pick' => ['titleimg'],

        ]); ?>
    </td>
</tr>
<!-- titleimg ENDE -->

<!-- Content -->
<tr>
    <td class="content">
        <table width="<?= $table_width ?>" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><?= $this->element('newsletter-spacer', ['height' => 25]); ?></td>
            </tr>
            <tr>
                <td align="center" valign="top"
                    class="content"><?= $this->element('newsletter-anrede',
                        ['newsletterrecipient' => @$newslettercampaign->newsletterrecipient]) ?><?= $this->element('article/content'); ?></td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer', ['height' => 25]); ?></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Content Ende -->

