<?


$defaults = [
    'table_width' => '',
];


extract($defaults, EXTR_SKIP);

?>

<tr>
    <td valign="top">
        <table width="<?= $table_width ?>" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <!-- linke Spalte-->
                <td width="200" class="responsive-img" align="center" valign="top">
                    <?= $this->element('img', [

                            'data' => $data,
                            'lazyLoad' => false,
                            'link' => true,
                            'width' => 200,
                            'wrap' => false,
                            'format' => '1_1',
                            'pick' => ['teaser'],
                            'imgoptions' => [
                                'width' => 200,
                                'height' => 200,
                            ],

                    ]); ?>
                </td>
                <!-- linke Spalteg ENDE -->

                <!-- rechte Spalte -->
                <td width="320" class="content" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><?= $this->element('newsletter-spacer', ['height' => 25]); ?></td>
                        </tr>
                        <tr>
                            <td align="center" valign="center" class="teaserheadline">
                                <h3>
                                    <?= $this->Fe->link($data); ?>
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td><?= $this->element('newsletter-spacer',  ['height' => 8]); ?></td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" class="content">
                                <?= $this->element('article/teasertext',
                                     ['showMore' => false, 'data' => $data]); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= $this->element('newsletter-spacer', ['height' => 8]); ?></td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <?= $this->element('moretext', ['data' => $data]); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= $this->element('newsletter-spacer',  ['height' => 25]); ?></td>
                        </tr>
                    </table>
                </td>
                <!-- rechte Spalte ENDE-->
            </tr>
        </table>
    </td>
</tr>
<?= $this->element('newsletter-trenner', ['folder' => $folder, 'content_width' => $content_width]); ?>
