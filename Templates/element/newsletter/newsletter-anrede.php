<?



$defaults = [
    'anrede' => __('Sehr geehrte Damen und Herren'),
    'gruss' => __('Sehr geehrte/r'),
];


extract($defaults, EXTR_SKIP);

$temp_anrede = $anrede;

if (!empty($newsletterrecipient)) {

    if (!empty($newsletterrecipient->salutation)) {
        $temp_anrede = $newsletterrecipient->salutation;
    } else {

        if (!empty($newsletterrecipient->Person->salutation) && !empty($newsletterrecipient->Person->lastname)) {
            $temp_anrede = $newsletterrecipient->Person->salutation;

            if (!empty($newsletterrecipient->Person->firstname)) {
                $temp_anrede = $temp_anrede . ' ' . $newsletterrecipient->Person->firstname;
            }

            $temp_anrede = $temp_anrede . ' ' . $newsletterrecipient->Person->lastname;

            if (!empty ($temp_anrede)) {
                $temp_anrede = trim($gruss . ' ' . $temp_anrede);
            }
        }
    }
}

echo '<span class="newsletter-anrede">' . $temp_anrede . '</span>,<br/>';
