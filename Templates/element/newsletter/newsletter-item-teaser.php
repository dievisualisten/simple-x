<?


$defaults = [

];


extract($defaults, EXTR_SKIP);

?>

<tr>
    <td class="responsive-img" align="center"><?= $this->element('img', [

                'data' => $data,
                'lazyLoad' => false,
                'link' => true,
                'width' => 200,
                'wrap' => false,
                'format' => '2_1',
                'pick' => ['teaser'],
                'imgoptions' => [
                    'width' => 520,
                    'height' => 260,
                ],

        ]); ?>
    </td>
</tr>
<tr>
    <td class="content" valign="top" width="<?= $content_width ?>">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><?= $this->element('newsletter-spacer',['height' => 25]); ?></td>
            </tr>
            <tr>
                <td align="center" valign="center" class="teaserheadline">
                    <h3><?= $this->Fe->link($data); ?></h3>
                </td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer',  ['height' => 8]); ?></td>
            </tr>
            <tr>
                <td align="center" valign="top" class="content">
                    <?= $this->element('article/teasertext', ['showMore' => false, 'data' => $data]); ?>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                <td><?= $this->element('newsletter-spacer',  ['height' => 8]); ?></td>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <?= $this->element('moretext',  ['data' => $data]); ?>
                </td>
            </tr>
            <tr>
                <td><?= $this->element('newsletter-spacer',  ['height' => 25]); ?></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Teasecontent ENDE-->
<?= $this->element('newsletter-trenner',  ['folder' => $folder, 'content_width' => $content_width]); ?>
