<?

$defaults = [
    'folder' => 'default',
    'width' => 1,
    'height' => 1,
];

extract($defaults, EXTR_SKIP);
?>
<img src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif" border="0" width="<?= $width ?>"
     height="<?= $height ?>" alt=""/>
