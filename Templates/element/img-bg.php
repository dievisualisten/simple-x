<?

/*

Beispiele: Minimalconfig => überall das gleiche Format Bild:

'formats' => [
    'default' => [
        'default' => '4_3',
],


Beispiele: xs,sm das gleiche Bild:

'formats' => [
        'default' => [
            'default' => '2_1',
            'portrait' => '2_1',
            'landscape' => '2_1',
        ],
        'xs,sm' => [
            'default' => '2_1',
            'portrait' => '4_3',
            'landscape' => '2_1',
        ],
    ],


*/
$defaults = [
    'pick' => ['all'],
    'formats' => [
        'default' => [
            'default' => '4_3'
        ],
    ],
    'width' => false, //konkrete Breite, egal auf welchem Breakpoint
    'withWidthPlaceholder' => true,
    'class' => 'lazyload',
    'dataOptions' => null,
];

extract($defaults, EXTR_SKIP);

$w = null;

if ($withWidthPlaceholder) {
    $w = 'width';
}

if ($width) {
    $w = $width;
}

$dataOptions = null;

foreach ($formats as $breakpoint => $intFormats) {
    $breakpoints = explode(',', $breakpoint);
    foreach ($breakpoints as $bp) {
        foreach ($intFormats as $orientation => $format) {
            $curOptions = [
                'pick' => $pick,
                'format' => $format,
            ];

            $src = $this->Fe->getMediaUrl($this->Fe->getImg($data, $curOptions), $curOptions['format'], $w);
            $dataOptions .= 'data-bg-' . $orientation . '-' . $bp . '="' . $src . '" ' . "\n";
        }
    }
}

if (!empty($dataOptions)) {
    ?>
    <div class="background-image <?= $class ?>" <?= $dataOptions ?>>

    </div>
<? } ?>
