<?php

use App\Model\Entity\Article;

?>
<?= $this->Html->charset(); ?>
    <link rel="dns-prefetch" href="//<?= request()->domain(); ?>">
    <link rel="preconnect" href="//<?= request()->domain(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php
if ( ! empty($data) && is_object($data) ) {

    $pageTitle = $data->get('pagetitle');

    if ( empty($pageTitle) ) {
        $pageTitle = str_join(' - ', $data->get('title'), menu()->getDomainRecord()->get('extended_data.menu.pagetitle'));
    }
    ?>

    <title><?= $pageTitle; ?></title>

    <?php
    $description = $data->get('description') ?: $data->get('content');
    echo $this->Html->meta('description', trim(htmlspecialchars($this->Text->truncate(strip_tags($description), 157, ['exact' => false])))) . "\n";

    $attachment = $this->Media->getAttachment($data, [
      'pick' => ['titleimg', 'all'],
    ]);;

    if ( ! empty($attachment) ) {
        ?>
        <meta property="og:image" content="<?= $this->Media->getVersionUrl($attachment, 'xl', 1920) ?>">
        <link href="<?= $this->Media->getVersionUrl($attachment, 'xl', 1920) ?>" rel="image_src" />
        <?
    }

    $canonicalUrl = request()->fullPath();

    //Fixme Paginator next & prev & current

    echo $this->Html->meta('canonical', $data->get('canonical') ?: $canonicalUrl);

    if ( $data->get('noindex', false) ) {
        echo $this->Html->meta('content', 'noindex');
    }

    if ( config('Sx.app.language.multilingual') ) {
        foreach ( language()->getAllowedLanguages() as $locale ) {
            ?>
            <link href="<?= $this->Article->url(menu()->getMenuRecord("locales.{$locale}")) ?>" rel="alternate" hreflang="<?= $locale ?>" />
            <?
        }
    }

    echo $this->element('favicon');
}
