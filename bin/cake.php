#!/usr/bin/php -q
<?php

require dirname(__DIR__) . '/vendor/autoload.php';

/*
 * Configure paths required to find CakePHP + general filepath constants
 */
require dirname(__DIR__) . '/src-sx/Bootstrap/paths.php';

/*
 * Load application functions
 */
require dirname(__DIR__) . '/src-sx/Bootstrap/functions.php';

/*
 * Check platform requirements
 */
require dirname(__DIR__) . '/src/Bootstrap/requirements.php';

use Cake\Console\CommandRunner;
use App\Application;

// Build the runner with an application and root executable name.
$runner = new CommandRunner(new Application(dirname(__DIR__) . '/src/config'), 'cake');

try {
    $runner->run($argv);
} catch (\Exception $e) {
    echo $e->getMessage();
}
