<?php

namespace App\Command\Crontask;

use App\Library\Facades\Logger;
use Cake\Utility\Hash;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxRegenerateAttachments extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\AttachmentsTable
     */
    protected $Attachments;

    public $pluckSize = 5;

    public function __construct($task)
    {
        $this->loadModel('Attachments');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $workbench = $this->getWorkbench();

        $attachments = $this->Attachments->find()
            ->where([
                'id IN' => $workbench
            ])
            ->all();

        $fallback = [];

        foreach ( config('Media.formats') as $key => $value ) {
            $fallback[$key] = [];
        }

        foreach ($attachments as $attachment ) {

            $id = $attachment->id;

            try {
                $attachment = $this->Attachments->cropImage([
                    'id' => $attachment->id, 'versions' => array_merge($fallback, $attachment->instructions)
                ], true);
            } catch (\Exception $exception) {
                $attachment = null;
            }

            $this->removeAttachment($id);

            if ( $attachment ) {
                Logger::info("Attachment done: \"{$id}\"");
            } else {
                Logger::error("Attachment can not be saved: \"{$id}\"");
            }

        }

        $total = max(0, count($this->getAttachments()));

        if ( $total === 0 ) {
            $this->clearAttchments();
        }

        Logger::info("{$this->pluckSize} Attachments have been handled, {$total} remaining.");
    }

    public function getTmpPath(): string
    {
        return TMP . DS . 'attachments.php';
    }

    public function getAttachments() : array
    {
        $path = $this->getTmpPath();

        if ( ! file_exists($path) ) {
            $this->prepareAttachments();
        }

        return include $path;
    }

    public function putAttchments($data) : void
    {
        file_put_contents($this->getTmpPath(), "<?php \n return " .
            var_export($data, true) . ";");
    }

    public function clearAttchments() : void
    {
        unlink($this->getTmpPath());

        $this->task->set('active', false);
    }

    public function prepareAttachments()
    {
        $attachments = $this->Attachments->find()
            ->where([
                'type' => 'image'
            ])
            ->select([
                'id'
            ])
            ->all();

        $data = Hash::extract($attachments->toArray(), '{n}.id');

        $this->task->set('log', '');

        $this->putAttchments($data);
    }

    public function removeAttachment($id) : void
    {
        $data = array_diff($this->getAttachments(), [$id]);

        $this->putAttchments($data);
    }

    public function getWorkbench() : array
    {
        return array_slice($this->getAttachments(), 0,
            $this->pluckSize);
    }

}
