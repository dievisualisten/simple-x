<?php

namespace App\Command\Crontask;

use App\Library\ConsoleLogService;
use Cake\Datasource\ModelAwareTrait;

/**
 * Class DefaultTask
 *
 * @package App\Crontask
 */
class SxTaskDefault implements SxTaskInterface
{
    use ModelAwareTrait;

    /**
     * @var \Cake\Datasource\EntityInterface
     */
    protected $task;

    /**
     * DefaultTask constructor.
     *
     * @param $task
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    /**
     * Boot function
     */
    public function boot()
    {
        // Setup crontask
    }

}
