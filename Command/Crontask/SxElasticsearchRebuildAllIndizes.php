<?php

namespace App\Command\Crontask;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxElasticsearchRebuildAllIndizes extends SxTaskDefault
{

    public function __construct($task)
    {
        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $locales = language()->getAllowedLanguages();

        $allModelNames = ['Articles'];

        if ( $this->task && $this->task->parameter ) {
            $allModelNames = explode(',', $this->task->parameter);
        }

        foreach ( $allModelNames as $model ) {

            if ( preg_match('/^Sx/', $model) ) {
                continue;
            }

            foreach ( $locales as $locale ) {

                search()->reIndexAll($model, $locale);
                logger()->info("$model => $locale");
            }
        }

        logger()->info('ElasticsearchRebuildAllIndizes is done!');
    }
}
