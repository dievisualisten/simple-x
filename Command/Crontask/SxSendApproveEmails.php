<?php

namespace App\Command\Crontask;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxSendApproveEmails extends SxTaskDefault
{

    /**
     * @var \App\Model\Table\NewsletterrecipientsTable
     */
    protected $Newsletterrecipients;

    /**
     * Boot crontask
     */
    public function boot()
    {

        $this->loadModel('Newsletterrecipients');

        $query = $this->Newsletterrecipients->find()
            ->where([
                'approve_mail' => 1
            ]);

        foreach ( $query->all() as $recipient ) {

        }


        logger()->info('SendApproveEmails is done!');
    }

}
