<?php

namespace App\Command\Crontask;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/**
 * Class Make301RedirectsTask
 *
 * @package App\Command\Crontask
 */
class SxMake301Redirects extends SxTaskDefault
{
    /**
     * @var \Cake\Datasource\ConnectionManager
     */
    protected $connection;

    /**
     * @var array
     */
    protected $states;

    /**
     * @var string
     */
    protected $protocol;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var \App\Model\Table\RedirectsTable
     */
    protected $Redirects;

    /**
     * @var \App\Model\Table\MenusTemporaryTable
     */
    protected $MenusTemporary;

    /**
     * Boot crontask
     */
    public function boot()
    {
        $this->connection = ConnectionManager::get('default');

        $this->states = config('Redirect.states');
        $this->locale = config('Sx.app.language.defaultlanguage');
        $this->protocol = config('Sx.app.baseprotocol');

        $this->removeUnchangedTempMenus();
        $this->updateAlreadyExistingRedirects();
        $this->copyMovedPathsToRedirects();
        $this->copyChangedPathsToRedirects();
        $this->copyRemovedPathsToRedirects();
        $this->activateExistingRedirects();
        $this->deactivateExistingRedirects();
        $this->updateDublicatedRedirects();
        $this->removeSelfRefrencedRedirects();
        $this->submitUnsolvableRedirects();
        $this->mirrorMenusToTempMenus();

        logger()->info('Make301Redirects is done!');
    }

    /**
     *
     */
    public function submitUnsolvableRedirects()
    {
        $this->loadModel('MenusTemporary');

        $query = $this->MenusTemporary->find();

        if ( $query->count() == 0 ) {
            return;
        }

        logger()->info('Error in menus redirects.');
    }

    /**
     * Deactivate dublicates
     */
    public function updateDublicatedRedirects()
    {
        $this->loadModel('Redirects');

        $query = $this->Redirects->find()
            ->select([
                'Redirects.from_url',
            ])
            ->group([
                'Redirects.from_url',
            ])
            ->having([
                "COUNT(*) > 1",
            ]);

        $updateBag = [];

        foreach ( $query->all() as $redirect ) {

            $query = $this->Redirects->find()
                ->where([
                    "Redirects.from_url = '{$redirect->from_url}'",
                    "Redirects.status != '{$this->states['updated']}'",
                    "Redirects.status != '{$this->states['dublicated']}'",
                ])
                ->order([
                    "Redirects.locked ASC",
                    "Redirects.active ASC",
                    "LENGTH(Redirects.target_url) ASC",
                ]);

            $dublicates = $query->all();

            foreach ( $dublicates as $key => $dublicate ) {

                if ( $dublicate->status == $this->states['updated'] ) {
                    continue;
                }

                $clones = $this->Redirects->find()->where([
                    'from_url' => $dublicate->from_url,
                    'target_url' => $dublicate->target_url,
                ]);

                $this->Redirects->patchEntity($dublicate, [
                    'active' => $key == 0,
                    'locked' => false,
                    'status' => $this->states['dublicated'],
                ], [ 'notify' => true ]);

                if ( $dublicate->id != $clones->first()->id ) {
                    $this->Redirects->delete($dublicate);
                }

                if ( $dublicate->id == $clones->first()->id ) {
                    array_push($updateBag, $dublicate);
                }

            }

        }

        $options = [
            'validate' => false,
            'callbacks' => false,
        ];

        $this->Redirects->saveMany($updateBag, $options);
    }

    /**
     * Remove existsing redirects from menus_temporary
     */
    public function cleanMenusFromTempMenus()
    {
        $this->connection->execute("
            DELETE Temp FROM 
                menus_temporary AS Temp 
            WHERE 
                CONCAT('{$this->protocol}', Temp.path) IN (SELECT from_url FROM redirects GROUP BY from_url)
            ;
        ");
    }

    /**
     * Copy all menu entries to menus_temporary
     */
    public function mirrorMenusToTempMenus()
    {
        $this->connection->execute("
            INSERT INTO 
                menus_temporary (`id`, `locale`, `foreign_key`, `path`, `created`, `modified`) 
            SELECT 
                Menu.id, '{$this->locale}', Menu.foreign_key, Menu.path, Menu.created, Menu.modified 
            FROM 
                menus AS Menu 
            WHERE 
                Menu.type != 'alias' AND Menu.type != 'link' AND IFNULL(Menu.path, '') != '' 
            ;
            -- Translations --
            INSERT INTO 
                menus_temporary (`id`, `locale`, `foreign_key`, `path`, `created`, `modified`) 
            SELECT 
                MenuTrans.id, MenuTrans.locale, Menu.foreign_key, MenuTrans.path, MenuTrans.created, MenuTrans.modified 
            FROM 
                menus_translations AS MenuTrans 
            LEFT JOIN 
                menus AS Menu ON MenuTrans.id = Menu.id 
            WHERE 
                Menu.type != 'alias' AND Menu.type != 'link' AND IFNULL(MenuTrans.path, '') != '' 
            ;
        ");
    }

    /**
     * Remove unchanged menu entries from menus_temporary
     */
    public function removeUnchangedTempMenus()
    {
        $this->connection->execute("
            DELETE Temp FROM 
                menus_temporary AS Temp 
            LEFT JOIN 
                menus as Menu ON Menu.id = Temp.id 
            WHERE 
                Temp.locale = '{$this->locale}' AND Temp.path = Menu.path
            ;
            -- Translations --
            DELETE Temp FROM 
                menus_temporary AS Temp 
            LEFT JOIN 
                menus_translations as MenuTrans ON MenuTrans.id = Temp.id 
            LEFT JOIN 
                menus as Menu ON Menu.id = MenuTrans.id 
            WHERE 
                Temp.locale = MenuTrans.locale AND Temp.path = MenuTrans.path
            ;
        ");
    }

    /**
     * Update existing redirects with new redirect
     */
    public function updateAlreadyExistingRedirects()
    {
        $this->connection->execute("
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN 
                menus_temporary AS Temp ON Redirect.from_url = CONCAT('{$this->protocol}', Temp.path) 
            LEFT JOIN 
                menus AS Menu ON Menu.id = Temp.id 
            SET 
                Redirect.target_url = CONCAT('{$this->protocol}', Menu.path), Redirect.modified = NOW()
            WHERE
                Temp.locale = '{$this->locale}' AND Menu.id = Temp.id AND Redirect.locked = 0
            ;
            -- Translations --
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN 
                menus_temporary AS Temp ON Redirect.from_url = CONCAT('{$this->protocol}', Temp.path) 
            LEFT JOIN 
                menus_translations AS MenuTrans ON MenuTrans.id = Temp.id 
            SET 
                Redirect.target_url = CONCAT('{$this->protocol}', MenuTrans.path), Redirect.modified = NOW()
            WHERE
                Temp.locale != '{$this->locale}' AND MenuTrans.id = Temp.id AND Redirect.locked = 0
            ;
        ");

        $this->cleanMenusFromTempMenus();
    }

    /**
     * Copy paths wich have still an matching id
     */
    public function copyMovedPathsToRedirects()
    {
        $this->connection->execute("
            INSERT INTO
                redirects (`id`, `from_url`, `target_url`, `active`, `generated`, `locked`, `notify`, `status`, `created`, `modified`)
            SELECT
                UUID(), CONCAT('{$this->protocol}', Temp.path), CONCAT('{$this->protocol}', Menu.path), 1, 1, 0, 0, '{$this->states['moved']}', NOW(), NOW()
            FROM
                menus_temporary AS Temp
            LEFT JOIN
                menus AS Menu ON Menu.id = Temp.id
            WHERE
                Temp.locale = '{$this->locale}' AND Menu.id = Temp.id
            ;
            -- Translations --
            INSERT INTO
                redirects (`id`, `from_url`, `target_url`, `active`, `generated`, `locked`, `notify`, `status`, `created`, `modified`)
            SELECT
                UUID(), CONCAT('{$this->protocol}', Temp.path), CONCAT('{$this->protocol}', MenuTrans.path), 1, 1, 0, 0, '{$this->states['moved']}', NOW(), NOW()
            FROM
                menus_temporary AS Temp
            LEFT JOIN
                menus_translations AS MenuTrans ON MenuTrans.id = Temp.id
            WHERE
                Temp.locale != '{$this->locale}' AND MenuTrans.id = Temp.id
            ;
        ");

        $this->cleanMenusFromTempMenus();
    }

    /**
     * Copy paths with an still existing foreign_key
     */
    public function copyChangedPathsToRedirects()
    {
        $this->connection->execute("
            INSERT INTO 
                redirects (`id`, `from_url`, `target_url`, `active`, `generated`, `locked`, `notify`, `status`, `created`, `modified`) 
            SELECT 
                UUID(), CONCAT('{$this->protocol}', Temp.path), CONCAT('{$this->protocol}', Menu.path), 1, 1, 0, 0, '{$this->states['changed']}', NOW(), NOW() 
            FROM 
                menus_temporary AS Temp 
            LEFT JOIN 
                menus AS Menu ON Menu.foreign_key = Temp.foreign_key
            WHERE 
                Temp.locale = '{$this->locale}' AND Menu.foreign_key = Temp.foreign_key
            ;
            -- Translations --
            INSERT INTO 
                redirects (`id`, `from_url`, `target_url`, `active`, `generated`, `locked`, `notify`, `status`, `created`, `modified`) 
            SELECT 
                UUID(), CONCAT('{$this->protocol}', Temp.path), CONCAT('{$this->protocol}', MenuTrans.path), 1, 1, 0, 0, '{$this->states['changed']}', NOW(), NOW() 
            FROM 
                menus_temporary AS Temp 
            LEFT JOIN 
                menus AS Menu ON Menu.foreign_key = Temp.foreign_key
            LEFT JOIN 
                menus_translations AS MenuTrans ON MenuTrans.id = Menu.id
            WHERE 
                Temp.locale != '{$this->locale}' AND MenuTrans.id = Menu.id AND Menu.foreign_key = Temp.foreign_key
            ;
        ");

        $this->cleanMenusFromTempMenus();
    }

    /**
     * Copy paths with no target
     */
    public function copyRemovedPathsToRedirects()
    {
        $this->connection->execute("
            INSERT INTO 
                redirects (`id`, `from_url`, `target_url`, `active`, `generated`, `locked`, `notify`, `status`, `created`, `modified`) 
            SELECT 
                UUID(), CONCAT('{$this->protocol}', Temp.path), '', 0, 1, 1, 1, '{$this->states['removed']}', NOW(), NOW() 
            FROM 
                menus_temporary AS Temp
            ;
        ");

        $this->cleanMenusFromTempMenus();
    }

    /**
     * Activate redirects if menu has been removed
     */
    public function activateExistingRedirects()
    {
        $this->connection->execute("
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN
                menus AS Menu ON CONCAT('{$this->protocol}', path) = Redirect.from_url
            SET 
                Redirect.status = '{$this->states['activated']}', Redirect.active = 1, Redirect.modified = NOW()
            WHERE
                Redirect.active = 0 AND Redirect.status = '{$this->states['deactivated']}' AND Menu.id IS NULL
            ;
            -- Translations --
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN
                menus_translations AS MenuTrans ON CONCAT('{$this->protocol}', path) = Redirect.from_url
            SET 
                Redirect.status = '{$this->states['activated']}', Redirect.active = 1, Redirect.modified = NOW()
            WHERE
                Redirect.active = 0 AND Redirect.status = '{$this->states['deactivated']}' AND MenuTrans.id IS NULL
            ;
        ");
    }

    /**
     * Deactivate redirects wich are not required cause menu exists
     */
    public function deactivateExistingRedirects()
    {
        $res = $this->connection->execute("
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN
                menus AS Menu ON CONCAT('{$this->protocol}', path) = Redirect.from_url
            SET 
                Redirect.status = '{$this->states['deactivated']}', Redirect.active = 0, Redirect.modified = NOW()
            WHERE
                Redirect.active = 1 AND Redirect.status != '{$this->states['updated']}' AND Menu.id IS NOT NULL
            ;
            -- Translations --
            UPDATE 
                redirects AS Redirect 
            LEFT JOIN
                menus_translations AS MenuTrans ON CONCAT('{$this->protocol}', path) = Redirect.from_url
            SET 
                Redirect.status = '{$this->states['deactivated']}', Redirect.active = 0, Redirect.modified = NOW()
            WHERE
                Redirect.active = 1 AND Redirect.status != '{$this->states['updated']}' AND MenuTrans.id IS NOT NULL
            ;
        ");
    }

    /**
     * Deactivate redirects poiting to themself
     */
    public function removeSelfRefrencedRedirects()
    {
        $this->connection->execute("
            UPDATE 
                redirects AS Redirect 
            SET 
                Redirect.status = '{$this->states['refrenced']}', Redirect.active = 0, Redirect.notify = 1, Redirect.modified = NOW()
            WHERE
                Redirect.from_url = Redirect.target_url
            ;
        ");
    }

}
