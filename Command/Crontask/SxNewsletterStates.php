<?php

namespace App\Command\Crontask;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxNewsletterStates extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\NewslettersTable
     */
    protected $Newsletters;

    /**
     * @var \App\Model\Table\NewsletterrecipientsTable
     */
    protected $Newsletterrecipients;

    /**
     * @var \App\Model\Table\NewslettercampaignsTable
     */
    protected $Newslettercampaigns;

    public function __construct($task)
    {
        $this->loadModel('Newsletters');
        $this->loadModel('Newsletterrecipients');
        $this->loadModel('Newslettercampaigns');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $newsletters = $this->Newsletters->find()
            ->where([
                'DATE(Newsletter.senddate) >=' => date('Y-m-d', strtotime("-7 days")),
                'Newsletter.to_send' => 0,
                'IFNULL(Newsletter.senddate, "") != ' => '',
            ]);

        if (empty($newsletters)) {
            $this->log->info('Keine Newsletter aus den letzten 7 Tagen, die überprüft werden müssten.');
        }

        foreach ($newsletters as $newsletter) {

            //read
            $reads = $this->Newslettercampaigns->find()
                ->where([
                    'newsletter_id' => $newsletter['Newsletter']['id'],
                    'read' => "1",
                ])
                ->count();

            $this->Newsletters->id = $newsletter['Newsletter']['id'];
            $this->Newsletters->saveField('readcount', $reads);

            //unsubscribed
            $unsubscribes = $this->Newslettercampaigns->find()
                ->where('count', [
                    'newsletter_id' => $newsletter['Newsletter']['id'],
                    'unsubscribed' => "1",
                ])
                ->count();

            $this->Newsletters->id = $newsletter['Newsletter']['id'];
            $this->Newsletters->saveField('unsubscribes', $unsubscribes);

            //clicked
            $clickers = $this->Newslettercampaigns->find()
                ->where([
                    'newsletter_id' => $newsletter['Newsletter']['id'],
                    'clicked' => "1",
                ])
                ->count();

            $this->Newsletters->id = $newsletter['Newsletter']['id'];
            $this->Newsletters->saveField('clickers', $clickers);

        }

        logger()->info('NewsletterStates is done!');
    }

}
