<?php

namespace App\Command\Crontask;

use Cake\Core\Configure;

class SxDbDump extends SxTaskDefault
{

    public function __construct($task)
    {
        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $dbconfig = Configure::read('mysqldump.dumpout');

        $config = [
            'host' => $dbconfig['host'],
            'db' => $dbconfig['database'],
            'user' => $dbconfig['login'],
            'pass' => $dbconfig['password'],
            'dump_command' => Configure::read('mysqldump.path') . Configure::read('mysqldump.mysqldump'),
            'temp_file' => TMP . date('Y-m-d') . '_export.sql', //, Configure::read('mysqldump.temp_file') ,
            'default_character_set' => $dbconfig['encoding'],
        ];

        $dump = $config['dump_command'] . ' ' . $config['db'] . ' -u' . $config['user'] . ' -p' . $config['pass'] . ' --host=' . $config['host'] . ' --default-character-set=' . $config['default_character_set'] . ' | gzip -9 > ' . $config['temp_file'] . '.gz';

        exec($dump, $dump_array, $dump_return_var);

        if ($dump_return_var == 0) {
            // von vor 2 Wochen löschen
            @unlink(TMP . date('Y-m-d', strtotime('-14 days')) . '_export.sql');
        } else {
            throw new \Exception('Error');
        }

        logger()->info('DbDump is done!');
    }

}
