<?php

namespace App\Command\Crontask;

use App\Library\MailerService;
use App\Library\PageBuilder;
use App\Library\RouterService;
use Cake\Core\Configure;
use Cake\ORM\Query;
use Cake\Utility\Hash;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxNewsletterSendToSend extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\NewslettersTable
     */
    protected $Newsletters;

    /**
     * @var \App\Model\Table\NewsletterrecipientsTable
     */
    protected $Newsletterrecipients;

    /**
     * @var \App\Model\Table\NewslettercampaignsTable
     */
    protected $Newslettercampaigns;

    public function __construct($task)
    {
        $this->loadModel('Newsletters');
        $this->loadModel('Newsletterrecipients');
        $this->loadModel('Newslettercampaigns');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {

        $newsletter = $this->Newsletters->find()
            ->where([
                'to_send' => true,
            ])
            ->order([
                'created' => 'DESC',
            ])
            ->first();

        //Nothing To Do
        if ( empty($newsletter) ) {
            $this->log->info(self::class, 'Keine Kampagne im Versand');
        }

        $this->newsletterSend($newsletter->id);

        logger()->info('NewsletterSendToSend is done!');
    }

    private function newsletterSend($newsletterId = null)
    {
        $success = true;
        $numSent = 0;

        $newsletter = $this->Newsletters->find()
            ->where([
                'Newsletters.id' => $newsletterId,
            ])
            ->contain([
                'Emails', 'Newsletterinterests', 'Menus',
            ])
            ->first();

        if ( empty($newsletter) ) {
            throw new \Exception('Es existiert kein Newsletter mit der ID' . $newsletterId);
        }

        if ( empty($newsletter->campaigncreated) ) {

            $this->log->info(self::class, 'Erstellung der Newsletterkampagne ' . $newsletter->name);

            $query = $this->Newsletterrecipients->find()
                ->where([
                    'approved' => true,
                    'unsubscribed' => false,
                    'hardbounced' => false,
                ])
                ->select([
                    'id',
                ])
                ->group('email');

            if ( $newsletter->language ) {
                $query->andWhere([
                    'language' => $newsletter->language,
                ]);
            }

            $interestIds = Hash::extract($newsletter->newsletterinterests, '{n}.id');

            if ( ! empty($interestIds) ) {
                $query->matching('Newsletterinterests', function (Query $q) use ($interestIds) {
                    return $q->where(['Newsletterinterests.id IN' => $interestIds]);
                });
            }

            foreach ( $query as $newsletterRecipient ) {

                $newsletterCampaignEntity = $this->Newslettercampaigns->newEntity([
                    'newsletter_id' => $newsletterId,
                    'newsletterrecipient_id' => $newsletterRecipient->id,
                ]);

                if ( ! $this->Newslettercampaigns->save($newsletterCampaignEntity) ) {
                    throw new \Exception('Es ist ein Fehler beim Erstellen der Newsletterkampagne aufgetreten.');
                }

            }

            $newsletter = $this->Newsletters->get($newsletterId);
            $newsletter->campaigncreated = true;

            if ( ! $this->Newsletters->save($newsletter) ) {
                throw new \Exception('Es ist ein Fehler beim Erstellen der Newsletterkampagne aufgetreten.');
            }

            $this->log->info(self::class, 'Die Newsletterkampagne ' . $newsletter->name . ' wurde erstellt. Es wird an ' . $query->count() . ' Empfänger gesendet.');

        }    // Ende Erstellung der Newsletterkampagne

        //Content holen

        if ( ! empty($newsletter->menu) ) {

            $domain = request()->getDomainFromPath($newsletter->menu);

            if ( empty($domain) ) {
                throw new Exception(self::class, 'Domain nicht gefunden');
            }

        }

        if ( ! empty($newsletter->language) ) {
            Configure::write('Config.language', $newsletter->language);
        } else {
            Configure::write('Config.language', Configure::read('Sx.app.language.defaultlanguage'));
        }

        $article = PageBuilder::articleView($newsletter->menu->foreign_key, $newsletter->menu->id, $newsletter->menu->type);

        if ( empty($article) ) {
            throw new \Exception('Der gewählten Artikel für den Newsletter ' . $newsletter->name . ' konnte nicht gefunden werden.');
        }

        //Abbruch, wenn die Sprachvariante nicht vorhanden ist
        if ( ! empty($newsletter->language) && $article['Article']['locale'] != Configure::read('Config.language') ) {
            throw new \Exception('Den gewählten Artikel für den Newsletter ' . $newsletter->name . ' gibt es nicht auf der  Sprache ' . $newsletter->language);
        }

        //Newsletter Newsletterrecipient holen
        $newsletterCampaigns = $this->Newslettercampaigns->find()
            ->where([
                'send' => false,
                'newsletter_id' => $newsletterId,
            ])
            ->contain([
                'Newsletterrecipients.Persons'
            ])
            ->limit(100)->all();

        //Kampagne beendet
        if ( $newsletterCampaigns->isEmpty() ) {

            $newsletter->sendcount = $this->Newslettercampaigns->find()
                ->where([
                    'newsletter_id' => $newsletterId
                ])
                ->count();

            $newsletter->delivered = $this->Newslettercampaigns->find()
                ->where([
                    'status IS' => null,
                    'send' => true,
                    'newsletter_id' => $newsletterId,
                ]);

            $newsletter->senddate = date("Y-m-d H:i:s");
            $newsletter->to_send = false;
            $newsletter->send = true;

            $this->log->info('Der Versand für ' . $newsletter->name . ' ist beendet.');

            return;

            //Kampagne läuft
        } else {

            // $this->log->info(self::class, 'Beginne mit dem Versand an ' . count($newsletter_empfaengers) . ' Empfänger.');

            $sendingErrorCount = 0;

            foreach ( $newsletterCampaigns as $newsletterCampaign ) {

                $campaignCount = $this->Newslettercampaigns->find()
                    ->where([
                        'send' => true,
                        'id' => $newsletterCampaign->id
                    ])
                    ->count();

                //wenn schon erhalten, dann nicht nochmal schicken
                if ( $campaignCount > 0 ) {
                    continue;
                }

                $newsletterCampaign->senddate = date("Y-m-d H:i:s");
                // $newsletterCampaign->send = true; TODO Debug

                if ( $this->Newslettercampaigns->save($newsletterCampaign) ) {

                    $sent = MailerService::sendMail([
                        'to' => $newsletterCampaign->newsletterrecipient->email,
                        'subject' => $newsletter->subject,
                        'emailConfig' => $newsletter->email,
                        'layout' => 'blank',
                        'template' => 'newsletter' . DS . $newsletter->template,
                        'base_href' => request()->getDomainFromPath($newsletter->menu),
                        'viewVars' => [
                            'newslettercampaign' => $newsletterCampaign,
                            'newsletter' => $newsletter,
                            'data' => $article['data'],
                            'children' => $article['children'],
                        ],
                    ]);

                    if ( ! $sent ) {
                        $sendingErrorCount++;
                        $newsletterCampaign->staus = 'sending error'; // refactor
                    } else {
                        $sendingErrorCount = 0;
                        $numSent++;
                    }

                    $newsletterCampaign->senddate = date("Y-m-d H:i:s");
                    $this->Newslettercampaigns->save($newsletterCampaign);

                } else {
                    throw new \Exception('Newslettercampaign mit der ID: ' . $newsletterCampaign->id . ' konnte nicht gespeichert werden.');
                }

                //wenn mehr als 30 in Folge nicht versand werden konnten
                //scheint es einen Fehler im SMTP Server zu geben, Versand beenden
                if ( $sendingErrorCount == 30 ) {
                    throw new \Exception('Es scheint einen Fehler mit dem SMTP Server zu geben. Siehe SxMailer LogFile.');
                }
            }
        }

        $this->log->info(self::class, 'Newsletter ' . $newsletter->name . ' wurde an ' . $numSent . ' von ' . count($newsletterCampaigns) . ' Empfänger gesendet. Es wird weiter gesendet.');

        //  $this->setStatus('Newsletter ' . $newsletter->name . ' wurde an ' . $numSent . ' von ' . count($newsletterCampaigns) . ' Empfänger mit zuletzt einem kritischen Fehlern gesendet. Es wird nicht weiter gesendet.', CRON_ERROR);
    }

}
