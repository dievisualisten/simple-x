<?php

namespace App\Command\Crontask;

use Cake\Datasource\ConnectionManager;

/**
 * Class SxReplaceMovedArticleUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxReplaceMovedArticleUrls extends SxTaskDefault
{
    protected $connection;

    protected $Redirects;

    /**
     * Boot crontask
     */
    public function boot()
    {
        $this->connection = ConnectionManager::get('default');

        $this->loadModel('Redirects');

        $query = $this->Redirects->find()
            ->where([
                'active' => true
            ]);

        $replace = '%s';

        foreach ($query->all() as $redirect) {
            $replace = "REPLACE({$replace}, '\"{$redirect->from_url}\"', '\"{$redirect->target_url}\"')";
        }

        foreach ($query->all() as $redirect) {
            $replace = "REPLACE({$replace}, '>{$redirect->from_url}<', '>{$redirect->target_url}<')";
        }

        $query = "UPDATE %s SET teasertext = {$replace}, teasertext2 = {$replace}, content = {$replace}, content2 = {$replace};";

        $this->connection->execute(
            sprintf($query, 'articles', 'teasertext', 'teasertext2', 'content', 'content2')
        );

        $this->connection->execute(
            sprintf($query, 'articles_translations', 'teasertext', 'teasertext2', 'content', 'content2')
        );

        logger()->info('ReplaceMovedArticleUrls is done!');
    }

}
