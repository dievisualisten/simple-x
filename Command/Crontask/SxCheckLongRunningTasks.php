<?php

namespace App\Command\Crontask;

use App\Library\ConsoleLogService;

/**
 * Class SxCheckTaskTimeoutTask
 *
 * @package App\Crontask
 */
class SxCheckLongRunningTasks extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\CrontasksTable
     */
    protected $Crontasks;

    public function __construct($task)
    {
        $this->loadModel('Crontasks');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $query = $this->Crontasks->find()
            ->where([
                'method !=' => 'checkLongRunningTasks',
                'active' => true,
                'status' => STATUS_RUNNING,
            ]);

        foreach ( $query->all() as $task ) {

            if ( ! $task->last_start->addHours(12)->isPast() ) {
                continue;
            }

            $this->checkTask($task);
        }

        logger()->info('Crontasks still running: ' . $query->count());
    }

    /**
     * Test if task is restartable
     *
     * @param $task
     * @throws \Exception
     */
    protected function checkTask($task)
    {
        if ( $task->restartable ) {

            $this->Crontasks->patchEntity($task, [
                'active' => true,
                'status' => '',
            ]);

            $message = 'Long Running Task - ' . $task->name . ' - wurde neue gestartet.';
        } else {

            $this->Crontasks->patchEntity($task, [
                'active' => false,
                'status' => '',
            ]);

            $message = 'Long Running Task - ' . $task->name . ' - wurde angehalten. Bitte überprüfen.';
        }

        // Save task state
        $this->Crontasks->save($task);

        // Send mail to sysadmin
        logger()->critical($message);
    }

}
