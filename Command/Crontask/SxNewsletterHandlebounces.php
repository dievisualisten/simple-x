<?php

namespace App\Command\Crontask;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxNewsletterHandlebounces extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\NewslettersTable
     */
    protected $Newsletters;

    public function __construct($task)
    {
        $this->loadModel('Newsletters');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $newsletters = $this->Newsletters->find()
            ->contain([
                'Email',
            ])
            ->where([
                'DATE(Newsletter.senddate) >=' => date('Y-m-d', strtotime("-3 days")),
                'to_send' => 0,
                'IFNULL(Newsletter.senddate, "") != ' => '',
            ])
            ->all();

        if ( empty($newsletters) ) {
            $this->log->info(self::class, 'Keine Newsletter aus den letzten 3 Tagen, die überprüft werden müssten.');
        }

        // TODO: SX3
        foreach ( $newsletters as $newsletter ) {

            //imapostfach abrufen, wenn vorhanden
            //die Bounces rausfischen speichern, löschen
            if ( ! empty($newsletter['Email']['imap_host']) && ! empty($newsletter['Email']['imap_port']) && ! empty($newsletter['Email']['imap_username']) && ! empty($newsletter['Email']['imap_password']) ) {

                App::import('Vendor', 'bouncemail/Bouncehandler');

                $opts['server'] = $newsletter['Email']['imap_host'];
                $opts['port'] = $newsletter['Email']['imap_port'];

                if ( $opts['port'] ) {
                    $opts['port'] = ':' . $opts['port'];
                }

                $opts['type'] = 'imap';
                if ( $opts['type'] ) {
                    $opts['type'] = '/' . $opts['type'];
                }
                $opts['user'] = $newsletter['Email']['imap_username'];
                $opts['pass'] = $newsletter['Email']['imap_password'];
                $opts['mailbox'] = 'INBOX';

                $bouncehandler = new Bouncehandler();

                //verbindung aufbauen (ähnlich wie mysql verbindung)
                $imap_con = imap_open('{' . $opts['server'] . $opts['port'] . $opts['type'] . '}' . $opts['mailbox'], $opts['user'],
                    $opts['pass']) or die("can't connect: " . imap_last_error());

                //Anzahl der Mails
                $nr_msgs = imap_num_msg($imap_con);

                //Schleife von 0 bis Anzahl der Mails
                for ( $i = 0; $i <= $nr_msgs; $i++ ) {

                    //headerinfo der mail mit nr <2.param> in $mail speichern
                    $mail = imap_headerinfo($imap_con, $i);
                    //mail inhalt in $mail_content speichern
                    $mail_content = imap_body($imap_con, $i);
                    //mail betreff in $mail_header speichern
                    $mail_header = imap_fetchheader($imap_con, $i);
                    //mail betreff und inhalt in $multiArray speichern
                    $multiArray = $bouncehandler->get_the_facts($mail_header . ' ' . $mail_content);

                    if ( ! empty($multiArray[0]) && ! empty($multiArray[0]['recipient']) ) {

                        //gibt es dem Empfänger
                        $this->Newsletterrecipient->contain();
                        $newsletterrecipient = $this->Newsletterrecipient->find('first', ['conditions' => ['email' => trim($multiArray[0]['recipient'])]]);

                        if ( ! empty($newsletterrecipient) ) {
                            //Hardbounce
                            if ( $multiArray[0]['action'] == 'failed' && ($multiArray[0]['status'] == '5.1.1' || $multiArray[0]['status'] == '5.1.2' || $multiArray[0]['status'] == '5.1.6') ) {

                                //Als hardbounce markieren, damit die diese nicht wieder angeschrieben wird
                                $this->Newsletterrecipient->id = $newsletterrecipient['Newsletterrecipient']['id'];
                                $newsletterrecipient['Newsletterrecipient']['hardbounced'] = 1;
                                $newsletterrecipient['Newsletterrecipient']['bouncestatus'] = $multiArray[0]['status'];
                                $this->Newsletterrecipient->save($newsletterrecipient);

                                //kampagne finden
                                $campaign = $this->Newslettercampaign->find('first', [
                                    'conditions' => [
                                        'newsletter_id' => $newsletter['Newsletter']['id'],
                                        'newsletterrecipient_id' => $newsletterrecipient['Newsletterrecipient']['id'],
                                    ],
                                ]);

                                if ( ! empty($campaign) ) {
                                    $this->Newslettercampaign->id = $campaign['Newslettercampaign']['id'];
                                    $campaign['Newslettercampaign']['hardbounced'] = 1;
                                    $this->Newslettercampaign->save($campaign);
                                }

                                //softbounce
                            } else {
                                if ( $multiArray[0]['action'] == 'failed' || $multiArray[0]['action'] == 'autoreply' ) {

                                    //kampagne finden
                                    $campaign = $this->Newslettercampaign->find('first', [
                                        'conditions' => [
                                            'newsletter_id' => $newsletter['Newsletter']['id'],
                                            'newsletterrecipient_id' => $newsletterrecipient['Newsletterrecipient']['id'],
                                        ],
                                    ]);

                                    if ( ! empty($campaign) ) {
                                        $this->Newslettercampaign->id = $campaign['Newslettercampaign']['id'];
                                        $campaign['Newslettercampaign']['softbounced'] = 1;
                                        $this->Newslettercampaign->save($campaign);
                                    }
                                }
                            }
                        }
                    }
                }
                //verbindung trennen
                imap_close($imap_con);

            }

            $this->Newslettercampaign->contain();
            $hardbounces = $this->Newslettercampaign->find('count', [
                'conditions' => [
                    'newsletter_id' => $newsletter['Newsletter']['id'],
                    'hardbounced' => 1,
                ],
            ]);

            $this->Newsletter->id = $newsletter['Newsletter']['id'];
            $this->Newsletter->saveField('hardbounces', $hardbounces);

            $this->Newslettercampaign->contain();
            $softbounces = $this->Newslettercampaign->find('count', [
                'conditions' => [
                    'newsletter_id' => $newsletter['Newsletter']['id'],
                    'softbounced' => 1,
                ],
            ]);

            $this->Newsletter->id = $newsletter['Newsletter']['id'];
            $this->Newsletter->saveField('softbounces', $softbounces);

        }

        logger()->info('NewsletterSendToSend is done!');
    }

}
