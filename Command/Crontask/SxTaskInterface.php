<?php

namespace App\Command\Crontask;

use App\Model\Entity\Crontask;

/**
 * Interface TaskInterface
 *
 * @package App\Crontask\Inteface
 */
interface SxTaskInterface
{
    /**
     * TaskInterface constructor.
     *
     * @param \App\Model\Entity\Crontask $task
     */
    public function __construct($task);

    /**
     * @return mixed
     */
    public function boot();
}
