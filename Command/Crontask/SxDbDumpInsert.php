<?php

namespace App\Command\Crontask;

use Cake\Core\Configure;

class SxDbDumpInsert extends SxTaskDefault
{

    public function __construct($task)
    {
        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $dbconfig = Configure::read('mysqldump.dumpin');
        $config = [
            'host' => $dbconfig['host'],
            'db' => $dbconfig['database'],
            'user' => $dbconfig['login'],
            'pass' => $dbconfig['password'],
            'import_command' => Configure::read('mysqldump.path') . Configure::read('mysqldump.mysql'),
            'temp_file' => Configure::read('mysqldump.temp_file'),
            'default_character_set' => $dbconfig['encoding'],
        ];

        $insert = $config['import_command'] . ' -h ' . $config['host'] . ' -u ' . $config['user'] . ' -p' . $config['pass'] . ' ' . $config['db'] . ' < ' . $config['temp_file'] . ' --default-character-set=' . $config['default_character_set'];

        exec($insert, $dump_array, $dump_return_var);

        if ($dump_return_var != 0) {
            throw new \Exception('Error');
        }

        logger()->info('SxDbDumpInsert is done!');
    }

}
