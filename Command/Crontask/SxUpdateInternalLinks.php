<?php

namespace App\Command\Crontask;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxUpdateInternalLinks extends SxTaskDefault
{
    /**
     * @var \App\Model\Table\NewslettersTable
     */
    protected $Newsletters;

    /**
     * @var \App\Model\Table\NewsletterrecipientsTable
     */
    protected $Newsletterrecipients;

    /**
     * @var \App\Model\Table\NewslettercampaignsTable
     */
    protected $Newslettercampaigns;

    public function __construct($task)
    {
        $this->loadModel('Newsletters');
        $this->loadModel('Newsletterrecipients');
        $this->loadModel('Newslettercampaigns');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        logger()->info('NewsletterSendToSend is done!');
    }

}
