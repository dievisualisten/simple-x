<?php

namespace App\Command\Crontask;

use App\Library\ConsoleLogService;

/**
 * Class SxReplaceMovedCononicalUrlsTask
 *
 * @package App\Command\Crontask
 */
class SxReplaceMovedCanonicalUrls extends SxTaskDefault
{

    /**
     * @var \App\Model\Table\ArticlesTable
     */
    protected $Articles;

    /**
     * @var \App\Model\Table\RedirectsTable
     */
    protected $Redirects;

    public function __construct($task)
    {
        $this->loadModel('Articles');
        $this->loadModel('ArticlesTranslations');
        $this->loadModel('Redirects');

        parent::__construct($task);
    }

    /**
     * Boot crontask
     */
    public function boot()
    {
        $redirectsQuery = $this->Redirects->find()
            ->where([
                'active' => true
            ]);

        $redirects = $redirectsQuery->all();

        $articlesQuery = $this->Articles->find()
            ->where([
                'type NOT IN' => ['link'],
                'canonical !=' => '',
            ]);

        $updateBag = [];

        foreach ( $articlesQuery->all() as $article ) {

            $redirect = $redirects->match([
                'from_url' => $article->canonical
            ]);

            if ( $redirect->count() == 0 ) {
                continue;
            }

            $this->Articles->patchEntity($article, [
                'canonical' => $redirect->first()->target_url,
            ]);

            array_push($updateBag, $article);
        }

        $this->Articles->saveMany($updateBag);

        // TODO: Canonical column missing in articles_translations

        logger()->info('ReplaceMovedCanonicalUrls is done!');
    }

}
