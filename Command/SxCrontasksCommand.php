<?php

namespace App\Command;

use App\Command\Crontask\SxTaskInterface;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\ConsoleException;
use Cake\Datasource\ModelAwareTrait;
use Cake\I18n\Time;
use Cake\Utility\Inflector;
use Exception;

class SxCrontasksCommand extends Command
{
    use ModelAwareTrait;

    /**
     * @var \App\Model\Table\CrontasksTable
     */
    protected $Crontasks;

    public function __construct()
    {
        $this->loadModel('Crontasks');

        parent::__construct();
    }

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser
            ->addArgument('method', [
                'help' => 'Crontask method',
            ]);

        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        $method = $args->getArgument('method');

        if ( $method != null ) {

            try {
                $this->runTask($method);
            } catch ( Exception $e ) {
                logger()->error($e->getMessage());
            }

            return;
        }

        $query = $this->Crontasks->find()
            ->where([
                'active' => 1,
                'status !=' => STATUS_RUNNING,
            ])
            ->order([
                'modified' => 'DESC',
            ]);

        $tasks = $query->all();

        foreach ( $tasks as $task ) {

            // Start logging
            logger()->start();

            if ( ! $this->Crontasks->checkExecution($task) ) {
                continue;
            }

            try {
                $this->runTask($task->method, $task);
                $task->status = config('Crontask.success');
            } catch ( Exception $e ) {
                logger()->error($e->getMessage());
                $task->status = config('Crontask.error');
            }

            // Get logs
            $task->log = logger()->stop();

            // Store logs in crontask
            $this->Crontasks->save($task);
        }

        logger()->info($tasks->count() . ' total tasks run.');
    }

    protected function runTask($method, $task = null)
    {
        if ( $task !== null ) {

            $this->Crontasks->patchEntity($task, [
                'last_start' => Time::now(), 'life_sign' => Time::now(),
            ]);

            if ( ! config('Cronjob.debug') ) {
                $task->status = STATUS_RUNNING;
            }

            $this->Crontasks->save($task);
        }

        $taskName = 'App\\Command\\Crontask\\' . Inflector::camelize($method);

        if ( ! class_exists($taskName) ) {
            throw new ConsoleException('Task ' . $taskName . ' doesn\'t exist.');
        }

        if ( ! is_subclass_of($taskName, SxTaskInterface::class) ) {
            throw new ConsoleException('Task ' . $taskName . ' doesn\'t implement the TaskInterface.');
        }

        call_user_func_array([
            new $taskName($task), 'boot'
        ], []);
    }

}
