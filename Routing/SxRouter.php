<?php

namespace App\Routing;

/**
 * @version 4.0.0
 */

use Cake\Routing\Router;
use Cake\Routing\RouteBuilder;

class SxRouter extends Router
{
    static public $initialized = false;

    public static function createRouteBuilder($path, array $options = []): RouteBuilder
    {
        $defaults = [
            'routeClass' => static::defaultRouteClass(),
            'extensions' => static::$_defaultExtensions,
        ];

        $options += $defaults;

        return new SxRouteBuilder(static::$_collection, $path, [], [
            'routeClass' => $options['routeClass'],
            'extensions' => $options['extensions'],
        ]);
    }

    public static function locale(\Closure $callback)
    {
        static::$initialized = true;

        static::scope('/', function ($routes) use ($callback) {

            /** @var \App\Routing\RouteBuilder $routes */
            $routes->locale($callback);
        });
    }

}
