<?php

namespace App\Routing\Route;

use App\Library\Facades\Menu;
use Cake\Routing\Route\Route;

class SxDatabaseRoute extends Route
{

    public function parse($url, $method = ''): ?array
    {
        return Menu::getRouteParams();
    }

}
