<?php

namespace App\Routing\Route;

use App\Utility\StringUtility;
use Cake\Core\App;
use Cake\Routing\Route\InflectedRoute;

class SxControllerRoute extends InflectedRoute
{

    public function parse($url, $method = ''): ?array
    {
        $override = null;

        if ( ! $parsed = parent::parse($url, $method) ) {
            return null;
        }

        $className = App::className(
            $parsed['controller'], 'Controller', 'Controller'
        );

        $requestType = strtolower($_SERVER['REQUEST_METHOD']);

        $explicitAction = StringUtility::camel(isset($requestType) ?
            $requestType . '_' . $parsed['action'] : $parsed['action']);

        $methodExists = method_exists($className, $explicitAction);

        if ( $methodExists === true ) {
            $override = $explicitAction;
        }

        $explicitAction = StringUtility::camel(isset($parsed['_ext']) ?
            $parsed['action'] . '_' . $parsed['_ext'] : $parsed['action']);

        $methodExists = method_exists($className, $explicitAction);

        if ( $methodExists === true ) {
            $override = $explicitAction;
        }

        if ( $override !== null ) {
            $parsed['action'] = $override;
        }

        $methodExists = method_exists($className, $parsed['action']);

        if ( ! $parsed || ! $className || ! $methodExists ) {
            return null;
        }

        return $parsed;
    }

}
