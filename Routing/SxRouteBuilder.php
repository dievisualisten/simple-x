<?php

namespace App\Routing;

use Cake\Routing\Route\Route;
use Cake\Routing\RouteBuilder;
use App\Routing\Route\ControllerRoute;

class SxRouteBuilder extends RouteBuilder
{

    protected static $_resourceMap = [
        'index' => [
            'action' => 'index', 'method' => 'GET', 'path' => ''
        ],
        'total' => [
            'action' => 'total', 'method' => 'GET', 'path' => ''
        ],
        'create' => [
            'action' => 'save', 'method' => 'POST', 'path' => ''
        ],
        'view' => [
            'action' => 'load', 'method' => 'GET', 'path' => ':id'
        ],
        'update' => [
            'action' => 'save', 'method' => [ 'PUT', 'PATCH','POST' ], 'path' => ':id'
        ],
        'delete' => [
            'action' => 'delete', 'method' => 'DELETE', 'path' => 'delete'
        ],
    ];

    public function locale($params, $callback = null): void
    {
        $locales = (array) config('Sx.app.language.accept');

        $defaultRouteClass = $this->getRouteClass();

        $this->setRouteClass(ControllerRoute::class);

        foreach ($locales as $locale) {
            parent::scope('/' . $locale, $params, $callback);
        }

        parent::scope('/', $params, $callback);

        $this->setRouteClass($defaultRouteClass);
    }

    public function scope($path, $params, $callback = null)
    {
        $paths = [];

        if ( ! preg_match_all('/\/[a-z0-9\_\-\*\:]+\?/i', $path, $matches) ) {
            return parent::scope($path, $params, $callback);
        }

        $matches = array_merge(
            array_reverse($matches[0]), [null]
        );

        foreach ( $matches as $match ) {

            $paths[] = $this->scope(
                str_replace('?', '', $path), $params, $callback
            );

            $path = str_replace($match, '', $path);
        }

        return $paths;
    }

    public function connect($route, $defaults = [], array $options = []): Route
    {
        $routes = [];

        if ( ! preg_match_all('/\/[a-z0-9\_\-\*\:]+\?/i', $route, $matches) ) {
            return parent::connect($route, $defaults, $options);
        }

        $matches = array_merge(
            array_reverse($matches[0]), [null]
        );

        foreach ( $matches as $match ) {

            $routes[] = parent::connect(
                str_replace('?', '', $route), $defaults, $options
            );

            $route = str_replace($match, '', $route);
        }

        return reset($routes);
    }

}
