<?php

namespace App\Controller;

use Cake\Event\EventInterface;
use App\Controller\Component\System\RequestHandlerComponent;
use Cake\Http\Response;

/**
 * @version 4.0.0
 */

class SxMaintenanceController extends DisplayController
{
    /**
     * @inheritdoc
     */
    public function initialize(): void
    {
        $this->loadComponent('RequestHandler', [
            'className' => RequestHandlerComponent::class
        ]);

        parent::initialize();
    }

    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * @inheritdoc
     */
    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
    }

    /**
     * @inheritdoc
     */
    public function render(?string $template = null, ?string $layout = null): Response
    {
        $this->viewBuilder()
            ->setLayout('maintenance')
            ->setTemplatePath('Maintenance');

        return parent::render($template, $layout);
    }

    /**
     * @inheritdoc
     */
    public function afterFilter(EventInterface $event)
    {
        // parent::afterFilter($event);
    }

}
