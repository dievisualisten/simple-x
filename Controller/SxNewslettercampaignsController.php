<?php

namespace App\Controller;

use Cake\Event\Event;

class SxNewslettercampaignsController extends CrudController
{

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['counter', 'rd']);
        parent::beforeFilter($event);
    }

    public function counter($id)
    {
        $campaign = $this->Newslettercampaigns->find()->contain('Newsletters')->where([
            'Newslettercampaigns.id' => $id,
            'Newslettercampaigns.read' => false,
        ])->first();

        if (!empty($campaign)) {
            $campaign->read = true;
            $campaign->readdate = date("Y-m-d H:i:s");
            $this->Newslettercampaigns->save($campaign);
        }

        $this->response->file(WWW_ROOT . 'img' . DS . 'email' . DS . 'newsletter' . DS . 'tracking_pixel.gif');

        return $this->response;
    }

    public function rd($id)
    {
        $campaign = $this->Newslettercampaigns->find()->contain('Newsletters')->where(['Newslettercampaigns.id' => $id])->first();

        if (!empty($campaign) && !$campaign->clicked) {
            $campaign->clicked = true;
            $this->Newslettercampaigns->save($campaign);
        }

        $url = $_GET['url'];
        $urlpieces = parse_url($url, PHP_URL_PATH);

        $analytics = "utm_source=newsletter&utm_medium=email&utm_campaign=" . urlencode($campaign->newsletter->name);
        if (empty($urlpieces['query'])) {
            $url = $url . '?' . $analytics;
        } else {
            $url = $url . '&' . $analytics;
        }
        $this->redirect($url);
    }

}
