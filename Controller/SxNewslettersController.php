<?php

namespace App\Controller;

use App\Library\DomainManager;
use App\Library\MailerService;
use App\Library\RouterService;
use App\Library\ServiceManager;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\Filesystem\Folder;
use Cake\Utility\Hash;

class SxNewslettersController extends CrudController
{
    public $uses = [
        'Newsletter',
        'Newsletterrecipient',
        'Menu',
    ];

    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions([
            'view', 'webview'
        ]);

        parent::beforeFilter($event);
    }

    public function afterLoad($data)
    {
        $this->loadModel('Menus');

        if ( $data->isNew() ) {
            $data->menu = null;
        }

        $parents = $this->Menus->getParentPaths(
            array_filter([$data->menu]), 'id', false);

        $parentPaths = [];

        foreach( $parents as $tab ) {
            $parentPaths = array_merge($parentPaths, explode('/', $tab->parent_path));
        }

        $data->menu_expanded = array_values(
            array_filter(array_unique($parentPaths))
        );

        return $data;
    }

    public function save($id = null)
    {

        if (!empty($this->request->data['sourceparents'])) {
            $this->request->data['menu_id'] = $this->request->data['sourceparents'][0]['id'];
        }
        parent::save($id);
    }

    public function save_old($id = null)
    {

        if (!empty($this->request->data['Quellmenu'])) {
            $this->request->data['Newsletter']['menu_id'] = $this->request->data['Quellmenu'];
        } else {
            $this->request->data['Newsletter']['menu_id'] = null;
        }

        unset($this->request->data['Menu']);

        if ($this->isTrueOrFalseRainbow($this->Newsletter->saveAll($this->request->data, ['validate' => 'first', 'atomic' => false]))) {
            $this->Newsletter->commit();
            $this->flashSuccess();
            $data = $this->Newsletter->read(null, $this->Newsletter->id);
            $data['Quellmenu'] = $this->Menus->getParentPaths([$data['Menu']], 'id');
            $this->set('data', $data);
        } else {
            $this->Newsletter->rollback();
            $this->flashError();
        }
    }

    public function sendtest()
    {
        $send = false;

        $q = $this->Newsletters->find();
        $data = $q->where(['Newsletters.id' => $this->request->data['id']])->contain(['Menus', 'Emails'])->first();

        if (empty($data)) {
            throw new NotFoundException();
        }

        if (!empty($data->language)) {
            Configure::write('Config.language', $data->language);
        }

        $article = $this->PageBuilder->articleView($data->menu->foreign_key, $data->menu->id, $data->menu->type);

        if ($article->_locale != Configure::read('Config.language')) {
            $this->flashError(__d('system', "Den gewählten Artikel gibt es nicht auf der gewählten Sprache."));
        } else {


            $send = MailerService::sendMail([
                'to' => $data->testreceiveremail,
                'layout' => 'blank',
                'template' => 'newsletter' . DS . $data->template,
                'subject' => $data->subject,
                'base_href' => request()->getDomainFromPath($data->menu),
                'emailConfig' => $data->email,
                'viewVars' => Hash::merge($this->viewVars, [
                    'newsletterrecipient' => [
                        'newslettercampaigns' => ['id' => 'test'],
                        'newsletterrecipients' => ['id' => 'test-do-not-unsubscribe'],
                    ],
                    'newsletter' => $data,
                ]),
            ]);

            if ($send) {
                $this->flashSuccess(__d('system', "Der Test-Newsletter wurde versandt."));
            } else {
                $this->flashError(__d('system', "Der Newsletter konnte nicht versandt werden."));
            }
        }
    }

    public function webview($id)
    {
        $q = $this->Newsletters->find();
        $data = $q->where(['Newsletters.id' => $id])->contain('Menus')->first();

        if (empty($data)) {
            throw new NotFoundException();
        }


        $this->Pagebuilder->articleView($data->menu->foreign_key, $data->menu->id, $data->menu->type);

        $this->set('base_href', request()->getDomainFromPath($data->menu));
        $this->set('newslettercampaign', []);
        $this->set('newsletter', $data);

        $this->viewBuilder()->layout('blank');
        $this->viewBuilder()->template('webview');

    }

    public function getTemplateDomain()
    {

        $folder = new Folder(APP . 'Template' . DS . 'Email' . DS . 'html' . DS . 'newsletter');
        $contents = $folder->find('.*', true);

        $templates = [];

        foreach ($contents as $t) {

            $name = explode('.', $t);

            if (count($name) == 2 && $name[1] == 'ctp') {
                $templates[] = ucfirst($name[0]);
            }
        }

        $this->set('data', DomainManager::format($templates));
    }

}
