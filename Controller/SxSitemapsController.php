<?php

namespace App\Controller;

use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;

/**
 * @version 4.0.0
 */

class SxSitemapsController extends CrudController
{
    protected $limit = 40000;

    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions([
            'index', 'sitemap', 'robots',
        ]);

        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        return parent::beforeFilter($event);
    }

    private function getAllMenuUrls($count = true, $page = 1, $language = null)
    {
        if ( empty($language) ) {
            $language = language()->getLanguage();
        }

        language()->setLanguage($language);

        $menuTable = TableRegistry::getTableLocator()->get('Menus');

        $domain = menu()->getDomainRecord();

        if ( empty($domain) ) {
            return $count ? 0 : [];
        }

        $query = $menuTable->find('published')->where([
            'Menus.teaser NOT IN' => TEASER_WITHOUT_URL,
            'Menus.type NOT IN' => ['Menu', 'Domain', 'Link', 'Alias'],
            'Menus.lft >' => $domain->get('lft'),
            'Menus.rght <' => $domain->get('rght'),
            'Articles.noindex' => false,
        ])->order('Menus.lft asc')->contain('Articles.Attachments');

        if ( language()->getDefaultLanguage() !== $language ) {
            $query->where([
                'MenusTranslation.locale' => $language,
            ]);
        }


        //find all urls under an unpublished page
        $offlineUrlsQuery = $menuTable->find('unpublished')->where([
            'Menus.lft >' => $domain->get('lft'),
            'Menus.rght <' => $domain->get('rght'),
        ]);

        if ( language()->getDefaultLanguage() !== $language ) {
            $offlineUrlsQuery->where([
                'MenusTranslation.locale' => $language,
            ]);
        }


        $offlineUrls = $offlineUrlsQuery->select('path')->extract('path')->toArray();

        foreach ( $offlineUrls as $offlineUrl ) {
            if ( language()->getDefaultLanguage() === $language ) {
                $query->where([
                    'Menus.path NOT LIKE' => $offlineUrl . '%',
                ]);
            } else {
                $query->where([
                    'MenusTranslation.path NOT LIKE' => $offlineUrl . '%',
                ]);
            }
        }

        //Fixme ZS, denied Urls via Resourcen nicht in die Sitemap

        if ( $count ) {
            return $query->count();
        } else {

            $query->limit($this->limit)->page((int) ($page < 1 ? 1 : $page));

            $data = $query->toArray();

            //TODO ZS test
            foreach ( $data as $k => $item ) {
                $p = explode('://', $item->get('article.canonical'));

                if ( count($p) > 1 ) {
                    $item->set('article.canonical', $p[1]);
                }

                if ( ! empty($item->get('article.canonical')) && $item->get('article.canonical') !== $item->get('path') ) {
                    unset ($data[$k]);
                }
            }

            return $data;
        }
    }

    /*
     * render a sitemapindex XML
     */
    public function index()
    {
        $counter = [];
        $languages = language()->getAllowedLanguages();

        foreach ( $languages as $language ) {
            $counter[$language] = ceil($this->getAllMenuUrls(true, 1, $language) / $this->limit);
        }

        $this->set('counter', $counter);
    }

    /*
    * render a sitmap XML
    */

    public function sitemap($page = 1)
    {
        $this->set('data', $this->getAllMenuUrls(false, $page));
    }

    /*
    * This make a simple robot.txt file use it if you don't have your own
    */
    public function robots()
    {
        $expire = 25920000;
        header('Date: ' . date("D, j M Y G:i:s ", time()) . ' GMT');
        header('Expires: ' . gmdate("D, d M Y H:i:s", time() + $expire) . ' GMT');
        header('Content-Type: text/plain');
        header('Cache-Control: max-age=' . $expire . ', s-maxage=' . $expire . ', must-revalidate, proxy-revalidate');
        header('Pragma: nocache');
    }
}
