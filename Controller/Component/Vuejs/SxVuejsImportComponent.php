<?php

namespace App\Controller\Component\Vuejs;

use Cake\Controller\Component;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\BelongsToMany;
use App\Controller\Component\Vuejs\Traits\ImportExportOptionsTrait;
use App\Controller\Component\Vuejs\Traits\LoadModelClassTrait;
use App\Controller\Component\Vuejs\Traits\LoadSpreadsheetTrait;

class SxVuejsImportComponent extends Component
{
    use LoadModelClassTrait;
    use LoadSpreadsheetTrait;
    use ImportExportOptionsTrait;

    protected $filtersPath = 'Spreadsheet.filters';

    public function initialize(array $config = []): void
    {
        $controller = $this->_registry->getController();

        $this->loadModelClass($controller->getModelClass());
    }

    public function import($callback, $file, $options = [])
    {
        $this->loadSpreadsheetInstance($file);

        $this->setOptions($options);

        $items = $this->getActiveSpreadsheet()->toArray();

        if ( ! is_array($items) || count($items) <= 1 ) {
            throw new \Exception('File not accepted for import');
        }

        $keys = array_splice($items, 0, 1)[0];

        foreach ( $keys as $index => $key ) {

            $columnKeys = array_flip(
                $this->getOption('columnNames', [])
            );

            $keys[$index] = @$columnKeys[$key] ?: $key;
        }

        $errors = [];

        foreach ( $items as $index => $item ) {

            $item = collect($keys)->combine($item);

            $item = $item->map([
                $this, 'replaceFilters'
            ]);

            $item = $item->map([
                $this, 'solveManyRelation'
            ]);

            $item = array_undot($item);

            $itemEntity = $this->modelTable->firstOrNew(
                $item, $this->getOption('contain', []), ['validate' => false]
            );

            if ( $this->getOption('replaceModel', false) ) {
                $itemEntity->clear();
            }

            $this->modelTable->patchEntity($itemEntity, $item);

            $itemEntity = call_user_func_array($callback, [
                $itemEntity, $this->modelTable
            ]);

            $items[$index] = $this->modelTable->save($itemEntity);

            if ( $items[$index] === false ) {
                $errors[$index] = ['item' => $item, 'errors' => $itemEntity->errors()];
            }

            continue;
        }

        return [$items, $errors];
    }

    public function replaceFilters($value, $key)
    {
        $explode = explode('.', $key);

        $model = $this->_walkModelRelation(
            $this->modelTable, $explode
        );

        $type = $model->getSchema()->getColumnType(
            array_last($explode)
        );

        $config = config("{$this->filtersPath}.{$type}", []);

        foreach ( $config as $filter) {

            $escaped = preg_quote($filter[1]);

            if ( ! preg_match("/^{$escaped}$/i", $value) ) {
                continue;
            }

            $value = $filter[0];
        }

        return format_type($type, $value);
    }

    public function solveManyRelation($value, $key)
    {
        $explode = explode('.', $key);

        $relation = $this->_walkModelRelation(
            $this->modelTable, $explode
        );

        if (
            ! is_a($relation, HasMany::class) &&
            ! is_a($relation, BelongsToMany::class)
        ) {
            return $value;
        }

        $model = $relation->getTarget();

        $displayField = $this->getOption(
            "behavior.{$key}", $model->getDisplayField()
        );

        $items = array_filter(
            preg_split('/(?<!\\\)(,)/', $value)
        );

        foreach ($items as $index => $item) {

            $item = str_replace(
                '\,', ',', trim($item)
            );

            $entity = $model->firstOrNew([
                $displayField => $item
            ]);

            if ( $entity->isNew() ) {
                $model->save($entity);
            }

            $items[$index] = $entity->toArray();
        }

        return $items;
    }

}
