<?php

namespace App\Controller\Component\Vuejs\Traits;

use Cake\ORM\TableRegistry;

/**
 * @version 4.0.0
 */

trait LoadModelClassTrait
{
    protected $modelClass;

    protected $modelTable;

    protected function loadModelClass($modelClass = null)
    {
        if ( $modelClass != null ) {
            $this->modelClass = $modelClass;
        }

        $this->modelTable = TableRegistry::getTableLocator()->get($this->modelClass);
    }

    protected function _walkModelRelation($model, $keys, $index = 0)
    {
        if ( ! isset($keys[$index]) ) {
            return $model;
        }

        $buffer = $model->associations()->getByProperty($keys[$index]);

        if ( $buffer == null ) {
            return $model;
        }

        return $this->_walkModelRelation($buffer, $keys, $index + 1);
    }

    public function getTable()
    {
        return $this->modelTable;
    }

}
