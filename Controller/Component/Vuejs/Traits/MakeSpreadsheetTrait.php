<?php

namespace App\Controller\Component\Vuejs\Traits;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * @version 4.0.0
 */

trait MakeSpreadsheetTrait
{
    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    protected $spreadsheetInstance;

    protected $spreadsheetFile = 'Spreadsheet';

    protected function makeSpreadsheetInstance($spreadsheetFile)
    {
        if ( $spreadsheetFile != null ) {
            $this->spreadsheetFile = $spreadsheetFile;
        }

        $this->spreadsheetInstance = new Spreadsheet();
    }

    public function setDefaultColumnWidth($value = 20)
    {
        $this->getActiveSpreadsheet()->getDefaultColumnDimension()->setWidth($value);
    }

    public function getActiveSpreadsheet()
    {
        return $this->spreadsheetInstance->getActiveSheet();
    }

    public function fillActiveSpreadsheet($data, $null = null, $start = 'A1')
    {
        $this->getActiveSpreadsheet()->fromArray($data, $null, $start);
    }

    public function exportSpreadsheet($data = null, $extension = 'xlsx')
    {
        if ($data != null) {
            $this->fillActiveSpreadsheet($data);
        }

        $writer = IOFactory::createWriter(
            $this->spreadsheetInstance, ucfirst($extension)
        );

        $writer->save(
            $filePath = CACHE . $this->spreadsheetFile . '.' . $extension
        );

        return $filePath;
    }

}
