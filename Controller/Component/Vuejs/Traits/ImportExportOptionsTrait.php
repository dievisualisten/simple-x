<?php

namespace App\Controller\Component\Vuejs\Traits;

/**
 * @version 4.0.0
 */

trait ImportExportOptionsTrait
{

    protected $options = [

        /**
         * Include virtual fields
         */
        'includeVirtual' => false,

        /**
         * Strategy to get fields (only|except)
         */
        'strategy' => 'except',

        /**
         * Fields which shoud be included or rejected
         */
        'fields' => [
            // 'user_id', 'user.id',
        ],

        /**
         * Rename columns
         */
        'columnNames' => [
            // 'user.id' => 'ID',
        ],

        /**
         * Set behavior for many relations
         */
        'behavior' => [
            // 'menus' => 'id',
        ],

    ];

    public function setOptions($options)
    {
        $this->options = array_merge(
            $this->options, $this->modelTable->getExportDefaults(), $options
        );
    }

    public function getOptions()
    {
        if ( ! isset($this->options['contain']) ) {
            $this->options['contain'] = $this->modelTable->getDefaultContains();
        }

        return $this->options;
    }

    public function setOption($key, $value)
    {
        array_set($this->options, $key, $value);
    }

    public function getOption($key, $default = null)
    {
        return array_get($this->getOptions(), $key, $default);
    }

}
