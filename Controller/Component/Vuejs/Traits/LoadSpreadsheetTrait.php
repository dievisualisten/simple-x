<?php

namespace App\Controller\Component\Vuejs\Traits;

use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * @version 4.0.0
 */

trait LoadSpreadsheetTrait
{
    protected $spreadsheetInstance;

    protected $spreadsheetFile;

    protected function loadSpreadsheetInstance($spreadsheetFile)
    {
        if ( $spreadsheetFile != null ) {
            $this->spreadsheetFile = $spreadsheetFile;
        }

        $this->spreadsheetInstance = IOFactory::load($this->spreadsheetFile);
    }

    public function getActiveSpreadsheet()
    {
        return $this->spreadsheetInstance->getActiveSheet();
    }

}
