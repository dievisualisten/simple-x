<?php

namespace App\Controller\Component\Vuejs;

use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use App\Controller\Component\Vuejs\Traits\ImportExportOptionsTrait;
use App\Controller\Component\Vuejs\Traits\LoadModelClassTrait;
use App\Controller\Component\Vuejs\Traits\MakeSpreadsheetTrait;

class SxVuejsExportComponent extends Component
{
    use LoadModelClassTrait;
    use MakeSpreadsheetTrait;
    use ImportExportOptionsTrait;

    protected $filtersPath = 'Spreadsheet.filters';

    public function initialize(array $config = []): void
    {
        $controller = $this->_registry->getController();

        $this->loadModelClass($controller->getModelClass());
        $this->makeSpreadsheetInstance($controller->getModelClass());
    }

    public function export($query, $extension = 'xlsx', $options = [])
    {
        $this->setOptions($options);

        $entities = $query->contain($this->getOption('contain', []))
            ->all();

        $items = [];

        foreach ( $entities as $index => $entity ) {

            $item = collect($entity->toArray());

            if ( ! $this->getOption('includeVirtual') ) {
                $item = $item->except(
                    $entity->getVirtual()
                );
            }

            if ( $this->getOption('strategy') == 'only' ) {
                $item = $item->only(
                    $this->getOption('fields', [])
                );
            }

            if ( $this->getOption('strategy') == 'except' ) {
                $item = $item->except(
                    $this->getOption('fields', [])
                );
            }

            $item = $item->map([
                $this, 'solveOneRelation'
            ]);

            $item = $item->map([
                $this, 'solveManyRelation'
            ]);

            $item = $item->map([
                $this, 'checkRelationVirtuals'
            ]);

            $item = collect(
                array_dot($item->toArray())
            );

            $item = $item->map([
                $this, 'replaceFilters'
            ]);

            if ( $this->getOption('strategy') == 'only' ) {
                $item = $item->only($this->getOption('fields', []));
            }

            if ( $this->getOption('strategy') == 'except' ) {
                $item = $item->except($this->getOption('fields', []));
            }

            $items[$index] = $item->toArray();
        }

        $keys = array_keys(@$items[0] ?: []);

        if ( empty($keys) && $this->getOption('strategy') == 'only' ) {
            $keys = $this->getOption('fields', []);
        }

        if ( empty($keys) && $this->getOption('strategy') == 'except' ) {
            $keys = array_diff(
                $query->getRepository()->getSchema()->columns(),
                $this->getOption('fields', [])
            );
        }

        return $this->exportSpreadsheet(
            array_merge([$keys], $items), $extension
        );
    }

    public function replaceFilters($value, $key)
    {
        $explode = explode('.', $key);

        $model = $this->_walkModelRelation(
            $this->modelTable, $explode
        );

        $type = $model->getSchema()->getColumnType(
            array_last($explode)
        );

        $config = config("{$this->filtersPath}.{$type}", []);

        foreach ( $config as $filter) {

            if ( format_type($type, $filter[0]) != $value ) {
                continue;
            }

            return $filter[1];
        }

        return $value;
    }

    public function solveOneRelation($value, $key)
    {
        $explode = explode('.', $key);

        $relation = $this->_walkModelRelation(
            $this->modelTable, $explode
        );

        if (
            ! is_a($relation, HasOne::class) &&
            ! is_a($relation, BelongsTo::class)
        ) {
            return $value;
        }

        // Get model from relation
        $model = $relation->getTarget();

        // Get column fields
        $fields = $model->getSchema()->columns();

        if ( $this->getOption('includeVirtual') == true ) {
            $fields += $model->newEntity()->getVirtual();
        }

        $result = [];

        foreach ($fields as $index ) {
            $result[$index] = @$value[$index] ?: null;
        }

        return $result;
    }

    public function solveManyRelation($value, $key)
    {
        $explode = explode('.', $key);

        $relation = $this->_walkModelRelation(
            $this->modelTable, $explode
        );

        if (
            ! is_a($relation, HasMany::class) &&
            ! is_a($relation, BelongsToMany::class)
        ) {
            return $value;
        }

        $model = $relation->getTarget();

        $displayField = $this->getOption(
            "behavior.{$key}", $model->getDisplayField()
        );

        $results = collect($value)->pluck($displayField)->toArray();

        foreach ( $results as $index => $item ) {
            $results[$index] = str_replace(',', '\,', $item);
        }

        return implode(',', $results);
    }

    public function checkRelationVirtuals($value) {

        if ( ! is_a($value, Entity::class) ) {
            return $value;
        }

        if ( $this->getOption('includeVirtual') == true ) {
            return $value;
        }

        return collect($value)->except($value->getVirtual())->toArray();
    }

}
