<?php

namespace App\Controller\Component\Vuejs;

use Cake\Controller\Component;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class SxVuejsQueryComponent extends Component
{
    protected $controller;

    protected $Table;

    protected $query;

    public function initialize(array $config = []): void
    {
        $this->controller = $this->_registry->getController();

        $this->Table = TableRegistry::getTableLocator()->get($this->controller->getModelClass());
    }

    public function getTable()
    {
        return $this->Table;
    }

    public function query($query = null)
    {
        return $query ?: $this->query ?: $this->Table->find()->contain(
            $this->Table->getDefaultContains()
        );
    }

    public function tree($query = null, $steady = false, $options = [])
    {
        $request = $this->getRequest($steady);

        if ( ! $this->Table->hasBehavior('Translation') ) {
            return $this->dynamicTree($query, $steady, $options);
        }

        if ( $query == null ) {
            $query = $this->Table->find('threaded');
        }

        $primaryKey = $this->Table->getAlias() . '.' .
            $this->Table->getPrimaryKey();

        $options += [
            'searchFields' => $this->Table->getDefaultSearchFields()
        ];

        $search = $request->get('search', null);

        $conditions = [
            "{$primaryKey} IS" => $request->get('parent_id', null)
        ];

        $parent = $this->Table->find()
            ->where($conditions)->first();

        if ( $search && count($options['searchFields']) !== 0 ) {

            $conditions = [];

            foreach ( $options['searchFields'] as $field ) {
                array_push($conditions, [
                    $this->Table->getAlias() . '.' . $field . ' LIKE' => '%' . trim($search) . '%',
                ]);
            }

            $subquery = $this->Table->find()->where(function (QueryExpression $exp) use ($conditions) {
                return $exp->or_($conditions);
            });

            $ids = [];

            foreach ( $subquery->all() as $entity ) {

                $path = $this->Table->find('path', [
                    'for' => $entity->get($this->Table->getPrimaryKey())
                ]);

                $ids = array_merge($ids, Hash::extract($path->toArray(),
                    '{n}.' . $this->Table->getPrimaryKey()));
            }

            $query->where([
                $this->Table->getAlias() . '.' . $this->Table->getPrimaryKey() .
                    (! empty($ids) ? ' IN' : ' IS') => $ids ?: null
            ]);
        }

        if ( $parent !== null ) {
            $query->where([
                $this->Table->getAlias() . '.lft >' => $parent->lft,
                $this->Table->getAlias() . '.lft <' => $parent->rght,
            ]);
        }

        return $query->order(['lft' => 'ASC'])->toArray();
    }

    public function dynamicTree($query = null, $steady = false, $options = [])
    {
        $primaryKey = $this->Table->getPrimaryKey();
        $displayKey = $this->Table->getDisplayField();

        $query = $this->filter($query, $steady, $options)
            ->order([
                "LENGTH({$this->Table->getAlias()}.{$displayKey})" => 'ASC'
            ], true);

        $treeDisplay = $this->Table->getTreeDisplay();

        $treeStructure = [];

        foreach ( $query->toArray() as $entity ) {
            data_set($treeStructure, str_replace('.', '.children.', $entity->get($treeDisplay)), $entity);
        }

        $treeStructure = array_map_recursive_keys($treeStructure, 'children', function ($value, $key, $path) use ($treeDisplay, $primaryKey) {

            $value[$treeDisplay] = preg_replace('/(.*?)(?<=\.)([^\.]+)$/',
                '$2', @$value[$treeDisplay] ?: $key);

           if ( ! isset($value[$primaryKey]) ) {
               $value[$primaryKey] = implode('.', $path);
               $value['_virtual'] = true;
           }

           return $value;
        });

        return $treeStructure;
    }

    public function filter($query = null, $steady = false, $options = [])
    {
        $request = $this->getRequest($steady);

        $options += [
            'searchFields' => $this->Table->getDefaultSearchFields(),
        ];

        $query = $this->query($query)->order([
            $this->Table->getAlias() . '.' . $this->Table->getPrimaryKey() => 'ASC',
        ]);

        if ( $request->has('sort') ) {

            $order = collect($request['sort'])->flatMap(function ($value) {
                return [
                    $this->getProperty($value['property']) => $value['direction']
                ];
            });

            $query = $query->order($order->toArray(), true);
        }

        if ( $request->has('filter') && count($request['filter']) != 0 ) {

            foreach ( $request['filter'] as $filter ) {

                if ( $filter['operator'] == 'li' ) {
                    $filter['value'] = '%' . $filter['value'] . '%';
                }

                if ( $filter['operator'] == 'nl' ) {
                    $filter['value'] = '%' . $filter['value'] . '%';
                }

                if ( $filter['operator'] == 'in' && ! is_array($filter['value']) ) {
                    $filter['value'] = explode(',', $filter['value']);
                }

                if ( $filter['operator'] == 'ni' && ! is_array($filter['value']) ) {
                    $filter['value'] = explode(',', $filter['value']);
                }

                if ( $filter['type'] == 'date' ) {
                    $filter['property'] = "DATE_FORMAT({$filter['property']}, \"%d.%m.%Y\")";
                }

                $query = $query->andWhere(function ($exp) use ($filter) {

                    $filter['property'] = $this->getProperty($filter['property']);

                    if ( $filter['operator'] == 'li' ) {
                        return $exp->like($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'nl' ) {
                        return $exp->notLike($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'ne' ) {
                        return $exp->notEq($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'eq' ) {
                        return $exp->eq($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'lt' ) {
                        return $exp->lt($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'gt' ) {
                        return $exp->gt($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'in' ) {
                        return $exp->in($filter['property'], $filter['value']);
                    }

                    if ( $filter['operator'] == 'ni' ) {
                        return $exp->notIn($filter['property'], $filter['value']);
                    }

                    return $exp->and_([ $filter['property'] => $filter['value'] ]);
                });
            }

        }

        if ( $request->get('search') && count($options['searchFields']) != 0 ) {

            $conditions = [];

            foreach ( $options['searchFields'] as $field ) {
                array_push($conditions, [
                    $this->getProperty($field) . ' LIKE' => '%' . trim($request['search']) . '%',
                ]);
            }

            $query = $query->andWhere(function (QueryExpression $exp) use ($conditions) {
                return $exp->or_($conditions);
            });
        }

        return $query;
    }

    public function paginate($query = null, $steady = false)
    {
        $request = $this->getRequest($steady);

        $query = $this->query($query)->page(
            $request->get('page', 1) ?: 1, $request->get('limit', 100)
        );

        return $query;
    }

    public function output($query = null, $steady = false)
    {
        $request = $this->getRequest($steady);

        $output = [];

        $output['data'] = (array)
            $this->paginate($query, $steady)->toArray();

        $output['limit'] = (int)
            $request->get('limit', 100);

        $output['page'] = (int)
            $request->get('page', 1);

        $output['total'] = (int)
            $this->query($query)->count();

        return $output;
    }

    public function getProperty($property)
    {
        if ( ! preg_match('/\./', $property) ) {
            return  $this->Table->getAlias() . '.' . $property;
        }

        $tableKey = preg_replace('/^(.*?)\.(.*?)$/', '$1', $property);

        foreach ( $this->Table->associations() as $association ) {
            if ( $tableKey === $association->getProperty() ) {
                $property = preg_replace('/^(.*?)(?=\.)/', $association->getName(), $property);
            }
        }

        return $property;
    }

    public function getRequest($steady = false)
    {
        $session = $this->controller->getRequest()->getSession()->read(
            $this->Table->getAlias() . 'Query'
        );

        $request = collect(
            $this->controller->getRequest()->getQuery()
        );

        if ( $request->has('sort') && is_string($request['sort']) ) {
            $request['sort'] = json_decode($request['sort'], true);
        }

        if ( $request->has('filter') && is_string($request['filter']) ) {
            $request['filter'] = json_decode($request['filter'], true);
        }

        if ( $steady == true ) {
            $request = collect($session)->merge($request);
        }

        $this->controller->getRequest()->getSession()->write(
            $this->Table->getAlias() . 'Query', $request->toArray()
        );

        return $request;
    }
}
