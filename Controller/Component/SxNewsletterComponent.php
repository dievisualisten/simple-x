<?php

namespace App\Controller\Component;

use App\Library\RouterService;
use Tools\Utility\Text;

App::uses('String', 'Utility');

class SxNewsletterComponent extends Component
{


    public function doJob(Controller $controller)
    {
        $this->controller = $controller;
        $this->controllerData = $this->controller->request->data;


        if (!empty($this->controllerData['Formdata']['email']) && !empty($this->controllerData['Formdata']['newsletter'])) {

            $saveData = [];
            App::import('Model', 'Newsletterrecipient');
            $Newsletterrecipient = ClassRegistry::init('Newsletterrecipient');


            if (!empty($this->controllerData['Formdata']['interessen'])) {
                $this->controllerData['Formdata']['interessen'][] = request()->domain();
            } else {
                $this->controllerData['Formdata']['interessen'] = [request()->domain()];
            }

            //Interes aus den interessen anlegen oder id holen
            if (!empty($this->controllerData['Formdata']['interessen'])) {
                foreach ($this->controllerData['Formdata']['interessen'] as $interest) {

                    $existingInterest = $Newsletterrecipient->Newsletterinterest->find('first', ['conditions' => ['value' => $interest]]);
                    if (empty($existingInterest)) {
                        $Newsletterrecipient->Newsletterinterest->create();
                        $Newsletterrecipient->Newsletterinterest->save(['value' => $interest]);
                        $saveData['Newsletterinterest'][] = $Newsletterrecipient->Newsletterinterest->id;
                    } else {
                        $saveData['Newsletterinterest'][] = $existingInterest['Newsletterinterest']['id'];
                    }
                }
            }

            //Update
            $Newsletterrecipient->contain('Newsletterinterest', 'Person');
            $nr = $Newsletterrecipient->find('first', ['conditions' => ['email' => $this->controllerData['Formdata']['email']]]);

            if (!empty($nr)) {
                $saveData['Newsletterrecipient']['id'] = $nr['Newsletterrecipient']['id'];
                foreach ($nr['Newsletterinterest'] as $interest) {
                    $saveData['Newsletterinterest'][] = $interest['id'];
                }

                $saveData['Newsletterinterest'] = array_unique($saveData['Newsletterinterest']);


                $saveData['Person'] = $nr['Person'];

            } else {
                $saveData['Newsletterrecipient']['ip'] = $_SERVER['REMOTE_ADDR'];
            }

            $saveData['Newsletterrecipient']['email'] = $this->controllerData['Formdata']['email'];
            $saveData['Newsletterrecipient']['path'] = request()->url();
            $saveData['Newsletterrecipient']['language'] = Configure::read('Config.language');

            if (!empty($this->controllerData['Formdata']['firstname']) || !empty($this->controllerData['Formdata']['lastname'])) {
                if (empty($saveData['Person'])) {
                    $saveData['Person'] = [];
                }
                $saveData['Person'] = am($saveData['Person'], $this->controllerData['Formdata']);
                $saveData['Person']['model'] = 'Newsletterrecipient';
            }

            $Newsletterrecipient->begin();
            $Newsletterrecipient->create();

            if ($this->controller->isTrueOrFalseRainbow($Newsletterrecipient->saveAll($saveData, [
                'validate' => 'first',
                'atomic' => false,
            ]))) {
                $Newsletterrecipient->commit();

                $currentdomain = menu()->getDomainRecord();

                $mailcontent = Text::insert(__('newsletter.bestaetigung.email.content'), [
                    'newsletterrecipient_id' => $Newsletterrecipient->id,
                ]);


                $send = SxMailer::sendMail([
                    'mailcontent' => $mailcontent,
                    'to' => $this->controllerData['Formdata']['email'],
                    'subject' => __('newsletter.bestaetigung.email.subject'),
                    'formularConfig' => $this->controllerData['Formularconfig'],
                    'emailConfig' => $this->controllerData['Formularconfig']['Email'],
                ]);


            } else {

                pr('notsaved');
                $Newsletterrecipient->rollback();
            }
        }
    }
}
