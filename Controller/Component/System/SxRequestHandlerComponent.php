<?php

namespace App\Controller\Component\System;

use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventInterface;

/**
 * @version 4.0.0
 */

class SxRequestHandlerComponent extends RequestHandlerComponent
{
    /**
     * @var string $ext
     */
    public $ext = 'html';

    /**
     * @var array $extensionMapping
     */
    protected $extensionMapping = [
        'html' => 'Html', 'json' => 'Json', 'xml' => 'Xml',
    ];

    /**
     * @inheritdoc
     */
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        $config = array_merge($config, [
            'viewClassMap' => $this->extensionMapping,
        ]);

        parent::__construct($registry, $config);
    }

    /**
     * @inheritdoc
     */
    public function startup(EventInterface $event): void
    {
        /** @var \Cake\Controller\Controller $controller */
        $controller = $event->getSubject();

        if ( request()->is(['html', 'htm']) ) {
            $this->_setExtension($controller->getRequest(), $controller->getResponse());
        }

        if ( request()->is('json') ) {
            $controller->setRequest($controller->getRequest()->withParam('isAjax', true));
        }

        if ( array_key_exists(request()->ext(), $this->extensionMapping) ) {
            $this->ext = request()->ext();
        }

        parent::startup($event);
    }

}
