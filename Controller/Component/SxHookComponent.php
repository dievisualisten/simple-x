<?php

namespace App\Controller\Component;

use App\Utility\StringUtility;
use Cake\Controller\Component;
use Cake\Event\Event;

/**
 * @version 4.0.0
 */
class SxHookComponent extends Component
{
    /**
     * Formular default method
     *
     * @var string
     */
    protected $_formularMethod = 'doJob';

    /**
     * Formular namespace
     *
     * @var string
     */
    protected $_formularNamespace = '\\App\\Controller\\Component\\Formular\\%sComponent';

    /**
     * Formular component
     *
     * @var \App\Controller\Component\Formular\FormularComponent
     */
    public $Component;

    public function beforeRender(Event $event)
    {
        $data = $this->getController()->viewBuilder()->getVar('data');

        if ( ! $formular = data_get($data, 'formularconfig.formular') ) {
            return;
        }

        // Get formular component
        $componentName = $formular->get('component', 'Formular');


        // Get default formular method
        $componentMethod = $this->_formularMethod;

        $componentSplit = explode('::', $componentName);

        if ( count($componentSplit) === 2 ) {
            $componentName = $componentSplit[0];
            $componentMethod = $componentSplit[1];
        }


        // Remove component suffix
        $componentName = StringUtility::grizzly(
            preg_replace('/Component$/i', '', $componentName)
        );


        $this->Component = $this->getController()->loadComponent($componentName, [
            'className' => sprintf($this->_formularNamespace, $componentName),
        ]);


        // Startup component
        $this->Component->startup($event);

        // Call formular method
        $this->Component->{$componentMethod}($event);
    }

}
