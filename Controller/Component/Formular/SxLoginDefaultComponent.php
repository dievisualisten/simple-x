<?php

namespace App\Controller\Component\Formular;

use Cake\Event\EventInterface;
use Cake\Http\Exception\NotImplementedException;

/**
 * @version 4.0.0
 */
class SxLoginDefaultComponent extends BaseFormularComponent
{

    /**
     * @var null
     */
    public $authConfigKey = null;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function initialize(array $config): void
    {
        $configs = $this->getIdentifierConfig();

        $this->authConfigKey = data_get($configs, 'authconfig'); // [authconfig => auth.defaultfrontend]

        $authConfig = config('Sx.auth.' . str_replace('auth.', '', $this->authConfigKey));

        if ( empty($authConfig) ) {
            throw new NotImplementedException("Es wurde keine AuthConfig zu {$this->authConfigKey} gefunden. Bitte anlegen.");
        }

        parent::initialize($config);
    }

    /**
     * @return bool
     */
    public function beforeFinalizeCallback(EventInterface $event): bool
    {
        $return = $this->getController()->Authentication->login(null, $this->authConfigKey);

        if ( $return ) {
            $event->stopPropagation();
        };

        return $return;

    }
}
