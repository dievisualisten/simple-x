<?php

namespace App\Controller\Component\Formular;


use Cake\Event\EventInterface;
use Cake\Http\Exception\NotImplementedException;
use Cake\ORM\TableRegistry;


class SxRegisterDefaultComponent extends BaseFormularComponent
{

    /**
     * @var \App\Model\Table\UsersTable
     */
    public $Users = null;

    /**
     * @var string
     */

    public $authConfigKey = null;

    /**
     * @var array
     */
    public $authConfig = [];

    /**
     * @inheritDoc
     */
    public function initialize(array $config): void
    {
        $configs = $this->getIdentifierConfig();

        $this->authConfigKey = data_get($configs, 'authconfig');

        $this->authConfig = config('Sx.auth.' . str_replace('auth.', '', $this->authConfigKey));

        if ( empty($this->authConfig) ) {
            throw new NotImplementedException('Es wurde keine AuthConfig zu ' . $this->authConfigKey . ' gefunden. Bitte anlegen.');
        }

        $this->Users = TableRegistry::getTableLocator()->get('Users');

        parent::initialize($config);
    }

    /**
     * @inheritDoc
     */
    public function modifyRequestData($data = null)
    {

        $configs = $this->getIdentifierConfig();

        if ( empty(data_get($data, 'login')) ) {
            data_set($data, 'login', data_get($data, 'communication.email'));
        }

        //Fixme ZS

        data_set($data, 'aco_id', SHARED_FOLDER_ACO);
        data_set($data, 'type', 'Users');
        data_set($data, 'roles', []);

        return parent::modifyRequestData($data);
    }


    /**
     * @inheritDoc
     */
    public function beforeValidateCallback(EventInterface $event): bool
    {

        $this->getFormDataModel()->getValidator()->add('communication.email', 'isEmailUnique', [
            'rule' => function ($check, $context = []) {
                return (empty($check) || $this->Users->Communications->find()->where([
                        'email' => $check,
                        'email',
                    ])->count() === 0);


            },
            'message' => __('Diese E-Mail-Adresse ist schon registriert. Haben Sie vielleicht nur Ihr Passwort vergessen?'),
        ]);

        return true;
    }


    /**
     * @inheritDoc
     */
    public function save()
    {

        $entity = $this->Users->newEntity($this->getFormDataModel()->getData());


        $return = (bool) $this->Users->save($entity);

        if ( $return === false ) {
            $entity->getErrors();
        }

        return $return;
    }

    /**
     * @return bool
     */
    public function sendFormularMail(): bool
    {
        //Fixme
        return true;
        $config = $this->getIdentifierConfig();
        if ( ! empty($this->userdata) ) {
            $mailcontent = $this->getEmailContent();
            $mailcontent = Text::insert($mailcontent, [
                'salutation' => DomainManager::get('salutation',
                        $this->userdata['Person']['salutation']) . ' ' . $this->userdata['Person']['lastname'],
                'link' => '/' . Configure::read('Config.language') . '/users/approve/' . $this->userdata['User']['id'],
            ]);


            $send = SxMailer::sendMail([
                'mailcontent' => $mailcontent,
                'to' => $this->userdata['Communication']['email'],
                'subject' => $this->getEmailSubject(),
                'formularConfig' => $this->controllerData['Formularconfig'],
                'emailConfig' => $this->controllerData['Formularconfig']['Email'],
            ]);


            if ( ! $send ) {
                $this->log('E-Mail konnte nicht versendet werden', Inflector::slug(__METHOD__));
                $this->log($this->controllerData, Inflector::slug(__METHOD__));
            }
        }

        return parent::_sendFormularMail();


    }
}
