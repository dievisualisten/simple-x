<?php

namespace App\Controller\Component\Formular;

class SxCheckoutComponent extends BaseFormularComponent
{
    //!Achtung, wird beim Extenden nicht gemerged
    public $components = [
        'Session',
        'Pagebuilder',
    ];


    public function startCallback()
    {
        $valid = true;
        //Antwort vom Payment Provider verarbeiten
        $query = $this->controller->request->query;
        if ( isset($query) && isset($query['payment_success']) ) {
            $valid = $this->Shopping->handlePaymentproviderResponse($query, $this);
            if ( $valid ) {
                //hier sollte ab jetzt nur noch die Dankesseite kommen und der Seitenbetreiber über die Bestellung informiert werden.
                $this->_finalize();
            }
        } else {
            if ( ! $this->Session->read('Card.items') ) {
                $this->Session->delete('Card');
                $this->controllerData['Article']['content'] = __('formular.checkout.cardempty.content');
                $this->Pagebuilder->getActionviewArticle('formular/checkout/cardempty', $this->controllerData);
                return false;
            }
        }

        $this->Shopping->reflashErrors();

        return $valid;
    }

    public function afterSetDefaultsCallback(): bool
    {
        $order = $this->Shopping->getOrder();
        if ( ! empty($order) && ! empty($order['Formdata']) ) {
            $this->controllerData['Formdata'] = am($this->controllerData['Formdata'], $order['Formdata']);
            $this->controller->request->data = am($this->controller->request->data, $this->controllerData);
            //Option aus der Session, wenn via Ajax gesetzt, aber Formular nicht abgesendet
            $this->controller->request->data['Formdata']['shippingoption'] = $this->Shopping->getShippingOption();
        }

        return true;
    }

    public function beforeValidateCallback()
    {
        $this->Shopping->deleteOrder();

        return $this->Shopping->validateCard();
    }

    public function afterValidateCallback(): bool
    {
        $valid = $this->Shopping->saveOrder($this->controllerData);
        if ( ! $valid ) {
            //die Fehlermeldung stimmt hier nicht so ganz. Evtl. noch eine weitere Fehlermeldung hinzufügen
            $this->controllerData['Article']['content'] = __('formular.checkout.cardempty.content');
            $this->Pagebuilder->getActionviewArticle('formular/' . strtolower(Inflector::slug($this->controllerData['Formularconfig']['name'], '_')),
                $this->controllerData, false);

            return false;
        }

        //hier findet ggf. ein exit statt, da zum Paymentprovider weitergeleitet wird, rückkehr hierher via payment_success
        //ansonsten ist der Vorgang abgeschlossen
        $valid = $valid & $this->Shopping->handlePaymentproviderRequest($this->controllerData);

        if ( ! $valid ) {
            $this->formDataModel->invalidate('paymentmethod',
                __('Mit dem gewählten Zahlungsanbieter konnte keine Zahlung durchgeführt werden. Bitte versuchen Sie es erneut oder wählen Sie eine andere Zahlungsart.'));
        }

        return $valid;
    }

    public function _finalize()
    {
        $this->controllerData['Order'] = $this->Shopping->getOrder();
        //Zur Verwendung im Tracking
        $this->Session->delete('Card'); //Zum Testen auskomentieren, Warenkorb bleibt erhalten
        parent::_finalize();
    }

}
