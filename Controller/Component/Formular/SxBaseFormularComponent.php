<?php

namespace App\Controller\Component\Formular;

use App\Form\FormdataForm;
use App\Library\Facades\Request;
use Cake\Controller\Component;
use Cake\Event\EventInterface;
use Cake\Utility\Hash;
use Tools\Utility\Text;

/**
 * @version 4.0.0
 */
class SxBaseFormularComponent extends Component
{
    /**
     * The "Model" with data from the Frontendform
     *
     * @var null
     *
     */
    public $formDataModel = null;

    /**
     * The data for the global $data in views
     *
     * @var null
     */
    public $viewData = null;

    /**
     *  The field definitions from the form used
     *
     * @var array
     */
    public $formFields = [];

    /**
     * TODO
     *
     * @var null
     */
    public $attachments = null;

    /**
     * TODO
     *
     * @var array
     */
    public $uploadedFiles = [];

    /**
     * @var array
     * @see EventInterface
     */
    public $event = null;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function initialize(array $config): void
    {
        $this->getController()->loadComponent('FormProtection');
        parent::initialize($config);
    }

    /**
     * @param \Cake\Event\EventInterface $event
     */
    public function beforeFilter(EventInterface $event)
    {

    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return void
     */
    public function startup(EventInterface $event): void
    {

    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return bool
     */
    public function doJob(EventInterface $event): bool
    {
        $this->event = $event;

        // Initialize view data from controller
        $this->initViewData();

        // Initialize form fields from formular
        $this->initFormFields();

        // Initialize formdata model
        $this->initFormDataModel();

        // Save formdata inside controller view
        $this->getController()->set('Formdata', $this->getFormDataModel());

        // Run start callback
        $success = $this->startCallback($event);

        if ( $success && Request::isMethod('get') ) {
            $success &= $this->doJobGet($event);
        }

        if ( $success && Request::isMethod('post') ) {
            $success &= $this->doJobPost($event);
        }

        $success &= $this->doneCallback($event);

        return $success;
    }

    public function doJobGet($event)
    {
        $success = $this->beforeSetDefaultsCallback();

        if ( $success ) {
            $this->setDefaults();
        }

        if ( $success ) {
            $this->setFormValuesFromQueryString();
        }

        $success &= $this->afterSetDefaultsCallback($event);

        return $success;
    }

    public function doJobPost($event)
    {
        $success = true;

        $this->getFormDataModel()->setData($this->modifyRequestData($this->getController()->getRequest()->getData()));

        $success = $success && $this->beforeValidateCallback($event);

        if ( $success ) {

            $valid = $this->getFormDataModel()->validate($this->getFormDataModel()->getData())
                && $this->validate($this->getController()->getRequest()->getData());

            if ( ! $valid ) {
                $this->getController()->flashWarning(__('Es sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingaben.'));
                $success = false;
            }
        }


        $success = $success && $this->validateCaptcha();
        $success = $success && $this->afterValidateCallback($event);
        $success = $success && $this->beforeSaveCallback($event);

        $savedOrErrors = $this->save();

        if ( $savedOrErrors !== true ) {
            $this->getFormDataModel()->addErrors((array) $savedOrErrors);
            $success = false;
        }

        $success = $success && $this->saveFiles();
        $success = $success && $this->afterSaveCallback($event);
        $success = $success && $this->beforeSendFormularMail();
        $success = $success && $this->sendFormularMail();
        $success = $success && $this->beforeFinalizeCallback($event);
        $success = $success && $this->finalize($event);
        $success = $success && $this->afterFinalizeCallback($event);
        return $success;
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return bool
     */
    public function startCallback(EventInterface $event)
    {
        return true;
    }

    public function modifyRequestData($data = null)
    {
        $data = array_map(function ($val) {
            return is_string($val) ? trim($val) : $val;
        }, Hash::flatten($data));

        return Hash::expand($data);
    }

    /**
     * @param \Cake\Event\EventInterface $event
     * @return bool
     */
    public function beforeValidateCallback(EventInterface $event): bool
    {
        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate($data = null): bool
    {
        return true;
    }

    public function validateCaptcha(): bool
    {
        if ( ! $this->getViewData('formularconfig.formular.captcha') ) {
            return true;
        }

        $honeypots = [
            'email_repeat' => __('E-Mail wiederholen'),
            'note_' => __('Notiz'),
            'lastname_' => __('Nachname'),
            'message_' => __('2. Nachricht'),
            'email_' => __('2. E-Mail'),
        ];

        foreach ( $honeypots as $honeypot ) {
            if ( ! empty($this->getController()->getRequest()->getData($honeypot)) ) {
                logger()->log($this->getController()->getRequest()->getData(), 'formular');

                $this->finalize();

                return false;
            }
        }

        return true;
    }

    public function afterValidateCallback(EventInterface $event): bool
    {
        return true;
    }

    public function beforeSaveCallback(EventInterface $event): bool
    {
        return true;
    }

    public function beforeSaveFile($file)
    {
        return $file;
    }

    //Fixme need rework
    protected function saveFiles()
    {

        return true;
    }


    public function afterSaveFile($attachment): bool
    {
        return true;
    }


    public function save()
    {
        return true;
    }

    public function afterSaveCallback(EventInterface $event): bool
    {
        return true;
    }

    public function beforeFinalizeCallback(EventInterface $event): bool
    {
        return true;
    }

    public function beforeSendFormularMail(): bool
    {
        return true;
    }

    /* Fixme need rework*/
    public function sendFormularMail(): bool
    {
        if ( ! $this->getViewData('formularconfig.formular.send_email') ) {
            return true;
        }

        return true;

        $send = true;

        // E-Mail senden an Betreiber der Seite
        if ( $this->getViewData('formularconfig.formular.send_email') ) {

            $attachments = [];
            if ( data_get($this->getViewData(), 'formularconfig.formular.send_attachments') && ! empty($this->uploadedFiles) ) {
                $attachments = (array) $this->uploadedFiles;
            }

            $send = mailer()->sendMail([
                'template' => 'formular',
                'sendAs' => 'html',
                'subject' => $this->getEmailSubjectIntern(),
                'formdata' => $this->getController()->getRequest()->getData(),
                'formularConfig' => data_get($this->getViewData(), 'formularconfig'),
                'emailConfig' => data_get($this->getViewData(), 'formularconfig.email'),
                'attachments' => $attachments,
                'viewVars' => [
                    'data' => $this->getViewData(),
                    'cleanFullUrl' => request()->fullUrl(),
                ],
            ]);

            if ( ! $send ) {
                logger()->error('E-Mail konnte nicht versendet werden');
            }

        }

        return $send;
    }


    protected function finalize(EventInterface $event): bool
    {
        $data = $this->getViewData();

        $identifier = $this->getIdentifier();


        $redirect = $this->getViewData('formularconfig.after_send_redirect');

        if ( ! empty($redirect) ) {
            $event->stopPropagation();

            $redirect .= (parse_url($redirect, PHP_URL_QUERY) ? '&' : '?') . 'formularconfig_id=' . data_get($data, 'formularconfig.id') . '&submitted=1#';
            $this->getController()->redirect($redirect, 303);
        }

        $headline = data_get($data, 'formularconfig.after_send_headline');

        if ( empty ($headline) && $identifier ) {
            $key = 'formular.' . $identifier . '.send.headline';
            $headline = $key !== __($key) ? __($key) : "";
        }

        if ( ! empty($headline) ) {
            data_set($data, 'headline', $headline);
        }

        $content = data_get($data, 'formularconfig.after_send_content');

        if ( empty ($content) && $identifier ) {
            $key = 'formular.' . $identifier . '.send.content';
            $content = $key !== __($key) ? __($key) : "";
        }

        if ( ! empty($content) ) {
            data_set($data, 'content', $content);
        }

        $this->setViewData($data);

        $this->getController()->viewBuilder()->setTemplate('feedback_default');

        return true;
    }

    public function afterFinalizeCallback(EventInterface $event): bool
    {
        return true;
    }

    public function beforeSetDefaultsCallback(): bool
    {
        return true;
    }

    public function setDefaults()
    {
        $data = $this->getFormDataModel()->getData();

        foreach ( $this->getFormFields() as $field ) {


            //some magic defaults
            if ( empty($field['defaultValue']) ) {
                if ( $field['options'] === "language" ) {
                    $data[$field['inputName']] = menu()->getDomainRecord('extended_data.menu.locale') ?: language()->getLanguage();
                }
                if ( $field['options'] === "country" && ! empty(menu()->getDomainRecord('extended_data.menu.country')) ) {
                    $data[$field['inputName']] = menu()->getDomainRecord('extended_data.menu.country');
                }
            } else {
                $data[$field['inputName']] = $field['defaultValue'];
            }
        }


        $this->getFormDataModel()->setData($data);
    }

    public function afterSetDefaultsCallback(EventInterface $event): bool
    {
        return true;
    }

    public function doneCallback(EventInterface $event): bool
    {
        return true;
    }


    //Fixme need rework
    protected function _getAttachmentAcoId($aco_id)
    {

        //App::import('Model', 'Aco');
        $this->Aco = ClassRegistry::init('Aco');

        $path = $this->Aco->getPath($aco_id);
        foreach ( array_reverse($path) as $aco ) {
            $attachment = $this->Attachment->find('first', ['conditons' => ['Attachment . aco_id' => $aco['Aco']['id']]]);
            if ( ! empty($attachment) ) {
                return $attachment['Attachment']['aco_id'];
            }
        }

        return 'upload';
    }

    // save Files
    //Fixme need rework
    public function getAdditionalAttachmentData()
    {
        return [
            'model' => 'Formular',
            'foreign_key' => null,
        ];
    }

    public function getEmailSubjectIntern()
    {
        $ret = data_get($this->viewData, 'formularconfig.sbuject');

        if ( empty($ret) ) {
            $ret = __('formular.formular.email.subject');
        }

        $ret = Text::insert($ret, ['domain' => request()->domain(true)]);

        return $ret;
    }

    /*
     * Set GET params like name=value or person[Lastname] = value or checkboxes[]=1&checkboxes[]=2 or radios=1 as form values
     *
     */
    public function setFormValuesFromQueryString(): void
    {
        $this->getFormDataModel()->setData($this->getController()->getRequest()->getQueryParams());
    }


    public function getIdentifierConfig()
    {
        $identifer = $this->getIdentifier();

        if ( ! empty($identifer) ) {
            return config('Sx.identifier.' . $identifer);
        }

        return [];
    }

    public function getIdentifier()
    {
        //z.B. register
        if ( ! empty(data_get($this->getViewData(), 'formularconfig.identifier')) ) {
            return strtolower(data_get($this->getViewData(), 'formularconfig.identifier'));
        }

        $identifer = strtolower(str_replace('Component', '', str_replace('Formular', '', substr(strrchr(get_class($this), "\\"), 1))));

        if ( empty($identifer) ) {
            $identifer = 'base';
        }

        return $identifer;
    }

    public function getEmailSubject()
    {
        return Text::insert(__('formular.' . $this->getIdentifier() . '.email.subject'), ['domain' => request()->domain(true)]);
    }

    public function getEmailContent()
    {
        return __('formular.' . $this->getIdentifier() . '.email.content');
    }

    /**
     * @return FormdataForm
     */
    public function getFormDataModel()
    {
        return $this->formDataModel;
    }

    /**
     * @param FormdataForm $formDataModel
     */
    public function setFormDataModel(FormdataForm $formDataModel): void
    {
        $this->formDataModel = $formDataModel;
    }

    /**
     */
    public function initFormDataModel(): void
    {
        $this->formDataModel = new FormdataForm();

        $this->formDataModel->setFields($this->getFormFields());
    }

    public function getViewData($key = null, $fallback = null)
    {
        if ( ! empty($key) ) {
            return data_get($this->viewData, $key, $fallback);
        }

        return $this->viewData;
    }

    /**
     * @param null $viewData
     */
    public function setViewData($viewData): void
    {
        $this->viewData = $viewData;

        $this->getController()->viewBuilder()->setVar('data', $this->viewData);
    }

    public function initViewData(): void
    {
        $this->viewData = $this->getController()->viewBuilder()->getVar('data');
    }

    /**
     * @return null
     */
    public function getFormFields()
    {
        return $this->formFields;
    }

    /**
     * @param null $formFields
     */
    public function setFormFields($formFields): void
    {
        $this->formFields = $formFields;
    }

    public function initFormFields(): void
    {
        $this->formFields = $this->getViewData('formularconfig.formular.extended_data.fields', []);
    }

}
