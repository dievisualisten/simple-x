<?php

namespace App\Controller\Component\Formular;

use App\Library\MailerService;
use Tools\Utility\Text;

class SxPwlostDefaultComponent extends BaseFormularComponent
{
    //!Achtung, wird beim extenden nicht gemerged
    public $components = [

        'Pagebuilder',
    ];

    public function sendFormularMail(): bool
    {
        App::import('User', 'Model');

        $this->User = ClassRegistry::init('User');
        $this->User->contain('Communication', 'Person');

        $users = $this->User->find('all', [
            'conditions' => [
                'OR' => [
                    'Communication.email' => $this->controllerData['Formdata']['login'],
                    'User.login' => $this->controllerData['Formdata']['login'],
                ],
            ],
        ]);

        //Es kann sein, dass es mehrer User mit der Selben E-Mail Adresse, gibt (mehr als 2 Loginbereiche). Also bekommen alle eine E-Mail zur Anforderung eines neuen Passwortes.
        foreach ( $users as $user ) {
            $uuid = Text::uuid();


            $mailcontent = $this->getEmailContent();
            $mailcontent = Text::insert($mailcontent,
                [
                    'salutation' => DomainManager::get('salutation', $user['Person']['salutation']) . ' ' . $user['Person']['lastname'],
                    'link' => '/' . Configure::read('Config.language') . '/users/sendnewpw/' . $user['User']['id'] . '/' . $uuid,
                ]);

            $this->User->create();
            $this->User->id = $user['User']['id'];
            $user['User']['auth_id'] = $uuid;

            if ( $this->User->save($user) ) {

                $send = MailerService::sendMail([
                    'mailcontent' => $mailcontent,
                    'to' => $user['Communication']['email'],
                    'subject' => $this->getEmailSubject(),
                    'formularConfig' => $this->controllerData['Formularconfig'],
                    'emailConfig' => $this->controllerData['Formularconfig']['Email'],
                ]);


                if ( ! $send ) {
                    $this->log('E-Mail konnte nicht versendet werden', Inflector::slug(__METHOD__));
                    $this->log($this->controllerData, Inflector::slug(__METHOD__));
                }
            } else {
                $this->log('User konnte nicht gespeichert werden', Inflector::slug(__METHOD__));
                $this->log($user, Inflector::slug(__METHOD__));
            }
        }

        return $send;
    }


}
