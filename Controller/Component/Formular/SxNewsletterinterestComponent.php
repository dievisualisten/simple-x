<?php

namespace App\Controller\Component\Formular;

use App\Library\RouterService;

class SxNewsletterinterestComponent extends BaseFormularComponent
{
    //!Achtung, wird beim extenden nicht gemerged
    public $components = [

        'Pagebuilder',

    ];

    public function beforeSetDefaultsCallback(): bool
    {
        if ( ! isset($this->controller->request->query['nr_id']) ) {
            throw new NotFoundException();
        }

        $id = $this->controller->request->query['nr_id'];
        App::import('Model', 'Newsletterrecipient');
        $Newsletterrecipient = ClassRegistry::init('Newsletterrecipient');
        $Newsletterrecipient->contain('Person', 'Newsletterinterest');
        $nr = $Newsletterrecipient->find('first', ['conditions' => ['Newsletterrecipient.id' => $id]]);

        if ( ! empty($nr) ) {
            $this->controllerData['Formdata'] = Hash::merge($nr['Person'], $nr['Newsletterrecipient']);

            $this->controllerData['Formdata']['nr-id'] = $nr['Newsletterrecipient']['id'];
            unset($this->controllerData['Formdata']['id']);

            foreach ( $nr['Newsletterinterest'] as $int ) {
                $this->controllerData['Formdata']['interessen'][] = $int['value'];
            }
        } else {
            return false;
        }

        return true;
    }


    public function beforeSaveCallback(): bool
    {
        if ( ! empty($this->controllerData['Formdata']['email']) && ! empty($this->controllerData['Formdata']['nr-id']) ) {

            $saveData = [];
            App::import('Model', 'Newsletterrecipient');
            $Newsletterrecipient = ClassRegistry::init('Newsletterrecipient');

            $extData = json_decode($this->controllerData['Formularconfig']['Formular']['extended_data'], true);
            $exludingInterests = [];
            foreach ( $extData['Fields'] as $field ) {
                if ( $field['inputName'] == 'interessen' ) {
                    $temp = explode('|', $field['options']);
                    foreach ( $temp as $int ) {
                        $kv = explode('#', $int);
                        $exludingInterests[] = $kv[0];
                    }
                }
            }

            $exludingInterests = $Newsletterrecipient->Newsletterinterest->find('all', ['conditions' => ['value' => $exludingInterests]]);
            $exludingInterests = Hash::extract($exludingInterests, '{n}.Newsletterinterest.id');

            $newInterests = [];

            if ( ! empty($this->controllerData['Formdata']['interessen']) ) {
                $this->controllerData['Formdata']['interessen'][] = request()->domain();
            } else {
                $this->controllerData['Formdata']['interessen'] = [request()->domain()];
            }


            //Interes aus den interessen anlegen oder id holen
            if ( ! empty($this->controllerData['Formdata']['interessen']) ) {
                foreach ( $this->controllerData['Formdata']['interessen'] as $interest ) {

                    $existingInterest = $Newsletterrecipient->Newsletterinterest->find('first', ['conditions' => ['value' => $interest]]);
                    if ( empty($existingInterest) ) {
                        $Newsletterrecipient->Newsletterinterest->create();
                        $Newsletterrecipient->Newsletterinterest->save(['value' => $interest]);
                        $newInterests[] = $Newsletterrecipient->Newsletterinterest->id;
                    } else {
                        $newInterests[] = $existingInterest['Newsletterinterest']['id'];
                    }
                }
            }

            //Update
            $Newsletterrecipient->contain('Newsletterinterest', 'Person');
            $nr = $Newsletterrecipient->find('first', ['conditions' => ['Newsletterrecipient.id' => $this->controllerData['Formdata']['nr-id']]]);

            if ( ! empty($nr) ) {
                $saveData['Newsletterinterest'] = $newInterests;
                $oldIntr = [];

                foreach ( $nr['Newsletterinterest'] as $oldInt ) {
                    if ( ! in_array($oldInt['id'], $exludingInterests) ) {
                        $saveData['Newsletterinterest'][] = $oldInt['id'];
                    }
                }

                $saveData['Newsletterinterest'] = array_unique($saveData['Newsletterinterest']);

                $saveData['Newsletterrecipient'] = Hash::merge($nr['Newsletterrecipient'], $this->controllerData['Formdata']);
                $saveData['Person'] = Hash::merge($nr['Person'], $this->controllerData['Formdata']);
                $saveData['Person']['model'] = 'Newsletterrecipient';

            } else {
                return false;
            }

            $Newsletterrecipient->begin();
            $Newsletterrecipient->create();

            if ( $this->controller->isTrueOrFalseRainbow($Newsletterrecipient->saveAll($saveData, [
                'validate' => 'first',
                'atomic' => false,
            ])) ) {
                $Newsletterrecipient->commit();

                return true;


            } else {
                return false;
                $Newsletterrecipient->rollback();
            }
        }
    }
}
