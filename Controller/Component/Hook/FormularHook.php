<?php

namespace App\Controller\Component\Hook;

use App\Utility\StringUtility;
use Cake\Datasource\ModelAwareTrait;

/**
 * @version 4.0.0
 */

class FormularHook
{
    use ModelAwareTrait;

    /**
     * @var \App\Controller\AppController
     */
    public $controller;

    /**
     * @var \App\Model\Table\FormularsTable
     */
    protected $Formulars;

    /**
     * @var string
     */
    public $namespace = '\\App\\Controller\\Component\\Formular\\';

    /**
     * FormularHook constructor.
     *
     * @param \App\Controller\AppController $controller
     */
    public function __construct($controller)
    {
        $this->loadModel('formulars');

        $this->controller = $controller;
    }

    /**
     *
     */
    public function boot()
    {
        $form = $this->Formulars->find(
            $this->controller->getRequest()->getData('formular_id')
        );

        if ( empty($form) ) {
            return;
        }

        $componentName = $this->namespace . StringUtility::camel(
            $form->get('formular', 'formular')
        );

        if ( ! class_exists($componentName) ) {
            throw new \Exception("Component class missing \"{$componentName}\"");
        }

        $this->controller->loadComponent($componentName, [
            'formular' => $form
        ]);
    }

}
