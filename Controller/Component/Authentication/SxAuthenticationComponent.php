<?php

namespace App\Controller\Component\Authentication;

use App\Library\AuthManager;
use App\Library\Facades\Request;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Auth\WeakPasswordHasher;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * @version 4.0.0
 */
class SxAuthenticationComponent extends \Authentication\Controller\Component\AuthenticationComponent
{
    /**
     * Global password
     *
     * @var string
     */
    protected $_defaultPassword = null;

    protected $_authConfig = [];

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        if ( empty($this->_defaultPassword) ) {
            $this->_defaultPassword = SUPER_ADMIN_PW;
        }

        $credentials = [
          'username' => ['login'],
          'active'   => true,
        ];

        $this->_authConfig = [
          'credentials'              => $credentials,
          'after_login_redirect_url' => '/' . language()->getLanguage(),
          'error_msg'                => __('Es konnte kein Benutzer zu den angegebenen Daten gefunden werden.'),
          'success_msg'              => __('Willkommen zurück!'),
        ];

        AuthManager::setComponent($this);
    }

    /*
     * @param  mixed|null $authConfig
     *
     * */
    public function login($currentAcoId = null, $authConfigKey = "backend")
    {
        AuthManager::pause();

        $authConfig = array_merge($this->_authConfig, config('Sx.auth.' . str_replace('auth.', '', $authConfigKey)));

        $requestData = $this->getController()->getRequest()->getData();

        $users = TableRegistry::getTableLocator()->get('Users');

        $query = $users->find();

        foreach ( (array) data_get($authConfig, 'credentials.username') as $k => $field ) {
            $credentials[$field] = data_get($requestData, 'login', false);
        }

        $query->where($credentials);

        $user = $query->contain($users->getDefaultContains())->first();

        if ( empty($user) ) {
            return $this->_abortLogin();
        }

        $formPassword = data_get($requestData, 'password', '');

        $passwordHasher = new DefaultPasswordHasher;

        $doesMatchPassword = $passwordHasher->check($formPassword, $this->_defaultPassword) ||
          $passwordHasher->check($formPassword, $user->password);

        if ( ! $doesMatchPassword ) {
            return $this->_abortLogin();
        }

        $user = $users->get($user->get('id'))->set('last_login', Time::now());

        //Legacy, need rehash
//        if ( ! $this->_defaultPassword === data_get($requestData, 'password', '')
//            && (new WeakPasswordHasher)->check(data_get($requestData, 'password', ''), $user->get('password')) ) {
//            $user->set('newpassword', data_get($requestData, 'password', ''));
//        }
//        if ( ! $this->_defaultPassword === data_get($requestData, 'password', '') ) {
//            // Touch last login
//            $users->save($user);
//        }

        $users->save($user);

        $this->setIdentity($user);

        // Cache user with aco_id
        AuthManager::cacheUser($currentAcoId, $authConfigKey);
        AuthManager::setAuthConfig($this->_authConfig);
        $this->_authConfig = AuthManager::getAuthConfig();

        AuthManager::resume();
        $this->getController()->flashSuccess(data_get($this->_authConfig, 'success_msg'));

        if ( Request::is('html') ) {
            $this->getController()->redirect(data_get($this->_authConfig, 'after_login_redirect_url'));
        }

        return true;
    }

    protected function _abortLogin()
    {
        AuthManager::resume();

        $this->getController()->flashError(data_get($this->_authConfig, 'error_msg'));

        return false;
    }


    public function logout(): ?string
    {
        $link = AuthManager::getAuthConfig('after_logout_redirect_url', '/' . (language()->getLanguage() === language()->getDefaultLanguage() ? '' : language()->getLanguage()));

        AuthManager::clearSession();
        parent::logout();

        if ( request()->ext() === 'html' ) {
            $this->getController()->flashSuccess(__('Sie haben sich erfolgreich abgemeldet.'));
        }

        return $this->getController()->redirect($link);
    }

}
