<?php

namespace App\Controller;

use Cake\Event\EventInterface;
use App\Library\AuthManager;

/**
 * @version 4.0.0
 */
class SxUsersController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->Authorization->skipAuthorization([
            'login', 'loginJson',
        ]);

        $this->Authentication->addUnauthenticatedActions([
            'login', 'loginJson', 'logout', 'approve', 'sendnewpw', 'showUser',
        ]);

        parent::beforeFilter($event);
    }

    /**
     * @inheritdoc
     */
    public function beforeCopy($entity)
    {
        $entity->set('login', 'Kopie von ' . $entity->login);

        return $entity;
    }

    /**
     * Render system login
     */
    public function login()
    {
        $this->flashClear();
        $this->viewBuilder()->setLayout('system');
    }

    /**
     * Login user via json, special Router
     *
     * @throws \Exception
     */
    public function loginJson()
    {
        if ( ! $this->Authentication->login(null, 'auth.backend') ) {
            $response = $this->getResponse()->withStatus(403);
            $this->setResponse($response);
        };
    }

    /**
     * Logout user and redirect
     */
    public function logout()
    {
        $this->Authentication->logout();
    }

//    public function approve($id)
//    {
//        if (!empty($id)) {
//            $this->Users->contain();
//            $user = $this->Users->find('first', ['conditions' => ['id' => $id]]);
//            if (!empty($user)) {
//                if (empty($user['Users']['approved_date'])) {
//                    $user['Users']['approved'] = 1;
//                    $user['Users']['approved_date'] = date('Y-m-d H:i');
//                    $this->Users->create();
//                    $this->Users->save($user);
//                    $this->flashSuccess(__('Ihr Zugang wurde aktiviert. Sie können sich nun mit Ihren Benutzerdaten anmelden.'));
//                } else {
//                    $this->flashSuccess(__('Ihr Zugang wurde bereits aktiviert. Bitte melden Sie sich mit Ihren Benutzerdaten an.'));
//                }
//
//                $this->redirect('/' . Configure::read('Config.language') . Configure::read('Sx.login.' . strtolower($user['Users']['type']) . '.loginurl'));
//            } else {
//                throw new NotFoundException();
//            }
//        } else {
//            throw new NotFoundException();
//        }
//    }
//
//    public function sendnewpw($id, $auth_id)
//    {
//        $send = false;
//        if (!empty($id) && !empty($auth_id)) {
//
//            $this->Users->contain('Communication', 'Person');
//            $user = $this->Users->find('first', ['conditions' => ['Users.id' => $id, 'auth_id' => $auth_id]]);
//
//            if (empty($user)) {
//                throw new NotFoundException(__('Der Benutzer wurde nicht gefunden.'));
//            } else {
//                $pw = substr(str_shuffle(strtolower(sha1(rand() . time() . "Der Benutzer möchte ein neues Passwort"))),
//                    0, 8);
//                $link = '/' . Configure::read('Config.language') . '/' . Configure::read('Sx.login.' . strtolower($user['Users']['type']) . '.loginurl');
//
//                $mailcontent = Text::insert(__('user.sendnewpw.email.content'), [
//                    'salutation' => DomainManager::get('salutation',
//                            $user['Person']['salutation']) . ' ' . $user['Person']['lastname'],
//                    'pw' => $pw,
//                    'link' => $link,
//                ]);
//
//                $this->Users->create();
//                $this->Users->id = $user['Users']['id'];
//
//                $user['Users']['auth_id'] = null;
//                $user['Users']['newpassword'] = $pw;
//
//                if ($this->Users->save($user)) {
//
//                    $send = SxMailer::sendMail([
//                        'mailcontent' => $mailcontent,
//                        'to' => $user['Communication']['email'],
//                        'subject' => __('user.sendnewpw.email.subject'),
//                    ]);
//
//                    $this->flashSuccess(__('Wir haben Ihnen ein neues Passwort an Ihre E-Mail Adresse gesendet. Bitte rufen Sie Ihre E-Mails ab.'));
//                }
//                $this->redirect($link);
//            }
//        }
//    }

    public function getByRole()
    {
        App::import('Sanitize');
        $this->Users->bindModel(['hasOne' => ['RolesUsers']], false);
        $q = isset($_REQUEST['query']) ? Sanitize::escape($_REQUEST['query']) : null;
        $this->paginate = $this->Extjs->getPagination($this->Users,
            ['conditions' => ['RolesUsers.role_id' => $q], 'sort' => 'id']);
        $this->paginate = Set::merge($this->paginate, ['contain' => ['SearchIndex', 'RolesUsers']]);
        $this->set('results', $this->paginate());
        $this->set('total', $this->params['paging']['Users']['count']);
    }

    public function getdomain()
    {
        $data = $this->Users->find()->contain(['Persons'])->all();
        $this->set('data', $data);
    }

    public function showUser()
    {
        AuthManager::cacheUser();
        pr($this->request->getSession()->read('AuthManager'));
    }

}
