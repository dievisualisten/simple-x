<?php

namespace App\Controller;

use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use App\Form\FormdataForm;

class SxVisitorsController extends AppController
{
    public $uses = [];

    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->allowUnauthenticated([
            'validate'
        ]);

        parent::beforeFilter($event);
    }

    public function index()
    {

    }

    public function view($id = null)
    {

    }

    public function head()
    {
        $this->Session->write('Visitor.header', $this->getRequest()->getData('state'));
        exit();
    }

    public function map()
    {
        $this->Session->write('Visitor.map', $this->getRequest()->getData('state'));
        exit();
    }

    public function edit($id = null)
    {

    }

    public function del()
    {
        $scope = $key = $value = "";
        $scope = substr($this->request->data['scope'], 0, 255);
        $key = substr($this->request->data['key'], 0, 255);

        if (empty($scope)) {
            $this->Session->delete('Visitor.' . $key);
        } else {
            $states = $this->Session->read('Visitor.' . $scope);
            unset($states[$key]);
            $this->Session->write('Visitor.' . $scope, $states);
        }

        $this->Session->delete('Visitor.' . substr($this->request->data['key'], 0, 255));
    }

    public function add()
    {
        $scope = $key = $value = "";

        $scope = substr($this->request->data['scope'], 0, 255);
        $key = substr($this->request->data['key'], 0, 255);
        $value = substr($this->request->data['value'], 0, 255);

        if (empty($scope)) {
            $this->Session->write('Visitor.' . $key, $value);
        } else {
            $states = $this->Session->read('Visitor.' . $scope);
            $states[$key] = $value;
            $this->Session->write('Visitor.' . $scope, $states);
        }
    }

    public function getstates()
    {
        $this->Session->read('Visitor.header', $this->request->data['state']);
    }

    public function getstate()
    {
        pr($this->request->data);
    }

    public function get()
    {
        $this->set('data', $this->Session->read('Visitor'));;
    }


    //TODO SX4 => Formular ...
    function validate($formconfig_id)
    {

        $formularconfig = TableRegistry::getTableLocator()->get('Formularconfigs');

        $data = $formularconfig->find()->contain(['Formulars'])->where(['Formularconfigs.id' => $formconfig_id])->first();

        $this->formFields = $data->formular->extended_data['fields'];
        $this->Formdatamodel = new FormdataForm($this->formFields);


        $displayFields = [];
        foreach ($this->request->data as $k => $v) {
            if (endsWith($k, 'Display')) {
                $newKey = str_replace('Display', '', $k);
                $displayFields[] = $newKey;
                $this->request->data[$newKey] = $v;
                unset($this->request->data[$k]);
            }
        }

        $valid = $this->Formdatamodel->validate($this->request->data);

        if (!$valid) {

            $errors = $this->Formdatamodel->errors();
            foreach ($displayFields as $field) {
                if (isset($errors[$field])) {
                    $errors[$field . 'Display'] = $errors[$field];
                    unset($errors[$field]);
                }
            }

            $this->set('errors', $errors);
            $this->flashError();
        } else {
            $this->flashSuccess('OK');
        }

    }

}
