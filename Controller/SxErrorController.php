<?php

namespace App\Controller;

use App\Library\Facades\Menu;
use Cake\Event\EventInterface;
use Cake\Http\Response;

/**
 * @version 4.0.0
 */
class SxErrorController extends DisplayController
{
    /**
     * @inheritdoc
     */
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event): EventInterface
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['*']);
    }

    /**
     * @inheritdoc
     */
    public function beforeRender(EventInterface $event): EventInterface
    {
        parent::beforeRender($event);

        $layout = 'error';

        if ( $this->response->getStatusCode() === 404 ) {
            $layout = Menu::getDomainRecord('layout', 'default');
        }

        $this->viewBuilder()->setLayout($layout)
            ->setTemplatePath('Error');

        return $event;
    }

    /**
     * @inheritdoc
     */
    public function render(?string $template = null, ?string $layout = null): Response
    {
        return parent::render($template, $layout);
    }

    /**
     * @inheritdoc
     */
    public function afterFilter(EventInterface $event): EventInterface
    {
        return $event;
    }

}
