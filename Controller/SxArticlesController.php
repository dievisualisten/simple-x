<?php

namespace App\Controller;

use App\Library\AuthManager;
use App\Library\Facades\Article;
use App\Library\Facades\Language;
use App\Utility\StringUtility;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * @version 4.0.0
 */

/**
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class SxArticlesController extends DisplayController
{

    /**
     * @var array
     */
    public $paginate = [];

    /**
     * Initialize controller.
     */
    public function initialize(): void
    {
        $this->loadModel('ArticlesElements');

        parent::initialize();
    }

    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event)
    {
        if ( AuthManager::isUrlAllowed() ) {
            $this->Authentication->allowUnauthenticated(['view']);
        }

        $this->paginate['Menu'] = [
            'limit' => config('Sx.app.articles.limit', 10),
        ];

        parent::beforeFilter($event);
    }

    /**
     * @inheritdoc
     */
    public function indexQuery($query)
    {
        $query->contain(['Acos', 'Users.Persons', 'Menus'], true);

        $this->Articles->getTypeCondition($query);
    }

    /**
     * @inheritdoc
     */
    public function delete($ids = null)
    {
        if ( ! $ids ) {
            $ids = $this->getRequest()->getData('ids');
        }

        $deleted = true;

        $this->Articles->begin();

        foreach ( explode(',', $ids) as $id ) {

            if ( empty($id) ) {
                continue;
            }

            $conditions = [
                'id' => $id,
            ];

            if ( Language::isDefaultLanguage() ) {

                $entity = $this->Articles->get($id);

                if ( $entity ) {
                   $this->Articles->delete($entity);
                }

                continue;

            }

            $translationsTable = TableRegistry::getTableLocator()
                ->get('ArticlesTranslations');

            $conditions['locale'] = Language::getLanguage();

            $entity = $translationsTable->find()
                ->where($conditions)->first();

            if ( $entity ) {

                $deleted = $deleted && $this->Articles->deleteTranslation($id,
                        Language::getLanguage());

                $this->Articles->Menus->deleteTranslation($id,
                    Language::getLanguage());
            }

        }

        if ( $deleted ) {
            $this->Articles->commit();
            $this->flashSuccess(__d('system', 'Die Daten wurden erfolgreich gelöscht.'));
        } else {
            $this->Articles->rollback();
            $this->flashError(__d('system', 'Die Daten konnten nicht gelöscht werden.'), 500);
        }

        $this->set('data', []);
    }


    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function beforeSave($entity, $data = null)
    {
        $isUnchangedCopied = preg_match('/^copied-/', $entity->slug) &&
            ! preg_match('/^Copied\s/', $entity->title);

        if ( $isUnchangedCopied ) {
            $entity->set('slug', $entity->title);
        }

        if ( ! empty($entity->title) && empty($entity->slug) ) {
            $entity->set('slug', $entity->title);
        }

        if ( ! isset($data['media']) ) {
            $data['media'] = [];
        }

        $this->Articles->patchAttachedWithMedia($entity, $data['media']);

        $entity->set('extended_data', $data['extended_data']);
        $entity->set('extended_data_cfg', $data['extended_data_cfg']);

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Table\ArticlesTable $table
     */
    public function beforeLoad($table)
    {
        $table->behaviors()->get('Translation')->mergeConfig([
            'clearNotTranslatedFields' => true,
        ]);

//        $table->setDefaultContains([
//            'Menus', 'Attached.Attachments', 'Attached.Attachments.Thumbnail'
//        ]);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function afterLoad($entity)
    {
        if ( ! Language::isDefaultLanguage() ) {
            $entity->baselocale = $entity->getDefaultTranslation();
        }

        $entity->makeElements();
        $entity->makeTabparents();

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function beforeCopy($entity)
    {
        $entity->set('title', 'Copied ' . $entity->title);
        $entity->set('slug', $entity->title);

        foreach ( $entity->get('attached') as $attched ) {
            $attched->set('id', Text::uuid());
        };

        $entity->set('menus', []);
        $entity->set('tabparents', []);
        $entity->set('elements', []);

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\ArticlesTranslation $entity
     */
    public function beforeCopyTranslation($entity)
    {
        $entity->set('title', 'Copied ' . $entity->title);
        $entity->set('slug', $entity->title);

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function view($id, $menu_id = null, $action = null)
    {
        $article = Article::getArticleView($this, $menu_id);
        //search()->perfomSearch($this, $article);
    }

}
