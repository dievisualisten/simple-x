<?php

namespace App\Controller;

class SxActionviewsController extends CrudController
{

    public function load($id = null)
    {
        if ($id) {
            $data = $this->Actionview->find('first', ['conditions' => ['Actionview.id' => $id]]);
            $data['Quellmenu'] = $this->Actionview->Menus->getParentPaths([$data['Menu']], 'id');

        }
        if (empty($data)) {
            throw new NotFoundException();
        }
        $this->set('data', $data);
    }

    public function save($id = null)
    {
        if ( ! empty($this->request->getData('Quellmenu')) ) {
            $this->request->setData('Actionview.menu_id', $this->request->getData('Quellmenu'));
        } else {
            $this->request->setData('Actionview.menu_id', null);
        }

        $this->request->setData('Menu', null);


        $data = $this->request->getData();



        if ($this->isTrueOrFalseRainbow($this->Actionview->saveAll($this->request->getData(), [
            'validate' => 'first',
            'atomic' => false,
        ]))) {
            $this->Actionview->commit();
            $this->flashSuccess();
            $data = $this->Actionview->read(null, $this->Actionview->id);
            $data['Quellmenu'] = $this->Actionview->Menus->getParentPaths([$data['Menu']], 'id');
            $this->set('data', $data);
        } else {
            $this->Actionview->rollback();
            $this->flashError();
        }
    }

}
