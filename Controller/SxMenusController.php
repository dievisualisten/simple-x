<?php

namespace App\Controller;

use App\Library\AuthManager;
use App\Library\Facades\Menu;
use App\Controller\Traits\MoveTrait;
use App\Controller\Traits\InsertTrait;

/**
 * @version 4.0.0
 */

class SxMenusController extends CrudController
{
    use MoveTrait, InsertTrait;

    /**
     * @inheritdoc
     */
    public function beforeMoveBeforeOrAfter($source, $target)
    {
        if ( empty($source->alias_id) ) {
            return;
        }

        $condition = [
            'parent_id' => $target->parent_id, 'alias_id' => $source->alias_id
        ];

        if ( $this->{$this->modelClass}->find()->where($condition)->count() ) {
            throw new \Exception(__('Alias existiert bereits auf dieser Ebene.'), 500);
        }
    }

    /**
     * @inheritdoc
     */
    public function tree()
    {
        $expanded = [];

        if ( $id = Menu::getDomainRecord('id') ) {
            $expanded[] = $id;
        }

        $this->set('expanded', $expanded);

        parent::tree();
    }

    /**
     * @inheritdoc
     */
    public function save($id = null)
    {
        // Stop auth manager while editing cause it conflicts with tree
        AuthManager::stop();

        $this->Menus->begin();

        $entity = null;

        if ( $this->request->getData('type') === 'alias' ) {
            $entity = $this->Menus->saveAlias($this->getRequest()->getData());
        }

        if ( $this->request->getData('type') === 'domain' ) {
            $entity = $this->Menus->saveDomain($this->getRequest()->getData());
        }

        if ( $this->request->getData('type') === 'menu' ) {
            $entity = $this->Menus->saveMenu($this->getRequest()->getData());
        }

        if ( $entity ) {
            $this->flashSuccess();
            $this->Menus->commit();
        } else {
            /** @var \App\Model\Table\MenusTable $this->Menus */
            $this->flashError(__d('system', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist'), 422);
            $this->Menus->rollback();
        }

        $this->set('data', $entity);
    }

    /**
     * @inheritdoc
     */
    public function afterLoad($data)
    {
        if ( $data->type !== 'alias' ) {
            return $data;
        }

        $menus = $this->Menus->find()
            ->where([
                'id' => $data->get('alias_id'),
            ])
            ->first();

        $data->tabmenus = $this->Menus->getParentPaths([$menus], 'id');

        $menuPaths = [];

        foreach( $data->tabmenus as $tab ) {
            $menuPaths = array_merge($menuPaths, explode('/', $tab->parent_path));
        }

        $data->tabmenus_expanded = array_values(array_filter(array_unique($menuPaths)));

        $parents = $this->Menus->find()
            ->where([
                'alias_id' => $data->get('alias_id'), 'type' => 'alias',
            ])
            ->all();

        $data->tabparents = $this->Menus->getParentPaths($parents);

        $parentPaths = [];

        foreach( $data->tabparents as $tab ) {
            $parentPaths = array_merge($parentPaths, explode('/', $tab->parent_path));
        }

        $data->tabparents_expanded = array_values(array_filter(array_unique($parentPaths)));

        return $data;
    }

//    public function getDomainDomain()
//    {
//        $domains = $this->Menus->find('domains')->all();
//        $this->set('data', DomainManager::format($domains, 'title', 'id'));
//    }

}
