<?php

namespace App\Controller;

use App\Library\Facades\Language;
use App\Utility\StringUtility;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Utility\Text;

/**
 * @version 4.0.0
 */

/**
 * @property \App\Model\Table\ElementsTable $Elements
 */
class SxElementsController extends DisplayController
{
    /**
     * @var array
     */
    public $paginate = [];

    /**
     * Initialize controller.
     */
    public function initialize(): void
    {
        $this->loadModel('Articles');
        $this->loadModel('ArticlesElements');

        parent::initialize();
    }

    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    public function indexQuery($query)
    {
        if ( $type = $this->request->getQuery('pagetype') ) {
            $query->where(['Elements.type' => $type]);
        }

        if ( $except = $this->request->getQuery('except') ) {

            $elements = $this->ArticlesElements->find()
                ->where([
                    'article_id' => $except
                ])
                ->all();

            $conditions = [
                'Elements.id NOT IN' => Hash::extract($elements->toArray(), '{n}.element_id')
            ];

            if ( $elements->count() ) {
                $query->where($conditions);
            }
        }

        $query = $this->Elements->getTypeCondition($query);
    }

    /**
     * @inheritdoc
     */
    public function delete($ids = null)
    {
        if ( ! $ids ) {
            $ids = $this->getRequest()->getData('ids');
        }

        $deleted = true;

        $this->Elements->begin();

        foreach ( explode(',', $ids) as $id ) {

            if ( empty($id) ) {
                continue;
            }

            $conditions = [
                'id' => $id,
            ];

            if ( Language::isDefaultLanguage() ) {

                $entity = $this->Elements->get($id);

                if ( $entity ) {
                    $this->Elements->delete($entity);
                }

                continue;

            }

            $translationsTable = TableRegistry::getTableLocator()
                ->get('ElementsTranslations');

            $conditions['locale'] = Language::getLanguage();

            $entity = $translationsTable->find()
                ->where($conditions)->first();

            if ( $entity ) {

                $deleted = $deleted && $this->Elements->deleteTranslation($id,
                        Language::getLanguage());
            }

        }

        if ( $deleted ) {
            $this->Elements->commit();
            $this->flashSuccess(__d('system', 'Die Daten wurden erfolgreich gelöscht.'));
        } else {
            $this->Elements->rollback();
            $this->flashError(__d('system', 'Die Daten konnten nicht gelöscht werden.'), 500);
        }

        $this->set('data', []);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Element $entity
     */
    public function beforeSave($entity, $data = null)
    {
        $isUnchangedCopied = preg_match('/^copied-/', $entity->slug) &&
            ! preg_match('/^Copied\s/', $entity->title);

        if ( $isUnchangedCopied ) {
            $entity->set('slug', $entity->title);
        }

        if ( ! isset($data['media']) ) {
            $data['media'] = [];
        }

        $this->Elements->patchAttachedWithMedia($entity, $data['media']);

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Element $entity
     */
    public function afterSave($entity, $data = null)
    {
        if ( ! isset($data['article_id']) ) {
            return;
        }

        $article = $this->Articles->find()
            ->where([
                'id' => $data['article_id']
            ])
            ->first();

        $tableKey = Inflector::pluralize($entity->type);

        $relations = $this->ArticlesElements->find()
            ->where([
                'element_id' => $entity->id,
                'article_id' => $data['article_id'],
            ]);

        if( $relations->count() ) {
            return;
        }

        $relation = $this->ArticlesElements->newEntity([
            'element_id' => $entity->id,
            'article_id' => $data['article_id'],
            'draft'      => 1,
            'sequence'   => 9999,
        ]);

        if ( isset($article->{$tableKey}) ) {
            $relation->sequence = count($article->{$tableKey});
        }
	
        $result = $this->ArticlesElements->save($relation);

        if ( ! $result ) {
            throw new \Exception(__d('system', 'Elementverbindung kann nicht erstellt werden!'));
        }
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Table\ElementsTable $table
     */
    public function beforeLoad($table)
    {
        $table->behaviors()->get('Translation')->mergeConfig([
            'clearNotTranslatedFields' => true,
        ]);

        $table->setDefaultContains([
            'Attached.Attachments', 'Attached.Attachments.Thumbnail',
        ]);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Element $entity
     */
    public function afterLoad($entity)
    {
        if ( ! Language::isDefaultLanguage() ) {
            $entity->baselocale = $entity->getDefaultTranslation();
        }

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Element $entity
     */
    public function beforeCopy($entity)
    {
        $entity->set('title', 'Copied ' . $entity->title);

        foreach ( $entity->get('attached') as $attched ) {
            $attched->set('id', Text::uuid());
        };

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\ElementsTranslation $entity
     */
    public function beforeCopyTranslation($entity)
    {
        $entity->set('title', 'Copied ' . $entity->title);

        return $entity;
    }

    public function add()
    {
        $article_id = $this->request->getData('article_id');

        if ( ! $article_id ) {
            throw new \Exception('Kein Artikel zum anhaengen gefunden.');
        }

        $this->ArticlesElements->begin();

        foreach ( $this->request->getData('element_ids') as $index => $element ) {

            $entity = $this->ArticlesElements->newEntity([
                'article_id' => $article_id, 'element_id' => $element, 'draft' => true, 'sequence' => 9999
            ]);

            $this->ArticlesElements->save($entity, [
                'atomic' => false
            ]);

            if ( ! $entity ) {
                throw new \Exception('Verknüpfung kann nicht gespeichert werden.');
            }
        }

        $this->flashSuccess();
        $this->getModelTable()->commit();
    }

}
