<?php

namespace App\Controller;

use App\Controller\Traits\MoveTrait;
use App\Library\Facades\Language;
use App\Utility\VimeoUtility;
use App\Utility\YoutubeUtility;

class SxAttachmentsController extends CrudController
{
    use MoveTrait;

    /**
     * @var \App\Model\Table\AttachmentsTable $Attachments
     */
    public $Attachments;

    /**
     * @inheritdoc
     */
    public function initialize(): void
    {
        $this->loadModel('Attachments');

        parent::initialize();
    }

    public function treeQuery($query)
    {
        $query->where([
            'Attachments.type' => 'folder',
        ])->order('Attachments.name ASC');

        return $query;
    }

    /**
     * @inheritdoc
     */
    public function indexQuery($query)
    {
        $parentId = $this->request->getQuery('parent_id', null);

        if ( $parentId === '11111111-1111-1111-1111-111111111111' ) {
            $parentId = null;
        }

        $conditions = [
            'Attachments.id IS' => $parentId,
        ];

        $parent = $this->Attachments->find()
            ->where($conditions)->first();

        $conditions = [
            'Attachments.type !=' => 'folder',
        ];

        if ( $parent !== null ) {
            $conditions['Attachments.lft >'] = $parent->lft;
            $conditions['Attachments.lft <'] = $parent->rght;
        }

        if ( $parent !== null && $parent->parent_id !== null ) {
            $conditions['Attachments.parent_id'] = $parent->id;
        }

        $query->where($conditions);
        $query->contain('Articles');
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Table\ArticlesTable $table
     */
    public function beforeLoad($table)
    {
        $table->behaviors()->get('Translation')->mergeConfig([
            'clearNotTranslatedFields' => true,
        ]);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function afterLoad($entity)
    {
        if ( ! Language::isDefaultLanguage() ) {
            $entity->baselocale = $entity->getDefaultTranslation();
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function crop()
    {
        $entity = $this->Attachments->cropImage($this->request->getData());

        if ( $entity === false || $entity === null ) {
            $this->flashError(null, 422);
        }

        $this->set('data', $entity);
    }

    /**
     * @inheritdoc
     */
    public function focus()
    {
        $entity = $this->Attachments->focusImage($this->request->getData());

        if ( $entity === false || $entity === null ) {
            $this->flashError(null, 422);
        }

        $this->set('data', $entity);
    }

    /**
     * @inheritdoc
     */
    public function whitespace()
    {
        $entity = $this->{$this->modelClass}->whitespaceImage($this->request->getData());

        if ( $entity === false || $entity === null ) {
            $this->flashError(null, 422);
        }

        $this->set('data', $entity);
    }

    /**
     * @inheritdoc
     */
    public function beforeSavePatch($data)
    {
        if ( empty($data['thumbnail']['id']) ) {
            $data['thumb_id'] = null;
        }

        if ( isset($data['thumbnail']['id']) ) {
            $data['thumb_id'] = $data['thumbnail']['id'];
        }

        //Todo: fixme @later
        unset($data['thumbnail']);

        return $data;
    }

    public function beforeSave($entity, $data = null)
    {
        if ( $entity->get('type') === 'video' ) {
            $this->_appendVideoToEntity($entity);
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function _appendVideoToEntity($entity)
    {
        $url = $this->getRequest()->getData('videourl');

        if ( strpos($url, 'vimeo') !== false ) {
            $entity = VimeoUtility::getAttachmentData($url, $entity);
        }

        if ( strpos($url, 'youtube') !== false ) {
            $entity = YoutubeUtility::getAttachmentData($url, $entity);
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function upload()
    {
        ini_set('memory_limit', '512M');

        if ( empty($this->getRequest()->getData('parent_id')) ) {
            throw new \Exception(__d('system', 'Bitte wählen Sie einen Order aus.'));
        }

        $this->save();
        $this->flashClear();
    }

}
