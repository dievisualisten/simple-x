<?php

namespace App\Controller;

use App\Library\AuthManager;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\ORM\Entity;
use Cake\Utility\Hash;
use App\Controller\Component\System\RequestHandlerComponent;
use App\Controller\Component\Authentication\AuthenticationComponent;

class SxAppController extends Controller
{
    /**
     * @var \App\Controller\Component\Authentication\AuthenticationComponent
     */
    public $Authentication;

    public $_helpers = [
        'Html' => [
            //
        ],
        'Time' => [
            //
        ],
        'Number' => [
            //
        ],
        'Text' => [
            //
        ],
        'Tree' => [
            //
        ],
        'Fe' => [
            //
        ],
        'Paginator' => [
            //
        ],

        'Flash' => [
            //
        ],
    ];

    public function initialize(): void
    {
        parent::initialize();

        $this->viewBuilder()->setHelpers($this->_helpers);

        $this->loadComponent('RequestHandler', [
            'className' => RequestHandlerComponent::class,
        ]);

        $this->loadComponent('Authentication', [
            'className' => AuthenticationComponent::class,
            'requireIdentity' => true,
        ]);


        $this->loadComponent('Authorization.Authorization', [
            //
        ]);

        $this->loadComponent('Flash', [
            //
        ]);

        $this->loadComponent('Paginator', [
            //
        ]);

        $this->loadComponent('Hook', [
            //
        ]);

        if ( config('debug') && isset($_GET['SXTEST']) ) {
            $this->Authentication->allowUnauthenticated(get_class_methods($this));
        }
    }

    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions(
            AuthManager::getAuthorizedByRequest($this->request)
        );

        //Damit in den Elemets keine not isset $data Fehler auftauchen
        $this->set('data', new \stdClass());

        parent::beforeFilter($event);
    }

    protected function _templatePath(): string
    {
        $templatePath = mb_strtolower($this->name);
        if ($this->request->getParam('prefix')) {
            $prefixes = array_map(
              'mb_strtolower',
              explode('/', $this->request->getParam('prefix'))
            );
            $templatePath = implode(DIRECTORY_SEPARATOR, $prefixes) . DIRECTORY_SEPARATOR . $templatePath;
        }

        return $templatePath;
    }

    public function flashClear()
    {
        $this->getRequest()->getSession()->delete('Flash');
    }

    public function flashError($msg = null, $statusCode = 400, $params = null)
    {
        $this->Flash->error($msg ?? __d('system', 'Es sind Fehler aufgetreten.'),
            ['key' => 'error', 'params' => Hash::merge(['code' => $statusCode, 'message' => $msg, 'msgTitel' => __('Fehler')], $params)]);
    }

    public function flashSuccess($msg = null, $statusCode = 200, $params = null)
    {
        $this->Flash->success($msg ?? __d('system', 'Die Daten wurden gespeichert.'),
            ['key' => 'success', 'params' => Hash::merge(['code' => $statusCode, 'message' => $msg, 'msgTitel' => __('Hinweis')], $params)]);
    }

    public function flashWarning($msg = null, $statusCode = 200, $params = null)
    {
        $this->Flash->warning($msg ?? __d('system', 'Bitte beachten Sie die Hinweise.'),
            ['key' => 'warning', 'params' => Hash::merge(['code' => $statusCode, 'message' => $msg, 'msgTitel' => __('Warnung')], $params)]);
    }

    public function flashInfo($msg = null, $statusCode = 200, $params = null)
    {
        $this->Flash->info($msg ?? __d('system', 'Bitte beachten Sie die Hinweise.'),
            ['key' => 'info', 'params' => Hash::merge(['code' => $statusCode, 'message' => $msg, 'msgTitel' => __('Hinweis')], $params)]);
    }

    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

        if ( ! $this->viewBuilder()->getVar('data') ) {
            $this->viewBuilder()->setVar('data', new \stdClass());
        }


        $session = $this->getRequest()->getSession();

        $flashes = [];

        foreach ( ['error', 'success', 'info', 'warning', 'flash'] as $key ) {
            $flashes[$key] = (array) $session->read("Flash.$key");
        }

        $response = $this->getResponse();

        foreach ( $flashes as $key => $flash ) {

            if ( count($flash) === 0 ) {
                continue;
            }

            $status = array_get($flash[0], 'params.code', null);

            if ( $status ) {
                $response = $response->withStatus($status);
            }

            if ( isset($flash[0]['message']) ) {
                $this->set('message', $flash[0]['message']);
            }

        }

        if ( request()->is('json') === false ) {
            return;
        }

        $session->delete('Flash');

        if ( $this->viewBuilder()->hasVar('errors') ) {
            $response = $response->withStatus(422);
        }

        $this->setResponse($response);

        $hasExceptionError = $response->getStatusCode() !== 200 &&
          is_a($this->viewBuilder()->getVar('errors'), \Exception::class);

        if ( $hasExceptionError ) {
            $this->viewBuilder()->setVar('message',
              $this->viewBuilder()->getVar('errors')->getMessage());
        }

        $hasValidationError = $response->getStatusCode() !== 200 &&
            is_a($this->viewBuilder()->getVar('data'), Entity::class);

        if ( $hasValidationError ) {
            $this->viewBuilder()->setVar('errors',
              $this->viewBuilder()->getVar('data')->getErrors());
        }

        $this->set('_serialize', true);
    }

    /**
     * @return string|null
     */
    public function getModelClass(): ?string
    {
        return $this->modelClass;
    }

    /**
     * @return string|null
     */
    public function getModelTable()
    {
        return $this->{$this->modelClass};
    }

}
