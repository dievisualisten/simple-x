<?php

namespace App\Controller;

/**
 * @version 4.0.0
 */
class SxConfigurationsController extends CrudController
{

    /**
     * @inheritdoc
     */
    public function indexQuery($query)
    {
        $query->where(['is_pageconfiguration' => false]);
    }


}
