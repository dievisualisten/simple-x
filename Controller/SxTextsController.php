<?php

namespace App\Controller;

use App\Library\Facades\Language;
use Cake\Command\I18nExtractCommand;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use PoParser\Parser;

class SxTextsController extends CrudController
{
    public $uses = [
        'Text',
        'Formular',
        'Domain',
        'Category',
    ];

    /**
     * @inheritdoc
     *
     * @var \App\Model\Table\ArticlesTable $table
     */
    public function beforeLoad($table)
    {
        $table->behaviors()->get('Translation')->mergeConfig([
            'clearNotTranslatedFields' => true,
        ]);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function afterLoad($entity)
    {
        if ( ! Language::isDefaultLanguage() ) {
            $entity->baselocale = $entity->getDefaultTranslation();
        }

        return $entity;
    }

    public function load2($id = null)
    {
        $data = [];
        if ( $id ) {
            $this->Texts->behaviors()->get('Translation')->config([
                'clearNotTranslatedFields' => true,
            ]);
            $data = $this->Texts->get($id);
        }

        $this->set('data', $data);
    }

    public function generate()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(180);

        $comand = new I18nExtractCommand();

        $comand->execute(new Arguments([], [
            'output' => APP . 'Locale' . DS,
            'extract-core' => true,
            'quiet' => true,
            'paths' => APP . ',' . SX,
            'overwrite' => true,
            'merge' => 'no',
        ], []), new ConsoleIo());

        $this->_importText();
        $this->Texts->writePos();

        $this->flashSuccess(__d('system', 'Die Texte wurden neu generiert.'));
    }

    protected function _importText()
    {


        $pots = [
            APP . 'Locale' . DS . 'default.pot' => '__(Texte)',
            APP . 'Locale' . DS . 'system.pot' => 'system',
            APP . 'Locale' . DS . 'cake.pot' => 'cake',
        ];

        $this->Texts->begin();
        $texts = [];

        foreach ( $pots as $pot => $type ) {
            $pp = new Parser();
            $pp->read($pot);
            $entries = $pp->getEntriesAsArrays();
            foreach ( $entries as $line ) {
                if ( ! empty($line['msgid']) ) {
                    $texts[stripslashes($line['msgid'])][] = [
                        'source' => 'locales',
                        'type' => $type,
                        'name' => $line['msgid'],
                        'value' => $line['msgid'],
                    ];
                }
            }
        }

        //Domain
        $all = TableRegistry::getTableLocator()->get('Domains')->find()->where([
            'type NOT IN' => [
                'tld',
                'phone',
                'currency',
            ],
        ])->all();

        foreach ( $all as $line ) {
            $texts[$line->value][] = [
                'source' => 'domain',
                'type' => $line->type,
                'name' => $line->value,
                'value' => $line->value,
            ];
        }

        $all = TableRegistry::getTableLocator()->get('Formulars')->find()->all();

        foreach ( $all as $line ) {


            if ( ! empty($line['btnname']) ) {
                $texts[$line['btnname']][] = [
                    'source' => 'formular',
                    'type' => 'buttonname',
                    'name' => $line['btnname'],
                    'value' => $line['btnname'],
                ];
            }

            pr($line);
            exit;

            if ( isset($line->extended_data['fields']) ) {
                foreach ( $line->extended_data['fields'] as $f ) {
                    if ( in_array($f['type'], [
                        'hr',
                        'br',
                        'element',
                        'hidden',
                        'containeropen',
                        'containerclose',
                    ]) ) {
                        continue;
                    }


                    $keys = [
                        'label',
                        'infotext',
                        'validationMessage',
                        'validation1Message',
                        'validation2Message',
                        'validation3Message',
                        'placeholder',
                        'defaultValue',
                        'tooltip',
                        'prepend',
                        'append',
                    ];


                    foreach ( $keys as $key ) {
                        if ( ! empty($f[$key]) ) {
                            $texts[$f[$key]][] = ['source' => 'formular', 'type' => $key, 'name' => $f[$key], 'value' => $f[$key]];
                        }
                    }

                    if ( ! empty($f['options']) ) {
                        $options_tmp = explode('|', $f['options']);
                        if ( count($options_tmp) > 1 ) {
                            foreach ( $options_tmp as $v ) {
                                $kv = explode('#', $v);
                                if ( count($kv) > 1 ) {
                                    $texts[$kv[1]][] = ['source' => 'formular', 'type' => 'domain', 'name' => $kv[1], 'value' => $kv[1]];

                                } else {
                                    $texts[$v][] = ['source' => 'formular', 'type' => 'domain', 'name' => $v, 'value' => $v];
                                }
                            }
                        }
                    }
                }
            }
        }

        $ids = [];


        foreach ( $texts as $text ) {

            $entity = $this->Texts->find()->where(['name' => $text[0]['name']])->first();
            if ( ! $entity ) {
                $text[0]['in_use'] = true;
                $entity = $this->Texts->newEntity($text[0]);
            }
            $ids[] = $entity->id;
            $this->Texts->save($entity);
        }

        $this->Texts->updateAll(['in_use' => true], []);
        $this->Texts->updateAll(['in_use' => false], ['id NOT IN' => $ids, 'source !=' => 'text']);

        $this->Texts->commit();
    }
}
