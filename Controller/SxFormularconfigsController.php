<?php

namespace App\Controller;

use App\Library\Facades\Language;

/**
 * @version 4.0.0
 */

class SxFormularconfigsController extends CrudController
{
    /**
     * @inheritdoc
     *
     * @var \App\Model\Table\ArticlesTable $table
     */
    public function beforeLoad($table)
    {
        $table->behaviors()->get('Translation')->mergeConfig([
            'clearNotTranslatedFields' => true,
        ]);
    }

    /**
     * @inheritdoc
     *
     * @var \App\Model\Entity\Article $entity
     */
    public function afterLoad($entity)
    {
        if ( ! Language::isDefaultLanguage() ) {
            $entity->baselocale = $entity->getDefaultTranslation();
        }

        return $entity;
    }

}
