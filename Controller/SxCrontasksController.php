<?php

namespace App\Controller;

use App\Shell\Task;
use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Inflector;

class SxCrontasksController extends CrudController
{

    public function run()
    {
        $path = str_join('/', ROOT, 'pulse.sh');

        if ( ! preg_match('/^[A-Z]:/', $path) ) {
            $path = 'source ~/.bashrc; cd ' . ROOT . '; sh /' . $path . ' 2>&1';
        }

        try {
            exec("{$path}", $output);
        } catch ( \Exception $e ) {
            $output = [$e->getMessage()];
        }

        $this->set('output', implode("\n", $output));
    }

    //Methode zum Testen
    public function test($task = '')
    {
        ini_set('memory_limit', '512M');
        set_time_limit(720);

        $crontask = $this->getModelTable()->newEntity(['parameter' => func_get_args()]);

        $command = 'App\\Command\\Crontask\\' . Inflector::camelize($task);

        if ( ! class_exists($command) ) {
            throw new NotFoundException('Task ' . $command . ' doesn\'t exist.');
        }

        $command = new $command($crontask);

        logger()->start();

        $command->boot();

        Configure::write('debug', true);
        pr(logger()->stop());

        exit;
    }
}
