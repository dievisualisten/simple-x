<?php

namespace App\Controller;

/**
 * @version 4.0.0
 */
class SxPageconfigurationsController extends CrudController
{
    /**
     * @inheritdoc
     */
    function treeQuery($query)
    {
        $pageType = $this->getRequest()->getQuery('pagetype', '');

        $conditions = [
            'is_pageconfiguration' => true,
        ];

        if ( ! empty($pageType) ) {
            $conditions['name LIKE'] = "%pagetypes.{$pageType}.%";
        }

        $query->where($conditions);
    }

    /**
     * @inheritdoc
     */
    function indexQuery($query)
    {
        $pageType = $this->getRequest()->getQuery('pagetype', '');

        $conditions = [
            'is_pageconfiguration' => true,
        ];

        if ( ! empty($pageType) ) {
            $conditions['name LIKE'] = "%pagetypes.{$pageType}.%";
        }

        $query->where($conditions);
    }


    /**
     * @inheritdoc
     */
    public function save($id = null)
    {
        $data = $this->request->getData();

        if ( ! empty($data['is_global']) ) {

            $this->Pageconfigurations->begin();

            if ( $this->Pageconfigurations->addNewConfigToAllPagetypes($this->request->getData()) ) {
                $this->flashSuccess();
                $this->Pageconfigurations->commit();
            } else {
                $this->flashError(__d('system', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist'), 422);
                $this->Pageconfigurations->rollback();
            }

        } else {
            parent::save($id);
        }
    }


    /**
     * TODO SX4, aktuell nicht getestet da noch keine Backendfunction, ggf in die save integrieren...
     * erstellt alle Configs für einen neuen Seitentypen.
     * Dabei wird Page kopiert oder die angegebene Config.
     */

    public function pagetype()
    {
        $this->Pageconfigurations->begin();

        $entity = $this->Pageconfigurations
            ->addNewPagetype($this->request->getData());

        if ( $entity ) {
            $this->flashSuccess(__d('system', 'Alle Konfigurationen für Seitentyp wurden erstellt.'));
            $this->Pageconfigurations->commit();
        } else {
            $this->flashError(null, 500);
            $this->Pageconfigurations->rollback();
        }

        $this->set('data', $entity);
    }


}
