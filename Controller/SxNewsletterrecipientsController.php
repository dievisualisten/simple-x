<?php

namespace App\Controller;

use App\Support\Extjs\SxExtjsImport;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\Time;

class SxNewsletterrecipientsController extends DisplayController
{

    public $uses = [
        'Newsletterrecipient',
        'Newslettercampaign',
    ];

    public function initialize(): void
    {
        $this->loadModel('Newsletterrecipients');
        $this->loadModel('Newslettercampaign');
        $this->loadModel('Newsletterinterests');

        parent::initialize();
    }

    public function beforeFilter2(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions([
            'approve', 'approve_redirect', 'unsubscribe', 'change'
        ]);

        parent::beforeFilter($event);
    }

    public function save1()
    {
        if (isset($this->request->data)) {
            $this->request->data['Person']['model'] = 'Newsletterrecipient';
            $this->Newsletterrecipient->begin();
            if ($this->isTrueOrFalseRainbow($this->Newsletterrecipient->saveAll($this->request->data, [
                'validate' => 'first',
                'atomic' => false,
            ]))) {
                $this->Newsletterrecipient->commit();
                $this->flashSuccess();
                $data = $this->Newsletterrecipient->read(null, $this->Newsletterrecipient->id);
                $this->set('data', $data);
            } else {
                $this->Newsletterrecipient->rollback();
                $this->flashError();
            }
        }
    }

    public function approve($id = null)
    {
        if (!empty($id)) {
            $this->Newsletterrecipient->contain();
            $recipient = $this->Newsletterrecipient->findById($id);

            if (!empty($recipient)) {
                $recipient['Newsletterrecipient']['approved'] = 1;
                $recipient['Newsletterrecipient']['unsubscribed'] = 0;
                $recipient['Newsletterrecipient']['approved_date'] = date('Y-m-d H:i:s');
                $this->Newsletterrecipient->create();
                $this->Newsletterrecipient->save($recipient);
            } else {
                throw new NotFoundException();
            }
        } else {
            throw new NotFoundException();
        }
        $this->Pagebuilder->getActionviewArticle('newsletterrecipients/approve');
    }


    public function approve_redirect($id = null)
    {
        if (!empty($id)) {
            $this->Newsletterrecipient->contain();
            $recipient = $this->Newsletterrecipient->findById($id);

            if (!empty($recipient)) {
                $recipient['Newsletterrecipient']['approved'] = 1;
                $recipient['Newsletterrecipient']['unsubscribed'] = 0;
                $recipient['Newsletterrecipient']['approved_date'] = date('Y-m-d H:i:s');
                $this->Newsletterrecipient->create();
                $this->Newsletterrecipient->save($recipient);
            } else {
                throw new NotFoundException();
            }
        } else {
            throw new NotFoundException();
        }

        $page = $this->Pagebuilder->getActionviewUrl('newsletterrecipients/approve_redirect');

        if (!empty($page)) {
            $this->redirect($page . '?nr_id=' . $id);
        }
    }

    public function unsubscribe($id = null, $campaign_id = null)
    {
        if (!empty($id)) {
            $this->Newsletterrecipient->contain();
            $recipient = $this->Newsletterrecipient->findById($id);
            if (!empty($recipient)) {
                $recipient['Newsletterrecipient']['unsubscribed'] = '1';
                $recipient['Newsletterrecipient']['unsubscribed_date'] = date('Y-m-d H:i');
                $this->Newsletterrecipient->create();
                $this->Newsletterrecipient->save($recipient);
            }
        }

        if (!empty($campaign_id)) {
            $campaign = $this->Newslettercampaign->findById($campaign_id);
            if (!empty($recipient)) {
                $this->Newslettercampaign->id = $campaign['Newslettercampaign']['id'];
                $this->Newslettercampaign->saveField('unsubscribed', 1, false);
            }
        }

        if (empty($id) && empty($campaign_id)) {
            throw new NotFoundException();
        }

        $this->Pagebuilder->getActionviewArticle('newsletterrecipients/unsubscribe');
    }


    public function change($id = null)
    {
        if (!empty($id)) {
            $this->Newsletterrecipient->contain();
            $recipient = $this->Newsletterrecipient->findById($id);
        }

        if (empty($id) && empty($recipient)) {
            throw new NotFoundException();
        }

        $page = $this->Pagebuilder->getActionviewUrl('newsletterrecipients/change');

        if (!empty($page)) {
            $this->redirect($page . '?nr_id=' . $id);
        }
    }

    /**
     * @inheritdoc
     */
    public function importEntity($entity)
    {
        $entity->modified = Time::now();

        if ( $entity->isNew() && $entity->created == null ) {
            $entity->created = Time::now();
        }

        if ( $entity->isNew() && $entity->source == null ) {
            $entity->source = "Import - " . date('Y-m-d H:i');
        }

        if ( $entity->getOriginal('unsubscribed') == true ) {
            $entity->unsubscribed = true;
        }

        return $entity;
    }

}
