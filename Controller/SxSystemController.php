<?php

namespace App\Controller;

use App\Library\AuthManager;
use App\Library\DomainManager;
use App\Utility\StringUtility;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Filesystem\Folder;

/**
 * @version 4.0.0
 */
class SxSystemController extends AppController
{
    /**
     * @inheritdoc
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    protected function _getDomains()
    {
        $this->loadModel('Domains');

        /**
         * Fetch Domains
         */
        $domains = $this->Domains->find()
          ->where([
            'active' => true,
          ])
          ->order([
            'sequence' => 'asc', 'value' => 'asc',
          ])
          ->all();

        return $domains->toArray();
    }

    public function domains()
    {
        $this->set('data', $this->_getDomains());
    }

    protected function _getAcos()
    {
        $this->loadModel('Acos');

        /**
         * Fetch Acos
         */
        $acos = $this->Acos->find()
          ->where([
              //
          ])
          ->order([
            'pathname' => 'asc',
          ])
          ->all();

        return $acos->toArray();
    }

    public function acos()
    {
        $this->set('data', $this->_getAcos());
    }

    protected function _getRoles()
    {
        $this->loadModel('Roles');

        /**
         * Fetch Roles
         */
        $roles = $this->Roles->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $roles->toArray();
    }

    public function roles()
    {
        $this->set('data', $this->_getRoles());
    }

    protected function _getUsers()
    {
        $this->loadModel('Users');

        /**
         * Fetch Users
         */
        $users = $this->Users->find()
          ->where([
              //
          ])
          ->order([
            'login' => 'asc',
          ])
          ->all();

        return $users->toArray();
    }

    public function users()
    {
        $this->set('data', $this->_getUsers());
    }

    protected function _getResources()
    {
        $this->loadModel('Resources');

        /**
         * Fetch Resources
         */
        $resources = $this->Resources->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $resources->toArray();
    }

    public function resources()
    {
        $this->set('data', $this->_getResources());
    }

    protected function _getEmails()
    {
        $this->loadModel('Emails');

        /**
         * Fetch Emails
         */
        $emails = $this->Emails->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $emails->toArray();
    }

    public function emails()
    {
        $this->set('data', $this->_getEmails());
    }

    protected function _getFormulars()
    {
        $this->loadModel('Formulars');

        /**
         * Fetch Formulars
         */
        $formulars = $this->Formulars->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $formulars->toArray();
    }

    public function formulars()
    {
        $this->set('data', $this->_getFormulars());
    }

    protected function _getFormularconfigs()
    {
        $this->loadModel('Formularconfigs');

        /**
         * Fetch Formularconfigs
         */
        $formularconfigs = $this->Formularconfigs->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $formularconfigs->toArray();
    }

    public function formularconfigs()
    {
        $this->set('data', $this->_getFormularconfigs());
    }

    protected function _getNewsletterinterests()
    {
        $this->loadModel('Newsletterinterests');

        /**
         * Fetch Newsletterinterests
         */
        $newsletterinterests = $this->Newsletterinterests->find()
          ->where([
              //
          ])
          ->order([
            'name' => 'asc',
          ])
          ->all();

        return $newsletterinterests->toArray();
    }

    public function newsletterinterests()
    {
        $this->set('data', $this->_getNewsletterinterests());
    }

    protected function _getAuth()
    {
        /**
         * Activate authentication
         */
        AuthManager::start();

        /**
         * Activate authentication
         */
        AuthManager::cacheUser();

        /**
         * Get logged user
         */
        return AuthManager::get();
    }

    public function auth()
    {
        $this->set('data', $this->_getAuth());
    }

    protected function _getConfig()
    {
        return Configure::read('Sx', []);
    }

    public function config()
    {
        $this->set('data', $this->_getConfig());
    }

    protected function _getLocales()
    {
        return DomainManager::getAllowedLanguages();
    }

    public function locales()
    {
        $this->set('data', $this->_getLocales());
    }

    protected function _getArticle()
    {
        $this->loadModel('Articles');

        return [
          'types'   => $this->Articles->getTypes(),
          'layouts' => $this->Articles->getTypeLayouts(),
        ];
    }

    public function article()
    {
        $this->set('data', $this->_getArticle());
    }

    /**
     * Fetch stats for system
     */
    public function stats()
    {
        $data = [];

        $this->loadModel('Articles');

        $data['articles']['total'] = $this->Articles
          ->find('all')->count();

        $data['articles']['active'] = $this->Articles
          ->find('published')->count();

        $this->loadModel('Attachments');

        $data['attachments']['total'] = $this->Attachments
          ->find('all')->where(['type !=' => 'folder'])->count();

        $data['attachments']['active'] = $this->Attachments
          ->find('attached')->where(['type !=' => 'folder'])->count();

        $this->loadModel('Menus');

        $data['menus']['total'] = $this->Menus
          ->find('all')->count();

        $data['menus']['active'] = $this->Menus
          ->find('published')->count();


        $this->set('data', $data);

    }

    public function debug()
    {
        $env = file_get_contents(ROOT . DS . '.env');

        if ( ! $this->getRequest()->getData('value', false) ) {
            $env = str_replace('APP_DEBUG="true"', 'APP_DEBUG="false"', $env);
        } else {
            $env = str_replace('APP_DEBUG="false"', 'APP_DEBUG="true"', $env);
        }

        file_put_contents(ROOT . DS . '.env', $env);

        $env = file_get_contents(ROOT . DS . '.env');

        $this->flashSuccess();
    }

    /**
     * Fetch extras from webroot
     */
    public function extras()
    {
        $path = $this->request->getQuery('path', '');

        $folder = new Folder(
          WWW_ROOT . 'resources' . DS . 'extras' . ltrim($path, '/')
        );

        $files = [];

        $languages = [
          'js' => 'javascript', 'css' => 'css', 'php' => 'php',
        ];

        foreach ( $folder->findRecursive('.*?\.(js|css|php)', true) as $fileName ) {

            $fileExt = preg_replace('/(.*?)\.(.*?)/',
              '$2', $fileName);

            $content = file_get_contents($fileName);

            $fileName = str_replace(WWW_ROOT . 'resources' .
              DS . 'extras' . DS, '', $fileName);

            $files[] = [
              'label' => $fileName, 'language' => $languages[$fileExt], 'content' => $content,
            ];
        }

        $this->set('data', $files);
    }

    /**
     * Update extras
     */
    public function postExtras()
    {
        dd('yay');
    }

    public function elements()
    {
        $path = $this->request->getQuery('path', '');

        if ( $path === '' ) {
            $path = $this->request->getData('path', '');
        }

        $templates = [];

        $folder = new Folder(
          SX . 'templates' . DS . ltrim($path, '/')
        );

        foreach ( $folder->find('.*?\.php', true) as $fileName ) {

            $fileName = preg_replace('/\.php$/', '', $fileName);

            $templates[$fileName] = [
              'value' => $fileName, 'label' => StringUtility::humanCamel($fileName),
            ];
        }

        $folder = new Folder(
          APP . 'templates' . DS . ltrim($path, '/')
        );

        foreach ( $folder->find('.*?\.php', true) as $fileName ) {

            $fileName = preg_replace('/\.php$/', '', $fileName);

            $templates[$fileName] = [
              'value' => $fileName, 'label' => StringUtility::humanCamel($fileName),
            ];
        }

        $this->set('data', array_values($templates));
    }


    public function index($aco_id = null)
    {
        /**
         * Set export
         */
        $this->set('export', [
          'domains' => $this->_getDomains(),
          'acos' => $this->_getAcos(),
          'roles' => $this->_getRoles(),
          'users' => $this->_getUsers(),
          'resources' => $this->_getResources(),
          'emails' => $this->_getEmails(),
          'formulars' => $this->_getFormulars(),
          'formularconfigs' => $this->_getFormularconfigs(),
          'newsletterinterests' => $this->_getNewsletterinterests(),
          'auth' => $this->_getAuth(),
          'config' => $this->_getConfig(),
          'article' => $this->_getArticle(),
          'locales' => $this->_getLocales(),
        ]);

        $this->viewBuilder()->setLayout('system');
    }

}
