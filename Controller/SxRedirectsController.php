<?php

namespace App\Controller;

/**
 * @version 4.0.0
 */

class SxRedirectsController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function indexQuery($query)
    {
        if ( $this->getRequest()->getQuery('notify') ) {
            $query->where(['notify' => true]);
        }

        return $query;
    }

    /**
     * @inheritdoc
     */
    public function importEntity($entity)
    {
        if ( is_null($entity->active) ) {
            $entity->active = true;
        }

        return $entity;
    }

}
