<?php

namespace App\Controller;

use App\Controller\Component\Vuejs\VuejsExportComponent;
use App\Controller\Component\Vuejs\VuejsImportComponent;
use App\Controller\Component\Vuejs\VuejsQueryComponent;
use App\Library\Facades\Language;
use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;
use Exception;

class SxCrudController extends AppController
{
    public $paginate = [];

    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('VuejsQuery', [
            'className' => VuejsQueryComponent::class
        ]);

        $this->loadComponent('VuejsImport', [
            'className' => VuejsImportComponent::class
        ]);

        $this->loadComponent('VuejsExport', [
            'className' => VuejsExportComponent::class
        ]);
    }

    public function beforeFilter(EventInterface $event)
    {
        $this->Authentication->addUnauthenticatedActions([
            'dispatchview'
        ]);

        return parent::beforeFilter($event);
    }

    public function tree()
    {
        ini_set('memory_limit', '512M');

        $query = $this->getModelTable()->find('threaded');

        if ( method_exists($this, 'treeQuery') ) {
            $this->treeQuery($query);
        }

        $this->set('data', $this->VuejsQuery->tree($query));
    }

    public function index()
    {
        $query = $this->VuejsQuery->filter();

        if ( method_exists($this, 'indexQuery') ) {
            $this->indexQuery($query);
        }

        $this->set($this->VuejsQuery->output($query));
    }

    public function total()
    {
        $query = $this->getModelTable()->find();

        if ( method_exists($this, 'totalQuery') ) {
            $this->totalQuery($query);
        }

        $this->set('data', $query->toArray());
    }

    // article_id, menu_id, action, extra
    public function dispatchview($id, $menu_id = null, $action = null, $params = [])
    {
        if ( method_exists($this, $action) ) {
            $this->{$action}($id, $menu_id, $action, $params);
        } else {
            $this->view($id, $menu_id, $action, $params);
        }
    }

    public function view($id)
    {
        $data = [];

        if ( $id ) {
            $data = $this->getModelTable()->find()
                ->where([
                    $this->modelClass . '.id' => $id
                ])
                ->contain(
                    $this->getModelTable()->getDefaultContains()
                )
                ->first();
        }

        if ( empty($data) ) {
            throw new NotFoundException();
        }

        $this->set('data', $data);
    }

    public function beforeLoad($table)
    {
        //
    }

    public function queryLoad($query)
    {
        return $query;
    }

    public function afterLoad($data)
    {
        return $data;
    }

    public function load($id = null, $withHooks = true)
    {
        if ( $this->getModelTable()->hasBehavior('Translation') ) {

            //
            $this->getModelTable()->behaviors()->get('Translation')
                ->mergeConfig(['forceRealLocale' => true]);
        }

        if ( method_exists($this, 'beforeLoad') ) {
            $this->beforeLoad($this->getModelTable());
        }

        /** @var \Cake\ORM\Query $query */
        $query = $this->getModelTable()->find()
            ->where([
                $this->getModelTable()->getAlias() . '.id IS' => $id
            ])
            ->contain(
                $this->getModelTable()->getDefaultContains()
            );

        if ( $withHooks && method_exists($this, 'queryLoad') ) {
            $query = $this->queryLoad($query);
        }

        /** @var \App\Model\Entity\SxAppEntity $entity */
        $entity = $query->first();

        if ( empty($entity) ) {
            $entity = $this->getModelTable()->newEntity(['id' => $id]);
        }

        $entity = $entity->autofill();

        if ( $withHooks && method_exists($this, 'afterLoad') ) {
            $entity = $this->afterLoad($entity);
        }

        $this->set('data', $entity);
    }

    public function beforeSavePatch($data)
    {
        return $data;
    }

    public function beforeSave($entity, $data = null)
    {
        return $entity;
    }

    public function save($id = null)
    {
        $this->getModelTable()->begin();

        $entity = $this->getModelTable()->find()
            ->where([
                $this->getModelTable()->getAlias() . '.id IS' => $id
            ])
            ->contain(
                $this->getModelTable()->getDefaultContains()
            )
            ->first();

        $data = $this->request->getData();

        if ( method_exists($this, 'beforeSavePatch') ) {
            $data = $this->beforeSavePatch($data);
        }

        if ( empty($entity) ) {
            $entity = $this->getModelTable()->newEntity(['id' => $id]);
        }

        $this->getModelTable()->patchEntity($entity, $data,
            ['validate' => 'default']);

        // Required for error messages
        $this->set('data', $entity);

        if ( method_exists($this, 'beforeSave') ) {
            $this->beforeSave($entity, $data);
        }

        // Modified fix
        $entity->set('modified', new \DateTime);

        try {
            $entity = $this->getModelTable()->save($entity, [
             'atomic' => false
            ]);
        } catch (Exception $exception) {
            throw new \Exception($exception->getMessage() ?: __d('system', 'Die Daten konnten nicht gespeichert werden'), 500);
        }

        if ( $entity ) {

            if ( method_exists($this, 'afterSave') ) {
                $this->afterSave($entity, $data);
            }

            $this->flashSuccess();
            $this->getModelTable()->commit();
        } else {
            $this->flashError(__d('system', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist'), 422);
            $this->getModelTable()->rollback();
        }

        if ( empty($entity) ) {
            return;
        }

        Language::inLanguage($entity->get('_locale'), function() use ($entity) {
            $this->load($entity->id);
        });
    }

    public function beforeDelete($entity)
    {
        return $entity;
    }

    public function delete($ids = null)
    {
        if ( ! $ids ) {
            $ids = $this->getRequest()->getData('ids');
        }

        /** @var \App\Model\Table\AppTable $modelTable */
        $modelTable = $this->getModelTable();

        $deleted = true;

        $modelTable->begin();

        foreach ( $ids = explode(',', $ids) as $id ) {

            if ( empty($id) ) {
                continue;
            }

            $entity = $modelTable->get($id);

            if ( empty($entity) ) {
                continue;
            }

            if ( method_exists($this, 'beforeDelete') ) {
                $entity = $this->beforeDelete($entity);
            }

            $deleted = $deleted && !! $modelTable->delete($entity);
        }

        if ( $deleted ) {
            $modelTable->commit();
            $this->flashSuccess(__d('system', 'Die Daten wurden erfolgreich gelöscht.'));
        } else {
            $modelTable->rollback();
            $this->flashError(__d('system', 'Die Daten konnten nicht gelöscht werden.'), 500);
        }

        $this->set('data', []);
    }

    public function beforeCopy($entity)
    {
        return $entity;
    }

    public function copy($ids = null)
    {
        if ( ! $ids ) {
            $ids = $this->getRequest()->getQuery('ids');
        }

        $copied = true;

        $this->getModelTable()->begin();

        foreach ( $ids = explode(',', $ids) as $id ) {

            if ( empty($id) ) {
                continue;
            }

            /** @var \App\Model\Entity\Article $entity */
            $copied = $copied && !! Language::inDefaultLanguage(function () use ($id) {

                $query = $this->getModelTable()->find()
                    ->where([
                        $this->getModelTable()->getAlias() . '.id' => $id
                    ])
                    ->contain(
                        $this->getModelTable()->getDefaultContains()
                    );

                if ( $this->getModelTable()->hasBehavior('Translation') ) {
                    $query = $query->find('translations');
                }

                $entity = $query->first();

                $clone = $entity->dublicate();

                if ( method_exists($this, 'beforeCopy') ) {
                    $clone = $this->beforeCopy($clone);
                }

                $clone->set('modified', new \DateTime);

                foreach ( $entity->get('_translations', []) as $translation ) {

                    /** @var \App\Model\Entity\ArticlesTranslation $translation */

                    $translation->set('id', $clone->id);
                    $translation->isNew(true);

                    if ( method_exists($this, 'beforeCopyTranslation') ) {
                        $translation = $this->beforeCopyTranslation($translation);
                    }

                    $translation->set('modified', new \DateTime);
                }

                return !! $entity->getTable()->save($clone,
                    ['atomic' => false]);
            });

        }

        if ( $copied ) {
             $this->getModelTable()->commit();
            $this->flashSuccess(__d('system', 'Die Daten wurden erfolgreich kopiert.'));
        } else {
             $this->getModelTable()->rollback();
            $this->flashError(__d('system', 'Die Daten konnten nicht kopiert werden.'), 500);
        }

        if ( count($ids) === 1 && isset($entity) ) {
            $this->load($entity->id);
        }
    }

    public function export()
    {
        $query = $this->VuejsQuery->filter(null, true);

        if ( method_exists($this, 'indexQuery') ) {
            $this->indexQuery($query);
        }

        $filePath = $this->VuejsExport->export($query);

        return $this->response->withFile($filePath, [
            'download' => true,
        ]);
    }

    public function import()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        \Laminas\Diactoros\UploadedFile::class;

        $replace = $this->request->getData('replace', false);
        $file = $this->request->getData('file', null);

        // check for empty post
        if ( empty($file) && ! is_object($file) ) {
            throw new Exception(__('Bitte wählen Sie eine Datei aus.'), 400);
        }

        $filePath = TMP . $file->getClientFilename();

        // move uploaded file from php tmp to cake tmp
        $file->moveTo($filePath);

        // move uploaded file from php tmp to cake tmp
        if ( ! file_exists($filePath) ) {
            throw new Exception(__('Die Datei konnte nicht verschoben werden.'), 400);
        }

        $data = $this->VuejsImport->import([$this, 'importEntity'], $filePath, [
            'replaceModel' => format_type('boolean', $replace)
        ]);

        if ( count($data[1]) ) {
            return $this->flashError(__('Irgendwas ist schief gelaufen.'));
        }

        $this->flashSuccess(__('Insgesamt {0} Einträgen importiert.',
            count($data)));
    }

    public function importEntity($entity)
    {
        // Import entity data
        return $entity;
    }

}
