<?php

namespace App\Controller\Traits;

use App\Library\AuthManager;
use App\Library\Facades\Language;
use Cake\Utility\Hash;

trait MoveTrait
{

    public function recover()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        AuthManager::stop();
        print_r($this->{$this->modelClass}->recover());
    }

    public function move()
    {
        AuthManager::pause();
        Language::setLanguage();

        $this->{$this->modelClass}->begin();

        $ids = $this->request->getData('source_id');

        $target = $this->{$this->modelClass}->get(
            $this->request->getData('target_id')
        );

        $position = $this->request->getData('position');

        if ( empty($target) ) {
            throw new \Exception(__('Ziel konnte nicht gefunden werden.'), 500);
        }

        $move = true;

        if ( $position == 'inner' ) {
            $sources = explode(',', $ids);
            $move = $this->_moveInner($sources, $target, $position);
        }

        if ( $position === 'before' || $position === 'after' ) {
            $sources = explode(',', $ids);
            $move = $this->_moveBeforeOrAfter($sources, $target, $position);
        }

        if ( !! $move ) {
            $this->{$this->modelClass}->commit();
        } else {
            $this->{$this->modelClass}->rollback();
            throw new \Exception(__('Einträge konnten nicht verschoben werden.'), 500);
        }

        return AuthManager::resume();
    }

    protected function _moveInner($sources, $target, $position)
    {
        $move = true;

        foreach ($sources as $source) {

            $source = $this->{$this->modelClass}->get($source);

            if ( empty($source) ) {
                throw new \Exception(__('Quelle konnte nicht gefunden werden.'), 500);
            }

            if ( method_exists($this, 'beforeMoveInner') ) {
                $this->beforeMoveInner($source, $target);
            }

            $this->{$this->modelClass}->patchEntity($source, [
                'parent_id' => $target->id
            ]);

            $move = $move && !! $this->{$this->modelClass}
                ->save($source, ['atomic' => false]);

            // Move down if its the same folder
            $move = $move && !! $this->{$this->modelClass}
                ->moveDown($source, true);

        }

        return $move;
    }

    protected function _moveBeforeOrAfter($sources, $target, $position)
    {
        $move = true;

        foreach ($sources as $source) {

            $source = $this->{$this->modelClass}->get($source);

            if ( empty($source) ) {
                throw new \Exception(__('Quelle konnte nicht gefunden werden.'), 500);
            }

            if ( method_exists($this, 'beforeMoveBeforeOrAfter') ) {
                $this->beforeMoveBeforeOrAfter($source, $target);
            }

            $this->{$this->modelClass}->patchEntity($source, [
                'parent_id' => $target->parent_id
            ]);

            $move = $move && !! $this->{$this->modelClass}
                ->save($source, ['atomic' => true]);

            $parentKey = 'parent_id';

            if ( $target->parent_id === null ) {
                $parentKey = $parentKey . ' IS';
            }

            $childs = $this->{$this->modelClass}->find()
                ->where([
                    $parentKey => $target->parent_id
                ])->order([
                    'lft' => 'asc'
                ])->toArray();

            $ids = Hash::extract($childs, '{n}.id');

            $targetPos = array_search($target->id, $ids);
            $sourcePos = array_search($source->id, $ids);

            if ( $sourcePos > $targetPos ) {
                $move = $move && !! $this->{$this->modelClass}->moveUp($source,
                    $sourcePos - $targetPos - ($position === 'after' ? 1 : 0));
            }

            if ( $sourcePos < $targetPos ) {
                $move = $move && !! $this->{$this->modelClass}->moveDown($source,
                    $targetPos - $sourcePos - ($position === 'before' ? 1 : 0));
            }

        }

        return $move;
    }

}
