<?php

namespace App\Controller\Traits;

use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use App\Library\AuthManager;
use App\Library\Facades\Language;
use Cake\ORM\Locator\TableLocator;
use Exception;

trait InsertTrait
{
    public $sourceClass = 'Articles';

    public function insert()
    {
        AuthManager::pause();
        Language::setLanguage();

        $this->{$this->modelClass}->begin();

        $ids = $this->request->getData('source_id');

        $target = $this->{$this->modelClass}->get(
            $this->request->getData('target_id')
        );

        $position = $this->request->getData('position');

        if ( empty($target) ) {
            throw new \Exception(__('Ziel konnte nicht gefunden werden.'), 500);
        }

        $insert = true;

        if ( $position === 'inner' ) {
            $sources = array_reverse(explode(',', $ids));
            $insert = $this->_insertInner($sources, $target, $position);
        }

        if ( $position === 'before' || $position === 'after' ) {
            $sources = array_reverse(explode(',', $ids));
            $insert = $this->_insertBeforeOrAfter($sources, $target, $position);
        }

        if ( !! $insert ) {
            $this->{$this->modelClass}->commit();
        } else {
            $this->{$this->modelClass}->rollback();
            throw new \Exception(__('Einträge konnten nicht verschoben werden.'), 500);
        }

        return AuthManager::resume();
    }

    protected function _insertInner($sources, $target, $position)
    {
        $insert = true;

        $update = $this->request->getData('update');

        if ( empty($update) ) {
            throw new Exception('Unbekannter Fehler');
        }

        foreach ($sources as $source) {

            $source = TableRegistry::getTableLocator()
                ->get($this->sourceClass)->get($source);

            if ( empty($source) ) {
                throw new \Exception(__('Quelle konnte nicht gefunden werden.'), 500);
            }

            $source['tabparents'] = [
                array_merge(['id' => $target->id], $update)
            ];

            $this->{$this->modelClass}->saveMenus($source, 'Article');

            $final = $this->{$this->modelClass}->find()
                ->where([
                    'parent_id'   => $target->id, 
                    'foreign_key' => $source->id
                ])
                ->first();

            if ( method_exists($this, 'beforeInsertInner') ) {
                $this->beforeInsertInner($final, $target);
            }

            $insert = $insert && !! $this->{$this->modelClass}
                ->save($final, ['atomic' => true]);

            // Move down if its the same folder
            $insert = $insert && !! $this->{$this->modelClass}
                ->moveDown($final, true);

        }

        return $insert;
    }

    protected function _insertBeforeOrAfter($sources, $target, $position)
    {
        $insert = true;

        $update = $this->request->getData('update');

        if ( empty($update) ) {
            throw new Exception('Unbekannter Fehler');
        }

        foreach ($sources as $source) {

            $source = TableRegistry::getTableLocator()
                ->get($this->sourceClass)->get($source);

            if ( empty($source) ) {
                throw new \Exception(__('Quelle konnte nicht gefunden werden.'), 500);
            }

            $source['tabparents'] = [
                array_merge(['id' => $target->parent_id], $update)
            ];

            $this->{$this->modelClass}->saveMenus($source, 'Article');

            $final = $this->{$this->modelClass}->find()
                ->where([
                    'parent_id'   => $target->parent_id, 
                    'foreign_key' => $source->id
                ])
                ->first();

            if ( method_exists($this, 'beforeInsertBeforeOrAfter') ) {
                $this->beforeInsertBeforeOrAfter($final, $target);
            }

            $insert = $insert && !! $this->{$this->modelClass}
                ->save($final, ['atomic' => true]);

            $parentKey = 'parent_id';

            if ( $target->parent_id === null ) {
                $parentKey = $parentKey . ' IS';
            }

            $childs = $this->{$this->modelClass}->find()
                ->where([
                    $parentKey => $target->parent_id
                ])->order([
                    'lft' => 'asc'
                ])->toArray();

            $ids = Hash::extract($childs, '{n}.id');

            $targetPos = array_search($target->id, $ids);
            $finalPos = array_search($final->id, $ids);

            if ( $finalPos > $targetPos ) {
                $insert = $insert && !! $this->{$this->modelClass}->moveUp($final,
                    $finalPos - $targetPos - ($position === 'after' ? 1 : 0));
            }

            if ( $finalPos < $targetPos ) {
                $insert = $insert && !! $this->{$this->modelClass}->moveDown($final,
                    $targetPos - $finalPos - ($position === 'before' ? 1 : 0));
            }

        }

        return $insert;
    }

}
